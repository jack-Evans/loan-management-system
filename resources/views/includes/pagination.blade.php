@if(isset($pagination))

    @php

    if(isset($pagination->links->previous)) {
        $parts_prev = parse_url($pagination->links->previous);
        parse_str($parts_prev['query'], $query);
        $previous = $query['page'];
    }

    if(isset($pagination->links->next)) {
        $parts_next = parse_url($pagination->links->next);
        parse_str($parts_next['query'], $query);
        $next = $query['page'];
    }

    @endphp

    @if(isset($previous))
        <a href="{{ url(''.$page.'?page=' . $previous) }}">Previous</a>
    @endif
    @if(isset($next))
        <a href="{{url(''.$page.'?page=' . $next) }}">Next</a>
    @endif

@endif
