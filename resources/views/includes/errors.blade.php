<!-- dd(count($errors), !is_array($errors)) -->
        @if(count($errors) > 0 && !is_array($errors))
            @foreach ($errors->all() as $error)
            <div class="row">
              <div class="col-6 col-offset-2">
                <div class="alert alert-danger">{{ $error }}</div>
               </div>
            </div>
            @endforeach
        @endif

