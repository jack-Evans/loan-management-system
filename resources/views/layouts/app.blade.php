<!DOCTYPE html>
<html>
<head>
    <title>{{env('app_name')}} - @yield('title')</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="csrf-token" content="{{ csrf_token() }}" />
</head>
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
<link href="https://unpkg.com/gijgo@1.9.11/css/gijgo.min.css" rel="stylesheet" type="text/css" />
<link href="{{url('assets/css').'/style.css'}}" rel="stylesheet" type="text/css" />
<!-- <link href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet" type="text/css" /> -->
<link href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.11.2/css/all.css" rel="stylesheet" type="text/css" />
<!-- MDBootstrap Datatables  -->
<link href="{{url('assets/css').'/datatables.min.css'}}" rel="stylesheet">
<!-- <link rel="stylesheet" href="{{url('assets/css').'/jquery.dropdown.css'}}"> -->
<link href="{{url('assets/css').'/multiselect.css'}}" rel="stylesheet">
<!-- For jquery select box -->
<link href="{{url('assets/css').'/multi-select.css'}}" rel="stylesheet">
<link rel="stylesheet" href="https://unpkg.com/ag-grid-community/dist/styles/ag-grid.css">
<link rel="stylesheet" href="https://unpkg.com/ag-grid-community/dist/styles/ag-theme-balham.css">
<link rel="stylesheet" href="https://unpkg.com/ag-grid-community/dist/styles/ag-theme-alpine.css">
<link rel="stylesheet" href="https://unpkg.com/ag-grid-community/dist/styles/ag-theme-material.css">
<link href="https://cdn.jsdelivr.net/npm/select2@4.0.13/dist/css/select2.min.css" rel="stylesheet" />
@yield('page-css')
<body>
  <header>
    <div class="top-navigation">
      <div class="container">
        <div class="row">
          <div class="col-lg-5">
            <ul class="navigation align-left">
              <li>
                <a class="nav-link padding-right-o padding-left-o " href="#"><i class="fa fa-comment"></i></a>
              </li>
              <li>
                <a class="nav-link" href="#"><i class="fa fa-envelope"></i></a>
              </li>
            </ul>
          </div>
          <div class="col-lg-2">
            <ul class="navigation">
              <li class="logo-text">
                <a href="#">
                  <image src="{{url('images/kuflink_logo-white.png')}}" alt="kuflink-logo">
                </a>
              </li>
            </ul>
          </div>
          <div class="col-lg-5">
            <ul class="navigation align-right">
              <li class="nav-item hidden-sm-down search-box">
                <a class="nav-link hidden-sm-down text-muted waves-effect waves-dark" href="javascript:void(0)">
                  <i class="fa fa-search"></i>
                </a>
                  <form class="app-search" style="display: none;">
                    <input type="text" class="form-control" placeholder="Search &amp; enter">
                    <a class="srh-btn"><i class="ti-close"></i></a>
                  </form>
              </li>
              <li class="nav-item dropdown pull-right">
                <a class="nav-link dropdown-toggle text-muted" href="" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true">
                  <img src="{{url('images/1.jpg')}}" alt="user" class="profile-pic">
                </a>
                <div class="dropdown-menu dropdown-menu-right scale-up">
                  <ul class="dropdown-user">
                    <li>
                      <div class="dw-user-box">
                        <div class="u-img">
                          <img src="{{url('images/1.jpg')}}" alt="user">
                        </div>
                        <div class="u-text">
                          <h4>Steave Jobs</h4>
                          <p class="text-muted">varun@gmail.com</p>
                          <a href="pages-profile.html" class="btn btn-rounded btn-danger btn-sm">View Profile</a>
                        </div>
                      </div>
                    </li>
                    <li><a href="#"><i class="fa fa-user"></i> My Profile</a></li>
                    <li><a href="#"><i class="fa fa-book"></i> My Balance</a></li>
                    <li><a href="#"><i class="fa fa-envelope"></i> Inbox</a></li>
                    <li><a href="#"><i class="fa fa-cog"></i> Account Setting</a></li>
                    <li><a href="#"><i class="fa fa-power-off"></i> Logout</a></li>
                  </ul>
                </div>
              </li>
            </ul>
          </div>
        </div>
      </div>
    </div>
    <div class="sub-header">
      <div class="container">
        <div class="row">
          <div class="col-lg-12">
            <div class="sub-navigation">
              <ul class="navigation">
                <li class=""><a href="{{ url('calculations') }}"><i class="fa fa-calculator"></i> Selected loan</a></li>
                <li class=""><a href="{{ url('loans') }}"><i class="fas fa-list"></i></i> All Loans</a></li>
                <li class=""><a href="{{ url('nominalcodes') }}"><i class="fas fa-seedling"></i></i> Nominal Codes</a></li>
                <li class=""><a href="{{ url('tags') }}"><i class="fas fa-seedling"></i></i> Tags</a></li>
                <!-- <li class=""><a href="{{ url('loans') }}">Loan</a></li> -->
                <li class=""><a href="{{ url('charges') }}"><i class="fa fa-calculator"></i> Initial Charges</a></li>
                <li class=""><a href="{{ url('closingcharges') }}"><i class="fas fa-file-invoice-dollar"></i> Closing Charges</a></li>
                <li class="active"><a href="{{ url('customers') }}"><i class="fa fa-users"></i> Customers</a></li>
                <li class=""><a href="{{ url('holidays') }}"><i class="fab fa-accusoft"></i> Holidays</a></li>
                <!-- {{-- <li class=""><a href="{{ url('datatypes') }}"><i class="fab fa-accusoft"></i> Datatypes</a></li> --}} -->
                <!-- {{-- <li class=""><a href="{{ url('fileinfo') }}"><i class="fab fa-accusoft"></i> File Info</a></li> --}} -->
                <li class=""><a href="{{ url('optionalvariables') }}"><i class="fab fa-accusoft"></i> Optional Variables</a></li> 
                <li class=""><a href="{{ url('grids') }}"><i class="fab fa-accusoft"></i> Grids</a></li>
                <li class=""><a href="{{ url('retained-loan-summary') }}"><i class="fab fa-accusoft"></i> Retained Loan Summary</a></li>
                <li class=""><a href="{{ url('statement-ending-balance') }}"><i class="fab fa-accusoft"></i> Statement Ending Balance</a></li>
                <li class=""><a href="{{ url('logout') }}"><i class="fa fa-power-off"></i> Logout</a></li>
              </ul>
            </div>
          </div>
        </div>
      </div>
    </div>
  </header>
  @yield('wrapper1')
  <div class="main-wrapper">
    <div class="container">
        <div class="row">
          <div class="col-lg-12">
            @if(count($errors) > 0 && !is_array($errors))
            <div class="has-bg">
              @include('includes.errors')
            </div>
            @endif
          </div>
          @yield('content')

        </div>
    </div>
    <footer>
      <div class="copyright-section align-center">
        <p>© {{\Carbon\Carbon::now()->format('Y')}} Kuflink All right Reserved </p>
      </div>
    </footer>
  </div>
<script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.11.2/js/all.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>
<script src="https://unpkg.com/gijgo@1.9.11/js/gijgo.min.js" type="text/javascript"></script>
<script src="https://cdn.jsdelivr.net/npm/sweetalert2@8"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.18.1/moment.min.js"></script>
<!-- MDBootstrap Datatables  -->
<script type="text/javascript" src="{{url('assets/js').'/datatables.min.js'}}"></script>
<!-- <script src="{{url('assets/js').'/jquery.dropdown.js'}}"></script> -->
<script src="{{url('assets/js').'/multiselect.min.js'}}"></script>
<!-- <script src="https://unpkg.com/ag-grid-community/dist/ag-grid-community.min.js"></script> -->
<!-- <script src="https://unpkg.com/ag-grid-community/dist/ag-grid-community.min.noStyle.js"></script> -->
<script src="https://unpkg.com/ag-grid-enterprise/dist/ag-grid-enterprise.min.noStyle.js"></script>
<!-- <script src="https://cdn.ckeditor.com/ckeditor5/16.0.0/classic/ckeditor.js"></script> -->
<script src="https://cdn.jsdelivr.net/npm/select2@4.0.13/dist/js/select2.min.js"></script>
<!-- For jquery select box -->
<script src="{{url('assets/js').'/jquery.multi-select.js'}}"></script>
<!-- <script src="{{url('assets/js').'/drag-arrange.js'}}"></script> -->
@yield('pages-js')
<script>
  $(document).ready(function(){
    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });
    $('[data-toggle="tooltip"]').tooltip();
    // $.ajaxSetup({ headers: { 'csrftoken' : '{{ csrf_token() }}' } });
    
    $('.js-multi-search').select2({
      width: '100%',
      placeholder: 'Select optional variables to map',
      allowClear: true,
      closeOnSelect: false,
    });

    $('.js-loan-tags').select2({
      width: '100%',
      placeholder: 'Select loan tags',
      allowClear: true,
      closeOnSelect: false,
    });

    $(window).keydown(function(event){
      if(event.keyCode == 13) {
        event.preventDefault();
        return false;
      }
    });
    $('.MDBootstrapDatatable').DataTable({
      "scrollX": true
    });
    $('.MDOptionalVariableDataTable').DataTable({
      "scrollX": true,
      "ordering": false,
    });
    $('.dataTables_length').addClass('bs-select');

  });
  
  var date = (date === undefined) ? null : date;
  if(date){
    var d = new Date(date);
    var day = (d.getDate() > 9) ? d.getDate() : ('0' + d.getDate());
    var month = (d.getMonth() > 8) ? (d.getMonth() + 1) : ('0' + (d.getMonth() + 1));
    var year = d.getFullYear();
    date = year + "-" + month + "-" + day;
  } else {
    date = null;
  }
    $('#multiSelect').multiselect();
    $('#multiSelect1').multiselect();
    $('#multiSelect2').multiselect();

    $('.datepicker').datepicker({
        uiLibrary: 'bootstrap4',
        format: 'yyyy-mm-dd'
    });

    $('.datepicker1').datepicker({
        uiLibrary: 'bootstrap4',
        format: 'yyyy-mm-dd'
    });

    $('.datepicker2').datepicker({
        uiLibrary: 'bootstrap4',
        format: 'yyyy-mm-dd'
    });

    $('.datepicker3').datepicker({
        uiLibrary: 'bootstrap4',
        format: 'yyyy-mm-dd',
        value: date
    });
    $('.datepicker4').datepicker({
        uiLibrary: 'bootstrap4',
        format: 'yyyy-mm-dd',
        value: date
    });
    $('.datepicker5').datepicker({
        uiLibrary: 'bootstrap4',
        format: 'yyyy-mm-dd'
    });
    $('.datepicker6').datepicker({
        uiLibrary: 'bootstrap4',
        format: 'yyyy-mm-dd',
        value: date
    });
    $('.datepicker7').datepicker({
        uiLibrary: 'bootstrap4',
        format: 'yyyy-mm-dd'
    });
    $('.datepicker8').datepicker({
        uiLibrary: 'bootstrap4',
        format: 'yyyy-mm-dd',
        value: date
    });
    $('.datepicker9').datepicker({
        uiLibrary: 'bootstrap4',
        format: 'yyyy-mm-dd'
    });
    $('.datepicker10').datepicker({
        uiLibrary: 'bootstrap4',
        format: 'yyyy-mm-dd'
    });
    $('.datepicker11').datepicker({
        uiLibrary: 'bootstrap4',
        format: 'yyyy-mm-dd'
    });
    $('.datepicker12').datepicker({
        uiLibrary: 'bootstrap4',
        format: 'yyyy-mm-dd'
    });
    $('.datepicker13').datepicker({
        uiLibrary: 'bootstrap4',
        format: 'yyyy-mm-dd'
    });
    $('.datepicker14').datepicker({
        uiLibrary: 'bootstrap4',
        format: 'yyyy-mm-dd',
        value: date
    });
    $('.datepicker15').datepicker({
        uiLibrary: 'bootstrap4',
        format: 'yyyy-mm-dd',
        value: date
    });
    $('.datepicker16').datepicker({
        uiLibrary: 'bootstrap4',
        format: 'yyyy-mm-dd'
    });
    $('.datepicker17').datepicker({
        uiLibrary: 'bootstrap4',
        format: 'yyyy-mm-dd'
    });
    $('.datepicker18').datepicker({
        uiLibrary: 'bootstrap4',
        format: 'yyyy-mm-dd'
    });
    $('.datepicker19').datepicker({
        uiLibrary: 'bootstrap4',
        format: 'yyyy-mm-dd'
    });
    $('.datepicker20').datepicker({
        uiLibrary: 'bootstrap4',
        format: 'yyyy-mm-dd'
    });
    $('.datepicker21').datepicker({
        uiLibrary: 'bootstrap4',
        format: 'yyyy-mm-dd'
    });
    $('.datepicker22').datepicker({
        uiLibrary: 'bootstrap4',
        format: 'yyyy-mm-dd'
    });
    $('.datepicker23').datepicker({
        uiLibrary: 'bootstrap4',
        format: 'yyyy-mm-dd'
    });
    $('.datepicker24').datepicker({
        uiLibrary: 'bootstrap4',
        format: 'yyyy-mm-dd'
    });
    $('.datepicker25').datepicker({
        uiLibrary: 'bootstrap4',
        format: 'yyyy-mm-dd'
    });

    // $(".confirm-alert").click(function(e){
    //     e.preventDefault();
    //     var link = $(this).data('href');
    //     var confirmButtonText = $(this).data('confirm-btn-txt');
    //     var successText = $(this).data('success-txt');
    //     var freezeDate = $("#freeze-date").val();
    //     link = link+"&freezeDate="+freezeDate;
    //     console.log(link);
    //     Swal.fire({
    //         title: 'Are you sure?',
    //         text: $(this).data('text'),
    //         type: 'warning',
    //         showCancelButton: true,
    //         confirmButtonColor: '#3085d6',
    //         cancelButtonColor: '#d33',
    //         confirmButtonText: confirmButtonText
    //     }).then((result) => {
    //         if (result.value) {
    //             window.location.href = link;
    //             Swal.fire(
    //               'Success!',
    //               successText,
    //               'success'
    //             );
    //         }
    //     })
    // });

    $('.freezeForm').submit(function(e){
      e.preventDefault();
      var link = $(this).data('href');
      var successText = $(this).data('success-txt');
      var freezeDate = $('.freeze-date').val();
      link = link+"&freezeDate="+freezeDate;
      Swal.fire({
        title: 'Are you sure?',
        text: 'The loan will be freezed.',
        type: 'warning',
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        confirmButtonText: "Yes, Freeze loan!"
      }).then((result) => {
        if (result.value) {
          window.location.href = link;
          Swal.fire(
            'Success!',
            'Your freeze loan request is processing.!',
            'success'
          );
        }
      });
    });

    $('.delete-loan').on('click', function(e){
      e.preventDefault();
      var link = $(this).prop('href');
      console.log(link);
      Swal.fire({
        title: 'Are you sure?',
        text: 'The loan will be deleted.',
        type: 'warning',
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        confirmButtonText: "Yes, Delete loan!"
      }).then((result) => {
        if (result.value) {
          window.location.href = link;
          Swal.fire(
            'Success!',
            'Your delete loan request is processing.!',
            'success'
          );
        }
      });
    });

    $('.js-delete').on('click', function(e){
      e.preventDefault();
      var link = $(this).prop('href');
      var title = $(this).data('title');
      console.log(title);
      Swal.fire({
        title: 'Are you sure?',
        text: 'The '+title+' will be deleted.',
        type: 'warning',
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        confirmButtonText: "Yes, Delete "+title+"!"
      }).then((result) => {
        if (result.value) {
          window.location.href = link;
          Swal.fire(
            'Success!',
            'Your delete '+title+' request is processing.!',
            'success'
          );
        }
      });
    });

    $('.unFreezeForm').submit(function(e){
      e.preventDefault();
      var link = $(this).data('href');
      var successText = $(this).data('success-txt');
      var freezeDate = $('.un-freeze-date').val();
      link = link+"&freezeDate="+freezeDate;
      Swal.fire({
        title: 'Are you sure?',
        text: 'The loan will be un-freezed.',
        type: 'warning',
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        confirmButtonText: 'Yes, Un-freeze loan!'
      }).then((result) => {
        if (result.value) {
          window.location.href = link;
          Swal.fire(
            'Success!',
            'Your un-freeze loan request is processing.!',
            'success'
          );
        }
      });
    });

    $('.js-close-loan').on('click', function(e){
      e.preventDefault();
      var link = $(this).data('href');
      var successText = $(this).data('success-txt');
      Swal.fire({
        title: 'Are you sure?',
        text: 'The loan will be closed!',
        type: 'warning',
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        confirmButtonText: 'Yes, Close loan!'
      }).then((result) => {
        if (result.value) {
          window.location.href = link;
          Swal.fire(
            'Success!',
            'Your close loan request is processing!',
            'success'
          );
        }
      });
    });

    $(':submit').click(function() {
      $(this).attr('disabled', 'disabled');
      var btn = $(this);
      $(this).parents('form').submit();

      // setTimeout(function() {
      //   btn.prop('disabled', false);
      // }, 5000);
      setTimeout(function() {
        $('.js-addInitialCharge').attr('disabled', false);
      }, 5000);

    });    

    $('#mt-amount').on('change paste keyup', function(){
      var duration = parseFloat($('#mt-amount').val()) / parseFloat($('#mt-monthlyPmt').val());
      // var total = amount / $('#mt-minTerm').val();
      $('#mt-minTerm').val((duration).toFixed(2));
    });

    $('#mt-minTerm, #mt-monthlyPmt').on('change paste keyup', function(){
      adjustBalance();
    });

    function adjustBalance(){

      var term = $('#mt-minTerm').val();
      var monthlyPmt = $('#mt-monthlyPmt').val();

      var total = (term * monthlyPmt) - $('#mt-amountPaid').val();

      $('#mt-amount').val((total).toFixed(2));

    }

    $('.closeLoanCalculator').on('change paste keyup', function(e){
      // $('.js-closeloan-btn').attr('disabled', 'disabled');
      var closeDate = $("#CLFreezeDate").val();
      var virtualPmt = $("#CLVirtualPmt").val();
      console.log("calculations/currentbalance/"+closeDate+"/"+virtualPmt);
      $.ajax({
        url: "calculations/currentbalance/"+closeDate+"/"+virtualPmt,
        success: function(result){
          var date = moment(closeDate).format('ll');
          var breakdown = result['breakdown'];
          $('.js-closing-balance').html('<p>Closing loan balance = <b>'+result['closing_balance']+'</b></p>');

          $('.js-final-breakdown').html(
            '<table class="table table-striped table-hover table-bordered text-left m-0">'+
              '<thead>'+
                '<tr>'+
                  '<th>Amount Due</th>'+
                  '<th>Amount Paid</th>'+
                  '<th>Amount Pending</th>'+
                  '<th>Amount Type</th>'+
                  '<th></th>'+
                  '<th></th>'+
                  '<th></th>'+
                '</tr>'+
              '</thead>'+
              '<tbody>'+
                '<tr>'+
                  '<td>'+breakdown['amount']+'</td>'+
                  '<td>'+breakdown['amount_paid']+'</td>'+
                  '<td>'+breakdown['amount_pending']+'</td>'+
                  '<td>'+breakdown['type']+
                  (breakdown['payment_break_down'] ?
                    '<div class="target-description" data-key="pmtPlanBrkDn">'+
                      '<span class="text-primary fas fa-chevron-up cursor"></span>'+
                    '</div>'
                    : ''
                  ) +
                  '</td>'+
                  '<td></td>'+
                  '<td></td>'+
                  '<td></td>'+
                '</tr>'+

              // main contents finished, now main breakdown
                (breakdown['payment_break_down'] ?
                  '<tr class="show-descriptionpmtPlanBrkDn font-weight-bold" style="display: none; background-color: #efefef;">'+
                    '<td>Date</td>'+
                    '<td>Amount</td>'+
                    '<td>Description</td>'+
                    '<td></td>'+
                    '<td></td>'+
                    '<td></td>'+
                    '<td></td>'+
                  '</tr>'+
                  $.map(breakdown['payment_break_down'], function(brkdnVal, brkdnKey){
                  return '<tr class="show-descriptionpmtPlanBrkDn" style="display: none; background-color: #efefef;">'+
                    '<td>'+moment(brkdnVal['created_at']['date']).format('ll')+'</td>'+
                    '<td>'+brkdnVal['amount']+'</td>'+
                    '<td>'+brkdnVal['description']+
                    (brkdnVal['sub_break_down'] ?
                      '<div class="target-description" data-key="'+brkdnKey+'pmtPlanSubBrkDn">'+
                      '<span class="text-primary fas fa-chevron-up cursor"></span>'+
                      '</div>'
                      : ''
                    ) +
                    '</td>'+
                    '<td></td>'+
                    '<td></td>'+
                    '<td></td>'+
                    '<td></td>'+
                  '</tr>'+
                  // sub break down
                    (brkdnVal['sub_break_down'] ?
                      '<tr class="show-description'+brkdnKey+'pmtPlanSubBrkDn font-weight-bold" style="display: none; background-color: #c7bdbd;">'+
                        '<td>From</td>'+
                        '<td>To</td>'+
                        '<td>Days</td>'+
                        '<td>Amount</td>'+
                        '<td>Rate</td>'+
                        '<td>Daily Rate</td>'+
                        '<td>Total</td>'+
                      '</tr>'+
                      $.map(brkdnVal['sub_break_down'], function(planSubBrkdnVal, planSubBrkdnKey){
                        return  $.map(planSubBrkdnVal, function(subBrkdnVal, subBrkdnKey){
                          return '<tr class="show-description'+brkdnKey+'pmtPlanSubBrkDn" style="display: none; background-color: #c7bdbd;">'+
                            '<td>'+moment(subBrkdnVal['start']['date']).format('ll')+'</td>'+
                            '<td>'+moment(subBrkdnVal['end']['date']).format('ll')+'</td>'+
                            '<td>'+subBrkdnVal['days']+'</td>'+
                            '<td>'+subBrkdnVal['balance']+'</td>'+
                            '<td>'+subBrkdnVal['rate']+'</td>'+
                            '<td>'+subBrkdnVal['daily_rate']+'</td>'+
                            '<td>'+subBrkdnVal['total']+'</td>'+
                          '</tr>'
                          })
                      })
                    : ''
                    )
                  })
                  : ''
                ) +
              '</tbody>'+
            '</table>'
            );
          $('.js-closeloan-btn').attr('disabled', false);
        },
      });
    });

  @php $nominalCodes = isset($nominalCodes) ? $nominalCodes : []; @endphp
  $("#addInitialCharge").submit(function(event){
    event.preventDefault(); //prevent default action       
    var post_url = $(this).attr("action"); //get form action url
    var request_method = $(this).attr("method"); //get form GET/POST method
    var form_data = $(this).serialize(); //Encode form elements for submission
    $.ajax({
      url : post_url,
      type: request_method,
      data : form_data,
      success: function(result){
        var charge = result['data'];
        console.log($('.js-keyGetter').last().data('key'));
        var key = $('.js-keyGetter').last().data('key')+1;

        if(charge['chargetype'] === 'percentage')
          $('.js-retainedLoanForm').append(
            '<div class="col-8 offset-2">'+
              '<div class="form-group">'+
                
                '<div class="row">'+
                  '<div class="col-9">'+
                    '<label class="text-capitalize">'+charge['name']+':</label>'+
                  '</div>'+
                  '<div class="col-3">'+
                    '<label for="nominalCode">Nominal Codes:</label>'+
                  '</div>'+
                '</div>'+
                
                '<input type="hidden" name="RLChargeId[]" value="'+charge['id']+'">'+
                
                '<div class="row">'+
                  '<div class="col-3">'+
                    '<input type="text" class="form-control js-percentChange js-valChange'+key+'" onchange="adjustPercentage();" data-key="'+key+'" value="'+charge['value']+'">'+
                  '</div>'+
                  '<div class="col-3">'+
                    '<p class="small">% of Gross loan = </p>'+
                  '</div>'+
                  '<div class="col-3">'+
                    '<input type="text" class="form-control js-RLInitialCharge'+key+' js-fixedValChange js-keyGetter" data-key="'+key+'" onchange="percentFixedValChange(this);" name="initialChargeValue[]" value="0">'+
                  '</div>'+
                  '<div class="col-3">'+
                    '<select class="form-control" name="nominalCodeForCharges[]">'+
                      '@foreach($nominalCodes as $nominalCode)'+
                        '<option value="{{$nominalCode->id}}">{{$nominalCode->code}}</option>'+
                      '@endforeach'+
                    '</select>'+
                  '</div>'+
                  
                '</div>'+
              '</div>'+
            '</div>'
          );
        else
          $('.js-retainedLoanForm').append(
            '<div class="col-8 offset-2">'+
              '<div class="form-group">'+
                
                '<div class="row">'+
                  '<div class="col-9">'+
                    '<label class="text-capitalize">'+charge['name']+':</label>'+
                  '</div>'+
                  '<div class="col-3">'+
                    '<label for="nominalCode">Nominal Codes:</label>'+
                  '</div>'+
                '</div>'+
                
                '<input type="hidden" name="RLChargeId[]" value="'+charge['id']+'">'+
                
                '<div class="row">'+
                  '<div class="col-7">'+ 
                    '<input type="text" class="form-control js-fixedValChange js-RLInitialCharge'+key+' js-keyGetter" onchange="adjustLoan();" data-key="'+key+'" name="initialChargeValue[]" value="'+charge['value']+'">'+
                  '</div>'+
                  '<div class="col-3 offset-2">'+
                    '<select class="form-control" name="nominalCodeForCharges[]">'+
                      '@foreach($nominalCodes as $nominalCode)'+
                        '<option value="{{$nominalCode->id}}">{{$nominalCode->code}}</option>'+
                      '@endforeach'+
                    '</select>'+
                  '</div>'+
                '</div>'+

              '</div>'+
            '</div>'
          );
      $('#addNewChargeModal').modal('toggle');
      adjustLoan();
      },
      error: function(errors){
        $('.js-errors').html("");
        setTimeout(
          function()
          {
          $('.js-errors').html("<p class='text-danger text-center'>Opps an error occurred. There can be any of the following errors <br/> i) Name is missing ii) Name isn't unique iii) Name length might be greater than 50 characters</p>");
          }, 1000);
      },
    });
  });

$(".js-searchName").on('change', function(){
  var searchName = $('.js-searchName').val();
  console.log(searchName);
  $.ajax({
    url : "{{url('searchoption')}}",
    type: 'get',
    data : {name: searchName},
    success: function(result){
      var data = "";
        data += $.map(result.message.data, function(item){
          console.log(item.create_date == null, item.create_date === false, item.updated_at == null, item.updated_at === false);
          return  "<tr>"+
                    "<td>"+item.name+"</td>"+
                    "<td>"+item.value+"</td>"+
                    "<td></td>"+
                    "<td></td>"+
                    "<td>"+
                      "<button type='button' class='btn btn-primary ml-1 editOptionModal'> Edit</button>"+
                      "<button type='button' class='btn btn-danger ml-1'>Delete</button>"+
                    "</td>"+
                  "</tr>";
        });
      $('.js-optionalVariable').html(data);
    }, 
    error: function(errors){
      console.log(errors);
    }
  });
});

  $('.js-RLDuration').on('change paste keyup', function(e){
    setEndDate();
    serviceLoanDuration();
  });

  $('.js-RLIssuedate').on('change paste keyup', function(e){
    var duration = parseFloat($('.js-RLDuration').val());
    if(isNaN(duration) == false){
      setEndDate();
      adjustPercentage();
      serviceLoanDuration();
    } else {
      setDuration();
      adjustPercentage();
      // serviceLoanDuration();
    }
  });
  $('.js-RLEndDate').on('change paste keyup', function(e){
    setDuration();
    adjustPercentage();
    serviceLoanDuration();
  });

  function setEndDate()
  {
    var issuedate = $('.js-RLIssuedate').val();
    var duration = parseFloat($('.js-RLDuration').val());
    var monthNumber = duration.toString().split(".")[0];
    var daysOfDec = parseFloat(0+"."+(duration.toString().split(".")[1])) * 30.4;
    // var endDate = moment(issuedate).add(dur, 'M').subtract(1, 'days');
    var endDate = moment(issuedate).add(monthNumber, 'M');
    endDate = moment(endDate).add(daysOfDec, 'd');

    var totalNoOfDays = moment(endDate).diff(moment(issuedate), 'days');
    $('.js-RLNoOfDays').val(totalNoOfDays);

    // $('.js-ServiceLoanStartDate').val(endDate.format('YYYY-MM-DD'));
    $('.js-RLEndDate').val(endDate.subtract(1, 'days').format('YYYY-MM-DD'));
    // serviceLoanDuration();
  }

  $(document).on('change paste keyup', ".js-duration2", function(e){
    serviceLoanDuration();
  });
  
  function serviceLoanDuration()
  {
    var issuedate = $('.js-RLEndDate').val();
    // var issuedate = $('.js-ServiceLoanStartDate').val();
    var duration2 = parseFloat($('.js-duration2').val());
    var monthNumber = duration2.toString().split(".")[0];
    var daysOfDec = parseFloat(0+"."+(duration2.toString().split(".")[1])) * 30.4;
    var endDate = moment(issuedate).add(monthNumber, 'M');
    endDate = moment(endDate).add(daysOfDec, 'd');
    $('.js-ServiceLoanStartDate').val(moment(issuedate).add(1, 'd').format('YYYY-MM-DD'));
    $('.js-ServiceLoanEndDate').val(endDate.format('YYYY-MM-DD'));
  }
  function setDuration()
  {
    var fromDate = $('.js-RLIssuedate').val(), toDate = $('.js-RLEndDate').val(), from, to, months, days, totalNoOfDays;
    from = moment(fromDate); // format in which you have the date
    to = moment(toDate).add(1, 'days');     // format in which you have the date
    /* using diff */
    months = to.diff(from, 'months');
    totalNoOfDays = to.diff(from, 'days');
    months = to.diff(from, 'months', true).toFixed(2);
    $('.js-RLDuration').val(months);
    $('.js-RLNoOfDays').val(totalNoOfDays);
  }

  $('.js-RLGrossLoan, .js-RLDuration, .js-RLRate, .js-fixedValChange').on('change paste keyup', function(e){
    adjustPercentage();
  });

  $('.js-percentFixedVal').on('change paste keyup', function(e){
    percentFixedValChange($(this));
  });
  function percentFixedValChange(parent){
    var gross = $('.js-RLGrossLoan').val();
    var value = $(parent).val();
    console.log(value);
    var key = $(parent).data('key');
    var percentageVal = ((value/gross)*100).toFixed(8);
    $('.js-valChange'+key).val(percentageVal);
    adjustLoan();
  }

  $('.js-percentChange').on('change paste keyup', function(e){
    var gross = $('.js-RLGrossLoan').val();
    var value = $(this).val();
    var percentVal = (gross*(value/100)).toFixed(2);
    var key = $(this).data('key');
    $('.js-RLInitialCharge'+key).val(percentVal);
    adjustLoan();
  });

  function adjustPercentage()
  {
    adjustLoan();
    $('.js-percentChange').each(function() {
      var gross = $('.js-RLGrossLoan').val();
      var value = $(this).val();
      var percentVal = (gross*(value/100)).toFixed(2);
      var key = $(this).data('key');
      $('.js-RLInitialCharge'+key).val(percentVal);
    });
    adjustLoan();
  }


  function adjustLoan(){
    var gross = $('.js-RLGrossLoan').val();
    var rate = $('.js-RLRate').val();
    var defaultRate = $('.js-RLDefaultRate').val();
    // var duration = $('.js-RLDuration').find(':selected').text();
    var duration = $('.js-RLDuration').val();
    var noOfDays = $('.js-RLNoOfDays').val();

    var charges = 0;
    $('input[name="initialChargeValue[]"]').each( function() {
      charges += Number(this.value);
    });

    var perMonth = (gross*(rate/100)).toFixed(2);
    var interest = (perMonth*duration).toFixed(2);
    var dailyRate = (interest/noOfDays).toFixed(2);
    var roundingError = (interest-(dailyRate*noOfDays)).toFixed(2);
    var netLoan = (gross-interest-charges).toFixed(2);

    $('.js-RLRoundingError').val(roundingError);
    $('.js-RLRatePerDay').val(dailyRate);
    $('.js-RLRatePerMonth').val(perMonth);
    $('.js-RLInterest').val((dailyRate*noOfDays).toFixed(2));
    $('.js-RLNetLoan').val(netLoan);
  }

  function toDate(dateStr) {
    var parts = dateStr.split("-");
    return new Date(parts[0], parts[1] - 1, parts[2]);
  }

  $(document).on("click", ".createHalfLoanModal", function () {
    $('.js-ServiceDuration').html(
      '<div class="row">'+
        '<div class="col-4">'+
          '<div class="form-group">'+
            '<label for="duration">Service loan duration(in months):</label>'+
            '<input class="form-control js-duration2" type="text" name="duration2" value="6">'+
          '</div>'+
        '</div>'+

        '<div class="col-4">'+
          '<div class="form-group">'+
            '<label for="duration">Service loan start date:</label>'+
            '<input class="form-control js-ServiceLoanStartDate" type="text" name="serviceLoanStartDate" readonly>'+
          '</div>'+
        '</div>'+

        '<div class="col-4">'+
          '<div class="form-group">'+
            '<label for="duration">Service loan end date:</label>'+
            '<input class="form-control js-ServiceLoanEndDate" type="text" name="serviceLoanEndDate" readonly>'+
          '</div>'+
        '</div>'+
      '</div>'
    );
    $("input[name=loanType]").val('half');
    $('.js-LoanModalTitle').text('Create half-half loan:');
    $('.js-issuedate').text('Retained loan issue date:');
    $('.js-duration').text('Retained loan duration(in months):');
    $('.js-endDate').text('Retained loan end date:');
  });

  $(document).on("click", ".createRetainedLoanModal", function () {
    $('.js-ServiceDuration').html('');
    $("input[name=loanType]").val('retained');
    $('.js-LoanModalTitle').text('Create Retained Loan:');
    $('.js-issuedate').text('Issue date:');
    $('.js-duration').text('Duration(in months):');
    $('.js-endDate').text('End date:');
  });
  
  // var defaultRateRefunds = [];
  // var insertCount = removeCount = 0;
  // $('.bulkCheckBox').on('click', function(){
  //   var refundData = [];
  //   var index = $(this).data('index');
  //   refundData.push($(this).data('loanid'));
  //   refundData.push($(this).data('start-date'));
  //   refundData.push($(this).data('end-date'));
  //   refundData.push($(this).data('days'));
  //   refundData.push($(this).data('balance'));
  //   refundData.push($(this).data('refund-date'));
  //   refundData.push($(this).data('index'));

  //   // console.log($(this).prop("checked"));
  //   console.log(index);
  //   if($(this).prop("checked")){
  //     defaultRateRefunds[index] = refundData;
  //     // insertCount++;
  //   } else{
  //     // console.log(index,index-removeCount);
  //     // console.log(defaultRateRefunds.splice((index-removeCount),1));
  //     defaultRateRefunds[index] = [];
  //     // console.log(defaultRateRefunds.splice((index-removeCount),0), []);
  //     // removeCount++
  //     // if(insertCount == removeCount)
  //       // insertCount = removeCount = 0;
  //     // defaultRateRefunds.splice(index,1);
  //   }
  //   console.log(defaultRateRefunds);
  // });

  // $('.js-refund-bulk').on('click', function(){
  //   var messageBox = jQuery('#message-box');
  //   $.map(defaultRateRefunds, function(refund, key){
  //     console.log(key);
  //     if(typeof refund === "undefined" || refund.length == 0){return;}

  //     console.log(refund);
  //     $.ajax({
  //       beforeSend: function() {
  //         messageBox.text('Please wait while refunds are in progress.');
  //       },
  //       // complete: function() {
  //         // messageBox.text('Refunds completed successfully.');
  //         // window.location.reload();
  //       // },
  //       url : "{{url('calculations/createdefaultraterefund')}}",
  //       type: 'post',
  //       data : {loanId:refund[0],startDate:refund[1],endDate:refund[2],days:refund[3],balance:refund[4],refundDate:refund[5],json:true},
  //       success: function(result){
  //         // messageBox.text('Refunds done.');
  //         messageBox.text(result.message);
  //         console.log(result);
  //         if(result.code == 200)
  //           window.location.reload();
  //       }, 
  //       error: function(error){
  //         // messageBox.text('Oops an error occurred while refunding bulk refunds.');
  //         messageBox.text(error.message);
  //         console.log(error);
  //       }
  //       // console.log(refund[0]);
  //     });
  //   });
  //   $(this).attr('disabled', false);
  // });

</script>
</body>
</html>
