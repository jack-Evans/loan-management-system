  var defaultRateRefunds = [];
  var hide = hideTransaction = null;

  $('.js-hideTransaction').on('click', function(){
    $(".js-classEye").toggleClass('fa-eye fa-eye-slash')
    if(hideTransaction == 1)
      hideTransaction = null;
    else
      hideTransaction = 1;
  });

  $('.js-hide').on('click', function(){
    $(".js-classEye2").toggleClass('fa-eye fa-eye-slash')
    if(hide == 1)
      hide = null;
    else
      hide = 1;
  });

  $('.bulkCheckBox').on('click', function(){
    var refundData = [];
    var index = $(this).data('index');
    refundData.push($(this).data('loanid'));
    refundData.push($(this).data('start-date'));
    refundData.push($(this).data('end-date'));
    refundData.push($(this).data('days'));
    refundData.push($(this).data('balance'));
    refundData.push($(this).data('refund-date'));
    refundData.push($(this).data('index'));

    // console.log($(this).prop("checked"));
    console.log(index);
    if($(this).prop("checked")){
      defaultRateRefunds[index] = refundData;
      // insertCount++;
    } else{
      // console.log(index,index-removeCount);
      // console.log(defaultRateRefunds.splice((index-removeCount),1));
      defaultRateRefunds[index] = [];
      // console.log(defaultRateRefunds.splice((index-removeCount),0), []);
      // removeCount++
      // if(insertCount == removeCount)
        // insertCount = removeCount = 0;
      // defaultRateRefunds.splice(index,1);
    }
    console.log(defaultRateRefunds);
  });

  $('.js-refund-bulk').on('click', function(){
    var messageBox = jQuery('#message-box');
    var bulkRefundDate = $('.js-bulkRefundDate').val();
    $.map(defaultRateRefunds, function(refund, key){
      if(typeof refund === "undefined" || refund.length == 0){return;}
      // console.log(refund, hide, hideTransaction, "{{url('calculations/createdefaultraterefund')}}");
      if(bulkRefundDate)
        refund[5] = bulkRefundDate;
      $.ajax({
        beforeSend: function() {
          messageBox.text('Please wait while refunds are in progress.');
        },
        // complete: function() {
          // messageBox.text('Refunds completed successfully.');
          // window.location.reload();
        // },
        url : "calculations/createdefaultraterefund",
        type: 'post',
        data : {loanId:refund[0],startDate:refund[1],endDate:refund[2],days:refund[3],balance:refund[4],refundDate:refund[5],json:true,hide:hide,hideTransaction:hideTransaction},
        success: function(result){
          messageBox.text(result.message);
          console.log(result);
          if(result.code == 200)
            window.location.reload();
        },
        error: function(error){
          // messageBox.text('Oops an error occurred while refunding bulk refunds.');
          messageBox.text(error.message);
          console.log(error);
        }
        // console.log(refund[0]);
      });
    });
    $(this).attr('disabled', false);
  });

  $(".js-showAllTnx").on('click', function(){
    var showAllTnx = $(this).val();
    if(showAllTnx == 1)
      showAllTnx = 0;
    else
      showAllTnx = 1;
    console.log(showAllTnx);
    $.ajax({
      url : "show-all-tnx?showAllTnx="+showAllTnx,
      type: 'get',
      // data : {loanId:refund[0],startDate:refund[1],endDate:refund[2],days:refund[3],balance:refund[4],refundDate:refund[5],json:true,hide:hide,hideTransaction:hideTransaction},
      success: function(result){
        console.log(result);
        window.location.reload();
      },
      error: function(error){
        window.location.reload();
      }
    });
  });