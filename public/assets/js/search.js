var max_fields = 10; //maximum input boxes allowed
var wrapper = $(".input_fields_wrap"); //Fields wrapper
var addButton = $("#form-add"); //Add button ID
var form = $('#main-form');
var x = 1; //initlal text box count
// var operatorArray = ['AND', 'OR', 'NOT'];

// wrapper.append($("#content-template").html());
//alert($(".showForProg").length);
// setProg($(".showForProg"));

// $(document).on('change', '.alarm_action', function(e) {
//   var $container = $(this).parents('.form-field');
//   if ($(this).val() == "listofcompany") {
//     $('.filefield', $container).hide();
//     $(".myTextarea", $container).hide();
//     $(".showForProg", $container).show();

//   } else if ($(this).val() == "runprogram") {
//     $('.filefield', $container).hide();
//     $(".myTextarea", $container).hide();
//     $(".showForProg", $container).show();
//   } else {
//     $('.filefield', $container).hide();
//     $(".myTextarea", $container).hide();
//     $(".showForProg", $container).show();
//   }
// });

$(addButton).click(function(e) { //on add input button click
  e.preventDefault();
  if (x < max_fields) { //max input box allowed
    x++; //text box increment
    wrapper.append($("#content-template").html());
    // setProg($(".showForProg").last());
  } else {
    alert("Sorry, you have reached maximum add options.");
  }
});

$(wrapper).on("click", ".remove_field", function(e) { //user click on remove text
  e.preventDefault();
  $(this).parent().parent('div').remove();
  x--;
});

// $(document).on('change', 'select.removeDuplication', function(e) {
//   e.preventDefault();
//   var cI = $(this);
//   var others = $('select.removeDuplication').not(cI);
//   $.each(others, function() {
//     if ($(cI).val() == $(this).val() && $(cI).val() != "") {
//       $(cI).val('');
//       alert($(this).find('option:selected').text() + ' already selected.');
//     }
//   });
// });
// form.on('submit', function(e) {
//   e.preventDefault()
//   var queries = [];
//   var slectedall = true;
//   var fillupfield = true;
//   form.find('.form-field').each(function(index, field) {
//     var query = {};
//     query.type = $(field).find('select').val();

//     console.log(query.type);
//     if (query.type != "") {
//       if (query.type == "listofcompany") {
//         query.value = $(field).find(',filefield').val();
//       } else if (query.type == "runprogram") {
//         query.value = $(field).find(',showForProg').val();
//       } else {
//         query.value = $(field).find('textarea').val().replace(/\n/g, '\\n');
//       }
//       queries.push(query);
//     } else {
//       slectedall = false;
//     }
//   });
//   var url = window.location.href;
//   url += "/search/advanced/";
//   for (i = 0; i < queries.length; i += 1) {
//     var query = queries[i];
//     var ampOrQ = (i === 0) ? "?" : "&";
//     if (query.value.trim() === "") {
//       fillupfield = false;
//     } else {
//       url += ampOrQ + query.type + "=" + query.value;
//     }
//   };
//   if (slectedall === false) {
//     alert('Please select option.');
//   } else {
//     if (fillupfield === false) {
//       alert('Input can not be left blank');
//     } else {
//       //alert(url);
//       window.location.href = url;
//     }

//   }
//   console.log($(".js-search-submit").attr('disabled', false));

// });



// function setProg($programDropdown) {
//   $.each(operatorArray , function(key, value) {
//     $programDropdown
//      .append($("<option></option>")
//      .attr("value",value)
//      .text(value));
//    });
// }
