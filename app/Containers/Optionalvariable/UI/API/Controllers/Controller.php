<?php

namespace App\Containers\Optionalvariable\UI\API\Controllers;

use App\Containers\Optionalvariable\UI\API\Requests\CreateOptionalvariableRequest;
use App\Containers\Optionalvariable\UI\API\Requests\DeleteOptionalvariableRequest;
use App\Containers\Optionalvariable\UI\API\Requests\GetAllOptionalvariablesRequest;
use App\Containers\Optionalvariable\UI\API\Requests\FindOptionalvariableByIdRequest;
use App\Containers\Optionalvariable\UI\API\Requests\UpdateOptionalvariableRequest;
use App\Containers\Optionalvariable\UI\API\Requests\ImportOptionalVariableRequest;
use App\Containers\Optionalvariable\UI\API\Requests\MapOptionalvariableRequest;
use App\Containers\Optionalvariable\UI\API\Transformers\OptionalvariableTransformer;
use App\Containers\Optionalvariable\UI\API\Transformers\MovncVariableTransformer;
use App\Ship\Parents\Controllers\ApiController;
use Apiato\Core\Foundation\Facades\Apiato;

/**
 * Class Controller
 *
 * @package App\Containers\Optionalvariable\UI\API\Controllers
 */
class Controller extends ApiController
{
    /**
     * @param CreateOptionalvariableRequest $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function createOptionalvariable(CreateOptionalvariableRequest $request)
    {
        $optionalvariable = Apiato::call('Optionalvariable@CreateOptionalvariableAction', [$request]);

        return $this->created($this->transform($optionalvariable, OptionalvariableTransformer::class));
    }

    /**
     * @param FindOptionalvariableByIdRequest $request
     * @return array
     */
    public function findOptionalvariableById(FindOptionalvariableByIdRequest $request)
    {
        $optionalvariable = Apiato::call('Optionalvariable@FindOptionalvariableByIdAction', [$request]);

        return $this->transform($optionalvariable, OptionalvariableTransformer::class);
    }

    /**
     * @param GetAllOptionalvariablesRequest $request
     * @return array
     */
    public function getAllOptionalvariables(GetAllOptionalvariablesRequest $request)
    {
        $optionalvariables = Apiato::call('Optionalvariable@GetAllOptionalvariablesAction', [$request]);

        return $this->transform($optionalvariables, OptionalvariableTransformer::class);
    }

    /**
     * @param UpdateOptionalvariableRequest $request
     * @return array
     */
    public function updateOptionalvariable(UpdateOptionalvariableRequest $request)
    {
        $optionalvariable = Apiato::call('Optionalvariable@UpdateOptionalvariableAction', [$request]);

        return $this->transform($optionalvariable, OptionalvariableTransformer::class);
    }

    /**
     * @param DeleteOptionalvariableRequest $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function deleteOptionalvariable(DeleteOptionalvariableRequest $request)
    {
        Apiato::call('Optionalvariable@DeleteOptionalvariableAction', [$request]);

        return $this->noContent();
    }

    /**
     * @param GetAllOptionalvariablesRequest $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function mapOptionalVariable(MapOptionalvariableRequest $request)
    {
        Apiato::call('Optionalvariable@MapOptionalVariableAction', [$request]);

        return $this->noContent();
    }

    /**
     * @param GetAllOptionalvariablesRequest $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function unMapOptionalVariable(GetAllOptionalvariablesRequest $request)
    {
        Apiato::call('Optionalvariable@UnMapOptionalvariableAction', [$request]);

        return $this->noContent();
    }
    
    /**
     * @param GetAllOptionalvariablesRequest $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function syncOptionalVariable(GetAllOptionalvariablesRequest $request)
    {
        $newAdded = Apiato::call('Optionalvariable@SyncOptionalvariableAction', [$request]);

        return $newAdded;
    }

    /**
     * @param GetAllOptionalvariablesRequest $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function syncMovnc(GetAllOptionalvariablesRequest $request)
    {
        $movncVariables = Apiato::call('Optionalvariable@SyncMovncAction', [$request]);

        return $this->transform($movncVariables, MovncVariableTransformer::class);
    }

    /**
     * @param ImportOptionalVariableRequest $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function importOptionalVariable(ImportOptionalVariableRequest $request)
    {
        $movncVariables = Apiato::call('Optionalvariable@ImportOptionalVariableAction', [$request]);

        return $this->transform($movncVariables, MovncVariableTransformer::class);
    }
}
