<?php

namespace App\Containers\Optionalvariable\UI\API\Transformers;

use App\Containers\Optionalvariable\Models\Optionalvariable;
use App\Ship\Parents\Transformers\Transformer;

class OptionalvariableTransformer extends Transformer
{
    /**
     * @var  array
     */
    protected $defaultIncludes = [

    ];

    /**
     * @var  array
     */
    protected $availableIncludes = [

    ];

    /**
     * @param Optionalvariable $entity
     *
     * @return array
     */
    public function transform(Optionalvariable $entity)
    {
        $response = [
            'object' => 'Optionalvariable',
            'id' => $entity->getHashedKey(),
            'optional_variable' => $entity->optional_variable,
            'mapped_id' => $entity->mapped_id,
            'mapped' => $entity->mapped,
            'mandatory' => ($entity->mandatory === 1) ? 'Yes' : 'No',
            'used_for' => $entity->used_for,
            'notes' => $entity->notes,
            'used' => $entity->used,
            'empty_value' => $entity->empty_value,
            'optional_variable_group_id' => ($entity->optional_variable_group_id) ? $entity->optional_variable_group_id : null,
            'created_at' => $entity->created_at,
            'updated_at' => $entity->updated_at,

        ];

        $response = $this->ifAdmin([
            'real_id'    => $entity->id,
            // 'deleted_at' => $entity->deleted_at,
        ], $response);

        return $response;
    }
}
