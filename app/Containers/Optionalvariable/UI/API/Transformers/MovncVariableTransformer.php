<?php

namespace App\Containers\Optionalvariable\UI\API\Transformers;

use App\Containers\Option\Models\Option;
use App\Ship\Parents\Transformers\Transformer;

class MovncVariableTransformer extends Transformer
{
    /**
     * @var  array
     */
    protected $defaultIncludes = [

    ];

    /**
     * @var  array
     */
    protected $availableIncludes = [

    ];

    /**
     * @param Option $entity
     *
     * @return array
     */
    public function transform(Option $entity)
    {
        $response = [
            'object' => 'Options',
            'id' => $entity->hashid,
            'name' => $entity->name,
            'value' => $entity->value,
            'loanId' => $entity->loan->loan_id,

        ];

        $response = $this->ifAdmin([
            'real_id'    => $entity->id,
            // 'deleted_at' => $entity->deleted_at,
        ], $response);

        return $response;
    }
}
