<?php

/**
 * @apiGroup           Syncoptionalvariable
 * @apiName            updateOptionalvariable
 *
 * @api                {PATCH} /v1/apoptionalvariables/:id Endpoint title here..
 * @apiDescription     Endpoint description here..
 *
 * @apiVersion         1.0.0
 * @apiPermission      none
 *
 * @apiParam           {String}  parameters here..
 *
 * @apiSuccessExample  {json}  Success-Response:
 * HTTP/1.1 200 OK
{
  // Insert the response of the request here...
}
 */

/** @var Route $router */
$router->get('optionalvariables-sync', [
    'as' => 'api_optionalvariable__sync',
    'uses'  => 'Controller@syncOptionalVariable',
    'middleware' => [
      'auth:api',
    ],
]);
