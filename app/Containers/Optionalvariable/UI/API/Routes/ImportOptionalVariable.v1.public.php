<?php

/** @var Route $router */
$router->post('optionalvariables/import', [
    'as' => 'api_optionalvariable__import',
    'uses'  => 'Controller@importOptionalVariable',
    'middleware' => [
      'auth:api',
    ],
]);

?>