<?php

/**
 * @apiGroup           Optionalvariable
 * @apiName            findOptionalvariableById
 *
 * @api                {GET} /v1/optionalvariables/:id Endpoint title here..
 * @apiDescription     Endpoint description here..
 *
 * @apiVersion         1.0.0
 * @apiPermission      none
 *
 * @apiParam           {String}  parameters here..
 *
 * @apiSuccessExample  {json}  Success-Response:
 * HTTP/1.1 200 OK
{
  // Insert the response of the request here...
}
 */

/** @var Route $router */
$router->get('optionalvariables/{id}', [
    'as' => 'api_optionalvariable_find_optionalvariable_by_id',
    'uses'  => 'Controller@findOptionalvariableById',
    'middleware' => [
      'auth:api',
    ],
]);
