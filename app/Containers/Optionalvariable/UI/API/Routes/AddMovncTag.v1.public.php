<?php

/** @var Route $router */
$router->get('optionalvariables/movnc', [
    'as' => 'api_optionalvariable_sync_movnc',
    'uses'  => 'Controller@syncMovnc',
    'middleware' => [
      'auth:api',
    ],
]);


?>