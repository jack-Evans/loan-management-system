<?php

/**
 * @apiGroup           Mapoptionalvariable
 * @apiName            updateOptionalvariable
 *
 * @api                {PATCH} /v1/apoptionalvariables/:id Endpoint title here..
 * @apiDescription     Endpoint description here..
 *
 * @apiVersion         1.0.0
 * @apiPermission      none
 *
 * @apiParam           {String}  parameters here..
 *
 * @apiSuccessExample  {json}  Success-Response:
 * HTTP/1.1 200 OK
{
  // Insert the response of the request here...
}
 */

/** @var Route $router */
$router->post('optionalvariables/un-map', [
    'as' => 'api_optionalvariable__map',
    'uses'  => 'Controller@unMapOptionalVariable',
    'middleware' => [
      'auth:api',
    ],
]);
