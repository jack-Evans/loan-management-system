<?php

/**
 * @apiGroup           Optionalvariable
 * @apiName            deleteOptionalvariable
 *
 * @api                {DELETE} /v1/optionalvariables/:id Endpoint title here..
 * @apiDescription     Endpoint description here..
 *
 * @apiVersion         1.0.0
 * @apiPermission      none
 *
 * @apiParam           {String}  parameters here..
 *
 * @apiSuccessExample  {json}  Success-Response:
 * HTTP/1.1 200 OK
{
  // Insert the response of the request here...
}
 */

/** @var Route $router */
$router->delete('optionalvariables/{id}', [
    'as' => 'api_optionalvariable_delete_optionalvariable',
    'uses'  => 'Controller@deleteOptionalvariable',
    'middleware' => [
      'auth:api',
    ],
]);
