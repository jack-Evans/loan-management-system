@extends('layouts.app')

@section('title', 'Optional Variable')

@section('content')
<div class="col-lg-12">
    <div class="has-bg">
        <h3>{{isset($optionalVariable->id) ? 'Edit ' : 'Create '}}Optional Variable
          <a href="{{url('optionalvariables')}}" class="pull-right">
              <button class="btn btn-success">Back</button>
          </a>
        </h3>
        <form action="{{url(isset($optionalVariable->id) ? 'optionalvariables/'.$optionalVariable->id.'/update': 'optionalvariables/store' )}}" method="post">
          @csrf()
          <div class="row">
            <div class="col-md-6">
              <div class="form-group">
                <label for="optionalVariable">Optional Variable:</label>
                <input type="text" class="form-control" name="optionalVariable"
                       value="{{isset($optionalVariable->optional_variable) ? $optionalVariable->optional_variable : old('optionalVariable')}}">
              </div>

              <div class="form-group">
                <label>Is Mandatory ?</label>
                <select class="form-control" name="mandatory">
                  <option value="0">No</option>
                  <option value="1" {{ (isset($optionalVariable->mandatory) && $optionalVariable->mandatory == 'Yes' ) ? 'selected' : '' }}>Yes</option>
                </select>
              </div>
            </div>
            <div class="col-md-6">
              <div class="form-group">
                <label>Used For:</label>
                <input type="text" class="form-control" name="usedFor" value="{{isset($optionalVariable->used_for) ? $optionalVariable->used_for : old('usedFor')}}">
              </div>
              <div class="form-group">
                <label>Notes:</label>
                <input type="text" class="form-control" name="notes" value="{{isset($optionalVariable->notes) ? $optionalVariable->notes : old('notes')}}">
              </div>
              <button type="submit" class="btn btn-primary pull-right">Submit</button>
            </div>
          </div>
        </form>
    </div>
</div>
@endsection
