@extends('layouts.app')

@section('title', 'Optional Variable')

@section('wrapper1')
<div class="main-wrapper">
    <div class="container">
      <div class="has-bg">
        <h3>Movnc
          <a href="{{url('optionalvariables')}}" class="pull-right">
            <button class="btn btn-success">Back</button>
          </a>
        </h3>
        <div class="columns">
          <div class="column">
              <label class="option">
                  <input type="checkbox" id="columnWidth">
                  columnWidth =
                  <select id="columnWidthValue">
                      <option>100</option>
                      <option>200</option>
                      <option>myColumnWidthCallback</option>
                  </select>
              </label>
              <label class="option">
                  <input type="checkbox" id="sheetName">
                  sheetName =
                  <input type="text" id="sheetNameValue" value="custom-name" maxlength="31">
              </label>
              <label class="option">
                  <input type="checkbox" id="exportModeXml">
                  <span class="option-name">exportMode = "xml"</span>
              </label>
          </div>
          <div class="column" style="margin-left: 30px">
              <label class="option">
                  <input type="checkbox" id="suppressTextAsCDATA">
                  <span class="option-name">suppressTextAsCDATA</span>
              </label>
              <div class="option">
                  <label>
                      <input type="checkbox" id="rowHeight">
                      rowHeight =
                  </label>
                  <input type="text" id="rowHeightValue" value="30" style="width: 40px;">
              </div>
              <div class="option">
                  <label>
                      <input type="checkbox" id="headerRowHeight">
                      headerRowHeight =
                  </label>
                  <input type="text" id="headerRowHeightValue" value="40" style="width: 40px;">
              </div>
          </div>
        </div>
        <div style="margin: 5px;">
          <label>
              <button class="btn btn-primary" onclick="onBtExport()" style="margin: 5px; font-weight: bold;">Export to Excel</button>
          </label>
        </div>
        <div class="grid-wrapper">
            <div id="myGrid" class="ag-theme-material" style="height: 600px;width:100%;"></div>
        </div>
      </div>
    </div>
</div>
@endsection

@section('content')

  <!-- Edit option Modal Modal -->
<!--     <div class="modal" id="editOptionModal" data-backdrop="static">
      <div class="modal-dialog" style="max-width: 1100px">
        <div class="modal-content">
          <div class="modal-body custom_modal">
            <div class="row">
              <div class="col-lg-1">
                <div class="left-side">
                </div>
              </div>
              <div class="col-lg-11">
                <form action="{{url('calculations/createoption')}}" method="post" class="js-edit-option">
                <h4 class="modal-title"><span class="optModalTitle"></span> <button type="button" class="close" data-dismiss="modal">&times;</button></h4>
                  @csrf()
                  <div class="row">  
                    <div class="optId">
                      <input type="hidden" name="optId">
                      <input type="hidden" name="web" value="true">
                    </div>
                    <div class="col-4 form-group">
                      <label>Variable Name:</label>
                      <input class="form-control" type="text" name="name"/>
                    </div>

                    <div class="col-4 form-group">
                      <label>Value:</label>
                      <input class="form-control" type="text" name="value"/>
                    </div>

                    <div class="col-4 form-group">
                      <label>Create Date:</label>
                      <input class="form-control datepicker21" autocomplete="off" name="createDate"/>
                    </div>

                    <input type="hidden" name="mappedId">
                    
                    <div class="col-12">
                      <button type="submit" class="btn custom-padding pull-right btn-primary optionBtn js-ajax-form">Submit</button>
                    </div>

                    <p class="text-warning js-option-response"></p>

                  </div>
                </form>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div> -->

@endsection

@section('pages-js')

<script>
  // $(document).on("click", ".editOptionModal", function () {
  //   $('.optModalTitle').html('Edit Optional Variable ('+$(this).data('loan')+')');
  //   $("input[name=optId]").val($(this).data('opt-id'));
  //   $("input[name=name]").val($(this).data('name'));
  //   $("input[name=value]").val($(this).data('value'));
  //   $("input[name=createDate]").val($(this).data('create-date'));
  //   // $("input[name=mappedId]").val($(this).data('mapped-id'));
  //   // $(".optModalTitle").text('Edit Optional Variable');
  // });

  // $(".js-ajax-form").submit(function(e){
  //     e.preventDefault();
  //     var action = $(this).attr("action"); //get form action url
  //     var requestMethod = $(this).attr("method"); //get form GET/POST method
  //     var formData = $(this).serialize(); //Encode form elements for submission
  //     console.log(action, requestMethod, formData);
  //     $.ajax({
  //       url : action,
  //       type: requestMethod,
  //       data : formData,
  //       success: function(result){
  //         console.log('success');
  //         if(result.code == 400){
  //           $(".js-ajax-response").html('<p class="text-danger">Error updating transaction info. Please try again later.</p>');
  //         }
  //         else 
  //         window.location.reload();
  //       }, 
  //       error: function(errors){
  //         $(".js-ajax-response").html('<p class="text-danger">Error updating transaction info. Please try again later.</p>');
  //       }
  //     });
  //   });

  agGrid.LicenseManager.setLicenseKey("[TRIAL]_24_April_2020_[v2]_MTU4NzY4NjQwMDAwMA==4403d049826997713de83933aa326f78");
    // specify the columns
    var columnDefs = [
        {field: "loanId"},
        {field: "name"},
        {field: "value"},
    ];

    var gridOptions = {
        defaultColDef: {
            cellClassRules: {
                lightGreyBackground: function(params) {
                    return params.rowIndex % 2 == 0;
                }
            },
            sortable: true,
            editable: true,
            filter: true
        },
        columnDefs: columnDefs,
        rowSelection: 'multiple',
        onCellValueChanged: function(event) {
          console.log('data after changes is: ', event.data);
          var data = event.data;
          $.ajax({
            url: "{{url('calculations/createoption')}}",
            type: 'post',
            data: {optId: data.id, name: data.name, value: data.value},
            success: function(result){
                console.log(result);
            }, error: function(errors){
                console.log(errors);
            }
          });
        },

        excelStyles: [
            {
                id: 'greenBackground',
                interior: {
                    color: '#b5e6b5',
                    pattern: 'Solid'
                }
            },
            {
                id: 'redFont',
                font: {
                    fontName: 'Calibri Light',
                    underline: 'Single',
                    italic: true,
                    color: '#ff0000'
                }
            },
            {
                id: 'lightGreyBackground',
                interior: {
                    color: '#e8ebec',
                    pattern: 'Solid'
                },
                font: {
                    fontName: 'Calibri Light',
                    // color: '#ffffff'
                }
            },
            {
                id: 'boldBorders',
                borders: {
                    borderBottom: {
                        color: '#000000',
                        lineStyle: 'Continuous',
                        weight: 3
                    },
                    borderLeft: {
                        color: '#000000',
                        lineStyle: 'Continuous',
                        weight: 3
                    },
                    borderRight: {
                        color: '#000000',
                        lineStyle: 'Continuous',
                        weight: 3
                    },
                    borderTop: {
                        color: '#000000',
                        lineStyle: 'Continuous',
                        weight: 3
                    }
                }
            },
            {
                id: 'header',
                interior: {
                    color: '#CCCCCC',
                    pattern: 'Solid'
                },
                borders: {
                    borderBottom: {
                        color: '#5687f5',
                        lineStyle: 'Continuous',
                        weight: 1
                    },
                    borderLeft: {
                        color: '#5687f5',
                        lineStyle: 'Continuous',
                        weight: 1
                    },
                    borderRight: {
                        color: '#5687f5',
                        lineStyle: 'Continuous',
                        weight: 1
                    },
                    borderTop: {
                        color: '#5687f5',
                        lineStyle: 'Continuous',
                        weight: 1
                    }
                }
            },
            {
                id: 'dateFormat',
                dataType: 'dateTime',
                numberFormat: {
                    format: 'mm/dd/yyyy;@'
                }
            },
            {
                id: 'twoDecimalPlaces',
                numberFormat: {
                    format: '#,##0.00'
                }
            },
            {
                id: 'textFormat',
                dataType: 'string'
            },
            {
                id: 'bigHeader',
                font: {
                    size: 25
                }
            }
        ]
    };

    function getBooleanValue(cssSelector) {
        return document.querySelector(cssSelector).checked === true;
    }

    function getTextValue(cssSelector) {
        return document.querySelector(cssSelector).value;
    }

    function getNumericValue(cssSelector) {
        var value = parseFloat(getTextValue(cssSelector));
        if (isNaN(value)) {
            var message = "Invalid number entered in " + cssSelector + " field";
            alert(message);
            throw new Error(message);
        }
        return value;
    }

    function myColumnWidthCallback(params) {
        var originalWidth = params.column.getActualWidth();
        if (params.index < 7) {
            return originalWidth;
        }
        return 30;
    }

    function onBtExport() {
        var columnWidth = getBooleanValue('#columnWidth') ? getTextValue('#columnWidthValue') : undefined;
        var params = {
            columnWidth: columnWidth === 'myColumnWidthCallback' ? myColumnWidthCallback : parseFloat(columnWidth),
            sheetName: getBooleanValue('#sheetName') && getTextValue('#sheetNameValue'),
            exportMode: getBooleanValue('#exportModeXml') ? "xml" : undefined,
            suppressTextAsCDATA: getBooleanValue('#suppressTextAsCDATA'),
            rowHeight: getBooleanValue('#rowHeight') ? getNumericValue('#rowHeightValue') : undefined,
            headerRowHeight: getBooleanValue('#headerRowHeight') ? getNumericValue('#headerRowHeightValue') : undefined,
        };

        gridOptions.api.exportDataAsExcel(params);
    }

    // setup the grid after the page has finished loading
    document.addEventListener('DOMContentLoaded', function() {
        var gridDiv = document.querySelector('#myGrid');
        new agGrid.Grid(gridDiv, gridOptions);

        $.ajax({
            url: "{{url('optionalvariables-movnc?json=true')}}",
            type: 'get',
            success: function(result){
                var arr = [];
                console.log(result);
                $(result).each(function(i, val) { 
                   arr.push(val);
                });
                console.log(arr);
                gridOptions.api.setRowData(arr);
            }, error: function(errors){
                console.log('error');
            }
        });
    });

</script>

@endsection