@extends('layouts.app')

@section('title', 'Optional Variable')

@section('content')
<style type="text/css">
    tr.hide-table-padding td {
    padding: 0;
    }

    .expand-button {
        position: relative;
    }

    .accordion-toggle .expand-button:after
    {
      position: absolute;
      left:.75rem;
      top: 50%;
      transform: translate(0, -50%);
      content: '-';
    }
    .accordion-toggle.collapsed .expand-button:after
    {
      content: '+';
    }
</style>
<!-- <div class="main-wrapper"> -->
    <!-- <div class="container"> -->
        <!-- <div class="row"> -->
            <div class="col-lg-12">
              <!-- <div class="has-bg js-importErrorsAll">
              </div> -->
                <div class="has-bg js-importErrorsAll">
                  <h3>
                      Optional Variables
                      <a href="{{url('optionalvariables/create')}}" class="pull-right">
                          <button class="btn btn-success">Create Optional Variable</button>
                      </a>
                      <div class="target-description">
                        <span class="text-primary fas fa-chevron-up cursor"></span>
                      </div>
                  </h3>
                  <div class="show-description hidden">
                    <div class="columns">
                      <div class="column">
                          <label class="option">
                              <input type="checkbox" id="columnWidth">
                              columnWidth =
                              <select id="columnWidthValue">
                                  <option>100</option>
                                  <option>200</option>
                                  <option>myColumnWidthCallback</option>
                              </select>
                          </label>
                          <label class="option">
                              <input type="checkbox" id="sheetName">
                              sheetName =
                              <input type="text" id="sheetNameValue" value="custom-name" maxlength="31">
                          </label>
                          <label class="option">
                              <input type="checkbox" id="exportModeXml">
                              <span class="option-name">exportMode = "xml"</span>
                          </label>
                      </div>
                      <div class="column" style="margin-left: 30px">
                          <label class="option">
                              <input type="checkbox" id="suppressTextAsCDATA">
                              <span class="option-name">suppressTextAsCDATA</span>
                          </label>
                          <div class="option">
                              <label>
                                  <input type="checkbox" id="rowHeight">
                                  rowHeight =
                              </label>
                              <input type="text" id="rowHeightValue" value="30" style="width: 40px;">
                          </div>
                          <div class="option">
                              <label>
                                  <input type="checkbox" id="headerRowHeight">
                                  headerRowHeight =
                              </label>
                              <input type="text" id="headerRowHeightValue" value="40" style="width: 40px;">
                          </div>
                      </div>
                    </div>
                    <div style="margin: 5px;">
                        <label>
                            <button class="btn btn-primary" onclick="onBtExport()" style="margin: 5px; font-weight: bold;">Export to Excel</button>
                        </label>
                    </div>

                    <div class="grid-wrapper">
                      <div id="myGrid" class="ag-theme-material" style="height: 600px;width:100%;"></div>
                    </div>
                  </div>
                    
                  <span class="space"></span>
                  <div class="col-lg-12">
                  <form class="js-mapForm" action="{{ url('optionalvariables/map' )}}" method="post" enctype="multipart/form-data">
                    @csrf()
                    <span class="js-sync-response"></span>
                    <table class="table table-striped table-hover table-bordered text-left MDOptionalVariableDataTable" cellspacing="0" width="100%">
                      <thead>
                        <tr>
                          <th>Map checks</th>
                          <th>Optional Variable</th>
                          <th>Used For</th> 
                          <th>Notes</th> 
                          <th>Used</th> 
                          <th>Empty values</th>
                          <th>Optional Variable Group</th>
                          <th class="th-sm">Mapped Id</th>
                          <th class="th-sm">Is Mandatory</th>
                          <th>Actions</th>
                        </tr>
                      </thead>
                      <tbody>
                        @foreach($optionalVariables as $key => $optionalvariable)
                          <tr>
                            <td class="text-center"><input class="chk-box-zoom" type="checkbox" name="optionalVariableId[]" value="{{$optionalvariable->id}}"></td>
                            <td>{{$optionalvariable->optional_variable}}</td>
                            <td>{{$optionalvariable->used_for}}</td>
                            <td>{{$optionalvariable->notes}}</td>
                            <td>{{$optionalvariable->used}}</td>
                            <td>{{$optionalvariable->empty_value}}</td>
                            <td>
                              <select class="form-control js-optVrblGroupId" data-variableid="{{$optionalvariable->id}}" name="optVrblGroupId">
                                <option value="">Non-Group</option>
                                @foreach($optionalVariableGroups as $group)
                                  <option value="{{$group->id}}" {{($group->id == $optionalvariable->optional_variable_group_id) ? 'selected' : ''}}>{{$group->group_name}}</option>
                                @endforeach
                              </select>
                            </td>
                            @php $mapped = collect($optionalVariables)->where('mapped_id', $optionalvariable->mapped_id)->where('mapped', 1)->all(); @endphp
                            <td @php if($optionalvariable->mapped){
                                    $mappedVariables = '';
                                    foreach($mapped as $map){
                                      $mappedVariables .= $map->optional_variable.' ';
                                    }
                                    echo 'class="cursor text-primary js-unMap" data-variableid="'.$optionalvariable->id.'" data-toggle="tooltip" title="'.$mappedVariables.'"';
                                  }
                                @endphp>
                                {{ ($optionalvariable->mapped == 1) ? $optionalvariable->mapped_id : ''}}
                                @php $mapped = []; @endphp
                            </td>
                            <td>{{$optionalvariable->mandatory}}</td>
                            <td>
                                <a href="{{url('optionalvariables/'.$optionalvariable->id.'/edit')}}">
                                    <button class="btn btn-primary">Edit</button>
                                </a>
                                <a href="{{url('optionalvariables/'.$optionalvariable->id.'/delete')}}">
                                    <button class="btn btn-danger">Delete</button>
                                </a>
                            </td>
                          </tr>
                        @endforeach
                      </tbody>
                    </table>

                    <!-- <div class="col-lg-12"> -->
                      <button type="submit" class="btn btn-primary pull-right mt-3 js-map" style="position: absolute; top: -5px; right: 250px;">Map</button>
                    <!-- </div> -->
                  </form>
                  </div>
                </div>
            </div>
        <!-- </div> -->
    <!-- </div> -->
<!-- </div> -->

    <div class="col-lg-12">
        <div class="has-bg button_holder">
            <div class="left-side-button">

                <button class="btn btn-primary cursor" data-toggle="tooltip" title="Optional Variables sync with optional variables in loans" onclick="syncOptionalVariable()">
                  Sync Optional Variables
                    <!-- <i class="fas fa-sync-alt"></i> -->
                </button>

                <button class="btn btn-primary cursor" data-toggle="tooltip" title="Sync Optional Variables for Movnc tags.(if mandatory optional variable value is missing, Movnc tag will be added to the loan)" onclick="syncOptionalVariableMovncTags()">
                  Sync for Movnc Tags
                    <!-- <i class="fas fa-sync-alt"></i> -->
                </button>

                <a href="{{url('optionalvariables-movnc')}}">
                  <button class="btn btn-primary cursor">
                    Movnc
                  </button>
                </a>

                <button type="button" class="btn btn-primary ml-1" data-toggle="modal" data-target="#mapOptionalVariablesModal">
                  Map Optional Variables
                </button>

                <button type="button" class="btn btn-primary ml-1" data-toggle="modal" data-target="#resetOptionalVariablesModal">
                  Reset Optional Variables
                </button>

                <button type="button" class="btn btn-primary ml-1 cursor" data-toggle="modal" data-target="#importOptionalVariablesModal">
                  Import Optional Variables
                </button>

            </div>
        </div>
    </div>

    <div class="modal" id="mapOptionalVariablesModal" data-backdrop="static">
      <div class="modal-dialog" style="max-width: 1000px">
        <div class="modal-content">
          <div class="modal-body custom_modal">
            <div class="row">
              <div class="col-lg-1">
                <div class="left-side">
                </div>
              </div>
              <div class="col-lg-11">
                
                <form action="{{ url('optionalvariables/map' )}}" method="post" enctype="multipart/form-data">
                  <h4 class="modal-title">Map Optional Variables<button type="button" class="close" data-dismiss="modal">&times;</button></h4>
                
                  <div class="row">
                    @csrf()
                    <div class="col-8">
                      <div class="form-group">
                        <select class="form-control js-multi-search"  name="optionalVariableId[]" multiple>
                            @foreach($optionalVariables as $key => $variable)
                              <option value="{{$variable->id}}">{{$variable->optional_variable}}</option>
                            @endforeach
                        </select>
                      </div>
                    </div>

                    <div class="col-lg-12">
                      <button type="submit" class="btn pull-right btn-primary custom-padding mt-3">Map</button>
                    </div>
                  </div>
                </form>

              </div>
            </div>
          </div>
        </div>
      </div>
    </div>

    <div class="modal" id="resetOptionalVariablesModal" data-backdrop="static">
      <div class="modal-dialog" style="max-width: 1000px">
        <div class="modal-content">
          <div class="modal-body custom_modal">
            <div class="row">
              <div class="col-lg-1">
                <div class="left-side">
                </div>
              </div>
              <div class="col-lg-11">
                
                <form action="{{ url('optionalvariables/un-map' )}}" method="post" enctype="multipart/form-data">
                  <h4 class="modal-title">Reset Optional Variables<button type="button" class="close" data-dismiss="modal">&times;</button></h4>
                
                  <div class="row">
                    @csrf()
                    <div class="col-8">
                      <div class="form-group">
                        <select class="form-control js-multi-search"  name="optionalVariableId[]" multiple>
                            @foreach($optionalVariables as $key => $variable)
                              <option value="{{$variable->id}}">{{$variable->optional_variable}}</option>
                            @endforeach
                        </select>
                      </div>
                    </div>

                    <div class="col-lg-12">
                      <button type="submit" class="btn pull-right btn-primary custom-padding mt-3">Reset</button>
                    </div>
                  </div>
                </form>

              </div>
            </div>
          </div>
        </div>
      </div>
    </div>

    <div class="modal" id="importOptionalVariablesModal" data-backdrop="static">
      <div class="modal-dialog" style="max-width: 1000px">
        <div class="modal-content">
          <div class="modal-body custom_modal">
            <div class="row">
              <div class="col-lg-1">
                <div class="left-side">
                </div>
              </div>
              <div class="col-lg-11">
                
                <form action="{{ url('optionalvariables/import' )}}" method="post" enctype="multipart/form-data" class="js-importForm">
                  <h4 class="modal-title">Import Optional Variables<button type="button" class="close" data-dismiss="modal">&times;</button></h4>
                  <div class="row">
                    @csrf()
                    <div class="col-6">
                      <div class="form-group">
                        <label for="file">Choose csv/excel file:</label>
                        <input type="file" class="form-control" name="file">
                      </div>
                    </div>
                    <div class="col-lg-12">
                      <p class="text-danger js-importError"></p>
                    </div>
                    <div class="col-6 offset-5">
                      <div class="loader hide"></div>
                    </div> 
                    <div class="col-lg-12">
                      <button type="submit" class="btn pull-right btn-primary custom-padding mt-3 js-importBtn">Import</button>
                    </div>
                  </div>
                </form>

              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
@endsection

@section('pages-js')

<script>
  $(document).ready(function(){
      $(document).on('click', '.target-description', function() {
        // var key = $(this).data('key');
        $('.show-description').toggle("slide");
        $(this).find(".svg-inline--fa").toggleClass("fa-chevron-up fa-chevron-down" );
      });
  });

  $('.js-optVrblGroupId').on('change', function(){
    console.log($(this).val());
    var optVrblGroupId = $(this).val();
    var id = $(this).data('variableid');
    $.ajax({
      url : "{{url('optionalvariables')}}/"+id+'/update',
      type: 'POST',
      data: {optVrblGroupId: optVrblGroupId},
      // processData: false,
      // contentType: false,
      success: function(result){
        console.log(result);
      }, 
      error: function(error){
        console.log(error);
      }
    });
  });

  $('.js-unMap').on('click', function(){
    // $('.delete-loan').on('click', function(e){
      // e.preventDefault();
      var link = "{{url('optionalvariables/un-map')}}/"+$(this).data('variableid');
      console.log(link);
      Swal.fire({
        title: 'Are you sure?',
        text: 'Optional variable will be un-mapped.',
        type: 'warning',
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        confirmButtonText: "Yes, Un-map"
      }).then((result) => {
        if (result.value) {
          window.location.href = link;
          Swal.fire(
            'Success!',
            'Your un-map optional variable request is processing.!',
            'success'
          );
        }
      });
    console.log($(this).data('variableid'));
  });

  // $('.js-map').on('click', function(){
    // console.log('clicked');
    // var optionalVariableId = [];
    // $('input:checkbox[name=optionalVariableId]:checked').each(function(){
    //   optionalVariableId.push($(this).val());
    // });
    // $('.js-mapForm').submit(function(e){
    //   e.preventDefault();
    //   var formData = new FormData($(this));
    //   console.log(formData);
    // var formData = new FormData($('.js-mapForm').serializeArray());
      // console.log(formData);
    // $.ajax({
    //   url : "{{url('optionalvariables/map')}}",
    //   type: 'POST',
    //   processData: false,
    //   contentType: false,
    //   // data : formData,

    //   // beforeSend: function(){
    //   //   $('.js-sync-response').html('Loading...');
    //   // },
    //   success: function(result){
    //     console.log(result);
    //     // $('.js-sync-response').html('<p class="text-success">Optional Variables synced successfully. Newely added variables: ' + result.added + '</p>');
    //   },
    //   error: function(error){
    //     console.log(error);
    //     // $('.js-sync-response').html('<p class="text-danger">Internal server error in syncing optional variables.</p>');
    //   }
    // });
    // console.log(optionalVariableId);
    // });
  // $('.js-mapForm').on('submit', function(e){
  //   e.preventDefault();
  //   var formData = new FormData($(this));
  //   console.log(formData);

  // });

    function syncOptionalVariable(){
        $.ajax({
          url : "{{url('optionalvariables-sync')}}",
          type: 'GET',
          processData: false,
          contentType: false,
          beforeSend: function(){
            $('.js-sync-response').html('Loading...');
          },
          success: function(result){
            console.log(result);
            $('.js-sync-response').html('<p class="text-success">Optional Variables synced successfully. Newely added variables: ' + result.added + '</p>');
          },
          error: function(error){
            $('.js-sync-response').html('<p class="text-danger">Internal server error in syncing optional variables.</p>');
          }
        });
    }

    function syncOptionalVariableMovncTags(){
        $.ajax({
          url : "{{url('optionalvariables-sync?movncTags=true')}}",
          type: 'GET',
          processData: false,
          contentType: false,
          beforeSend: function(){
            $('.js-sync-response').html('Loading...');
          },
          success: function(result){
            console.log(result);
            $('.js-sync-response').html('<p class="text-success">Optional variable "Movnc tags" synced successfully.</p>');
          },
          error: function(error){
            $('.js-sync-response').html('<p class="text-danger">Internal server error in syncing optional variables "Movnc tags".</p>');
          }
        });
    }

  $('.js-importForm').on("submit", function(e){
    e.preventDefault();
    $('.js-importBtn').attr('disabled', 'disabled');
    var formData = new FormData(this);
    // console.log(formData);
    $.ajax({
      url : $(this).attr('action'),
      type: $(this).attr('method'),
      beforeSend: function(){
        $('.loader').removeClass('hide');
        $('.js-importError').html('<span class="text-info">Please wait while your file is being processed.</span>');
      },
      complete: function(){$('.loader').addClass('hide');},
      data : formData,
      processData: false,
      contentType: false,
      success: function(result){
        console.log(result);
        if(result.code == 401)
            $('.js-importError').html('<div class="row js-msgs"><p class="text-danger mb-0">'+result.message+'</p></div>');
        else{
          $('.js-importErrorsAll').html('<div class="row js-msgs"><p class="text-success mb-0">Optional variables uploaded successfully.</p></div>');
          // var success = errors = ignored = '';
          // if(result.success.length){
            // $.each(result.errors, function(f, files){
            //   success += '<div class="col-3"><h5>'+files.file+'</h5>';
            //   $.each(files.message, function(m, msg){
            //     success += '<p class="text-success mb-0">'+msg+'</p>';
            //   });
            //   $('.js-msgs').append(success);
            //   success = '';
            // });
          // }
          // scroll window to the top
          $('#importOptionalVariablesModal').modal('toggle');
          // $('.js-importError').html('');
        // }
        // else
        //   $('.js-importError').html('Internal server error. Please refresh the page and try fixing loan data.');
          window.scrollTo(0,0);
        }
        $('.js-importBtn').attr('disabled', false);
        // queueCount();
      },
      error: function(errors){
        $('.js-importError').html('Internal server error.');
        $('.js-importBtn').attr('disabled', false);
      },
    });
  });

    agGrid.LicenseManager.setLicenseKey("{{env('AG_GRID_LICENCE_KEY')}}");
    // specify the columns
    var columnDefs = [
        {field: "optional_variable"},
        {field: "used_for"},
        {field: "notes"},
        {field: "mandatory"},
        {field: "mapped_id"},
        {field: "used"},
        {field: "empty_value"},
    ];

    var gridOptions = {
        defaultColDef: {
            cellClassRules: {
                lightGreyBackground: function(params) {
                    return params.rowIndex % 2 == 0;
                }
            },
            sortable: true,
            filter: true
        },
        columnDefs: columnDefs,
        rowSelection: 'multiple',

        excelStyles: [
            {
                id: 'greenBackground',
                interior: {
                    color: '#b5e6b5',
                    pattern: 'Solid'
                }
            },
            {
                id: 'redFont',
                font: {
                    fontName: 'Calibri Light',
                    underline: 'Single',
                    italic: true,
                    color: '#ff0000'
                }
            },
            {
                id: 'lightGreyBackground',
                interior: {
                    color: '#e8ebec',
                    pattern: 'Solid'
                },
                font: {
                    fontName: 'Calibri Light',
                    // color: '#ffffff'
                }
            },
            {
                id: 'boldBorders',
                borders: {
                    borderBottom: {
                        color: '#000000',
                        lineStyle: 'Continuous',
                        weight: 3
                    },
                    borderLeft: {
                        color: '#000000',
                        lineStyle: 'Continuous',
                        weight: 3
                    },
                    borderRight: {
                        color: '#000000',
                        lineStyle: 'Continuous',
                        weight: 3
                    },
                    borderTop: {
                        color: '#000000',
                        lineStyle: 'Continuous',
                        weight: 3
                    }
                }
            },
            {
                id: 'header',
                interior: {
                    color: '#CCCCCC',
                    pattern: 'Solid'
                },
                borders: {
                    borderBottom: {
                        color: '#5687f5',
                        lineStyle: 'Continuous',
                        weight: 1
                    },
                    borderLeft: {
                        color: '#5687f5',
                        lineStyle: 'Continuous',
                        weight: 1
                    },
                    borderRight: {
                        color: '#5687f5',
                        lineStyle: 'Continuous',
                        weight: 1
                    },
                    borderTop: {
                        color: '#5687f5',
                        lineStyle: 'Continuous',
                        weight: 1
                    }
                }
            },
            {
                id: 'dateFormat',
                dataType: 'dateTime',
                numberFormat: {
                    format: 'mm/dd/yyyy;@'
                }
            },
            {
                id: 'twoDecimalPlaces',
                numberFormat: {
                    format: '#,##0.00'
                }
            },
            {
                id: 'textFormat',
                dataType: 'string'
            },
            {
                id: 'bigHeader',
                font: {
                    size: 25
                }
            }
        ]
    };

    function getBooleanValue(cssSelector) {
        return document.querySelector(cssSelector).checked === true;
    }

    function getTextValue(cssSelector) {
        return document.querySelector(cssSelector).value;
    }

    function getNumericValue(cssSelector) {
        var value = parseFloat(getTextValue(cssSelector));
        if (isNaN(value)) {
            var message = "Invalid number entered in " + cssSelector + " field";
            alert(message);
            throw new Error(message);
        }
        return value;
    }

    function myColumnWidthCallback(params) {
        var originalWidth = params.column.getActualWidth();
        if (params.index < 7) {
            return originalWidth;
        }
        return 30;
    }

    function onBtExport() {
        var columnWidth = getBooleanValue('#columnWidth') ? getTextValue('#columnWidthValue') : undefined;
        var params = {
            columnWidth: columnWidth === 'myColumnWidthCallback' ? myColumnWidthCallback : parseFloat(columnWidth),
            sheetName: getBooleanValue('#sheetName') && getTextValue('#sheetNameValue'),
            exportMode: getBooleanValue('#exportModeXml') ? "xml" : undefined,
            suppressTextAsCDATA: getBooleanValue('#suppressTextAsCDATA'),
            rowHeight: getBooleanValue('#rowHeight') ? getNumericValue('#rowHeightValue') : undefined,
            headerRowHeight: getBooleanValue('#headerRowHeight') ? getNumericValue('#headerRowHeightValue') : undefined,
        };

        gridOptions.api.exportDataAsExcel(params);
    }

    // setup the grid after the page has finished loading
    document.addEventListener('DOMContentLoaded', function() {
        var gridDiv = document.querySelector('#myGrid');
        new agGrid.Grid(gridDiv, gridOptions);

        $.ajax({
            url: "{{url('optionalvariables?json=').'true'}}",
            type: 'get',
            async: false,
            success: function(result){
            // agGrid.simpleHttpRequest({url: "{{url('datatype?limit=0')}}"}).then(function(data) {
                console.log(result.data);
                // console.log(gridOptions);
                gridOptions.api.setRowData(result.data);
            }, error: function(errors){
                console.log('error');
            }
        });
    });

</script>

@endsection
