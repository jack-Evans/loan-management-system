<?php

/** @var Route $router */
$router->get('optionalvariables/un-map/{optionalVariableId}', [
    'as' => 'web_optionalvariable_map',
    'uses'  => 'Controller@unMapOptionalVariable',
    'middleware' => [
      'auth:web',
    ],
]);
