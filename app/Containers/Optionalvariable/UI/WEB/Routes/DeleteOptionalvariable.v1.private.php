<?php

/** @var Route $router */
$router->get('optionalvariables/{id}/delete', [
    'as' => 'web_optionalvariable_delete',
    'uses'  => 'Controller@delete',
    'middleware' => [
      'auth:web',
    ],
]);
