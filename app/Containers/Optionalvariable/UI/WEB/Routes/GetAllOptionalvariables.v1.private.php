<?php

/** @var Route $router */
$router->get('optionalvariables', [
    'as' => 'web_optionalvariable_index',
    'uses'  => 'Controller@index',
    'middleware' => [
      'auth:web',
    ],
]);
