<?php

/** @var Route $router */
$router->get('optionalvariables-sync', [
    'as' => 'web_optionalvariable_sync',
    'uses'  => 'Controller@syncOptionalVariable',
    'middleware' => [
      'auth:web',
    ],
]);
