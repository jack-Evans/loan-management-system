<?php

/** @var Route $router */
$router->post('optionalvariables/map', [
    'as' => 'web_optionalvariable_map',
    'uses'  => 'Controller@mapOptionalVariable',
    'middleware' => [
      'auth:web',
    ],
]);
