<?php

/** @var Route $router */
$router->get('optionalvariables/create', [
    'as' => 'web_optionalvariable_create',
    'uses'  => 'Controller@create',
    'middleware' => [
      'auth:web',
    ],
]);
