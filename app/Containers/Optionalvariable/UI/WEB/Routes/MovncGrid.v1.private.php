<?php

/** @var Route $router */
$router->get('optionalvariables-movnc', [
    'as' => 'web_optionalvariable_movnc',
    'uses'  => 'Controller@movncOptionalVariableGrid',
    'middleware' => [
      'auth:web',
    ],
]);
