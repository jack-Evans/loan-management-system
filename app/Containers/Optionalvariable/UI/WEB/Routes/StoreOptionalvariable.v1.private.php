<?php

/** @var Route $router */
$router->post('optionalvariables/store', [
    'as' => 'web_optionalvariable_store',
    'uses'  => 'Controller@store',
    'middleware' => [
      'auth:web',
    ],
]);
