<?php

/** @var Route $router */
$router->post('optionalvariables/import', [
    'as' => 'web_optionalvariable_import',
    'uses'  => 'Controller@importOptionalVariableWeb',
    'middleware' => [
      'auth:web',
    ],
]);
