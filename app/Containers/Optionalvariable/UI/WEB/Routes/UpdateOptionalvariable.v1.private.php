<?php

/** @var Route $router */
$router->post('optionalvariables/{id}/update', [
    'as' => 'web_optionalvariable_update',
    'uses'  => 'Controller@update',
    'middleware' => [
      'auth:web',
    ],
]);
