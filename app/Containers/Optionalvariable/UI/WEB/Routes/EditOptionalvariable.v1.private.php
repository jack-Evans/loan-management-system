<?php

/** @var Route $router */
$router->get('optionalvariables/{id}/edit', [
    'as' => 'web_optionalvariable_edit',
    'uses'  => 'Controller@edit',
    'middleware' => [
      'auth:web',
    ],
]);
