<?php

/** @var Route $router */
$router->get('optionalvariables/{id}', [
    'as' => 'web_optionalvariable_show',
    'uses'  => 'Controller@show',
    'middleware' => [
      'auth:web',
    ],
]);
