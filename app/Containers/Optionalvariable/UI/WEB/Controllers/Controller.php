<?php

namespace App\Containers\Optionalvariable\UI\WEB\Controllers;

use App\Containers\Optionalvariable\UI\WEB\Requests\CreateOptionalvariableRequest;
use App\Containers\Optionalvariable\UI\WEB\Requests\DeleteOptionalvariableRequest;
use App\Containers\Optionalvariable\UI\WEB\Requests\GetAllOptionalvariablesRequest;
use App\Containers\Optionalvariable\UI\WEB\Requests\FindOptionalvariableByIdRequest;
use App\Containers\Optionalvariable\UI\WEB\Requests\UpdateOptionalvariableRequest;
use App\Containers\Optionalvariable\UI\WEB\Requests\StoreOptionalvariableRequest;
use App\Containers\Optionalvariable\UI\WEB\Requests\EditOptionalvariableRequest;
use App\Ship\Parents\Controllers\WebController;
use Apiato\Core\Foundation\Facades\Apiato;

/**
 * Class Controller
 *
 * @package App\Containers\Optionalvariable\UI\WEB\Controllers
 */
class Controller extends WebController
{

    /**
     * The assigned API PATH for this Controller
     *
     * @var string
     */
    
    protected $apiPath;
    
    public function __construct(){
        $this->apiPath = config('token-container.WEB_API_URL');
    }

    /**
     * Show all entities
     *
     * @param GetAllOptionalvariablesRequest $request
     */
    public function index(GetAllOptionalvariablesRequest $request)
    {
        // optionalvariables
        $url = $this->apiPath.'optionalvariables';
        try {
            $response = get($url, ['limit' => 0]);
            $optionalVariables = $response->data;
        } catch (\Exception $e) {
            return exception($e);
        }
        if($request->json)
            return response()->json(['data' => $optionalVariables]);
         
        $optionalVariableGroups = \App\Containers\Grid\Models\OptionalVariableGroup::all();

        return view('optionalvariable::optionalvariable', compact('optionalVariables', 'optionalVariableGroups'));
    }

    /**
     * Show one entity
     *
     * @param FindOptionalvariableByIdRequest $request
     */
    public function show(FindOptionalvariableByIdRequest $request)
    {
        $optionalvariable = Apiato::call('Optionalvariable@FindOptionalvariableByIdAction', [$request]);

        // ..
    }

    /**
     * Create entity (show UI)
     *
     * @param CreateOptionalvariableRequest $request
     */
    public function create(CreateOptionalvariableRequest $request)
    {
        return view('optionalvariable::create-update-optionalvariable');
    }

    /**
     * Add a new entity
     *
     * @param StoreOptionalvariableRequest $request
     */
    public function store(StoreOptionalvariableRequest $request)
    {
        $url = $this->apiPath.'optionalvariables';
        $request = [
            // 'id'                => $request->id,
            'optionalVariable' => $request->optionalVariable,
            'mandatory'        => $request->mandatory,
            'notes'        => $request->notes,
            'usedFor'        => $request->usedFor,
            'mandatory'        => $request->mandatory,
        ];
        // dd($request);
        try {
            $response = post($url, $request);
        } catch (\Exception $e) {
            return exception($e);
        }
        return redirect('optionalvariables');
    }

    /**
     * Edit entity (show UI)
     *
     * @param EditOptionalvariableRequest $request
     */
    public function edit(EditOptionalvariableRequest $request)
    {
        $url = $this->apiPath.'optionalvariables/'.$request->id;
        try {
            $response = get($url);
            $optionalVariable = $response->data;
        } catch (\Exception $e) {
            return exception($e);
        }
// dd($optionalVariable);
        return view('optionalvariable::create-update-optionalvariable', compact('optionalVariable'));
    }

    /**
     * Update a given entity
     *
     * @param UpdateOptionalvariableRequest $request
     */
    public function update(UpdateOptionalvariableRequest $request)
    {
        $url = $this->apiPath.'optionalvariables/'.$request->id;
        $data = [
            'optionalVariable' => $request->optionalVariable,
            // 'mappedId'        => $request->mappedId,
            'mandatory'        => $request->mandatory,
            'notes'        => $request->notes,
            'usedFor'        => $request->usedFor,
            'optVrblGroupId'        => $request->optVrblGroupId,
        ];
        try {
            $response = patch($url, $data);
        } catch (\Exception $e) {
            return exception($e);
        }
        
        return redirect('optionalvariables');
    }

    /**
     * Delete a given entity
     *
     * @param DeleteOptionalvariableRequest $request
     */
    public function delete(DeleteOptionalvariableRequest $request)
    {
        $url = $this->apiPath.'optionalvariables/'.$request->id;
        
        try {
            $response = delete($url);
        } catch (\Exception $e) {
            return exception($e);
        }
        
        return redirect('optionalvariables');
    }

    /**
     * Map optional variables
     *
     * @param DeleteOptionalvariableRequest $request
     */
    public function mapOptionalVariable(DeleteOptionalvariableRequest $request)
    {
        $url = $this->apiPath.'optionalvariables/map';

        $data = [
            'optionalVariableId' => $request->optionalVariableId,
        ];
        // dd($data);
        try {
            $response = post($url, $data);
        } catch (\Exception $e) {
            return exception($e);
        }
        
        return redirect('optionalvariables');
    }

    /**
     * Map optional variables
     *
     * @param DeleteOptionalvariableRequest $request
     */
    public function unMapOptionalVariable(DeleteOptionalvariableRequest $request)
    {
        $url = $this->apiPath.'optionalvariables/un-map';
        $data = [
            'optionalVariableId' => array($request->optionalVariableId),
        ];
        
        // dd($data,post($url, $data));
        try {
            $response = post($url, $data);
        } catch (\Exception $e) {
            return exception($e);
        }
        
        return redirect('optionalvariables');
    }

    /**
     * Sync optional variables
     *
     * @param DeleteOptionalvariableRequest $request
     */
    public function syncOptionalVariable(DeleteOptionalvariableRequest $request)
    {
        if($request->movncTags)
            $url = $this->apiPath.'optionalvariables/movnc';
        else
            $url = $this->apiPath.'optionalvariables-sync';

        try {
            $response = get($url);
        } catch (\Exception $e) {
            return exception($e);
        }
        
        return response()->json($response);
    }

    /**
     * Sync optional variables
     *
     * @param DeleteOptionalvariableRequest $request
     */
    public function movncOptionalVariableGrid(DeleteOptionalvariableRequest $request)
    {
        $url = $this->apiPath.'optionalvariables/movnc';

        try {
            $movncOptionalVariables = get($url)->data;
        } catch (\Exception $e) {
            return exception($e);
        }
        if($request->json)
            return response()->json($movncOptionalVariables);

        return view('optionalvariable::movnc-optional-variable', compact('movncOptionalVariables'));
    }

    /**
     * @param ImportOptionalVariableRequest $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function importOptionalVariableWeb(DeleteOptionalvariableRequest $request)
    {
        $url = $this->apiPath.'optionalvariables/import';

        if(empty($_FILES['file']['name']))
            return response()->json(['code' => 401, 'message' => 'Please Upload A File!']);

        try{
            $response = postWithFile($url, $fileName = "file", []);
            return response()->json($response);
        } catch (\Exception $e) {
            return response()->json(['code' => 400, 'message' => $e->getMessage()]);
        }

        return response()->json(['code' => 400, 'message' => 'Internal server error.']);
    }
}
