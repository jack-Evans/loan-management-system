<?php

namespace App\Containers\Optionalvariable\Tasks;

use App\Containers\Optionalvariable\Data\Repositories\OptionalvariableRepository;
use App\Ship\Parents\Tasks\Task;

class GetAllOptionalvariablesTask extends Task
{

    protected $repository;

    public function __construct(OptionalvariableRepository $repository)
    {
        $this->repository = $repository;
    }

    public function run()
    {
        return $this->repository->paginate();
    }
}
