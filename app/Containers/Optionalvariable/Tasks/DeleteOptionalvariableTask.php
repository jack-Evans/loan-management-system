<?php

namespace App\Containers\Optionalvariable\Tasks;

use App\Containers\Optionalvariable\Data\Repositories\OptionalvariableRepository;
use App\Ship\Exceptions\DeleteResourceFailedException;
use App\Ship\Parents\Tasks\Task;
use App\Containers\Grid\Data\Repositories\GridOptionalVariableRepository;
use Exception;

class DeleteOptionalvariableTask extends Task
{

    protected $repository;
    protected $gridOptionalVariableRepository;

    public function __construct(OptionalvariableRepository $repository, GridOptionalVariableRepository $gridOptionalVariableRepository)
    {
        $this->repository = $repository;
        $this->gridOptionalVariableRepository = $gridOptionalVariableRepository;
    }

    public function run($id)
    {
        try {
            \DB::beginTransaction();
            $deletedEntery = $this->repository->delete($id);
            $this->gridOptionalVariableRepository->deleteWhere(['optional_variable_id' => $id]);
            \DB::commit();
            return $deletedEntery;
        }
        catch (Exception $exception) {
            throw new DeleteResourceFailedException();
        }
    }
}
