<?php

namespace App\Containers\Optionalvariable\Tasks;

use App\Containers\Optionalvariable\Data\Repositories\OptionalvariableRepository;
use App\Ship\Exceptions\NotFoundException;
use App\Ship\Parents\Tasks\Task;
use Exception;

class FindOptionalvariableByIdTask extends Task
{

    protected $repository;

    public function __construct(OptionalvariableRepository $repository)
    {
        $this->repository = $repository;
    }

    public function run($id)
    {
        try {
            return $this->repository->find($id);
        }
        catch (Exception $exception) {
            throw new NotFoundException();
        }
    }
}
