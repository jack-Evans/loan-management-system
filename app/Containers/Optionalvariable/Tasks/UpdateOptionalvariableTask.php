<?php

namespace App\Containers\Optionalvariable\Tasks;

use App\Containers\Optionalvariable\Data\Repositories\OptionalvariableRepository;
use App\Ship\Exceptions\UpdateResourceFailedException;
use App\Ship\Parents\Tasks\Task;
use Exception;

class UpdateOptionalvariableTask extends Task
{

    protected $repository;

    public function __construct(OptionalvariableRepository $repository)
    {
        $this->repository = $repository;
    }

    public function run($id, array $data)
    {
        try {
            return $this->repository->update($data, $id);
        }
        catch (Exception $exception) {
            throw new UpdateResourceFailedException();
        }
    }
}
