<?php

namespace App\Containers\Optionalvariable\Tasks;

use App\Containers\Optionalvariable\Data\Repositories\OptionalvariableRepository;
use App\Ship\Exceptions\CreateResourceFailedException;
use App\Ship\Parents\Tasks\Task;
use Exception;

class CreateOptionalvariableTask extends Task
{

    protected $repository;

    public function __construct(OptionalvariableRepository $repository)
    {
        $this->repository = $repository;
    }

    public function run(array $data)
    {
        try {
            return $this->repository->create($data);
        }
        catch (Exception $exception) {
            throw new CreateResourceFailedException();
        }
    }
}
