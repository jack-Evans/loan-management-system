<?php

namespace App\Containers\Optionalvariable\Models;

use App\Ship\Parents\Models\Model;

class Optionalvariable extends Model
{
    protected $fillable = [
        'optional_variable',
        'mapped_id',
        'reset_map_id',
        'mapped',
        'used_for',
        'notes',
        'mandatory',
        'optional_variable_group_id',
    ];

    protected $attributes = [

    ];

    protected $hidden = [

    ];

    protected $casts = [

    ];

    protected $dates = [
        'created_at',
        'updated_at',
    ];

    /**
     * A resource key to be used by the the JSON API Serializer responses.
     */
    protected $resourceKey = 'optionalvariables';

    public function group()
    {
        return $this->belongsTo(\App\Containers\Grid\Models\OptionalVariableGroup::class, 'optional_variable_group_id');
    }
}
