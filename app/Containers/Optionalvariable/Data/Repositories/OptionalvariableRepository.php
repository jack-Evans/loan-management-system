<?php

namespace App\Containers\Optionalvariable\Data\Repositories;

use App\Ship\Parents\Repositories\Repository;

/**
 * Class OptionalvariableRepository
 */
class OptionalvariableRepository extends Repository
{

    /**
     * @var array
     */
    protected $fieldSearchable = [
        'id' => '=',
        // ...
    ];

}
