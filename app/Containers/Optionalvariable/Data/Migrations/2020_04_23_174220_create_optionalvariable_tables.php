<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateOptionalvariableTables extends Migration
{

    /**
     * Run the migrations.
     */
    public function up()
    {
        Schema::create('optionalvariables', function (Blueprint $table) {

            $table->increments('id');
            $table->string('optional_variable');
            $table->unsignedInteger('mapped_id')->nullable();
            $table->unsignedInteger('reset_map_id')->nullable();
            $table->unsignedInteger('mapped')->default(0);
            $table->timestamps();
            //$table->softDeletes();

        });
    }

    /**
     * Reverse the migrations.
     */
    public function down()
    {
        Schema::dropIfExists('optionalvariables');
    }
}
