<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class AddOptionalvariableGroupId extends Migration
{

    /**
     * Run the migrations.
     */
    public function up()
    {
        Schema::table('optionalvariables', function (Blueprint $table) {
            $table->tinyInteger('optional_variable_group_id', false)->after('mandatory')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down()
    {
        Schema::table('optionalvariables', function($table) {
            $table->dropColumn('optional_variable_group_id');
        });
    }
}
