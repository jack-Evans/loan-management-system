<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class AddMandatoryIntoOptionalvariables extends Migration
{

    /**
     * Run the migrations.
     */
    public function up()
    {
        Schema::table('optionalvariables', function (Blueprint $table) {

           $table->string('used_for')->after('mapped')->nullable();
           $table->string('notes')->after('used_for')->nullable();
           $table->boolean('mandatory')->after('notes');

        });
    }

    /**
     * Reverse the migrations.
     */
    public function down()
    {
        Schema::table('optionalvariables', function($table) {
            $table->dropColumn('used_for');
            $table->dropColumn('notes');
            $table->dropColumn('mandatory');
        });
    }
}
