<?php

namespace App\Containers\Optionalvariable\Actions;

use App\Ship\Parents\Actions\Action;
use App\Ship\Parents\Requests\Request;
use Apiato\Core\Foundation\Facades\Apiato;
use App\Containers\Optionalvariable\Models\Optionalvariable;

class MapOptionalVariableAction extends Action
{
    public function run(Request $request)
    {
        $optionalVariableIds = [];
        foreach ($request->optionalVariableId as $key => $id) {
            $optionalVariableIds[] = \Hashids::decode($id)[0];
        }

        // $ids = implode(',', $optionalVariableIds);
        $previousMap = Optionalvariable::whereIn('id', $optionalVariableIds)->where('mapped', 1)->first();
        // dd($previousMap);
        if($previousMap)
            $lastMappedId = $previousMap->mapped_id;
        else
            $lastMappedId = Optionalvariable::whereIn('id', $optionalVariableIds)->min('mapped_id');

        $optionalvariable = Optionalvariable::whereIn('id', $optionalVariableIds)->update(['mapped_id' => $lastMappedId, 'mapped' => 1]);
            // dd($optionalvariable);
        return $optionalvariable;
    }
}
