<?php

namespace App\Containers\Optionalvariable\Actions;

use App\Ship\Parents\Actions\Action;
use App\Ship\Parents\Requests\Request;
use Apiato\Core\Foundation\Facades\Apiato;
use App\Containers\Optionalvariable\Models\Optionalvariable;
use App\Containers\Option\Models\Option;

class SyncOptionalvariableAction extends Action
{
    public function run(Request $request)
    {
        $optionalVariables = Optionalvariable::pluck('optional_variable');
        $options = Option::select('options.name')->whereNotIn('name', $optionalVariables)->groupBy('options.name')->get();

        if(count($options)){
            foreach ($options as $key => $option) {
                $request->optionalVariable = $option->name;
                Apiato::call('Optionalvariable@CreateOptionalvariableAction', [$request]);
                $request->mappedId = null;
                $request->optionalVariable = null;
            }
        }
            
        return ['added' => count($options)];
    }
}
