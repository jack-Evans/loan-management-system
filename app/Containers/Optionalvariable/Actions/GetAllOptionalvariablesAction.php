<?php

namespace App\Containers\Optionalvariable\Actions;

use App\Ship\Parents\Actions\Action;
use App\Ship\Parents\Requests\Request;
use Apiato\Core\Foundation\Facades\Apiato;
use App\Containers\Option\Models\Option;

class GetAllOptionalvariablesAction extends Action
{
    public function run(Request $request)
    {
        $optionalvariables = Apiato::call('Optionalvariable@GetAllOptionalvariablesTask', [], ['addRequestCriteria']);
        foreach ($optionalvariables as $key => $variable) {
    		$count = Option::where('name', 'LIKE', "{$variable->optional_variable}")->count();
    		$emptyValue = Option::where('name', 'LIKE', "{$variable->optional_variable}")->whereNull('value')->count();
    		$optionalvariables[$key]->used = $count;
    		$optionalvariables[$key]->empty_value = $emptyValue;
        }
        
        return $optionalvariables->sortBy('mapped_id');
    }
}
