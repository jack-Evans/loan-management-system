<?php

namespace App\Containers\Optionalvariable\Actions;

use App\Ship\Parents\Actions\Action;
use App\Ship\Parents\Requests\Request;
use Apiato\Core\Foundation\Facades\Apiato;

class FindOptionalvariableByIdAction extends Action
{
    public function run(Request $request)
    {
        $optionalvariable = Apiato::call('Optionalvariable@FindOptionalvariableByIdTask', [$request->id]);

        return $optionalvariable;
    }
}
