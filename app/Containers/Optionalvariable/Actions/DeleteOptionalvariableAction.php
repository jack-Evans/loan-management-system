<?php

namespace App\Containers\Optionalvariable\Actions;

use App\Ship\Parents\Actions\Action;
use App\Ship\Parents\Requests\Request;
use Apiato\Core\Foundation\Facades\Apiato;

class DeleteOptionalvariableAction extends Action
{
    public function run(Request $request)
    {
        return Apiato::call('Optionalvariable@DeleteOptionalvariableTask', [$request->id]);
    }
}
