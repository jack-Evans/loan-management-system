<?php

namespace App\Containers\Optionalvariable\Actions;

use App\Ship\Parents\Actions\Action;
use App\Ship\Parents\Requests\Request;
use Apiato\Core\Foundation\Facades\Apiato;
use App\Containers\Optionalvariable\Models\Optionalvariable;

class UnMapOptionalvariableAction extends Action
{
    public function run(Request $request)
    {
        foreach ($request->optionalVariableId as $key => $id) {
            $optionalVariable = Optionalvariable::where('id', \Hashids::decode($id)[0])->first();
            $optionalVariable->mapped_id = $optionalVariable->reset_map_id;
            $optionalVariable->mapped = 0;
            $optionalVariable->save();
        }
                    
        return $optionalVariable;
    }
}
