<?php

namespace App\Containers\Optionalvariable\Actions;

use App\Ship\Parents\Actions\Action;
use App\Ship\Parents\Requests\Request;
use Apiato\Core\Foundation\Facades\Apiato;
use App\Containers\Optionalvariable\Models\Optionalvariable;
use App\Containers\Loan\Models\loan;

class AssignMandatoryOptionalVariablesAction extends Action
{
    public function run(Loan $loan, Request $request)
    {
    		$mandatoryVaraibles = Optionalvariable::where('mandatory', 1)->get();

    		foreach ($mandatoryVaraibles as $key => $optVariable) {
    			$request->loanid = $loan->id;
    			$request->name = $optVariable->optional_variable;
    			$request->value = '';
    			$request->createDate = '';

    			Apiato::call('Option@CreateOptionAction', [$request, $addOptionalVariable = false]);

    		}
    }

} 

?>