<?php

namespace App\Containers\Optionalvariable\Actions;

use App\Ship\Parents\Actions\Action;
use App\Ship\Parents\Requests\Request;
use Apiato\Core\Foundation\Facades\Apiato;

class UpdateOptionalvariableAction extends Action
{
    public function run(Request $request)
    {
        $data = [];
        if(!is_null($request->optionalVariable))
	        $data['optional_variable'] = $request->optionalVariable;
	    if(!is_null($request->mappedId))
            $data['mapped_id'] = $request->mappedId;
        if(!is_null($request->resetMappedId))
            $data['reset_map_id'] = $request->resetMappedId;
        if(!is_null($request->mandatory))
            $data['mandatory'] = $request->mandatory;
        if(!is_null($request->notes))
            $data['notes'] = $request->notes;
        if(!is_null($request->usedFor))
            $data['used_for'] = $request->usedFor;
        if(!is_null($request->optVrblGroupId))
            $data['optional_variable_group_id'] = $request->optVrblGroupId;


        $optionalvariable = Apiato::call('Optionalvariable@UpdateOptionalvariableTask', [$request->id, $data]);

        return $optionalvariable;
    }
}
