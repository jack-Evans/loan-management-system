<?php

namespace App\Containers\Optionalvariable\Actions;

use App\Ship\Parents\Actions\Action;
use App\Ship\Parents\Requests\Request;
use Apiato\Core\Foundation\Facades\Apiato;
use App\Containers\Optionalvariable\Models\Optionalvariable;
use App\Containers\Loan\Models\Loan;
use App\Containers\Option\Models\Option;
use App\Containers\Tag\Models\Tag;
use App\Containers\Tag\Models\LoanTag;

class SyncMovncAction extends Action
{
    public function run(Request $request)
    {
        \DB::beginTransaction();

        $movncTag = Tag::where('name', 'like', 'movnc')->first();
        if(is_null($movncTag)){
            $request->name = 'movnc';
            $movncTag = Apiato::call('Tag@CreateTagAction', [$request]);
        }
        $movncTagId = $movncTag->id;
        $optionalVariables = Optionalvariable::where('mandatory', 1)->pluck('optional_variable');
        $loans = Option::whereIn('name', $optionalVariables)->whereNull('value')->groupBy('options.loan_id')->with('loan')->get();
        $tagData = [];
        foreach ($loans as $key => $loan) {
            $loanId = $loan->loan_id;
            $alreadyLoanTag = LoanTag::where('loan_id', $loanId)->where('tag_id', $movncTagId)->first();
            if(is_null($alreadyLoanTag))
                $tagData[] = array(
                    'loan_id' => $loanId,
                    'tag_id' => $movncTagId
                );
        }
        
        LoanTag::insert($tagData);
        $loans = Option::whereIn('name', $optionalVariables)->whereNull('value')->with('loan')->get();
        
        \DB::commit();

        return $loans;
    }
}
