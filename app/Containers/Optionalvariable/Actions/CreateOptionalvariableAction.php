<?php

namespace App\Containers\Optionalvariable\Actions;

use App\Ship\Parents\Actions\Action;
use App\Ship\Parents\Requests\Request;
use Apiato\Core\Foundation\Facades\Apiato;
use App\Containers\Optionalvariable\Models\Optionalvariable;

class CreateOptionalvariableAction extends Action
{
    public function run(Request $request)
    {
        // if(is_null($request->mappedId) || !$request->mappedId)
        // {
        	$lastMappedId = Optionalvariable::max('mapped_id');

            if($lastMappedId)
            	$request->mappedId = ++$lastMappedId;
            else
                $request->mappedId = 1;
        // }

        $data = [
            'optional_variable' => removeExtraSpaces($request->optionalVariable),
            'mapped_id' => $request->mappedId,
            'reset_map_id' => $request->mappedId,
            'notes' => $request->notes,
            'used_for' => $request->usedFor,
            'mandatory' => $request->mandatory,
        ];
        
        $optionalvariable = Apiato::call('Optionalvariable@CreateOptionalvariableTask', [$data]);

        return $optionalvariable;
    }
}
