<?php

namespace App\Containers\Optionalvariable\Actions;

use App\Ship\Parents\Actions\Action;
use App\Ship\Parents\Requests\Request;
use Apiato\Core\Foundation\Facades\Apiato;
use App\Containers\Payment\Exceptions\HTTPPreConditionFailedException;
use App\Containers\Optionalvariable\Models\Optionalvariable;
// excel libraries
use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\Worksheet\Worksheet;
use PhpOffice\PhpSpreadsheet\Reader\Csv;
use PhpOffice\PhpSpreadsheet\Reader\Xlsx;
use Illuminate\Support\Facades\Storage;

class ImportOptionalVariableAction extends Action
{
  public function run(Request $request)
  {
  	$excel = new \App\Classes\Excel();
		$spreadsheet = $excel->initializeExcel($request);
		if($spreadsheet != false){
		    // $isImport = $request->isImport;
		    $file = $request->file('file');
				// $path = Storage::putFileAs(
				//     'public/excel-imports', $request->file('file'), $_FILES['file']['name']
				// );
				$sheetData = $spreadsheet->getSheet(0)->toArray();
				\DB::beginTransaction();
				$request->mandatory = 0;
				foreach($sheetData as $key => $d) {
					if(!isset($d[0]))
						continue;
					$request->optionalVariable = trimedLowerStr($d[0]);
					if(strcmp($request->optionalVariable, 'optional_variable') == 0)
						continue;
					$optVariable = Optionalvariable::where('optional_variable', $request->optionalVariable)->first();
					$request->usedFor = removeExtraSpaces($d[1]);
					$request->notes = removeExtraSpaces($d[2]);
					if(isset($d[3]))
						$request->mandatory = (strcmp(trimedLowerStr($d[3], 'yes')) == 0) ? 1 : 0;

					if(is_null($optVariable))
						$optionalVariable = Apiato::call("Optionalvariable@CreateOptionalvariableAction", [$request]);
					else{
						$request->mappedId = removeExtraSpaces($d[4]);
						$request->id = $optVariable->id;
						$optionalVariable = Apiato::call("Optionalvariable@UpdateOptionalvariableAction", [$request]);
					}
					$request->optionalVariable = $request->usedFor = $request->notes = $request->id = $request->mappedId = $request->resetMappedId = null;
					$request->mandatory = 0;
				}
				\DB::commit();
    } else{
			throw new HTTPPreConditionFailedException("Please upload right file of type csv/xlsx.");
		}
  }

}

?>