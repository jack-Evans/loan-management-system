<?php

namespace App\Containers\Statement\Models;

use App\Ship\Parents\Models\Model;

class Transfer extends Model
{
    protected $fillable = [
        'sender_loan_id',
        'receiver_loan_id',
        'amount',
        'charge_value',
        'sender_tnx_date',
        'receiver_tnx_date',
        'description',
    ];

    protected $attributes = [

    ];

    protected $hidden = [

    ];

    protected $casts = [

    ];

    protected $dates = [
        'sender_tnx_date',
        'receiver_tnx_date',
        'created_at',
        'updated_at',
    ];

    /**
     * A resource key to be used by the the JSON API Serializer responses.
     */
    protected $resourceKey = 'transfers';
}
