<?php

namespace App\Containers\Statement\Models;

use App\Ship\Parents\Models\Model;

class Breakdown extends Model
{
    protected $fillable = [
        'loan_id',
        'refund_id',
        'daily_rate',
        'days',
        'balance',
        'rate',
        'total',
        'start_date',
        'end_date',
    ];

    protected $attributes = [

    ];

    protected $hidden = [

    ];

    protected $casts = [

    ];

    protected $dates = [
        'created_at',
        'updated_at',
    ];

    /**
     * A resource key to be used by the the JSON API Serializer responses.
     */
    protected $resourceKey = 'breakdown';
}
