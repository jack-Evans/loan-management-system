<?php

namespace App\Containers\Statement\Actions;

use App\Ship\Parents\Actions\Action;
use App\Ship\Parents\Requests\Request;
use Apiato\Core\Foundation\Facades\Apiato;

class CreateStatementAction extends Action
{
    public function run(Request $request)
    {
        $data = $request->sanitizeInput([
            // add your request data here
        ]);

        $statement = Apiato::call('Statement@CreateStatementTask', [$data]);

        return $statement;
    }
}
