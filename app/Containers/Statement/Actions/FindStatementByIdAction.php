<?php

namespace App\Containers\Statement\Actions;

use App\Ship\Parents\Actions\Action;
use App\Ship\Parents\Requests\Request;
use Apiato\Core\Foundation\Facades\Apiato;

class FindStatementByIdAction extends Action
{
    public function run(Request $request)
    {
        $statement = Apiato::call('Statement@FindStatementByIdTask', [$request->id]);

        return $statement;
    }
}
