<?php

namespace App\Containers\Statement\Actions;

use App\Ship\Parents\Actions\Action;
use App\Ship\Parents\Requests\Request;
use Apiato\Core\Foundation\Facades\Apiato;
use App\Containers\Payment\Exceptions\HTTPPreConditionFailedException;
// excel libraries
use PhpOffice\PhpSpreadsheet\Spreadsheet;
// use PhpOffice\PhpSpreadsheet\Worksheet\Worksheet;
// use PhpOffice\PhpSpreadsheet\Reader\Csv;
use PhpOffice\PhpSpreadsheet\Writer\Xlsx;
use PhpOffice\PhpSpreadsheet\Style\NumberFormat;
// use Illuminate\Support\Facades\Storage;
use App\Containers\Loan\Models\Loan;
use App\Containers\Option\Models\Option;
use Carbon\Carbon;

class CompareMonthBalancesAction extends Action
{
    public function run(Request $request)
    {
    	$excel = new \App\Classes\Excel();
		$spreadsheet = $excel->initializeExcel($request);
		if($spreadsheet != false){
			// $d => data cell of excel
			$sheetData = $spreadsheet->getSheet(0)->toArray();
	    	$spreadsheet = new Spreadsheet();
			$spreadsheet->getProperties()->setCreator('lms.kuflink')
				->setLastModifiedBy('Admin')
				->setTitle('Exported Loans')
				->setSubject('Exported Loans')
				->setDescription('Exported Loans');
			$sheet = $spreadsheet->getActiveSheet();

			$datesArr = [];
			$row = 1;
			$cell = 'A';
			foreach ($sheetData as $key => $d) {
				try{
					$d = array_map('trim', $d);
					$sheet->setCellValue("A1", "Loan Type");
					$sheet->setCellValue("B1", "Title");
					if(is_null($d[0]) || empty($d[0]))
						continue;
					elseif(strtolower($d[0]) == 'borrower') {
						$datesArr = $d;
						$cell = 'C';
						foreach ($datesArr as $dtKey => $date) {
							if(is_null($date) || empty($date))
								continue;
							$sheet->setCellValue("$cell"."$row", "$date");
							$cell++;
							$statementDate = $this->excelCellDate($date);
						}
						$row++;
					} else {
						$sheet->setCellValue("B"."$row", "Sage");
						$cell = 'C';
						foreach ($d as $sheetDataKey => $sheetData) {
							if($sheetDataKey < 2)
								$sheet->setCellValue("$cell"."$row", $sheetData);
							else
								$sheet->setCellValue("$cell"."$row", str_replace(',', '', $sheetData))->getStyle("$cell"."$row")->getNumberFormat()->setFormatCode(NumberFormat::FORMAT_NUMBER_COMMA_SEPARATED1);
							$cell++;
						}
						$row++;
						$loan = $rDiff = null;
						$loanOption = Option::where('value', $d[1])->first();
						if(!is_null($loanOption))
							$loan = Loan::where('id', $loanOption->loan_id)->first();
						if($loan){
							$rDiff = $row+1;
							$sheet->setCellValue("A"."$row", "$loan->loan_type");
							$sheet->setCellValue("B"."$row", "Lms");
							$sheet->setCellValue("B"."$rDiff", "Diff");
							$sheet->setCellValue("C"."$row", "$loan->loan_id");
							$sheet->setCellValue("D"."$row", "$d[1]");
							if($statementDate->gte($loan->issue_at)){
								$request->loanid = $loan->id;
								$request->date = $statementDate->format('Y-m-d');
								$statements = Apiato::call('Payment@GetAllStatementsAction', [$request]);
							} else
								$statements = false;

							$cell = 'E';
							foreach ($datesArr as $tnxDtKey => $date) {
								if($tnxDtKey < 2 || is_null($date))
									continue;
								if($statements == false){
									$sheet->setCellValue("$cell"."$row", "-");
									$cell++;
									continue;
								}

								$endOfMonthDate = $this->excelCellDate($date);
			                    if($endOfMonthDate == false){
									$sheet->setCellValue("$cell"."$row", "-");
									$cell++;
									continue;
			                    }
			                    if($endOfMonthDate->gt($loan->issue_at)) {
			                    	$endOfMonthTnx = collect($statements)->where('created_at', '>=', $endOfMonthDate)->where('description', 'interest')->first();
			                    	$balance = isset($endOfMonthTnx->balance) ? $endOfMonthTnx->balance : '-';
									$sheet->setCellValue("$cell"."$row", "$balance")->getStyle("$cell"."$row")->getNumberFormat()->setFormatCode(NumberFormat::FORMAT_NUMBER_COMMA_SEPARATED1);
									$rSage = $row - 1;
									$sheet->setCellValue("$cell"."$rDiff", '='."$cell"."$rSage"."-"."$cell"."$row")->getStyle("$cell"."$rDiff")->getNumberFormat()->setFormatCode(NumberFormat::FORMAT_NUMBER_COMMA_SEPARATED1);

			                    } else {
									$sheet->setCellValue("$cell"."$row", "-");
			                    }
								$cell++;

							}
							$row++;
							$row++;
						}
						else{
							$cell = 'A';
							// $sheet->setCellValue("$cell"."$row", "$d[0]");
							// $cell++;
							$sheet->setCellValue("$cell"."$row", "Loan data not found");
							$row++;
						}

						// dd($loan, $d[0], $datesArr, $d);
					}

				} catch(Exception $e){
					$sheet->setCellValue("$cell"."$row", "$e->getMessage()");
				}
			}

			$writer = new Xlsx($spreadsheet);
			$fileName = 'CustomerActivity.xlsx';
			$writer->save($fileName);
			return $fileName;
		}
		else
			throw new \App\Containers\Payment\Exceptions\HTTPPreConditionFailedException("Error loading excel file.");
	}

	public function excelCellDate($date)
    {
        $endOfMonthDate = false;
        $dateArr = explode('/', (string)$date);
        if(count($dateArr) == 3){
            $endOfMonthDate = Carbon::parse($dateArr[1].'-'.$dateArr[0].'-'.$dateArr[2]);
        }

        return $endOfMonthDate;
    }
}