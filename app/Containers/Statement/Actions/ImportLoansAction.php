<?php

namespace App\Containers\Statement\Actions;

use App\Ship\Parents\Actions\Action;
use App\Ship\Parents\Requests\Request;
use Apiato\Core\Foundation\Facades\Apiato;
use App\Containers\Payment\Exceptions\HTTPPreConditionFailedException;
// excel libraries
use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\Worksheet\Worksheet;
use PhpOffice\PhpSpreadsheet\Reader\Csv;
use PhpOffice\PhpSpreadsheet\Reader\Xlsx;
use Illuminate\Support\Facades\Storage;

class ImportLoansAction extends Action
{
    public function run(Request $request)
    {
  //   	ini_set('memory_limit', '1000M');
		// $extensions = array("xls","xlsx","csv");
		// $result = array($request->file('file')->getClientOriginalExtension());

  //   	if($_FILES['file']['error'] == 1)
		// 	throw new HTTPPreConditionFailedException("Your file exceeds max file size limit.");
  //   	if($_FILES['file']['error'] != 0)
		// 	throw new HTTPPreConditionFailedException("Error loading your file.");
    	
		// if(in_array($result[0],$extensions)){
		 
		//     $arr_file = explode('.', $_FILES['file']['name']);
		//     $extension = end($arr_file);

		//     if('csv' == $extension) {
		//         $reader = new \PhpOffice\PhpSpreadsheet\Reader\Csv();
		//     } else {
		//         $reader = new \PhpOffice\PhpSpreadsheet\Reader\Xlsx();
		//     }
		// 	// $reader->setReadDataOnly(true);
		//     $spreadsheet = $reader->load($_FILES['file']['tmp_name']);
		$excel = new \App\Classes\Excel();
		$spreadsheet = $excel->initializeExcel($request);
		if($spreadsheet != false){
		    $isImport = $request->isImport;
		    // if($isImport)
			   //  \App\Jobs\ExportLoansQueue::dispatch();

		    // $sheetCount = $spreadsheet->getSheetCount();
	        $errors = $success = [];
		 	if($request->uploadAtSharepoint){
				$file = $request->file('file');
				$path = Storage::putFileAs(
				    'public/excel-imports', $request->file('file'), $_FILES['file']['name']
				);
			 	if($path) {
			    	$sharepoint = new \App\Classes\Sharepoint();
					$sharepoint->uploadToSharepoint(storage_path("app/").$path, "LMS Backup on import/".$_FILES['file']['name']);
					Storage::delete($path);
				}
		    }
		    $ignoredSheets = [];
		    if($request->byQueue){
		    	foreach($spreadsheet->getSheetNames() as $key => $sheet){
		    		if($spreadsheet->getSheet($key)->getSheetState() == Worksheet::SHEETSTATE_HIDDEN || $spreadsheet->getSheet($key)->getSheetState() == Worksheet::SHEETSTATE_VERYHIDDEN){
		    	   		$ignoredSheets[] = $spreadsheet->getSheet($key)->getTitle();
		    	   		continue;
		    		}
				    $sheetData = $spreadsheet->getSheet($key)->toArray();
			    	\App\Jobs\ProcessImport::dispatch($sheetData, $sheet, $isImport);
			    }
			    // upload import info on sharepoint after loan imports
			    \App\Jobs\ProcessQueueImportInfoFile::dispatch();

				$queueMsg[] = ['file' => 'All', 'message' => ['Queue is under process.info']];
	        	$response = ['code' => 200, 'ignored' => $ignoredSheets, 'success' => $queueMsg, 'errors' => $queueMsg];
		    } else{
		    	$sheetNames = $spreadsheet->getSheetNames();

		        \DB::beginTransaction();
			    
			    foreach($spreadsheet->getSheetNames() as $key => $sheet){

		    	   if($spreadsheet->getSheet($key)->getSheetState() == Worksheet::SHEETSTATE_HIDDEN || $spreadsheet->getSheet($key)->getSheetState() == Worksheet::SHEETSTATE_VERYHIDDEN){
		    	   		$ignoredSheets[] = $spreadsheet->getSheet($key)->getTitle();
		    	   	    continue;
		    	   }

			 	   	$sheetData = $spreadsheet->getSheet($key)->toArray();
			 	   	try{
				        $request = new \App\Containers\Calculator\UI\WEB\Requests\StoreCalculatorRequest();
				    	$importErrors = Apiato::call('Statement@ImportFormatedSpreadSheet2Task', [$sheetData, $request, $sheet]);
				    	// if($request->isImport == 0)
				    	// 	$isImport = $request->isImport;
			    	} catch (\Exception $e) {
			    		$errors[] = array(
					    	'file' => $sheet,
					    	'message' => ['Error importing the sheet.error'],
					    );
					    continue;
		            }
				    // $importErrors = Apiato::call('Statement@ImportAsManualLoanTask', [$sheetData, $request]);
				    
				    if(count($importErrors)){
					    $errors[] = array(
					    	'file' => $sheet,
					    	'message' => collect($importErrors)['message'],
					    );
				    }
					else{
						$errors[] = array(
					    	'file' => $sheet,
					    	'message' => ['Empty Sheet.error'],
					    );
					}
				}
			    
			    if($isImport)
		    	    \DB::commit();
		    	else
		    		\DB::rollback();
				
				$response = ['code' => 200, 'ignored' => $ignoredSheets, 'success' => $success, 'errors' => $errors];
		    }
// dd('stop');
        	return json_encode($response);
		} else{
			throw new HTTPPreConditionFailedException("Please upload right file of type csv/xlsx.");
		}

    }
}
