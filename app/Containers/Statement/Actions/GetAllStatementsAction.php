<?php

namespace App\Containers\Statement\Actions;

use App\Ship\Parents\Actions\Action;
use App\Ship\Parents\Requests\Request;
use Apiato\Core\Foundation\Facades\Apiato;

class GetAllStatementsAction extends Action
{
    public function run(Request $request)
    {
        $statements = Apiato::call('Statement@GetAllStatementsTask', [], ['addRequestCriteria']);

        return $statements;
    }
}
