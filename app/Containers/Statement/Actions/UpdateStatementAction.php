<?php

namespace App\Containers\Statement\Actions;

use App\Ship\Parents\Actions\Action;
use App\Ship\Parents\Requests\Request;
use Apiato\Core\Foundation\Facades\Apiato;

class UpdateStatementAction extends Action
{
    public function run(Request $request)
    {
        $data = $request->sanitizeInput([
            // add your request data here
        ]);

        $statement = Apiato::call('Statement@UpdateStatementTask', [$request->id, $data]);

        return $statement;
    }
}
