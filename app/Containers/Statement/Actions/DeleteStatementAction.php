<?php

namespace App\Containers\Statement\Actions;

use App\Ship\Parents\Actions\Action;
use App\Ship\Parents\Requests\Request;
use Apiato\Core\Foundation\Facades\Apiato;

class DeleteStatementAction extends Action
{
    public function run(Request $request)
    {
        return Apiato::call('Statement@DeleteStatementTask', [$request->id]);
    }
}
