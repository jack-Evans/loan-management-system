<?php

namespace App\Containers\Statement\Actions;

use App\Ship\Parents\Actions\Action;
use App\Ship\Parents\Requests\Request;
use Apiato\Core\Foundation\Facades\Apiato;

class CreateTransferAction extends Action
{
    public function run(Request $request)
    {
    	$transferRequest = [
    		'sender_loan_id' => $request->senderLoanId,
    		'receiver_loan_id' => $request->receiverLoanId,
    		'amount' => $request->amount,
    		'charge_value' => $request->chargeValue,
    		'sender_tnx_date' => $request->senderTnxDate,
    		'receiver_tnx_date' => $request->receiverTnxDate,
    		'description' => $request->description,
            'nominalCode' => $request->nominalCode,
    	];

    	$paymentRequest = [
            'paid_at'           => $request->receiverTnxDate,
            'amount'            => $request->amount,
            'loan_id'           => $request->receiverLoanId,
            'description'	    => $request->description,
            'is_capital_reduction' => $request->isCapitalReduction,
            'nominalCode'       => $request->nominalCode,
        ];

    	$loan = Apiato::call('Loan@FindLoanByIdTask', [$request->senderLoanId]);
    	$receiverLoan = Apiato::call('Loan@FindLoanByIdTask', [$request->receiverLoanId]);
    	

        Apiato::call('Statement@VerifyStatementDateTask', [$receiverLoan->id, $request->receiverTnxDate]);
        Apiato::call('Payment@CheckCreatePaymentPreCheckTask', [$receiverLoan, $paymentRequest]);

        $transfer = Apiato::call('Statement@CreateTransferTask', [$transferRequest, $paymentRequest, $loan, $receiverLoan]);

        return $transfer;
    }
}
