<?php

namespace App\Containers\Statement\Actions;

use App\Ship\Parents\Actions\Action;
use App\Ship\Parents\Requests\Request;
use Apiato\Core\Foundation\Facades\Apiato;
use App\Containers\Loan\Models\Loan;
// excel libraries
use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\Writer\Csv;
use PhpOffice\PhpSpreadsheet\Writer\Xlsx;
use PhpOffice\PhpSpreadsheet\Worksheet\Worksheet;
use PhpOffice\PhpSpreadsheet\Style\NumberFormat;

class MonthEndBalanceAction extends Action
{
    public function run(Request $request)
    {

    	$fontBold = [
		    'font' => [
		        'bold' => true,
		    ],
		];
    	$spreadsheet = new Spreadsheet();
		$spreadsheet->getProperties()->setCreator('lms.kuflink')
	      ->setLastModifiedBy('Admin')
	      ->setTitle('Month Starting Balance')
	      ->setSubject('Month Starting Balance')
	      ->setDescription('Month Starting Balance Excel');
		
		$loans = Loan::all()->groupBy('loan_type');
		$key = 0;
		foreach ($loans as $type => $loan) {
			$sheet = new Worksheet($spreadsheet, $type);
			$spreadsheet->addSheet($sheet, $key);
			$this->insertLoans($sheet, $loan, $request);
			$key++;
		}

		$writer = new Xlsx($spreadsheet);
		$fileName = 'MonthEndBalance.xlsx';
		$writer->save($fileName);

		return $fileName;
	}

	public function insertLoans($sheet, $loans, $request)
	{
        $r = 1;
	    foreach($loans as $key => $loan){
	        $c = 'A';
			$sheet->setCellValue("$c"."$r", $loan->loan_id);
			$c++;

	    	$sageId = collect($loan->options)->where('name', 'sage user id')->first();
	    	if(is_null($sageId))
				$sheet->setCellValue("$c"."$r", '');
			else
				$sheet->setCellValue("$c"."$r", $sageId->value);
			$c++;

	        $request->loanid = $loan->id;
	        $monthStatements = Apiato::call('Payment@GetMonthStatementsAction', [$request]);
	        foreach ($monthStatements as $statement) {
				$sheet->setCellValue("$c"."$r", $statement->end_date);
				$c++;
				$sheet->setCellValue("$c"."$r", $statement->closing_balance);
				$c++;
	        }

        	$r++;
	    }

    }

}