<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateTransferTables extends Migration
{

    /**
     * Run the migrations.
     */
    public function up()
    {
        Schema::create('transfers', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('sender_loan_id');
            $table->unsignedInteger('receiver_loan_id');
            $table->float('amount', 12, 4);
            $table->float('charge_value', 12, 4)->default(0);
            $table->date('sender_tnx_date');
            $table->date('receiver_tnx_date');
            $table->text('description');
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down()
    {
        Schema::drop('transfers');
    }
}
