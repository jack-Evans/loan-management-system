<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateOtherChargeBreakdownTable extends Migration
{

    /**
     * Run the migrations.
     */
    public function up()
    {
        Schema::create('breakdowns', function (Blueprint $table) {

            $table->increments('id');
            $table->unsignedInteger('loan_id')->nullable();
            $table->unsignedInteger('refund_id')->nullable();
            $table->float('daily_rate', 12, 4)->nullable();
            $table->integer('days')->nullable();
            $table->float('balance', 12, 4)->nullable();
            $table->float('rate', 12, 4)->nullable();
            $table->float('total', 12, 4)->nullable();
            $table->date('start_date')->nullable();
            $table->date('end_date')->nullable();

            $table->timestamps();
            $table->softDeletes();

        });
    }

    /**
     * Reverse the migrations.
     */
    public function down()
    {
        Schema::drop('breakdowns');
    }
}
