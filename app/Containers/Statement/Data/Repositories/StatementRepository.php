<?php

namespace App\Containers\Statement\Data\Repositories;

use App\Ship\Parents\Repositories\Repository;

/**
 * Class StatementRepository
 */
class StatementRepository extends Repository
{

    /**
     * @var array
     */
    protected $fieldSearchable = [
        'id' => '=',
        // ...
    ];

}
