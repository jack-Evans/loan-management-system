<?php

namespace App\Containers\Statement\Data\Repositories;

use App\Ship\Parents\Repositories\Repository;

/**
 * Class BreakdownRepository
 */
class BreakdownRepository extends Repository
{

    /**
     * @var array
     */
    protected $fieldSearchable = [
        'id' => '=',
        // ...
    ];

}
