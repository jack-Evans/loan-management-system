<?php

namespace App\Containers\Statement\Data\Repositories;

use App\Ship\Parents\Repositories\Repository;

/**
 * Class TransferRepository
 */
class TransferRepository extends Repository
{

    /**
     * @var array
     */
    protected $fieldSearchable = [
        'id' => '=',
        // ...
    ];

}
