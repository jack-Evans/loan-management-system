<?php

/** @var Route $router */
$router->delete('statements/{id}', [
    'as' => 'web_statement_delete',
    'uses'  => 'Controller@delete',
    'middleware' => [
      'auth:web',
    ],
]);
