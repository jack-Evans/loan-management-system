<?php

/** @var Route $router */
$router->get('transactions/revert-transaction/{tnxId}', [
    'as' => 'web_compare_month_end_balance',
    'uses'  => 'Controller@revertTransaction',
    'middleware' => [
      'auth:web',
    ],
]);
