<?php

/** @var Route $router */
$router->get('statements', [
    'as' => 'web_statement_index',
    'uses'  => 'Controller@index',
    'middleware' => [
      'auth:web',
    ],
]);
