<?php

/** @var Route $router */
$router->post('statements/store', [
    'as' => 'web_statement_store',
    'uses'  => 'Controller@store',
    'middleware' => [
      'auth:web',
    ],
]);
