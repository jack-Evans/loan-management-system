<?php

/** @var Route $router */
$router->get('statements/create', [
    'as' => 'web_statement_create',
    'uses'  => 'Controller@create',
    'middleware' => [
      'auth:web',
    ],
]);
