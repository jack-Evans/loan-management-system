<?php

/** @var Route $router */
$router->get('month-end-balance', [
    'as' => 'web_compare_month_end_balance',
    'uses'  => 'Controller@monthEndBalanceWeb',
    'middleware' => [
      'auth:web',
    ],
]);
