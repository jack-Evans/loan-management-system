<?php

/** @var Route $router */
$router->get('statements/{id}/edit', [
    'as' => 'web_statement_edit',
    'uses'  => 'Controller@edit',
    'middleware' => [
      'auth:web',
    ],
]);
