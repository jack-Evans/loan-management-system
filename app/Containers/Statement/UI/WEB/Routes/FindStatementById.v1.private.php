<?php

/** @var Route $router */
$router->get('statements/{id}', [
    'as' => 'web_statement_show',
    'uses'  => 'Controller@show',
    'middleware' => [
      'auth:web',
    ],
]);
