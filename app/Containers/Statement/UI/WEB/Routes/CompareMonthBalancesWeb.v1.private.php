<?php

/** @var Route $router */
$router->post('compare-month-balances', [
    'as' => 'web_compare_month_balances',
    'uses'  => 'Controller@compareMonthBalancesWeb',
    'middleware' => [
      'auth:web',
    ],
]);
