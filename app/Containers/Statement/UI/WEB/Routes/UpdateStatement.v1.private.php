<?php

/** @var Route $router */
$router->patch('statements/{id}', [
    'as' => 'web_statement_update',
    'uses'  => 'Controller@update',
    'middleware' => [
      'auth:web',
    ],
]);
