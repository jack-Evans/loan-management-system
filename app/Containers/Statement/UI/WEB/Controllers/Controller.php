<?php

namespace App\Containers\Statement\UI\WEB\Controllers;

use App\Containers\Statement\UI\WEB\Requests\CreateStatementRequest;
use App\Containers\Statement\UI\WEB\Requests\DeleteStatementRequest;
use App\Containers\Statement\UI\WEB\Requests\GetAllStatementsRequest;
use App\Containers\Statement\UI\WEB\Requests\FindStatementByIdRequest;
use App\Containers\Statement\UI\WEB\Requests\UpdateStatementRequest;
use App\Containers\Statement\UI\WEB\Requests\StoreStatementRequest;
use App\Containers\Statement\UI\WEB\Requests\EditStatementRequest;
use App\Ship\Parents\Controllers\WebController;
use Apiato\Core\Foundation\Facades\Apiato;

/**
 * Class Controller
 *
 * @package App\Containers\Statement\UI\WEB\Controllers
 */
class Controller extends WebController
{

    /**
     * The assigned API PATH for this Controller
     *
     * @var string
     */
    
    protected $apiPath;
    
    public function __construct(){
        $this->apiPath = config('token-container.WEB_API_URL');
    }

    /**
     * Show all entities
     *
     * @param GetAllStatementsRequest $request
     */
    public function index(GetAllStatementsRequest $request)
    {
        $statements = Apiato::call('Statement@GetAllStatementsAction', [$request]);

        // ..
    }

    /**
     * Show one entity
     *
     * @param FindStatementByIdRequest $request
     */
    public function show(FindStatementByIdRequest $request)
    {
        $statement = Apiato::call('Statement@FindStatementByIdAction', [$request]);

        // ..
    }

    /**
     * Create entity (show UI)
     *
     * @param CreateStatementRequest $request
     */
    public function create(CreateStatementRequest $request)
    {
        // ..
    }

    /**
     * Add a new entity
     *
     * @param StoreStatementRequest $request
     */
    public function store(StoreStatementRequest $request)
    {
        $statement = Apiato::call('Statement@CreateStatementAction', [$request]);

        // ..
    }

    /**
     * Edit entity (show UI)
     *
     * @param EditStatementRequest $request
     */
    public function edit(EditStatementRequest $request)
    {
        $statement = Apiato::call('Statement@GetStatementByIdAction', [$request]);

        // ..
    }

    /**
     * Update a given entity
     *
     * @param UpdateStatementRequest $request
     */
    public function update(UpdateStatementRequest $request)
    {
        $statement = Apiato::call('Statement@UpdateStatementAction', [$request]);

        // ..
    }

    /**
     * Delete a given entity
     *
     * @param DeleteStatementRequest $request
     */
    public function delete(DeleteStatementRequest $request)
    {
         $result = Apiato::call('Statement@DeleteStatementAction', [$request]);

         // ..
    }

    /**
     * Delete a given entity
     *
     * @param DeleteStatementRequest $request
     */
    public function compareMonthBalancesWeb(DeleteStatementRequest $request)
    {
        $url = $this->apiPath.'compare-month-balances';
        $request = [];

        if(empty($_FILES['file']['name']))
            return redirect()->back()->withErrors('Please Upload A File!');
        try{
            $response = postWithFile($url, $fileName = "file", $request);
            // $file = $response->data[0]->file_name;
            $file = 'CustomerActivity.xlsx';
            if (file_exists($file)) {
                header('Content-Description: File Transfer');
                header('Content-Type: application/octet-stream');
                header('Content-Disposition: attachment; filename='.basename($file));
                header('Content-Transfer-Encoding: binary');
                header('Expires: 0');
                header('Cache-Control: must-revalidate, post-check=0, pre-check=0');
                header('Pragma: public');
                header('Content-Length: ' . filesize($file));
                ob_clean();
                flush();
                readfile($file);
                unlink($file);
                exit;
                return redirect()->back();
            } else {
                return redirect()->back()->withErrors('Error exporting month balances check. Please try again later.');
            }
        } catch (\Exception $e) {
            return exception($e);
        }
    }

    /**
     * Delete a given entity
     *
     * @param DeleteStatementRequest $request
     */
    public function monthEndBalanceWeb(DeleteStatementRequest $request)
    {
        $url = $this->apiPath.'month-end-balance';

        try{
            $response = get($url);
            $file = 'MonthEndBalance.xlsx';
            if (file_exists($file)) {
                header('Content-Description: File Transfer');
                header('Content-Type: application/octet-stream');
                header('Content-Disposition: attachment; filename='.basename($file));
                header('Content-Transfer-Encoding: binary');
                header('Expires: 0');
                header('Cache-Control: must-revalidate, post-check=0, pre-check=0');
                header('Pragma: public');
                header('Content-Length: ' . filesize($file));
                ob_clean();
                flush();
                readfile($file);
                unlink($file);
                exit;
                return redirect()->back();
            } else {
                return redirect()->back()->withErrors('Error exporting month end balance. Please try again later.');
            }
        } catch (\Exception $e) {
            return exception($e);
        }
    }

    /**
     * Delete a given entity
     *
     * @param DeleteStatementRequest $request
     */
    public function revertTransaction(DeleteStatementRequest $request)
    {
        $url = $this->apiPath.'statements/revert-transaction/'.$request->tnxId;

        try{
            $response = get($url);
        } catch (\Exception $e) {
            return exception($e);
        }
        return redirect()->back();
    }
    
}
