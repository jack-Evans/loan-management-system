<?php

namespace App\Containers\Statement\UI\API\Controllers;

use App\Containers\Statement\UI\API\Requests\CreateStatementRequest;
use App\Containers\Statement\UI\API\Requests\DeleteStatementRequest;
use App\Containers\Statement\UI\API\Requests\GetAllStatementsRequest;
use App\Containers\Statement\UI\API\Requests\FindStatementByIdRequest;
use App\Containers\Statement\UI\API\Requests\UpdateStatementRequest;
use App\Containers\Statement\UI\API\Requests\ImportLoansRequest;
use App\Containers\Statement\UI\API\Requests\MonthEndBalanceRequest;
use App\Containers\Statement\UI\API\Requests\RevertTransactionRequest;
use App\Containers\Statement\UI\API\Transformers\StatementTransformer;
use App\Containers\Statement\UI\API\Transformers\TransferTransformer;
use App\Ship\Parents\Controllers\ApiController;
use Apiato\Core\Foundation\Facades\Apiato;
use App\Containers\Statement\UI\API\Requests\CreateTransferRequest;
/**
 * Class Controller
 *
 * @package App\Containers\Statement\UI\API\Controllers
 */
class Controller extends ApiController
{
    /**
     * @param CreateStatementRequest $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function createStatement(CreateStatementRequest $request)
    {
        $statement = Apiato::call('Statement@CreateStatementAction', [$request]);

        return $this->created($this->transform($statement, StatementTransformer::class));
    }

    /**
     * @param FindStatementByIdRequest $request
     * @return array
     */
    public function findStatementById(FindStatementByIdRequest $request)
    {
        $statement = Apiato::call('Statement@FindStatementByIdAction', [$request]);

        return $this->transform($statement, StatementTransformer::class);
    }

    /**
     * @param GetAllStatementsRequest $request
     * @return array
     */
    public function getAllStatements(GetAllStatementsRequest $request)
    {
        $statements = Apiato::call('Statement@GetAllStatementsAction', [$request]);

        return $this->transform($statements, StatementTransformer::class);
    }

    /**
     * @param UpdateStatementRequest $request
     * @return array
     */
    public function updateStatement(UpdateStatementRequest $request)
    {
        $statement = Apiato::call('Statement@UpdateStatementAction', [$request]);

        return $this->transform($statement, StatementTransformer::class);
    }

    /**
     * @param DeleteStatementRequest $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function deleteStatement(DeleteStatementRequest $request)
    {
        Apiato::call('Statement@DeleteStatementAction', [$request]);

        return $this->noContent();
    }

    /**
     * @param CreateTransferRequest $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function createTransfer(CreateTransferRequest $request)
    {
        $transfer = Apiato::call('Statement@CreateTransferAction', [$request]);

        return $this->transform($transfer, TransferTransformer::class);
    }

    /**
     * @param CreateTransferRequest $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function importLoans(ImportLoansRequest $request)
    {
        return $loans = Apiato::call('Statement@ImportLoansAction', [$request]);
        // dd($loans);
        // return $this->transform($loans, \App\Containers\Loan\UI\API\Transformers\LoanTransformer::class);
    }

    /**
     * @param CreateTransferRequest $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function compareMonthBalances(ImportLoansRequest $request)
    {
        $file = Apiato::call('Statement@CompareMonthBalancesAction', [$request]);
        return json_encode(['data' => array(['file_name' => $file])]);
    }

    /**
     * @param CreateTransferRequest $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function monthEndBalance(MonthEndBalanceRequest $request)
    {
        $file = Apiato::call('Statement@MonthEndBalanceAction', [$request]);
        return json_encode(['data' => array(['file_name' => $file])]);
    }

    /**
     * @param RevertTransactionRequest $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function revertTransaction(RevertTransactionRequest $request)
    {
        $transaction = Apiato::call('Statement@RevertTransactionTask', [$request->tnxId]);
        return $transaction;
    }
}
