<?php

$router->post('compare-month-balances', [
    'as' => 'api_compare_month_balances',
    'uses'  => 'Controller@compareMonthBalances',
    'middleware' => [
      'auth:api',
    ],
]);

?>