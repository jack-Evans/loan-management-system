<?php

/**
 * @apiGroup           Statement
 * @apiName            createStatement
 *
 * @api                {POST} /v1/statements Endpoint title here..
 * @apiDescription     Endpoint description here..
 *
 * @apiVersion         1.0.0
 * @apiPermission      none
 *
 * @apiParam           {String}  parameters here..
 *
 * @apiSuccessExample  {json}  Success-Response:
 * HTTP/1.1 200 OK
{
  // Insert the response of the request here...
}
 */

/** @var Route $router */
$router->post('statements', [
    'as' => 'api_statement_create_statement',
    'uses'  => 'Controller@createStatement',
    'middleware' => [
      'auth:api',
    ],
]);
