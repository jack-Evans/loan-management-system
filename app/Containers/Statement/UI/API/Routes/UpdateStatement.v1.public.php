<?php

/**
 * @apiGroup           Statement
 * @apiName            updateStatement
 *
 * @api                {PATCH} /v1/statements/:id Endpoint title here..
 * @apiDescription     Endpoint description here..
 *
 * @apiVersion         1.0.0
 * @apiPermission      none
 *
 * @apiParam           {String}  parameters here..
 *
 * @apiSuccessExample  {json}  Success-Response:
 * HTTP/1.1 200 OK
{
  // Insert the response of the request here...
}
 */

/** @var Route $router */
$router->patch('statements/{id}', [
    'as' => 'api_statement_update_statement',
    'uses'  => 'Controller@updateStatement',
    'middleware' => [
      'auth:api',
    ],
]);
