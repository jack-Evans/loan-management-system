<?php

$router->get('statements/revert-transaction/{tnxId}', [
    'as' => 'api_revert_transaction',
    'uses'  => 'Controller@revertTransaction',
    'middleware' => [
      'auth:api',
    ],
]);
