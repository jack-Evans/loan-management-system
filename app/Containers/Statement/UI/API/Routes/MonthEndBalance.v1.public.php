<?php

$router->get('month-end-balance', [
    'as' => 'api_statement_month_end_balance',
    'uses'  => 'Controller@monthEndBalance',
    'middleware' => [
      'auth:api',
    ],
]);
