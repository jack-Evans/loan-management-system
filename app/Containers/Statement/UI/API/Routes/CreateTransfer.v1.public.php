<?php

$router->post('createtransfer', [
    'as' => 'api_statement_create_transfer',
    'uses'  => 'Controller@createTransfer',
    'middleware' => [
      'auth:api',
    ],
]);
