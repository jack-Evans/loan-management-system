<?php

namespace App\Containers\Statement\UI\API\Transformers;

use App\Containers\Statement\Models\Transfer;
use App\Ship\Parents\Transformers\Transformer;
use Apiato\Core\Traits\HashIdTrait;

class TransferTransformer extends Transformer
{
    use HashIdTrait;
    /**
     * @var  array
     */
    protected $defaultIncludes = [

    ];

    /**
     * @var  array
     */
    protected $availableIncludes = [

    ];

    /**
     * @param Transfer $entity
     *
     * @return array
     */
    public function transform(Transfer $entity)
    {
        $response = [
            'object' => 'Statement',
            'id' => $entity->getHashedKey(),
            'sender_loan_id' => $this->encode($entity->sender_loan_id),
            'receiver_loan_id' => $this->encode($entity->receiver_loan_id),
            'amount' => $entity->amount,
            'charge_value' => $entity->charge_value,
            'tnx_date' => $entity->tnx_date,
            'description' => $entity->description,
            'created_at' => $entity->created_at,
            'updated_at' => $entity->updated_at,

        ];

        $response = $this->ifAdmin([
            'real_id'    => $entity->id,
            // 'deleted_at' => $entity->deleted_at,
        ], $response);

        return $response;
    }
}
