<?php

namespace App\Containers\Statement\UI\API\Transformers;

use App\Containers\Statement\Models\Statement;
use App\Ship\Parents\Transformers\Transformer;

class StatementTransformer extends Transformer
{
    /**
     * @var  array
     */
    protected $defaultIncludes = [

    ];

    /**
     * @var  array
     */
    protected $availableIncludes = [

    ];

    /**
     * @param Statement $entity
     *
     * @return array
     */
    public function transform(Statement $entity)
    {
        $response = [
            'object' => 'Statement',
            'id' => $entity->getHashedKey(),
            'created_at' => $entity->created_at,
            'updated_at' => $entity->updated_at,

        ];

        $response = $this->ifAdmin([
            'real_id'    => $entity->id,
            // 'deleted_at' => $entity->deleted_at,
        ], $response);

        return $response;
    }
}
