<?php

namespace App\Containers\Statement\UI\API\Requests;

use App\Ship\Parents\Requests\Request;

/**
 * Class CreateTransferRequest.
 */
class CreateTransferRequest extends Request
{

    /**
     * The assigned Transporter for this Request
     *
     * @var string
     */
    // protected $transporter = \App\Ship\Transporters\DataTransporter::class;

    /**
     * Define which Roles and/or Permissions has access to this request.
     *
     * @var  array
     */
    protected $access = [
        'permissions' => '',
        'roles'       => '',
    ];

    /**
     * Id's that needs decoding before applying the validation rules.
     *
     * @var  array
     */
    protected $decode = [
        'senderLoanId',
        'receiverLoanId',
    ];

    /**
     * Defining the URL parameters (e.g, `/user/{id}`) allows applying
     * validation rules on them and allows accessing them like request data.
     *
     * @var  array
     */
    protected $urlParameters = [
        // 'id',
    ];

    /**
     * @return  array
     */
    public function rules()
    {
        return [
            'senderLoanId'   => 'required|exists:loans,id',
            'receiverLoanId' => 'required|exists:loans,id',
            'amount'         => 'required|regex:/^\d*(\.\d{1,4})?$/',
            'chargeValue'    => 'required|regex:/^\d*(\.\d{1,4})?$/',
            'senderTnxDate'  => 'required|date_format:Y-m-d',
            'receiverTnxDate'=> 'required|date_format:Y-m-d',
            'description'    => 'required|string|min:3',
        ];
    }

    /**
     * @return  bool
     */
    public function authorize()
    {
        return $this->check([
            'hasAccess',
        ]);
    }
}
