<?php

namespace App\Containers\Statement\UI\API\Requests;

use App\Ship\Parents\Requests\Request;

/**
 * Class RevertTransactionRequest.
 */
class RevertTransactionRequest extends Request
{

    /**
     * The assigned Transporter for this Request
     *
     * @var string
     */
    // protected $transporter = \App\Ship\Transporters\DataTransporter::class;

    /**
     * Define which Roles and/or Permissions has access to this request.
     *
     * @var  array
     */
    protected $access = [
        'permissions' => '',
        'roles'       => '',
    ];

    /**
     * Id's that needs decoding before applying the validation rules.
     *
     * @var  array
     */
    protected $decode = [
        'tnxId',
    ];

    /**
     * Defining the URL parameters (e.g, `/user/{id}`) allows applying
     * validation rules on them and allows accessing them like request data.
     *
     * @var  array
     */
    protected $urlParameters = [
        'tnxId',
    ];

    /**
     * @return  array
     */
    public function rules()
    {
        return [
             'tnxId' => 'required|exists:journals,id',
            // 'endDate' => 'required|date_format:Y-m-d',
            // '{user-input}' => 'required|max:255',
        ];
    }

    /**
     * @return  bool
     */
    public function authorize()
    {
        return $this->check([
            'hasAccess',
        ]);
    }
}
