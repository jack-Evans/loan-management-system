<?php

namespace App\Containers\Statement\Tasks;

use App\Containers\Statement\Data\Repositories\StatementRepository;
use App\Ship\Parents\Tasks\Task;

class GetAllStatementsTask extends Task
{

    protected $repository;

    public function __construct(StatementRepository $repository)
    {
        $this->repository = $repository;
    }

    public function run()
    {
        return $this->repository->paginate();
    }
}
