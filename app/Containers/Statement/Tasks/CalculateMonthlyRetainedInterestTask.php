<?php
namespace App\Containers\Statement\Tasks;

use App\Ship\Parents\Tasks\Task;
use App\Containers\Loan\Models\Loan;
use App\Containers\Payment\Models\Statement;
use Apiato\Core\Foundation\Facades\Apiato;
use Carbon\Carbon;

class CalculateMonthlyRetainedInterestTask extends Task
{
    public function __construct()
    {
    }
    public function run(Loan $loan, $balance, $lastTnxDate, $payments = [], $requestDate = false, $grossLoan, $renewalCharges = [], $transactionId = null, $loanClosingDate)
    {
        $startDate = $lastTnxDate;
        $endOfMonth = $lastTnxDate->copy()->endOfMonth();
        if($requestDate && $endOfMonth->gt($requestDate))
            $endOfMonth = $requestDate;

        $interestBreakDown = [];
        $normalRate = $rate = $loan->interest->rate;
        $defaultRate = $loan->interest->default_rate;
        // $loanClosingDate = addDecimalMonthsIntoDate($loan->interest->duration, $loan->issue_at)->copy()->subDay();
        $sotredPayments = [];

		if($lastTnxDate->gte($loanClosingDate))
            $grossLoan = $balance;

        if($loanClosingDate->lte($lastTnxDate) && count($renewalCharges)){
            $startDateWithInDuration = $renewalCharges->where('renew_end_date', '>=', $lastTnxDate)->where('renew_date', '<=', $lastTnxDate)->first();
            if($startDateWithInDuration){
                $loanClosingDate = $startDateWithInDuration->renew_end_date->copy()->subDay();
                $rate = $normalRate;
                // $grossLoan = $loan->interest->gross_loan;
            } else{
                $rate = $defaultRate;
                $grossLoan = $balance;
            }
        } elseif($loanClosingDate->lte($lastTnxDate) && count($renewalCharges) == 0)
            $rate = $defaultRate;

        $ignorePmt = 0;
        if(($loanClosingDate->month === $startDate->month && $loanClosingDate->year === $startDate->year) && $endOfMonth->gt($loanClosingDate) && count($payments) == 0){
            // var_dump($startDate, $loanClosingDate); 
            $breakDown = Apiato::call('Statement@CreateInterestBreakDownTask', [$loan, $rate, $grossLoan, $startDate, $loanClosingDate, $plusOne = true]);
            if($breakDown != false)
                $interestBreakDown[] = $breakDown;
            $startDate = $loanClosingDate->copy()->addDay();
            $rate = $defaultRate;
        }
        if($payments) {
            $sotredPayments = sortByDate($payments);
            foreach($sotredPayments as $key => $payment) {
                if($payment['ignorePmt'])
                    $ignorePmt += $payment['ignorePmt'];
                // to handle multiple payments at one date OR at first day of month.(skip other payments on same date)
                if($startDate == $payment['date'] && $key > 0 || $payment['date'] == $payment['date']->copy()->startOfMonth())
                    continue;
                if(($loanClosingDate->month === $startDate->month && $loanClosingDate->year === $startDate->year) && $payment['date']->gte($loanClosingDate)){
                    // var_dump($loanClosingDate, $startDate, $payment['previousBalance'], $grossLoan);
                    $breakDown = Apiato::call('Statement@CreateInterestBreakDownTask', [$loan, $rate, $payment['previousBalance'], $startDate, $loanClosingDate, $plusOne = true]);
                    if($breakDown != false){
                        $interestBreakDown[] = $breakDown;
                    	$startDate = $loanClosingDate->copy()->addDay();
                	}
                    // dd($grossLoan, $breakDown);
                    $rate = $defaultRate;
                }
                $breakDown = Apiato::call('Statement@CreateInterestBreakDownTask', [$loan, $rate, $payment['previousBalance'], $startDate, $payment['date']->copy()->subDay(), $plusOne = true ]);
                if($breakDown != false)
                    $interestBreakDown[] = $breakDown;

                $grace = getTransactionGrace($loan, $startDate)->last();
                if($grace) {
                    $addDays = $grace->days;
                    if($startDate->copy()->subDay()->lte($loanClosingDate->copy()->addDays($addDays))) {
                        $breakDown = Apiato::call('Statement@CreateInterestBreakDownTask', [$loan, $defaultRate, $payment['previousBalance'], $loanClosingDate->copy()->addDay(), $payment['date']->copy()->subDay(), $plusOne = true, $negativeInterest = true]);
                        if($breakDown != false)
                            $interestBreakDown[] = $breakDown;
                    }
                }

                $startDate = $payment['date']->eq($loanClosingDate) ? $payment['date']->copy()->addDay() : $payment['date'];

                if($rate == $defaultRate && count($renewalCharges)) {
	                $dateWithInDuration = $renewalCharges->where('renew_end_date', '>', $startDate)->where('renew_date', '<=', $startDate)->first();
		            if($dateWithInDuration){
		                $loanClosingDate = $dateWithInDuration->renew_end_date->copy()->subDay();
		                $rate = $normalRate;
		            } else
		            	$rate = $defaultRate;
                }
            }
        }

        if( ($loanClosingDate->month === $startDate->month && $loanClosingDate->year === $startDate->year) && $startDate->lte($loanClosingDate) && $rate == $normalRate){
            $breakDown = Apiato::call('Statement@CreateInterestBreakDownTask', [$loan, $rate, $grossLoan, $startDate, $loanClosingDate, $plusOne = true]);
            if($breakDown != false){
                $interestBreakDown[] = $breakDown;
            	$startDate = $loanClosingDate->copy()->addDay();
        	}
            $rate = $defaultRate;
        }

        $breakDown = Apiato::call('Statement@CreateInterestBreakDownTask', [$loan, $rate, $grossLoan, $startDate, $endOfMonth, $plusOne = true]);
        if($breakDown != false)
            $interestBreakDown[] = $breakDown;

        if($requestDate && $requestDate->eq($endOfMonth)) {
            $grace = getTransactionGrace($loan, $startDate)->last();
            if($grace) {
                $addDays = $grace->days;
                if($requestDate->lte($loanClosingDate->copy()->addDays($addDays))) {
                    $breakDown = Apiato::call('Statement@CreateInterestBreakDownTask', [$loan, $defaultRate, $balance, $startDate, $endOfMonth, $plusOne = true, $negativeInterest = true]);
                    if($breakDown != false)
                        $interestBreakDown[] = $breakDown;
                }
            }
        }
            

        $totalInterestAmount = collect(Apiato::call('Statement@SetInterestBreakDownTask', [$interestBreakDown]))->sum('total') - $ignorePmt;
        $balanceAfterInterest = $balance + $totalInterestAmount;

        $statement  = new Statement;
        $statement->setCreatedAtAttribute($endOfMonth);
        $statement->setDescriptionAttribute('interest');
        $statement->setCommentsAttribute("Retained interest from {$lastTnxDate->format('M d y')} to {$endOfMonth->format('M d y')}");
        $statement->setDebitAttribute($totalInterestAmount);
        $statement->setCreditAttribute(null);
        $statement->setBalanceAttribute($balanceAfterInterest);
        $statement->setInterestBreakDownAttribute(Apiato::call('Statement@SetInterestBreakDownTask', [$interestBreakDown]));
        $statement->setTransactionIdAttribute($transactionId);
        // dd($statement);
        return $statement;
    }

}