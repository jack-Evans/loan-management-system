<?php
namespace App\Containers\Statement\Tasks;

use App\Ship\Parents\Tasks\Task;
use App\Containers\Loan\Models\Loan;
use App\Containers\Payment\Models\Statement;
use Apiato\Core\Foundation\Facades\Apiato;

class CalculateFirstMonthInterestTask extends Task
{
    public function run(Loan $loan, $balance , $payments = [], $requestDate = false, $transactionId = null)
    {
        $startDate = $loan->issue_at;
        $endOfMonth = $loan->issue_at->copy()->endOfMonth();
        $interestBreakDown = [];
        $rate = $loan->interest->rate;
        $balanceforCalculation = $balance;
        // Days Before Payments
        $ignorePmt = 0;
        if($payments) {
            foreach(sortByDate($payments) as $payment) {
                if($payment['ignorePmt'])
                    $ignorePmt += $payment['ignorePmt'];
                $balanceforCalculation = $payment['previousBalance'];
                // $dailyRate = calculateDailyRate($rate, $balanceforCalculation);
                // $diffInDays = $startDate->diffInDays($payment['date']);
                // $debitInterestAmount = round($dailyRate * $diffInDays, 2);
                // if($debitInterestAmount){
                    $breakDown = Apiato::call('Statement@CreateInterestBreakDownTask', [$loan, $rate, $balanceforCalculation, $startDate->copy(), $payment['date']->copy()->subDays(1), $plusOne = true]);
                    if($breakDown != false)
                        $interestBreakDown[] = $breakDown;
                    // $interestBreakDown[] = ['dailyRate' => $dailyRate, 'days' => $diffInDays, 'balance' => $balanceforCalculation, 'rate' => $rate, 'total' => $debitInterestAmount, 'start' => $startDate->copy(), 'end' => $payment['date']->copy()->subDays(1)];
                // }
                $startDate = $payment['date']->copy();
                $balanceforCalculation = $payment['currentBalance'];
            }
        }
        if($requestDate && $endOfMonth > $requestDate)
            $endOfMonth = $requestDate;

        // Days After Payment
        // $diffInDays = $startDate->diffInDays($endOfMonth) + 1;
        // $dailyRate = calculateDailyRate($rate, $balanceforCalculation);
        // $debitInterestAmount = round($dailyRate * $diffInDays, 2);
        $breakDown = Apiato::call('Statement@CreateInterestBreakDownTask', [$loan, $rate, $balanceforCalculation, $startDate->copy(), $endOfMonth, $plusOne = true]);
        if($breakDown != false)
            $interestBreakDown[] = $breakDown;
        // $interestBreakDown[] = ['dailyRate' => $dailyRate, 'days' => $diffInDays, 'balance' => $balanceforCalculation, 'rate' => $rate, 'total' => $debitInterestAmount, 'start' => $startDate->copy(), 'end' => $endOfMonth];

        // $totalInterestAmount = round(collect($interestBreakDown)->sum('total'), 2);
        $totalInterestAmount = round(collect(Apiato::call('Statement@SetInterestBreakDownTask', [$interestBreakDown]))->sum('total') - $ignorePmt, 2);
        // $balanceAfterInterest =  ($loan->loan_type === 'retained') ? $balance : $balance + $totalInterestAmount;

        // if($loan->loan_type === 'retained')
        //     $balanceAfterInterest = $balance;
        // else
        $balanceAfterInterest = $balance + $totalInterestAmount;

        $statement  = new Statement;
        $statement->setCreatedAtAttribute($endOfMonth);
        $statement->setDescriptionAttribute('interest');
        $statement->setCommentsAttribute("Int from {$loan->issue_at->format('M d y')} to {$endOfMonth->format('M d y')}");
        $statement->setDebitAttribute($totalInterestAmount);
        $statement->setCreditAttribute(null);
        $statement->setBalanceAttribute($balanceAfterInterest);
        $statement->setInterestBreakDownAttribute(Apiato::call('Statement@SetInterestBreakDownTask', [$interestBreakDown]));
        $statement->setTransactionIdAttribute($transactionId);

        return $statement;
    }

}