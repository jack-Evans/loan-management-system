<?php

namespace App\Containers\Statement\Tasks;

use App\Containers\Statement\Data\Repositories\StatementRepository;
use App\Ship\Exceptions\UpdateResourceFailedException;
use App\Ship\Parents\Tasks\Task;
use Exception;

class UpdateStatementTask extends Task
{

    protected $repository;

    public function __construct(StatementRepository $repository)
    {
        $this->repository = $repository;
    }

    public function run($id, array $data)
    {
        try {
            return $this->repository->update($data, $id);
        }
        catch (Exception $exception) {
            throw new UpdateResourceFailedException();
        }
    }
}
