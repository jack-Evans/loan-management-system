<?php

namespace App\Containers\Statement\Tasks;

use App\Containers\Payment\Data\Repositories\JournalRepository;
use App\Ship\Parents\Tasks\Task;
use Carbon\Carbon;
use App\Containers\Payment\Exceptions\HTTPPreConditionFailedException;
use Apiato\Core\Foundation\Facades\Apiato;
use App\Containers\Loan\Models\Extras;

class VerifyStatementDateTask extends Task
{

    protected $journalRepository;

    public function __construct(JournalRepository $journalRepository)
    {
        $this->journalRepository = $journalRepository;
    }

    public function run($loanId, $date)
    {
        $lastJournal = $this->journalRepository->orderBy('tnx_date', 'asc')->findWhere(['loan_id' => $loanId])->where('type', '!=', 'skip')->last();
        $date = Carbon::parse($date);
        // $lastJournal->tnx_date  = Carbon::parse('2020-01-01');
        // $date                   = Carbon::parse('2019-12-30');
// dd($lastJournal->tnx_date->gt($date) && ($lastJournal->tnx_date->format('Y') <= $date->format('Y') && $lastJournal->tnx_date->format('m') > $date->format('m')));
        if($lastJournal->tnx_date->gt($date) && ($lastJournal->tnx_date->year > $date->year || $lastJournal->tnx_date->month > $date->month)){

            $verifyStatementDate = Extras::where('name', 'VerifyStatementDate')->first();
            if( ($verifyStatementDate && $verifyStatementDate->value) || is_null($verifyStatementDate) )
            	throw new HTTPPreConditionFailedException('Transaction can not be made in previous month.');

        }
// dd($lastJournal->tnx_date->gt($date) , $lastJournal->tnx_date->year >= $date->year , $lastJournal->tnx_date->month , $date->month);
        // else{
            $interests = Apiato::call('Interest@GetInterestsAsPmtPlanVersionsTask', [$loanId])->last();
            $paymentPlanDate = isset($interests->payment_plan_date) ? Carbon::parse($interests->payment_plan_date) : false;
            if($paymentPlanDate && Carbon::parse($paymentPlanDate)->gt($date))
                throw new HTTPPreConditionFailedException("Transaction can only be made after revised payment plan date. '{$paymentPlanDate->format("d M Y")}'");
        // }
    }
}
