<?php

namespace App\Containers\Statement\Tasks;

use App\Containers\Statement\Data\Repositories\TransferRepository;
use App\Ship\Exceptions\CreateResourceFailedException;
use App\Ship\Parents\Tasks\Task;
use Exception;
use Carbon\Carbon;
use Apiato\Core\Foundation\Facades\Apiato;
use App\Containers\Payment\Data\Transporters\CreatePaymentTransporter;

class CreateTransferTask extends Task
{

    protected $repository;

    public function __construct(TransferRepository $repository)
    {
        $this->repository = $repository;
    }

    public function run(array $data, $paymentRequest, $loan, $receiverLoan)
    {
        try {

            $assetType = 'transfer';
            $tnxDate        = Carbon::parse($data['sender_tnx_date']);
            $accountingPeriod   = "{$tnxDate->format('M')} {$tnxDate->year}";

            \DB::beginTransaction();

            // Create transfer charge
            $transfer = $this->repository->create($data);

            $transferJournal = [
                'creditAccountId' => $loan->customer->account->id,
                'debitAccountId' => 0,
                'type' => $assetType,
                'loanid' => $loan->id,
                'tnxDate' => $tnxDate,
                'amount' => $data['amount'],
                'accountingPeriod' => $accountingPeriod,
                'assetType' => $assetType,
                'description' => $data['description'],
                'payableId' => $transfer->id,
                'payableType' => 'App\Containers\Statement\Models\Transfer',
                'nominalCode' => (isset($data['nominalCode'])) ? $data['nominalCode'] : 1,
            ];
            $transferJournal = Apiato::call('Payment@CreateJournalPostingTask', [$transferJournal]);

            $assetType = 'charge';

            $chargeJournal = [
                'creditAccountId' => $loan->customer->account->id,
                'debitAccountId' => 0,
                'type' => $assetType,
                'loanid' => $loan->id,
                'tnxDate' => $tnxDate,
                'amount' => $data['charge_value'],
                'accountingPeriod' => $accountingPeriod,
                'assetType' => $assetType,
                'description' => 'Charge for:'.$data['description'],
                'payableId' => $transfer->id,
                'payableType' => 'App\Containers\Statement\Models\Transfer',
                'nominalCode' => (isset($data['nominalCode'])) ? $data['nominalCode'] : 1,
            ];
            $chargeJournal = Apiato::call('Payment@CreateJournalPostingTask', [$chargeJournal]);

            // $paymentRequest = [
            //     'paid_at'           => $data['receiver_tnx_date'],
            //     'amount'            => $data['amount'],
            //     'loan_id'           => $receiverLoan->id,
            //     'description'       => $data['description'],
            //     'is_capital_reduction' => 0
            // ];
            
            $payment = Apiato::call('Payment@CreatePaymentTask', [(new CreatePaymentTransporter($paymentRequest))->toArray(), $receiverLoan]);

            // dd($transfer, $payment);
            // if( ($prevousRenewals && $loan->loan_type === 'retained') || $loan->loan_type === 'serviced'){
            //     $journalPosting = [
            //         'creditAccountId' => $loan->customer->account->id,
            //         'debitAccountId' => 0,
            //         'type' => 'skip',
            //         'loanid' => $loan->id,
            //         'tnxDate' => ($loan->loan_type === 'serviced') ? Carbon::parse($data['renew_end_date'])->copy()->addDay() : $prevousRenewals->renew_end_date->copy()->addDay(),
            //         'amount' => 0,
            //         'accountingPeriod' => $accountingPeriod,
            //         'assetType' => 'skip',
            //         'description' => 'skip',
            //         'payableId' => 0,
            //         'payableType' => ''
            //     ];
            //     $journalPosting = Apiato::call('Payment@CreateJournalPostingTask', [$journalPosting]);
            //     // dd($journalPosting);
            // }
            // dd('stop');
            \DB::commit();
            return $transfer;
        }
        catch (Exception $exception) {
            throw new CreateResourceFailedException();
        }
    }
}
