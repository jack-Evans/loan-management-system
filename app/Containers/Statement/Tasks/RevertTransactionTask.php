<?php

namespace App\Containers\Statement\Tasks;

use App\Ship\Exceptions\UpdateResourceFailedException;
use App\Containers\Payment\Data\Repositories\JournalRepository;
use App\Containers\Payment\Data\Repositories\PostRepository;
use App\Ship\Parents\Tasks\Task;
use Exception;

class RevertTransactionTask extends Task
{

    protected $repository;
    protected $postRepository;

    public function __construct(JournalRepository $repository, PostRepository $postRepository)
    {
        $this->repository = $repository;
        $this->postRepository = $postRepository;
    }

    public function run($id)
    {
        $data['type'] = 'skip';
        try {
            \DB::beginTransaction();
             $journal = $this->repository->update($data, $id);
             $post = $this->postRepository->where('journal_id', $id)->update(['asset_type' => 'skip']);
             \DB::commit();
             return $journal;
        }
        catch (Exception $exception) {
            throw new UpdateResourceFailedException();
        }
    }
}
