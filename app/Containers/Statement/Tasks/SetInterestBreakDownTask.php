<?php
namespace App\Containers\Statement\Tasks;

use App\Ship\Parents\Tasks\Task;
use App\Containers\Payment\Models\InterestBreakDown;

class SetInterestBreakDownTask extends Task
{
    public function run(Array $breakDownRequest)
    {
        $response = [];
        
        foreach($breakDownRequest as $breakDown)
        {
            // if(!isset($breakDown['dailyRate'])){
            if(is_array($breakDown)){
                foreach ($breakDown as $subBreakDown) {
                    $response[] = $this->setInterestBreakDown($subBreakDown);                
                }
            }
            // } else{
            //     $response[] = $this->setInterestBreakDown($breakDown);
            // }
        }
        return $response; 
    }

    public function setInterestBreakDown($breakDown)
    {
        $interestBreakDown = new InterestBreakDown;
    
        $interestBreakDown->setDailyRateAttribute(round($breakDown['dailyRate'], 2));
        $interestBreakDown->setDaysAttribute($breakDown['days']);
        $interestBreakDown->setBalanceAttribute(round($breakDown['balance'], 2));
        $interestBreakDown->setRateAttribute(round($breakDown['rate'], 2));
        $interestBreakDown->setTotalAttribute(round($breakDown['total'], 2));
        $interestBreakDown->setStartAttribute($breakDown['start']);
        $interestBreakDown->setEndAttribute($breakDown['end']);
        if(isset($breakDown['refunded'])) 
            $interestBreakDown->setRefundedAttribute($breakDown['refunded']);
        if(isset($breakDown['rounding_error'])) 
            $interestBreakDown->setRoundingError(round($breakDown['rounding_error'], 2));
        if(isset($breakDown['hide'])) 
            $interestBreakDown->setHideAttribute($breakDown['hide']);
        if(isset($breakDown['hideTransaction'])) 
            $interestBreakDown->setHideTransactionAttribute($breakDown['hideTransaction']);

        return $interestBreakDown;
    }

}