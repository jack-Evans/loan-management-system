<?php

namespace App\Containers\Statement\Tasks;

use App\Ship\Parents\Tasks\Task;
use Exception;
use App\Containers\Loan\Data\Transporters\CreateLoanTransporter;
use App\Containers\Interest\Data\Transporters\CreateInterestTransporter;
use Carbon\Carbon;
use Apiato\Core\Foundation\Facades\Apiato;
use App\Containers\Payment\Data\Transporters\CreateRefundTransporter;
use App\Containers\Loan\Models\Loan;
use App\Containers\NominalCode\Models\NominalCode;
use App\Containers\Payment\Exceptions\HTTPPreConditionFailedException;
use App\Containers\OtherCharge\Models\OtherCharge;
use App\Containers\Payment\Models\Payment;
use App\Containers\RenewalCharge\Models\RenewalCharge;
use App\Containers\LoanCharge\Models\LoanCharge;

class ImportFormatedSpreadSheetTask extends Task
{
    public function run(array $sheetData, $request = [])
    {
        // $lastLoanId = Loan::all()->last()->id + 1;
        $loan = $loanRequest = $interestRequest = [];
        $loanRequest['loan_type'] = 'manual';
        $loanRequest['status'] = 'approved';
        // $loanRequest['loan_id'] = $lastLoanId;
        $loanRequest['customer_id'] = 1;
        $loanRequest['holiday_refund'] = 1;
        $interestRequest['min_term'] = $interestRequest['monthly_payment'] = 0;
        
        $errors = [];
        
        // \DB::beginTransaction();
        foreach ($sheetData as $key => $data) {
            array_shift($data);
            if($key === 0)
                continue;
            $rowNumber = $key+1;
            // dd((strtolower($data[0]) == 'loan' && $rowNumber == 2) && (strtolower($data[6]) == 'serviced' || strtolower($data[6]) == 'retained'), $rowNumber, strtolower($data[6]), $data);
            if((strtolower($data[0]) == 'loan' && $rowNumber == 2) && (strtolower($data[6]) == 'serviced' || strtolower($data[6]) == 'retained')) {
                $data[6] = strtolower($data[6]);
                $loan = Loan::where('loan_id', '=', $data[5])->first();
                if($loan === null){
                    $request->loanId = $data[5];
                    $request->customerId = 1;
                    $request->netloan = $data[9];
                    $request->loanType = $data[6];
                    $request->issueAt = $data[1];
                    $request->holidayRefund = ($data[2] == 'yes') ? 1 : 0;
                    $request->status = 'processing';
                    
                    $request->duration = $data[11];
                    $request->duration2 = null;
                    $request->minTerm = $data[12];
                    $request->rate = $data[7]*100;
                    $request->defaultRate = $data[8]*100;
                    $request->grossloan = ($request->loanType == 'serviced' || $request->loanType == 'manual') ? $request->netloan : $data[10];
                    $request->nominalCode   = $this->getNominalCode($data[4], $data[3]);

                    try{
                        $loan = Apiato::call('Loan@CreateLoanAction', [$request, $excelRequest = false]);
                    } catch (Exception $e) {
                        $errors['message'][] = "Row $rowNumber: ".$e->getMessage()."(incorrect loan data)";
                        return $errors;
                    }
                }
                // else{
                //     $errors['message'][] = "Row $rowNumber: Processed previously.";
                //     // break;
                // }
                $loanId = $loan->id;
            } elseif(!isset($loanId)) {
                $errors['message'][] = "Incorrect loan info.";
                return $errors;
            } elseif($data[0] == 'initial_charge') {
                $initialCharge = Apiato::call('Charge@GetAllChargesTask')->where('name', 'like', $data[3])->first();
                // dd($initialCharge);
                if($initialCharge == null) {
                    $request->name          = $data[3];
                    $request->value         = $data[2];
                    $request->amounttype    = 'netloan';
                    $request->chargetype    = 'fixed';
                    try{
                        $initialCharge = Apiato::call('Charge@CreateChargeAction', [$request]);
                    } catch (Exception $e) {
                        $errors['message'][] = "Row $rowNumber: ".$e->getMessage()."(unable to create initial charge)";
                        continue;
                    }
                }
                $loanCharge = LoanCharge::whereRaw("loan_id = $loanId and charge_id = $initialCharge->id and value = $data[2]")->first();
                if($loanCharge === null){
                    $request->loanid        = $loanId;
                    $request->chargeid      = $initialCharge->id;
                    $request->value         = $data[2];
                    $request->nominalCode   = $this->getNominalCode($data[4], $data[3]);
                    try{
                        $charge = Apiato::call('LoanCharge@CreateLoanChargeAction', [$request, $excelRequest = true]);
                    } catch (Exception $e) {
                        $errors['message'][] = "Row $rowNumber: ".$e->getMessage();
                        // break;
                        // return ['status' => false, 'message' => $e->getMessage()];
                    }
                } 
                // else
                //     $errors['message'][] = "Row $rowNumber: ".'Processed previously.';
            } elseif($data[0] == 'other_charge') {
                $otherCharge = OtherCharge::whereRaw("loan_id = $loanId and charge_date = '$data[1]' and name = '$data[3]' and value = $data[2]")->first();
                if($otherCharge === null){
                    $request->chargedate    = $data[1];
                    $request->loanid        = $loanId;
                    $request->name          = $data[3];
                    $request->value         = $data[2];
                    $request->amounttype    = 'netloan';
                    $request->chargetype    = 'fixed';
                    $request->nominalCode   = $this->getNominalCode($data[4], $data[3]);
                    try{
                        $otherCharge = Apiato::call('OtherCharge@CreateOtherChargeAction', [$request]);
                    } catch (Exception $e) {
                        $errors['message'][] = "Row $rowNumber: ".$e->getMessage();
                    }
                }
                // else
                //     $errors['message'][] = "Row $rowNumber: ".'Processed previously.';
            } elseif($data[0] == 'payment' || $data[0] == 'transfer') {
                $payment = Payment::whereRaw("loan_id = $loanId and paid_at = '$data[1]' and amount = $data[2]")->first();
                if($payment === null){
                    $request->paidAt            = $data[1];
                    $request->amount            = $data[2];
                    $request->loanId            = $loanId;
                    $request->description       = $data[3];
                    $request->isCapitalReduction= ($data[5] == 'yes') ? 1 : 0;
                    $request->nominalCode       = $this->getNominalCode($data[4], $data[3]);
                    try{
                        $payment = Apiato::call('Payment@CreatePaymentAction', [$request]);
                    } catch (Exception $e) {
                        $errors['message'][] = "Row $rowNumber: ".$e->getMessage();
                    }
                }
                // else
                //     $errors['message'][] = "Row $rowNumber: ".'Processed previously.';
            } elseif($data[0] == 'renewal_charge') {
                $renewalCharge = RenewalCharge::whereRaw("loan_id = $loanId and duration = '$data[5]' and value = $data[2] and renew_date = '$data[6]'")->first();
                if($renewalCharge === null){
                    $request->loanId = $loanId;
                    $request->duration = $data[5];
                    $request->value = $data[2];
                    $request->description = $data[3];
                    $request->renewDate = $data[6];
                    $request->nominalCode = $this->getNominalCode($data[4], $data[3]);
                    try{
                        $renewal = Apiato::call('RenewalCharge@CreateRenewalChargeAction', [$request]);
                    } catch (Exception $e) {
                        $errors['message'][] = "Row $rowNumber: ".$e->getMessage();
                    }
                } 
                // else
                //     $errors['message'][] = "Row $rowNumber: ".'Processed previously.';
            }
        }
        // \DB::commit();

        return $errors;
    }

    public function getNominalCode($nominalCode, $description)
    {
        if($nominalCode != null){
            $code = NominalCode::where('code', '=', $nominalCode)->first();
            if($code == null){
                $data = [
                    'code' => $nominalCode,
                    'name' => $description,
                ];
                $code = Apiato::call('NominalCode@CreateNominalCodeTask', [$data]);
            }
        } else
            $code = NominalCode::get()->first();
        return $code->id;
    }
}
