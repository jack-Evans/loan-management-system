<?php
namespace App\Containers\Statement\Tasks;

use App\Containers\Loan\Models\Loan;
use App\Ship\Parents\Tasks\Task;
use Carbon\Carbon;

class CreateInterestBreakDownTask extends Task
{
    public function run(Loan $loan, $rate, $balance, $startDate, $endDate, $plusOne = false, $negativeInterest = false)
    {
        // $freezedLog = $loan->loanStatusLog->where('status', '=', 'freezed')->first();
        // if($freezedLog && Carbon::parse($freezedLog->status_change_date)->lt($startDate))
        if($loan->status === 'freezed' && Carbon::parse($loan->status_change_date)->lte($startDate))
            return false;
        $normalRate = $loan->interest->rate;
        $defaultRate = $loan->interest->default_rate;

        if($startDate > $endDate)
            return false;
        
        $addOne = 0;
        if($plusOne)
            $addOne = 1;        
        $dailyRate1 = $dailyRate = calculateDailyRate($normalRate, $balance, $loan, $startDate);
        $dailyRate = round($dailyRate, 8);

        $diffInDays = $startDate->diffInDays($endDate) + $addOne;
        $debitInterestAmount = round($dailyRate * $diffInDays, 8);
        
        if($negativeInterest){

            $dailyRate1 = $dailyRate = calculateDailyRate($rate, $balance, $loan, $startDate);
            $dailyRate = round($dailyRate, 8);
    
            $diffInDays = $startDate->diffInDays($endDate) + $addOne;
            $debitInterestAmount = round($dailyRate * $diffInDays, 8);

            $breakDown[] = ['dailyRate' => $dailyRate, 'days' => $diffInDays, 'balance' => $balance, 'rate' => $rate, 
                'total' => -$debitInterestAmount, 'start' => $startDate->copy(), 'end' => $endDate->copy(), 'rounding_error' => $debitInterestAmount - (round($dailyRate1, 8)*($diffInDays))];
            return $breakDown;    
        }
        
        if($debitInterestAmount && $debitInterestAmount > 0)
        {
            // dd(round($dailyRate, 2)*$diffInDays - $debitInterestAmount);
            $breakDown[] = ['dailyRate' => $dailyRate, 'days' => $diffInDays, 'balance' => $balance, 'rate' => $normalRate, 
                'total' => $debitInterestAmount, 'start' => $startDate->copy(), 'end' => $endDate->copy(), 'rounding_error' => $debitInterestAmount - (round($dailyRate1, 8)*($diffInDays))];
            if($rate == $defaultRate){
                $dailyRate1 = $dailyRate = calculateDailyRate($defaultRate, $balance, $loan, $startDate);
                $dailyRate = round($dailyRate, 8);
        
                $debitInterestAmount = round($dailyRate * $diffInDays, 8);
                $breakDown[] = ['dailyRate' => $dailyRate, 'days' => $diffInDays, 'balance' => $balance, 'rate' => $defaultRate, 
                'total' => $debitInterestAmount, 'start' => $startDate->copy(), 'end' => $endDate->copy(), 'rounding_error' => $debitInterestAmount - (round($dailyRate1, 8)*($diffInDays))];
            }
            return $breakDown;
        }
        
        return false;
    }
}