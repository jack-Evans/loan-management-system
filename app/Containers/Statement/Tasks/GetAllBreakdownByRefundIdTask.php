<?php

namespace App\Containers\Statement\Tasks;

use App\Containers\Statement\Data\Repositories\BreakdownRepository;
use App\Ship\Parents\Tasks\Task;

class GetAllBreakdownByRefundIdTask extends Task
{

    protected $repository;

    public function __construct(BreakdownRepository $repository)
    {
        $this->repository = $repository;
    }

    public function run($loanId, $refundId)
    {
    	return $this->repository->scopeQuery(function($q) use($loanId, $refundId) {
    		$q->where('refund_id', $refundId);
            return $q->where('loan_id', $loanId);
        })->orderBy('start_date', 'asc')->all();
    }
}
