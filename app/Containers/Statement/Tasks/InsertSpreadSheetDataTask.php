<?php

namespace App\Containers\Statement\Tasks;

use App\Ship\Parents\Tasks\Task;
use Exception;
use App\Containers\Loan\Data\Transporters\CreateLoanTransporter;
use App\Containers\Interest\Data\Transporters\CreateInterestTransporter;
use Carbon\Carbon;
use Apiato\Core\Foundation\Facades\Apiato;
use App\Containers\Payment\Data\Transporters\CreateRefundTransporter;
use App\Containers\Loan\Models\Loan;

class InsertSpreadSheetDataTask extends Task
{
    public function run(array $sheetData)
    {
        // $lastLoanId = Loan::all()->last()->id + 1;
        $loan = $loanRequest = $interestRequest = [];
        $loanRequest['loan_type'] = 'manual';
        $loanRequest['status'] = 'approved';
        // $loanRequest['loan_id'] = $lastLoanId;
        $loanRequest['customer_id'] = 1;
        $loanRequest['holiday_refund'] = 1;
        $interestRequest['min_term'] = $interestRequest['monthly_payment'] = 0;
        
        \DB::beginTransaction();

        foreach ($sheetData as $key => $data) {
            // dd($data);
            $data7 = is_null($data[6]) ? $data[7] : $data[6];
            if($key === 0) {
                $loanExist = Loan::where('loan_id', '=', $data[2])->get();
                if(count($loanExist))
                    break;

                $loanRequest['loan_id'] = $data[2];
                $interestRequest['rate'] = is_null($data[4]) ? 1 : getNumber($data[4]);
                $interestRequest['default_rate'] = is_null($data[5]) ? 2 : getNumber($data[5]);
                $interestRequest['duration'] = is_null($data[5]) ? : getNumber($data7);
            } elseif($key === 3) {
                // dd($data[2], Carbon::parse($data[2])->format('Y-m-d'));
                $loanRequest['issue_at'] = Carbon::parse($data[2])->format('Y-m-d');
                $interestRequest['principal_amount'] = $interestRequest['gross_loan'] = $loanRequest['net_loan'] = getNumber($data[3]);
                
                $loan = Apiato::call('Loan@CreateLoanTask', [(new CreateLoanTransporter($loanRequest))->toArray()]);
                $interestRequest['loan_id'] = $loan->id;
                $interest = Apiato::call('interest@CreateInterestTask', [(new CreateInterestTransporter($interestRequest))->toArray()]);
                $dt                 = $loan->issue_at;
                $accountingPeriod   = "{$dt->format('M')} {$dt->year}";
                $journalPostingRequest = [
                    'creditAccountId' => 0,
                    'debitAccountId' => 1,
                    'type' => 'loan',
                    'loanid' => $loan->id,
                    'tnxDate' => $loan->issue_at,
                    'amount' => $loan->net_loan,
                    'accountingPeriod' => $accountingPeriod,
                    'assetType' => 'loan',
                    'description' => $data7,
                    'nominalCode' => 1,
                    'payableId' => $loan->id,
                    'payableType' => 'App\Containers\Loan\Models\Loan'
                ];
                $journal = Apiato::call('Payment@CreateJournalPostingTask', [$journalPostingRequest]);
                continue;
            } elseif($key > 3) {
                
                if(is_null($data[2]))
                    break;

                $tnxDate = Carbon::parse($data[2]);
                $amount = is_null($data[3]) ? $data[4] : $data[3];
                $assetType = is_null($data[3]) ? 'credit_filler' : 'debit_filler';
                $description = $data7;
                
                $refundRequest = [
                    'refund_at'         => $tnxDate,
                    'amount'            => getNumber($amount),
                    'loan_id'           => $loan->id,
                    'description'       => $description,
                    'nominalCode'       => 1,
                ];
                $refundArr = Apiato::call('Payment@CreateRefundTask', [(new CreateRefundTransporter($refundRequest))->toArray(), $loan, $assetType]);
                $refund = $refundArr['refund'];
            }
        }

        \DB::commit();

        return $loan;
    }
}
