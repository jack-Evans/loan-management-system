<?php
namespace App\Containers\Statement\Tasks;

use App\Ship\Parents\Tasks\Task;
use App\Containers\Loan\Models\Loan;
use Apiato\Core\Foundation\Facades\Apiato;
use Carbon\Carbon;
use App\Containers\Payment\Data\Repositories\PaymentRepository;
use App\Containers\Interest\Data\Repositories\InterestRepository;

class PaymentLateCheckTask extends Task
{
    protected $paymentRepository;    
    protected $interestRepository;
    protected $loanClosingDate;
    protected $monthlyPMT;
    protected $rate;
    protected $closingDate;

    public function __construct(PaymentRepository $paymentRepository, InterestRepository $interestRepository)
    {
        $this->paymentRepository = $paymentRepository;
        $this->interestRepository = $interestRepository;
    }
    public function run(Loan $loan, $startDate, $endDate)
    {
        $loanId = $loan->id;
        $this->rate = $loan->interest->rate;
        $this->monthlyPMT = round(calculateMonthlyPMT($this->rate, $loan->interest->gross_loan), 2);
        $this->closingDate = $loan->issue_at->copy()->addMonths($loan->interest->duration)->subDay();
        $paymentsPaid = $this->tillNowPaymentsSum($loanId, $startDate, $endDate);
        $paymentsRequired = $this->tillNowPaymentsRequired($loanId, $startDate, $endDate); 

        // if($loan->loan_type === 'retained'){
        //     // $retainLoanClosingDate = $loan->issue_at->copy()->addMonths($loan->interest->duration)->subDay();

        //     // if($endDate->gte($retainLoanClosingDate)) {
        //     if($endDate->gte($this->closingDate)) {
        //         $renewalCharges = Apiato::call('RenewalCharge@GetRenewalChargesByLoanIdTask', [$loanId]);
        //         $dateWithInDuration = $renewalCharges->where('renew_end_date', '>=', $endDate)->where('renew_date', '<=', $endDate)->first();
        //         if(count($renewalCharges) > 0 && $dateWithInDuration != null){
                    
        //             // var_dump($endDate->format('Y-m-d'));
        //             return false;
        //         }
        //         // dd($renewalCharges->where('renew_end_date', '>=', '2020-02-25')->where('renew_date', '<=', '2020-02-25')->first());
        //             // var_dump($endDate->format('Y-m-d'));
        //         // var_dump(count($renewalCharges->where('renew_end_date', '>', $endDate->format('Y m d'))->where('renew_date', '<=', $endDate->format('Y m d'))), $endDate->format('y m d'));
        //         // dd(count($renewalCharges), $renewalCharges->where('renew_end_date', '>', '2020-02-25')->where('renew_date', '<=', '2020-02-25')->first());
        //         return true;
        //     }
        //     return false;
        // }
        if($this->closingDate->lte($endDate)){
            $renewalCharges = Apiato::call('RenewalCharge@GetRenewalChargesByLoanIdTask', [$loanId]);
            $dateWithInDuration = $renewalCharges->where('renew_end_date', '>=', $endDate)->where('renew_date', '<=', $endDate)->first();
            if(count($renewalCharges) > 0 && $dateWithInDuration != null){
                if(floatgt($paymentsRequired, $paymentsPaid))
                    return true;
                else
                    return false;
            }
            return true;
        }
        elseif(floatgt($paymentsRequired, $paymentsPaid) && $loan->loan_type === 'serviced')
            return true;
        else
            return false;
    }
    public function tillNowPaymentsSum($loanId, $startDate, $endDate)
    {
        $interest = $this->interestRepository->scopeQuery(function($q) use($endDate, $loanId){
            return $q->where('payment_plan_date', '<=', $endDate)->where('loan_id', $loanId);
        })->get()->last();
        if($interest)            
            $startDate = Carbon::parse($interest->payment_plan_date)->addDay();        

        return $this->paymentRepository->scopeQuery(function($q) use($startDate, $endDate) {
                    return $q->whereBetween('paid_at', [$startDate, $endDate]);
                })->findWhere(['loan_id' => $loanId])->where('is_capital_reduction', '=', false)->sum('amount');
    }
    
    public function tillNowPaymentsRequired($loanId, $startDate, $endDate)
    {
        $interest = $this->interestRepository->scopeQuery(function($q) use($endDate, $loanId){
            return $q->where('payment_plan_date', '<=', $endDate)->where('loan_id', $loanId);
        })->get()->last();
        if($interest){
            $this->monthlyPMT = round(calculateMonthlyPMT($this->rate, $interest->gross_loan), 2);
            $startDate = Carbon::parse($interest->payment_plan_date)->addDay();

            $dueDay = $this->closingDate->format('d');
            $pmtPlanDay = $startDate->format('d');
            if($dueDay >= $pmtPlanDay){
                $addDays = 0;
                if($endDate->gt($startDate))
                    $addDays++;
                $diffInMonths = $startDate->copy()->diffInMonths($endDate->copy()->addDay()) + $addDays;
            } else {
                $diffInMonths = $startDate->copy()->diffInMonths($endDate->copy()->addDay());
                // In case of new pmt plan first month is calculated this way.
                $pmtPlanMonth = $startDate->copy()->format('m');
                $endDateMonth = $endDate->copy()->format('m');
                $endDateGTETnxDate = $endDate->copy()->format('d') >= $this->closingDate->format('d');
                $startDateLTETnxDate = $startDate->copy()->addDay()->format('d') <= $this->closingDate->format('d');
                if( (($endDateMonth - $pmtPlanMonth) > $diffInMonths) && ($endDateGTETnxDate) )
                    $diffInMonths++;
                elseif( ( ($endDateMonth == $pmtPlanMonth) ) && $endDateGTETnxDate && $startDateLTETnxDate )
                    $diffInMonths++;
            }
        }

        else
            $diffInMonths = $startDate->copy()->diffInMonths($endDate->copy()->addDay());
        $pmtRequired = $this->monthlyPMT * $diffInMonths;
        // var_dump($startDate, $endDate);
        return round($pmtRequired, 2);
    }
    
}