<?php

namespace App\Containers\Statement\Tasks;

use App\Ship\Parents\Tasks\Task;
use Apiato\Core\Traits\ResponseTrait;
use Apiato\Core\Foundation\Facades\Apiato;
use App\Containers\Loan\Models\Loan;
use Carbon\Carbon;

class CalculateNTranchInterestTask extends Task
{
	public function run(Loan $loan, $balance, $startDate, $endDate)
	{
		$normalRate = $loan->interest->rate;
        $defaultRate = $loan->interest->default_rate;
        
        $dailyRate1 = $dailyRate = calculateDailyRate($normalRate, $balance, $loan);
        $dailyRate = round($dailyRate, 8);

        $diffInDays = $startDate->diffInDays($endDate) + 1;
        $debitInterestAmount = round($dailyRate * $diffInDays, 8);

		$breakDown[] = ['dailyRate' => $dailyRate, 'days' => $diffInDays, 'balance' => $balance, 'rate' => $normalRate, 'total' => $debitInterestAmount, 'start' => $startDate, 'end' => $endDate->copy(), 'rounding_error' => $debitInterestAmount - (round($dailyRate1, 8)*($diffInDays))];
		return $breakDown;
	}

}