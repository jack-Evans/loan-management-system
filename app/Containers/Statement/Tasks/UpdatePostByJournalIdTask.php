<?php

namespace App\Containers\Statement\Tasks;

use App\Containers\Payment\Data\Repositories\JournalRepository;
use App\Containers\Payment\Data\Repositories\PostRepository;
use App\Ship\Exceptions\UpdateResourceFailedException;
use App\Ship\Parents\Tasks\Task;
use Exception;

class UpdatePostByJournalIdTask extends Task
{

    protected $journalRepository;
    protected $postRepository;

    public function __construct(JournalRepository $journalRepository, PostRepository $postRepository)
    {
        $this->journalRepository = $journalRepository;
        $this->postRepository   = $postRepository;
    }

    public function run($id, array $data)
    {
        try {
            $journal = $this->journalRepository->with('posts')->find($id);
            foreach ($journal->posts as $post) {
                $this->postRepository->update($data, $post->id);
            }
        }
        catch (Exception $exception) {
            throw new UpdateResourceFailedException();
        }
    }
}
