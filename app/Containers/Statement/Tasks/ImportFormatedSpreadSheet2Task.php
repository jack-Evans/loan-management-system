<?php

namespace App\Containers\Statement\Tasks;

use App\Ship\Parents\Tasks\Task;
use Exception;
use Carbon\Carbon;
use Apiato\Core\Foundation\Facades\Apiato;
use App\Containers\Loan\Models\Loan;
use App\Containers\NominalCode\Models\NominalCode;
use App\Containers\OtherCharge\Models\OtherCharge;
use App\Containers\Payment\Models\Payment;
use App\Containers\RenewalCharge\Models\RenewalCharge;
use App\Containers\LoanCharge\Models\LoanCharge;
use App\Containers\Charge\Models\Charge;
use App\Containers\Payment\Models\Refund;
use App\Containers\Interest\Models\Interest;
use PhpOffice\PhpSpreadsheet\Shared\Date;
use App\Containers\Optionalvariable\Models\Optionalvariable;
// use Apiato\Core\Traits\HashIdTrait;

class ImportFormatedSpreadSheet2Task extends Task
{
    // use HashIdTrait;

    protected $errors;
    protected $pmtPlans;

    public function run(array $sheetData, $request = [], $sheetName)
    {
        if(isset($sheetData[0][3]) && $sheetData[0][3] == 1)
        {
            $request->sheetName = $sheetName;
            $request->sheetData = json_encode($sheetData);
            $ignoredSheet = Apiato::call('LoanExtras@ManageIgnoredSheetsAction', [$request]);

            $this->errors['message'][] = "Ignored Sheet info saved successfully.warning";

            return $this->errors;
        }

        // $lastLoanId = Loan::all()->last()->id + 1;
        $this->errors = [];
        
        // \DB::beginTransaction();
        // start array from 1 instead of 0.
        if(count($sheetData) <= 14){
            $this->errors['message'][] = "Empty Sheet.error";
            return $this->errors;
        }
        array_unshift($sheetData, '');
        unset($sheetData[0]);
        // get number duration from string. ie 6 months

        $loanId = $optionKey = null;
        $request->loanId = trim($sheetData[4][1]);
        // $request->loanId = rtrim($sheetData[4][1]);
        if($request->loanId == '')
            $request->loanId = $sheetData[4][1];
        $loan = Loan::where(\DB::raw("LOWER(loan_id)"), '=', trimedLowerStr($request->loanId))->first();
        if($loan)
            $loanId = $loan->id;
        // remove first 14 rows and start afterwards
        // array_splice($sheetData, 0, 14);
        // $rowNumber = 14;
        $rowNumber = $fileTotalInterest = $fileDefaultInterest = $noOfTransactions = 0;
        // $notesKey = null;

        $options = $this->pmtPlans = $renewals = $grossloanTranch = $graces = [];
        foreach ($sheetData as $key => $d) {
            // Loan info gethering
            $journalStatements = $journalTags = $notes = $tnxId = null;
            if(is_null($loanId)) { 
                $d[0] = trimedLowerStr($d[0]);
                if(strcmp($d[0], 'sheet number') === 0)
                    $request->sheetNo = $d[1];
                elseif(strcmp($d[0], 'name') === 0){
                    $request->loanId = trim(str_replace('-', '', strstr($d[1], '-')));
                    if($request->loanId == '')
                        $request->loanId = $d[1];
                }
                elseif(strcmp($d[0], 'loan term') === 0){
                    preg_match_all('!\d+!', $d[1], $duration);
                    $request->duration = isset($duration[0][0]) ? $duration[0][0] : null;
                }
                elseif(strcmp($d[0], 'retained loan duration') === 0){
                    preg_match_all('!\d+!', $d[1], $duration2);
                    $request->duration2 = isset($duration2[0][0]) ? $duration2[0][0] : null;
                }
                elseif(strcmp($d[0], 'loan start date') === 0)
                    $request->issueAt = ($this->getDate($d[1])) ? $this->getDate($d[1])->format('Y-m-d') : false;
                elseif(strcmp($d[0], 'rate') === 0)
                    $request->rate = (float)str_replace( '%', '', $d[1]);
                elseif(strcmp($d[0], 'default rate') === 0)
                    $request->defaultRate = (float)str_replace( '%', '', $d[1]);
                elseif(strcmp($d[0], 'interest') === 0){
                    $request->loanType = trimedLowerStr($d[1]);
                    if(!in_array($request->loanType, array('serviced', 'retained', 'half', 'tranch', 'n-tranch', 'rolled', 'manual'))){
                        $request->isImport = 0;
                        $this->errors['message'][] = "Incorrect loan type '$request->loanType'. Only 'serviced', 'retained', 'half', 'tranch', 'n-tranch', 'rolled', 'manual' loan types are importable at the moment.error";
                        return $this->errors;
                    }
                }
                elseif(strcmp($d[0], 'holiday auto refund') === 0)
                    $request->holidayRefund = (strcmp(trimedLowerStr($d[1]), 'yes') == 0) ? 1 : 0;
                elseif(strcmp($d[0], 'gross loan') === 0){
                    $request->grossloan = filter_var($d[1], FILTER_SANITIZE_NUMBER_FLOAT, FILTER_FLAG_ALLOW_FRACTION);
                }
                elseif(strpos(trimedLowerStr($d[0]), 'gross loan') !== false){
                    $grossloanTranch[] = filter_var($d[1], FILTER_SANITIZE_NUMBER_FLOAT, FILTER_FLAG_ALLOW_FRACTION);
                }
            }

            $d[5] = trimedLowerStr($d[5]);
            $rowNumber++;
            // if date is null its not a valid transaction, continue to next iteration.
            if(is_null($d[0]) || strcmp($d[5], 'type') === 0)
                continue;

            $tnxType = ucfirst(str_replace( '_', ' ', $d[5]));
            $tnxDateCarbon = $this->getDate($d[0]);
            $tnxAmount = is_null($d[1]) ? $d[2] : $d[1];
            $tnxAmount = filter_var($tnxAmount, FILTER_SANITIZE_NUMBER_FLOAT, FILTER_FLAG_ALLOW_FRACTION);

            if($tnxDateCarbon)
                $tnxDate = $tnxDateCarbon->format('Y-m-d');
            // if tnx type is there and we also have tnx date.(transaction seems to be fine)
            if(!empty($d[5]) && $tnxDateCarbon){
                $lastTnxDateCarbon = $tnxDateCarbon;
                $lastTnxDate = $tnxDate;
                $fileLastBalance = filter_var($d[3], FILTER_SANITIZE_NUMBER_FLOAT, FILTER_FLAG_ALLOW_FRACTION);
                
                $journalStatements = isset($d[9]) ? trimedLowerStr($d[9]) : null;
                $notes = isset($d[11]) ? $d[11] : null;
                $journalTags = isset($d[10]) ? $d[10] : null;
            }
            // if(strcmp($d[5], 'interest') === 0 || !is_null($d[7]) && strcmp($d[5], 'loan') !== 0){
            // if(!is_null($d[7]) && strcmp($d[5], 'loan') !== 0){
            if( (strcmp($d[5], 'interest') === 0 && (isset($loan) && $loan->loan_type != 'manual' ) || (isset($d[8]) && !is_null($d[8]))) && $loanId ) {
                if(isset($d[8]) && !is_null($d[8]))
                    $this->errors['message'][] = "Row $rowNumber: "."$tnxType transaction already created.info";
                else
                    $this->errors['message'][] = "Row $rowNumber: "."$tnxType transaction.info";
                if(strcmp($d[5], 'interest') === 0) {
                    $fileTotalInterest += $tnxAmount;
                    if($d[6] === 4001)
                        $fileDefaultInterest += $tnxAmount;
                    // $this->errors['message'][] = "Row $rowNumber: "."$tnxType transaction.info";
                    $noOfTransactions++;
                }
                
                if($tnxDateCarbon && count($this->pmtPlans)){
                    $pmtPlanDt = $this->getDate($this->pmtPlans[0][1]);
                    if($pmtPlanDt && $tnxDateCarbon->gt($pmtPlanDt))
                        $this->addPaymentPlan($request, $tnxDateCarbon, $loanId);
                }

                continue;
            }
            if(!$tnxDateCarbon){
                if(strpos(trimedLowerStr($d[0]), 'renewal') !== false && strpos(trimedLowerStr($d[0]), 'renewal fee') === false)
                    $renewals[] = $d;
                elseif(strpos(trimedLowerStr($d[0]), 'payment plan date') !== false || strpos(trimedLowerStr($d[0]), 'payment plan term') !== false)
                    $this->pmtPlans[] = $d;
                elseif(strpos(trimedLowerStr($d[0]), 'grace') !== false)
                    $graces[] = $d;
                elseif(strcmp(trimedLowerStr($d[0]), 'notes') === 0 || strcmp(trimedLowerStr($d[0]), 'breakdown') === 0 || strcmp(trimedLowerStr($d[0]), 'optional variable') === 0) {
                    $breakdowns = array_splice($sheetData, $key, count($sheetData)-1);
                    $this->addLoanExtras($request, $breakdowns, $loanId);
                    if(count($this->pmtPlans))
                        $this->addPaymentPlan($request, $tnxDateCarbon, $loanId);
                    break;
                }
                continue;
            }
            if(in_array($d[5], array('loan', 'charge', 'other_charge', 'interest', 'renewal_charge', 'debit_filler')) && is_null($d[1]) && !is_null($d[2])){
                $request->isImport = 0;
                $this->errors['message'][] = "Row $rowNumber: $tnxType is a debit transaction but amount is representing credit field.error";
            }
            elseif(in_array($d[5], array('payment', 'capreduction', 'transfer', 'refund', 'other_charge_refund', 'credit_filler')) && is_null($d[2]) && !is_null($d[1])){
                $request->isImport = 0;
                $this->errors['message'][] = "Row $rowNumber: $tnxType is a credit transaction but amount is representing debit field.error";
            }

            $noOfTransactions++;
            $nominalCodeId = $this->getNominalCode($d[6], $d[4]);
            if(strcmp($d[5], 'loan') === 0 && is_null($loanId)){
                if(is_null($loan)){
                    $request->netloan = $tnxAmount;
                    $request->description = $d[4];
                    $request->holidayRefund = 0;
                    $request->customerId = 1;
                    $request->status = 'processing';
                    // $request->duration2 = null;
                    $request->minTerm = 0;
                    
                    if(in_array($request->loanType, ['retained', 'half']) && (is_null($request->grossloan) || $request->grossloan == 0)) {
                        $this->errors['message'][] = "Gross loan is required for $request->loanType loan.error";
                        break;
                    }
                    
                    try{
                        $loan = Apiato::call('Loan@CreateLoanAction', [$request, $nonExcelRequest = false]);
                        $tnxId = $loan->journal->id;
                        $loanId = $loan->id;
                        $this->errors['message'][] = "Row $rowNumber: Loan created successfully.info";
                    } catch (Exception $e) {
                        $request->isImport = 0;
                        $this->errors['message'][] = "Row $rowNumber: ".$e->getMessage()."(incorrect loan data).error";
                        break;
                    }
                } else {
                    $tnxId = $loan->journal->id;
                    $loanId = $loan->id;
                    $this->errors['message'][] = "Row $rowNumber: Loan already created.info";
                }
                $this->updateTransaction($request, $tnxId, $journalStatements, $notes, $journalTags, $rowNumber);
                if(count($graces)) {
                    $this->addGraceInfo($loanId, $graces, $request);
                    $graces = [];
                }
                continue;
            } if(is_null($loanId)) {
                $request->isImport = 0;
                $this->errors['message'][] = "Row $rowNumber: An un-expected error occurred. Please try fixing loan data.error";
                break;
            } elseif(strcmp($d[5], 'charge') === 0){
                $initialCharge = Charge::where(\DB::raw("LOWER(name)"), 'like', "%".trimedLowerStr($d[4])."%")->first();
                if(is_null($initialCharge)) {
                    $request->name          = $d[4];
                    $request->value         = $tnxAmount;
                    $request->amounttype    = 'netloan';
                    $request->chargetype    = 'fixed';
                    try{
                        $initialCharge = Apiato::call('Charge@CreateChargeAction', [$request]);
                    } catch (Exception $e) {
                        $request->isImport = 0;
                        $this->errors['message'][] = "Row $rowNumber: ".$e->getMessage()."(unable to create initial charge).error";
                        break;
                    }
                }
                $request->loanid        = $loanId;
                $request->chargeid      = $initialCharge->id;
                $request->value         = $tnxAmount;
                $request->nominalCode   = $nominalCodeId;
                try{
                    $loanCharge = Apiato::call('LoanCharge@CreateLoanChargeAction', [$request, $excelRequest = true]);
                    $tnxId = $loanCharge->journal->id;
                    $this->errors['message'][] = "Row $rowNumber: $tnxType transaction created successfully.info";
                } catch (Exception $e) {
                    $request->isImport = 0;
                    $this->errors['message'][] = "Row $rowNumber: ".$e->getMessage()."error";
                    break;
                }
            } elseif(strcmp($d[5], 'other_charge') === 0) {
                $request->chargedate    = $tnxDate;
                $request->loanid        = $loanId;
                $request->name          = $d[4];
                $request->value         = $tnxAmount;
                $request->amounttype    = 'netloan';
                $request->chargetype    = 'fixed';
                $request->nominalCode   = $nominalCodeId;
                try{
                    $otherCharge = Apiato::call('OtherCharge@CreateOtherChargeAction', [$request]);
                    $tnxId = $otherCharge->journal->id;
                    $this->errors['message'][] = "Row $rowNumber: $tnxType transaction created successfully.info";
                } catch (Exception $e) {
                    $request->isImport = 0;
                    $this->errors['message'][] = "Row $rowNumber: ".$e->getMessage()."error";
                    break;
                }
            } elseif(strcmp($d[5], 'payment') === 0 || strcmp($d[5], 'capreduction') === 0 || strcmp($d[5], 'transfer') === 0) {
                $request->paidAt            = $tnxDate;
                $request->amount            = $tnxAmount;
                $request->loanId            = $loanId;
                $request->description       = $d[4];
                $request->isCapitalReduction= (strcmp($d[5], 'capreduction') === 0) ? 1 : 0;
                $request->nominalCode       = $nominalCodeId;
                try{
                    $payment = Apiato::call('Payment@CreatePaymentAction', [$request]);
                    $tnxId = $payment->journal->id;
                    $this->errors['message'][] = "Row $rowNumber: $tnxType transaction created successfully.info";
                } catch (Exception $e) {
                    $request->isImport = 0;
                    $this->errors['message'][] = "Row $rowNumber: ".$e->getMessage()."error";
                    break;
                }
            // } elseif(strcmp($d[5], 'refund')  === 0 || strcmp($d[5], 'debit_filler') === 0 || strcmp($d[5], 'credit_filler')  === 0) {
            } elseif(strcmp($d[5], 'debit_filler') === 0 || strcmp($d[5], 'credit_filler')  === 0) {
                $request->refundAt = $tnxDate;
                $request->amount = $tnxAmount;
                $request->loanId = $loanId;
                $request->description = $d[4];
                $request->nominalCode = $nominalCodeId;
                if(strcmp($d[5], 'refund')  != 0)
                    $request->assetType = (strcmp($d[5], 'debit_filler')  === 0) ? 'debit_filler' : 'credit_filler';
                try{
                    $refund = Apiato::call('Payment@CreateRefundAction', [$request]);
                    $tnxId = $refund->journal->id;
                    $this->errors['message'][] = "Row $rowNumber: $tnxType transaction created successfully.info";
                } catch (Exception $e) {
                    $request->isImport = 0;
                    $this->errors['message'][] = "Row $rowNumber: ".$e->getMessage()."error";
                    break;
                }
            } elseif(strcmp($d[5], 'renewal_charge')  === 0) {
                $currentArr = array_splice($renewals, 0, 4);
                if(!isset($currentArr[1][1])){
                    $request->isImport = 0;
                    $this->errors['message'][] = "Row $rowNumber: Renewal charge information is missing.error";
                    break;
                }
                foreach ($currentArr as $renews) {
                    foreach($renews as $rnKey => $renew){
                        if(strpos(trimedLowerStr($renew), 'renewal term') !== false)
                            preg_match_all('!\d+!', $renews[$rnKey+1], $renewDuration);
                    }
                }

                $request->loanId = $loanId;
                $request->duration = isset($renewDuration[0][0]) ? $renewDuration[0][0] : null;
                $request->value = $tnxAmount;
                $request->description = $d[4];
                $request->nominalCode = $nominalCodeId;
                $request->renewDate = $tnxDate;
                try{
                    $renewalCharge = Apiato::call('RenewalCharge@CreateRenewalChargeAction', [$request]);
                    $tnxId = $renewalCharge->journal->id;
                    $this->errors['message'][] = "Row $rowNumber: $tnxType transaction created successfully.info";
                } catch (Exception $e) {
                    $request->isImport = 0;
                    $this->errors['message'][] = "Row $rowNumber: ".$e->getMessage()."error";
                    break;
                }
            } elseif(strcmp($d[5], 'tranch')  === 0 || (strcmp($d[5], 'loan') === 0 && !is_null($loanId)) ) {
                $request->loanid        = $loanId;
                $request->amount        = $tnxAmount;
                $request->tranchDate    = $tnxDate;
                $request->description   = $d[4];
                $request->nominalCode   = $nominalCodeId;
                if(trimedLowerStr($loan->loan_type) != 'rolled')
                    $request->grossLoan     = isset($grossloanTranch[0]) ? $grossloanTranch[0] : $loan->interest->gross_loan;
                
                try{
                    $tranch = Apiato::call('Tranch@CreateTranchAction', [$request]);
                    $tnxId = $tranch->journal->id;
                    $this->errors['message'][] = "Row $rowNumber: Tranch transaction created successfully.info";
                } catch (Exception $e) {
                    $request->isImport = 0;
                    $this->errors['message'][] = "Row $rowNumber: ".$e->getMessage()."error";
                    break;
                }
                array_splice($grossloanTranch, 0, 1);
            } elseif(strcmp($d[5], 'refund_default_interest') == 0 || strcmp($d[5], 'other_charge_refund') == 0 || strcmp($d[5], 'refund') == 0) {
                $request->refundAt = $tnxDate;
                $request->amount = $tnxAmount;
                $request->loanId = $loanId;
                $request->description = $d[4];
                $request->nominalCode = $nominalCodeId;
                $request->assetType = $d[5];
                try{
                    $refundOtherCharge = Apiato::call('Payment@CreateRefundAction', [$request]);
                    $tnxId = $refundOtherCharge->journal->id;
                    $this->errors['message'][] = "Row $rowNumber: $d[5] transaction created successfully.info";
                } catch (Exception $e) {
                    $request->isImport = 0;
                    $this->errors['message'][] = "Row $rowNumber: ".$e->getMessage()."error";
                    break;
                }
            } elseif(strcmp($d[5], 'interest') == 0) {
                $journalPosting = [
                    'creditAccountId' => 1,
                    'debitAccountId' => 0,
                    'loanid' => $loanId,
                    'tnxDate' => $tnxDate,
                    'amount' => $tnxAmount,
                    'accountingPeriod' => $tnxDate,
                    'type' => 'interest',
                    'assetType' => 'interest',
                    'description' => $d[4],
                    'nominalCode' => $nominalCodeId,
                    'payableId' => 0,
                    'payableType' => ''
                ];
                try{
                    $journalEntry = Apiato::call('Payment@CreateJournalPostingTask', [$journalPosting]);
                    $tnxId = $journalEntry->id;
                    $this->errors['message'][] = "Row $rowNumber: $tnxType transaction created successfully.info";
                } catch (Exception $e) {
                    $request->isImport = 0;
                    $this->errors['message'][] = "Row $rowNumber: ".$e->getMessage()."error";
                    break;
                }
            } else{
                $this->errors['message'][] = "Row $rowNumber: $tnxType unrecognised transaction.warning";
            }

            if(count($this->pmtPlans)) {
                $pmtPlanDt = $this->getDate($this->pmtPlans[0][1]);
                if($pmtPlanDt && $tnxDateCarbon->gt($pmtPlanDt))
                    $this->addPaymentPlan($request, $tnxDateCarbon, $loanId);
            }

            $this->updateTransaction($request, $tnxId, $journalStatements, $notes, $journalTags, $rowNumber);

        }
        // file import info
        if($loanId){
            try{
                $request->loanid = $loanId;
                $this->addFileInfo($request, $noOfTransactions, $lastTnxDate, $lastTnxDateCarbon, $tnxAmount, $fileLastBalance, $fileTotalInterest, $fileDefaultInterest, $loan);
            } catch (Exception $e) {
                $request->isImport = 0;
                $this->errors['message'][] = "Error saving file info. ".$e->getMessage().".error";
            }
        }
        return $this->errors;
    }

    public function getNominalCode($nominalCode, $description)
    {
        // $code = NominalCode::get()->first();
        $code = NominalCode::where('code', $nominalCode)->first();
        // if(is_null($code) && !is_null($nominalCode)) {
        //     $data = [
        //         'code' => $nominalCode,
        //         'name' => $description,
        //     ];
        //     try{
        //         $code = Apiato::call('NominalCode@CreateNominalCodeTask', [$data]);
        //         $this->errors['message'][] = "Nominal code '$nominalCode' added successfully.info";
        //     } catch (Exception $e) {
        //         $this->errors['message'][] = "Nominal code '$nominalCode' create error: ".$e->getMessage()."error";
        //     }
        // }
        // if(!isset($code->id))
        //     $code = NominalCode::get()->first();
        if($code)
            return $code->id;
        else{
            $this->errors['message'][] = "Nominal code '$nominalCode' didn't found.info";
            $codeDefault = NominalCode::get()->first();
            return $codeDefault->id;
        }
    }

    // public function getGrossLoanAmount($sheetData)
    // {
    //     foreach ($sheetData as $key => $data) {
    //         if(trimedLowerStr($data[4]) == 'retained interest')
    //             return str_replace( ',', '', $data[3]);
    //         else
    //             continue;
    //     }
    //     return 0;
    // }

    public function getDate($date, $format = 'd/m/Y')
    {
        $d = \DateTime::createFromFormat($format, $date);
        // The Y ( 4 digits year ) returns TRUE for any integer with any number of digits so changing the comparison from == to === fixes the issue.
        $format = $d && $d->format($format) === $date;
        if($d){
            // try{
            //     $dateObj = new Carbon($date);
            // } catch(Exception $e){
                $dateArr = explode('/', (string)$date);
                // if($format){
                //     try{
                //         $dateObj = Carbon::parse($dateArr[0].'-'.$dateArr[1].'-'.$dateArr[2]);
                //     } catch(Exception $e){
                //         try{
                //             $dateObj = Carbon::parse($dateArr[0].'-'.$dateArr[1].'-'.$dateArr[2]);
                //         } catch(Exception $e){
                //             $dateObj = false;
                //         }
                //         $dateObj = false;
                //     }
                // }
                // else{
                    try{
                        $dateObj = Carbon::parse($dateArr[1].'-'.$dateArr[0].'-'.$dateArr[2]);
                    } catch(Exception $e){
                        try{
                            $dateObj = Carbon::parse($dateArr[0].'-'.$dateArr[1].'-'.$dateArr[2]);
                        } catch(Exception $e){
                            $dateObj = false;
                        }
                        $dateObj = false;
                    }
                // }
                // var_dump($date, $format, $dateObj);
            // }
        }
        else
            return false;

        return $dateObj;
    }

    public function addLoanExtras($request, $sheetData, $loanId)
    {
        if(is_null($loanId)){
            $request->isImport = 0;
            $this->errors['message'][] = "An un-expected error occurred. Please try fixing loan data before entring loan extras.error";
            return false;
        }
        // array_unshift($sheetData, '');
        // unset($sheetData[0]);
        // dd($sheetData);
        $notesKey = $brkdnKey = $optionKey = null;
        $notes = $breakdowns = $options = null;
        $request->loanid = $loanId;
        foreach ($sheetData as $key => $d) {
            if(strcmp(trimedLowerStr($d[0]), 'notes') === 0)
                $notesKey = $key;

            if(strcmp(trimedLowerStr($d[0]), 'breakdown') === 0)
                $brkdnKey = $key;

            if(strcmp(trimedLowerStr($d[0]), 'optional variable') === 0)
                $optionKey = $key;

            if( (strcmp(trimedLowerStr($d[0]), 'breakdown') === 0 || strcmp(trimedLowerStr($d[0]), 'optional variable') === 0 || $key === count($sheetData)-1 ) && isset($notesKey)) {
                $endKey = $key - $notesKey;
                $notes = array_slice($sheetData, $notesKey, $endKey);
                $notesKey = null;
            }

            if((strcmp(trimedLowerStr($d[0]), 'optional variable') === 0 || $key === count($sheetData)-1 ) && isset($brkdnKey)) {
                $endKey = $key - $brkdnKey;
                $breakdowns = array_slice($sheetData, $brkdnKey, $endKey);
                $brkdnKey = null;
            }

            if($key === count($sheetData)-1 && isset($optionKey)) {
                $endKey = $key - $optionKey;
                $options = array_slice($sheetData, $optionKey+1, $endKey);
                $optionKey = null;
            }

            // elseif((strcmp(trimedLowerStr($d[0]), 'breakdown') === 0) || $key === (count($sheetData) - 1) ) {
            // elseif(strcmp(trimedLowerStr($d[0]), 'breakdown') === 0) {
            //     if($notesKey || $key === (count($sheetData) - 1) ) {
            //         $notes = array_splice($sheetData, 0, $key);
            //         $request->note = json_encode($notes);
            //     }

            //     if((strcmp(trimedLowerStr($d[0]), 'breakdown') === 0)) {
            //         $breakdowns = array_splice($sheetData, 0, count($sheetData));
            //         $request->breakdown = json_encode($breakdowns);
            //         $hasLoanExtras = true;
            //         // break;
            //     }
            // } elseif(strcmp(trimedLowerStr($d[0]), 'optional variable') === 0) {
            //     $breakdowns = array_splice($sheetData, 0, count($sheetData));
            //     $request->breakdown = json_encode($breakdowns);
            //     $optionalVariables = true;
            // }
        }
        // dd($notes, $breakdowns, $options);
        if($notes || $breakdowns){
            $request->note = isset($notes) ? json_encode($notes) : null;
            $request->breakdown = isset($breakdowns) ? json_encode($breakdowns) : null;
            try{
                $extras = Apiato::call('LoanExtras@CreateLoanExtrasAction', [$request]);
                $this->errors['message'][] = "Loan extras has been saved successfully.info";
            } catch (Exception $e) {
                $request->isImport = 0;
                $this->errors['message'][] = "Error creating loan extras. ".$e->getMessage().".error";
            }
        } else
            $this->errors['message'][] = "No loan extras found.info";
        if($options)
            $this->addOptions($request, $options, $loanId);
        // return $message;
    }

    public function addPaymentPlan($request, $tnxDateCarbon, $loanId) {
        // if($this->pmtPlans){
            $pmtPlanDt = $this->getDate($this->pmtPlans[0][1]);
        //     // dd($pmtPlanDt, $tnxDateCarbon, $this->pmtPlans);
        //     if($pmtPlanDt && $tnxDateCarbon->gte($pmtPlanDt)){
                $pmtPlanDtStr = $pmtPlanDt->format('Y-m-d');
                $interest = Interest::whereRaw("loan_id = $loanId and payment_plan_date = '$pmtPlanDtStr'")->first();
                if(is_null($interest)){
                    $request->loanId = $loanId;
                    $request->date = $pmtPlanDt;
                    try{
                        $interest = Apiato::call('Payment@CreateInterestForPaymentPlanAction', [$request, $this->pmtPlans[1][1]]);
                        $this->errors['message'][] = "Payment plan at ".$pmtPlanDt->format('Y-m-d')." created successfully.info";
                    } catch (Exception $e) {
                        $request->isImport = 0;
                        $this->errors['message'][] = "Error creating payment plan at ".$pmtPlanDt->format('Y-m-d')." ".$e->getMessage()."error";
                    }
                } else {
                    $request->isImport = 0;
                    $this->errors['message'][] = "Payment plan at ".$pmtPlanDt->format('Y-m-d')." already created.error";
                }
                array_splice($this->pmtPlans, 0, 2);
        //     }
        // }
    }

    public function addOptions($request, $options, $loanId){
        $request->loanid = $loanId;
        foreach ($options as $key => $d) {
            if(!is_null($d[0])){
                $option = \App\Containers\Option\Models\Option::whereRaw("loan_id = $loanId")->where(\DB::raw("LOWER(name)"), 'like', "%".trimedLowerStr($d[0])."%")->first();
                if(!$option && $d[0]) {
                    $d[0] = trimedLowerStr($d[0]);
                    $request->name      = $d[0];
                    $request->value     = $d[1];
                    $request->createDate = ($this->getDate($d[2])) ? ($this->getDate($d[2]))->format('Y-m-d') : null;
                    try{
                        $option = Apiato::call('Option@CreateOptionAction', [$request]);
                        // $this->errors['message'][] = "Option '".$d[0]."' created successfully.info";
                    } catch (Exception $e) {
                        $request->isImport = 0;
                        $this->errors['message'][] = $e->getMessage()."error";
                    }
                } 
                $optionalVariables = Optionalvariable::where('optional_variable', 'LIKE', "{$option->name}")->first();
                if($optionalVariables->mapped)
                   $this->errors['message'][] = "Option '".$option->name."' is mapped to other optional variables. Provide same values for other variables too.warning";
            } 
            // else 
            //     $this->errors['message'][] = "Option '".$d[0]."' already created.";
        }
        // $this->errors['message'][] = "Loan options has been saved successfully.info";
    }

    public function addFileInfo($request, $noOfTransactions, $lastTnxDate, $lastTnxDateCarbon, $tnxAmount, $fileLastBalance, $fileTotalInterest, $fileDefaultInterest, $loan) {
        $requestDate = $lastTnxDate;
        if($loan->loan_type === 'retained' && $lastTnxDateCarbon->lte(addDecimalMonthsIntoDate($loan->interest->duration, $loan->issue_at)))
            $requestDate = addDecimalMonthsIntoDate($loan->interest->duration, $loan->issue_at)->copy()->subDay()->format('Y-m-d');
        $request->date = $requestDate;
        $statements = Apiato::call('Payment@GetAllStatementsAction', [$request]);
        $firstStatement = $statements->first();
        $lastStatement = $statements->last();

        $request->type = 'file';
        $request->createDate = Carbon::now()->format('Y-m-d');
        $request->noOfTransactions = $noOfTransactions;
        $request->tnxFirstDate = $loan->issue_at->format('Y-m-d');
        $request->tnxLastDate = $lastTnxDate;
        $request->endBalance = $fileLastBalance;
        $request->calculationDifference = round($lastStatement->balance - (float)$fileLastBalance, 2);
        $request->interestDays = 0;
        $request->totalInterest = $fileTotalInterest;
        $request->defaultInterest = $fileDefaultInterest;
        $request->defaultInterestDays = 0;
        try{
            $fileInfo = Apiato::call('FileInfo@CreateFileInfoAction', [$request]);
            $this->errors['message'][] = "File info saved successfully. Calculation difference: $request->calculationDifference.warning";
        } catch (Exception $e) {
            $request->isImport = 0;
            $this->errors['message'][] = "File info: ".$e->getMessage()."error";
        }

        $request->type = 'system';
        $request->noOfTransactions = count($statements);
        $request->tnxLastDate = $lastStatement->created_at->format('Y-m-d');
        $request->endBalance = $lastStatement->balance;
        $totalMonthlyInterests = $statements->where('description', '=', 'interest')->all();
        $grossInterest = collect($totalMonthlyInterests)->sum('debit');
        $defaultRateInterest = $defaultRateDays = 0;
        foreach ($totalMonthlyInterests as $totalMonthlyInterest) {
            $defaultRateTnx = collect($totalMonthlyInterest->interestBreakDown)->where('rate', $loan->interest->default_rate)->where('total', '>', 0)->all();
            if($defaultRateTnx){
                $defaultRateInterest += collect($defaultRateTnx)->sum('total');
                $defaultRateDays += collect($defaultRateTnx)->sum('days');
            }
        }
        $request->totalInterest = $grossInterest;
        $request->interestDays = $firstStatement->created_at->diffInDays($lastStatement->created_at)+1;
        $request->defaultInterest = $defaultRateInterest;
        $request->defaultInterestDays = $defaultRateDays;
        try{
            $fileInfo = Apiato::call('FileInfo@CreateFileInfoAction', [$request]);
            $this->errors['message'][] = "System info saved successfully.info";
        } catch (Exception $e) {
            $request->isImport = 0;
            $this->errors['message'][] = "System info: ".$e->getMessage()."error";
        }
    }

    public function updateTransaction($request, $tnxId, $journalStatements, $notes, $journalTags, $rowNumber)
    {
        if($tnxId){
            $tags = explode(',', $journalTags);
            foreach ($tags as $key => $tag) {
                try{
                    $dbTag = \App\Containers\Tag\Models\Tag::where('name', 'like', "%$tag%")->first();
                    if(empty($dbTag)) {
                        $data = [
                            'name' => $request->name,
                        ];
                        $dbTag = Apiato::call('Tag@CreateTagTask', [$data]);
                    }
                    $data = [
                        'journal_id' => $tnxId,
                        'tag_id' => $dbTag->id,
                    ];
                    $journalTag = Apiato::call('Tag@CreateJournalTagTask', [$data]);
                } catch (Exception $e) {
                    $request->isImport = 0;
                    $this->errors['message'][] = "Row $rowNumber: Error saving Tags information, ".$e->getMessage().".error";    
                }
            }

            $request->journalId        = $tnxId;
            $request->hide    = ($journalStatements == 'statement' || $journalStatements == 'transaction') ? 1 : null;
            $request->hideTransaction    = ($journalStatements == 'transaction') ? 1 : null;
            $request->notes        = $notes;
            try{
                $updateJournal = Apiato::call('Payment@UpdateTransactionAction', [$request]);
            } catch (Exception $e) {
                $request->isImport = 0;
                $this->errors['message'][] = "Row $rowNumber: ".$e->getMessage()."error";
            }

        }
    }

    public function addGraceInfo($loanId, $graces, $request){
        $request->loanId = $loanId;
        do{
            $currentArr = array_splice($graces, 0, 4);
            foreach ($currentArr as $key => $grace) {
                if(strpos(trimedLowerStr($grace[0]), 'grace days') !== false)
                    $request->days = $grace[1];
                elseif(strpos(trimedLowerStr($grace[0]), 'grace start date') !== false)
                    $request->startDate = Carbon::parse(str_replace('/', '-', $grace[1]))->format('Y-m-d');
                elseif(strpos(trimedLowerStr($grace[0]), 'grace end date') !== false)
                    $request->endDate = Carbon::parse(str_replace('/', '-', $grace[1]))->format('Y-m-d');
                elseif(strpos(trimedLowerStr($grace[0]), 'grace description') !== false)
                    $request->description = $grace[1];
            }

            try{
                $graceAction = Apiato::call('Grace@CreateGraceAction', [$request]);
            } catch (Exception $e) {
                $request->isImport = 0;
                $this->errors['message'][] = "Error saving grace info. ".$e->getMessage().".error";
                break;
            }
        } while (count($graces) >= 3);

        $this->errors['message'][] = "Grace info saved successfully.info";
    }
}
