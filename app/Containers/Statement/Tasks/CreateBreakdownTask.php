<?php

namespace App\Containers\Statement\Tasks;

use App\Containers\Statement\Data\Repositories\BreakdownRepository;
use App\Ship\Exceptions\CreateResourceFailedException;
use App\Ship\Parents\Tasks\Task;
use Exception;

class CreateBreakdownTask extends Task
{

    protected $repository;

    public function __construct(BreakdownRepository $repository)
    {
        $this->repository = $repository;
    }

    public function run(array $data)
    {
        try {
            return $this->repository->create($data);
        }
        catch (Exception $exception) {
            throw new CreateResourceFailedException();
        }
    }
}
