<?php

namespace App\Containers\Statement\Tasks;

use App\Containers\Statement\Data\Repositories\StatementRepository;
use App\Ship\Exceptions\DeleteResourceFailedException;
use App\Ship\Parents\Tasks\Task;
use Exception;

class DeleteStatementTask extends Task
{

    protected $repository;

    public function __construct(StatementRepository $repository)
    {
        $this->repository = $repository;
    }

    public function run($id)
    {
        try {
            return $this->repository->delete($id);
        }
        catch (Exception $exception) {
            throw new DeleteResourceFailedException();
        }
    }
}
