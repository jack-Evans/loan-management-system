<?php
namespace App\Containers\Statement\Tasks;

use App\Ship\Parents\Tasks\Task;
use Carbon\Carbon;
use Apiato\Core\Foundation\Facades\Apiato;

class AddClosingChargeMinTermTransactionsTask extends Task
{
    public function run($loanId, $tnxDate, $balanceBeforePayment, $statements)
    {
        $totalChargeAmount = 0;
        $tnxDate = Carbon::parse($tnxDate);
        $closingCharges = Apiato::call('ClosingCharge@FindClosingLoanChargeByIdTask', [$loanId]);
        if(count($closingCharges) > 0){
            foreach ($closingCharges as $key => $closingCharge) {
                $balanceBeforePayment += $closingCharge->value;
                $statementData = [
                    'tnxDate' => $tnxDate,
                    'debit' => $closingCharge->value,
                    'balance' => $balanceBeforePayment,
                    'description' => 'closing_charge',
                    'comments' => $closingCharge->description,
                ];
                $statement = Apiato::call('Payment@SetStatementTask', [$statementData]);
                array_push($statements, $statement);
                $totalChargeAmount += $closingCharge->value;
            }
        }

        $minTermPending = Apiato::call('ClosingCharge@GetMinTermBalanceTask', [$loanId]);
        if($minTermPending->min_term_balance > 0){
            $balanceBeforePayment += $minTermPending->min_term_balance;
            $statementData = [
                'tnxDate' => $tnxDate,
                'debit' => $minTermPending->min_term_balance,
                'balance' => $balanceBeforePayment,
                'description' => 'min_term_amount',
                'comments' => 'Minimum term amount pending',
            ];

            $totalChargeAmount += $minTermPending->min_term_balance;
            $statement = Apiato::call('Payment@SetStatementTask', [$statementData]);
            array_push($statements, $statement);
        }
        return ['statements' => $statements, 'totalChargeAmount' => $totalChargeAmount];
    }

}