<?php
namespace App\Containers\Statement\Tasks;

use App\Ship\Parents\Tasks\Task;
use App\Containers\Loan\Models\Loan;
use App\Containers\Payment\Models\Statement;
use Apiato\Core\Foundation\Facades\Apiato;
use Carbon\Carbon;

class CalculateMonthInterestTask extends Task
{
    protected $loanClosingDate;

    public function __construct()
    {
    }
    public function run(Loan $loan, $balance, $month, $payments = [], $requestDate = false, $statements, $transactionId = null)
    {            
        // $startDate = $month->copy()->startOfMonth();
        $startDate = $month;
        $endOfMonth = $month->copy()->endOfMonth();
        if($requestDate && $endOfMonth->gt($requestDate)){
            $endOfMonth = $requestDate;
            // var_dump($startDate->format('Y M d'), $endOfMonth->format('Y M d'));
        }

        $interestBreakDown = [];
        $normalRate = $rate = $loan->interest->rate;
        $defaultRate = $loan->interest->default_rate;
        $balanceforCalculation = $balance;
        $issueDate = $loan->issue_at;
        $dueDay = $issueDate->copy()->format('d') - 1;
        $currentDueDate  = Carbon::parse("{$startDate->format('y')}-{$startDate->format('m')}-{$dueDay}");
        // $retainedInterest = $sotredPayments = [];
        $sotredPayments = [];

        $this->loanClosingDate = $issueDate->copy()->subDay()->addMonths($loan->interest->duration);
        $ignorePmt = 0;
        if($payments) {
            $sotredPayments = sortByDate($payments);            
            foreach($sotredPayments as $key => $payment) {
                if($payment['ignorePmt'])
                    $ignorePmt += $payment['ignorePmt'];
                // if($startDate->eq(Carbon::parse('2019-11-20'))){
                    // var_dump($startDate == $payment['date'] && $key > 0 || $payment['date'] == $payment['date']->copy()->startOfMonth(), $startDate, $payment['date'], $key);
                    // dd($payments);
                // }
                // to handle multiple payments at one date OR at first day of month.(skip other payments on same date)
                // if($startDate == $payment['date'] && $key > 0 || $payment['date'] == $payment['date']->copy()->startOfMonth())
                if($startDate == $payment['date'] || $payment['date'] == $payment['date']->copy()->startOfMonth())
                    continue;
                
                $isPaymentLate = Apiato::call('Statement@PaymentLateCheckTask', [$loan, $issueDate, $startDate]);
                if($isPaymentLate)
                    $rate = $defaultRate;
                else
                    $rate = $normalRate;

                // if($payment['currentBalance'] <= 0)
                // {
                //     $breakDown = Apiato::call('Statement@CreateInterestBreakDownTask', [$loan, $rate, $payment['previousBalance'], $startDate, $payment['date']->copy()->subDay(), $plusOne = true]);
                //     if($breakDown != false)
                //         $interestBreakDown[] = $breakDown;
                    
                //     // $totalInterestAmount = collect($interestBreakDown)->sum('total');
                //     $totalInterestAmount = collect(Apiato::call('Statement@SetInterestBreakDownTask', [$interestBreakDown]))->sum('total') - $ignorePmt;
                //     // $balanceAfterInterest =  ($loan->loan_type === 'retained' && $rate == $normalRate) ? $balance : $balance + $totalInterestAmount;
                //     // if($loan->loan_type === 'retained')
                //     //     $balanceAfterInterest = $balance;
                //     // else
                //     $balanceAfterInterest = $balance + $totalInterestAmount;

                //     $statement  = new Statement;
                //     // $statement->setCreatedAtAttribute($payment['date']->copy()->subDay());
                //     $statement->setCreatedAtAttribute($payment['date']);
                //     $statement->setDescriptionAttribute('interest');
                //     $statement->setCommentsAttribute("Int from {$startDate->copy()->format('M d y')} to {$payment['date']->copy()->subDay()->format('M d y')}");
                //     $statement->setDebitAttribute($totalInterestAmount);
                //     $statement->setCreditAttribute(null);
                //     $statement->setBalanceAttribute($balanceAfterInterest);
                //     $statement->setInterestBreakDownAttribute(Apiato::call('Statement@SetInterestBreakDownTask', [$interestBreakDown]));
                //     $statement->setTransactionIdAttribute($transactionId);

                //     return $statement;
                // }
                
                $previousRate = $rate;
                $isPaymentLate = Apiato::call('Statement@PaymentLateCheckTask', [$loan, $issueDate, $currentDueDate]);
                if(($startDate < $currentDueDate) && ($payment['date'] > $currentDueDate) && ($isPaymentLate))
                {
                    $breakDown = Apiato::call('Statement@CreateInterestBreakDownTask', [$loan, $rate, $payment['previousBalance'], $startDate, $currentDueDate, $plusOne = true]);
                    if($breakDown != false)
                        $interestBreakDown[] = $breakDown;
                    $startDate = $currentDueDate->copy()->addDay();
                    if($startDate->eq($payment['date']))
                        $startDate = $startDate->copy()->addDay();
                    if($isPaymentLate)
                        $rate = $defaultRate;
                    else
                        $rate = $normalRate;

                }
                // payment date is one day next to current due day
                if($startDate == $payment['date']){
                    // $breakDown = Apiato::call('Statement@CreateInterestBreakDownTask', [$loan, $rate, $payment['previousBalance'], $startDate, $payment['date'], $plausOne = true]);
                    $isPaymentLate = Apiato::call('Statement@PaymentLateCheckTask', [$loan, $issueDate, $payment['date']]);
                    if(!$isPaymentLate)
                        $rate = $normalRate;
                    // amount paid on day after payment due date and payment is complete. charge at normal rate for the time being as per excel calculations.
                    $breakDown = Apiato::call('Statement@CreateInterestBreakDownTask', [$loan, $rate, $payment['currentBalance'], $startDate, $payment['date'], $plausOne = true]);
                    if($breakDown != false)
                        $interestBreakDown[] = $breakDown;
                    $startDate = $payment['date'] = $payment['date']->copy()->addDay();
                } else {
                    $breakDown = Apiato::call('Statement@CreateInterestBreakDownTask', [$loan, $rate, $payment['previousBalance'], $startDate, $payment['date']->copy()->subDay(), $plausOne = true]);
                    if($breakDown != false)
                        $interestBreakDown[] = $breakDown;
                }

                $publicHoliday = Apiato::call('Holiday@FindHolidayByDateTask', [$currentDueDate]);
                // due date on Saturday, Sunday or public holiday, needs to charge according to normal rate
                $holidayRefundCheck = ( ($currentDueDate->copy()->isSaturday() || $currentDueDate->copy()->isSunday() || $publicHoliday) && ( ($previousRate == $normalRate && $rate == $defaultRate) || ($previousRate == $defaultRate && $rate == $normalRate) ) );
                if($holidayRefundCheck && $loan->holiday_refund)
                {
                    $addDays = getDueDateHolidays($currentDueDate->copy());
                    if($payment['date']->eq($currentDueDate->copy()->addDays($addDays))) {
                        $breakDown = Apiato::call('Statement@CreateInterestBreakDownTask', [$loan, $defaultRate, $payment['previousBalance'], $currentDueDate->copy()->addDay(), $currentDueDate->copy()->addDays($addDays), $plusOne = false, $negativeInterest = true]);
                        if($breakDown != false)
                            $interestBreakDown[] = $breakDown;
                    } else
                        $holidayRefundCheck = false;
                }
                $grace = getTransactionGrace($loan, $payment['date'])->last();
                if( ($holidayRefundCheck == false) && ( ($grace) && ( ($previousRate == $normalRate && $rate == $defaultRate) || ($previousRate == $defaultRate && $rate == $normalRate) ) ) ) {
                    $addDays = $grace->days;
                    if($payment['date']->copy()->subDay()->lte($currentDueDate->copy()->addDays($addDays))) {
                        $breakDown = Apiato::call('Statement@CreateInterestBreakDownTask', [$loan, $defaultRate, $payment['previousBalance'], $currentDueDate->copy()->addDay(), $payment['date']->copy()->subDay(), $plusOne = true, $negativeInterest = true]);
                        if($breakDown != false)
                            $interestBreakDown[] = $breakDown;
                    }
                }

                $startDate = $payment['date'];
            }
            // $startDate = $startDate->copy()->subDay();
        }
        $isPaymentLate = Apiato::call('Statement@PaymentLateCheckTask', [$loan, $issueDate, $startDate]);
        if($isPaymentLate)
            $rate = $defaultRate;
        else
            $rate = $normalRate;
        $isPaymentLateAtEndOfLastMonth = Apiato::call('Statement@PaymentLateCheckTask', [$loan, $issueDate, $endOfMonth]);
        /*
         * Charge according to current rate if these scenarios met.
         * 1) If there wasn't any payment previously but before due date payment was above payment due and it become less after payment due date.
         * 2) OR If there was payment before due date but it was less than due payment amount untill current due date.
         * to handle edge case.($startDate <= $currentDueDate). Start date can be greater than endofmonth date when loan issues at end of month date.
         */
        if( ( ($isPaymentLate == false && $isPaymentLateAtEndOfLastMonth) || ($this->loanClosingDate < $endOfMonth) ) && ($startDate <= $currentDueDate) ){

            $breakDown = Apiato::call('Statement@CreateInterestBreakDownTask', [$loan, $rate, $balanceforCalculation, $startDate, $currentDueDate, $plusOne = true]);
            if($breakDown != false)
                $interestBreakDown[] = $breakDown;
            $startDate = $currentDueDate->copy()->addDay();

            // if( $loan->loan_type === 'retained' && ($isPaymentLate == false && $isPaymentLateAtEndOfLastMonth)) {
            //    $totalInterestAmount = collect(Apiato::call('Statement@SetInterestBreakDownTask', [$interestBreakDown]))->sum('total') - $ignorePmt;
            //     $retainedInterest = Apiato::call('Payment@RetainedInterestStatementTask', [$loan, $balanceforCalculation, $startDate, $paymentsInThisMonth = [], $currentDueDate, $statements, $totalInterestAmount]);
            //     $balanceforCalculation = $retainedInterest['balance'];
            // }
            
            $isPaymentLate = Apiato::call('Statement@PaymentLateCheckTask', [$loan, $issueDate, $endOfMonth]);
            if($isPaymentLate)
                $rate = $defaultRate;
        }

        $breakDown = Apiato::call('Statement@CreateInterestBreakDownTask', [$loan, $rate, $balanceforCalculation, $startDate, $endOfMonth, $plusOne = true]);
            if($breakDown != false)
                $interestBreakDown[] = $breakDown;
// dd(collect(Apiato::call('Statement@SetInterestBreakDownTask', [$interestBreakDown]))->sum('total'));
        // $totalInterestAmount = collect($interestBreakDown)->sum('total');
        $totalInterestAmount = collect(Apiato::call('Statement@SetInterestBreakDownTask', [$interestBreakDown]))->sum('total') - $ignorePmt;
        // $balanceAfterInterest =  ($loan->loan_type === 'retained' && $rate == $normalRate) ? $balance : $balance + $totalInterestAmount;
        // if($loan->loan_type === 'retained')
        //     $balanceAfterInterest = $balance;
        // else
            $balanceAfterInterest = $balance + $totalInterestAmount;

        $statement  = new Statement;
        $statement->setCreatedAtAttribute($endOfMonth);
        $statement->setDescriptionAttribute('interest');
        $statement->setCommentsAttribute("Int from {$month->format('M d y')} to {$endOfMonth->format('M d y')}");
        $statement->setDebitAttribute($totalInterestAmount);
        $statement->setCreditAttribute(null);
        $statement->setBalanceAttribute($balanceAfterInterest);
        $statement->setInterestBreakDownAttribute(Apiato::call('Statement@SetInterestBreakDownTask', [$interestBreakDown]));
        $statement->setTransactionIdAttribute($transactionId);

        // if(sizeof($retainedInterest) > 0){
        //     return [
        //         'retainedInterest' => $retainedInterest,
        //         'statement' => $statement,
        //     ];
        // }
        // else
        return $statement;
    }

}