<?php

namespace App\Containers\Statement\Tasks;

use App\Containers\Statement\Data\Repositories\StatementRepository;
use App\Ship\Exceptions\CreateResourceFailedException;
use App\Ship\Parents\Tasks\Task;
use Exception;

class CreateStatementTask extends Task
{

    protected $repository;

    public function __construct(StatementRepository $repository)
    {
        $this->repository = $repository;
    }

    public function run(array $data)
    {
        try {
            return $this->repository->create($data);
        }
        catch (Exception $exception) {
            throw new CreateResourceFailedException();
        }
    }
}
