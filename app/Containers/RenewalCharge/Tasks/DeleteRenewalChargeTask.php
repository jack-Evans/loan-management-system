<?php

namespace App\Containers\RenewalCharge\Tasks;

use App\Containers\RenewalCharge\Data\Repositories\RenewalChargeRepository;
use App\Ship\Exceptions\DeleteResourceFailedException;
use App\Ship\Parents\Tasks\Task;
use Exception;

class DeleteRenewalChargeTask extends Task
{

    protected $repository;

    public function __construct(RenewalChargeRepository $repository)
    {
        $this->repository = $repository;
    }

    public function run($id)
    {
        try {
            return $this->repository->delete($id);
        }
        catch (Exception $exception) {
            throw new DeleteResourceFailedException();
        }
    }
}
