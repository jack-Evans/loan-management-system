<?php

namespace App\Containers\RenewalCharge\Tasks;

use App\Containers\RenewalCharge\Data\Repositories\RenewalChargeRepository;
use App\Ship\Exceptions\NotFoundException;
use App\Ship\Parents\Tasks\Task;
use Exception;

class FindRenewalChargeByIdTask extends Task
{

    protected $repository;

    public function __construct(RenewalChargeRepository $repository)
    {
        $this->repository = $repository;
    }

    public function run($id)
    {
        try {
            return $this->repository->find($id);
        }
        catch (Exception $exception) {
            throw new NotFoundException();
        }
    }
}
