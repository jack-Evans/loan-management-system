<?php

namespace App\Containers\RenewalCharge\Tasks;

use App\Containers\RenewalCharge\Data\Repositories\RenewalChargeRepository;
use App\Ship\Exceptions\UpdateResourceFailedException;
use App\Ship\Parents\Tasks\Task;
use Exception;

class UpdateRenewalChargeTask extends Task
{

    protected $repository;

    public function __construct(RenewalChargeRepository $repository)
    {
        $this->repository = $repository;
    }

    public function run($id, array $data)
    {
        try {
            return $this->repository->update($data, $id);
        }
        catch (Exception $exception) {
            throw new UpdateResourceFailedException();
        }
    }
}
