<?php

namespace App\Containers\RenewalCharge\Tasks;

use App\Containers\RenewalCharge\Data\Repositories\RenewalChargeRepository;
use App\Ship\Exceptions\CreateResourceFailedException;
use App\Ship\Parents\Tasks\Task;
use Exception;
use Apiato\Core\Foundation\Facades\Apiato;
use Carbon\Carbon;
class CreateRenewalChargeTask extends Task
{

    protected $repository;

    public function __construct(RenewalChargeRepository $repository)
    {
        $this->repository = $repository;
    }

    public function run(array $data, $loan, $assetType = 'renewal_charge')
    {
        try {
            
            $prevousRenewals = Apiato::call('RenewalCharge@GetRenewalChargesByLoanIdTask', [$loan->id])->last();

            $tnxDate        = Carbon::parse($data['renew_date']);
            $accountingPeriod   = "{$tnxDate->format('M')} {$tnxDate->year}";

            \DB::beginTransaction();

            // Create renewal charge
            $renewalCharge = $this->repository->create($data);

            $journalPosting = [
                'creditAccountId' => $loan->customer->account->id,
                'debitAccountId' => 0,
                'type' => $assetType,
                'loanid' => $loan->id,
                'tnxDate' => $tnxDate,
                'amount' => $data['value'],
                'accountingPeriod' => $accountingPeriod,
                'assetType' => $assetType,
                'description' => $data['description'],
                'nominalCode' => isset($data['nominalCode']) ? $data['nominalCode'] : 1,
                'payableId' => $renewalCharge->id,
                'payableType' => 'App\Containers\RenewalCharge\Models\RenewalCharge'
            ];
            $journalPosting = Apiato::call('Payment@CreateJournalPostingTask', [$journalPosting]);

            // if( ($prevousRenewals && $loan->loan_type === 'retained') || $loan->loan_type === 'serviced'){
                $journalPosting = [
                    'creditAccountId' => $loan->customer->account->id,
                    'debitAccountId' => 0,
                    'type' => 'skip',
                    'loanid' => $loan->id,
                    // 'tnxDate' => ($loan->loan_type === 'serviced') ? Carbon::parse($data['renew_end_date'])->copy()->addDay() : $prevousRenewals->renew_end_date->copy()->addDay(),
                    'tnxDate' => ($loan->loan_type === 'retained') ? $data['renew_end_date'] : Carbon::parse($data['renew_end_date'])->copy()->addDay(),
                    'amount' => 0,
                    'accountingPeriod' => $accountingPeriod,
                    'assetType' => 'skip',
                    'description' => 'skip',
                    'payableId' => 0,
                    'payableType' => ''
                ];
                $journalPosting = Apiato::call('Payment@CreateJournalPostingTask', [$journalPosting]);
                // dd($journalPosting);
            // }
            // dd('stop');
            \DB::commit();
            return $renewalCharge;
        }
        catch (Exception $exception) {
            throw new CreateResourceFailedException();
        }
    }
}
