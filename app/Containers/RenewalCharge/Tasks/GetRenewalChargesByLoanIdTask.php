<?php

namespace App\Containers\RenewalCharge\Tasks;

use App\Containers\RenewalCharge\Data\Repositories\RenewalChargeRepository;
use App\Ship\Exceptions\NotFoundException;
use App\Ship\Parents\Tasks\Task;
use Exception;

class GetRenewalChargesByLoanIdTask extends Task
{

    protected $repository;

    public function __construct(RenewalChargeRepository $repository)
    {
        $this->repository = $repository;
    }

    public function run($loanid)
    {
        return $this->repository->scopeQuery(function($q) use ($loanid){
            return $q->where('loan_id', '=', $loanid);
        })->orderBy('renew_end_date','ASC')->get();
    }
}
