<?php

namespace App\Containers\RenewalCharge\Tasks;

use App\Containers\RenewalCharge\Data\Repositories\RenewalChargeRepository;
use App\Ship\Parents\Tasks\Task;

class GetAllRenewalChargesTask extends Task
{

    protected $repository;

    public function __construct(RenewalChargeRepository $repository)
    {
        $this->repository = $repository;
    }

    public function run()
    {
        return $this->repository->paginate();
    }
}
