<?php

namespace App\Containers\RenewalCharge\Data\Repositories;

use App\Ship\Parents\Repositories\Repository;

/**
 * Class RenewalChargeRepository
 */
class RenewalChargeRepository extends Repository
{

    /**
     * @var array
     */
    protected $fieldSearchable = [
        'id' => '=',
        // ...
    ];

}
