<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateRenewalchargeTables extends Migration
{

    /**
     * Run the migrations.
     */
    public function up()
    {
        Schema::create('renewal_charges', function (Blueprint $table) {

            $table->increments('id');
            $table->unsignedInteger('loan_id');
            $table->unsignedInteger('duration');
            $table->float('value', 12, 4);
            $table->date('renew_date');
            $table->date('renew_end_date');
            $table->string('description');
            $table->timestamps();
            $table->softDeletes();

        });
    }

    /**
     * Reverse the migrations.
     */
    public function down()
    {
        Schema::drop('renewal_charges');
    }
}
