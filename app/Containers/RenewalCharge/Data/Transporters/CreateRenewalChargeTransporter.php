<?php

namespace App\Containers\RenewalCharge\Data\Transporters;

use App\Ship\Parents\Transporters\Transporter;

class CreateRenewalChargeTransporter extends Transporter
{

    /**
     * @var array
     */
    protected $schema = [
        'type' => 'object',
        'properties' => [
            'loan_id'   => ['type' => 'number'],
            'duration'   => ['type' => 'number'],
            'value'     => ['type' => 'number'],
            'description'     => ['type' => 'string'],
            'renew_date',
        ],
        'required'   => [
            'loan_id',
            'duration',
            'value',
            'description',
            'renew_date',
        ],
        'default'    => [
            // provide default values for specific properties here
        ]
    ];
}
