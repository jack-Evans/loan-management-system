<?php

namespace App\Containers\RenewalCharge\UI\API\Requests;

use App\Ship\Parents\Requests\Request;

/**
 * Class CreateRenewalChargeRequest.
 */
class CreateRenewalChargeRequest extends Request
{

    /**
     * The assigned Transporter for this Request
     *
     * @var string
     */
    protected $transporter = \App\Containers\RenewalCharge\Data\Transporters\CreateRenewalChargeTransporter::class;

    /**
     * Define which Roles and/or Permissions has access to this request.
     *
     * @var  array
     */
    protected $access = [
        'permissions' => '',
        'roles'       => '',
    ];

    /**
     * Id's that needs decoding before applying the validation rules.
     *
     * @var  array
     */
    protected $decode = [
        'loanId',
    ];

    /**
     * Defining the URL parameters (e.g, `/user/{id}`) allows applying
     * validation rules on them and allows accessing them like request data.
     *
     * @var  array
     */
    protected $urlParameters = [
        // 'id',
    ];

    /**
     * @return  array
     */
    public function rules()
    {
        return [
            'loanId'        => 'required|integer|exists:loans,id',
            'duration'      => 'required|regex:/^\d*(\.\d{1,1})?$/',
            'value'         => 'required|regex:/^\d*(\.\d{1,2})?$/',
            'description'   => 'required|string|min:3|max:100',
            'renewDate'     => 'required|date_format:Y-m-d',
        ];
    }

    /**
     * @return  bool
     */
    public function authorize()
    {
        return $this->check([
            'hasAccess',
        ]);
    }
}
