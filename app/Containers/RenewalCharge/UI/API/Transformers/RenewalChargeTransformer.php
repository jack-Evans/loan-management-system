<?php

namespace App\Containers\RenewalCharge\UI\API\Transformers;

use App\Containers\RenewalCharge\Models\RenewalCharge;
use App\Ship\Parents\Transformers\Transformer;

class RenewalChargeTransformer extends Transformer
{
    /**
     * @var  array
     */
    protected $defaultIncludes = [

    ];

    /**
     * @var  array
     */
    protected $availableIncludes = [

    ];

    /**
     * @param RenewalCharge $entity
     *
     * @return array
     */
    public function transform(RenewalCharge $entity)
    {
        $response = [
            'object' => 'RenewalCharge',
            'id' => $entity->getHashedKey(),
            'loan_id' => $entity->loan_id,
            'duration' => $entity->duration,
            'value' => $entity->value,
            'description' => $entity->description,
            'renew_date' => $entity->renew_date,
            'renew_end_date' => $entity->renew_end_date,
            'journal'   => $entity->journal,
            'created_at' => $entity->created_at,
            'updated_at' => $entity->updated_at,

        ];

        $response = $this->ifAdmin([
            'real_id'    => $entity->id,
            // 'deleted_at' => $entity->deleted_at,
        ], $response);

        return $response;
    }
}
