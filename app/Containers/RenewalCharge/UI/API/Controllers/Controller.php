<?php

namespace App\Containers\RenewalCharge\UI\API\Controllers;

use App\Containers\RenewalCharge\UI\API\Requests\CreateRenewalChargeRequest;
use App\Containers\RenewalCharge\UI\API\Requests\DeleteRenewalChargeRequest;
use App\Containers\RenewalCharge\UI\API\Requests\GetAllRenewalChargesRequest;
use App\Containers\RenewalCharge\UI\API\Requests\FindRenewalChargeByIdRequest;
use App\Containers\RenewalCharge\UI\API\Requests\UpdateRenewalChargeRequest;
use App\Containers\RenewalCharge\UI\API\Transformers\RenewalChargeTransformer;
use App\Ship\Parents\Controllers\ApiController;
use Apiato\Core\Foundation\Facades\Apiato;

/**
 * Class Controller
 *
 * @package App\Containers\RenewalCharge\UI\API\Controllers
 */
class Controller extends ApiController
{
    /**
     * @param CreateRenewalChargeRequest $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function createRenewalCharge(CreateRenewalChargeRequest $request)
    {
        $renewalcharge = Apiato::call('RenewalCharge@CreateRenewalChargeAction', [$request]);

        return $this->created($this->transform($renewalcharge, RenewalChargeTransformer::class));
    }

    /**
     * @param FindRenewalChargeByIdRequest $request
     * @return array
     */
    public function findRenewalChargeById(FindRenewalChargeByIdRequest $request)
    {
        $renewalcharge = Apiato::call('RenewalCharge@FindRenewalChargeByIdAction', [$request]);

        return $this->transform($renewalcharge, RenewalChargeTransformer::class);
    }

    /**
     * @param GetAllRenewalChargesRequest $request
     * @return array
     */
    public function getAllRenewalCharges(GetAllRenewalChargesRequest $request)
    {
        $renewalcharges = Apiato::call('RenewalCharge@GetAllRenewalChargesAction', [$request]);

        return $this->transform($renewalcharges, RenewalChargeTransformer::class);
    }

    /**
     * @param UpdateRenewalChargeRequest $request
     * @return array
     */
    public function updateRenewalCharge(UpdateRenewalChargeRequest $request)
    {
        $renewalcharge = Apiato::call('RenewalCharge@UpdateRenewalChargeAction', [$request]);

        return $this->transform($renewalcharge, RenewalChargeTransformer::class);
    }

    /**
     * @param DeleteRenewalChargeRequest $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function deleteRenewalCharge(DeleteRenewalChargeRequest $request)
    {
        Apiato::call('RenewalCharge@DeleteRenewalChargeAction', [$request]);

        return $this->noContent();
    }
}
