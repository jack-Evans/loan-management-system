<?php

/**
 * @apiGroup           RenewalCharge
 * @apiName            getAllRenewalCharges
 *
 * @api                {GET} /v1/renewalcharges Endpoint title here..
 * @apiDescription     Endpoint description here..
 *
 * @apiVersion         1.0.0
 * @apiPermission      none
 *
 * @apiParam           {String}  parameters here..
 *
 * @apiSuccessExample  {json}  Success-Response:
 * HTTP/1.1 200 OK
{
  // Insert the response of the request here...
}
 */

/** @var Route $router */
$router->get('renewalcharges', [
    'as' => 'api_renewalcharge_get_all_renewal_charges',
    'uses'  => 'Controller@getAllRenewalCharges',
    'middleware' => [
      'auth:api',
    ],
]);
