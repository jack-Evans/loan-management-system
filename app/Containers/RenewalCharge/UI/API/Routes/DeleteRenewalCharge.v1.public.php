<?php

/**
 * @apiGroup           RenewalCharge
 * @apiName            deleteRenewalCharge
 *
 * @api                {DELETE} /v1/renewalcharges/:id Endpoint title here..
 * @apiDescription     Endpoint description here..
 *
 * @apiVersion         1.0.0
 * @apiPermission      none
 *
 * @apiParam           {String}  parameters here..
 *
 * @apiSuccessExample  {json}  Success-Response:
 * HTTP/1.1 200 OK
{
  // Insert the response of the request here...
}
 */

/** @var Route $router */
$router->delete('renewalcharges/{id}', [
    'as' => 'api_renewalcharge_delete_renewal_charge',
    'uses'  => 'Controller@deleteRenewalCharge',
    'middleware' => [
      'auth:api',
    ],
]);
