<?php

/**
 * @apiGroup           RenewalCharge
 * @apiName            createRenewalCharge
 *
 * @api                {POST} /v1/renewalcharges Endpoint title here..
 * @apiDescription     Endpoint description here..
 *
 * @apiVersion         1.0.0
 * @apiPermission      none
 *
 * @apiParam           {String}  parameters here..
 *
 * @apiSuccessExample  {json}  Success-Response:
 * HTTP/1.1 200 OK
{
  // Insert the response of the request here...
}
 */

/** @var Route $router */
$router->post('renewalcharges', [
    'as' => 'api_renewalcharge_create_renewal_charge',
    'uses'  => 'Controller@createRenewalCharge',
    'middleware' => [
      'auth:api',
    ],
]);
