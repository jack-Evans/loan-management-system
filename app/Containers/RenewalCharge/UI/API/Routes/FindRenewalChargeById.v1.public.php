<?php

/**
 * @apiGroup           RenewalCharge
 * @apiName            findRenewalChargeById
 *
 * @api                {GET} /v1/renewalcharges/:id Endpoint title here..
 * @apiDescription     Endpoint description here..
 *
 * @apiVersion         1.0.0
 * @apiPermission      none
 *
 * @apiParam           {String}  parameters here..
 *
 * @apiSuccessExample  {json}  Success-Response:
 * HTTP/1.1 200 OK
{
  // Insert the response of the request here...
}
 */

/** @var Route $router */
$router->get('renewalcharges/{id}', [
    'as' => 'api_renewalcharge_find_renewal_charge_by_id',
    'uses'  => 'Controller@findRenewalChargeById',
    'middleware' => [
      'auth:api',
    ],
]);
