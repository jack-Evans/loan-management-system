<?php

/**
 * @apiGroup           RenewalCharge
 * @apiName            updateRenewalCharge
 *
 * @api                {PATCH} /v1/renewalcharges/:id Endpoint title here..
 * @apiDescription     Endpoint description here..
 *
 * @apiVersion         1.0.0
 * @apiPermission      none
 *
 * @apiParam           {String}  parameters here..
 *
 * @apiSuccessExample  {json}  Success-Response:
 * HTTP/1.1 200 OK
{
  // Insert the response of the request here...
}
 */

/** @var Route $router */
$router->patch('renewalcharges/{id}', [
    'as' => 'api_renewalcharge_update_renewal_charge',
    'uses'  => 'Controller@updateRenewalCharge',
    'middleware' => [
      'auth:api',
    ],
]);
