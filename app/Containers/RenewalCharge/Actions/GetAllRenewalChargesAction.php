<?php

namespace App\Containers\RenewalCharge\Actions;

use App\Ship\Parents\Actions\Action;
use App\Ship\Parents\Requests\Request;
use Apiato\Core\Foundation\Facades\Apiato;

class GetAllRenewalChargesAction extends Action
{
    public function run(Request $request)
    {
        $renewalcharges = Apiato::call('RenewalCharge@GetAllRenewalChargesTask', [], ['addRequestCriteria']);

        return $renewalcharges;
    }
}
