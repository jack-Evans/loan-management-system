<?php

namespace App\Containers\RenewalCharge\Actions;

use App\Ship\Parents\Actions\Action;
use App\Ship\Parents\Requests\Request;
use Apiato\Core\Foundation\Facades\Apiato;

class FindRenewalChargeByIdAction extends Action
{
    public function run(Request $request)
    {
        $renewalcharge = Apiato::call('RenewalCharge@GetRenewalChargesByLoanIdTask', [$request->id]);

        return $renewalcharge;
    }
}
