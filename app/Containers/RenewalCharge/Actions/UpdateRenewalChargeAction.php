<?php

namespace App\Containers\RenewalCharge\Actions;

use App\Ship\Parents\Actions\Action;
use App\Ship\Parents\Requests\Request;
use Apiato\Core\Foundation\Facades\Apiato;

class UpdateRenewalChargeAction extends Action
{
    public function run(Request $request)
    {
    	$updateRequest = [];

    	if($request->duration)
    		$updateRequest['duration'] = $request->duration;

    	if($request->value)
    		$updateRequest['value'] = $request->value;

    	if($request->description)
    		$updateRequest['description'] = $request->description;

    	if($request->renewDate)
    		$updateRequest['renew_date'] = $request->renewDate;

        $renewalcharge = Apiato::call('RenewalCharge@UpdateRenewalChargeTask', [$request->id, $updateRequest]);

        return $renewalcharge;
    }
}
