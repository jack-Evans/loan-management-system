<?php

namespace App\Containers\RenewalCharge\Actions;

use App\Ship\Parents\Actions\Action;
use App\Containers\RenewalCharge\Data\Transporters\CreateRenewalChargeTransporter;
use App\Ship\Parents\Requests\Request;
use Apiato\Core\Foundation\Facades\Apiato;
use Carbon\Carbon;
use App\Containers\Payment\Exceptions\HTTPPreConditionFailedException;

class CreateRenewalChargeAction extends Action
{
    public function run(Request $request)
    {
        $requestData = [
         	'loan_id' 	=> $request->loanId,
         	'duration' 	=> $request->duration,
         	'value'		=> $request->value,
         	'description'=> $request->description,
         	'renew_date' => $request->renewDate,
            'nominalCode' => $request->nominalCode
        ];
        $renewalDate = Carbon::parse($requestData['renew_date']);
        $requestData['renew_end_date'] = $renewalDate->copy()->addMonths($request->duration);
        
        $loan = Apiato::call('Loan@FindLoanByIdTask', [$request->loanId]);
        $loanClosingDate = $loan->issue_at->copy()->addMonths($loan->interest->duration)->subDay();

        $lastRenewal = Apiato::call('RenewalCharge@GetRenewalChargesByLoanIdTask', [$request->loanId])->last();
        if($lastRenewal){
            $lastRenewalDuration = $lastRenewal->renew_date->copy()->addMonths($lastRenewal->duration);
            if($lastRenewalDuration->gt($renewalDate))
                throw new HTTPPreConditionFailedException('Renew date should be greater than (equal to) previous renew duration end date.('.$lastRenewalDuration->format("M d Y").')');
        }

        if($loanClosingDate->gt($renewalDate))
        	throw new HTTPPreConditionFailedException('Renew date should be greater than (equal to) loan closing date.('.$loanClosingDate->format("M d Y").')');

        $renewalcharge = Apiato::call('RenewalCharge@CreateRenewalChargeTask', [$requestData, $loan, $type = 'renewal_charge']);

        return $renewalcharge;
    }
}
