<?php

namespace App\Containers\RenewalCharge\Actions;

use App\Ship\Parents\Actions\Action;
use App\Ship\Parents\Requests\Request;
use Apiato\Core\Foundation\Facades\Apiato;

class DeleteRenewalChargeAction extends Action
{
    public function run(Request $request)
    {
        return Apiato::call('RenewalCharge@DeleteRenewalChargeTask', [$request->id]);
    }
}
