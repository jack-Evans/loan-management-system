<?php

namespace App\Containers\RenewalCharge\Models;

use App\Ship\Parents\Models\Model;

class RenewalCharge extends Model
{
    protected $fillable = [
        'loan_id',
        'duration',
        'value',
        'description',
        'renew_date',
        'renew_end_date',
    ];

    protected $attributes = [

    ];

    protected $hidden = [

    ];

    protected $casts = [

    ];

    protected $dates = [
        'renew_date',
        'renew_end_date',
        'created_at',
        'updated_at',
    ];

    /**
     * A resource key to be used by the the JSON API Serializer responses.
     */
    protected $resourceKey = 'renewalcharges';

    public function journal() {
        return $this->morphOne(\App\Containers\Payment\Models\Journal::class, 'payable');
    }
}
