<?php

namespace App\Containers\OtherCharge\Tasks;

use App\Containers\OtherCharge\Data\Repositories\OtherChargeRepository;
use App\Containers\Payment\Data\Repositories\JournalRepository;
use App\Containers\Payment\Data\Repositories\PostRepository;
use App\Ship\Exceptions\DeleteResourceFailedException;
use App\Ship\Parents\Tasks\Task;
use Exception;
use Apiato\Core\Foundation\Facades\Apiato;

class DeleteOtherChargeTask extends Task
{

    protected $otherChargeRepository;
    protected $journalRepository;
    protected $postingRepository;

    public function __construct(OtherChargeRepository $otherChargeRepository, JournalRepository $journalRepository, PostRepository $postingRepository)
    {
        $this->journalRepository = $journalRepository;
        $this->postingRepository = $postingRepository;
        $this->otherChargeRepository = $otherChargeRepository;
    }

    public function run($id)
    {
        try {
            $otherCharge = Apiato::call('OtherCharge@FindOtherChargeByIdTask', [$id]);
            \DB::beginTransaction();
            
            foreach($otherCharge->journal->posts as $post) {
                $this->postingRepository->delete($post->id);
            }
            
            $this->journalRepository->delete($otherCharge->journal->id);
            $deletedOtherCharge = $this->otherChargeRepository->delete($id);
            
            \DB::commit();
            return $deletedOtherCharge;
        }
        catch (Exception $exception) {
            throw new DeleteResourceFailedException();
        }
    }
}
