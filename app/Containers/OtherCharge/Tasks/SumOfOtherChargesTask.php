<?php

namespace App\Containers\OtherCharge\Tasks;

use App\Containers\OtherCharge\Data\Repositories\OtherChargeRepository;
use App\Containers\Payment\Data\Repositories\PostRepository;
use App\Ship\Parents\Tasks\Task;

class SumOfOtherChargesTask extends Task
{

    protected $repository;
    protected $postingRepository;

    public function __construct(OtherChargeRepository $repository, PostRepository $postingRepository)
    {
        $this->repository = $repository;
        $this->postingRepository = $postingRepository;
    }

    public function run($loanId)
    {
        $debit = 0;
        $charges = $this->repository->scopeQuery(function($q) use($loanId) {
            return $q->where('loan_id', $loanId);
        })->with('journal')->get();
        
        foreach($charges as $charge)
        {
            $debit += $this->postingRepository->findWhere(['journal_id' => $charge->journal->id, 'type' => 'debit'])->sum('amount');            
        }
        
        return $debit;
    }
}
