<?php

namespace App\Containers\OtherCharge\Tasks;

use App\Containers\OtherCharge\Data\Repositories\OtherChargeRepository;
use App\Ship\Exceptions\UpdateResourceFailedException;
use App\Ship\Parents\Tasks\Task;
use Exception;
use App\Containers\Payment\Data\Repositories\JournalRepository;
use App\Containers\Payment\Data\Repositories\PostRepository;
use Apiato\Core\Foundation\Facades\Apiato;

class UpdateOtherChargeTask extends Task
{

    protected $otherChargeRepository;
    protected $journalRepository;
    protected $postingRepository;

    public function __construct(OtherChargeRepository $otherChargeRepository, JournalRepository $journalRepository, PostRepository $postingRepository)
    {
        $this->otherChargeRepository = $otherChargeRepository;
        $this->journalRepository = $journalRepository;
        $this->postingRepository = $postingRepository;
    }

    public function run($id, array $data)
    {
       try {
            // \DB::beginTransaction();            
            
            return $otherCharge =  $this->otherChargeRepository->update($data, $id);
            // $otherCharge->journal;
            
            // // affected fields in journals
            // if(isset($data['name']))
            // {
            //     $this->journalRepository->update(['comment' => $data['name']], $otherCharge->journal->id);
            // }
            
            // if(isset($data['chargedate']))
            // {
            //     $this->journalRepository->update(['tnx_date' => $data['chargedate']], $otherCharge->journal->id);
            // }
            
            // // affected fields in postings
            // if(isset($data['value']) || isset($data['chargedate']))
            // {
            //     $postingRequest = [];
            //     if(isset($data['value']))
            //     {
            //         $loan = Apiato::call('Loan@FindLoanByIdTask', [$otherCharge->loan_id]);        
            //         $chargeValue = $otherCharge->value;

            //         if($otherCharge->charge_type == 'percentage')
            //             $postingRequest['amount'] = round(resolvePercentage($loan->net_loan) * $otherCharge->value, 2);
            //         else
            //             $postingRequest['amount'] = $otherCharge->value;
                    
            //         Apiato::call('Payment@AddAmountInLastPaymentPlanAction', [$otherCharge->loan_id, $postingRequest['amount']]);
            //     }
            //     if(isset($data['chargedate']))
            //     {
            //       $paid_at = \Carbon\Carbon::parse($data['chargedate']);
            //       $postingRequest['accounting_period']  = "{$paid_at->format('M')} {$paid_at->year}";
            //     }

            //     foreach($otherCharge->journal->posts as $post) {
            //         $this->postingRepository->update($postingRequest, $post->id);
            //     }
            // }
            // \DB::commit();

            // $otherCharge = Apiato::call('OtherCharge@FindOtherChargeByIdTask', [$id]);
            // return $otherCharge;
       }
       catch (Exception $exception) {
           throw new UpdateResourceFailedException();
       }
        

    }
}
