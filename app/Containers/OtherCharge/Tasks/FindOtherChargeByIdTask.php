<?php

namespace App\Containers\OtherCharge\Tasks;

use App\Containers\OtherCharge\Data\Repositories\OtherChargeRepository;
use App\Ship\Exceptions\NotFoundException;
use App\Ship\Parents\Tasks\Task;
use Exception;

class FindOtherChargeByIdTask extends Task
{

    protected $repository;

    public function __construct(OtherChargeRepository $repository)
    {
        $this->repository = $repository;
    }

    public function run($id)
    {
        try {
            return $this->repository->with(['journal'])->find($id);
        }
        catch (Exception $exception) {
            throw new NotFoundException();
        }
    }
}
