<?php

namespace App\Containers\OtherCharge\Tasks;

use App\Containers\OtherCharge\Data\Repositories\OtherChargeRepository;
use App\Ship\Exceptions\CreateResourceFailedException;
use App\Ship\Parents\Tasks\Task;
use Exception;

class CreateOtherChargeTask extends Task
{

    protected $repository;

    public function __construct(OtherChargeRepository $repository)
    {
        $this->repository = $repository;
    }

    public function run(array $data)
    {        
        try {
            return $this->repository->create($data);
        }
        catch (Exception $exception) {
            throw new CreateResourceFailedException();
        }
    }
}
