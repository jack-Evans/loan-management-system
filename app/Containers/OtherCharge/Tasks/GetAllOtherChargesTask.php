<?php

namespace App\Containers\OtherCharge\Tasks;

use App\Containers\OtherCharge\Data\Repositories\OtherChargeRepository;
use App\Ship\Parents\Tasks\Task;

class GetAllOtherChargesTask extends Task
{

    protected $repository;

    public function __construct(OtherChargeRepository $repository)
    {
        $this->repository = $repository;
    }

    public function run($loanId)
    {
        return $this->repository->scopeQuery(function($q) use($loanId) {
            return $q->where('loan_id', $loanId);
        })->with('journal')->get();
    }
}
