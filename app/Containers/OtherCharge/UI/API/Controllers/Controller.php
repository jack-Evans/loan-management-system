<?php

namespace App\Containers\OtherCharge\UI\API\Controllers;

use App\Containers\OtherCharge\UI\API\Requests\CreateOtherChargeRequest;
use App\Containers\OtherCharge\UI\API\Requests\DeleteOtherChargeRequest;
use App\Containers\OtherCharge\UI\API\Requests\GetAllOtherChargesRequest;
use App\Containers\OtherCharge\UI\API\Requests\FindOtherChargeByIdRequest;
use App\Containers\OtherCharge\UI\API\Requests\UpdateOtherChargeRequest;
use App\Containers\OtherCharge\UI\API\Transformers\OtherChargeTransformer;
use App\Ship\Parents\Controllers\ApiController;
use Apiato\Core\Foundation\Facades\Apiato;

/**
 * Class Controller
 *
 * @package App\Containers\OtherCharge\UI\API\Controllers
 */
class Controller extends ApiController
{
    /**
     * @param CreateOtherChargeRequest $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function createOtherCharge(CreateOtherChargeRequest $request)
    {        
        $othercharge = Apiato::call('OtherCharge@CreateOtherChargeAction', [$request]);

        return $this->created($this->transform($othercharge, OtherChargeTransformer::class));
    }

    /**
     * @param FindOtherChargeByIdRequest $request
     * @return array
     */
    public function findOtherChargeById(FindOtherChargeByIdRequest $request)
    {
        $othercharge = Apiato::call('OtherCharge@FindOtherChargeByIdAction', [$request]);

        return $this->transform($othercharge, OtherChargeTransformer::class);
    }

    /**
     * @param GetAllOtherChargesRequest $request
     * @return array
     */
    public function getAllOtherCharges(GetAllOtherChargesRequest $request)
    {
        $othercharges = Apiato::call('OtherCharge@GetAllOtherChargesAction', [$request]);

        return $this->transform($othercharges, OtherChargeTransformer::class);
    }

    /**
     * @param UpdateOtherChargeRequest $request
     * @return array
     */
    public function updateOtherCharge(UpdateOtherChargeRequest $request)
    {
        $othercharge = Apiato::call('OtherCharge@UpdateOtherChargeAction', [$request]);

        return $this->transform($othercharge, OtherChargeTransformer::class);
    }

    /**
     * @param DeleteOtherChargeRequest $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function deleteOtherCharge(DeleteOtherChargeRequest $request)
    {
        Apiato::call('OtherCharge@DeleteOtherChargeAction', [$request]);

        return $this->noContent();
    }
}
