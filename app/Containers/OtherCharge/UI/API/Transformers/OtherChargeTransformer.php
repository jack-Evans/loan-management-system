<?php

namespace App\Containers\OtherCharge\UI\API\Transformers;

use App\Containers\OtherCharge\Models\OtherCharge;
use App\Ship\Parents\Transformers\Transformer;

class OtherChargeTransformer extends Transformer
{
    /**
     * @var  array
     */
    protected $defaultIncludes = [

    ];

    /**
     * @var  array
     */
    protected $availableIncludes = [

    ];

    /**
     * @param OtherCharge $entity
     *
     * @return array
     */
    public function transform(OtherCharge $entity)
    {
        $response = [
            'object' => 'OtherCharge',
            'id' => $entity->getHashedKey(),
            'loan_id' => $entity->loan_id,
            'name' => $entity->name,
            'value' => $entity->value,
            'amountType' => $entity->amount_type,
            'chargeType' => $entity->charge_type,
            'refunded' => $entity->refunded,
            'charge_date' => $entity->charge_date,
            'journal'   => $entity->journal,
            'created_at' => $entity->created_at,
            'updated_at' => $entity->updated_at,

        ];

        $response = $this->ifAdmin([
            'real_id'    => $entity->id,
            // 'deleted_at' => $entity->deleted_at,
        ], $response);

        return $response;
    }
}
