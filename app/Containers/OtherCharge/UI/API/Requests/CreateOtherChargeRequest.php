<?php

namespace App\Containers\OtherCharge\UI\API\Requests;

use App\Ship\Parents\Requests\Request;

/**
 * Class CreateOtherChargeRequest.
 */
class CreateOtherChargeRequest extends Request
{

    /**
     * The assigned Transporter for this Request
     *
     * @var string
     */
    protected $transporter = \App\Containers\OtherCharge\Data\Transporters\CreateOtherChargeTransporter::class;

    /**
     * Define which Roles and/or Permissions has access to this request.
     *
     * @var  array
     */
    protected $access = [
        'permissions' => '',
        'roles'       => '',
    ];

    /**
     * Id's that needs decoding before applying the validation rules.
     *
     * @var  array
     */
    protected $decode = [
         'loanid',
    ];

    /**
     * Defining the URL parameters (e.g, `/user/{id}`) allows applying
     * validation rules on them and allows accessing them like request data.
     *
     * @var  array
     */
    protected $urlParameters = [
        // 'id',
    ];

    /**
     * @return  array
     */
    public function rules()
    {
        return [
            'loanid'           => 'required|integer',
            'name'             => 'required|max:50|string',
            'value'            => 'required|regex:/^\d*(\.\d{1,2})?$/',
            'amounttype'       => 'required_if:chargetype,percentage|in:netloan,principal_amount,grossloan',
            'chargetype'       => 'required|in:fixed,percentage',
            'chargedate'       => 'required|date_format:Y-m-d'
        ];
    }

    /**
     * @return  bool
     */
    public function authorize()
    {
        return $this->check([
            'hasAccess',
        ]);
    }
}
