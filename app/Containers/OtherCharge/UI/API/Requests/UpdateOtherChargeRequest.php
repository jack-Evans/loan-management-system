<?php

namespace App\Containers\OtherCharge\UI\API\Requests;

use App\Ship\Parents\Requests\Request;

/**
 * Class UpdateOtherChargeRequest.
 */
class UpdateOtherChargeRequest extends Request
{

    /**
     * The assigned Transporter for this Request
     *
     * @var string
     */
    protected $transporter = \App\Containers\OtherCharge\Data\Transporters\UpdateOtherChargeTransporter::class;

    /**
     * Define which Roles and/or Permissions has access to this request.
     *
     * @var  array
     */
    protected $access = [
        'permissions' => '',
        'roles'       => '',
    ];

    /**
     * Id's that needs decoding before applying the validation rules.
     *
     * @var  array
     */
    protected $decode = [
         'id',
    ];

    /**
     * Defining the URL parameters (e.g, `/user/{id}`) allows applying
     * validation rules on them and allows accessing them like request data.
     *
     * @var  array
     */
    protected $urlParameters = [
         'id',
    ];

    /**
     * @return  array
     */
    public function rules()
    {
        return [
            'name'             => 'required_without_all:value,chargetype,chargedate|max:50|string|unique:charges,name',
            'value'            => 'required_without_all:name,chargetype,chargedate|regex:/^\d*(\.\d{1,2})?$/',
            'amounttype'       => 'required_if:chargetype,percentage|in:netloan,principal_amount,grossloan',
            'chargetype'       => 'required_without_all:name,value,chargedate|in:fixed,percentage,chargedate',
            'chargedate'       => 'required_without_all:name,value,chargetype|date_format:Y-m-d',
        ];
    }

    /**
     * @return  bool
     */
    public function authorize()
    {
        return $this->check([
            'hasAccess',
        ]);
    }
}
