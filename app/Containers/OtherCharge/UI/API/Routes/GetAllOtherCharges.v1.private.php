<?php

/**
 * @apiGroup           OtherCharge
 * @apiName            getAllOtherCharges
 *
 * @api                {GET} /v1/othercharges Endpoint title here..
 * @apiDescription     Endpoint description here..
 *
 * @apiVersion         1.0.0
 * @apiPermission      none
 *
 * @apiParam           {String}  parameters here..
 *
 * @apiSuccessExample  {json}  Success-Response:
 * HTTP/1.1 200 OK
{
  // Insert the response of the request here...
}
 */

/** @var Route $router */
$router->get('othercharges', [
    'as' => 'api_othercharge_get_all_other_charges',
    'uses'  => 'Controller@getAllOtherCharges',
    'middleware' => [
      'auth:api',
    ],
]);
