<?php

/**
 * @apiGroup           OtherCharge
 * @apiName            findOtherChargeById
 *
 * @api                {GET} /v1/othercharges/:id Endpoint title here..
 * @apiDescription     Endpoint description here..
 *
 * @apiVersion         1.0.0
 * @apiPermission      none
 *
 * @apiParam           {String}  parameters here..
 *
 * @apiSuccessExample  {json}  Success-Response:
 * HTTP/1.1 200 OK
{
  // Insert the response of the request here...
}
 */

/** @var Route $router */
$router->get('othercharges/{id}', [
    'as' => 'api_othercharge_find_other_charge_by_id',
    'uses'  => 'Controller@findOtherChargeById',
    'middleware' => [
      'auth:api',
    ],
]);
