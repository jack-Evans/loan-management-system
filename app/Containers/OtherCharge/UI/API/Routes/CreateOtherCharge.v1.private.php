<?php

/**
 * @apiGroup           OtherCharge
 * @apiName            createOtherCharge
 *
 * @api                {POST} /v1/othercharges Endpoint title here..
 * @apiDescription     Endpoint description here..
 *
 * @apiVersion         1.0.0
 * @apiPermission      none
 *
 * @apiParam           {String}  parameters here..
 *
 * @apiSuccessExample  {json}  Success-Response:
 * HTTP/1.1 200 OK
{
  // Insert the response of the request here...
}
 */

/** @var Route $router */
$router->post('othercharges', [
    'as' => 'api_othercharge_create_other_charge',
    'uses'  => 'Controller@createOtherCharge',
    'middleware' => [
      'auth:api',
    ],
]);
