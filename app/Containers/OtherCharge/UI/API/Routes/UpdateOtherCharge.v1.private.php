<?php

/**
 * @apiGroup           OtherCharge
 * @apiName            updateOtherCharge
 *
 * @api                {PATCH} /v1/othercharges/:id Endpoint title here..
 * @apiDescription     Endpoint description here..
 *
 * @apiVersion         1.0.0
 * @apiPermission      none
 *
 * @apiParam           {String}  parameters here..
 *
 * @apiSuccessExample  {json}  Success-Response:
 * HTTP/1.1 200 OK
{
  // Insert the response of the request here...
}
 */

/** @var Route $router */
$router->patch('othercharges/{id}', [
    'as' => 'api_othercharge_update_other_charge',
    'uses'  => 'Controller@updateOtherCharge',
    'middleware' => [
      'auth:api',
    ],
]);
