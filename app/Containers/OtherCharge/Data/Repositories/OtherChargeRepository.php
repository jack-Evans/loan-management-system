<?php

namespace App\Containers\OtherCharge\Data\Repositories;

use App\Ship\Parents\Repositories\Repository;

/**
 * Class OtherChargeRepository
 */
class OtherChargeRepository extends Repository
{

    /**
     * @var array
     */
    protected $fieldSearchable = [
        'id' => '=',
        // ...
    ];

}
