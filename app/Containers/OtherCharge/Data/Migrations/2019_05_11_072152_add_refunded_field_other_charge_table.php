<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class AddRefundedFieldOtherChargeTable extends Migration
{

    /**
     * Run the migrations.
     */
    public function up()
    {
        Schema::table('other_charges', function (Blueprint $table) {

            $table->boolean('refunded')->default(0)->after('charge_type');

        });
    }

    /**
     * Reverse the migrations.
     */
    public function down()
    {
        Schema::table('other_charges', function(Blueprint $table){
            $table->dropcolumn('refunded');
        });
    }
}
