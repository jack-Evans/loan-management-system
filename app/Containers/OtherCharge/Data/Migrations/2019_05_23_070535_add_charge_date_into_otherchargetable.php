<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class AddChargeDateIntoOtherchargetable extends Migration
{

    /**
     * Run the migrations.
     */
    public function up()
    {
        Schema::table('other_charges', function (Blueprint $table) {
            $table->date('charge_date')->after('refunded')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down()
    {
        Schema::table('other_charges', function (Blueprint $table) {
            $table->dropcolumn('charge_date');
        });
    }
}
