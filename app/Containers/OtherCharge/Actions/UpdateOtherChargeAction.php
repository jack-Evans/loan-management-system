<?php

namespace App\Containers\OtherCharge\Actions;

use App\Ship\Parents\Actions\Action;
use App\Ship\Parents\Requests\Request;
use Apiato\Core\Foundation\Facades\Apiato;

class UpdateOtherChargeAction extends Action
{
    public function run(Request $request)
    {        
        $data = [];
        
        if($request->name)
            $data['name'] = $request->name;
        
        if($request->value)
        {
            // remove previous charge value from paymentplan
            $removePreviousChargeAmount = Apiato::call('OtherCharge@RemovePreviousChargeAmountAction', [$request]);
                    
            $data['value'] = $request->value;
        }
        if($request->chargetype)
            $data['charge_type'] = $request->chargetype;
        
        if($request->amounttype)
            $data['amount_type'] = $request->amounttype;
        
        if($request->chargedate)
            $data['chargedate'] = $request->chargedate;
        
        $othercharge = Apiato::call('OtherCharge@UpdateOtherChargeTask', [$request->id, $data]);

        return $othercharge;
    }
}
