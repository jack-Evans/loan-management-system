<?php

namespace App\Containers\OtherCharge\Actions;

use App\Ship\Parents\Actions\Action;
use App\Ship\Parents\Requests\Request;
use Apiato\Core\Foundation\Facades\Apiato;
use Carbon\Carbon;
use App\Containers\Payment\Exceptions\HTTPPreConditionFailedException;

class CreateOtherChargeAction extends Action
{
    public function run(Request $request)
    {
        // $otherChargeDate = Carbon::parse($request->chargedate);

        $createOtherChargeRequest = [
            'loan_id' => $request->loanid,
            'name' => $request->name,
            'value' => $request->value,
            'amount_type' => $request->amounttype,
            'charge_type' => $request->chargetype,
            // 'charge_date' => $otherChargeDate,
            'charge_date' => $request->chargedate,
        ];

        $loan = Apiato::call('Loan@FindLoanByIdTask', [$request->loanid]);        

        $lastRealStatementDt = Apiato::call('Payment@GetStatementsTask', [$loan])->where('description', '!=', 'interest')->last()->created_at;
        // if($lastRealStatementDt->gte($otherChargeDate))
        //     throw new HTTPPreConditionFailedException("Other charge date should be greater than {$lastRealStatementDt->format("M d Y")}.");
        Apiato::call('Statement@VerifyStatementDateTask', [$loan->id, $request->chargedate]);

        \DB::beginTransaction();

        $otherCharge = Apiato::call('OtherCharge@CreateOtherChargeTask', [$createOtherChargeRequest]);

        $chargeValue = $otherCharge->value;
        
        if($otherCharge->charge_type == 'percentage')
        {
            $chargeValue = round(resolvePercentage($loan->net_loan) * $request->value, 2);
        }
        
        $journalPostingRequest = [
            'creditAccountId' => 0,
            'debitAccountId' => $loan->customer_id,
            'type' => 'charge',
            'loanid' => $loan->id,
            'tnxDate' => Carbon::parse($request->chargedate)->format('Y-m-d'),
            'amount' => $chargeValue,
            'accountingPeriod' => Carbon::parse($request->chargedate)->format('M Y'),
            'assetType' => 'other_charge',
            'description' => $otherCharge->name,
            'nominalCode' => $request->nominalCode,
            'payableId' => $otherCharge->id,
            'payableType' => 'App\Containers\OtherCharge\Models\OtherCharge',
        ];        
                
        $journal = Apiato::call('Payment@CreateJournalPostingTask', [$journalPostingRequest]);
        
        $addNewAmount = Apiato::call('Payment@AddAmountInLastPaymentPlanAction', [$request->loanid, $chargeValue]);        
        
        \DB::commit();

        return $otherCharge;
    }
}
