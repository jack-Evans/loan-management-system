<?php

namespace App\Containers\OtherCharge\Actions;

use App\Ship\Parents\Actions\Action;
use App\Ship\Parents\Requests\Request;
use Apiato\Core\Foundation\Facades\Apiato;

class FindOtherChargeByIdAction extends Action
{
    public function run(Request $request)
    {
        $othercharge = Apiato::call('OtherCharge@FindOtherChargeByIdTask', [$request->id]);

        return $othercharge;
    }
}
