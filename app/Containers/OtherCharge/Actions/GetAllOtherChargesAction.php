<?php

namespace App\Containers\OtherCharge\Actions;

use App\Ship\Parents\Actions\Action;
use App\Ship\Parents\Requests\Request;
use Apiato\Core\Foundation\Facades\Apiato;

class GetAllOtherChargesAction extends Action
{
    public function run(Request $request)
    {
        $othercharges = Apiato::call('OtherCharge@GetAllOtherChargesTask', [$request->loanid]);
//dd($othercharges);
        return $othercharges;
    }
}
