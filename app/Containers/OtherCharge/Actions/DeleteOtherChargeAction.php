<?php

namespace App\Containers\OtherCharge\Actions;

use App\Ship\Parents\Actions\Action;
use App\Ship\Parents\Requests\Request;
use Apiato\Core\Foundation\Facades\Apiato;

class DeleteOtherChargeAction extends Action
{
    public function run(Request $request)
    {   
        \DB::beginTransaction();
        
        $removePreviousChargeAmount = Apiato::call('OtherCharge@RemovePreviousChargeAmountAction', [$request]);
        
        $deleteOtherCharge = Apiato::call('OtherCharge@DeleteOtherChargeTask', [$request->id]);
        
        \DB::commit();
        
        return $deleteOtherCharge;
    }
}
