<?php

namespace App\Containers\OtherCharge\Actions;

use App\Ship\Parents\Actions\Action;
use App\Ship\Parents\Requests\Request;
use Apiato\Core\Foundation\Facades\Apiato;

class RemovePreviousChargeAmountAction extends Action
{
    public function run(Request $request)
    {   
        \DB::beginTransaction();
        
        $otherCharge = Apiato::call('OtherCharge@FindOtherChargeByIdTask', [$request->id]);
        
        $lastPayment = Apiato::call('Payment@FindLastPaymentPlanTask', [$otherCharge->loan_id]);
        
        $updatePaymentPlanRequest = [
            'amount' => round($lastPayment->amount - $otherCharge->journal->posts[0]->amount, 2),
        ];
        
        $updateLastPaymentPlan = Apiato::call('Payment@UpdatePaymentPlanTask', [$lastPayment->id, $updatePaymentPlanRequest]);

        \DB::commit();
        
        return $updateLastPaymentPlan;
    }
}
