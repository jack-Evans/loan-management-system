<?php

namespace App\Containers\OtherCharge\Models;

use App\Ship\Parents\Models\Model;

class OtherCharge extends Model
{
    protected $fillable = [
        'loan_id',
        'name',
        'value',
        'amount_type',
        'charge_type',
        'refunded',
        'charge_date',
    ];

    protected $attributes = [

    ];

    protected $hidden = [

    ];

    protected $casts = [

    ];

    protected $dates = [
        'created_at',
        'updated_at',
    ];

    /**
     * A resource key to be used by the the JSON API Serializer responses.
     */
    protected $resourceKey = 'othercharges';
    
    public function journal() {
        return $this->morphOne(\App\Containers\Payment\Models\Journal::class, 'payable');
    }
}
