<?php

namespace App\Containers\LoanExtras\Data\Repositories;

use App\Ship\Parents\Repositories\Repository;

/**
 * Class IgnoredSheetRepository
 */
class IgnoredSheetRepository extends Repository
{

    /**
     * @var array
     */
    protected $fieldSearchable = [
        'id' => '=',
        // ...
    ];

}
