<?php

namespace App\Containers\LoanExtras\Data\Repositories;

use App\Ship\Parents\Repositories\Repository;

/**
 * Class LoanExtrasRepository
 */
class LoanExtrasRepository extends Repository
{

    /**
     * @var array
     */
    protected $fieldSearchable = [
        'id' => '=',
        // ...
    ];

}
