<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateLoanextrasTables extends Migration
{

    /**
     * Run the migrations.
     */
    public function up()
    {
        Schema::create('loan_extras', function (Blueprint $table) {

            $table->increments('id');
            $table->unsignedInteger('loan_id');
            $table->string('sheet_no')->nullable();
            $table->text('note')->nullable();
            $table->text('breakdown')->nullable();
            $table->timestamps();
            //$table->softDeletes();

        });
    }

    /**
     * Reverse the migrations.
     */
    public function down()
    {
        Schema::dropIfExists('loan_extras');
    }
}
