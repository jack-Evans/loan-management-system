<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateIgnoredsheetsTables extends Migration
{

    /**
     * Run the migrations.
     */
    public function up()
    {
        Schema::create('ignored_sheets', function (Blueprint $table) {

            $table->increments('id');
            $table->string('sheet_name');
            $table->text('sheet_data')->nullable();
            $table->timestamps();
            //$table->softDeletes();

        });
    }

    /**
     * Reverse the migrations.
     */
    public function down()
    {
        Schema::dropIfExists('ignored_sheets');
    }
}
