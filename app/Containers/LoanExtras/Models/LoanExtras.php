<?php

namespace App\Containers\LoanExtras\Models;

use App\Ship\Parents\Models\Model;

class LoanExtras extends Model
{
    protected $fillable = [
        'loan_id',
        'sheet_no',
        'note',
        'breakdown',
    ];

    protected $attributes = [

    ];

    protected $fieldSearchable = [
        'loan_id',
    ];

    protected $hidden = [

    ];

    protected $casts = [

    ];

    protected $dates = [
        'created_at',
        'updated_at',
    ];

    /**
     * A resource key to be used by the the JSON API Serializer responses.
     */
    protected $resourceKey = 'loanextras';
}
