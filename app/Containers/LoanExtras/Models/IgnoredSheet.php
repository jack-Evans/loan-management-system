<?php

namespace App\Containers\LoanExtras\Models;

use App\Ship\Parents\Models\Model;

class IgnoredSheet extends Model
{
    protected $fillable = [
        'sheet_name',
        'sheet_data',
    ];

    protected $attributes = [

    ];

    protected $fieldSearchable = [
        'sheet_name',
    ];

    protected $hidden = [

    ];

    protected $casts = [

    ];

    protected $dates = [
        'created_at',
        'updated_at',
    ];

    /**
     * A resource key to be used by the the JSON API Serializer responses.
     */
    protected $resourceKey = 'ignoredsheets';
}
