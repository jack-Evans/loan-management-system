<?php

namespace App\Containers\LoanExtras\Actions;

use App\Ship\Parents\Actions\Action;
use App\Ship\Parents\Requests\Request;
use Apiato\Core\Foundation\Facades\Apiato;

class GetAllLoanExtrasAction extends Action
{
    public function run(Request $request)
    {
        $loanextras = Apiato::call('LoanExtras@GetAllLoanExtrasTask', [], ['addRequestCriteria']);

        return $loanextras;
    }
}
