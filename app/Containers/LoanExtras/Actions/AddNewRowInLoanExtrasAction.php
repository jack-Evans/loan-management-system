<?php

namespace App\Containers\LoanExtras\Actions;

use App\Ship\Parents\Actions\Action;
use App\Ship\Parents\Requests\Request;
use Apiato\Core\Foundation\Facades\Apiato;

class AddNewRowInLoanExtrasAction extends Action
{
    public function run(Request $request)
    {
        $loanextras = Apiato::call('LoanExtras@GetAllLoanExtrasTask')->where('loan_id', '=', $request->loanid)->first();
        \DB::beginTransaction();
        $note = $breakdown = [];
        if($request->breakdown)
        	$breakdownArr = $this->getArrayFromWebObj($request->breakdown);
        // dd($breakdown);
        if(!$loanextras){
	        $extrasData['loan_id'] = $request->loanid;
	        if($request->note){
		        $note[] = array('Notes');
		        $note[] = $request->note;
	        	$extrasData['note'] 		= json_encode($note);
	        }
	        if($request->breakdown) {
		        $breakdown[] = array('Breakdown');
		        $breakdown[] = $breakdownArr;
		        $extrasData['breakdown'] 	= json_encode($breakdown);
	        }
	        $loanextras = Apiato::call('LoanExtras@CreateLoanExtrasTask', [$extrasData]);
	        // dd($loanextras);
        } else{
        	if(isset($request->note)) {
        		if($loanextras->note){
		        	$note = json_decode($loanextras->note);
		        	array_push($note, $request->note);
	        	} else {
	        		$note[] = array('Notes');
	        		$note[] = $request->note;
	        	}
        		$extrasData['note']	= json_encode($note);
        	} 
        	if(isset($request->breakdown)) {
        		if($loanextras->breakdown){
	    			$breakdown = json_decode($loanextras->breakdown);
		        	array_push($breakdown, $breakdownArr);
	        	} else{
	        		$breakdown[] = array('Breakdown');
	        		$breakdown[] = $breakdownArr;
	        	}
	        	$extrasData['breakdown'] = json_encode($breakdown);
        	}
// dd($extrasData);
	        $loanextras = Apiato::call('LoanExtras@UpdateLoanExtrasTask', [$loanextras->id, $extrasData]);
        }
		\DB::commit();

        return $loanextras;
    }

    public function getArrayFromWebObj($breakdownArr)
    {
    	$breakdown = [];
    	foreach ($breakdownArr as $key => $brkdn) {
    		$breakdown[] = $brkdn;
    	}
    	return $breakdown;
    }
}
