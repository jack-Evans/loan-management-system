<?php

namespace App\Containers\LoanExtras\Actions;

use App\Ship\Parents\Actions\Action;
use App\Ship\Parents\Requests\Request;
use Apiato\Core\Foundation\Facades\Apiato;
use App\Containers\LoanExtras\Models\IgnoredSheet;

class ManageIgnoredSheetsAction extends Action
{
    public function run(Request $request)
    {
    	$request->sheetName = removeExtraSpaces($request->sheetName);
    	
    	if(!$request->sheetName)
    		throw new \App\Containers\Payment\Exceptions\HTTPPreConditionFailedException("Sheet Name field is required.");
    		
        $ignoredSheet = IgnoredSheet::where('sheet_name', 'LIKE', "{$request->sheetName}")->first();
        
        if($ignoredSheet){
        
        	$ignoredSheet->sheet_name = isset($request->sheetName) ? $request->sheetName : $ignoredSheet->sheet_name;
	        $ignoredSheet->sheet_data = isset($request->sheetData) ? $request->sheetData : $ignoredSheet->sheet_data;
	        
	        $ignoredSheet->save();
        
        } else {
	        
	        $ignoredSheet = new IgnoredSheet();

	        $ignoredSheet->sheet_name = $request->sheetName;
	        $ignoredSheet->sheet_data = $request->sheetData;

	        $ignoredSheet->save();

        }

        return $ignoredSheet;
    }
}
