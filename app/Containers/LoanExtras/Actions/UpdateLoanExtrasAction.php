<?php

namespace App\Containers\LoanExtras\Actions;

use App\Ship\Parents\Actions\Action;
use App\Ship\Parents\Requests\Request;
use Apiato\Core\Foundation\Facades\Apiato;

class UpdateLoanExtrasAction extends Action
{
    public function run(Request $request)
    {
        $data = $request->sanitizeInput([
            // add your request data here
        ]);

        $loanextras = Apiato::call('LoanExtras@UpdateLoanExtrasTask', [$request->id, $data]);

        return $loanextras;
    }
}
