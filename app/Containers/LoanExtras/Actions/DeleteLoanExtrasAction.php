<?php

namespace App\Containers\LoanExtras\Actions;

use App\Ship\Parents\Actions\Action;
use App\Ship\Parents\Requests\Request;
use Apiato\Core\Foundation\Facades\Apiato;

class DeleteLoanExtrasAction extends Action
{
    public function run(Request $request)
    {
        return Apiato::call('LoanExtras@DeleteLoanExtrasTask', [$request->id]);
    }
}
