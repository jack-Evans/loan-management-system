<?php

namespace App\Containers\LoanExtras\Actions;

use App\Ship\Parents\Actions\Action;
use App\Ship\Parents\Requests\Request;
use Apiato\Core\Foundation\Facades\Apiato;

class CreateLoanExtrasAction extends Action
{
    public function run(Request $request)
    {
        $loanextras = Apiato::call('LoanExtras@GetAllLoanExtrasTask')->where('loan_id', '=', $request->loanid)->first();
        
        if($loanextras){
        	$extrasData = [
	         	'loan_id' => $request->loanid,   
	         	'sheet_no' => isset($request->sheetNo) ? $request->sheetNo : $loanextras->sheet_no,
	         	'note' => isset($request->note) ? $request->note : $loanextras->note,
	         	'breakdown' => isset($request->breakdown) ? $request->breakdown : $loanextras->breakdown,
	        ];
	        $loanextras = Apiato::call('LoanExtras@UpdateLoanExtrasTask', [$loanextras->id, $extrasData]);
	        // return $loanextras;
        }
        else {
	        $extrasData = [
	         	'loan_id' => $request->loanid,   
	         	'sheet_no' => $request->sheetNo,
	         	'note' => $request->note,
	         	'breakdown' => $request->breakdown,
	        ];
	        $loanextras = Apiato::call('LoanExtras@CreateLoanExtrasTask', [$extrasData]);
        }
	// dd($extrasData);

        return $loanextras;
    }
}
