<?php

namespace App\Containers\LoanExtras\Tasks;

use App\Containers\LoanExtras\Data\Repositories\LoanExtrasRepository;
use App\Ship\Exceptions\NotFoundException;
use App\Ship\Parents\Tasks\Task;
use Exception;

class FindLoanExtrasByIdTask extends Task
{

    protected $repository;

    public function __construct(LoanExtrasRepository $repository)
    {
        $this->repository = $repository;
    }

    public function run($id)
    {
        try {
            return $this->repository->find($id);
        }
        catch (Exception $exception) {
            throw new NotFoundException();
        }
    }
}
