<?php

namespace App\Containers\LoanExtras\Tasks;

use App\Containers\LoanExtras\Data\Repositories\LoanExtrasRepository;
use App\Ship\Exceptions\CreateResourceFailedException;
use App\Ship\Parents\Tasks\Task;
use Exception;

class CreateLoanExtrasTask extends Task
{

    protected $repository;

    public function __construct(LoanExtrasRepository $repository)
    {
        $this->repository = $repository;
    }

    public function run(array $data)
    {
        try {
            return $this->repository->create($data);
        }
        catch (Exception $exception) {
            throw new CreateResourceFailedException();
        }
    }
}
