<?php

namespace App\Containers\LoanExtras\Tasks;

use App\Containers\LoanExtras\Data\Repositories\LoanExtrasRepository;
use App\Ship\Parents\Tasks\Task;

class GetAllLoanExtrasTask extends Task
{

    protected $repository;

    public function __construct(LoanExtrasRepository $repository)
    {
        $this->repository = $repository;
    }

    public function run()
    {
        return $this->repository->all();
    }
}
