<?php

namespace App\Containers\LoanExtras\Tasks;

use App\Containers\LoanExtras\Data\Repositories\LoanExtrasRepository;
use App\Ship\Exceptions\DeleteResourceFailedException;
use App\Ship\Parents\Tasks\Task;
use Exception;

class DeleteLoanExtrasTask extends Task
{

    protected $repository;

    public function __construct(LoanExtrasRepository $repository)
    {
        $this->repository = $repository;
    }

    public function run($id)
    {
        try {
            return $this->repository->delete($id);
        }
        catch (Exception $exception) {
            throw new DeleteResourceFailedException();
        }
    }
}
