<?php

namespace App\Containers\LoanExtras\Tasks;

use App\Containers\LoanExtras\Data\Repositories\LoanExtrasRepository;
use App\Ship\Exceptions\UpdateResourceFailedException;
use App\Ship\Parents\Tasks\Task;
use Exception;

class UpdateLoanExtrasTask extends Task
{

    protected $repository;

    public function __construct(LoanExtrasRepository $repository)
    {
        $this->repository = $repository;
    }

    public function run($id, array $data)
    {
        try {
            return $this->repository->update($data, $id);
        }
        catch (Exception $exception) {
            throw new UpdateResourceFailedException();
        }
    }
}
