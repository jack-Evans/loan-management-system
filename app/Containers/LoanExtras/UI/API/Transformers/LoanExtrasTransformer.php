<?php

namespace App\Containers\LoanExtras\UI\API\Transformers;

use App\Containers\LoanExtras\Models\LoanExtras;
use App\Ship\Parents\Transformers\Transformer;

class LoanExtrasTransformer extends Transformer
{
    /**
     * @var  array
     */
    protected $defaultIncludes = [

    ];

    /**
     * @var  array
     */
    protected $availableIncludes = [

    ];

    /**
     * @param LoanExtras $entity
     *
     * @return array
     */
    public function transform(LoanExtras $entity)
    {
        $response = [
            'object' => 'LoanExtras',
            'id' => $entity->getHashedKey(),
            'created_at' => $entity->created_at,
            'updated_at' => $entity->updated_at,

        ];

        $response = $this->ifAdmin([
            'real_id'    => $entity->id,
            // 'deleted_at' => $entity->deleted_at,
        ], $response);

        return $response;
    }
}
