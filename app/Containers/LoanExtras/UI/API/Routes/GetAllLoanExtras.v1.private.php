<?php

/**
 * @apiGroup           LoanExtras
 * @apiName            getAllLoanExtras
 *
 * @api                {GET} /v1/loanextras Endpoint title here..
 * @apiDescription     Endpoint description here..
 *
 * @apiVersion         1.0.0
 * @apiPermission      none
 *
 * @apiParam           {String}  parameters here..
 *
 * @apiSuccessExample  {json}  Success-Response:
 * HTTP/1.1 200 OK
{
  // Insert the response of the request here...
}
 */

/** @var Route $router */
$router->get('loanextras', [
    'as' => 'api_loanextras_get_all_loan_extras',
    'uses'  => 'Controller@getAllLoanExtras',
    'middleware' => [
      'auth:api',
    ],
]);
