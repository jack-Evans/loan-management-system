<?php

/**
 * @apiGroup           LoanExtras
 * @apiName            findLoanExtrasById
 *
 * @api                {GET} /v1/loanextras/:id Endpoint title here..
 * @apiDescription     Endpoint description here..
 *
 * @apiVersion         1.0.0
 * @apiPermission      none
 *
 * @apiParam           {String}  parameters here..
 *
 * @apiSuccessExample  {json}  Success-Response:
 * HTTP/1.1 200 OK
{
  // Insert the response of the request here...
}
 */

/** @var Route $router */
$router->get('loanextras/{id}', [
    'as' => 'api_loanextras_find_loan_extras_by_id',
    'uses'  => 'Controller@findLoanExtrasById',
    'middleware' => [
      'auth:api',
    ],
]);
