<?php

/**
 * @apiGroup           LoanExtras
 * @apiName            deleteLoanExtras
 *
 * @api                {DELETE} /v1/loanextras/:id Endpoint title here..
 * @apiDescription     Endpoint description here..
 *
 * @apiVersion         1.0.0
 * @apiPermission      none
 *
 * @apiParam           {String}  parameters here..
 *
 * @apiSuccessExample  {json}  Success-Response:
 * HTTP/1.1 200 OK
{
  // Insert the response of the request here...
}
 */

/** @var Route $router */
$router->delete('loanextras/{id}', [
    'as' => 'api_loanextras_delete_loan_extras',
    'uses'  => 'Controller@deleteLoanExtras',
    'middleware' => [
      'auth:api',
    ],
]);
