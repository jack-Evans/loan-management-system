<?php

/**
 * @apiGroup           LoanExtras
 * @apiName            updateLoanExtras
 *
 * @api                {PATCH} /v1/loanextras/:id Endpoint title here..
 * @apiDescription     Endpoint description here..
 *
 * @apiVersion         1.0.0
 * @apiPermission      none
 *
 * @apiParam           {String}  parameters here..
 *
 * @apiSuccessExample  {json}  Success-Response:
 * HTTP/1.1 200 OK
{
  // Insert the response of the request here...
}
 */

/** @var Route $router */
$router->patch('loanextras/{id}', [
    'as' => 'api_loanextras_update_loan_extras',
    'uses'  => 'Controller@updateLoanExtras',
    'middleware' => [
      'auth:api',
    ],
]);
