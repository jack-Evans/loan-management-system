<?php

/**
 * @apiGroup           LoanExtras
 * @apiName            addNewRowInLoanExtras
 *
 * @api                {POST} /v1/loanextras/addnewrow Endpoint title here..
 * @apiDescription     Endpoint description here..
 *
 * @apiVersion         1.0.0
 * @apiPermission      none
 *
 * @apiParam           {String}  parameters here..
 *
 * @apiSuccessExample  {json}  Success-Response:
 * HTTP/1.1 200 OK
{
  // Insert the response of the request here...
}
 */

/** @var Route $router */
$router->post('loanextras/addnewrow', [
    'as' => 'api_loanextras_create_loan_extras',
    'uses'  => 'Controller@addNewRowInLoanExtras',
    'middleware' => [
      'auth:api',
    ],
]);
