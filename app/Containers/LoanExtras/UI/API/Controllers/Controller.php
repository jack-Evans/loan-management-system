<?php

namespace App\Containers\LoanExtras\UI\API\Controllers;

use App\Containers\LoanExtras\UI\API\Requests\CreateLoanExtrasRequest;
use App\Containers\LoanExtras\UI\API\Requests\DeleteLoanExtrasRequest;
use App\Containers\LoanExtras\UI\API\Requests\GetAllLoanExtrasRequest;
use App\Containers\LoanExtras\UI\API\Requests\FindLoanExtrasByIdRequest;
use App\Containers\LoanExtras\UI\API\Requests\UpdateLoanExtrasRequest;
use App\Containers\LoanExtras\UI\API\Requests\AddNewRowInLoanExtrasRequest;
use App\Containers\LoanExtras\UI\API\Transformers\LoanExtrasTransformer;
use App\Ship\Parents\Controllers\ApiController;
use Apiato\Core\Foundation\Facades\Apiato;

/**
 * Class Controller
 *
 * @package App\Containers\LoanExtras\UI\API\Controllers
 */
class Controller extends ApiController
{
    /**
     * @param CreateLoanExtrasRequest $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function createLoanExtras(CreateLoanExtrasRequest $request)
    {
        $loanextras = Apiato::call('LoanExtras@CreateLoanExtrasAction', [$request]);

        return $this->created($this->transform($loanextras, LoanExtrasTransformer::class));
    }

    /**
     * @param FindLoanExtrasByIdRequest $request
     * @return array
     */
    public function findLoanExtrasById(FindLoanExtrasByIdRequest $request)
    {
        $loanextras = Apiato::call('LoanExtras@FindLoanExtrasByIdAction', [$request]);

        return $this->transform($loanextras, LoanExtrasTransformer::class);
    }

    /**
     * @param GetAllLoanExtrasRequest $request
     * @return array
     */
    public function getAllLoanExtras(GetAllLoanExtrasRequest $request)
    {
        $loanextras = Apiato::call('LoanExtras@GetAllLoanExtrasAction', [$request]);

        return $this->transform($loanextras, LoanExtrasTransformer::class);
    }

    /**
     * @param UpdateLoanExtrasRequest $request
     * @return array
     */
    public function updateLoanExtras(UpdateLoanExtrasRequest $request)
    {
        $loanextras = Apiato::call('LoanExtras@UpdateLoanExtrasAction', [$request]);

        return $this->transform($loanextras, LoanExtrasTransformer::class);
    }

    /**
     * @param DeleteLoanExtrasRequest $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function deleteLoanExtras(DeleteLoanExtrasRequest $request)
    {
        Apiato::call('LoanExtras@DeleteLoanExtrasAction', [$request]);

        return $this->noContent();
    }

    /**
     * @param DeleteLoanExtrasRequest $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function addNewRowInLoanExtras(AddNewRowInLoanExtrasRequest $request)
    {
        Apiato::call('LoanExtras@AddNewRowInLoanExtrasAction', [$request]);

        return $this->noContent();
    }
}
