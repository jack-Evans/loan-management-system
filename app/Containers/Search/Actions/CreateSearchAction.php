<?php

namespace App\Containers\Search\Actions;

use App\Ship\Parents\Actions\Action;
use App\Ship\Parents\Requests\Request;
use Apiato\Core\Foundation\Facades\Apiato;
use App\Containers\Tag\Models\Tag;

class CreateSearchAction extends Action
{
    public function run(Request $request)
    {
        $data = [
            'name' => $request->name,
            'tags' => is_array($request->tags) ? json_encode($request->tags) : $request->tags,
            'operators' => is_array($request->operators) ? json_encode($request->operators) : $request->operators,
        ];
        $operators = json_decode($data['operators']);
        $tagIds = [];
        foreach(json_decode($data['tags']) as $tagId){
            $realTagId = \Hashids::decode($tagId)[0];
            array_push($tagIds, $realTagId);
        }

        $tags = Tag::whereIn('id', $tagIds)->get()->pluck('name')->toArray();
        $searchQuery = '';
        foreach ($tags as $key => $tag) {
            $searchQuery .= $tag.' ';
            if($key+1 < count($tags))
                $searchQuery .= array_splice($operators, 0, 1)[0].' ';
        }
        $data['search_query'] = $searchQuery;
        
        $search = Apiato::call('Search@CreateSearchTask', [$data]);

        return $search;
    }
}
