<?php

namespace App\Containers\Search\Actions;

use App\Ship\Parents\Actions\Action;
use App\Ship\Parents\Requests\Request;
use Apiato\Core\Foundation\Facades\Apiato;

class GetAllSearchesAction extends Action
{
    public function run(Request $request)
    {
        $searches = Apiato::call('Search@GetAllSearchesTask', [], ['addRequestCriteria']);

        return $searches;
    }
}
