<?php

namespace App\Containers\Search\Actions;

use App\Ship\Parents\Actions\Action;
use App\Ship\Parents\Requests\Request;
use Apiato\Core\Foundation\Facades\Apiato;

class DeleteSearchAction extends Action
{
    public function run(Request $request)
    {
        return Apiato::call('Search@DeleteSearchTask', [$request->id]);
    }
}
