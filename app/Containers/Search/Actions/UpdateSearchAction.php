<?php

namespace App\Containers\Search\Actions;

use App\Ship\Parents\Actions\Action;
use App\Ship\Parents\Requests\Request;
use Apiato\Core\Foundation\Facades\Apiato;

class UpdateSearchAction extends Action
{
    public function run(Request $request)
    {
        $data = $request->sanitizeInput([
            // add your request data here
        ]);

        $search = Apiato::call('Search@UpdateSearchTask', [$request->id, $data]);

        return $search;
    }
}
