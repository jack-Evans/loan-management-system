<?php

namespace App\Containers\Search\Actions;

use App\Ship\Parents\Actions\Action;
use App\Ship\Parents\Requests\Request;
use Apiato\Core\Foundation\Facades\Apiato;

class FindSearchByIdAction extends Action
{
    public function run(Request $request)
    {
        $search = Apiato::call('Search@FindSearchByIdTask', [$request->id]);

        return $search;
    }
}
