<?php

namespace App\Containers\Search\Tasks;

use App\Containers\Search\Data\Repositories\SearchRepository;
use App\Ship\Parents\Tasks\Task;

class GetAllSearchesTask extends Task
{

    protected $repository;

    public function __construct(SearchRepository $repository)
    {
        $this->repository = $repository;
    }

    public function run()
    {
        return $this->repository->paginate();
    }
}
