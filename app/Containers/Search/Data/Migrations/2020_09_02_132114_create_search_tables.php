<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateSearchTables extends Migration
{

    /**
     * Run the migrations.
     */
    public function up()
    {
        Schema::create('searches', function (Blueprint $table) {

            $table->increments('id');
            $table->string('name')->nullable();
            $table->string('tags')->nullable();
            $table->string('operators')->nullable();
            $table->string('search_query')->nullable();
            $table->timestamps();
            $table->softDeletes();

        });
    }

    /**
     * Reverse the migrations.
     */
    public function down()
    {
        Schema::dropIfExists('searches');
    }
}
