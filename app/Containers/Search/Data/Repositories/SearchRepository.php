<?php

namespace App\Containers\Search\Data\Repositories;

use App\Ship\Parents\Repositories\Repository;

/**
 * Class SearchRepository
 */
class SearchRepository extends Repository
{

    /**
     * @var array
     */
    protected $fieldSearchable = [
        'id' => '=',
        // ...
    ];

}
