<?php

namespace App\Containers\Search\UI\API\Transformers;

use App\Containers\Search\Models\Search;
use App\Ship\Parents\Transformers\Transformer;

class SearchTransformer extends Transformer
{
    /**
     * @var  array
     */
    protected $defaultIncludes = [

    ];

    /**
     * @var  array
     */
    protected $availableIncludes = [

    ];

    /**
     * @param Search $entity
     *
     * @return array
     */
    public function transform(Search $entity)
    {
        $response = [
            'object' => 'Search',
            'id' => $entity->getHashedKey(),
            'tags' => json_decode($entity->tags),
            'operators' => json_decode($entity->operators),
            'search_query' => $entity->search_query,
            'created_at' => $entity->created_at,
            'updated_at' => $entity->updated_at,

        ];

        $response = $this->ifAdmin([
            'real_id'    => $entity->id,
            // 'deleted_at' => $entity->deleted_at,
        ], $response);

        return $response;
    }
}
