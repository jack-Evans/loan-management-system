<?php

/**
 * @apiGroup           Search
 * @apiName            updateSearch
 *
 * @api                {PATCH} /v1/searches/:id Endpoint title here..
 * @apiDescription     Endpoint description here..
 *
 * @apiVersion         1.0.0
 * @apiPermission      none
 *
 * @apiParam           {String}  parameters here..
 *
 * @apiSuccessExample  {json}  Success-Response:
 * HTTP/1.1 200 OK
{
  // Insert the response of the request here...
}
 */

/** @var Route $router */
$router->patch('searches/{id}', [
    'as' => 'api_search_update_search',
    'uses'  => 'Controller@updateSearch',
    'middleware' => [
      'auth:api',
    ],
]);
