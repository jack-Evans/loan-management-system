<?php

/**
 * @apiGroup           Search
 * @apiName            deleteSearch
 *
 * @api                {DELETE} /v1/searches/:id Endpoint title here..
 * @apiDescription     Endpoint description here..
 *
 * @apiVersion         1.0.0
 * @apiPermission      none
 *
 * @apiParam           {String}  parameters here..
 *
 * @apiSuccessExample  {json}  Success-Response:
 * HTTP/1.1 200 OK
{
  // Insert the response of the request here...
}
 */

/** @var Route $router */
$router->delete('searches/{id}', [
    'as' => 'api_search_delete_search',
    'uses'  => 'Controller@deleteSearch',
    'middleware' => [
      'auth:api',
    ],
]);
