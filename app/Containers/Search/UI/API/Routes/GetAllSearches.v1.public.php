<?php

/**
 * @apiGroup           Search
 * @apiName            getAllSearches
 *
 * @api                {GET} /v1/searches Endpoint title here..
 * @apiDescription     Endpoint description here..
 *
 * @apiVersion         1.0.0
 * @apiPermission      none
 *
 * @apiParam           {String}  parameters here..
 *
 * @apiSuccessExample  {json}  Success-Response:
 * HTTP/1.1 200 OK
{
  // Insert the response of the request here...
}
 */

/** @var Route $router */
$router->get('searches', [
    'as' => 'api_search_get_all_searches',
    'uses'  => 'Controller@getAllSearches',
    // 'middleware' => [
    //   'auth:api',
    // ],
]);
