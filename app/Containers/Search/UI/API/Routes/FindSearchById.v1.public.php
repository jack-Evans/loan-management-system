<?php

/**
 * @apiGroup           Search
 * @apiName            findSearchById
 *
 * @api                {GET} /v1/searches/:id Endpoint title here..
 * @apiDescription     Endpoint description here..
 *
 * @apiVersion         1.0.0
 * @apiPermission      none
 *
 * @apiParam           {String}  parameters here..
 *
 * @apiSuccessExample  {json}  Success-Response:
 * HTTP/1.1 200 OK
{
  // Insert the response of the request here...
}
 */

/** @var Route $router */
$router->get('searches/{id}', [
    'as' => 'api_search_find_search_by_id',
    'uses'  => 'Controller@findSearchById',
    'middleware' => [
      'auth:api',
    ],
]);
