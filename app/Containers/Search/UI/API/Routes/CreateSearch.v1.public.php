<?php

/**
 * @apiGroup           Search
 * @apiName            createSearch
 *
 * @api                {POST} /v1/searches Endpoint title here..
 * @apiDescription     Endpoint description here..
 *
 * @apiVersion         1.0.0
 * @apiPermission      none
 *
 * @apiParam           {String}  parameters here..
 *
 * @apiSuccessExample  {json}  Success-Response:
 * HTTP/1.1 200 OK
{
  // Insert the response of the request here...
}
 */

/** @var Route $router */
$router->post('searches', [
    'as' => 'api_search_create_search',
    'uses'  => 'Controller@createSearch',
    // 'middleware' => [
    //   'auth:api',
    // ],
]);
