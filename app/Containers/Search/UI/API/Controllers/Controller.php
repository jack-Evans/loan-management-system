<?php

namespace App\Containers\Search\UI\API\Controllers;

use App\Containers\Search\UI\API\Requests\CreateSearchRequest;
use App\Containers\Search\UI\API\Requests\DeleteSearchRequest;
use App\Containers\Search\UI\API\Requests\GetAllSearchesRequest;
use App\Containers\Search\UI\API\Requests\FindSearchByIdRequest;
use App\Containers\Search\UI\API\Requests\UpdateSearchRequest;
use App\Containers\Search\UI\API\Transformers\SearchTransformer;
use App\Ship\Parents\Controllers\ApiController;
use Apiato\Core\Foundation\Facades\Apiato;

/**
 * Class Controller
 *
 * @package App\Containers\Search\UI\API\Controllers
 */
class Controller extends ApiController
{
    /**
     * @param CreateSearchRequest $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function createSearch(CreateSearchRequest $request)
    {
        $search = Apiato::call('Search@CreateSearchAction', [$request]);

        return $this->created($this->transform($search, SearchTransformer::class));
    }

    /**
     * @param FindSearchByIdRequest $request
     * @return array
     */
    public function findSearchById(FindSearchByIdRequest $request)
    {
        $search = Apiato::call('Search@FindSearchByIdAction', [$request]);

        return $this->transform($search, SearchTransformer::class);
    }

    /**
     * @param GetAllSearchesRequest $request
     * @return array
     */
    public function getAllSearches(GetAllSearchesRequest $request)
    {
        $searches = Apiato::call('Search@GetAllSearchesAction', [$request]);

        return $this->transform($searches, SearchTransformer::class);
    }

    /**
     * @param UpdateSearchRequest $request
     * @return array
     */
    public function updateSearch(UpdateSearchRequest $request)
    {
        $search = Apiato::call('Search@UpdateSearchAction', [$request]);

        return $this->transform($search, SearchTransformer::class);
    }

    /**
     * @param DeleteSearchRequest $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function deleteSearch(DeleteSearchRequest $request)
    {
        Apiato::call('Search@DeleteSearchAction', [$request]);

        return $this->noContent();
    }
}
