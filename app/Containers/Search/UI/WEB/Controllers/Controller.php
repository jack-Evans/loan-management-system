<?php

namespace App\Containers\Search\UI\WEB\Controllers;

use App\Containers\Search\UI\WEB\Requests\CreateSearchRequest;
use App\Containers\Search\UI\WEB\Requests\DeleteSearchRequest;
use App\Containers\Search\UI\WEB\Requests\GetAllSearchesRequest;
use App\Containers\Search\UI\WEB\Requests\FindSearchByIdRequest;
use App\Containers\Search\UI\WEB\Requests\UpdateSearchRequest;
use App\Containers\Search\UI\WEB\Requests\StoreSearchRequest;
use App\Containers\Search\UI\WEB\Requests\EditSearchRequest;
use App\Ship\Parents\Controllers\WebController;
use Apiato\Core\Foundation\Facades\Apiato;

/**
 * Class Controller
 *
 * @package App\Containers\Search\UI\WEB\Controllers
 */
class Controller extends WebController
{
    /**
     * Show all entities
     *
     * @param GetAllSearchesRequest $request
     */
    public function index(GetAllSearchesRequest $request)
    {
        $searches = Apiato::call('Search@GetAllSearchesAction', [$request]);

        // ..
    }

    /**
     * Show one entity
     *
     * @param FindSearchByIdRequest $request
     */
    public function show(FindSearchByIdRequest $request)
    {
        $search = Apiato::call('Search@FindSearchByIdAction', [$request]);

        // ..
    }

    /**
     * Create entity (show UI)
     *
     * @param CreateSearchRequest $request
     */
    public function create(CreateSearchRequest $request)
    {
        // ..
    }

    /**
     * Add a new entity
     *
     * @param StoreSearchRequest $request
     */
    public function store(StoreSearchRequest $request)
    {
        $search = Apiato::call('Search@CreateSearchAction', [$request]);

        // ..
    }

    /**
     * Edit entity (show UI)
     *
     * @param EditSearchRequest $request
     */
    public function edit(EditSearchRequest $request)
    {
        $search = Apiato::call('Search@GetSearchByIdAction', [$request]);

        // ..
    }

    /**
     * Update a given entity
     *
     * @param UpdateSearchRequest $request
     */
    public function update(UpdateSearchRequest $request)
    {
        $search = Apiato::call('Search@UpdateSearchAction', [$request]);

        // ..
    }

    /**
     * Delete a given entity
     *
     * @param DeleteSearchRequest $request
     */
    public function delete(DeleteSearchRequest $request)
    {
         $result = Apiato::call('Search@DeleteSearchAction', [$request]);

         // ..
    }
}
