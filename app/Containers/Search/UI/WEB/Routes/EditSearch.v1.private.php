<?php

/** @var Route $router */
$router->get('searches/{id}/edit', [
    'as' => 'web_search_edit',
    'uses'  => 'Controller@edit',
    'middleware' => [
      'auth:web',
    ],
]);
