<?php

/** @var Route $router */
$router->patch('searches/{id}', [
    'as' => 'web_search_update',
    'uses'  => 'Controller@update',
    'middleware' => [
      'auth:web',
    ],
]);
