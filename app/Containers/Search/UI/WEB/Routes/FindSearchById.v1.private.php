<?php

/** @var Route $router */
$router->get('searches/{id}', [
    'as' => 'web_search_show',
    'uses'  => 'Controller@show',
    'middleware' => [
      'auth:web',
    ],
]);
