<?php

/** @var Route $router */
$router->get('searches/create', [
    'as' => 'web_search_create',
    'uses'  => 'Controller@create',
    'middleware' => [
      'auth:web',
    ],
]);
