<?php

/** @var Route $router */
$router->post('searches/store', [
    'as' => 'web_search_store',
    'uses'  => 'Controller@store',
    'middleware' => [
      'auth:web',
    ],
]);
