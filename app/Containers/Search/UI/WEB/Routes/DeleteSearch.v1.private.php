<?php

/** @var Route $router */
$router->delete('searches/{id}', [
    'as' => 'web_search_delete',
    'uses'  => 'Controller@delete',
    'middleware' => [
      'auth:web',
    ],
]);
