<?php

/** @var Route $router */
$router->get('searches', [
    'as' => 'web_search_index',
    'uses'  => 'Controller@index',
    'middleware' => [
      'auth:web',
    ],
]);
