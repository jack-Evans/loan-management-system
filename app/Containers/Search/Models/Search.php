<?php

namespace App\Containers\Search\Models;

use App\Ship\Parents\Models\Model;

class Search extends Model
{
    protected $fillable = [
        'name',
        'tags',
        'operators',
        'search_query',
    ];

    protected $attributes = [

    ];

    protected $hidden = [

    ];

    protected $casts = [

    ];

    protected $dates = [
        'created_at',
        'updated_at',
    ];

    /**
     * A resource key to be used by the the JSON API Serializer responses.
     */
    protected $resourceKey = 'searches';
}
