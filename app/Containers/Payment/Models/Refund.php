<?php

namespace App\Containers\Payment\Models;

use App\Ship\Parents\Models\Model;

class Refund extends Model
{
    protected $fillable = [
        'name',
        'amount',
        'refund_at',
        'loan_id',
    ];

    protected $attributes = [

    ];

    protected $hidden = [

    ];

    protected $casts = [

    ];

    protected $dates = [
        'created_at',
        'updated_at',
    ];

    /**
     * A resource key to be used by the the JSON API Serializer responses.
     */
    protected $resourceKey = 'refunds';

    public function journal() {
        return $this->morphOne(\App\Containers\Payment\Models\Journal::class, 'payable');
    }

}
