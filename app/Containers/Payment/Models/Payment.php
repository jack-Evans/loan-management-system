<?php

namespace App\Containers\Payment\Models;

use App\Ship\Parents\Models\Model;

class Payment extends Model
{
    protected $fillable = [
        'amount',
        'status',
        'paid_at',
        'loan_id',
        'is_capital_reduction',
    ];

    protected $attributes = [

    ];

    protected $hidden = [

    ];

    protected $casts = [

    ];

    protected $dates = [
        'created_at',
        'updated_at',
    ];

    /**
     * A resource key to be used by the the JSON API Serializer responses.
     */
    protected $resourceKey = 'payments';

    public function journal() {
        return $this->morphOne(\App\Containers\Payment\Models\Journal::class, 'payable');
    }



}
