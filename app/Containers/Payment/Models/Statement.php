<?php

namespace App\Containers\Payment\Models;

use App\Ship\Parents\Models\Model;

class Statement extends Model
{
    protected $fillable = [

    ];

    protected $attributes = [
         'id',
         'created_at',
         'debit',
         'credit',
         'balance',
         'arrears',
         'description',
         'comments',
         'interestBreakDown',
         'nominalCode',
         'hide',
         'hide_transaction',
         'notes',
    ];

    protected $hidden = [

    ];

    protected $casts = [

    ];

    protected $dates = [
        // 'created_at',
        // 'updated_at',
    ];

    /**
     * A resource key to be used by the the JSON API Serializer responses.
     */
    protected $resourceKey = 'statements';

    public function setIdAttribute($value){
        $this->attributes['id'] = $value;
    }

    public function setCreatedAtAttribute($value){
        $this->attributes['created_at'] = $value;
    }

    public function setDebitAttribute($value){
        $this->attributes['debit'] = $value;
    }

    public function setCreditAttribute($value){
        $this->attributes['credit'] = $value;
    }

    public function setBalanceAttribute($value){
        $this->attributes['balance'] = $value;
    }

    public function setArrearsAttribute($value){
        $this->attributes['arrears'] = $value;
    }

    public function setDescriptionAttribute($value){
        $this->attributes['description'] = $value;
    }

    public function setCommentsAttribute($value){
        $this->attributes['comments'] = $value;
    }

    public function setInterestBreakDownAttribute($value){
        $this->attributes['interestBreakDown'] = $value;
    }

    public function setNominalCodeAttribute($value){
        $this->attributes['nominalCode'] = $value;
    }

    public function setTransactionIdAttribute($value){
        $this->attributes['transactionId'] = $value;
    }

    public function setHideAttribute($value){
        $this->attributes['hide'] = $value;
    }

    public function setHideTransactionAttribute($value){
        $this->attributes['hideTransaction'] = $value;
    }

    public function setNotesAttribute($value){
        $this->attributes['notes'] = $value;
    }
}
