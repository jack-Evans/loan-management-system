<?php

namespace App\Containers\Payment\Models;

use App\Ship\Parents\Models\Model;

class PaymentPlan extends Model
{
    protected $fillable = [
        'id',
        'plan_version',
        'amount',
        'amount_paid',
        'amount_pending',
        'expected_balance',
        'status',
        'paid_at',
        'pay_before',
        'type',
        'loan_id',
        'payment_break_down',
    ];

    protected $attributes = [
//        'amount',
//        'amount_paid',
//        'status',
//        'paid_at',
//        'pay_before',
//        'type',
//        'loan_id',
    ];

    protected $hidden = [

    ];

    protected $casts = [

    ];

    protected $dates = [
        'created_at',
        'updated_at',
    ];

    /**
     * A resource key to be used by the the JSON API Serializer responses.
     */
    protected $resourceKey = 'paymentplans';
    
    public function setIdAttribute($value){
        $this->attributes['id'] = $value;
    }
    public function setPlanVersionAttribute($value){
        $this->attributes['plan_version'] = $value;
    }
    public function setAmountAttribute($value){
        $this->attributes['amount'] = $value;
    }
    public function setAmountPaidAttribute($value){
        $this->attributes['amount_paid'] = $value;
    }
    public function setAmountPendingAttribute($value){
        $this->attributes['amount_pending'] = $value;
    }
    public function setExpectedBalanceAttribute($value){
        $this->attributes['expected_balance'] = $value;
    }
    public function setStatusAttribute($value){
        $this->attributes['status'] = $value;
    }
    public function setPayBeforeAttribute($value){
        $this->attributes['pay_before'] = $value;
    }
    public function setTypeAttribute($value){
        $this->attributes['type'] = $value;
    }
    public function setLoanIdAttribute($value){
        $this->attributes['loan_id'] = $value;
    }
    public function setCreatedAtAttribute($value){
        $this->attributes['created_at'] = $value;
    }
    public function setUpdatedAtAttribute($value){
        $this->attributes['updated_at'] = $value;
    }
    public function setPaymentBreakDownAttribute($value){
        $this->attributes['payment_break_down'] = $value;
    }
}
