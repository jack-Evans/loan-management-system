<?php

namespace App\Containers\Payment\Models;

use App\Ship\Parents\Models\Model;

class Account extends Model
{
    protected $fillable = [
        'customer_id',
        'credit',
        'debit',
        'balance',
        'status'
    ];

    protected $attributes = [

    ];

    protected $hidden = [

    ];

    protected $casts = [

    ];

    protected $dates = [
        'created_at',
        'updated_at',
    ];

    /**
     * A resource key to be used by the the JSON API Serializer responses.
     */
    protected $resourceKey = 'accounts';
}
