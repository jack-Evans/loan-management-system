<?php

namespace App\Containers\Payment\Models;

use App\Ship\Parents\Models\Model;

class MonthStatement extends Model
{
    protected $fillable = [

    ];

    protected $attributes = [];

    protected $hidden = [];

    protected $casts = [];

    protected $dates = [];

    /**
     * A resource key to be used by the the JSON API Serializer responses.
     */
    protected $resourceKey = 'monthstatements';

    public function setOpeningBalanceAttribute($value){
        $this->attributes['opening_balance'] = $value;
    }

    public function setClosingBalanceAttribute($value){
        $this->attributes['closing_balance'] = $value;
    }

    public function setGrossLoanAttribute($value){
        $this->attributes['gross_loan'] = $value;
    }

    public function setInterestChargedAttribute($value){
        $this->attributes['interest_charged'] = $value;
    }

    public function setStartDateAttribute($value){
        $this->attributes['start_date'] = $value;
    }

    public function setEndDateAttribute($value){
        $this->attributes['end_date'] = $value;
    }

    public function setCurrentInterestRateAttribute($value){
        $this->attributes['current_interest_rate'] = $value;
    }

    public function setInterestDueDateAttribute($value){
        $this->attributes['interest_due_date'] = $value;
    }

    public function setLoanEndDateAttribute($value){
        $this->attributes['loan_end_date'] = $value;
    }
}
