<?php

namespace App\Containers\Payment\Models;

use App\Ship\Parents\Models\Model;

class DefaultRateRefund extends Model
{
    protected $fillable = [
        'loan_id',
        'start_date',
        'end_date',
        'days',
        'rate',
        'daily_rate',
        'refund_amount',
        'balance',
        'refund_at',
    ];

    protected $attributes = [

    ];

    protected $hidden = [

    ];

    protected $casts = [

    ];

    protected $dates = [
        'created_at',
        'updated_at',
    ];

    /**
     * A resource key to be used by the the JSON API Serializer responses.
     */
    protected $resourceKey = 'defaultraterefund';
}
