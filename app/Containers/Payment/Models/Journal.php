<?php

namespace App\Containers\Payment\Models;

use App\Ship\Parents\Models\Model;

class Journal extends Model
{
    protected $fillable = [
        'default_credit_account_id',
        'default_debit_account_id',
        'loan_id',
        'invoice_id',
        'type',
        'comment',
        'tnx_date',
        'invoice',
        'nominal_code_id',
        'payable_id',
        'payable_type',
        'hide',
        'hide_transaction',
        'notes',
    ];

    protected $attributes = [

    ];

    protected $hidden = [

    ];

    protected $casts = [

    ];

    protected $dates = [
        'created_at',
        'updated_at',
        'tnx_date',
    ];

    /**
     * A resource key to be used by the the JSON API Serializer responses.
     */
    protected $resourceKey = 'journals';

    public function posts() {
        return $this->hasMany(\App\Containers\Payment\Models\Post::class);
    }
    
    public function creditPosts() {
        return $this->hasMany(\App\Containers\Payment\Models\Post::class)->where(['type' => 'credit']);
    }

    public function nominalcode() {
        return $this->belongsTo(\App\Containers\NominalCode\Models\NominalCode::class, 'nominal_code_id', 'id');
    }

    public function payable()
    {
        return $this->morphTo();
    }

    public function tags() {
        return $this->belongsToMany(\App\Containers\Tag\Models\Tag::class, 'journal_tags');
    }
}
