<?php

namespace App\Containers\Payment\Models;

use App\Ship\Parents\Models\Model;

class InterestBreakDown extends Model
{
    protected $fillable = [

    ];

    protected $attributes = [
    ];

    protected $hidden = [

    ];

    protected $casts = [

    ];

    protected $dates = [
        // 'created_at',
        // 'updated_at',
    ];

    /**
     * A resource key to be used by the the JSON API Serializer responses.
     */
    protected $resourceKey = 'InterestBreakDown';

    public function setDailyRateAttribute($value){
        $this->attributes['daily_rate'] = $value;
    }
    public function setDaysAttribute($value){
        $this->attributes['days'] = $value;
    }
    public function setBalanceAttribute($value){
        $this->attributes['balance'] = $value;
    }
    public function setRateAttribute($value){
        $this->attributes['rate'] = $value;
    }
    public function setTotalAttribute($value){
        $this->attributes['total'] = $value;
    }
    public function setStartAttribute($value){
        $this->attributes['start'] = $value;
    }
    public function setEndAttribute($value){
        $this->attributes['end'] = $value;
    }
    public function setRefundedAttribute($value){
        $this->attributes['refunded'] = $value;
    }
    public function setRoundingError($value){
        $this->attributes['rounding_error'] = $value;
    }
    public function setHideAttribute($value){
        $this->attributes['hide'] = $value;
    }
    public function setHideTransactionAttribute($value){
        $this->attributes['hideTransaction'] = $value;
    }
    
}
