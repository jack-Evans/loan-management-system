<?php

namespace App\Containers\Payment\Tasks;

use App\Ship\Parents\Tasks\Task;
use App\Containers\Loan\Models\Loan;

class AddDefaultChargeTask extends Task
{
    public function run(Loan $loan)
    {
        $interest 		 = $loan->interest;
        $defaultRate 	 = interest($interest->gross_loan, $interest->default_rate, $interest->duration);
        $debitAccountId = $loan->customer->account->id;

        $perDayDefaultRate	= perDay($defaultRate);
        $dt					= \Carbon\Carbon::today();
        $accountingPeriod	= "{$dt->format('M')} {$dt->year}";
        try {
        	\DB::beginTransaction();
	    	$journalPosting = [
                'creditAccountId' => 0,
                'debitAccountId' => $debitAccountId,
                'type' => 'default',
                'loanid' => $loan->id,
                'tnxDate' => $dt,
                'amount' => $perDayDefaultRate,
                'accountingPeriod' => $accountingPeriod,
                'assetType' => 'default',
                'description' => '',
                'payableId' => 0,
                'payableType' => ''
            ];
            $refundJournalPosting = Apiato::call('Payment@CreateJournalPostingTask', [$journalPosting]);

	    	\DB::commit();
		}
	    catch (Exception $exception) {
	    	\DB::rollback();
	        throw new CreateResourceFailedException();
	    }

	    return $defaultCharge;

    }
}
