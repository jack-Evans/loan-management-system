<?php

namespace App\Containers\Payment\Tasks;

use App\Containers\Payment\Data\Repositories\RefundRepository;
use App\Ship\Exceptions\UpdateResourceFailedException;
use App\Ship\Parents\Tasks\Task;

class UpdateRefundTask extends Task
{

    protected $refundRepository;

    public function __construct(RefundRepository $refundRepository)
    {
        $this->refundRepository = $refundRepository;
    }

    public function run($id, array $data)
    {
        try {
            return $this->refundRepository->update($data, $id);            
        }
        catch (Exception $exception) {
            throw new UpdateResourceFailedException();
        }
    }
}
