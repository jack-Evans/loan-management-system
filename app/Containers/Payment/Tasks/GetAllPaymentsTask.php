<?php

namespace App\Containers\Payment\Tasks;

use App\Containers\Payment\Data\Repositories\PaymentRepository;
use App\Ship\Parents\Tasks\Task;

class GetAllPaymentsTask extends Task
{

    protected $repository;

    public function __construct(PaymentRepository $repository)
    {
        $this->repository = $repository;
    }

    public function run(\App\Containers\Loan\Models\Loan $loan)
    {
        return $this->repository->with(['journal'])->orderBy('paid_at', 'ASC')->scopeQuery(function($query) use ($loan){
            $query->where('amount', '>', 0);
        	return $query->where(['loan_id' => $loan->id]);
        })->all();
    }
}
