<?php

namespace App\Containers\Payment\Tasks;

use App\Ship\Parents\Tasks\Task;
use Apiato\Core\Traits\ResponseTrait;
use Apiato\Core\Foundation\Facades\Apiato;
use App\Containers\Loan\Models\Loan;
use Carbon\Carbon;

class GetManualLoanStatementsTask extends Task
{
    use ResponseTrait;

    protected $statements;
    protected $balance;

    public function run(Loan $loan)
    {
        $this->statements = [];
        $getJournals = Apiato::call('Payment@GetJournalsTask', [$loan]);
        $journals = $getJournals['journals']->where('type', '!=', 'skip');

        foreach($journals as $key => $journal)
        {
            if($journal->type === 'payment' || $journal->type === 'refund' || $journal->type === 'refund_default_interest' || $journal->type === 'other_charge_refund' || $journal->type === 'other_charge_adjustment' || $journal->type === 'credit_filler') {
                $transactions = Apiato::call('Payment@SetTransactionsTask', [$loan, $journal, $this->balance, []]);
                $statement = $transactions['statement'];
                array_push($this->statements, $statement);
                $this->balance = $transactions['balance'];
            }
            else
            {
                $transactions = Apiato::call('Payment@SetTransactionsTask', [$loan, $journal, $this->balance, []]);
                $statement = $transactions['statement'];
                array_push($this->statements, $statement);
                $this->balance = $transactions['balance'];
            }
        }
        // if($this->balance <= 0){
            $totalClosingChargesAmount = $this->closingChargesTnxs($loan->id, $journal, $this->balance);
            // $this->balance += $totalClosingChargesAmount;
            // array_push($this->statements, $statement);
        // }

        return collect($this->statements, 'created_at')->where('description', '!=', 'skip');
    }

    public function closingChargesTnxs($loanId, $journal, $balanceBeforePayment)
    {
        $totalChargeAmount = 0;
        $closingCharges = Apiato::call('ClosingCharge@FindClosingLoanChargeByIdTask', [$loanId]);
        if(count($closingCharges) > 0){
            foreach ($closingCharges as $key => $closingCharge) {
                $balanceBeforePayment += $closingCharge->value;
                $statementData = [
                    // 'tnxDate' => $journal->tnx_date->copy()->subDay(),
                    'tnxDate' => $journal->tnx_date,
                    'debit' => $closingCharge->value,
                    'balance' => $balanceBeforePayment,
                    'description' => 'closing_charge',
                    'comments' => $closingCharge->description,
                ];
                $statement = Apiato::call('Payment@SetStatementTask', [$statementData]);
                array_push($this->statements, $statement);
                // $this->setStatement($statementData);
                $totalChargeAmount += $closingCharge->value;
            }
        }

        // $minTermPending = Apiato::call('ClosingCharge@GetMinTermBalanceTask', [$loanId]);
        // if($minTermPending->min_term_balance > 0){
        //     $balanceBeforePayment += $minTermPending->min_term_balance;
        //     $statementData = [
        //         // 'tnxDate' => $journal->tnx_date->copy()->subDay(),
        //         'tnxDate' => $journal->tnx_date,
        //         'debit' => $minTermPending->min_term_balance,
        //         // 'credit' => null,
        //         'balance' => $balanceBeforePayment,
        //         // 'arrears' => null,
        //         'description' => 'min_term_amount',
        //         'comments' => 'Minimum term amount pending',
        //         // 'breakDown' => null,
        //     ];

        //     $totalChargeAmount += $minTermPending->min_term_balance;
        //     // $this->setStatement($statementData);
        //     $statement = Apiato::call('Payment@SetStatementTask', [$statementData]);
        //     array_push($this->statements, $statement);
        // }
        return $totalChargeAmount;
    }
}
