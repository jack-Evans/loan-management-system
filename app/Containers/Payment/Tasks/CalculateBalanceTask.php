<?php

namespace App\Containers\Payment\Tasks;

use App\Ship\Parents\Tasks\Task;
use App\Containers\Payment\Data\Repositories\JournalRepository;
use App\Containers\Payment\Data\Repositories\PostRepository;
use App\Containers\Loan\Models\Loan;
use Carbon\Carbon;

class CalculateBalanceTask extends Task
{
	protected $journalRepository;
	protected $postingRepository;

    public function __construct(JournalRepository $journalRepository, PostRepository $postingRepository)
    {
        $this->journalRepository = $journalRepository;
        $this->postingRepository = $postingRepository;
    }

    public function run(loan $loan, Carbon $dt = null)
    {
        $journalIdsQuery = $this->journalRepository;
        
		if($dt) {
			$journalIdsQuery = $journalIdsQuery->scopeQuery(function($q) use($dt) {
				return $q->whereDate("tnx_date", "<=", $dt);
			});
		}
		$journalIds = $journalIdsQuery->findWhere(['loan_id' => $loan->id])->pluck('id');
		
		$postings = $this->postingRepository->findWhereIn('journal_id', $journalIds->toArray());
		$credit   = $postings->sum(function($post){
			if($post->type == 'credit' && $post->account_id != 0) {
				return $post->amount;
			}
			return 0;
		});

		$debit = $postings->sum(function($post){
			if($post->type == 'debit' && $post->account_id != 0) {
				return $post->amount;
			}
			return 0;
		});

		$balance = $debit - $credit;

		return [
			'totalDebit'	=> round($debit, 2),
			'totalCredit' 	=> round($credit, 2),
			'balance'		=> round($balance, 2),
		];
    }
}
