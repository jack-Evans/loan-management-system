<?php

namespace App\Containers\Payment\Tasks;

use App\Ship\Parents\Tasks\Task;
use Apiato\Core\Traits\ResponseTrait;
use Apiato\Core\Foundation\Facades\Apiato;
use App\Containers\Loan\Models\Loan;
use App\Containers\Payment\Models\Statement;
use Carbon\Carbon;

class GetNTranchStatementsTask extends Task
{
    use ResponseTrait;

    protected $statements;
    protected $issueDate;
    protected $interestBreakDown;
    protected $balance;
    protected $defaultRate;
    protected $grossLoan;
    protected $paymentsInThisMonth;
    protected $balanceForCalculation;

    public function run(Loan $loan, $statementDate = false)
    {
        $getJournals = Apiato::call('Payment@GetJournalsTask', [$loan, $statementDate]);
        $journals = $getJournals['journals'];
        $this->requestDate = $getJournals['requestDate'];

        $this->statements = $this->interestBreakDown = [];
        $this->balance = $this->balanceForCalculation = $balance = $interest = 0;
        $currentDate = $startDate = $this->issueDate = $lastTnxDate = $loan->issue_at;
        $this->loanClosingDate = addDecimalMonthsIntoDate($loan->interest->duration, $loan->issue_at)->copy()->subDay();
        $this->paymentsInThisMonth = [];
        $count = count($journals);
        $this->grossLoan = $loan->net_loan;
        $normalRate = $rate = $loan->interest->rate;
        $this->defaultRate = $loan->interest->default_rate;
        $transactionId = null;

        foreach($journals as $key => $journal){
            if($journal->type === 'interest'){
                $transactionId = $journal->invoice_id;
                continue;
            }
            if($journal->type === 'tranch'){
                $statement = $this->setInterestBreakDown($loan, $balance, $lastTnxDate, $transactionId, $this->paymentsInThisMonth);
                $balance = $statement->balance;
                $lastTnxDate = $journal->tnx_date;
            }
            $transactions = $this->setTransactions($loan, $journal, $balance, $this->paymentsInThisMonth, $this->requestDate);
            $this->paymentsInThisMonth = $transactions['paymentsInThisMonth'];
            $statement = $transactions['statement'];
            array_push($this->statements, $statement);
            $balance = $transactions['balance'];
        }

        $statement = $this->setInterestBreakDown($loan, $balance, $lastTnxDate, $transactionId, $this->paymentsInThisMonth);
        $balance = $statement->balance;
        // dd($statement);

        return collect(sortByDate($this->statements, 'created_at'))->where('description', '!=', 'skip');
    }

    public function setTransactions($loan, $journal, $balance, $paymentsInThisMonth, $requestDate)
    {
        $statementData = [];
        $statementData['id'] = $journal->id;
        $currentDate = $statementData['tnxDate'] = $journal->tnx_date;
        $statementData['nominalCode'] = $journal->nominalcode->code;
        $statementData['comments'] = $journal->comment;
        $statementData['description'] = $journal->posts->first()->asset_type;
        $statementData['tnxId'] = $journal->invoice_id;
        $statementData['hide'] = $journal->hide;
        $statementData['hideTransaction'] = $journal->hide_transaction;
        $statementData['notes'] = $journal->notes;

        $balanceBeforePayment = $this->balanceForCalculation;
        if($journal->type === 'payment' || $journal->type === 'refund' || $journal->type === 'refund_default_interest' || $journal->type === 'other_charge_refund' || $journal->type === 'other_charge_adjustment' || $journal->type === 'credit_filler') {
            
            $statementData['credit'] = $journal->posts->sum(function($post) use ($statementData) {
                if($post->type == 'credit') {
                    return $post->amount;
                }
                return 0;
            });
            $statementData['balance'] = $balance -= $statementData['credit'];
            $this->balanceForCalculation -= $statementData['credit'];
            if($currentDate->ne($loan->issue_at) && $currentDate->lte($this->loanClosingDate) || ($currentDate->month == $this->loanClosingDate->month && $currentDate->gt($this->loanClosingDate))) {
                if($journal->type != 'refund_default_interest')
                    $this->grossLoan -= $statementData['credit'];
            }
        }
        else
        {
           $statementData['debit'] = $journal->posts->sum(function($post) {
                if($post->type == 'debit') {
                    return $post->amount;
                }
                return 0;
            });
            
            $statementData['balance'] = $balance += $statementData['debit'];
            $this->balanceForCalculation += $statementData['debit'];
        }
        array_push($paymentsInThisMonth, ['date' => $journal->tnx_date, 'currentBalance' => $this->balanceForCalculation, 'previousBalance' => $balanceBeforePayment, 'ignorePmt' => false]);
        
        $statement = Apiato::call('Payment@SetStatementTask', [$statementData]);
        return [
            'paymentsInThisMonth' => $paymentsInThisMonth,
            'statement' => $statement,
            'balance' => $balance,
            'balanceBeforePayment' => $balanceBeforePayment,
        ];
    }

    public function setInterestBreakDown($loan, $balance, $startDate, $transactionId, $paymentsInThisMonth)
    {
        $lastTnxDate = $startDate;
        foreach (collect($paymentsInThisMonth) as $key => $payment) {
            if($payment['date']->eq($startDate) || $key == 0)
                continue;
            $interestBreakDown[] = Apiato::call('Statement@CalculateNTranchInterestTask', [$loan, $payment['previousBalance'], $startDate, $payment['date']]);    
            $startDate = $payment['date'];
        }
        $interestBreakDown[] = Apiato::call('Statement@CalculateNTranchInterestTask', [$loan, $this->balanceForCalculation, $startDate, $this->loanClosingDate]);
        
        $totalInterestAmount = collect(Apiato::call('Statement@SetInterestBreakDownTask', [$interestBreakDown]))->sum('total');
        $balanceAfterInterest = $balance + $totalInterestAmount;

        $statement  = new Statement;
        $statement->setCreatedAtAttribute($startDate);
        $statement->setDescriptionAttribute('interest');
        $statement->setCommentsAttribute("Retained interest from {$lastTnxDate->format('M d y')} to {$this->loanClosingDate->format('M d y')}");
        $statement->setDebitAttribute($totalInterestAmount);
        $statement->setCreditAttribute(null);
        $statement->setBalanceAttribute($balanceAfterInterest);
        $statement->setInterestBreakDownAttribute(Apiato::call('Statement@SetInterestBreakDownTask', [$interestBreakDown]));
        $statement->setTransactionIdAttribute($transactionId);

        array_push($this->statements, $statement);

        $this->paymentsInThisMonth = [];
        $this->balanceForCalculation = 0;
        return $statement;
    }

}