<?php

namespace App\Containers\Payment\Tasks;

use App\Ship\Parents\Tasks\Task;
use Apiato\Core\Foundation\Facades\Apiato;
use Exception;
use App\Containers\Loan\Models\Loan;

class CreateFirstMonthInterestTask extends Task
{
    public function run($loan)
    {
        try {

            $firstMonthInterest = Apiato::call('Payment@CalculateFirstMonthInterestTask', [$loan]);
            $endOfMonth = $firstMonthInterest['date'];
            $totalDaily = $firstMonthInterest['total'];
            $accountingPeriod  =  $firstMonthInterest['accountingPeriod'];
            $debitAccountId  = $loan->customer->account->id;
            \DB::beginTransaction();
            $journalPosting = [
                'creditAccountId' => 0,
                'debitAccountId' => $debitAccountId,
                'type' => 'first_month_interest',
                'loanid' => $loan->id,
                'tnxDate' => $endOfMonth,
                'amount' => $totalDaily,
                'accountingPeriod' => $accountingPeriod,
                'assetType' => 'first month interest',
                'description' => '',
                'payableId' => 0,
                'payableType' => ''
            ];
            $firstMonthJournal = Apiato::call('Payment@CreateJournalPostingTask', [$journalPosting]);
            \DB::commit();
            $posting = $firstMonthJournal->posts;
            return $posting;
        } catch (Exception $exception) {
        	 \DB::rollback();
            throw new CreateResourceFailedException();
        }
    }
}
