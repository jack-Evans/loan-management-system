<?php

namespace App\Containers\Payment\Tasks;

use App\Containers\Payment\Data\Repositories\PaymentRepository;
use App\Ship\Exceptions\NotFoundException;
use App\Ship\Parents\Tasks\Task;
use Exception;
use App\Containers\Loan\Models\Loan;

class GetTotalPaidAmountTask extends Task
{

    protected $repository;

    public function __construct(PaymentRepository $repository)
    {
        $this->repository = $repository;
    }

    public function run(Loan $loan)
    {
        try {
            return $this->repository->findWhere(['loan_id' => $loan->id, 'status' => 'paid'])->sum('amount');
        }
        catch (Exception $exception) {
            throw new NotFoundException();
        }
    }
}
