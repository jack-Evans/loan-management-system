<?php
namespace App\Containers\Payment\Tasks;

use App\Ship\Parents\Tasks\Task;
use App\Containers\Loan\Models\Loan;
use App\Containers\Payment\Data\Repositories\JournalRepository;
use Carbon\Carbon;
use App\Containers\Payment\Models\Statement;
use Apiato\Core\Traits\ResponseTrait;
use App\Containers\TempCalculation\Exceptions\DateShouldBeGreaterException;
use Apiato\Core\Foundation\Facades\Apiato;

class GetStatementsTask extends Task
{
    use ResponseTrait;
    
    protected $repository;
    protected $requestDate;
    protected $loanClosingDate;
    protected $statements;
    protected $lastTnxDate;

    public function __construct(JournalRepository $repository)
    {
        $this->repository = $repository;
        // $this->statements = [];
    }

    public function run(Loan $loan, $statementDate = false, $virtualPmt = false, $halfLoan = false)
    {
        $getJournals = Apiato::call('Payment@GetJournalsTask', [$loan, $statementDate]);
        $journals = collect($getJournals['journals'])->where('type', '!=', 'skip');
        // $journals = $getJournals['journals'];
        $this->requestDate = $getJournals['requestDate'];
        $this->statements = [];
        $balance = 0;
        
        if($halfLoan != false){
            $getJournals = Apiato::call('Payment@GetJournalsTask', [$loan, $statementDate, $halfLoan['date']]);
            // dd($halfLoan);
            $journals = $getJournals['journals'];
            $this->requestDate = $getJournals['requestDate'];

            $loan->net_loan = $loan->interest->gross_loan = $balance = $halfLoan['balance'];
            $loan->issue_at = Carbon::parse($halfLoan['date']);
            $loan->interest->duration = $loan->interest->duration - $loan->interest->half_loan_duration;
            $loan->loan_type = 'serviced';

        }
        
        $lastTnxDate = $loan->issue_at;
        $this->loanClosingDate = $loan->issue_at->copy()->subDay()->addMonths($loan->interest->duration);
        $paymentsInThisMonth = [];        
        $count = count($journals);
        $transactionId = null;

        foreach($journals as $key => $journal) {
            // If we are in new month    
            if($lastTnxDate->month < $journal->tnx_date->month || $lastTnxDate->year < $journal->tnx_date->year) {
                $currentMonthStart = $journal->tnx_date->copy()->startOfMonth();
                $lastMonthStart = $lastTnxDate->copy()->startOfMonth();		
                $diffInMonths = $lastMonthStart->diffInMonths($currentMonthStart);
                for($i = 0; $i < $diffInMonths; $i++) {
                    // If first month
                    if($lastTnxDate->month == $loan->issue_at->month && $lastTnxDate->year == $loan->issue_at->year) {
                        $statement = Apiato::call('Statement@CalculateFirstMonthInterestTask', [$loan, $balance, $paymentsInThisMonth, $this->requestDate, $transactionId]);
                    } else {
                        $statement = Apiato::call('Statement@CalculateMonthInterestTask', [$loan, $balance, $lastTnxDate, $paymentsInThisMonth, $this->requestDate, $this->statements, $transactionId]);
                        // if(is_array($statement)){
                            
                        //     $retainedInterest = $statement['retainedInterest'];
                        //     $this->setStatement($retainedInterest);
                            
                        //     $tempStatement = Apiato::call('Statement@CalculateMonthInterestTask', [$loan, $balance, $lastTnxDate, $paymentsInThisMonth, $lastTnxDate, $this->statements]);
                        //     $currentMonthInterestAmount = $tempStatement->debit;
                            
                        //     $retainedDebit = $retainedInterest['debit'] - $currentMonthInterestAmount;
                        //     $statement = $statement['statement'];
                        //     $statement->balance += $retainedDebit;
                        // }
                    }
                    $transactionId = null;
                    $balance = $statement->balance;
                    array_push($this->statements, $statement);
                    $lastTnxDate->startOfMonth()->addMonth();
                    $paymentsInThisMonth = [];
                }
            }
        	
            // $transactions = Apiato::call('Payment@SetTransactionsTask', [$loan, $journal, $balance, $paymentsInThisMonth]);
            // dd($transactions);
            // $statement = new Statement;
            if($journal->type === 'interest'){
                $transactionId = $journal->invoice_id;
                continue;
            }
            // $statementData = [];
            // $statementData['tnxDate'] = $journal->tnx_date;
            // $statementData['comments'] = $journal->comment;
            if($journal->type === 'renewal_charge'){
                $statement = Apiato::call('Statement@CalculateMonthInterestTask', [$loan, $balance, $lastTnxDate, $paymentsInThisMonth, $journal->tnx_date->copy()->subDay(), $this->statements, $transactionId]);
                $transactionId = null;
                $balance = $statement->balance;
                array_push($this->statements, $statement);
                $paymentsInThisMonth = [];
                $lastTnxDate = $journal->tnx_date;
            }
            if($journal->type === 'payment' || $journal->type === 'refund' || $journal->type === 'refund_default_interest' || $journal->type === 'other_charge_refund' || $journal->type === 'credit_filler') {  
                
                // if($journal->type === 'other_charge_refund'){
                //     $finalArr[] = Apiato::call('Payment@OtherChargeRefundBreakDownTask', [$loan, $journal]);
                //     $statementData['breakDown'] = Apiato::call('Statement@SetInterestBreakDownTask', [$finalArr]);
                // }

                // $statementData['credit'] = $journal->posts->sum(function($post) use ($statementData) {
                //     if($post->type == 'credit') {
                //         $statementData['description'] = $post->asset_type;
                //         return $post->amount;
                //     }
                //     return 0;	
                // });
                // $balanceBeforePayment = $balance;
                // $ignoreCredits = 0;
                // if($journal->type === 'ignore'){
                //     // $ignoreCredits += $statement->credit;
                //     $balance += $statementData['credit'];
                //     if($this->requestDate && $this->requestDate->format('M') == $statementData['tnxDate']->format('M'))
                //         $statementData['tnxDate'] = $this->requestDate;
                //     else
                //         $statementData['tnxDate']->endOfMonth();
                // }
                // $balance -= $statementData['credit'];
                // $statementData['balance'] = $balance;
                // // $statement->setDebitAttribute(null);
                // // We got payment calculate balance here
                // $ignorePmt = false;
                // if($journal->type === 'ignore')
                //     $ignorePmt = $statementData['credit'];

                // array_push($paymentsInThisMonth, ['date' => $journal->tnx_date, 'currentBalance' => $balance+$ignoreCredits, 'previousBalance' => $balanceBeforePayment, 'ignorePmt' => $ignorePmt]);
                // $statement = Apiato::call('Payment@SetStatementTask', [$statementData]);
                // array_push($this->statements, $statement);
                $transactions = Apiato::call('Payment@SetTransactionsTask', [$loan, $journal, $balance, $paymentsInThisMonth, $this->requestDate]);
                $paymentsInThisMonth = $transactions['paymentsInThisMonth'];
                array_push($this->statements, $transactions['statement']);
                // dd($transactions['balance']);
                $balance = $transactions['balance'];
                $balanceBeforePayment = $transactions['balanceBeforePayment'];
                if($balance <= 0 && $loan->loan_type == 'serviced' && !isset($closingChargesDone))
                {
                    $totalClosingChargesAmount = $this->closingChargesTnxs($loan->id, $journal, $balance);

                    $balance += $totalClosingChargesAmount;
                    $statement = Apiato::call('Statement@CalculateMonthInterestTask', [$loan, $balance, $lastTnxDate, $paymentsInThisMonth, $this->requestDate, $this->statements]);
                    $tempBalance = $statement->balance;
                    $closingChargesDone = true;
                    if($tempBalance <= 0){
                        $balance = $tempBalance;
                        array_push($this->statements, $statement);
                        $paymentsInThisMonth = [];
                        break;
                    }
                }
            }
            else
            {
                // $statementData['debit'] = $journal->posts->sum(function($post) use ($statementData) {
                //     if($post->type == 'debit') {
                //         $statementData['description'] = $post->asset_type;
                //         return $post->amount;
                //     }
                //     return 0;	
                // });

                // $balanceBeforePayment = $balance;
                // $balance += $statementData['debit'];
                // $statementData['balance'] = $balance;
                // // $statement->setCreditAttribute(null);
                // // We got debit calculate balance here
                // array_push($paymentsInThisMonth, ['date' => $journal->tnx_date, 'currentBalance' => $balance, 'previousBalance' => $balanceBeforePayment, 'ignorePmt' => false]);
                // $statement = Apiato::call('Payment@SetStatementTask', [$statementData]);
                // array_push($this->statements, $statement);
                $transactions = Apiato::call('Payment@SetTransactionsTask', [$loan, $journal, $balance, $paymentsInThisMonth, $this->requestDate]);
                $paymentsInThisMonth = $transactions['paymentsInThisMonth'];
                array_push($this->statements, $transactions['statement']);
                // dd($transactions['balance']);
                $balance = $transactions['balance'];
                $balanceBeforePayment = $transactions['balanceBeforePayment'];

                // if($journal->type === 'renewal_charge' && $loan->loan_type === 'retained'){
                //     // Add retained interest.
                //     // dd($journal->payable);
                //     $retainedInterest = Apiato::call('Payment@RetainedInterestStatementTask', [$loan, $balance, $lastTnxDate, $paymentsInThisMonth, $journal->tnx_date, $this->statements]);
                //     $this->setStatement($retainedInterest);
                //     $balance = $retainedInterest['balance'];
                // }
            }
            // if last iteration, add interest upto month end 
            if($key == ($count - 1))
            {
                // dd($lastTnxDate->copy()->startOfMonth() <= $loan->issue_at);
                if($lastTnxDate->copy()->startOfMonth() <= $loan->issue_at)
                    $statement = Apiato::call('Statement@CalculateFirstMonthInterestTask', [$loan, $balance, $paymentsInThisMonth, $this->requestDate]);
                else
                    $statement = Apiato::call('Statement@CalculateMonthInterestTask', [$loan, $balance, $lastTnxDate, $paymentsInThisMonth, $this->requestDate, $this->statements]);
                
                $balance = $statement->balance;
                array_push($this->statements, $statement);
                $lastTnxDate->startOfMonth()->addMonth();
                $paymentsInThisMonth = [];
                // if($this->requestDate >= $lastTnxDate->copy())
                if($this->requestDate && $this->requestDate->gte($lastTnxDate))
                {
                    if($virtualPmt)
                        $this->withVirtualPaymentBalance($loan, $balance, $lastTnxDate, $this->requestDate);
                    else
                        $this->normalFutureBalance($loan, $balance, $lastTnxDate, $this->requestDate);
                }
            }
        }
        // dd(collect(sortByDate($this->statements, 'created_at'))->where('description', '!=', 'skip'));
        // return collect(sortByDate($this->statements, 'created_at'));  
        // if($loan->loan_type === 'retained'){
        //     $retainedInterest = collect(sortByDate($this->statements, 'created_at'))->where('description', '=', 'interest')->sum('debit');
        //     dd($retainedInterest);
        // }
        // if(count($this->statements) == 0 && $halfLoan != false){
        //     $this->normalFutureBalance($loan, $balance, $lastTnxDate, $halfLoan['date']);
        // }
        // dd($this->statements);
        return collect(sortByDate($this->statements, 'created_at'))->where('description', '!=', 'skip');  
    }

    public function withVirtualPaymentBalance($loan, $balance, $lastTnxDate, $requestDate)
    {
        // add one so if request date is equal to lastTnxDate then it will count 1 day instead of 0 day.
        $diffInDays = $lastTnxDate->copy()->diffInDays($requestDate) + 1;
        if($diffInDays) 
        {
            $issueDate = $loan->issue_at;
            $loanId = $loan->id;
            $dueDay = $issueDate->copy()->format('d') - 1;
            // $normalRate = $loan->interest->rate;
            $rate = $normalRate = $loan->interest->rate;
            $defaultRate = $loan->interest->default_rate;

            do
            {
                $diffInMonths = $lastTnxDate->copy()->diffInMonths($requestDate);
                if($diffInMonths || ($lastTnxDate->copy()->month < $requestDate->copy()->month)) {
                    $endDate = $lastTnxDate->copy()->endOfMonth();
                    $subDays = $lastTnxDate->copy()->diffInDays($endDate) + 1;
                } else {
                    $endDate = $requestDate;
                    $subDays = $lastTnxDate->copy()->diffInDays($endDate) + 1;
                }
                
                if($lastTnxDate->gte($this->loanClosingDate))
                    $rate = $defaultRate;
                $currentDueDate  = Carbon::parse($lastTnxDate->copy()->format('y-m')."-".$dueDay);            
                $interestBreakDown = [];
                if($endDate->gt($currentDueDate)){

                    $interest = Apiato::call('Interest@GetInterestsAsPmtPlanVersionsTask', [$loan->id, $currentDueDate])->last();
                    if($interest)
                        $amountDue = calculateMonthlyPMT($loan->interest->rate, $interest->gross_loan);
                    else
                        $amountDue = calculateMonthlyPMT($loan->interest->rate, $loan->interest->gross_loan);
                    
                    $statement = new Statement;
                    $statement->setCreatedAtAttribute($currentDueDate);
                    $statement->setDescriptionAttribute('payment');
                    $statement->setCommentsAttribute('Virtual payment');
                    $statement->setDebitAttribute(null);
                    $statement->setBalanceAttribute($balance - $amountDue);
                    $statement->setCreditAttribute($amountDue);

                    array_push($this->statements, $statement);

                    $breakDown = Apiato::call('Statement@CreateInterestBreakDownTask', [$loan, $rate, $balance, $lastTnxDate, $currentDueDate->copy()->subDay(), $plusOne = true]);
                    if($breakDown != false)
                        $interestBreakDown[] = $breakDown;

                    // $grace = getTransactionGrace($loan, $this->requestDate)->last();
                    // if($grace){
                    //     $addDays = $grace->days;
                    //     if($this->requestDate->lte($this->loanClosingDate->copy()->addDays($addDays))){
                    //     // if($this->loanClosingDate->copy()->subDay()->lte($this->requestDate->copy()->addDays($addDays))) {
                    //         $defaultRate = $loan->interest->default_rate;
                    //         $breakDown = Apiato::call('Statement@CreateInterestBreakDownTask', [$loan, $defaultRate, $balance, $this->loanClosingDate, $this->requestDate, $plusOne = true, $negativeInterest = true]);
                    //         if($breakDown != false)
                    //             $interestBreakDown[] = $breakDown;
                    //         $lastInterestBreakdown = collect($this->statements)->last()->interestBreakDown;
                    //     }
                    // }

                    $balance -= $amountDue;
                    $lastTnxDate = $currentDueDate;
                    if($lastTnxDate->gte($this->loanClosingDate))
                        $rate = $defaultRate;
                }

                $statement = new Statement;
                $statement->setCreatedAtAttribute($endDate);
                $statement->setDescriptionAttribute('interest');
                $statement->setCommentsAttribute("Int from {$lastTnxDate->copy()->startOfMonth()->format('M d y')} to {$endDate->copy()->format('M d y')}");
                if($lastTnxDate <= $requestDate){
                    $breakDown = Apiato::call('Statement@CreateInterestBreakDownTask', [$loan, $rate, $balance, $lastTnxDate, $endDate, $plusOne = true]);
                    if($breakDown != false)
                        $interestBreakDown[] = $breakDown;
                }

                $statement->setInterestBreakDownAttribute(Apiato::call('Statement@SetInterestBreakDownTask', [$interestBreakDown]));
                $totalInterestAmount = collect(Apiato::call('Statement@SetInterestBreakDownTask', [$interestBreakDown]))->sum('total');
                $statement->setDebitAttribute($totalInterestAmount);
                // $balance += ($loan->loan_type === 'retained' && $rate == $normalRate) ? 0 : $statement->debit;
                $balance += $statement->debit;
                $statement->setBalanceAttribute($balance);
                $statement->setCreditAttribute(null);

                array_push($this->statements, $statement);
                $lastTnxDate = $endDate->copy()->startOfMonth()->addMonth();
                $interestBreakDown = [];
                $diffInDays -= $subDays;
            }    while ($diffInDays > 0);
        }
    }

    public function normalFutureBalance($loan, $balance, $lastTnxDate, $requestDate)
    {
        // add one so if request date is equal to lastTnxDate then it will count 1 day instead of 0 day.
        $diffInDays = $lastTnxDate->copy()->diffInDays($requestDate) + 1;
        if($diffInDays) 
        {
            $issueDate = $loan->issue_at;
            $loanId = $loan->id;
            $dueDay = $issueDate->copy()->format('d') - 1;
            $normalRate = $loan->interest->rate;
            $defaultRate = $loan->interest->default_rate;

            do
            {
                $diffInMonths = $lastTnxDate->copy()->diffInMonths($requestDate);
                if($diffInMonths || ($lastTnxDate->copy()->month < $requestDate->copy()->month)) {
                    $endDate = $lastTnxDate->copy()->endOfMonth();
                    $subDays = $lastTnxDate->copy()->diffInDays($endDate) + 1;
                } else {
                    $endDate = $requestDate;
                    $subDays = $lastTnxDate->copy()->diffInDays($endDate) + 1;
                }

                $currentDueDate  = Carbon::parse($lastTnxDate->copy()->format('y-m')."-".$dueDay);

                $statement = new Statement;
                $statement->setCreatedAtAttribute($endDate);
                $statement->setDescriptionAttribute('interest');
                $statement->setCommentsAttribute("Int from {$lastTnxDate->format('M d y')} to {$endDate->copy()->format('M d y')}");
                
                $isPaymentLate = Apiato::call('Statement@PaymentLateCheckTask', [$loan, $issueDate, $lastTnxDate]);
                if($isPaymentLate)
                    $rate = $defaultRate;
                else
                    $rate = $normalRate;

                $isPaymentLateAtEndOfLastMonth = Apiato::call('Statement@PaymentLateCheckTask', [$loan, $issueDate, $endDate]);
                $interestBreakDown = [];
                if(($isPaymentLate == false) && ($isPaymentLateAtEndOfLastMonth)){
                    $breakDown = Apiato::call('Statement@CreateInterestBreakDownTask', [$loan, $rate, $balance, $lastTnxDate, $currentDueDate, $plusOne = true]);
                    if($breakDown != false)
                        $interestBreakDown[] = $breakDown;
                    $lastTnxDate = $currentDueDate->copy()->addDay();                                
                }
                // addDay may increase lastTnxDate from requested date to cause an invalid result.
                if($lastTnxDate <= $requestDate){
                    $isPaymentLate = Apiato::call('Statement@PaymentLateCheckTask', [$loan, $loan->issue_at, $endDate]);
                    if($isPaymentLate)
                        $rate = $defaultRate;
                    else
                        $rate = $normalRate;
                    $breakDown = Apiato::call('Statement@CreateInterestBreakDownTask', [$loan, $rate, $balance, $lastTnxDate, $endDate, $plusOne = true]);
                    if($breakDown != false)
                        $interestBreakDown[] = $breakDown;

                    $grace = getTransactionGrace($loan, $this->requestDate)->last();
                    if($grace){
                        $addDays = $grace->days;
                        if($this->requestDate->copy()->addDay()->lte($this->loanClosingDate->copy()->addDays($addDays))){
                            $defaultRate = $loan->interest->default_rate;
                            $breakDown = Apiato::call('Statement@CreateInterestBreakDownTask', [$loan, $defaultRate, $balance, $this->loanClosingDate->copy()->addDay(), $this->requestDate, $plusOne = true, $negativeInterest = true]);
                            if($breakDown != false)
                                $interestBreakDown[] = $breakDown;
                        }
                    }
                }

                $statement->setInterestBreakDownAttribute(Apiato::call('Statement@SetInterestBreakDownTask', [$interestBreakDown]));
                // $totalInterestAmount = collect($interestBreakDown)->sum('total');
                $totalInterestAmount = collect(Apiato::call('Statement@SetInterestBreakDownTask', [$interestBreakDown]))->sum('total');
                $statement->setDebitAttribute($totalInterestAmount);
                // $balance += ($loan->loan_type === 'retained' && $rate == $normalRate) ? 0 : $statement->debit;
                $balance += $statement->debit;
                $statement->setBalanceAttribute($balance);
                $statement->setCreditAttribute(null);

                array_push($this->statements, $statement);
                $lastTnxDate = $endDate->copy()->startOfMonth()->addMonth();
                $interestBreakDown = [];
                $diffInDays -= $subDays;
            }    while ($diffInDays > 0);

            // if($loan->loan_type === 'retained'){
            //     // Add retained interest.
            //     $retainedInterest = Apiato::call('Payment@RetainedInterestStatementTask', [$loan, $balance, $lastTnxDate, $paymentsInThisMonth = [], $endDate, $this->statements, 0.000001]);
            //     $this->setStatement($retainedInterest);
            //     $balance = $retainedInterest['balance'];

            // }
        }
    }

    public function closingChargesTnxs($loanId, $journal, $balanceBeforePayment)
    {
        $totalChargeAmount = 0;
        $closingCharges = Apiato::call('ClosingCharge@FindClosingLoanChargeByIdTask', [$loanId]);
        if(count($closingCharges) > 0){
            foreach ($closingCharges as $key => $closingCharge) {
                $balanceBeforePayment += $closingCharge->value;
                $statementData = [
                    // 'tnxDate' => $journal->tnx_date->copy()->subDay(),
                    'tnxDate' => $journal->tnx_date,
                    'debit' => $closingCharge->value,
                    'balance' => $balanceBeforePayment,
                    'description' => 'closing_charge',
                    'comments' => $closingCharge->description,
                ];
                $statement = Apiato::call('Payment@SetStatementTask', [$statementData]);
                array_push($this->statements, $statement);
                // $this->setStatement($statementData);
                $totalChargeAmount += $closingCharge->value;
            }
        }

        $minTermPending = Apiato::call('ClosingCharge@GetMinTermBalanceTask', [$loanId]);
        if($minTermPending->min_term_balance > 0){
            $balanceBeforePayment += $minTermPending->min_term_balance;
            $statementData = [
                // 'tnxDate' => $journal->tnx_date->copy()->subDay(),
                'tnxDate' => $journal->tnx_date,
                'debit' => $minTermPending->min_term_balance,
                // 'credit' => null,
                'balance' => $balanceBeforePayment,
                // 'arrears' => null,
                'description' => 'min_term_amount',
                'comments' => 'Minimum term amount pending',
                // 'breakDown' => null,
            ];

            $totalChargeAmount += $minTermPending->min_term_balance;
            // $this->setStatement($statementData);
            $statement = Apiato::call('Payment@SetStatementTask', [$statementData]);
            array_push($this->statements, $statement);
        }
        return $totalChargeAmount;
    }

    public function setStatement(array $data)
    {
        $statement = new Statement;
        $statement->setCreatedAtAttribute($data['tnxDate']);
        $statement->setDebitAttribute($data['debit']);
        $statement->setCreditAttribute($data['credit']);
        $statement->setBalanceAttribute($data['balance']);
        $statement->setArrearsAttribute($data['arrears']);
        $statement->setDescriptionAttribute($data['description']);
        $statement->setCommentsAttribute($data['comments']);
        // $statement->setInterestBreakDownAttribute($data'tnx_date']);
        array_push($this->statements, $statement);
    }

    // public function retainedInterest($loan, $balance, $lastTnxDate, $paymentsInThisMonth = [], $journalTnxDate)
    // {
    //     $tempStatement = Apiato::call('Statement@CalculateMonthInterestTask', [$loan, $balance, $lastTnxDate, $paymentsInThisMonth, $journalTnxDate]);

    //     $previousInterest = collect(sortByDate($this->statements, 'created_at'))->where('description', '=', 'retained_interest')->sum('debit');

    //     $interestAmount = collect(sortByDate($this->statements, 'created_at'))->where('description', '=', 'interest')->sum('debit') + $tempStatement->debit - $previousInterest;

    //     $balance += $interestAmount;        

    //     $data = [
    //         'tnxDate' => $journalTnxDate,
    //         'debit' => $interestAmount,
    //         'credit' => null,
    //         'balance' => $balance,
    //         'arrears' => null,
    //         'description' => 'retained_interest',
    //         'comments' => "Retained Interest",
    //     ];
        
    //     // $this->setStatement($data);
    //     return $data;
    // }

}
