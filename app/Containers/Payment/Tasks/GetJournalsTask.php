<?php
namespace App\Containers\Payment\Tasks;

use App\Ship\Parents\Tasks\Task;
use App\Containers\Loan\Models\Loan;
use App\Containers\Payment\Data\Repositories\JournalRepository;
use Carbon\Carbon;
use App\Containers\TempCalculation\Exceptions\DateShouldBeGreaterException;
use Apiato\Core\Foundation\Facades\Apiato;

class GetJournalsTask extends Task
{
    protected $repository;
    protected $requestDate;

    public function __construct(JournalRepository $repository)
    {
        $this->repository = $repository;
    }

    public function run(Loan $loan, $statementDate = false, $halfLoanDate = false)
    {
        // $statementDate = $statementDate ? $statementDate : ($loan->issue_at <= Carbon::now() ? Carbon::now() : false);
        // if($statementDate || ($loan->status === 'closed') || ($loan->loan_type === 'retained') )
        $this->requestDate = ($statementDate) ? Carbon::parse($statementDate) : $statementDate;
        if($statementDate || $loan->status === 'closed')
        {
            // if($loan->loan_type === 'retained'){
            //     $journals = $this->repository->with(['posts'])->orderBy('tnx_date','ASC')->findWhere(['loan_id' => $loan->id]);
            //     $lastTnxDate = $journals->last()->tnx_date;

            //     $retainLoanClosingDate = $loan->issue_at->copy()->addMonths($loan->interest->duration)->subDay();
            //     $this->requestDate = $statementDate ? Carbon::parse($statementDate) : ($retainLoanClosingDate->gt($lastTnxDate) ? $retainLoanClosingDate : $lastTnxDate);
            // } else{
                $this->requestDate = ($loan->status === 'closed') ? ($statementDate ? Carbon::parse($statementDate) : Carbon::parse($loan->status_change_date) ) : Carbon::parse($statementDate);
            // }

            if($this->requestDate->lt($loan->issue_at))
                throw new DateShouldBeGreaterException();
                
            $journals = $this->repository->with(['posts', 'nominalcode'])->orderBy('tnx_date','ASC')->
                scopeQuery(function($q) {
                    return $q->where('tnx_date', '<=', $this->requestDate);
                })->findWhere(['loan_id' => $loan->id]);
        }

        if($halfLoanDate != false){
            $journals = $this->repository->with(['posts', 'nominalcode'])->orderBy('tnx_date','ASC')->
                scopeQuery(function($q) use($halfLoanDate) {
                    return $q->where('tnx_date', '>=', $halfLoanDate);
            })->findWhere(['loan_id' => $loan->id]);
            // $this->requestDate = false;
        }

        else{
            $journals = $this->repository->with(['posts', 'nominalcode', 'tags'])->orderBy('tnx_date','ASC')->findWhere(['loan_id' => $loan->id]);
            // $this->requestDate = false;
        }
        // If renewal charges then extend closing date upto renewal charge duration
        // $renewalCharge = Apiato::call('RenewalCharge@GetRenewalChargesByLoanIdTask', [$loan->id])->last();
        // if($renewalCharge && $this->requestDate){
        //     $renewEndDate = $renewalCharge->renew_end_date;

        //     if($renewEndDate->gt($this->requestDate) && $loan->loan_type === 'retained')
        //         $this->requestDate = $renewEndDate;
        // }

        return ['journals' => $journals, 'requestDate' => $this->requestDate];

    }
}