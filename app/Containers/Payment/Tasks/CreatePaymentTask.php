<?php

namespace App\Containers\Payment\Tasks;

use App\Containers\Payment\Data\Repositories\PaymentRepository;
use App\Ship\Exceptions\CreateResourceFailedException;
use App\Ship\Parents\Tasks\Task;
use Exception;
use Apiato\Core\Foundation\Facades\Apiato;

class CreatePaymentTask extends Task
{

    protected $paymentRepository;

    public function __construct(PaymentRepository $paymentRepository)
    {
        $this->paymentRepository = $paymentRepository;
    }

    public function run(array $data, $loan)
    {
        try {
            
            $paidAt        = \Carbon\Carbon::parse($data['paid_at']);
            $paidAmount     = $data['amount'];
            $description    = $data['description'];
            $accountingPeriod   = "{$paidAt->format('M')} {$paidAt->year}";
            $debitAccountId  = $loan->customer->account->id;
            \DB::beginTransaction();
            // Payment Tnx
            $payment = $this->paymentRepository->create([
                'amount'    => $paidAmount,
                'paid_at'   => $paidAt,
                'loan_id'   => $loan->id,
                'is_capital_reduction'   => $data['is_capital_reduction'],
            ]);
            $journalPosting = [
                'creditAccountId' => $debitAccountId,
                'debitAccountId' => 0,
                'loanid' => $loan->id,
                'tnxDate' => $paidAt,
                'amount' => $paidAmount,
                'accountingPeriod' => $accountingPeriod,
                'type' => 'payment',
                'assetType' => ($data['is_capital_reduction']) ? 'capreduction' : 'payment',
                'description' => $description,
                'nominalCode' => isset($data['nominalCode']) ? $data['nominalCode'] : 1,
                'payableId' => $payment->id,
                'payableType' => 'App\Containers\Payment\Models\Payment'
            ];
            $journalEntry = Apiato::call('Payment@CreateJournalPostingTask', [$journalPosting]);
            \DB::commit();
            $posting = $journalEntry->posts;
            return $payment;
        } catch (Exception $exception) {
            \DB::rollback();
            throw new CreateResourceFailedException();
        }
    }
}
