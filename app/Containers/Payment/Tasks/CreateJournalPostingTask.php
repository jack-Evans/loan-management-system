<?php

namespace App\Containers\Payment\Tasks;

use App\Containers\Payment\Data\Repositories\JournalRepository;
use App\Containers\Payment\Data\Repositories\PostRepository;
use App\Ship\Parents\Tasks\Task;
use Exception;
use App\Ship\Exceptions\CreateResourceFailedException;

class CreateJournalPostingTask extends Task
{

    protected $journalRepository;
    protected $postingRepository;

    public function __construct(JournalRepository $journalRepository, PostRepository $postingRepostiory)
    {
        $this->journalRepository = $journalRepository;
        $this->postingRepository = $postingRepostiory;
    }

    public function run(array $data)
    {
        preg_match('/^(.+):(\d+)$/i', now(), $matches);
        try {
            \DB::beginTransaction();
            $journal = $this->journalRepository->create([
                    'default_credit_account_id' => $data['creditAccountId'],
                    'default_debit_account_id'	=> $data['debitAccountId'],
                    'type'			=> $data['type'],
                    'loan_id' 			=> $data['loanid'],
                    // 'invoice_id'        => strtoupper(uniqid().crypt($data['loanid'], env('HASH_ID_KEY'))),
                    'invoice_id'        => strtotime($matches[1]).$matches[2].'.'.mt_rand(0,1000000),
                    'tnx_date'			=> $data['tnxDate'],
                    'comment'			=> $data['description'],
                    'payable_id'		=> $data['payableId'],
                    'payable_type'		=> $data['payableType'],
                    'nominal_code_id'   => (isset($data['nominalCode'])) ? $data['nominalCode'] : 1,
                    'hide'              => (isset($data['hide'])) ? $data['hide'] : null,
                    'hide_transaction'  => (isset($data['hide_transaction'])) ? $data['hide_transaction'] : null,
            ]);

            $debitPosting = $this->postingRepository->create([
                    'account_id' 		=> $data['debitAccountId'],
                    'journal_id'		=> $journal->id,
                    'type'			=> 'debit',
                    'amount'			=> round($data['amount'], 2),
                    'accounting_period'         => $data['accountingPeriod'],
                    'asset_type'		=> $data['assetType'],
            ]);

            $creditPosting = $this->postingRepository->create([
                    'account_id' 		=> $data['creditAccountId'],
                    'journal_id'		=> $journal->id,
                    'type'			=> 'credit',
                    'amount'			=> round($data['amount'], 2),
                    'accounting_period'         => $data['accountingPeriod'],
                    'asset_type'		=> $data['assetType'],
            ]);
            
            \DB::commit();
            // $posting = $journal->posts;
            return $journal;
            }
        catch (Exception $exception) {
            \DB::rollback();
            throw new CreateResourceFailedException();
        }
    }
}