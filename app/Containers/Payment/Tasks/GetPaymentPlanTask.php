<?php

namespace App\Containers\Payment\Tasks;

use App\Containers\Payment\Data\Repositories\PaymentPlanRepository;
use App\Containers\Interest\Data\Repositories\InterestRepository;
use App\Containers\OtherCharge\Data\Repositories\OtherChargeRepository;
use App\Ship\Parents\Tasks\Task;
use App\Ship\Exceptions\NotFoundException;
use App\Containers\Payment\Data\Repositories\PaymentRepository;
use App\Containers\Payment\Models\PaymentPlan;
use App\Containers\Payment\Models\InterestBreakDown;
use Carbon\Carbon;
use Apiato\Core\Foundation\Facades\Apiato;

class GetPaymentPlanTask extends Task
{
    protected $repository;
    protected $paymentRepository;
    protected $interestRepository;
    protected $otherChargeRepository;

    public function __construct(PaymentPlanRepository $repository, PaymentRepository $paymentRepository, InterestRepository $interestRepository, OtherChargeRepository $otherChargeRepository)
    {
        $this->repository = $repository;
        $this->paymentRepository = $paymentRepository;
        $this->interestRepository = $interestRepository;
        $this->otherChargeRepository = $otherChargeRepository;
    }

    public function run($loanId, $statementsDate = null, $virtualPmt = false)
    {
        // try
        // {
            $loan = Apiato::call('Loan@FindLoanByIdTask', [$loanId]);
            $halfLoan = [];
            // if($loan->loan_type == 'serviced')
            if(in_array($loan->loan_type, ['serviced', 'rolled']))
                $statementsTask = "Payment@GetStatementsTask";
            else
                $statementsTask = "Payment@GetMonthlyRetainedLoanStatementsTask";

            $issueDate = $startDate = $loan->issue_at;
            if($loan->loan_type === 'half'){
                $loan->loan_type = 'retained';
                $duration = $loan->interest->duration;
                $loan->interest->duration = $loan->interest->half_loan_duration;
                $statementDate = addDecimalMonthsIntoDate($loan->interest->half_loan_duration, $loan->issue_at);
                $retainedStatements = Apiato::call('Payment@GetMonthlyRetainedLoanStatementsTask', [$loan, $statementDate->copy()->subDay()]);

                $loan->loan_type = 'serviced';
                $loan->interest->duration = $duration - $loan->interest->half_loan_duration;
                $halfLoan['date'] = $statementDate;
                $halfLoan['balance'] = $retainedStatements->last()->balance;
                $loan->interest->gross_loan = $loan->net_loan = $halfLoan['balance'];
                $loan->issue_at = $statementDate;
                $issueDate = $startDate = $statementDate;
                // dd($statementDate, $halfLoan['balance']);
            }

            $updatedPaymentPlans = [];
            // $loan = Apiato::call('Loan@FindLoanByIdTask', [$loanId]);
            // $issueDate = $startDate = $loan->issue_at;
            $grossLoan = $loan->interest->gross_loan;
            $netLoan = $loan->net_loan;
            $rate = $loan->interest->rate;
            $defaltRate = $loan->interest->default_rate;
            $monthlyPMT = calculateMonthlyPMT($rate, $grossLoan);
            $paymentType = 'payment';
            $paymentBreakDown = [];
            $version = 1;
            $oldInterest = $loan->interest;
            $grossLoan = $netLoan;
            $paymentPlanDate = null;
            $previousMonthDiff = 0;
            
            $renewalCharges = Apiato::call('RenewalCharge@GetRenewalChargesByLoanIdTask', [$loanId]);
            // ->last()
            // dd($renewalCharges->sum('duration'));
            // $interests = Apiato::call('Interest@GetInterestsAsPmtPlanVersionsTask', [$loan->id]);
            // $totalDuration = $interests->sum('duration');
            $totalDuration = $loan->interest->duration + $renewalCharges->sum('duration');
            $renewalCharges = $renewalCharges->last();
            
            for($i = 1; $i <= $totalDuration; $i++)
            {

                $payBeforeDate = Carbon::parse($startDate->copy()->addMonth())->subDay();
                
                $interest = Apiato::call('Interest@GetInterestsAsPmtPlanVersionsTask', [$loan->id, $payBeforeDate])->last();
                if($interest){
                    $grossLoan = $interest->gross_loan;
                    $monthlyPMT = calculateMonthlyPMT($loan->interest->rate, $grossLoan);
                    $issueDate = $paymentPlanDate = Carbon::parse($interest->payment_plan_date);                    

                    if($oldInterest->gross_loan != $grossLoan)
                        ++$version;
                    $oldInterest = $interest;
                }

                // final payment with break down of all amounts
                if($i == $totalDuration || $loan->loan_type == 'retained'){
                    $sumOfCharges = 0;
                        
                    $paymentBreakDown[] = ['created_at' => $issueDate, 'description' => 'Net loan', 'amount' => round($netLoan, 2)];

                    $virtualPmt = ($loan->loan_type == 'retained') ? false : $virtualPmt;
                    $statements = Apiato::call($statementsTask, [$loan, $statementsDate, $virtualPmt, $halfLoan]);                        
                    
                    // Charges, other charges, refund breakdowns
                    // $breakdowns = $statements->when($interest, function($q) use($paymentPlanDate) {
                    //         return $q->where('created_at', '>', $paymentPlanDate);
                    // })->whereIn('description', ['charge', 'othercharge', 'refund', 'other_charge_refund', 'ignore'])->all();

                    $breakdowns = $statements->whereIn('description', ['charge', 'other_charge', 'refund', 'other_charge_refund', 'refund_default_interest', 'capreduction', 'hidden', 'credit_filler', 'debit_filler', 'renewal_charge'])->all();
                    foreach ($breakdowns as $key => $breakdown) {
                        if($breakdown->description == 'refund' || $breakdown->description == 'other_charge_refund' || $breakdown->description == 'refund_default_interest' || $breakdown->description == 'capreduction' || $breakdown->description == 'credit_filler')
                        {
                            $paymentBreakDown[] = ['created_at' => $breakdown->created_at, 'description' => $breakdown->comments, 'amount' => "-".round($breakdown->credit, 2)];
                            if($breakdown->description != 'refund_default_interest')
                                $sumOfCharges -= $breakdown->credit;
                        }
                        else
                        {
                            $paymentBreakDown[] = ['created_at' => $breakdown->created_at, 'description' => $breakdown->comments, 'amount' => $breakdown->debit];
                            $sumOfCharges += $breakdown->debit;
                        }
                    }

                    $closingCharges = Apiato::call('ClosingCharge@FindClosingLoanChargeByIdTask', [$loan->id]);
                    foreach ($closingCharges as $closingCharge){
                        $paymentBreakDown[] = ['created_at' => ($statementsDate) ? Carbon::parse($statementsDate) : $payBeforeDate, 'description' => $closingCharge->description, 'amount' => round($closingCharge->value, 2)];
                    }

                    $minTermCharge = Apiato::call('ClosingCharge@GetMinTermBalanceTask', [$loan->id]);
                    if($minTermCharge->min_term_balance > 0){
                        $paymentBreakDown[] = ['created_at' => ($statementsDate) ? Carbon::parse($statementsDate) : $payBeforeDate, 'description' => 'Minimum Term Amount Pending', 'amount' => $minTermCharge->min_term_balance];
                    }

                    // $totalMonthlyInterests = $statements->when($interest, function($q) use($paymentPlanDate) {
                    //         return $q->where('created_at', '>', $paymentPlanDate);
                    // })->where('description', '=', 'interest')->all();

                    $totalMonthlyInterests = $statements->where('description', '=', 'interest')->all();

                    // Interest breakdowns
                    $grossInterest = collect($totalMonthlyInterests)->sum('debit');
                    // incase of new pmt plan and tnxs from new pmt plan date to onwards
                    // if($interest){
                    //     $debitsBeforeDueDate = Apiato::call('Payment@GetStatementsTask', [$loan, $paymentPlanDate])->last()->debit;
                    //     $grossInterest -= $debitsBeforeDueDate;
                    // }
                    $defaultRateInterest = 0;
                    $autoRefundInterest = 0;
                    $normalRateTnxInterest = 0;
                    foreach ($totalMonthlyInterests as $totalMonthlyInterest) {
                        
                        $defaultRateTnx = collect($totalMonthlyInterest->interestBreakDown)->where('rate', '=', "$defaltRate")->where('total', '>', 0)->all();
                        if($defaultRateTnx){
                            $defaultRateInterest += collect($defaultRateTnx)->sum('total');
                            
                            $defaultRateInterestBreakDown[] = $this->setInterestBreakDown($defaultRateTnx);
                        }                        

                        $autoRefundInterestTnx = collect($totalMonthlyInterest->interestBreakDown)->where('total', '<=', 0)->all();
                        if($autoRefundInterestTnx){
                            $autoRefundInterest -= collect($autoRefundInterestTnx)->sum('total');

                            $autoRefundInterestBreakDown[] = $this->setInterestBreakDown($autoRefundInterestTnx);
                        }

                        $normalRateTnx = collect($totalMonthlyInterest->interestBreakDown)->where('rate', '=', "$rate")->all();
                        if($normalRateTnx){
                            $normalRateTnxInterest += collect($normalRateTnx)->sum('total');
                            
                            $normalRateInterestBreakDown[] = $this->setInterestBreakDown($normalRateTnx);
                        }
                     }

                    // $credits = $statements->when($interest, function($q) use($paymentPlanDate) {
                    //         return $q->where('created_at', '>', $paymentPlanDate);
                    // })->where('description', '=', 'payment')->sum('credit');
                     $credits = Apiato::call('Payment@GetAllPaymentsTask', [$loan])->where('is_capital_reduction', '=', 0)->sum('amount');
                    // $interestAmount = $grossInterest - $credits;

                    if($defaultRateInterest != 0)
                        $paymentBreakDown[] = ['created_at' => $statements->last()->created_at, 'description' => 'Default Rate Interest Amount', 'amount' => round($defaultRateInterest, 2), 'sub_break_down' => $defaultRateInterestBreakDown];
                    
                    if($autoRefundInterest != 0)
                        $paymentBreakDown[] = ['created_at' => $statements->last()->created_at, 'description' => 'Auto Refunded Interest Amount', 'amount' => round(-$autoRefundInterest, 2), 'sub_break_down' => $autoRefundInterestBreakDown];

                    if($normalRateTnxInterest != 0)
                        $paymentBreakDown[] = ['created_at' => $statements->last()->created_at, 'description' => 'Normal Rate Interest Amount', 'amount' => round($normalRateTnxInterest, 2), 'sub_break_down' => $normalRateInterestBreakDown];                    

                    $finalPMT = round($netLoan + $sumOfCharges + $grossInterest + $closingCharges->sum('value') + $minTermCharge->min_term_balance, 2);

                    $paymentType = 'gross amount';

                    // $renewalCharges = Apiato::call('RenewalCharge@GetRenewalChargesByLoanIdTask', [$loanId])->last();

                    $createPmtPlanData = [
                        'id' => $i,
                        'version' => 'v'.$version,
                        'amount' => $finalPMT,
                        'amountPaid' => $credits,
                        'amountPending' => $finalPMT - $credits,
                        'expectedBalance' => 0, 
                        'status' => ($finalPMT - $credits > 0) ? 'pending' : 'paid',
                        'payBeforeDate' => ($renewalCharges != null) ? $renewalCharges->renew_end_date : $payBeforeDate,
                        'pmtType' => $paymentType,
                        'loanId' => $loanId,
                        'createdAt' => $startDate,
                        'updatedAt' => $startDate,
                        'paymentBreakDown' => $paymentBreakDown,
                    ];

                    $pmtPlan = $this->setPaymentPlan($createPmtPlanData);
                    array_push($updatedPaymentPlans, $pmtPlan);
                    break;
                } else {

                    // $sumOfPayments = $this->paymentRepository->scopeQuery(function($q) use($startDate, $payBeforeDate, $issueDate){
                    //     if($startDate != $issueDate)
                    //     {
                    //         $startDate = $payBeforeDate->copy()->startOfMonth();
                    //         $payBeforeDate = $payBeforeDate->copy()->endOfMonth();
                    //     }
                    //     return $q->whereBetween('paid_at', [$startDate->copy(), $payBeforeDate->copy()->endOfMonth()]);
                    // })->findWhere(['loan_id' => $loanId])->sum('amount');

                    if($startDate != $issueDate)
                        $startDate = $payBeforeDate->copy()->subMonth()->addDay();

                    $sumOfPayments = $this->getPaymentsSum($loanId, $startDate, $payBeforeDate) - $previousMonthDiff;
                    $previousMonthDiff = 0;
                    if(floatgt($monthlyPMT, $sumOfPayments)){
                        $sumRemainingDaysPayment = 0;
                        if($payBeforeDate->copy()->addDay()->lt($payBeforeDate->copy()->endOfMonth()))
                            $sumRemainingDaysPayment = $this->getPaymentsSum($loanId, $payBeforeDate->copy()->addDay(), $payBeforeDate->copy()->endOfMonth());
                        if(floatgte($sumOfPayments + $sumRemainingDaysPayment, $monthlyPMT)) {
                            $sumOfPayments += $previousMonthDiff = -($sumOfPayments - $monthlyPMT);
                            $sumRemainingDaysPayment -= $previousMonthDiff;
                        } else {
                            $previousMonthDiff = $sumRemainingDaysPayment;
                            $sumOfPayments += $previousMonthDiff;
                        }
                    }

                    $lastMonthEndDate = $payBeforeDate->copy()->startOfMonth()->subDay();
                    if($lastMonthEndDate <= $issueDate)
                        $lastMonthEndDate = $payBeforeDate;
                    if($loan->loan_type === 'half'){
                        $halfLoanSatements = Apiato::call($statementsTask, [$loan, $lastMonthEndDate, $virtualPmt, $halfLoan]);
                        // dd($halfLoanSatements, $halfLoan);
                        if(count($halfLoanSatements))
                            $balanceUptoLastMonthEnd = $halfLoanSatements->last()->balance;
                        else
                            $balanceUptoLastMonthEnd = $halfLoan['balance'];
                    } else
                        $balanceUptoLastMonthEnd = Apiato::call($statementsTask, [$loan, $lastMonthEndDate, $virtualPmt, $halfLoan])->last()->balance;
                    $expectedBalance = $balanceUptoLastMonthEnd - $monthlyPMT;
                    $createPmtPlanData = [
                        'id' => $i,
                        'version' => 'v'.$version,
                        'amount' => $monthlyPMT,
                        'amountPaid' => $sumOfPayments,
                        'amountPending' => $monthlyPMT - $sumOfPayments,
                        'expectedBalance' => ($expectedBalance > 0) ? $expectedBalance : 0, 
                        'status' => ($monthlyPMT <= $sumOfPayments) ? 'paid' : 'pending',
                        'payBeforeDate' => $payBeforeDate,
                        'pmtType' => $paymentType,
                        'loanId' => $loanId,
                        'createdAt' => $startDate,
                        'updatedAt' => $startDate,
                        'paymentBreakDown' => null
                    ];

                    // if($loan->loan_type === 'retained'){
                    //     $createPmtPlanData['amount'] = 0;
                    //     $createPmtPlanData['amountPending'] = 0;
                    //     $createPmtPlanData['amountPending'] = 0;
                    //     $createPmtPlanData['status'] = null;
                    // }

                    $pmtPlan = $this->setPaymentPlan($createPmtPlanData);
                    array_push($updatedPaymentPlans, $pmtPlan);
                }
                $startDate = $payBeforeDate->copy()->addDay();
            }
            return collect($updatedPaymentPlans);
        // } catch (Exception $ex) {
        //     throw new NotFoundException();
        // }
    }

    protected function setPaymentPlan(array $createPmtPlanData)
    {
        $paymentPlan = new PaymentPlan();
        $paymentPlan->setIdAttribute($createPmtPlanData['id']);
        $paymentPlan->setAmountAttribute(round($createPmtPlanData['amount'], 2));
        $paymentPlan->setPlanVersionAttribute($createPmtPlanData['version']);
        $paymentPlan->setAmountPaidAttribute(round($createPmtPlanData['amountPaid'], 2));
        $paymentPlan->setAmountPendingAttribute(round($createPmtPlanData['amountPending'], 2));
        $paymentPlan->setExpectedBalanceAttribute(round($createPmtPlanData['expectedBalance'], 2));
        $paymentPlan->setStatusAttribute($createPmtPlanData['status']);
        $paymentPlan->setPayBeforeAttribute($createPmtPlanData['payBeforeDate']);
        $paymentPlan->setTypeAttribute($createPmtPlanData['pmtType']);
        $paymentPlan->setLoanIdAttribute($createPmtPlanData['loanId']);
        $paymentPlan->setCreatedAtAttribute($createPmtPlanData['createdAt']);
        $paymentPlan->setUpdatedAtAttribute($createPmtPlanData['updatedAt']);
        $paymentPlan->setPaymentBreakDownAttribute($createPmtPlanData['paymentBreakDown']);


        return $paymentPlan;
    }

    protected function getPaymentsSum($loanId, $startDate, $endDate)
    {
        return $this->paymentRepository->scopeQuery(function($q) use($startDate, $endDate){
            return $q->whereBetween('paid_at', [$startDate, $endDate]);
        })->findWhere(['loan_id' => $loanId])->where('is_capital_reduction', '=', false)->sum('amount');
    }

    public function setInterestBreakDown($breakDownRequest)
    {
        $response = [];
        
        foreach($breakDownRequest as $breakDown)
        {
            $interestBreakDown = new InterestBreakDown;
    
            $interestBreakDown->setDailyRateAttribute(round($breakDown->daily_rate, 2));
            $interestBreakDown->setDaysAttribute($breakDown->days);
            $interestBreakDown->setBalanceAttribute(round($breakDown->balance, 2));
            $interestBreakDown->setRateAttribute($breakDown->rate);
            $interestBreakDown->setTotalAttribute(round($breakDown->total, 2));
            $interestBreakDown->setStartAttribute($breakDown->start);
            $interestBreakDown->setEndAttribute($breakDown->end);
            
            $response[] = $interestBreakDown;
        }
        return $response; 
    }
}