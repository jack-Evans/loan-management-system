<?php

namespace App\Containers\Payment\Tasks;

use App\Containers\Payment\Data\Repositories\PaymentRepository;
use App\Containers\Payment\Data\Repositories\JournalRepository;
use App\Containers\Payment\Data\Repositories\PostRepository;
use App\Ship\Exceptions\DeleteResourceFailedException;
use App\Ship\Parents\Tasks\Task;
use Apiato\Core\Foundation\Facades\Apiato;
use Exception;

class DeletePaymentTask extends Task
{

    protected $journalRepository;
    protected $postingRepository;
    protected $paymentRepository;

    public function __construct(JournalRepository $journalRepository, PostRepository $postingRepository, PaymentRepository $paymentRepository)
    {
        $this->journalRepository = $journalRepository;
        $this->postingRepository = $postingRepository;
        $this->paymentRepository = $paymentRepository;
    }

    public function run($id)
    {
        try {
            $payment = $payments = Apiato::call('Payment@FindPaymentByIdTask', [$id]);
            
            \DB::beginTransaction();
            foreach($payment->journal->posts as $post) {
                $this->postingRepository->delete($post->id);
            }
            $this->journalRepository->delete($payment->journal->id);
            $deletedPayment = $this->paymentRepository->delete($id);
            \DB::commit();
            return $deletedPayment;
        }
        catch (Exception $exception) {
            throw new DeleteResourceFailedException();
        }
    }
}
