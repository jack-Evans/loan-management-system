<?php

namespace App\Containers\Payment\Tasks;

use App\Containers\Payment\Data\Repositories\RefundRepository;
use App\Ship\Exceptions\CreateResourceFailedException;
use App\Ship\Parents\Tasks\Task;
use Exception;
use Apiato\Core\Foundation\Facades\Apiato;

class CreateRefundTask extends Task
{

    protected $refundRepository;

    public function __construct(RefundRepository $refundRepository)
    {
        $this->refundRepository = $refundRepository;
    }

    public function run(array $data, $loan, $assetType = 'refund')
    {
        try {
            
            $refundAt        = \Carbon\Carbon::parse($data['refund_at']['date']);
            $refundAmount     = $data['amount'];
            $description    = $data['description'];        
            $accountingPeriod   = "{$refundAt->format('M')} {$refundAt->year}";
            $debitAccountId  = $loan->customer->account->id;

            \DB::beginTransaction();

            // Refund Tnx

            $refund = $this->refundRepository->create([
                'name'    => $description,
                'amount'    => $refundAmount,
                'refund_at'   => $refundAt,
                'loan_id'   => $loan->id,
            ]);

            $journalPosting = [
                'creditAccountId' => $debitAccountId,
                'debitAccountId' => 0,
                'type' => $assetType,
                'loanid' => $loan->id,
                'tnxDate' => $refundAt,
                'amount' => $refundAmount,
                'accountingPeriod' => $accountingPeriod,
                'assetType' => $assetType,
                'description' => $description,
                'nominalCode' => (isset($data['nominalCode'])) ? $data['nominalCode'] : 1,
                'payableId' => $refund->id,
                'payableType' => 'App\Containers\Payment\Models\Refund',
                'hide' => (isset($data['hide'])) ? $data['hide'] : null,
                'hide_transaction' => (isset($data['hide_transaction'])) ? $data['hide_transaction'] : null,
            ];
            $refundJournalPosting = Apiato::call('Payment@CreateJournalPostingTask', [$journalPosting]);

            \DB::commit();
            return ['refund' => $refund, 'refundJournalPosting' => $refundJournalPosting];
        } catch (Exception $exception) {
            \DB::rollback();
            throw new CreateResourceFailedException();
        }
    }
}
