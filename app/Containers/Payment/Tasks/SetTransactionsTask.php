<?php
namespace App\Containers\Payment\Tasks;

use App\Ship\Parents\Tasks\Task;
use App\Containers\Loan\Models\Loan;
use Carbon\Carbon;
use Apiato\Core\Foundation\Facades\Apiato;
use App\Containers\Payment\Models\Statement;

class SetTransactionsTask extends Task
{
    public function run(Loan $loan, $journal, $balance, $paymentsInThisMonth, $requestDate = false)
    {
        $statementData = [];
        $statementData['id'] = $journal->id;
        $statementData['tnxDate'] = $journal->tnx_date;
        $statementData['nominalCode'] = $journal->nominalcode->code;
        $statementData['comments'] = $journal->comment;
        $statementData['description'] = $journal->posts->first()->asset_type;
        $statementData['tnxId'] = $journal->invoice_id;
        $statementData['hide'] = $journal->hide;
        $statementData['notes'] = $journal->notes;
        $statementData['hideTransaction'] = $journal->hide_transaction;
        if($journal->type === 'payment' || $journal->type === 'refund' || $journal->type === 'refund_default_interest' || $journal->type === 'other_charge_refund' || $journal->type === 'other_charge_adjustment' || $journal->type === 'credit_filler') {  
            
            if($journal->type === 'other_charge_refund'){
                $finalArr[] = Apiato::call('Payment@OtherChargeRefundBreakDownTask', [$loan, $journal]);
                $statementData['breakDown'] = Apiato::call('Statement@SetInterestBreakDownTask', [$finalArr]);
            }

            $statementData['credit'] = $journal->posts->sum(function($post) use ($statementData) {
                if($post->type == 'credit') {
                    return $post->amount;
                }
                return 0;   
            });
            $balanceBeforePayment = $balance;
            $ignoreCredits = 0;
            if($journal->type === 'refund_default_interest'){
                // $ignoreCredits += $statement->credit;
                $balance += $statementData['credit'];
                if($requestDate && $requestDate->format('M') == $statementData['tnxDate']->format('M'))
                    $statementData['tnxDate'] = $requestDate;
                else
                    $statementData['tnxDate']->endOfMonth();
            }
            $balance -= $statementData['credit'];
            $statementData['balance'] = $balance;
            // $statement->setDebitAttribute(null);
            // We got payment calculate balance here
            $ignorePmt = false;
            if($journal->type === 'refund_default_interest')
                $ignorePmt = $statementData['credit'];

            array_push($paymentsInThisMonth, ['date' => $journal->tnx_date, 'currentBalance' => $balance+$ignoreCredits, 'previousBalance' => $balanceBeforePayment, 'ignorePmt' => $ignorePmt]);
            $statement = Apiato::call('Payment@SetStatementTask', [$statementData]);
            // array_push($this->statements, $statement);
        }
        else
        {
           $statementData['debit'] = $journal->posts->sum(function($post) {
                if($post->type == 'debit') {
                    return $post->amount;
                }
                return 0;   
            });

            $balanceBeforePayment = $balance;
            $balance += $statementData['debit'];
            $statementData['balance'] = $balance;
            // $statement->setCreditAttribute(null);
            // We got debit calculate balance here
            array_push($paymentsInThisMonth, ['date' => $journal->tnx_date, 'currentBalance' => $balance, 'previousBalance' => $balanceBeforePayment, 'ignorePmt' => false]);
            $statement = Apiato::call('Payment@SetStatementTask', [$statementData]);
            // array_push($this->statements, $statement);
        }
        return [
            'paymentsInThisMonth' => $paymentsInThisMonth,
            'statement' => $statement,
            'balance' => $balance,
            'balanceBeforePayment' => $balanceBeforePayment,
        ];
    }
}