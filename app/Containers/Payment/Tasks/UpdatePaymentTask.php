<?php

namespace App\Containers\Payment\Tasks;

use App\Containers\Payment\Data\Repositories\PaymentRepository;
use App\Containers\Payment\Data\Repositories\PostRepository;
use App\Containers\Payment\Data\Repositories\JournalRepository;
use App\Ship\Exceptions\UpdateResourceFailedException;
use App\Ship\Parents\Tasks\Task;
use Apiato\Core\Foundation\Facades\Apiato;
use Exception;

class UpdatePaymentTask extends Task
{

    protected $journalRepository;
    protected $postingRepository;
    protected $paymentRepository;

    public function __construct(JournalRepository $journalRepository, PostRepository $postingRepository, PaymentRepository $paymentRepository)
    {
        $this->journalRepository = $journalRepository;
        $this->postingRepository = $postingRepository;
        $this->paymentRepository = $paymentRepository;
    }

    public function run($id, array $data)
    {
        try {
            \DB::beginTransaction();            
            $payment =  $this->paymentRepository->update($data, $id);
            $payment->journal;
            
            if(!is_null($data['paid_at']))
            {
              $paid_at = \Carbon\Carbon::parse($data['paid_at']);
              $data['accounting_period']  = "{$paid_at->format('M')} {$paid_at->year}";
              $this->journalRepository->update(['tnx_date' => $data['paid_at']], $payment->journal->id);
            }

            foreach($payment->journal->posts as $post) {
                $this->postingRepository->update($data, $post->id);
            }
            \DB::commit();

            $payment = $payments = Apiato::call('Payment@FindPaymentByIdTask', [$id]);
            return $payment;
        }
        catch (Exception $exception) {
            throw new UpdateResourceFailedException();
        }
    }
}
