<?php

namespace App\Containers\Payment\Tasks;

use App\Containers\Payment\Data\Repositories\DefaultRateRefundRepository;
use App\Ship\Parents\Tasks\Task;

class GetDefaultRateRefundByDateTask extends Task
{

    protected $repository;

    public function __construct(DefaultRateRefundRepository $repository)
    {
        $this->repository = $repository;
    }

    public function run($loanId, $startDate, $endDate, $byMonth = false)
    {
        if($byMonth)
            return $this->repository->orderBy('refund_at', 'ASC')
            ->scopeQuery(function($query) use ($loanId, $startDate, $endDate){
                $query->where('start_date', '>=', $startDate);
                $query->where('end_date', '<=', $endDate);
                return $query->where(['loan_id' => $loanId]);
            })->all();

        else
            return $this->repository->orderBy('refund_at', 'ASC')
            ->scopeQuery(function($query) use ($loanId, $startDate, $endDate){
                $query->where('start_date', '<=', $startDate);
                $query->where('end_date', '>=', $endDate);
            	return $query->where(['loan_id' => $loanId]);
            })->first();
    }
}
