<?php

namespace App\Containers\Payment\Tasks;

use App\Ship\Parents\Tasks\Task;
use App\Containers\Payment\Data\Repositories\AccountRepository;

class CreateAccountTask extends Task
{
	protected $repository;

    public function __construct(AccountRepository $repository)
    {
        $this->repository = $repository;
    }

    public function run(array $data)
    {
        $this->repository->create($data);
    }
}
