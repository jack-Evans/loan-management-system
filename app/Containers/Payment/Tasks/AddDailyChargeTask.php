<?php

namespace App\Containers\Payment\Tasks;

use App\Ship\Parents\Tasks\Task;
use App\Containers\Loan\Models\Loan;
use Carbon\Carbon;

class AddDailyChargeTask extends Task
{

    public function run(Loan $loan, Carbon $dt, $amount)
    {
        $interest 			= $loan->interest;
        $debitAccountId 	= $loan->customer->account->id;

        $accountingPeriod	= "{$dt->format('M')} {$dt->year}";

        try {
        	\DB::beginTransaction();
	    	$journalPosting = [
                'creditAccountId' => 0,
                'debitAccountId' => $debitAccountId,
                'type' => 'daily_interest',
                'loanid' => $loan->id,
                'tnxDate' => $dt,
                'amount' => $amount,
                'accountingPeriod' => $accountingPeriod,
                'assetType' => 'daily',
                'description' => '',
                'nominalCode' => 1,
                'payableId' => 0,
                'payableType' => ''
            ];
            $journalEntry = Apiato::call('Payment@CreateJournalPostingTask', [$journalPosting]);
	    	\DB::commit();
	    	$posting = $journalEntry->posts;
	    	return $posting;
		}
	    catch (Exception $exception) {
	    	\DB::rollback();
	        throw new CreateResourceFailedException();
	    }

	    return $defaultCharge;
    }
}
