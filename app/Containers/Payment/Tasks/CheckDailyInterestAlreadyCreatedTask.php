<?php

namespace App\Containers\Payment\Tasks;

use App\Ship\Parents\Tasks\Task;
use App\Containers\Payment\Data\Repositories\JournalRepository;
use App\Containers\Loan\Models\Loan;

class CheckDailyInterestAlreadyCreatedTask extends Task
{

	protected $repository;

    public function __construct(JournalRepository $repository)
    {
        $this->repository = $repository;
    }

    public function run(Loan $loan, \Carbon\Carbon $dt)
    {
        try {
            $count =  $this->repository->scopeQuery(function($q) use($loan, $dt) {
            	return $q->whereDate("tnx_date", "=", $dt->format('Y-m-d'))->where(['loan_id' => $loan->id])->where(function($q){
            		return $q->orWhere(['type' => 'daily_interest'])->orWhere(['type' => 'default_interest']);
            	});
            })->get()->count();
            if($count) {
            	return true;
            }
            return false;
        }
        catch (Exception $exception) {
            throw new NotFoundException();
        }
    }
}
