<?php

namespace App\Containers\Payment\Tasks;

use App\Containers\Payment\Data\Repositories\RefundRepository;
use App\Ship\Parents\Tasks\Task;
use App\Containers\Loan\Models\Loan;

class GetAllRefundsTask extends Task
{

    protected $repository;

    public function __construct(RefundRepository $repository)
    {
        $this->repository = $repository;
    }

    public function run($loanId)
    {
        return $this->repository->with(['journal'])->orderBy('refund_at', 'ASC')
        ->scopeQuery(function($query) use ($loanId){
        	return $query->where(['loan_id' => $loanId]);
        })->all();
    }
}
