<?php

namespace App\Containers\Payment\Tasks;

use App\Containers\Payment\Data\Repositories\DefaultRateRefundRepository;
use App\Ship\Exceptions\CreateResourceFailedException;
use App\Ship\Parents\Tasks\Task;
use Exception;
use Apiato\Core\Foundation\Facades\Apiato;

class CreateDefaultRateInterestTask extends Task
{
    protected $refundRepository;

    public function __construct(DefaultRateRefundRepository $refundRepository)
    {
        $this->refundRepository = $refundRepository;
    }

    public function run(array $data, $loan, $createJournal = true)
    {
        try {
            
            $refundAt        = \Carbon\Carbon::parse($data['refund_at']);
            $refundAmount     = $data['refund_amount'];
            $description    = $data['description'];        
            $accountingPeriod   = "{$refundAt->format('M')} {$refundAt->year}";
            $debitAccountId  = $loan->customer->account->id;
            $type = $data['type'];

            \DB::beginTransaction();

            // Refund Tnx

            $refund = $this->refundRepository->create($data);
            if($createJournal){
                $journalPosting = [
                    'creditAccountId' => $debitAccountId,
                    'debitAccountId' => 0,
                    'type' => $type,
                    'loanid' => $loan->id,
                    'tnxDate' => $refundAt,
                    'amount' => $refundAmount,
                    'accountingPeriod' => $accountingPeriod,
                    'assetType' => $type,
                    'description' => $description,
                    'payableId' => $refund->id,
                    'payableType' => 'App\Containers\Payment\Models\DefaultRateRefund',
                    'hide' => $data['hide'],
                    'hide_transaction' => $data['hide_transaction'],
                ];
                $refundJournalPosting = Apiato::call('Payment@CreateJournalPostingTask', [$journalPosting]);
            }

            \DB::commit();
            return $refund;
        } catch (Exception $exception) {
            \DB::rollback();
            throw new CreateResourceFailedException();
        }
    }
}
