<?php

namespace App\Containers\Payment\Tasks;

use App\Containers\Payment\Data\Repositories\JournalRepository;
use App\Ship\Exceptions\UpdateResourceFailedException;
use App\Ship\Parents\Tasks\Task;
use Apiato\Core\Foundation\Facades\Apiato;
use Exception;

class UpdateJournalTask extends Task
{

    protected $journalRepository;

    public function __construct(JournalRepository $journalRepository)
    {
        $this->journalRepository = $journalRepository;
    }

    public function run($id, array $data)
    {
        try {
            $journal =  $this->journalRepository->update($data, $id);
        }
        catch (Exception $exception) {
            throw new UpdateResourceFailedException();
        }
        
        return $journal;
    }
}
