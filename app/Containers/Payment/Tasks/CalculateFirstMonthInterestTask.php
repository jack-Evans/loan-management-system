<?php

namespace App\Containers\Payment\Tasks;

use App\Ship\Parents\Tasks\Task;
use App\Containers\Loan\Models\Loan;
use Apiato\Core\Foundation\Facades\Apiato;

class CalculateFirstMonthInterestTask extends Task
{
    public function run(Loan $loan): array
    {
        $issue_at        = $loan->issue_at;
        $endOfMonth      = $issue_at->endOfMonth();
        $diffIndays		 = $endOfMonth->diffInDays($loan->issue_at) + 1;
        $balanceMap      = Apiato::call('Payment@CalculateBalanceTask', [$loan]);

        $dailyRate  = calculateDailyRate($loan->interest->rate, $balanceMap['balance']);
        
        $totalDaily = round($dailyRate * $diffIndays, 2);

        $accountingPeriod   = "{$issue_at->format('M')} {$issue_at->year}";

        return [
        	'balanceMap' => $balanceMap,
        	'rate'  	 => $dailyRate,
        	'total'      => $totalDaily,
        	'days'       => $diffIndays,
        	'accountingPeriod' => $accountingPeriod,
        	'date'		 => $endOfMonth,
        ];
    }
}
