<?php

namespace App\Containers\Payment\Tasks;

use App\Ship\Parents\Tasks\Task;
use App\Containers\Payment\Data\Repositories\JournalRepository;
use App\Containers\Loan\Models\Loan;

class CheckFirstMonthInterestTask extends Task
{

    protected $repository;

    public function __construct(JournalRepository $repository)
    {
        $this->repository = $repository;
    }

    public function run(Loan $loan)
    {
    	try {
	    	$count = $this->repository->findWhere(['loan_id' => $loan->id, 'type' => 'first_month_interest'])->count();
	        if($count) {
	        	return true;
	        }
	        return false;
	    } catch (\Exception $exception) {
            throw $exception;
        }
    }
}
