<?php

namespace App\Containers\Payment\Tasks;

use App\Ship\Parents\Tasks\Task;
use Apiato\Core\Traits\ResponseTrait;
use Apiato\Core\Foundation\Facades\Apiato;
use App\Containers\Loan\Models\Loan;
use Carbon\Carbon;

class GetRetainedLoanStatementsTask extends Task
{
    use ResponseTrait;

    protected $statements;
    protected $issueDate;
    protected $interestBreakDown;
    protected $balance;
    protected $defaultRate;
    protected $grossLoan;

    public function run(Loan $loan, $statementDate = false, $refundStartDate = false, $refundAmount = 0)
    {
        $getJournals = Apiato::call('Payment@GetJournalsTask', [$loan, $statementDate]);
        $journals = $getJournals['journals'];
        $this->requestDate = $getJournals['requestDate'];

        // $this->sumLoanCharges = $loan->charges->sum(function($charge){
        //     return $charge->pivot->value;
        // });

        $refundStartDate = ($refundStartDate) ? Carbon::parse($refundStartDate) : $refundStartDate;
        $refunded = false;
        
        $this->statements = $this->interestBreakDown = [];
        $this->balance = $balanceForCalculation = 0;
        $currentDate = $previousDate = $this->issueDate = $lastTnxDate = $loan->issue_at;
        $this->loanClosingDate = addDecimalMonthsIntoDate($loan->interest->duration, $loan->issue_at)->subDay();
        $paymentsInThisMonth = [];
        $count = count($journals);
        $this->grossLoan = $loan->interest->gross_loan;
        $normalRate = $rate = $loan->interest->rate;
        $this->defaultRate = $loan->interest->default_rate;
        $interest = 0;
        $renewalCharges = Apiato::call('RenewalCharge@GetRenewalChargesByLoanIdTask', [$loan->id]);

        foreach($journals as $key => $journal)
        {
            $currentDate = $journal->tnx_date;
            $balanceForCalculation = $this->balance;
            $dateWithInDuration = null;

            // only used for refunds from CreateRefundAction
            if(($refundStartDate && $refundStartDate->lte($currentDate)) && $refunded == false) {
                $this->balance -= $refundAmount;
                $refunded = true;
                $this->refundRetained($loan, $refundStartDate, $refundAmount, $plusOne = true);
            }

            if($currentDate->gte($this->loanClosingDate)) {
                if(($previousDate->eq($this->issueDate))){
                    $interest += $this->getRetainedInterest($previousDate, $this->loanClosingDate, $balanceForCalculation, $loan, $rate);
                    $statement = $this->setRetainedInterest($previousDate, $this->loanClosingDate, $this->issueDate, $interest, $balanceForCalculation);
                    $this->balance = $statement->balance;
                    $interest = 0;
                    $this->interestBreakDown = [];
                }
                $previousDate = $this->loanClosingDate;

                if(count($renewalCharges) > 0) {
                    $this->issueDate = $this->loanClosingDate->copy()->addDay();
                    $dateWithInDuration = $renewalCharges->where('renew_end_date', '>=', $currentDate)->where('renew_date', '<=', $currentDate)->last();
                    if($dateWithInDuration != null){
                        // if previously we were charging at default rate, add default interest upto renew start date.
                        if($rate === $this->defaultRate){
    
                            if($lastTnxDate->month < $currentDate->month || $lastTnxDate->year < $currentDate->year){
                                $this->genereateMonths($loan, $lastTnxDate, $currentDate->copy()->subDay(), $paymentsInThisMonth);
                                $paymentsInThisMonth = [];
                            }
                            $startDate = $this->loanClosingDate->gt($currentDate->copy()->startOfMonth()) ? $this->loanClosingDate->copy()->addDay() : $currentDate->copy()->startOfMonth();
                            $this->calculateMonthInterest($loan, $this->balance, $startDate, $dateWithInDuration->renew_date->copy()->subDay(), $paymentsInThisMonth, $rate);
    
                            $paymentsInThisMonth = [];
                        }
                        elseif($currentDate->gt($this->issueDate)){
                            $this->genereateMonths($loan, $this->issueDate, $currentDate->copy()->subDay(), $paymentsInThisMonth);
                            $startDate = $this->loanClosingDate->gt($currentDate->copy()->startOfMonth()) ? $this->loanClosingDate->copy()->addDay() : $currentDate->copy()->startOfMonth();
                            $this->calculateMonthInterest($loan, $this->balance, $startDate, $currentDate->copy()->subDay(), $paymentsInThisMonth, $this->defaultRate);
                        }
                        $rate = $normalRate;
                        $this->issueDate = $dateWithInDuration->renew_date;
                        $this->loanClosingDate = $dateWithInDuration->renew_end_date->copy()->subDay();
                    }
                    else
                        $rate = $this->defaultRate;
                }
            }
            if($previousDate->ne($currentDate)){
                if( ($previousDate->eq($this->issueDate)) || ($key === ($count-1) && $previousDate->eq($this->issueDate)) ) {
                    // $interest += $this->getRetainedInterest($previousDate, $currentDate, $balanceForCalculation, $loan, $rate);
                    $interest += $this->getRetainedInterest($this->issueDate, $this->loanClosingDate, $balanceForCalculation, $loan, $rate);
                    $statement = $this->setRetainedInterest($this->issueDate, $this->loanClosingDate, $this->issueDate, $interest, $this->balance);
                    $this->balance = $statement->balance;
                    $interest = 0;
                    $this->interestBreakDown = [];
                }
                $previousDate = $currentDate;
            }


            if( count($renewalCharges) == 0 && ($lastTnxDate->month < $currentDate->month || $lastTnxDate->year < $currentDate->year) ) {
                // var_dump($lastTnxDate->format('Y M d'), $currentDate->format('Y M d'));
                if($currentDate->gt($this->loanClosingDate)){
                    if(count($renewalCharges) === 0)
                        $lastTnxDate = $this->loanClosingDate;
                    $dateWithInDuration = $renewalCharges->where('renew_end_date', '>=', $currentDate)->where('renew_date', '<=', $currentDate)->first();
                    if($dateWithInDuration == null){
                        $lastTnxDate = $this->loanClosingDate;
                        $rate = $this->defaultRate;
                        if($lastTnxDate->month === $this->loanClosingDate->month)
                            $lastTnxDate = $this->loanClosingDate->copy()->addDay();
                        // $currentMonthStart = $journal->tnx_date->copy()->startOfMonth();
                        // $lastMonthStart = $lastTnxDate->copy()->startOfMonth();
                        // $this->genereateMonths($loan, $lastMonthStart, $currentMonthStart, $paymentsInThisMonth);                        
                    }
                    else{
                        // dd($lastTnxDate);
                        // $statement = $this->calculateMonthInterest($loan, $this->balance, $lastTnxDate, $journalTnxDate, $paymentsInThisMonth, $rate);
                        $rate = $normalRate;
                    }
                }
                $paymentsInThisMonth = [];
            }
            // we have entered into defaults, calculate at default rate with payments in months
            if( ($rate === $this->defaultRate && $lastTnxDate->gt($this->loanClosingDate)) && ($lastTnxDate->month < $journal->tnx_date->month || $lastTnxDate->year < $journal->tnx_date->year) ) {
                // var_dump($lastTnxDate->gt($this->loanClosingDate), $lastTnxDate->format('Y M d'), $currentDate->format('Y M d'));
                $this->genereateMonths($loan, $lastTnxDate, $currentDate, $paymentsInThisMonth);
                $paymentsInThisMonth = [];
            }
            
            if( $key === ($count-1) && ( ($rate == $this->defaultRate) && ($lastTnxDate->month < $currentDate->month || $lastTnxDate->year < $currentDate->year) && $lastTnxDate->eq($this->loanClosingDate)) ){
                $this->genereateMonths($loan, $lastTnxDate, $currentDate->copy()->subDay(), []);
            }

            if($journal->type === 'payment' || $journal->type === 'refund' || $journal->type === 'refund_default_interest' || $journal->type === 'other_charge_refund' || $journal->type === 'other_charge_adjustment' || $journal->type === 'credit_filler') {
                
                $transactions = Apiato::call('Payment@SetTransactionsTask', [$loan, $journal, $this->balance, $paymentsInThisMonth]);
                $statement = $transactions['statement'];
                array_push($this->statements, $statement);
                if($rate === $this->defaultRate)
                    $paymentsInThisMonth = $transactions['paymentsInThisMonth'];
                $this->balance = $transactions['balance'];
                $balanceBeforePayment = $transactions['balanceBeforePayment'];

                if( ($currentDate->ne($this->issueDate) && $rate === $normalRate) && $journal->type != 'other_charge_adjustment' ){
                    $this->refundRetained($loan, $journal->tnx_date, $statement['credit'], $plusOne = true);
                }
                if($this->balance <= 0)
                {
                    break;
                }
            }
            else
            {
                $transactions = Apiato::call('Payment@SetTransactionsTask', [$loan, $journal, $this->balance, $paymentsInThisMonth]);
                $statement = $transactions['statement'];
                array_push($this->statements, $statement);
                if($rate === $this->defaultRate)
                    $paymentsInThisMonth = $transactions['paymentsInThisMonth'];
                $this->balance = $transactions['balance'];
                $balanceBeforePayment = $transactions['balanceBeforePayment'];
            
                if( ($currentDate->ne($this->issueDate) && $rate === $normalRate) && $journal->type != 'renewal_charge')
                    $this->refundRetained($loan, $journal->tnx_date, $statement['debit'], $credit = false);
            }

            if($key === ($count-1) && ( ($this->issueDate->eq($currentDate) || $rate == $this->defaultRate) || ($this->requestDate && $this->requestDate->gt($this->loanClosingDate)) ) ) {
                if($rate == $this->defaultRate){
                //     $this->genereateMonths($loan, $lastTnxDate, $currentDate, $paymentsInThisMonth);
                    // dd($lastTnxDate, $currentDate, $previousDate, $this->loanClosingDate);
                    // if(($lastTnxDate->month < $currentDate->month || $lastTnxDate->year < $currentDate->year) && $lastTnxDate->eq($this->loanClosingDate) ){
                    //     $this->genereateMonths($loan, $lastTnxDate, $currentDate->copy()->subDay(), []);
                    // }
                    $startDate = $this->loanClosingDate->gt($currentDate->copy()->startOfMonth()) ? $this->loanClosingDate : $currentDate->copy()->startOfMonth();
                    $statement = $this->calculateMonthInterest($loan, $this->balance, $startDate, $currentDate, $paymentsInThisMonth, $rate);
                }

                if($this->requestDate && $this->requestDate->gt($this->loanClosingDate)){
                    
                    // if($previousDate->eq($currentDate)){
                    if($previousDate->eq($this->issueDate)){
                        $interest += $this->getRetainedInterest($this->issueDate, $this->loanClosingDate, $this->balance, $loan, $rate);
                        $statement = $this->setRetainedInterest($this->issueDate, $this->loanClosingDate, $this->issueDate, $interest, $this->balance);
                        $this->balance = $statement->balance;
                        $this->interestBreakDown = [];
                    }

                    $rate = $this->defaultRate;
                    $lastTnxDate = $this->loanClosingDate->copy()->addDay();
                    $currentMonthStart = $this->loanClosingDate->copy()->startOfMonth();
                    $lastMonthStart = $this->requestDate->copy()->startOfMonth();
                    $diffInMonths = $lastMonthStart->diffInMonths($currentMonthStart);
                    for($i = 0; $i < $diffInMonths; $i++) {
                        $payments = [];
                        $statement = $this->calculateMonthInterest($loan, $this->balance, $lastTnxDate, $lastTnxDate->copy()->endOfMonth(), [], $rate);
                        $lastTnxDate->startOfMonth()->addMonth();
                    }

                    $statement = $this->calculateMonthInterest($loan, $this->balance, $lastTnxDate, $this->requestDate, [], $rate);
                } else {
                    $currentDate = $this->loanClosingDate;
                    $interest += $this->getRetainedInterest($this->issueDate, $currentDate, $this->balance, $loan, $rate);
                    $statement = $this->setRetainedInterest($this->issueDate, $currentDate, $this->issueDate, $interest, $this->balance);
                    $this->balance = $statement->balance;
                    $this->interestBreakDown = [];
                }
            }

            $previousDate = $lastTnxDate = $currentDate;

        }
        // return collect(sortByDate($this->statements, 'created_at'))->sortBy('description')->where('description', '!=', 'skip');
        return collect(sortByDate($this->statements, 'created_at'))->where('description', '!=', 'skip');
    }

    public function refundRetained($loan, $startDate, $amount, $credit)
    {
        $text = 'Retained interest';
        if($credit)
            $text = 'Adjustment';
        $totalInterest = 0;
        $breakDowns = Apiato::call('Statement@CreateInterestBreakDownTask', [$loan, $loan->interest->rate, $amount, $startDate, $this->loanClosingDate, $plusOne = true]);
        if($breakDowns != false){
            $this->interestBreakDown[] = $breakDowns;
            $totalInterest += collect($breakDowns)->sum('total');
        }
        $statement = $this->setRetainedInterest($startDate, $this->loanClosingDate, $startDate, $totalInterest, $this->balance, $credit, $text);
        // dd($this->balance, $statement);
        $this->balance = $statement->balance;
        $this->interestBreakDown = [];
    }

    public function getRetainedInterest($startDate, $endDate, $balance, $loan, $rate)
    {
        $endDate = $endDate->copy()->addDay();
        $totalInterest = 0;
        if($rate != $this->defaultRate){
            if($startDate->ne($this->issueDate))
                $startDate = $startDate->copy()->addDay();

            $breakDowns = Apiato::call('Statement@CreateInterestBreakDownTask', [$loan, $rate, $this->grossLoan, $startDate, $endDate, $plusOne = true]);
            if($breakDowns != false){
                $grossAmount = $loan->interest->gross_loan;
                $closingDate = addDecimalMonthsIntoDate($loan->interest->duration, $loan->issue_at)->subDay();
                // var_dump($startDate->format('Y M d'), $closingDate->format('Y M d'));
                if($startDate->gte($closingDate)) {
                // if($startDate->gt($this->loanClosingDate)){
                    $this->grossLoan = $this->balance;
                    // $duration = $this->issueDate->diffInMonths($this->loanClosingDate);
                    // $startDate = $this->issueDate;
                    // $endDate = $this->loanClosingDate;
                    $months = $startDate->diffInMonths($endDate);
                    $days = $startDate->copy()->addMonths($months)->diffInDays($endDate);
                    $days += $days ? 1 : 0;
                    $duration = $months + round($startDate->copy()->addMonths($months)->diffInDays($endDate)/30.4, 2);
                } else
                    $duration = $loan->interest->duration;
                
// var_dump($duration, $months, $days);
                // var_dump($startDate->copy()->addMonths($months)->diffInDays($endDate));

                $monthlyPmt = calculateMonthlyPMT($loan->interest->rate, $this->grossLoan);
                $totalInterest += round(($monthlyPmt * $duration), 2);
                
                // $totalDays = ceil($duration*30.4);
                $totalDays = $startDate->diffInDays($endDate);
                
                $dailyCharge = round($totalInterest/$totalDays, 2);
                
                $this->interestBreakDown[][] = [
                    'dailyRate' => $dailyCharge,
                    'days' => $totalDays,
                    'balance' => $this->grossLoan,
                    'rate' => $rate,
                    'total' => round($dailyCharge*$totalDays, 2),
                    'start' => $startDate,
                    'end' => $endDate->copy()->subDay(),
                    'rounding_error' => round($totalInterest-($dailyCharge*$totalDays), 2),
                ];
                // dd($totalInterest, $totalDays, $dailyCharge, $breakDowns, $this->interestBreakDown);
                // var_dump($startDate->format('Y m d'), $loan->issue_at->copy()->subDay()->addMonths($loan->interest->duration)->format('Y m d'));
                // var_dump($monthlyPmt, $duration, $totalInterest);
            }
        }
        return $totalInterest;
    }

    public function setRetainedInterest($startDate, $endDate, $tnxDate, $interest, $balance, $credit = false, $text = 'Retained interest')
    {
        // $text = ($credit) ? "Adjustment" : "Retained interest";
        $statementData = [];
        $statementData['tnxDate'] = $tnxDate;
        // var_dump($statementData['tnxDate']->format('Y m d'));
        $statementData['description'] = ($interest) ? 'interest' : 'skip';
        $statementData['comments'] = "$text from {$startDate->copy()->format('M d y')} to {$endDate->format('M d y')}";
        if($credit)
            $statementData['credit'] = $interest;
        else
            $statementData['debit'] = $interest;
        $statementData['balance'] = ($credit) ? $this->balance - $interest : $this->balance + $interest;
        $statementData['breakDown'] = Apiato::call('Statement@SetInterestBreakDownTask', [$this->interestBreakDown]);

        $statement = Apiato::call('Payment@SetStatementTask', [$statementData]);
        array_push($this->statements, $statement);
        $this->interestBreakDown = [];
        return $statement;
    }

    public function calculateMonthInterest($loan, $balance, $startDate, $endOfMonth, $payments, $rate)
    {
        if($startDate->copy()->addDay()->eq($this->issueDate))
            $startDate = $startDate->copy()->addDay();
        $tnxStartDate = $startDate;
        $this->interestBreakDown = [];
        $totalInterest = 0;
        if(count($payments)) {
            $sotredPayments = sortByDate($payments);
            foreach($sotredPayments as $key => $payment) {
                // to handle multiple payments at one date OR at first day of month.(skip other payments on same date)
                if($startDate == $payment['date'] && $key > 0 || $payment['date'] == $payment['date']->copy()->startOfMonth())
                    continue;
                $breakDown = Apiato::call('Statement@CreateInterestBreakDownTask', [$loan, $rate, $payment['previousBalance'], $startDate, $payment['date']->copy()->subDay(), $plusOne = true]);
                if($breakDown != false){
                    $totalInterest += collect($breakDown)->sum('total');
                    $this->interestBreakDown[] = $breakDown;
                }
                $startDate = $payment['date'];
            }
        }
        // var_dump($startDate->format('Y M d'), $endOfMonth->format('Y M d'));
        if($startDate->lte($endOfMonth)){
            $breakDown = Apiato::call('Statement@CreateInterestBreakDownTask', [$loan, $rate, $this->balance, $startDate, $endOfMonth, $plusOne = true]);
            if($breakDown != false){
                $totalInterest += collect($breakDown)->sum('total');
                $this->interestBreakDown[] = $breakDown;
            }
        }
        $statement = $this->setRetainedInterest($tnxStartDate, $endOfMonth, $endOfMonth, $totalInterest, $this->balance, false, 'Default interest');
        $this->balance = $statement->balance;
    }

    public function genereateMonths($loan, $lastTnxDate, $journalTnxDate, $paymentsInThisMonth)
    {
        $rate = $this->defaultRate;
        if($lastTnxDate->month === $this->loanClosingDate->month)
            $lastTnxDate = $this->loanClosingDate->copy()->addDay();
        $currentMonthStart = $journalTnxDate->copy()->startOfMonth();
        $lastMonthStart = $lastTnxDate->copy()->startOfMonth();
        $diffInMonths = $lastMonthStart->diffInMonths($currentMonthStart);
        for($i = 0; $i < $diffInMonths; $i++) {
            if($lastTnxDate->ne($this->loanClosingDate->copy()->addDay()))
                $lastTnxDate = $lastTnxDate->copy()->startOfMonth();
            $statement = $this->calculateMonthInterest($loan, $this->balance, $lastTnxDate, $lastTnxDate->copy()->endOfMonth(), $paymentsInThisMonth, $rate);
            $lastTnxDate->startOfMonth()->addMonth();
            $paymentsInThisMonth = [];
        }
        // if($lastTnxDate->lt($journalTnxDate)){
        //     // $payments = collect($paymentsInThisMonth)->where('date', '>=', $lastTnxDate)->where('date', '<=', $journalTnxDate);
        //     $payments = $paymentsInThisMonth;
        //     // if($lastTnxDate->ne($this->loanClosingDate->copy()->addDay()))
        //     //     $lastTnxDate = $lastTnxDate->copy()->startOfMonth();
        //     $statement = $this->calculateMonthInterest($loan, $this->balance, $lastTnxDate, $journalTnxDate, $payments, $rate);
        // }
    }
}
