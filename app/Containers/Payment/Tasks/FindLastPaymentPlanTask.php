<?php

namespace App\Containers\Payment\Tasks;

use App\Containers\Payment\Data\Repositories\PaymentPlanRepository;
use App\Ship\Exceptions\NotFoundException;
use App\Ship\Parents\Tasks\Task;
use Exception;

class FindLastPaymentPlanTask extends Task
{

    protected $repository;

    public function __construct(PaymentPlanRepository $repository)
    {
        $this->repository = $repository;
    }

    public function run($loanId)
    {
        try {
            return $this->repository->findWhere(['loan_id' => $loanId])->last();
        }
        catch (Exception $exception) {
            throw new NotFoundException();
        }
    }
}
