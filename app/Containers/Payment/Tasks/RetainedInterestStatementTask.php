<?php

namespace App\Containers\Payment\Tasks;

use App\Ship\Parents\Tasks\Task;
use App\Containers\Loan\Models\Loan;
use Apiato\Core\Foundation\Facades\Apiato;

class RetainedInterestStatementTask extends Task
{
    public function run(Loan $loan, $balance, $lastTnxDate, $paymentsInThisMonth = [], $journalTnxDate, $statements, $currentMonthInterestAmount = false)
    {
        if($currentMonthInterestAmount == false){
            $tempStatement = Apiato::call('Statement@CalculateMonthInterestTask', [$loan, $balance, $lastTnxDate, $paymentsInThisMonth, $journalTnxDate, $statements]);
            $currentMonthInterestAmount = $tempStatement->debit;
        }

        $previousInterest = collect(sortByDate($statements, 'created_at'))->where('description', '=', 'interest')->sum('debit');

        $interestAmount = collect(sortByDate($statements, 'created_at'))->where('description', '=', 'interest')->sum('debit') + $currentMonthInterestAmount - $previousInterest;

        $balance += $interestAmount;        

        $data = [
            'tnxDate' => $journalTnxDate,
            'debit' => $interestAmount,
            'credit' => null,
            'balance' => $balance,
            'arrears' => null,
            'description' => 'interest',
            'comments' => "Retained Interest",
        ];

        return $data;
    }
}
