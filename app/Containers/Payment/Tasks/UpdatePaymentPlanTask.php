<?php

namespace App\Containers\Payment\Tasks;

use App\Containers\Payment\Data\Repositories\PaymentPlanRepository;
use App\Ship\Exceptions\UpdateResourceFailedException;
use App\Ship\Parents\Tasks\Task;
use Apiato\Core\Foundation\Facades\Apiato;
use Exception;

class UpdatePaymentPlanTask extends Task
{

    protected $paymentPlanRepository;

    public function __construct(PaymentPlanRepository $paymentPlanRepository)
    {
        $this->paymentPlanRepository = $paymentPlanRepository;
    }

    public function run($id, array $data)
    {
        try {
            return $this->paymentPlanRepository->update($data, $id);            
        }
        catch (Exception $exception) {
            throw new UpdateResourceFailedException();
        }
    }
}
