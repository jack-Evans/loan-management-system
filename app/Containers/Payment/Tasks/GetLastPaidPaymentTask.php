<?php

namespace App\Containers\Payment\Tasks;

use App\Containers\Payment\Data\Repositories\JournalRepository;
use App\Ship\Exceptions\NotFoundException;
use App\Ship\Parents\Tasks\Task;
use Exception;
use App\Containers\Loan\Models\Loan;
use Carbon\Carbon;

class GetLastPaidPaymentTask extends Task
{

    protected $repository;

    public function __construct(JournalRepository $repository)
    {
        $this->repository = $repository;
    }

    public function run(Loan $loan, Carbon $dt = null)
    {
        try {
            // \DB::enableQueryLog();
            $data =   $this->repository->scopeQuery(function($q) use ($loan, $dt){
                if($dt) {
                    $date = $dt->format('Y-m-d');
                    $q = $q->whereRaw("MONTH(tnx_date) = MONTH('$date') AND YEAR(tnx_date) = YEAR('$date')");
                }
                else {
                    $q = $q->latest('tnx_date');
                }
                return $q->latest('tnx_date')->where(['type' => 'payment', 'loan_id' => $loan->id]);
            })->first();
            // dd($data);
            // dd(\DB::getQueryLog());
            return $data;
        }
        catch (Exception $exception) {
            throw new NotFoundException();
        }
    }
}
