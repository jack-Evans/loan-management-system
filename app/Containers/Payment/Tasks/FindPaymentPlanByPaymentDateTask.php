<?php

namespace App\Containers\Payment\Tasks;

use App\Containers\Payment\Data\Repositories\PaymentPlanRepository;
use App\Ship\Parents\Tasks\Task;
use App\Ship\Exceptions\NotFoundException;
use Carbon\Carbon;

class FindPaymentPlanByPaymentDateTask extends Task
{
    protected $repository;

    public function __construct(PaymentPlanRepository $repository)
    {
        $this->repository = $repository;
    }

    public function run($loanId, $paymentDate)
    {      
        try
        {
            return $this->repository->scopeQuery(function($q) use($loanId, $paymentDate){
                return $q->whereBetween('pay_before', [$paymentDate->copy()->startOfMonth(), $paymentDate->copy()->endOfMonth()]);
            })->findWhere(['loan_id' => $loanId])->first();
            
        } catch (Exception $ex) {
            throw new NotFoundException();
        }
    }
}