<?php

namespace App\Containers\Payment\Tasks;

use App\Ship\Parents\Tasks\Task;
use App\Ship\Exceptions\NotFoundException;
use App\Containers\Payment\Data\Repositories\JournalRepository;

class PaymentDetailsTask extends Task
{

    protected $repository;
    
    public function __construct(JournalRepository $repository)
    {
        $this->repository = $repository;
    }

    public function run($loanid)
    {
        try {
            return $this->repository->with('creditPosts')->findWhere(
                    [
                        'loan_id'   => $loanid,
                        'type'      => 'payment',
                    ]);
        }
        catch (Exception $exception) {
            throw new NotFoundException();
        }
    }
}
