<?php

namespace App\Containers\Payment\Tasks;

use App\Ship\Parents\Tasks\Task;
use App\Containers\Loan\Models\Loan;
use Apiato\Core\Foundation\Facades\Apiato;
use Carbon\Carbon;
use App\Containers\Payment\Data\Transporters\CreateRefundTransporter;

class RetainedLoanRefundTask extends Task
{
    protected $totalDays;
    protected $rate;
    protected $amount;
    protected $refundAmount;
    protected $totalInterest;

    public function run(Loan $loan, $startDate, $refundDate, $amount, $description)
    {
        $this->totalInterest = 0;
        $this->amount = $amount;
        $rate = $loan->interest->rate;
        $defaultRate = $loan->interest->default_rate;
        $chargeDate = $startDate;
        // $endDate = $refundDate;
        $loanClosingDate = $loan->issue_at->copy()->addMonths($loan->interest->duration);
        // todo: find and fix a little bug here relating to "($chargeDate->gt($loanClosingDate)) ? $refundDate"
        $endingDate = ($refundDate->gt($loanClosingDate) ? ($chargeDate->gt($loanClosingDate)) ? $refundDate : $loanClosingDate : $refundDate);
        $this->calculateInterest($startDate, $endingDate, $rate, 1);
        
        $startDate = ($refundDate->gt($loanClosingDate) ? $loanClosingDate->copy()->addDay() : $refundDate);
        $renewalCharges = Apiato::call('RenewalCharge@GetRenewalChargesByLoanIdTask', [$loan->id]);
        if(count($renewalCharges) > 0){
            $relatedRenewals = $renewalCharges->where('renew_end_date', '>=', $chargeDate)->all();
            if(count($relatedRenewals)) {
                foreach($relatedRenewals as $key => $renewalCharge) {
                    $renewDate = $renewalCharge->renew_date;
                    if($loanClosingDate->lt($renewDate)) {
                        $this->calculateInterest($loanClosingDate->copy()->addDay(), $renewDate->copy()->subDay(), $rate+$defaultRate, 1);
                        $startDate = $renewDate;
                    }
                    $endingDate = ($refundDate->gte($renewalCharge->renew_end_date)) ? $renewalCharge->renew_end_date : $refundDate;
                    $this->calculateInterest($startDate, $endingDate, $rate, 1);
                    $startDate = $endingDate->copy()->addDay();
                    $loanClosingDate = $renewalCharge->renew_end_date;
                }
            }
        }
        if($startDate->lt($refundDate)){
            $rate = $defaultRate+$rate;
            if($startDate->copy()->endOfMonth()->lte($refundDate)){
                // $totalDays = $startDate->diffInDays($startDate->copy()->endOfMonth())+1;
                $this->calculateInterest($startDate, $startDate->copy()->endOfMonth(), $rate, 1);
                $startDate = $startDate->copy()->endOfMonth();
            }
            else{
                $this->calculateInterest($startDate, $refundDate, $rate, 0);
                // $totalDays = $startDate->diffInDays($refundDate);
                $startDate = $refundDate;
            }

            $diffInMonths = $startDate->diffInMonths($refundDate);
            if($diffInMonths){
                for($i = 0; $i < $diffInMonths; $i++) {
                    $duration = 1;
                    $monthlyPmt = calculateMonthlyPMT($rate, $this->amount);
                    $this->refundAmount = round(($monthlyPmt * $duration), 8);
                    $this->totalInterest += $this->refundAmount;
                    $this->amount += $this->refundAmount;

                    $startDate = $startDate->copy()->addMonth();
                    // dd($totalInterest, $amount, $refundAmount, $monthlyPmt);
                }
                $this->calculateInterest($startDate, $refundDate, $rate, 1);
            }
        }
        // dd($this->totalInterest, $this->amount);
        $extraChargeRequest = [
            'loan_id' => $loan->id,
            'amount' => round($this->totalInterest, 2),
            'description'       => "Extra interest refund from {$chargeDate->copy()->format('M d y')} to {$refundDate->format('M d y')} for: " .$description,
            'refund_at' => $refundDate,
        ];
        $extraChargeRefundArr = Apiato::call('Payment@CreateRefundTask', [(new CreateRefundTransporter($extraChargeRequest))->toArray(), $loan, $assetType = 'other_charge_adjustment']);
        $extraChargeRefund = $extraChargeRefundArr['refund'];
        // dd($extraChargeRefund);
        return $extraChargeRefund;
    }

    public function calculateInterest($startDate, $endDate, $rate, $plusOne = 1)
    {
        $this->totalDays = $startDate->diffInDays($endDate) + $plusOne;
        $duration = round($this->totalDays/30.4, 8);
        $monthlyPmt = calculateMonthlyPMT($rate, $this->amount);
        $this->refundAmount = round(($monthlyPmt * $duration), 8);
        $this->totalInterest += $this->refundAmount;
        $this->amount += $this->refundAmount;
    }
}