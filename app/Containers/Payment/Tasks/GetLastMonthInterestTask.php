<?php

namespace App\Containers\Payment\Tasks;

use App\Ship\Parents\Tasks\Task;
use App\Containers\Payment\Data\Repositories\JournalRepository;
use App\Ship\Exceptions\NotFoundException;
use Exception;
use App\Containers\Loan\Models\Loan;
use Carbon\Carbon;

class GetLastMonthInterestTask extends Task
{

    protected $repository;

    public function __construct(JournalRepository $repository)
    {
        $this->repository = $repository;
    }

    public function run(Loan $loan, Carbon $dt = null)
    {
    	$journalsQuery = $this->repository;
        // \DB::enableQueryLog();

        try {
            $journalsQuery = $journalsQuery->scopeQuery(function($q) use ($loan, $dt){
            	if($dt) {
            		$q = $q->where("tnx_date", "<=", $dt);
            	}
            	else {
            		$q = $q->latest('tnx_date');
            	}
                return $q->where(['loan_id' => $loan->id])->where(function($q){
                	return $q->orWhere(['type' => 'daily_interest'])->orWhere(['type' => 'default_interest']);
                });
            });
            $data = $journalsQuery->orderBy('tnx_date', 'desc')->first();
           
            // dd(\DB::getQueryLog());
            return $data;
        }
        catch (Exception $exception) {
            throw new NotFoundException();
        }
    }
}
