<?php

namespace App\Containers\Payment\Tasks;

use App\Containers\Payment\Data\Repositories\PaymentPlanRepository;
use App\Ship\Parents\Tasks\Task;

class CreatePaymentPlanTask extends Task
{
	protected $repository;

    public function __construct(PaymentPlanRepository $repository)
    {
        $this->repository = $repository;
    }

    public function run(array $paymentPlan)
    {
        try{
            return $this->repository->create($paymentPlan);
        }
        catch (Exception $exception) {
            throw new CreateResourceFailedException();
        }
    }
}
