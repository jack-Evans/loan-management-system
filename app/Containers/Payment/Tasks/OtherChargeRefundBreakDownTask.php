<?php

namespace App\Containers\Payment\Tasks;

use App\Ship\Parents\Tasks\Task;
use Apiato\Core\Foundation\Facades\Apiato;
use App\Containers\Loan\Models\Loan;
use Carbon\Carbon;

class OtherChargeRefundBreakDownTask extends Task
{
    public function run(Loan $loan, $journal)
    {
        $breakDowns = Apiato::call('Statement@GetAllBreakdownByRefundIdTask', [$loan->id, $journal->payable_id]);
        $innerArr = $finalArr = [];
        foreach ($breakDowns as $key => $breakDown) {
            $interestBreakDown['dailyRate'] = $breakDown->daily_rate;
            $interestBreakDown['days'] = $breakDown->days;
            $interestBreakDown['balance'] = $breakDown->balance;
            $interestBreakDown['rate'] = $breakDown->rate;
            $interestBreakDown['total'] = $breakDown->total;
            $interestBreakDown['start'] = Carbon::parse($breakDown->start_date);
            $interestBreakDown['end'] = Carbon::parse($breakDown->end_date);

            $innerArr[] = $interestBreakDown;
        }

        return $innerArr;
    }
}