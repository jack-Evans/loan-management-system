<?php

namespace App\Containers\Payment\Tasks;

use App\Ship\Parents\Tasks\Task;
use Apiato\Core\Traits\ResponseTrait;
use Apiato\Core\Foundation\Facades\Apiato;
use App\Containers\Loan\Models\Loan;
use Carbon\Carbon;

class GetMonthlyRetainedLoanStatementsTask extends Task
{
    use ResponseTrait;

    protected $statements;
    protected $issueDate;
    protected $interestBreakDown;
    protected $balance;
    protected $defaultRate;
    protected $grossLoan;
    // protected $renewalCharges;

    public function run(Loan $loan, $statementDate = false, $refundStartDate = false, $refundAmount = 0, $closingBalance = false)
    {
        $getJournals = Apiato::call('Payment@GetJournalsTask', [$loan, $statementDate]);
        $journals = $getJournals['journals'];
        $this->requestDate = $getJournals['requestDate'];

        $refundStartDate = ($refundStartDate) ? Carbon::parse($refundStartDate) : $refundStartDate;
        $refunded = false;
        
        $this->statements = $this->interestBreakDown = [];
        $this->balance = $balanceForCalculation = 0;
        $currentDate = $previousDate = $this->issueDate = $lastTnxDate = $loan->issue_at;
        $this->loanClosingDate = addDecimalMonthsIntoDate($loan->interest->duration, $loan->issue_at)->copy()->subDay();
        $paymentsInThisMonth = [];
        $count = count($journals);
        $this->grossLoan = $loan->interest->gross_loan;
        $normalRate = $rate = $loan->interest->rate;
        $this->defaultRate = $loan->interest->default_rate;
        $balance = $interest = 0;
        $renewalCharges = Apiato::call('RenewalCharge@GetRenewalChargesByLoanIdTask', [$loan->id]);
        $transactionId = null;

        foreach($journals as $key => $journal){
            // If we are in new month
            if($lastTnxDate->month < $journal->tnx_date->month || $lastTnxDate->year < $journal->tnx_date->year) {
                $currentMonthStart = $journal->tnx_date->copy()->startOfMonth();
                $lastMonthStart = $lastTnxDate->copy()->startOfMonth();
                $diffInMonths = $lastMonthStart->diffInMonths($currentMonthStart);
                for($i = 0; $i < $diffInMonths; $i++) {
                    $statement = Apiato::call('Statement@CalculateMonthlyRetainedInterestTask', [$loan, $balance, $lastTnxDate, $paymentsInThisMonth, $this->requestDate, $this->grossLoan, $renewalCharges, $transactionId, $this->loanClosingDate]);
                    $transactionId = null;
                    $balance = $statement->balance;
                    array_push($this->statements, $statement);
                    $lastTnxDate = $lastTnxDate->copy()->startOfMonth()->addMonth();
                    $paymentsInThisMonth = [];
                }
            }
            // if($journal->tnx_date->gte($this->loanClosingDate) && $journal->tnx_date->month == $this->loanClosingDate->month && $lastTnxDate->lte($this->loanClosingDate)) {
            //     $statement = Apiato::call('Statement@CalculateMonthlyRetainedInterestTask', [$loan, $balance, $lastTnxDate, $paymentsInThisMonth, $this->loanClosingDate, $this->grossLoan, $renewalCharges]);
            //     $balance = $statement->balance;
            //     array_push($this->statements, $statement);
            //     $lastTnxDate = $this->loanClosingDate->copy()->addDay();
            //     $paymentsInThisMonth = [];
            // }

            // if($journal->type === 'payment' || $journal->type === 'refund' || $journal->type === 'ignore' || $journal->type === 'other_charge_refund' || $journal->type === 'credit_filler') {
            //     $transactions = $this->setTransactions($loan, $journal, $balance, $paymentsInThisMonth, $this->requestDate);
            //     $paymentsInThisMonth = $transactions['paymentsInThisMonth'];
            //     $statement = $transactions['statement'];
            //     array_push($this->statements, $statement);
            //     $balance = $transactions['balance'];
            // } else {
                if($journal->type === 'interest'){
                    $transactionId = $journal->invoice_id;
                    continue;
                }
                $transactions = $this->setTransactions($loan, $journal, $balance, $paymentsInThisMonth, $this->requestDate, $renewalCharges);
                $paymentsInThisMonth = $transactions['paymentsInThisMonth'];
                $statement = $transactions['statement'];
                array_push($this->statements, $statement);
                $balance = $transactions['balance'];

            // only used for refunds from CreateRefundAction
            if(($refundStartDate && $refundStartDate->eq($journal->tnx_date)) && $refunded == false) {
                $balance -= $refundAmount;
                $this->grossLoan -= $refundAmount;
                $refunded = true;
            }
            // }
            // if last iteration, add interest upto month end 
            // var_dump($journal->tnx_date->gte($this->loanClosingDate), $journal->tnx_date->format('Y M d'));
            if($balance <= 0) {
                $closingChargesTnxs = Apiato::call('Statement@AddClosingChargeMinTermTransactionsTask', [$loan->id, $journal->tnx_date, $balance, $this->statements]);
                $totalClosingChargesAmount = $closingChargesTnxs['totalChargeAmount'];
                $this->statements = $closingChargesTnxs['statements'];
                // dd($totalClosingChargesAmount);
                $balance += $totalClosingChargesAmount;
                // $statement = Apiato::call('Statement@CalculateMonthInterestTask', [$loan, $balance, $lastTnxDate, $paymentsInThisMonth, $this->requestDate, $this->statements]);
                $statement = Apiato::call('Statement@CalculateMonthlyRetainedInterestTask', [$loan, $balance, $lastTnxDate, $paymentsInThisMonth, $this->requestDate, $this->grossLoan, $renewalCharges, $transactionId, $this->loanClosingDate]);
                $tempBalance = $statement->balance;
                if($tempBalance <= 0){
                    $balance = $tempBalance;
                    array_push($this->statements, $statement);
                    $paymentsInThisMonth = [];
                    break;
                }
            }

            if($key == ($count - 1))
            {
                $statement = Apiato::call('Statement@CalculateMonthlyRetainedInterestTask', [$loan, $balance, $lastTnxDate, $paymentsInThisMonth, $this->requestDate, $this->grossLoan, $renewalCharges, $transactionId, $this->loanClosingDate]);
                $balance = $statement->balance;
                array_push($this->statements, $statement);
                $lastTnxDate = $lastTnxDate->copy()->startOfMonth()->addMonth();
                $paymentsInThisMonth = [];

                if($this->requestDate && $this->requestDate->gt($lastTnxDate)){
                    $currentMonthStart = $lastTnxDate;
                    $lastMonthStart = $this->requestDate;
                    $diffInMonths = $lastMonthStart->diffInMonths($currentMonthStart);
                    for($i = 0; $i < $diffInMonths; $i++) {
                        $statement = Apiato::call('Statement@CalculateMonthlyRetainedInterestTask', [$loan, $balance, $lastTnxDate, $paymentsInThisMonth, $this->requestDate, $this->grossLoan, $renewalCharges, $transactionId, $this->loanClosingDate]);
                        $balance = $statement->balance;
                        array_push($this->statements, $statement);
                        $lastTnxDate = $lastTnxDate->copy()->startOfMonth()->addMonth();
                        // $paymentsInThisMonth = [];
                    }
                    if($lastTnxDate->diffInDays($this->requestDate)){
                        $statement = Apiato::call('Statement@CalculateMonthlyRetainedInterestTask', [$loan, $balance, $lastTnxDate, $paymentsInThisMonth, $this->requestDate, $this->grossLoan, $renewalCharges, $transactionId, $this->loanClosingDate]);
                        $balance = $statement->balance;
                        array_push($this->statements, $statement);
                        // $lastTnxDate->startOfMonth()->addMonth();
                    }
                }
            }
        }
        if($closingBalance){
            $loanCloseStatements = Apiato::call('Statement@AddClosingChargeMinTermTransactionsTask', [$loan->id, $statementDate, $balance, $this->statements]);
            $this->statements = $loanCloseStatements['statements'];
            // $totalClosingChargesAmount = $loanCloseStatements['totalChargeAmount'];
            // $balance += $totalClosingChargesAmount;
        }
        $roundingError = $loan->options->where('name', 'rounding error');
        if(count($roundingError)) {
            $value = collect($roundingError)->first()->value;
            $journal->posts[0]->asset_type = $journal->posts[1]->asset_type = $journal->type = ($value >= 0) ? 'debit_filler' : 'credit_filler';
            $journal->comment = 'Rounding error adjustment';
            $journal->tnx_date = addDecimalMonthsIntoDate($loan->interest->duration, $loan->issue_at)->copy()->subDay();
            $journal->posts[0]->amount = $journal->posts[1]->amount = abs($value);
            // dd($journal->posts[0], $journal->posts[0]);
            $transactions = $this->setTransactions($loan, $journal, $balance, $paymentsInThisMonth, $this->requestDate, $renewalCharges);
                $paymentsInThisMonth = $transactions['paymentsInThisMonth'];
                $statement = $transactions['statement'];
                array_push($this->statements, $statement);
        }
        return collect(sortByDate($this->statements, 'created_at'))->where('description', '!=', 'skip');
    }

    public function setTransactions($loan, $journal, $balance, $paymentsInThisMonth, $requestDate, $renewalCharges)
    {
        $statementData = [];
        $statementData['id'] = $journal->id;
        $currentDate = $statementData['tnxDate'] = $journal->tnx_date;
        $statementData['nominalCode'] = $journal->nominalcode->code;
        $statementData['comments'] = $journal->comment;
        $statementData['description'] = $journal->posts->first()->asset_type;
        $statementData['tnxId'] = $journal->invoice_id;
        $statementData['hide'] = $journal->hide;
        $statementData['hideTransaction'] = $journal->hide_transaction;
        $statementData['notes'] = $journal->notes;

        if($this->loanClosingDate->lte($currentDate) && count($renewalCharges)){
            $startDateWithInDuration = $renewalCharges->where('renew_end_date', '>=', $currentDate)->where('renew_date', '<=', $currentDate)->first();
            if($startDateWithInDuration){
                $this->loanClosingDate = $startDateWithInDuration->renew_end_date->copy()->subDay();
            }
        }

        $balanceBeforePayment = $balance;
        if($journal->type === 'payment' || $journal->type === 'refund' || $journal->type === 'refund_default_interest' || $journal->type === 'other_charge_refund' || $journal->type === 'other_charge_adjustment' || $journal->type === 'credit_filler') {
            
            $statementData['credit'] = $journal->posts->sum(function($post) use ($statementData) {
                if($post->type == 'credit') {
                    return $post->amount;
                }
                return 0;
            });
            $statementData['balance'] = $balance -= $statementData['credit'];
            if($currentDate->ne($loan->issue_at) && $currentDate->lte($this->loanClosingDate) || ($currentDate->month == $this->loanClosingDate->month && $currentDate->gt($this->loanClosingDate))) {
                $balanceBeforePayment = $this->grossLoan;
                if($journal->type != 'refund_default_interest')
                    $this->grossLoan -= $statementData['credit'];
            }
        }
        else
        {
           $statementData['debit'] = $journal->posts->sum(function($post) {
                if($post->type == 'debit') {
                    return $post->amount;
                }
                return 0;
            });
            
            $statementData['balance'] = $balance += $statementData['debit'];
            // var_dump($currentDate, $this->loanClosingDate);
            if($currentDate->ne($loan->issue_at) && $currentDate->lte($this->loanClosingDate) || ($currentDate->month == $this->loanClosingDate->month && $currentDate->gt($this->loanClosingDate)) ){
                $balanceBeforePayment = $this->grossLoan;
                $this->grossLoan += $statementData['debit'];
            } 
            if($journal->type === 'tranch' || $journal->type === 'loan')
            {
                if(isset($journal->payable->gross_loan)){
                    $tranchGross = $journal->payable->gross_loan - $loan->interest->gross_loan;
                    $dailyRate = calculateDailyRate($loan->interest->rate, $tranchGross, $loan, $loan->issue_at);
                    $retainedInterest = getRetainedInterest($dailyRate, $loan);
                    $this->grossLoan += $retainedInterest;
                    $loan->interest->gross_loan = $journal->payable->gross_loan;
                }
            }
            // elseif($currentDate->month == $this->loanClosingDate->month && $currentDate->gt($this->loanClosingDate)){
            //     $balanceBeforePayment = $this->grossLoan;
            //     $this->grossLoan += $statementData['debit'];
            // }

        }
        // var_dump($balance, $balanceBeforePayment);
        array_push($paymentsInThisMonth, ['date' => $journal->tnx_date, 'currentBalance' => $balance, 'previousBalance' => $balanceBeforePayment, 'ignorePmt' => false]);
        
        $statement = Apiato::call('Payment@SetStatementTask', [$statementData]);
        return [
            'paymentsInThisMonth' => $paymentsInThisMonth,
            'statement' => $statement,
            'balance' => $balance,
            'balanceBeforePayment' => $balanceBeforePayment,
        ];
    }
}