<?php

namespace App\Containers\Payment\Tasks;

use App\Ship\Parents\Tasks\Task;
use App\Containers\Loan\Models\Loan;
use Apiato\Core\Foundation\Facades\Apiato;
use App\Containers\Payment\Models\Statement;

class SetStatementTask extends Task
{
    public function run($data)
    {
        $statement = new Statement;
        $statement->setIdAttribute(isset($data['id']) ? $data['id'] : null);
        $statement->setCreatedAtAttribute(isset($data['tnxDate']) ? $data['tnxDate'] : null);
        $statement->setDebitAttribute(isset($data['debit']) ? $data['debit'] : null);
        $statement->setCreditAttribute(isset($data['credit']) ? $data['credit'] : null);
        $statement->setBalanceAttribute(isset($data['balance']) ? $data['balance'] : null);
        $statement->setArrearsAttribute(isset($data['arrears']) ? $data['arrears'] : null);
        $statement->setDescriptionAttribute(isset($data['description']) ? $data['description'] : null);
        $statement->setCommentsAttribute(isset($data['comments']) ? $data['comments'] : null);
        $statement->setInterestBreakDownAttribute(isset($data['breakDown']) ? $data['breakDown'] : null);
        $statement->setNominalCodeAttribute(isset($data['nominalCode']) ? $data['nominalCode'] : null);
        $statement->setTransactionIdAttribute(isset($data['tnxId']) ? $data['tnxId'] : null);
        $statement->setHideAttribute(isset($data['hide']) ? $data['hide'] : null);
        $statement->setHideTransactionAttribute(isset($data['hideTransaction']) ? $data['hideTransaction'] : null);
        $statement->setNotesAttribute(isset($data['notes']) ? $data['notes'] : null);

        return $statement;
    }
}
