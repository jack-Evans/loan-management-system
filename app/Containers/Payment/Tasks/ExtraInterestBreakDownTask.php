<?php

namespace App\Containers\Payment\Tasks;

use App\Ship\Parents\Tasks\Task;
use Apiato\Core\Foundation\Facades\Apiato;
use Carbon\Carbon;

class ExtraInterestBreakDownTask extends Task
{

    public function run($loan, $refund, $otherChargeDate, $refundAt, $refundAmount)
    {
        $statements = Apiato::call('Payment@GetStatementsTask', [$loan])->where('description', '=', 'interest')->all();
        $extraInterest = 0;
        $otherChargeDate = Carbon::parse($otherChargeDate);
        $otherChargeMonth = $otherChargeDate->format('m');
        // $refundAmount = (float) $refundRequest['amount'];
        // $tnxCount = 0;
        foreach($statements as $interests){
            foreach ($interests->interestBreakDown as $key => $interest) {
                if($interest->start->lt($otherChargeDate))
                    continue;
                elseif($interest->start->gte($refundAt))
                    break;
                else{
                    $balanceForCalculation = $interest->balance;
                    if($interest->start->format('m') > $otherChargeMonth) {
                        $otherChargeMonth = $interest->start->format('m');
                        $refundAmount += $extraInterest;
                        $extraInterest = 0;

                        $defaultRateRefunds = Apiato::call('Payment@GetDefaultRateRefundByDateTask', [$loan->id, $interest->start->copy()->startOfMonth(), $interest->start->copy()->endOfMonth(), $byMonth = true]);
                        if($defaultRateRefunds){
                            foreach ($defaultRateRefunds as $key => $defaultRateRefund) {
                                $dailyRate = round(calculateDailyRate($defaultRateRefund->rate, $defaultRateRefund->balance - $refundAmount), 2);
                                $extraInterest -= $breakDownInterest = round($defaultRateRefund->refund_amount - ($dailyRate * $defaultRateRefund->days), 2);

                                if($interest->end->format('m') < $refundAt->format('m'))
                                    $this->createOtherChargeBreakDown($loan, $refund->payable_id, $defaultRateRefund, $refundAmount, -$breakDownInterest);
                            }
                        }
                    }

                    $dailyRate = round(calculateDailyRate($interest->rate, $balanceForCalculation - $refundAmount), 2);
                    $extraInterest += $breakDownInterest = round($interest->total - ($dailyRate * $interest->days), 2);
                    // var_dump($interest->total , $dailyRate , $interest->days);
                    if($interest->end->format('m') < $refundAt->format('m'))
                        $this->createOtherChargeBreakDown($loan, $refund->payable_id, $interest, $refundAmount, $breakDownInterest);
                }
            }
        }
        // dd($extraInterest, $refundAmount);
        return [
            'extraInterest' => $extraInterest,
            'refundAmount' => $refundAmount,
        ];
    }

    public function createOtherChargeBreakDown($loan, $refundId, $interest, $refundAmount, $extraInterest){
        $breakdownRequest = [
            'loan_id' => $loan->id,
            'refund_id' => $refundId,
            'daily_rate' => round(calculateDailyRate($interest->rate, $refundAmount), 2),
            'days' => $interest->days,
            'balance' => $refundAmount,
            'rate' => $interest->rate,
            'total' => $extraInterest,
            'start_date' => isset($interest->start) ? $interest->start : $interest->start_date,
            'end_date' => isset($interest->end) ? $interest->end: $interest->end_date,
        ];
        Apiato::call('Statement@CreateBreakdownTask', [$breakdownRequest]);
    }
}