<?php

namespace App\Containers\Payment\Tasks;

use App\Ship\Parents\Tasks\Task;
use Apiato\Core\Foundation\Facades\Apiato;
use App\Containers\Loan\Models\Loan;

class CheckCreatePaymentPreCheckTask extends Task
{
    public function run(Loan $loan, array $data)
    {
        $paid_at = \Carbon\Carbon::parse($data['paid_at']);
        if($loan->issue_at->gt($paid_at))
            throw new \App\Containers\Payment\Exceptions\HTTPPreConditionFailedException("Payment paid at should be greater than {$loan->issue_at->format("d M Y")}");
        // Apiato::call('Payment@CheckPaymentDateIsGreaterThanLoanIssueDateTask', [$loan, $paid_at]);
        $subDays = 1;
        if($loan->issue_at->eq($paid_at))
            $subDays = 0;
        Apiato::call('Payment@CheckPaymentWithCurrentBalanceTask', [$loan, $paid_at, $data['amount'], 'pay', $subDays]);

        // $lastPaidPayment  = Apiato::call('Payment@GetLastPaidPaymentTask', [$loan, $paid_at]);
        
        // if($lastPaidPayment) {
        // 	$paymentPeriod = $paid_at->format("M")." ".$paid_at->year;
        // 	throw new \App\Containers\Payment\Exceptions\HTTPPreConditionFailedException("Payment for $paymentPeriod has already paid");
        // }

        // $interest = $loan->interest;
        // $monthlyPMT = calculateMonthlyPMT($interest->rate, $interest->gross_loan);
        
        // if($request->amount > $monthlyPMT) {
        // 	throw new \App\Containers\Payment\Exceptions\HTTPPreConditionFailedException("You cannot pay more than Monthly PMT $monthlyPMT");
        // }
    }
}
