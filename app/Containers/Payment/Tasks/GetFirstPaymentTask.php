<?php

namespace App\Containers\Payment\Tasks;

use App\Containers\Payment\Data\Repositories\PaymentRepository;
use App\Ship\Exceptions\NotFoundException;
use App\Ship\Parents\Tasks\Task;
use App\Containers\Loan\Models\Loan;
use Apiato\Core\Foundation\Facades\Apiato;
use Exception;

class GetFirstPaymentTask extends Task
{

    protected $repository;

    public function __construct(PaymentRepository $repository)
    {
        $this->repository = $repository;
    }

    public function run(Loan $loan)
    {
        try {
            // Check no payment has been made to this loan
            // $paid_amount        = Apiato::call('Payment@GetTotalPaidAmountTask', [$loan]);
            // if($paid_amount != 0) {
            //     throw new NotFoundException();
            // }

            // Get loan interest
            $loanInterest       = $loan->interest;

            // Calculate per month payment
            $perMonthPayment    = perMonth($loanInterest->gross_loan, $loanInterest->duration);

            // Find balance after paying first payment
            $current_balance    = $loanInterest->gross_loan - $perMonthPayment;
            // dd($current_balance);

            // Get payment by its first time current balance
            return $this->repository->findWhere(['loan_id' => $loan->id, 'status' => 'pending', 'current_balance' => $current_balance]);
        }
        catch (Exception $exception) {
            throw new NotFoundException();
        }
    }
}
