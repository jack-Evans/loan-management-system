<?php

namespace App\Containers\Payment\UI\API\Transformers;

use App\Containers\Payment\Models\Statement;
use App\Ship\Parents\Transformers\Transformer;
use Carbon\Carbon;

class StatementTransformer extends Transformer
{
    /**
     * @var  array
     */
    protected $defaultIncludes = [

    ];

    /**
     * @var  array
     */
    protected $availableIncludes = [

    ];

    /**
     * @param Post $entity
     *
     * @return array
     */
    public function transform(Statement $entity)
    {
        $explanation = '';
        if($entity->description == 'loan')
            $explanation = 'loan';
        elseif($entity->description == 'other_charge')
            $explanation = 'other charge';
        elseif($entity->description == 'charge')
            $explanation = 'charge';
        elseif($entity->description == 'interest')
            $explanation = 'Monthly interest';
        elseif($entity->description == 'payment')
            $explanation = 'Monthly payment';
        elseif($entity->description == 'refund_default_interest')
            $explanation = 'refund interest';
        elseif($entity->description == 'other_charge_refund')
            $explanation = 'other charge refund';
        elseif($entity->description == 'capreduction')
            $explanation = 'capital reduction';
        elseif($entity->description == 'refund')
            $explanation = 'refund';

        $response = [
            'object' => 'Statements',
//            'id' => $entity->getHashedKey(),
            'id' => ($entity->id) ? $entity->getHashedKey() : null,
            'debit' => ($entity->debit != null) ? round($entity->debit, 2) : null,
            // 'debit' => ($entity->debit != null) ? number_format($entity->debit, 2) : null,
            'credit' => ($entity->credit != null) ? round($entity->credit, 2) : null,
            // 'credit' => ($entity->credit != null) ? number_format($entity->credit, 2) : null,
            'balance' => round($entity->balance, 2),
            // 'balance' => number_format($entity->balance, 2),
            'arrears' => $entity->arrears,
            'description' => $entity->description,
            'explanation' => $explanation,
            'interestBreakDown' => $entity->interestBreakDown,
            'comments' => $entity->comments,
            'created_at' => $entity->created_at,
            'nominalCode' => $entity->nominalCode,
            'transactionId' => $entity->transactionId,
            'hide' => $entity->hide,
            'hideTransaction' => $entity->hideTransaction,
            'notes' => $entity->notes,

        ];

        $response = $this->ifAdmin([
            'real_id'    => $entity->id,
            // 'deleted_at' => $entity->deleted_at,
        ], $response);

        return $response;
    }
}
