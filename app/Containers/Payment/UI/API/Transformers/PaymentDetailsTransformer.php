<?php

namespace App\Containers\Payment\UI\API\Transformers;

use App\Containers\Payment\Models\Journal;
use App\Ship\Parents\Transformers\Transformer;
use Apiato\Core\Traits\HashIdTrait;

class PaymentDetailsTransformer extends Transformer
{
    use HashIdTrait;
    /**
     * @var  array
     */
    protected $defaultIncludes = [

    ];

    /**
     * @var  array
     */
    protected $availableIncludes = [

    ];

    /**
     * @param Journal $entity
     *
     * @return array
     */
    public function transform(Journal $entity)
    {
        $posts = [];
        foreach($entity->creditPosts as $post) {
            $posts[] = [
                'object'            => 'Posts',
                'tid'               => $this->encode($post->id),
                'assetType'         => $post->asset_type,
                'accountingPeriod'  => $post->accounting_period,
                'amount'            => $post->amount,
                'type'              => $post->type,
                'created_at'        => $post->created_at,
                'updated_at'        => $post->updated_at,
            ];
        }
        $response = [
            'object' => 'Journal',
            'id' => $entity->getHashedKey(),
            'default_credit_account_id' => $entity->default_credit_account_id,
            'default_debit_account_id' => $entity->default_debit_account_id,
            'loan_id' => $entity->loan_id,
            'invoice_id' => $entity->invoice_id,
            'type' => $entity->type,
            'tnx_date' => $entity->tnx_date,
            'created_at' => $entity->created_at,
            'updated_at' => $entity->updated_at,
            'deleted_at' => $entity->deleted_at,
            'posts' => $posts,

        ];

        $response = $this->ifAdmin([
            'real_id'    => $entity->id,
            // 'deleted_at' => $entity->deleted_at,
        ], $response);

        return $response;
    }
}
