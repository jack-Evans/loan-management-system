<?php

namespace App\Containers\Payment\UI\API\Transformers;

use App\Containers\Payment\Models\Post;
use App\Ship\Parents\Transformers\Transformer;

class PostTransformer extends Transformer
{
    /**
     * @var  array
     */
    protected $defaultIncludes = [

    ];

    /**
     * @var  array
     */
    protected $availableIncludes = [

    ];

    /**
     * @param Post $entity
     *
     * @return array
     */
    public function transform(Post $entity)
    {
        $response = [
            'object' => 'Post',
            'id' => $entity->getHashedKey(),
            'account_id' => $entity->account_id,
            'journal_id' => $entity->journal_id,
            'asset_type' => $entity->asset_type,
            'accounting_period' => $entity->accounting_period,
            'amount' => $entity->amount,
            'type' => $entity->type,
            'created_at' => $entity->created_at,
            'updated_at' => $entity->updated_at,
            'deleted_at' => $entity->deleted_at,

        ];

        $response = $this->ifAdmin([
            'real_id'    => $entity->id,
            // 'deleted_at' => $entity->deleted_at,
        ], $response);

        return $response;
    }
}
