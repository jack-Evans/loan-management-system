<?php

namespace App\Containers\Payment\UI\API\Transformers;

use App\Ship\Parents\Transformers\Transformer;
use App\Containers\Payment\Models\Statement;

class FinalDueAmountTransformer extends Transformer
{
    /**
     * @var  array
     */
    protected $defaultIncludes = [

    ];

    /**
     * @var  array
     */
    protected $availableIncludes = [

    ];

    /**
     * @param Payment $entity
     *
     * @return array
     */
    public function transform(Statement $entity)
    {
        $response = [
            'object'        => 'Statement',
            'balance'        => $entity->balance,
            'comments'        => $entity->comments,
            'created_at'        => $entity->created_at,

        ];

        $response = $this->ifAdmin([
            'real_id'    => $entity->id,
            // 'deleted_at' => $entity->deleted_at,
        ], $response);

        return $response;
    }
}
