<?php

namespace App\Containers\Payment\UI\API\Transformers;

use App\Ship\Parents\Transformers\Transformer;
use App\Containers\Payment\Models\DefaultRateRefund;

class DefaultRateRefundTransformer extends Transformer
{
    /**
     * @var  array
     */
    protected $defaultIncludes = [

    ];

    /**
     * @var  array
     */
    protected $availableIncludes = [

    ];

    /**
     * @param Post $entity
     *
     * @return array
     */
    public function transform(DefaultRateRefund $entity)
    {
        $response = [
            'object' => 'DefaultRateRefunds',
            'id' => $entity->getHashedKey(),
            'loan_id' => $entity->loan_id,
            'start_date' => $entity->start_date,
            'end_date' => $entity->end_date,
            'days' => $entity->days,
            'rate' => $entity->rate,
            'daily_rate' => $entity->daily_rate,
            'refund_amount' => $entity->refund_amount,            
            'balance' => $entity->balance,
            'refund_at' => $entity->refund_at,
            'description' => $entity->description,
        ];

        $response = $this->ifAdmin([
            'real_id'    => $entity->id,
            // 'deleted_at' => $entity->deleted_at,
        ], $response);

        return $response;
    }
}
