<?php

namespace App\Containers\Payment\UI\API\Transformers;

use App\Containers\Payment\Models\MonthStatement;
use App\Ship\Parents\Transformers\Transformer;
use Carbon\Carbon;

class MonthStatementTransformer extends Transformer
{
    /**
     * @var  array
     */
    protected $defaultIncludes = [

    ];

    /**
     * @var  array
     */
    protected $availableIncludes = [

    ];

    /**
     * @param Post $entity
     *
     * @return array
     */
    public function transform(MonthStatement $entity)
    {
        // dd(Carbon::parse('29-08-2020'));
        $response = [
            'object' => 'MonthStatements',
            'opening_balance' => round($entity->opening_balance, 2),
            'closing_balance' => round($entity->closing_balance, 2),
            'gross_loan' => round($entity->gross_loan, 2),
            'interest_charged' => round($entity->interest_charged, 2),
            'start_date' => Carbon::parse($entity->start_date)->format('d/m/Y'),
            'end_date' => Carbon::parse($entity->end_date)->format('d/m/Y'),
            'statement_month' => Carbon::parse($entity->end_date)->format('F'),
            'current_interest_rate' => round($entity->current_interest_rate, 2),
            'interest_due_date' => Carbon::parse($entity->interest_due_date)->format('d/m/Y'),
            'loan_end_date' => Carbon::parse($entity->loan_end_date)->format('d/m/Y'),
        ];

        $response = $this->ifAdmin([
            'real_id'    => $entity->id,
        ], $response);

        return $response;
    }
}
