<?php

namespace App\Containers\Payment\UI\API\Transformers;

use App\Containers\Payment\Models\Refund;
use App\Ship\Parents\Transformers\Transformer;

class RefundTransformer extends Transformer
{
    /**
     * @var  array
     */
    protected $defaultIncludes = [

    ];

    /**
     * @var  array
     */
    protected $availableIncludes = [

    ];

    /**
     * @param Refund $entity
     *
     * @return array
     */
    public function transform(Refund $entity)
    {
        $response = [
            'object'        => 'Refund',
            'id'            => $entity->getHashedKey(),
            'loan_id'       => $entity->loan_id,
            'amount'        => $entity->amount,
            'refund_at'     => $entity->refund_at,
            'journal'       => $entity->journal,
            'created_at'    => $entity->created_at,
            'updated_at'    => $entity->updated_at,

        ];

        $response = $this->ifAdmin([
            'real_id'    => $entity->id,
            // 'deleted_at' => $entity->deleted_at,
        ], $response);

        return $response;
    }
}
