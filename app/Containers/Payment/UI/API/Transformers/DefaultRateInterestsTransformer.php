<?php

namespace App\Containers\Payment\UI\API\Transformers;

use App\Ship\Parents\Transformers\Transformer;
use App\Containers\Payment\Models\InterestBreakDown;

class DefaultRateInterestsTransformer extends Transformer
{
    /**
     * @var  array
     */
    protected $defaultIncludes = [

    ];

    /**
     * @var  array
     */
    protected $availableIncludes = [

    ];

    /**
     * @param Post $entity
     *
     * @return array
     */
    public function transform(InterestBreakDown $entity)
    {
        $response = [
            'object' => 'InterestBreakDowns',
//            'id' => $entity->getHashedKey(),
            'daily_rate' => $entity->daily_rate,
            'days' => $entity->days,
            'balance' => $entity->balance,
            'rate' => $entity->rate,
            'total' => $entity->total,
            'start' => $entity->start,
            'end' => $entity->end,
            'refunded' => $entity->refunded,
            'hide' => $entity->hide,
            'hide_transaction' => $entity->hideTransaction,

        ];

        $response = $this->ifAdmin([
            'real_id'    => $entity->id,
            // 'deleted_at' => $entity->deleted_at,
        ], $response);

        return $response;
    }
}
