<?php

namespace App\Containers\Payment\UI\API\Transformers;

use App\Containers\Payment\Models\InterestBreakDown;
use App\Ship\Parents\Transformers\Transformer;

class InterestBreakDownTransformer extends Transformer
{
    /**
     * @var  array
     */
    protected $defaultIncludes = [

    ];

    /**
     * @var  array
     */
    protected $availableIncludes = [

    ];

    /**
     * @param Post $entity
     *
     * @return array
     */
    public function transform(InterestBreakDown $entity)
    {
        $response = [
            'object' => 'InterestBreakDonw',
//            'id' => $entity->getHashedKey(),
            'daily_rate' => round($entity->daily_rate, 2),
            'days' => $entity->days,
            'balance' => round($entity->balance, 2),
            'rate' => round($entity->rate, 2),
            'total' => round($entity->total, 2),
            'start_at' => $entity->start_at,
            'end_at' => $entity->end_at,
        ];

        $response = $this->ifAdmin([
            'real_id'    => $entity->id,
            // 'deleted_at' => $entity->deleted_at,
        ], $response);

        return $response;
    }
}
