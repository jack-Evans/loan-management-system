<?php

namespace App\Containers\Payment\UI\API\Transformers;

use App\Containers\Payment\Models\Payment;
use App\Ship\Parents\Transformers\Transformer;

class PaymentTransformer extends Transformer
{
    /**
     * @var  array
     */
    protected $defaultIncludes = [

    ];

    /**
     * @var  array
     */
    protected $availableIncludes = [

    ];

    /**
     * @param Payment $entity
     *
     * @return array
     */
    public function transform(Payment $entity)
    {
        $response = [
            'object' => 'Payment',
            'id'            => $entity->getHashedKey(),
            'amount'        => $entity->amount,
            'paid_at'       => $entity->paid_at,
            'is_capital_reduction'       => $entity->is_capital_reduction,
            'journal'       => $entity->journal,
            'created_at'    => $entity->created_at,
            'updated_at'    => $entity->updated_at,

        ];

        $response = $this->ifAdmin([
            'real_id'    => $entity->id,
            // 'deleted_at' => $entity->deleted_at,
        ], $response);

        return $response;
    }
}
