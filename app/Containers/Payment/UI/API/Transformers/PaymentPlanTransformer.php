<?php

namespace App\Containers\Payment\UI\API\Transformers;

use App\Containers\Payment\Models\PaymentPlan;
use App\Ship\Parents\Transformers\Transformer;

class PaymentPlanTransformer extends Transformer
{
    /**
     * @var  array
     */
    protected $defaultIncludes = [

    ];

    /**
     * @var  array
     */
    protected $availableIncludes = [

    ];

    /**
     * @param PaymentPlan $entity
     *
     * @return array
     */
    public function transform(PaymentPlan $entity)
    {
        $response = [
            'object' => 'PaymentPlan',
            'id'            => $entity->getHashedKey(),
            'plan_version'  => $entity->plan_version,
            'amount'        => $entity->amount,
            'amount_paid'   => $entity->amount_paid,
            'amount_pending'=> $entity->amount_pending,
            'expected_balance'=> $entity->expected_balance,
            'status'        => $entity->status,
            'paid_at'       => $entity->paid_at,
            'pay_before'    => $entity->pay_before,
            'type'          => $entity->type,
            'loan_id'       => $entity->loan_id,
            'payment_break_down'       => $entity->payment_break_down,
            'created_at'    => $entity->created_at,
            'updated_at'    => $entity->updated_at,

        ];

        $response = $this->ifAdmin([
            'real_id'    => $entity->id,
            // 'deleted_at' => $entity->deleted_at,
        ], $response);

        return $response;
    }
}
