<?php

namespace App\Containers\Payment\UI\API\Requests;

use App\Ship\Parents\Requests\Request;

/**
 * Class CreatePaymentRequest.
 */
class CreateRefundRequest extends Request
{

    /**
     * The assigned Transporter for this Request
     *
     * @var string
     */
    protected $transporter = \App\Containers\Payment\Data\Transporters\CreateRefundTransporter::class;

    /**
     * Define which Roles and/or Permissions has access to this request.
     *
     * @var  array
     */
    protected $access = [
        'permissions' => '',
        'roles'       => '',
    ];

    /**
     * Id's that needs decoding before applying the validation rules.
     *
     * @var  array
     */
    protected $decode = [
        'loanId',
        'chargeId',
        'otherChargeId',
    ];

    /**
     * Defining the URL parameters (e.g, `/user/{id}`) allows applying
     * validation rules on them and allows accessing them like request data.
     *
     * @var  array
     */
    protected $urlParameters = [
        // 'loanId',
    ];

    /**
     * @return  array
     */
    public function rules()
    {
        return [
            'amount'        => 'required|regex:/^\d*(\.\d{1,2})?$/',
            // 'amount'        => 'required|integer',
            'refundAt'      => 'required|date_format:Y-m-d',
            'loanId'        => 'required|integer|exists:loans,id',
            'description'   => 'required|string|min:3',
            'chargeId'      => 'integer|exists:charges,id',
            'otherChargeId' => 'integer|exists:other_charges,id',
            'assetType'     => 'string|in:debit_filler,credit_filler',
        ];
    }

    /**
     * @return  bool
     */
    public function authorize()
    {
        return $this->check([
            'hasAccess',
        ]);
    }
}
