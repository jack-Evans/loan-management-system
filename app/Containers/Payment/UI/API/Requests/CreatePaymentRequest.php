<?php

namespace App\Containers\Payment\UI\API\Requests;

use App\Ship\Parents\Requests\Request;

/**
 * Class CreatePaymentRequest.
 */
class CreatePaymentRequest extends Request
{

    /**
     * The assigned Transporter for this Request
     *
     * @var string
     */
    protected $transporter = \App\Containers\Payment\Data\Transporters\CreatePaymentTransporter::class;

    /**
     * Define which Roles and/or Permissions has access to this request.
     *
     * @var  array
     */
    protected $access = [
        'permissions' => '',
        'roles'       => '',
    ];

    /**
     * Id's that needs decoding before applying the validation rules.
     *
     * @var  array
     */
    protected $decode = [
        'loanId',
    ];

    /**
     * Defining the URL parameters (e.g, `/user/{id}`) allows applying
     * validation rules on them and allows accessing them like request data.
     *
     * @var  array
     */
    protected $urlParameters = [
        // 'loanId',
    ];

    /**
     * @return  array
     */
    public function rules()
    {
        return [
            'amount'    => 'required|regex:/^\d*(\.\d{1,2})?$/',
            'paidAt'    => 'required|date_format:Y-m-d',
            'loanId'   => 'required|integer|exists:loans,id',
            'description' => 'required|string|min:3',
            'isCapitalReduction' => 'required|in:0,1',
        ];
    }

    /**
     * @return  bool
     */
    public function authorize()
    {
        return $this->check([
            'hasAccess',
        ]);
    }
}
