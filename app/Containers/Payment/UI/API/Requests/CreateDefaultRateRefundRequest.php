<?php

namespace App\Containers\Payment\UI\API\Requests;

use App\Ship\Parents\Requests\Request;

/**
 * Class CreateDefaultRateRefundRequest.
 */
class CreateDefaultRateRefundRequest extends Request
{

    /**
     * The assigned Transporter for this Request
     *
     * @var string
     */
    // protected $transporter = \App\Ship\Transporters\DataTransporter::class;

    /**
     * Define which Roles and/or Permissions has access to this request.
     *
     * @var  array
     */
    protected $access = [
        'permissions' => '',
        'roles'       => '',
    ];

    /**
     * Id's that needs decoding before applying the validation rules.
     *
     * @var  array
     */
    protected $decode = [
        'loanId',
    ];

    /**
     * Defining the URL parameters (e.g, `/user/{id}`) allows applying
     * validation rules on them and allows accessing them like request data.
     *
     * @var  array
     */
    protected $urlParameters = [
        // 'id',
    ];

    /**
     * @return  array
     */
    public function rules()
    {
        return [
            'loanId'        => 'required|integer|exists:loans,id',
            'startDate'     => 'required|date_format:Y-m-d',
            'endDate'       => 'required|date_format:Y-m-d',
            'refundDate'    => 'date_format:Y-m-d',
            'days'          => 'required|integer',
            // 'rate'          => 'required|regex:/^\d*(\.\d{1,2})?$/',
            // 'dailyRate'     => 'required|regex:/^\d*(\.\d{1,2})?$/',
            // 'total'         => 'required|regex:/^\d*(\.\d{1,2})?$/',
            'balance'       => 'required|regex:/^\d*(\.\d{1,4})?$/',
            // 'description'   => 'required|string|min:3'
        ];
    }

    /**
     * @return  bool
     */
    public function authorize()
    {
        return $this->check([
            'hasAccess',
        ]);
    }
}
