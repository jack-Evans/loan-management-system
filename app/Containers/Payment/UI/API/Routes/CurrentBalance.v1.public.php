<?php

/**
 * @apiGroup           Current Balance
 * @apiName            currentBalance
 *
 * @api                {GET} /v1/getallstatements Endpoint title here..
 * @apiDescription     Endpoint description here..
 *
 * @apiVersion         1.0.0
 * @apiPermission      none
 *
 * @apiParam           {String}  parameters here..
 *
 * @apiSuccessExample  {json}  Success-Response:
 * HTTP/1.1 200 OK
{
  // Insert the response of the request here...
}
 */

/** @var Route $router */
$router->get('currentbalance/{loanid}', [
    'as' => 'api_payment_get_current_balance',
    'uses'  => 'Controller@currentBalance',
    'middleware' => [
      'auth:api',
    ],
]);
