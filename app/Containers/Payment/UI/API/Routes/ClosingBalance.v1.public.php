<?php

/**
 * @apiGroup           Closing Balance
 * @apiName            closingBalance
 *
 * @api                {GET} /v1/closingbalance Endpoint title here..
 * @apiDescription     Endpoint description here..
 *
 * @apiVersion         1.0.0
 * @apiPermission      none
 *
 * @apiParam           {String}  parameters here..
 *
 * @apiSuccessExample  {json}  Success-Response:
 * HTTP/1.1 200 OK
{
  // Insert the response of the request here...
}
 */

/** @var Route $router */
$router->get('closingbalance/{loanid}', [
    'as' => 'api_payment_get_closing_balance',
    'uses'  => 'Controller@closingBalance',
    'middleware' => [
      'auth:api',
    ],
]);
