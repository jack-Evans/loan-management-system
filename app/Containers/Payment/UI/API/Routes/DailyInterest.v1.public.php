<?php

/**
 * @apiGroup           Payment
 * @apiName            CreateDailyInterest
 *
 * @api                {POST} /v1/dailyinterest Endpoint title here..
 * @apiDescription     Endpoint description here..
 *
 * @apiVersion         1.0.0
 * @apiPermission      none
 *
 * @apiParam           {String}  parameters here..
 *
 * @apiSuccessExample  {json}  Success-Response:
 * HTTP/1.1 200 OK
{
  // Insert the response of the request here...
}
 */

/** @var Route $router */
$router->post('dailyinterest/{id}', [
    'as' => 'api_payment_create_daily_interest',
    'uses'  => 'Controller@createDailyInterest',
    'middleware' => [
      'auth:api',
    ],
]);
