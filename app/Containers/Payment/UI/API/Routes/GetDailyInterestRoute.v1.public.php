<?php

/**
 * @apiGroup           Payment
 * @apiName            getDailyInterest
 *
 * @api                {GET} /v1/dailyinterest/:$id Endpoint title here..
 * @apiDescription     Endpoint description here..
 *
 * @apiVersion         1.0.0
 * @apiPermission      none
 *
 * @apiParam           {String}  parameters here..
 *
 * @apiSuccessExample  {json}  Success-Response:
 * HTTP/1.1 200 OK
{
  // Insert the response of the request here...
}
 */

/** @var Route $router */
$router->get('dailyinterest/{id}', [
    'as' => 'api_payment_get_daily_interest',
    'uses'  => 'Controller@getDailyInterest',
    'middleware' => [
      'auth:api',
    ],
]);
