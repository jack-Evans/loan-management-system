<?php

/**
 * @apiGroup           Statement
 * @apiName            getAllStatements
 *
 * @api                {GET} /v1/statements Endpoint title here..
 * @apiDescription     Endpoint description here..
 *
 * @apiVersion         1.0.0
 * @apiPermission      none
 *
 * @apiParam           {String}  parameters here..
 *
 * @apiSuccessExample  {json}  Success-Response:
 * HTTP/1.1 200 OK
{
  // Insert the response of the request here...
}
 */

/** @var Route $router */
$router->post('monthstatementspdf', [
    'as' => 'api_statement_month_statements_pdf',
    'uses'  => 'Controller@monthStatementsPdf',
    'middleware' => [
      'auth:api',
    ],
]);
