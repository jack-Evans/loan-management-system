<?php

/**
 * @apiGroup           Payment
 * @apiName            deletePayment
 *
 * @api                {DELETE} /v1/payments/:id Endpoint title here..
 * @apiDescription     Endpoint description here..
 *
 * @apiVersion         1.0.0
 * @apiPermission      none
 *
 * @apiParam           {String}  parameters here..
 *
 * @apiSuccessExample  {json}  Success-Response:
 * HTTP/1.1 200 OK
{
  // Insert the response of the request here...
}
 */

/** @var Route $router */
$router->delete('payments/{id}', [
    'as' => 'api_payment_delete_payment',
    'uses'  => 'Controller@deletePayment',
    'middleware' => [
      'auth:api',
    ],
]);
