<?php

/**
 * @apiGroup           Statements
 * @apiName            getAllStatements
 *
 * @api                {GET} /v1/getallstatements Endpoint title here..
 * @apiDescription     Endpoint description here..
 *
 * @apiVersion         1.0.0
 * @apiPermission      none
 *
 * @apiParam           {String}  parameters here..
 *
 * @apiSuccessExample  {json}  Success-Response:
 * HTTP/1.1 200 OK
{
  // Insert the response of the request here...
}
 */

/** @var Route $router */
$router->get('getallstatements/{loanid}', [
    'as' => 'api_payment_get_all_statements',
    'uses'  => 'Controller@getAllStatements',
    'middleware' => [
      'auth:api',
    ],
]);
