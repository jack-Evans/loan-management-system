<?php

/**
 * @apiGroup           Payment
 * @apiName            processPayment
 *
 * @api                {POST} /v1/processpayment/:id Endpoint title here..
 * @apiDescription     Endpoint description here..
 *
 * @apiVersion         1.0.0
 * @apiPermission      none
 *
 * @apiParam           {String}  parameters here..
 *
 * @apiSuccessExample  {json}  Success-Response:
 * HTTP/1.1 200 OK
{
  // Insert the response of the request here...
}
 */

/** @var Route $router */
$router->post('processpayment/{id}', [
    'as' => 'api_payment_process_payment',
    'uses'  => 'Controller@processPayment',
    'middleware' => [
      'auth:api',
    ],
]);
