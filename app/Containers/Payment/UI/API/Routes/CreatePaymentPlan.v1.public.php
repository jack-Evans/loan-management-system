<?php

/**
 * @apiGroup           Payment
 * @apiName            calculatePayment
 *
 * @api                {GET} /v1/calculatepayment/:id Endpoint title here..
 * @apiDescription     Endpoint description here..
 *
 * @apiVersion         1.0.0
 * @apiPermission      none
 *
 * @apiParam           {String}  parameters here..
 *
 * @apiSuccessExample  {json}  Success-Response:
 * HTTP/1.1 200 OK
{
  // Insert the response of the request here...
}
 */

/** @var Route $router */
$router->post('createpaymentplan', [
    'as' => 'api_payment_create_payment_plan',
    'uses'  => 'Controller@createPaymentPlan',
    'middleware' => [
      'auth:api',
    ],
]);
