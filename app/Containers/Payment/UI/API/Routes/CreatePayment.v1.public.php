<?php

/**
 * @apiGroup           Payment
 * @apiName            createPayment
 *
 * @api                {POST} /v1/payments Endpoint title here..
 * @apiDescription     Endpoint description here..
 *
 * @apiVersion         1.0.0
 * @apiPermission      none
 *
 * @apiParam           {String}  parameters here..
 *
 * @apiSuccessExample  {json}  Success-Response:
 * HTTP/1.1 200 OK
{
  // Insert the response of the request here...
}
 */

/** @var Route $router */
$router->post('payments', [
    'as' => 'api_payment_create_payment',
    'uses'  => 'Controller@createPayment',
    'middleware' => [
      'auth:api',
    ],
]);
