<?php

/**
 * @apiGroup           Payment
 * @apiName            getAllPayments
 *
 * @api                {GET} /v1/payments Endpoint title here..
 * @apiDescription     Endpoint description here..
 *
 * @apiVersion         1.0.0
 * @apiPermission      none
 *
 * @apiParam           {String}  parameters here..
 *
 * @apiSuccessExample  {json}  Success-Response:
 * HTTP/1.1 200 OK
{
  // Insert the response of the request here...
}
 */

/** @var Route $router */
$router->get('payments', [
    'as' => 'api_payment_get_all_payments',
    'uses'  => 'Controller@getAllPayments',
    'middleware' => [
      'auth:api',
    ],
]);
