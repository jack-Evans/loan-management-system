<?php

/**
 * @apiGroup           Payment
 * @apiName            findPaymentById
 *
 * @api                {GET} /v1/payments/:id Endpoint title here..
 * @apiDescription     Endpoint description here..
 *
 * @apiVersion         1.0.0
 * @apiPermission      none
 *
 * @apiParam           {String}  parameters here..
 *
 * @apiSuccessExample  {json}  Success-Response:
 * HTTP/1.1 200 OK
{
  // Insert the response of the request here...
}
 */

/** @var Route $router */
$router->get('payments/{id}', [
    'as' => 'api_payment_find_payment_by_id',
    'uses'  => 'Controller@findPaymentById',
    'middleware' => [
      'auth:api',
    ],
]);
