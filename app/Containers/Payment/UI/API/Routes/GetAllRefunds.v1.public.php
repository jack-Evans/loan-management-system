<?php

/**
 * @apiGroup           GetAllRefunds
 * @apiName            refunds
 *
 * @api                {GET} /v1/dailyinterest/:$id Endpoint title here..
 * @apiDescription     Endpoint description here..
 *
 * @apiVersion         1.0.0
 * @apiPermission      none
 *
 * @apiParam           {String}  parameters here..
 *
 * @apiSuccessExample  {json}  Success-Response:
 * HTTP/1.1 200 OK
{
  // Insert the response of the request here...
}
 */

/** @var Route $router */
$router->get('refunds/{loanid}', [
    'as' => 'api_get_all_refunds',
    'uses'  => 'Controller@getAllRefunds',
    'middleware' => [
      'auth:api',
    ],
]);
