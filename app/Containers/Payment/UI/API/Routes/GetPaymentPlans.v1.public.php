<?php

/**
 * @apiGroup           GetPaymentPlan
 * @apiName            getpaymentplan
 *
 * @api                {GET} /v1/dailyinterest/:$id Endpoint title here..
 * @apiDescription     Endpoint description here..
 *
 * @apiVersion         1.0.0
 * @apiPermission      none
 *
 * @apiParam           {String}  parameters here..
 *
 * @apiSuccessExample  {json}  Success-Response:
 * HTTP/1.1 200 OK
{
  // Insert the response of the request here...
}
 */

/** @var Route $router */
$router->get('getpaymentplan/{loanid}', [
    'as' => 'api_payment_get_payment_plan',
    'uses'  => 'Controller@getPaymentPlan',
    'middleware' => [
      'auth:api',
    ],
]);
