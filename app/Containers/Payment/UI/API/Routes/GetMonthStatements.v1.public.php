<?php

/**
 * @apiGroup           Statement
 * @apiName            getAllStatements
 *
 * @api                {GET} /v1/statements Endpoint title here..
 * @apiDescription     Endpoint description here..
 *
 * @apiVersion         1.0.0
 * @apiPermission      none
 *
 * @apiParam           {String}  parameters here..
 *
 * @apiSuccessExample  {json}  Success-Response:
 * HTTP/1.1 200 OK
{
  // Insert the response of the request here...
}
 */

/** @var Route $router */
$router->get('getmonthstatements/{loanid}', [
    'as' => 'api_statement_get_month_statements',
    'uses'  => 'Controller@getMonthStatements',
    'middleware' => [
      'auth:api',
    ],
]);
