<?php

/**
 * @apiGroup           Refund
 * @apiName            createRefund
 *
 * @api                {POST} /v1/refunds Endpoint title here..
 * @apiDescription     Endpoint description here..
 *
 * @apiVersion         1.0.0
 * @apiPermission      none
 *
 * @apiParam           {String}  parameters here..
 *
 * @apiSuccessExample  {json}  Success-Response:
 * HTTP/1.1 200 OK
{
  // Insert the response of the request here...
}
 */

/** @var Route $router */
$router->post('refunds', [
    'as' => 'api_payment_create_refund',
    'uses'  => 'Controller@createRefund',
    'middleware' => [
      'auth:api',
    ],
]);
