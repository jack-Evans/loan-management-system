<?php

/**
 * @apiGroup           Payment
 * @apiName            calculatePayment
 *
 * @api                {GET} /v1/calculatepayment/:id Endpoint title here..
 * @apiDescription     Endpoint description here..
 *
 * @apiVersion         1.0.0
 * @apiPermission      none
 *
 * @apiParam           {String}  parameters here..
 *
 * @apiSuccessExample  {json}  Success-Response:
 * HTTP/1.1 200 OK
{
  // Insert the response of the request here...
}
 */

/** @var Route $router */
$router->get('paymentdetails/{id}', [
    'as' => 'api_payment_calculate_payment',
    'uses'  => 'Controller@paymentDetails',
    'middleware' => [
      'auth:api',
    ],
]);
