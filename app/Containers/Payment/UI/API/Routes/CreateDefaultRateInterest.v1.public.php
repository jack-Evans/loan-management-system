<?php

/**
 * @apiGroup           DefaultRateInterest
 * @apiName            createdefaultraterefund
 *
 * @api                {POST} /v1/createdefaultraterefund/:id Endpoint title here..
 * @apiDescription     Endpoint description here..
 *
 * @apiVersion         1.0.0
 * @apiPermission      none
 *
 * @apiParam           {String}  parameters here..
 *
 * @apiSuccessExample  {json}  Success-Response:
 * HTTP/1.1 200 OK
{
  // Insert the response of the request here...
}
 */

/** @var Route $router */
$router->post('createdefaultraterefund', [
    'as' => 'api_create_default_rate_refund',
    'uses'  => 'Controller@createDefaultRateRefund',
    'middleware' => [
      'auth:api',
    ],
]);
