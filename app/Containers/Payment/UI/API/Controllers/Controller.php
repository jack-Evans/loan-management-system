<?php

namespace App\Containers\Payment\UI\API\Controllers;

use App\Containers\Payment\UI\API\Requests\CreatePaymentRequest;
use App\Containers\Payment\UI\API\Requests\DeletePaymentRequest;
use App\Containers\Payment\UI\API\Requests\GetAllPaymentsRequest;
use App\Containers\Payment\UI\API\Requests\FindPaymentByIdRequest;
use App\Containers\Payment\UI\API\Requests\UpdatePaymentRequest;
use App\Containers\Payment\UI\API\Requests\CalculatePaymentRequest;
use App\Containers\Payment\UI\API\Requests\CreateDailyPaymentRequest;
use App\Containers\Payment\UI\API\Transformers\PaymentTransformer;
use App\Ship\Parents\Controllers\ApiController;
use Apiato\Core\Foundation\Facades\Apiato;
use App\Containers\Payment\UI\API\Requests\PaymentDetailsRequest;
use App\Containers\Payment\UI\API\Requests\GetAllStatementsRequest;
use App\Containers\Payment\UI\API\Requests\GetPaymentPlanRequest;
use App\Containers\Payment\UI\API\Transformers\PaymentDetailsTransformer;
use App\Containers\Payment\UI\API\Transformers\PaymentPlanTransformer;
use App\Containers\Payment\UI\API\Transformers\FinalDueAmountTransformer;
use App\Containers\Payment\UI\API\Transformers\StatementTransformer;
use App\Containers\Payment\UI\API\Transformers\MonthStatementTransformer;
use App\Containers\Payment\UI\API\Transformers\RefundTransformer;
use App\Containers\Payment\UI\API\Transformers\DefaultRateInterestsTransformer;
use App\Containers\Payment\UI\API\Transformers\DefaultRateRefundTransformer;
use App\Containers\Payment\UI\API\Requests\CreateRefundRequest;
use App\Containers\Payment\UI\API\Requests\GetRefundRequest;
use App\Containers\Payment\UI\API\Requests\CreatePaymentPlanRequest;
use App\Containers\Payment\UI\API\Requests\CreateDefaultRateRefundRequest;
use App\Containers\Payment\UI\API\Requests\UpdateTransactionRequest;
use App\Containers\Payment\UI\API\Requests\MonthStatementsPdfRequest;
/**
 * Class Controller
 *
 * @package App\Containers\Payment\UI\API\Controllers
 */
class Controller extends ApiController
{
    /**
     * @param CreatePaymentRequest $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function createPayment(CreatePaymentRequest $request)
    {
        $payment = Apiato::call('Payment@CreatePaymentAction', [$request]);
        return $this->transform($payment, PaymentTransformer::class);
    }

    /**
     * @param FindPaymentByIdRequest $request
     * @return array
     */
    public function findPaymentById(FindPaymentByIdRequest $request)
    {
        $payment = Apiato::call('Payment@FindPaymentByIdAction', [$request]);

        return $this->transform($payment, PaymentTransformer::class);
    }

    /**
     * @param GetAllPaymentsRequest $request
     * @return array
     */
    public function getAllPayments(GetAllPaymentsRequest $request)
    {
        $payments = Apiato::call('Payment@GetAllPaymentsAction', [$request]);

        return $this->transform($payments, PaymentTransformer::class);
    }

    /**
     * @param GetAllStatements $request
     * @return array
     */
    public function getAllStatements(GetAllStatementsRequest $request)
    {
        $statements = Apiato::call('Payment@GetAllStatementsAction', [$request]);

        return $this->transform($statements, StatementTransformer::class);
    }

    /**
     * @param getMonthStatements $request
     * @return array
     */
    public function getMonthStatements(GetAllStatementsRequest $request)
    {
        $monthStatements = Apiato::call('Payment@GetMonthStatementsAction', [$request]);        
        // dd($monthStatements);
        return $this->transform($monthStatements, MonthStatementTransformer::class);
    }

    /**
     * @param GetcurrentBalance $request
     * @return array
     */
    public function currentBalance(GetAllStatementsRequest $request)
    {
        $currentBalance = Apiato::call('Payment@CurrentBalanceAction', [$request]);

        return $this->transform($currentBalance, PaymentPlanTransformer::class);
    }

    /**
     * @param GetClosingBalance $request
     * @return array
     */
    public function closingBalance(GetAllStatementsRequest $request)
    {
        $currentBalance = Apiato::call('Payment@ClosingBalanceAction', [$request]);

        return $this->transform($currentBalance, PaymentPlanTransformer::class);
    }

    /**
     * @param GetAllStatementsRequest $request
     * @return array
     */
    public function getDefaultRateInterests(GetAllStatementsRequest $request)
    {
        $defaultRateInterests = Apiato::call('Payment@GetDefaultRateInterestsAction', [$request]);

        return $this->transform($defaultRateInterests, DefaultRateInterestsTransformer::class);
    }


    /**
     * @param GetcurrentBalance $request
     * @return array
     */
    public function createDefaultRateRefund(CreateDefaultRateRefundRequest $request)
    {
        $defaultRateRefund = Apiato::call('Payment@CreateDefaultRateInterestAction', [$request]);

        return $this->transform($defaultRateRefund, DefaultRateRefundTransformer::class);
    }

    /**
     * @param UpdatePaymentRequest $request
     * @return array
     */
    public function updatePayment(UpdatePaymentRequest $request)
    {
        $payment = Apiato::call('Payment@UpdatePaymentAction', [$request]);

        return $this->transform($payment, PaymentTransformer::class);
    }

    /**
     * @param DeletePaymentRequest $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function deletePayment(DeletePaymentRequest $request)
    {
        Apiato::call('Payment@DeletePaymentAction', [$request]);

        return $this->noContent();
    }

    /**
     * @param CalculatePaymentRequest $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function calculatePayment(CalculatePaymentRequest $request)
    {
        return  Apiato::call('Payment@CalculatePaymentAction', [$request]);
    }

    /**
     * @param CreateDailyPaymentRequest $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function createDailyInterest(CreateDailyPaymentRequest $request)
    {
        return  Apiato::call('Payment@CreateDailyInterestAction', [$request]);
    }
    
    public function paymentDetails(PaymentDetailsRequest $request)
    {
        $payments = Apiato::call('Payment@PaymentDetailsAction', [$request]);
        return $this->transform($payments, PaymentDetailsTransformer::class);

    }
    
    /**
     * @param CreateDailyPaymentRequest $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function getPaymentPlan(GetPaymentPlanRequest $request)
    {
        $paymentPlan = Apiato::call('Payment@GetPaymentPlanAction', [$request]);
        
        return $this->transform($paymentPlan, PaymentPlanTransformer::class);
    }

    /**
     * @param CreateDailyPaymentRequest $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function deletePaymentPlan(GetPaymentPlanRequest $request)
    {
        $paymentPlan = Apiato::call('Payment@DeletePaymentPlanAction', [$request]);
        return [];
    }

    /**
     * @param CreateDailyPaymentRequest $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function getFinalDueAmount(GetPaymentPlanRequest $request)
    {
        $finalDueAmount = Apiato::call('Payment@GetFinalDueAmountAction', [$request]);
        
        return $this->transform($finalDueAmount, FinalDueAmountTransformer::class);
    }

    /**
     * @param CreateDailyPaymentRequest $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function getDailyInterest(CreateDailyPaymentRequest $request)
    {
        return  Apiato::call('Payment@CalculayeDailyInterestAction', [$request]);

    }

    /**
     * @param CreateRefundRequest $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function createRefund(CreateRefundRequest $request)
    {
        $refund = Apiato::call('Payment@CreateRefundAction', [$request]);
        // return $this->transform($refund, RefundTransformer::class);
        return $refund;
    }

    /**
     * @param GetRefundRequest $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function getAllRefunds(GetRefundRequest $request)
    {
        $refunds = Apiato::call('Payment@GetAllRefundsAction', [$request]);
        return $this->transform($refunds, RefundTransformer::class);
    }
    /**
     * @param GetRefundRequest $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function createPaymentPlan(CreatePaymentPlanRequest $request)
    {
        return $paymentPlan = Apiato::call('Payment@CreateInterestForPaymentPlanAction', [$request]);
        // return $this->transform($paymentPlan, RefundTransformer::class);
    }
    /**
     * @param GetRefundRequest $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function updateTransaction(UpdateTransactionRequest $request)
    {
        $journal = Apiato::call('Payment@UpdateTransactionAction', [$request]);
        return $journal;
    }

    /**
     * @param GetRefundRequest $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function monthStatementsPdf(MonthStatementsPdfRequest $request)
    {
        \App\Jobs\ProcessMonthStatements::dispatch($request->all());
        // $pdfs = Apiato::call('Payment@MonthStatementsAction', [$request]);
        // return $pdfs;
        return $this->noContent();
    }
}
