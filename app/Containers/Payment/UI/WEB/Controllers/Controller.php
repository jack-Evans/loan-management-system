<?php

namespace App\Containers\Payment\UI\WEB\Controllers;

use App\Containers\Payment\UI\WEB\Requests\CreatePaymentRequest;
use App\Containers\Payment\UI\WEB\Requests\DeletePaymentRequest;
use App\Containers\Payment\UI\WEB\Requests\GetAllPaymentsRequest;
use App\Containers\Payment\UI\WEB\Requests\FindPaymentByIdRequest;
use App\Containers\Payment\UI\WEB\Requests\UpdatePaymentRequest;
use App\Containers\Payment\UI\WEB\Requests\StorePaymentRequest;
use App\Containers\Payment\UI\WEB\Requests\EditPaymentRequest;
use App\Ship\Parents\Controllers\WebController;
use Apiato\Core\Foundation\Facades\Apiato;

/**
 * Class Controller
 *
 * @package App\Containers\Payment\UI\WEB\Controllers
 */
class Controller extends WebController
{
    /**
     * Show all entities
     *
     * @param GetAllPaymentsRequest $request
     */
    public function index(GetAllPaymentsRequest $request)
    {
        $payments = Apiato::call('Payment@GetAllPaymentsAction', [$request]);

        // ..
    }

    /**
     * Show one entity
     *
     * @param FindPaymentByIdRequest $request
     */
    public function show(FindPaymentByIdRequest $request)
    {
        $payment = Apiato::call('Payment@FindPaymentByIdAction', [$request]);

        // ..
    }

    /**
     * Create entity (show UI)
     *
     * @param CreatePaymentRequest $request
     */
    public function create(CreatePaymentRequest $request)
    {
        // ..
    }

    /**
     * Add a new entity
     *
     * @param StorePaymentRequest $request
     */
    public function store(StorePaymentRequest $request)
    {
        $url = config('token-container.WEB_API_URL').'payments';        
        $paymentRequest = [
            'amount' => $request->amount,
            'paidAt' => $request->paidAt,
            'loanId' => $request->loanId,
    	];
        
        try{
            $response = __post($url, $paymentRequest);
        } catch (\Exception $e) {
            return __exception($e);
        }

        return redirect()->back();
    }

    /**
     * Edit entity (show UI)
     *
     * @param EditPaymentRequest $request
     */
    public function edit(EditPaymentRequest $request)
    {
        $payment = Apiato::call('Payment@GetPaymentByIdAction', [$request]);

        // ..
    }

    /**
     * Update a given entity
     *
     * @param UpdatePaymentRequest $request
     */
    public function update(UpdatePaymentRequest $request)
    {
        $payment = Apiato::call('Payment@UpdatePaymentAction', [$request]);

        // ..
    }

    /**
     * Delete a given entity
     *
     * @param DeletePaymentRequest $request
     */
    public function delete(DeletePaymentRequest $request)
    {
         $result = Apiato::call('Payment@DeletePaymentAction', [$request]);

         // ..
    }
}
