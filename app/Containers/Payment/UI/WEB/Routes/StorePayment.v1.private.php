<?php

/** @var Route $router */
$router->post('payments/store', [
    'as' => 'web_payment_store',
    'uses'  => 'Controller@store',
    'middleware' => [
      'auth:web',
    ],
]);
