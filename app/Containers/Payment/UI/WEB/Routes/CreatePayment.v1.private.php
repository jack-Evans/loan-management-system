<?php

/** @var Route $router */
$router->get('payments/create', [
    'as' => 'web_payment_create',
    'uses'  => 'Controller@create',
    'middleware' => [
      'auth:web',
    ],
]);
