<?php

/** @var Route $router */
$router->get('payments/{id}', [
    'as' => 'web_payment_show',
    'uses'  => 'Controller@show',
    'middleware' => [
      'auth:web',
    ],
]);
