<?php

/** @var Route $router */
$router->delete('payments/{id}', [
    'as' => 'web_payment_delete',
    'uses'  => 'Controller@delete',
    'middleware' => [
      'auth:web',
    ],
]);
