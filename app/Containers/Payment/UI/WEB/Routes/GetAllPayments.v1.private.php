<?php

/** @var Route $router */
$router->get('payments', [
    'as' => 'web_payment_index',
    'uses'  => 'Controller@index',
    'middleware' => [
      'auth:web',
    ],
]);
