<?php

/** @var Route $router */
$router->patch('payments/{id}', [
    'as' => 'web_payment_update',
    'uses'  => 'Controller@update',
    'middleware' => [
      'auth:web',
    ],
]);
