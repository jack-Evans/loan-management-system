<?php

/** @var Route $router */
$router->get('payments/{id}/edit', [
    'as' => 'web_payment_edit',
    'uses'  => 'Controller@edit',
    'middleware' => [
      'auth:web',
    ],
]);
