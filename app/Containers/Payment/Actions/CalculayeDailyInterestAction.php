<?php

namespace App\Containers\Payment\Actions;

use App\Ship\Parents\Actions\Action;
use Apiato\Core\Foundation\Facades\Apiato;
use App\Containers\Loan\Models\Loan;
use App\Ship\Parents\Requests\Request;

class CalculayeDailyInterestAction extends Action
{
    public function run(Request $request, Loan $loan = null)
    {
        if(!$loan) {
        	$loan = Apiato::call('Loan@FindLoanByIdTask', [$request->id]);
        }

        if(!$loan->issue_at) {
    		throw new \App\Containers\Loan\Exceptions\LoanNotIssuedException();
        }

        $date = \Carbon\Carbon::Parse($request->date);
        $date->endOfMonth();

        $isChargeAlreadyCreated = Apiato::call('Payment@CheckDailyInterestAlreadyCreatedTask', [$loan, $date]);

    	if($isChargeAlreadyCreated) {
    		throw new \App\Containers\Payment\Exceptions\DailyInterestAlreadyCreatedException();
    	}

        // difference between start of loan month and end of current supplied month
        $curentDateDiffFromLoan             = $loan->issue_at->startofMonth()->diffInMonths($date, false);
        $currentDateDiffFromLastInterest    = null;

        if($curentDateDiffFromLoan <= 0) {
            throw new \App\Containers\Payment\Exceptions\HTTPPreConditionFailedException('Issue Loan and provided date should have one month difference');
        }

        $firstMonthInterestStatus = Apiato::call('Payment@CheckFirstMonthInterestTask', [$loan, $date]);
        $firstMonthInterest = null;

        $lastInterest = Apiato::call('Payment@GetLastMonthInterestTask', [$loan, $date]);

        $lastMonth = $date->copy()->subDay()->subMonths(1);
        $lastInterestMonth = $lastMonth->format("F")." ".$lastMonth->year;
        $lastInterestDiff = $curentDateDiffFromLoan;

        if($lastInterest) {
            // difference between start of tnx month and end of current supplied month
            // payment found so we need current diff
            $lastInterestDiff = $date->diffInMonths($lastInterest->tnx_date->startofMonth());
        }

        if($lastInterestDiff > 1) {
            throw new \App\Containers\Payment\Exceptions\HTTPPreConditionFailedException("Please First Enter Payment for $lastInterestMonth");
        }

        if(!$firstMonthInterestStatus) {
           $firstMonthInterest = Apiato::call('Payment@CalculateFirstMonthInterestTask', [$loan]);
        }


        $lastPaidPayment            = Apiato::call('Payment@GetLastPaidPaymentTask', [$loan, $date]);

        $totaldays                  = $date->daysInMonth;
        $beforePaymentDays          = 0;
        $afterPaymentDays           = 0;
        $paidAt                     = null;
        $totalDailyBeforePayment    = 0;
        
        if($lastPaidPayment) {
            $lastPaidPayment->posts;
            $afterPaymentDays = $totaldays - $lastPaidPayment->tnx_date->day;
            $paidAt           = $lastPaidPayment->tnx_date->subDay();
        }

        $beforePaymentDays          = $totaldays - $afterPaymentDays;
        $balanceMapBeforePayment    = Apiato::call('Payment@CalculateBalanceTask', [$loan, $paidAt]);
        $dailyRateBeforePayment     = calculateDailyRate($loan->interest->rate, $balanceMapBeforePayment['balance']);
        $totalDailyBeforePayment    = round($dailyRateBeforePayment * $beforePaymentDays, 2);
        
        $balanceMapAfterPayment = Apiato::call('Payment@CalculateBalanceTask', [$loan, $date]);
        $dailyRateAfterPayment  = calculateDailyRate($loan->interest->rate, $balanceMapAfterPayment['balance']);  
        $totalDailyAfterPayment = round($dailyRateAfterPayment * $afterPaymentDays, 2);

        $totalInterest = round($totalDailyAfterPayment + $totalDailyBeforePayment, 2);

        return [
        	'total' 				=> $totalInterest,
        	'totalBeforePayment'	=> $totalDailyBeforePayment,
        	'totalAfterPayment'		=> $totalDailyAfterPayment,
        	'rateBeforePayment'		=> $dailyRateAfterPayment,
        	'rateAfterPayment'		=> $dailyRateBeforePayment,
        	'balanceMapAfterPayment'  =>  $balanceMapAfterPayment,
			'balanceMapBeforePayment' => $balanceMapBeforePayment,
			'daysBeforePayment'		=>  $beforePaymentDays,
			'daysAfterPayment'		=> $afterPaymentDays,
			'firstMonthInterest'	=> $firstMonthInterest,
			'payment'				=> $lastPaidPayment,
			'date'					=> $date,
        ];
    }
}
