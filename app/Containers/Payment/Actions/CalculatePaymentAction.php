<?php

namespace App\Containers\Payment\Actions;

use App\Ship\Parents\Actions\Action;
use Apiato\Core\Foundation\Facades\Apiato;
use App\Ship\Parents\Requests\Request;

class CalculatePaymentAction extends Action
{
    public function run(Request $request)
    {
        $loan = Apiato::call('Loan@FindLoanByIdTask', [$request->id]);
        $lastPaidPayment = Apiato::call('Payment@GetLastPaidPaymentTask', [$loan]);

        // first month payment
        if(is_null($lastPaidPayment)) {
        	return Apiato::call('Payment@GetFirstPaymentTask', [$loan]); 
        }
       
    }
}
