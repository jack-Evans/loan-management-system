<?php

namespace App\Containers\Payment\Actions;

use App\Ship\Parents\Actions\Action;
use App\Ship\Parents\Requests\Request;
use Apiato\Core\Foundation\Facades\Apiato;

class UpdatePaymentAction extends Action
{
    public function run(Request $request)
    {
        $data = [];
        
        if($request->paidAt)
        	$data['paid_at'] = $request->paidAt;

        if($request->amount)
        	$data['amount'] = $request->amount;

        $payment = Apiato::call('Payment@UpdatePaymentTask', [$request->id, $data]);

        return $payment;
    }
}
