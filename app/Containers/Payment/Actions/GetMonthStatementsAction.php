<?php

namespace App\Containers\Payment\Actions;

use App\Ship\Parents\Actions\Action;
use App\Ship\Parents\Requests\Request;
use Apiato\Core\Foundation\Facades\Apiato;
use App\Containers\Payment\Models\MonthStatement;

class GetMonthStatementsAction extends Action
{
    public function run(Request $request)
    {
    	$loan = Apiato::call('Loan@FindLoanByIdTask', [$request->loanid]);
        $dueDate = $loanEndDate = addDecimalMonthsIntoDate($loan->interest->duration, $loan->issue_at)->copy()->subDay();
        // $dueDay = $dueDate->format('d');
    	$statementDate = isset($request->date ) ? $request->date : false;
        
        $renewalCharges = Apiato::call('RenewalCharge@GetRenewalChargesByLoanIdTask', [$loan->id]);
        if(count($renewalCharges))
            $dueDate = $renewalCharges->last()->renew_end_date->copy()->subDay();

        if($loan->loan_type == 'retained')
            $statements = Apiato::call('Payment@GetMonthlyRetainedLoanStatementsTask', [$loan, $statementDate])->whereIn('description', ['interest'])->where('debit', '!=', null);
        
        elseif($loan->loan_type == 'manual')
            $statements = Apiato::call('Payment@GetAllStatementsAction', [$request])->whereIn('description', ['interest'])->where('debit', '!=', null);
        else
            $statements = Apiato::call('Payment@GetStatementsTask', [$loan, $statementDate])->whereIn('description', ['interest'])->where('debit', '!=', null);

        $monthStatements = [];
        $startDate = $loan->issue_at;
        $closingDate = addDecimalMonthsIntoDate($loan->interest->duration, $startDate)->copy()->subDay();
        $previousBalance = $loan->net_loan;
        $count = 1;
    	foreach ($statements as $statement) {
            $monthStatement = new MonthStatement;
            $monthStatement->setOpeningBalanceAttribute($previousBalance);
            $monthStatement->setClosingBalanceAttribute($statement->balance);
            if($closingDate->gte($statement->created_at) && $loan->loan_type == 'retained')
               $monthStatement->setGrossLoanAttribute(isset($statement->interestBreakDown[0]->balance) ? $statement->interestBreakDown[0]->balance : 0);
            // elseif($closingDate->gte($statement->created_at) && $loan->loan_type == 'serviced')
            elseif($closingDate->gte($statement->created_at) && in_array($loan->loan_type, ['serviced', 'rolled']))
               $monthStatement->setGrossLoanAttribute($loan->interest->gross_loan);
            else
               $monthStatement->setGrossLoanAttribute($statement->balance);
            $monthStatement->setInterestChargedAttribute($statement->debit);
            if(isset($statement->interestBreakDown[0]->start)){
                $monthStatement->setStartDateAttribute($statement->interestBreakDown[0]->start->format('M d Y'));
                $monthStatement->setCurrentInterestRateAttribute(collect($statement->interestBreakDown)->last()->rate);
            }
            $monthStatement->setEndDateAttribute($statement->created_at->format('M d Y'));
            if($loan->loan_type == 'serviced'){
                $dateWithInDuration = $renewalCharges->where('renew_end_date', '>=', $statement->created_at)->where('renew_date', '<=', $statement->created_at)->first();
                $latestRenew = $dateWithInDuration;
                if(count($renewalCharges) > 0 && !is_null($dateWithInDuration)){
                    // $latestRenew = $dateWithInDuration;
                    $dueDay = $dateWithInDuration->renew_date->format('d');
                    $dueDate = $statement->created_at->copy()->addDay()->format('Y-m')."-".$dueDay;
                    $dueDate = \Carbon\Carbon::parse($dueDate);
                    // var_dump($dateWithInDuration->renew_date->format('Y m d'));
                } elseif(count($renewalCharges) > 0 && (is_null($dateWithInDuration) && !is_null($latestRenew) && $statement->created_at->gte($loanEndDate))) {
                    $dueDate = $latestRenew->renew_end_date->copy()->subDay();
                }
                else
                    $dueDate = $loan->issue_at->copy()->subDay()->addMonths($count);
            }
            $monthStatement->setInterestDueDateAttribute($dueDate->format('M d Y'));
            $monthStatement->setLoanEndDateAttribute($loanEndDate->format('M d Y'));
            
            array_push($monthStatements, $monthStatement);

            $previousBalance = $statement->balance;
            // $startDate = $startDate->copy()->endOfMonth()->addDay();
            $count++;
    	}
        return collect($monthStatements);
    }
}
