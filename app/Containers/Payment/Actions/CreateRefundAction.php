<?php

namespace App\Containers\Payment\Actions;

use App\Ship\Parents\Actions\Action;
use App\Ship\Parents\Requests\Request;
use Apiato\Core\Foundation\Facades\Apiato;
use App\Containers\Payment\Data\Transporters\CreateRefundTransporter;
use App\Containers\Payment\Exceptions\HTTPPreConditionFailedException;
use Carbon\Carbon;

class CreateRefundAction extends Action
{
    public function run(Request $request)
    {
        $refundRequest = [
            'refund_at'         => Carbon::parse($request->refundAt),
            'amount'            => $request->amount,
            'loan_id'           => $request->loanId,
            'description'       => $request->description,
            'charge_id'         => $request->chargeId,
            'other_charge_id'   => $request->otherChargeId,
            'nominalCode'       => $request->nominalCode,
        ];

        $assetType = isset($request->assetType) ? $request->assetType : 'refund';
        
        $refundAt = \Carbon\Carbon::parse($request->refundAt);
        $loan = Apiato::call('Loan@FindLoanByIdTask', [$request->loanId]);
        if($loan->issue_at->gt($refundAt)) {
            throw new HTTPPreConditionFailedException("Date should be greater than {$loan->issue_at->format("d M Y")}");
        }

        Apiato::call('Statement@VerifyStatementDateTask', [$loan->id, $refundAt]);

        // $interests = Apiato::call('Interest@GetInterestsAsPmtPlanVersionsTask', [$request->loanId])->last();
        // $paymentPlanDate = isset($interests->payment_plan_date) ? Carbon::parse($interests->payment_plan_date) : false;
        // if($paymentPlanDate && Carbon::parse($paymentPlanDate)->gt($refundAt)){
        //     throw new HTTPPreConditionFailedException("Refund date should be greater than {$paymentPlanDate->format("d M Y")}");
        // }

        Apiato::call('Payment@CheckPaymentWithCurrentBalanceTask', [$loan, $refundAt, $refundRequest['amount'], $message = 'refund', $subDays = 0]);

        \DB::beginTransaction();

        if($refundRequest['charge_id'])
        {
            Apiato::call('LoanCharge@AssignLoanChargePermissionTask', [$loan]);
            $charge = Apiato::call('LoanCharge@FindLoanChargeByIdTask', [$refundRequest['loan_id']])->where('charge_id' , $refundRequest['charge_id'])->first();
            if(!$charge)
                throw new HTTPPreConditionFailedException("You have already refunded this loan charge.");
            
            if($loan->loan_type != 'retained'){
                $chargeValue = $charge->value;
                $loanChargeRequest['value'] = $chargeValue;
                $interestRequest = [
                    'gross_loan' => $loan->interest->gross_loan - $chargeValue,
                ];
                $refundRequest['refund_at'] = $loan->issue_at;
                $interest = Apiato::call('Interest@UpdateInterestTask', [($loan->interest->id), $interestRequest]);
                Apiato::call('LoanCharge@DeleteLoanChargeTask', [ $refundRequest['loan_id'], $refundRequest['charge_id'] ]);
                $refundArr = Apiato::call('Payment@CreateRefundTask', [(new CreateRefundTransporter($refundRequest))->toArray(), $loan]);
                $refund = $refundArr['refund'];
            }

            if($loan->loan_type === 'retained'){
                $chargeDate = $loan->issue_at;
                $refund = $this->retainedLoanRefund($loan, $refundRequest, $chargeDate);
                // $refund = Apiato::call('Payment@CreateRefundTask', [(new CreateRefundTransporter($refundRequest))->toArray(), $loan]);
                // $chargeDate = $loan->issue_at;
                // Apiato::call('Payment@RetainedLoanRefundTask', [$loan, $chargeDate, $refundRequest['refund_at'], $refundRequest['amount'], $refundRequest['description'] ]);
            }
        
        }

        elseif($refundRequest['other_charge_id'])
        {
            $otherCharge = Apiato::call('OtherCharge@FindOtherChargeByIdTask', [$refundRequest['other_charge_id']]);
            if($otherCharge->refunded)
                throw new HTTPPreConditionFailedException("This charge is already refunded.");
            $chargeDate = Carbon::parse($otherCharge->charge_date);
            if($chargeDate->gt($refundAt))
                throw new HTTPPreConditionFailedException("Other charge refund date should be greater than other charge date.");

            $otherCharge =  Apiato::call('OtherCharge@UpdateOtherChargeTask', [$refundRequest['other_charge_id'], ['refunded' => 1]]);
            $assetType = 'other_charge_refund';
            if($loan->loan_type != 'retained'){
                $refundArr = Apiato::call('Payment@CreateRefundTask', [(new CreateRefundTransporter($refundRequest))->toArray(), $loan, $assetType]);
                $refund = $refundArr['refund'];
                $refundJournalPosting = $refundArr['refundJournalPosting'];
                $refunds = Apiato::call('Payment@ExtraInterestBreakDownTask', [$loan, $refund, $otherCharge->charge_date, $refundAt, (float) $refundRequest['amount']]);
                
                $postData['amount'] = $refunds['refundAmount'];
                Apiato::call('Statement@UpdatePostByJournalIdTask', [$refundJournalPosting->id, $postData]);
                Apiato::call('Payment@UpdateRefundTask', [$refund->id, ['amount' => $refunds['refundAmount']]]);
                
                $refundRequest['amount'] = $refunds['extraInterest'];
                $refundRequest['description'] = "Refund interest for refund: ".$request->description;

                $refund2Arr = Apiato::call('Payment@CreateRefundTask', [(new CreateRefundTransporter($refundRequest))->toArray(), $loan, $assetType = 'refund_default_interest']);
                $refund2 = $refund2Arr['refund'];
            } elseif($loan->loan_type == 'retained')
                $refund = $this->retainedLoanRefund($loan, $refundRequest, $chargeDate);
        }

        else{
            $refundArr = Apiato::call('Payment@CreateRefundTask', [(new CreateRefundTransporter($refundRequest))->toArray(), $loan, $assetType]);
            $refund = $refundArr['refund'];
        }
// dd('stop');
        \DB::commit();

        return $refund;
    }

    public function retainedLoanRefund($loan, $refundRequest, $chargeDate)
    {
        $refundDate = $refundRequest['refund_at'];
        // $withOutRefundAmount = Apiato::call('Payment@GetRetainedLoanStatementsTask', [$loan, $refundDate->copy()->subDay(), $chargeDate, $refundRequest['amount']])->last()->balance;                
        // $includingRefundAmount = Apiato::call('Payment@GetRetainedLoanStatementsTask', [$loan, $refundDate->copy()->subDay()])->last()->balance;

        $withOutRefundAmount = Apiato::call('Payment@GetMonthlyRetainedLoanStatementsTask', [$loan, $refundDate->copy()->subDay(), $chargeDate, $refundRequest['amount']])->last()->balance;
        $includingRefundAmount = Apiato::call('Payment@GetMonthlyRetainedLoanStatementsTask', [$loan, $refundDate->copy()->subDay()])->last()->balance;
        
        $totalInterest = round($includingRefundAmount - $refundRequest['amount'] - $withOutRefundAmount, 2);
        // dd($withOutRefundAmount, $includingRefundAmount, $totalInterest);
        $refundArr = Apiato::call('Payment@CreateRefundTask', [(new CreateRefundTransporter($refundRequest))->toArray(), $loan, $assetType = 'other_charge_refund']);
        $refund = $refundArr['refund'];
        if($refundRequest['amount'] + $totalInterest) {
            $extraChargeRequest = [
                'loan_id' => $loan->id,
                'amount' => $totalInterest,
                'description'       => "Extra interest refund for: " .$refundRequest['description'],
                'refund_at' => $refundDate,
                'nominalCode' => $refundRequest['nominalCode'],
            ];
            // $extraChargeRefund = Apiato::call('Payment@CreateRefundTask', [(new CreateRefundTransporter($extraChargeRequest))->toArray(), $loan, $assetType = 'other_charge_adjustment']);
            $extraChargeRefundArr = Apiato::call('Payment@CreateRefundTask', [(new CreateRefundTransporter($extraChargeRequest))->toArray(), $loan, $assetType = 'refund_default_interest']);
            $extraChargeRefund = $extraChargeRefundArr['refund'];
        }
        return $refund;
    }
}
