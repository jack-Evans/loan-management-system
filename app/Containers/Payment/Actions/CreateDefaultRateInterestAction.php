<?php

namespace App\Containers\Payment\Actions;

use App\Ship\Parents\Actions\Action;
use App\Ship\Parents\Requests\Request;
use Apiato\Core\Foundation\Facades\Apiato;
use App\Containers\Payment\Exceptions\HTTPPreConditionFailedException;
use Carbon\Carbon;
use App\Containers\Payment\Data\Transporters\CreateRefundTransporter;

class CreateDefaultRateInterestAction extends Action
{
    public function run(Request $request)
    {
    	$loan = Apiato::call('Loan@FindLoanByIdTask', [$request->loanId]);
    	$startDate = Carbon::parse($request->startDate);
        $endDate = Carbon::parse($request->endDate);
        $refundAt = ($request->refundDate) ? Carbon::parse($request->refundDate) : $request->refundDate;
        
        Apiato::call('Statement@VerifyStatementDateTask', [$loan->id, $request->refundDate]);
        // $autoRefundDays = getDueDateHolidays($startDate->copy()->subDay());
        // $refundDays = $request->days - $autoRefundDays;
        // $autoRefundDays = getDueDateHolidays($startDate->copy()->subDay());
        $refundDays = $request->days;
        $refundRate = $loan->interest->default_rate;
        $refundBalance = (float) $request->balance;

        $previousRefunds = Apiato::call('Payment@GetDefaultRateRefundByDateTask', [$request->loanId, $startDate, $endDate]);
        
        if($previousRefunds)
            throw new HTTPPreConditionFailedException("You have already refunded this default rate interest amount.");
        
        // if($startDate->gt($refundAt))
        //     throw new HTTPPreConditionFailedException("Refund date should be greater than default rate interest date.");

        // if($startDate->copy()->endOfMonth()->gte($refundAt->copy()->endOfMonth()))
        //     throw new HTTPPreConditionFailedException("You can refund this month default rate interests only from ".$startDate->copy()->endOfMonth()->addDay()->format('M d Y'));

        $statements = Apiato::call('Payment@GetStatementsTask', [$loan]);
        // refund date will be last actual statement date.
        if(is_null($refundAt))
            $refundAt = $statements->where('description', '!=', 'interest')->last()->created_at;
        // dd($refundAt);
        $otherChargeStatements = $statements->where('description', '=', 'other_charge_refund')->where('created_at', '>=', $endDate)->where('created_at', '<=', $endDate->copy()->endOfMonth())->all();
        $extraInterest = 0;
        foreach($otherChargeStatements as $otherCharge){
            $extraInterest += $otherCharge->credit;
        }
        $dailyRate = calculateDailyRate($refundRate, $refundBalance - $extraInterest);
        $refundAmount = $dailyRate * $refundDays;
        
        $refundRequest = [
            'loan_id'       => $request->loanId,
            'refund_at'     => $refundAt,
            'description'   => "Refund default int of ".$startDate->format('d M y'),
            'start_date'    => $startDate,
            'end_date'      => $endDate,
            'days'          => $request->days,
            'rate'          => round($refundRate, 2),
            'daily_rate'    => round($dailyRate, 2),
            'refund_amount' => round($refundAmount, 2),
            'balance'       => round($request->balance, 2),
            'type'          => 'refund_default_interest',
            'hide'          => $request->hide,
            'hide_transaction' => $request->hideTransaction,
        ];

        \DB::beginTransaction();
        if($refundAt->copy()->endOfMonth()->gt($startDate->copy()->endOfMonth())) {
            $refundRequest['amount'] = round($refundAmount, 2);
            $refundArr = Apiato::call('Payment@CreateRefundTask', [(new CreateRefundTransporter($refundRequest))->toArray(), $loan, $assetType = 'other_charge_refund']);
            $refund = $refundArr['refund'];
            $refundJournalPosting = $refundArr['refundJournalPosting'];

            $refunds = Apiato::call('Payment@ExtraInterestBreakDownTask', [$loan, $refund, $startDate->copy()->endOfMonth(), $refundAt, (float) $refundRequest['amount']]);

            $postData['amount'] = $refunds['refundAmount'];
            Apiato::call('Statement@UpdatePostByJournalIdTask', [$refundJournalPosting->id, $postData]);
            Apiato::call('Payment@UpdateRefundTask', [$refund->id, ['amount' => $refunds['refundAmount']]]);
            
            $refundRequest['amount'] = $refunds['extraInterest'];
            $refundRequest['description'] = "Refund default int of ".$startDate->format('d M y');

            $refund2Arr = Apiato::call('Payment@CreateRefundTask', [(new CreateRefundTransporter($refundRequest))->toArray(), $loan, $assetType = 'refund']);
            $refund2 = $refund2Arr['refund'];
            $defaultRateRefund = Apiato::call('Payment@CreateDefaultRateInterestTask', [$refundRequest, $loan, $createJournal = false]);
            // dd($defaultRateRefund);

        } else{
            $defaultRateRefund = Apiato::call('Payment@CreateDefaultRateInterestTask', [$refundRequest, $loan]);
        }
        // dd($defaultRateRefund);
        \DB::commit();

        return $defaultRateRefund;
    }
}
