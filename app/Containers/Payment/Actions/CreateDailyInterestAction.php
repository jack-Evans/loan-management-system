<?php

namespace App\Containers\Payment\Actions;

use App\Ship\Parents\Actions\Action;
use Apiato\Core\Foundation\Facades\Apiato;
use App\Containers\Loan\Models\Loan;
use App\Ship\Parents\Requests\Request;

class CreateDailyInterestAction extends Action
{
    public function run(Request $request)
    {
        $loan = Apiato::call('Loan@FindLoanByIdTask', [$request->id]);
        $dailyInteresCalculation = Apiato::call('Payment@CalculayeDailyInterestAction', [$request ,$loan]);
        if($dailyInteresCalculation['firstMonthInterest']) {
           $firstMonthInterest = Apiato::call('Payment@CreateFirstMonthInterestTask', [$loan]);
        }
        $date = $dailyInteresCalculation['date'];
        $totalInterest = $dailyInteresCalculation['total'];

        $dailyInterest = Apiato::call('Payment@AddDailyChargeTask', [$loan, $date, $totalInterest]);

        $response = [
            'interest' => $dailyInterest
        ];

        foreach($dailyInteresCalculation as $k => $v) {
            $response[$k] = $v;
        }

        return $response;


        /*$isPaidOnTime           = false;
        $lastPaymentDueDate     = null;

        
        if($lastPaidPayment) {
           $lastPaymentDueDate = \Carbon\Carbon::parse("{$lastPaidPayment->tnx_date->year}-{$lastPaidPayment->tnx_date->month}-{$loanIssueDate->day}");
        }

        // If there is no last payment and difference in loans `issue_date` and `today` is less than one month
        if(!$lastPaidPayment && $today->diffInMonths($loanIssueDate) == 0) {
            // debit charges to account
            $isPaidOnTime = true; 
        }

        // If some payment has been paid
        if($today->diffInMonths($lastPaymentDueDate) == 0) { $isPaidOnTime = true; }
        
    	// dd("Loan Issue Day: {$loanIssueDay} AND Current Day: {$currentday}");

    	// Charge at gross loan rate else at default rate 
    	if($isPaidOnTime) {
    		// Add daily charges
    		$dailyCharges = Apiato::call('Payment@AddDailyChargeTask', [$loan]);
    	} else {
    		// Late!!! Add default charges
    		$defaultCharges = Apiato::call('Payment@AddDefaultChargeTask', [$loan]);
    	}*/
    }
}
