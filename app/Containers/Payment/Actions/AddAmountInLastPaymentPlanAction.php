<?php

namespace App\Containers\Payment\Actions;

use App\Ship\Parents\Actions\Action;
use Apiato\Core\Foundation\Facades\Apiato;

class AddAmountInLastPaymentPlanAction extends Action
{
    public function run($loanId, $chargeValue)
    {
    	$updateLastPaymentPlan = [];
	    $lastPayment = Apiato::call('Payment@FindLastPaymentPlanTask', [$loanId]);
		if($lastPayment != null) {
	        $updatePaymentPlanRequest = [
	            'amount' => round($lastPayment->amount + $chargeValue, 2),
	        ];
	    	$updateLastPaymentPlan = Apiato::call('Payment@UpdatePaymentPlanTask', [$lastPayment->id, $updatePaymentPlanRequest]);
	   	}
        return $updateLastPaymentPlan;
    }
}
