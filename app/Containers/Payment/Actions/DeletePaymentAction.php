<?php

namespace App\Containers\Payment\Actions;

use App\Ship\Parents\Actions\Action;
use App\Ship\Parents\Requests\Request;
use Apiato\Core\Foundation\Facades\Apiato;

class DeletePaymentAction extends Action
{
    public function run(Request $request)
    {
        return Apiato::call('Payment@DeletePaymentTask', [$request->id]);
    }
}
