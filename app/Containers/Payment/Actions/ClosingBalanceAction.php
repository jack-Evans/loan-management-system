<?php

namespace App\Containers\Payment\Actions;

use App\Ship\Parents\Actions\Action;
use App\Ship\Parents\Requests\Request;
use Apiato\Core\Foundation\Facades\Apiato;
use Carbon\Carbon;

class ClosingBalanceAction extends Action
{
    public function run(Request $request = null, $loanId = false, $closingDate = false)
    {
    	$loanId = ($request->loanid) ? $request->loanid : $loanId;
    	$loan = Apiato::call('Loan@FindLoanByIdTask', [$loanId]);
    	$closingDate = ($request->date) ? Carbon::parse($request->date)->copy()->subDay() : $closingDate;

        // if($loan->loan_type == 'serviced'){
        $virtualPmt = false;
        if(!is_bool($request->virtualPmt))
            $virtualPmt = ($request->virtualPmt === 'true') ? true: false;
        else
            $virtualPmt = $request->virtualPmt;
        $pmtPlan = Apiato::call('Payment@GetPaymentPlanTask', [$loanId, $closingDate, $virtualPmt])->last();
        // } else {
        //     $pmtPlan = Apiato::call('Payment@GetPaymentPlanTask', [$loanId, $closingDate])->last();
        //     // dd($pmtPlan);
        // }
        return $pmtPlan;
    }
}
