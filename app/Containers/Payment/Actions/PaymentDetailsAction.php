<?php

namespace App\Containers\Payment\Actions;

use App\Ship\Parents\Actions\Action;
use Apiato\Core\Foundation\Facades\Apiato;
use App\Ship\Parents\Requests\Request;

class PaymentDetailsAction extends Action
{
    public function run(Request $request)
    {
        $paymentDetails = Apiato::call('Payment@PaymentDetailsTask', [$request->id]);
        return $paymentDetails;
    }
}
