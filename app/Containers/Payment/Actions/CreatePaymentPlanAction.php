<?php

namespace App\Containers\Payment\Actions;

use App\Ship\Parents\Actions\Action;
use Apiato\Core\Foundation\Facades\Apiato;
use App\Containers\Loan\Models\Loan;
use Carbon\Carbon;

class CreatePaymentPlanAction extends Action
{
    public function run(Loan $loan)
    {
    	$amountPerMonth = round(resolvePercentage($loan->net_loan) * $loan->interest->rate, 2);
    	$duration 		= $loan->interest->duration;
    	$paymentPlan    = [];
    	$issueDate		= Carbon::parse($loan->issue_at);        
        $dueDay = $issueDate->format('d') - 1;
        \DB::beginTransaction();
        // create payments
        for($i = 0; $i < $duration; $i++) {

            $payBefore = $issueDate->startOfMonth()->addMonths(1);
            $dueDate = Carbon::parse("{$payBefore->format('y')}-{$payBefore->format('m')}-{$dueDay}");

            $paymentPlanRequest = [
                'amount' 	=> $amountPerMonth,
                'status'	=> 'pending',
                'pay_before'    => $dueDate,
                'loan_id'	=> $loan->id,
            ];
            
            $paymentPlan[] = Apiato::call('Payment@CreatePaymentPlanTask', [$paymentPlanRequest]);
        }

        // Loan Amount payable
        $paymentPlanRequest = [
            'amount' 		=> $loan->net_loan,
            'status'		=> 'pending',
            'pay_before'        => $dueDate,
            'loan_id'		=> $loan->id,
            'type'              => 'loan',
        ];
        
        Apiato::call('Payment@CreatePaymentPlanTask', [$paymentPlanRequest]);

        // All Charges
        // $paymentPlanRequest = [
        //     'amount' 		=> 0,
        //     'status'		=> 'pending',
        //     'pay_before'        => $dueDate,
        //     'loan_id'		=> $loan->id,
        //     'type'              => 'charge',
        // ];

        // Apiato::call('Payment@CreatePaymentPlanTask', [$paymentPlanRequest]);
        
        \DB::commit();
        
        return $paymentPlan;
    }
}
