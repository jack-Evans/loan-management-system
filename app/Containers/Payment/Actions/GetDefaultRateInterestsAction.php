<?php

namespace App\Containers\Payment\Actions;

use App\Ship\Parents\Actions\Action;
use App\Ship\Parents\Requests\Request;
use Apiato\Core\Foundation\Facades\Apiato;

class GetDefaultRateInterestsAction extends Action
{
    public function run(Request $request)
    {
    	$loan = Apiato::call('Loan@FindLoanByIdTask', [$request->loanid]);
    	$statementDate = isset($request->date ) ? $request->date : false;
    	$statements = Apiato::call('Payment@GetAllStatementsAction', [$request]);
    	$defaultInterests = [];
        $interestBreakDowns = $statements->where('interestBreakDown', '!=', null)->where('description', '=', 'interest');
    	foreach ($interestBreakDowns as $statement) {
    		$interests = collect($statement->interestBreakDown)->where('rate', '=', $loan->interest->default_rate)->where('total', '>=', 0);
    		foreach ($interests as $interest) {
                $newInterest = [];
                for ($i=0; $i < $interest->days; $i++) {
                    
                    $startDate = $interest->start->copy()->addDays($i);

                    $newInterest['dailyRate'] = $interest->daily_rate;
                    $newInterest['days']       = 1;
                    $newInterest['balance'] = $interest->balance;
                    $newInterest['rate'] = $interest->rate;
                    $newInterest['total'] = $interest->daily_rate;
                    $newInterest['start'] = $startDate;
                    $newInterest['end'] = $startDate;

                    $dueDateHolidays = getDueDateHolidays($interest->start->copy()->subDay());

                    $previousRefunds = Apiato::call('Payment@GetDefaultRateRefundByDateTask', [$loan->id, $startDate, $startDate]);
                    // var_dump($previousRefunds || ($interest->days == $dueDateHolidays && $loan->holiday_refund));
                    $newInterest['hide'] = false;
                    $newInterest['hideTransaction'] = false;
                    if($previousRefunds || ($interest->days == $dueDateHolidays && $loan->holiday_refund)){
                        $result = $statements->where('comments', 'LIKE', "Refund default int of ".$startDate->format('d M y'))->first();
                        if($result)
                        {
                            $newInterest['hide'] = $result->hide;    
                            $newInterest['hideTransaction'] = $result->hideTransaction;    
                        }
                        $newInterest['refunded']  = true;
                    }
                    else
                        $newInterest['refunded']  = false;

                        $defaultInterests[] = $newInterest;
                }
                // $dueDateHolidays = getDueDateHolidays($interest->start->copy()->subDay());

                // $previousRefunds = Apiato::call('Payment@GetDefaultRateRefundByDateTask', [$loan->id, $interest->start, $interest->end]);

                // if($previousRefunds || ($interest->days == $dueDateHolidays && $loan->holiday_refund))
                //     $interest['refunded']  = true;
    			// $defaultInterests[] = $interest;
    		}
    	}

        return collect(Apiato::call('Statement@SetInterestBreakDownTask', [array($defaultInterests)]));
        // return collect($defaultInterests);
    }
}
