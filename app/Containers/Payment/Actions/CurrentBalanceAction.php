<?php

namespace App\Containers\Payment\Actions;

use App\Ship\Parents\Actions\Action;
use App\Ship\Parents\Requests\Request;
use Apiato\Core\Foundation\Facades\Apiato;
use App\Containers\Payment\Models\PaymentPlan;

class CurrentBalanceAction extends Action
{
    public function run(Request $request)
    {
    	$loan = Apiato::call('Loan@FindLoanByIdTask', [$request->loanid]);
    	$statementDate = isset($request->date) ? $request->date : false;
    	if($loan->loan_type === 'retained'){
    		// $statements = Apiato::call('Payment@GetRetainedLoanStatementsTask', [$loan, $statementDate])->last();
            $statements = Apiato::call('Payment@GetMonthlyRetainedLoanStatementsTask', [$loan, $statementDate])->last();

    		$statement = new PaymentPlan();
	        $statement->setIdAttribute($statements->id);
	        $statement->setAmountAttribute(round($statements->balance, 2));
	        $statement->setAmountPendingAttribute(round($statements->balance, 2));
    	}
    	elseif($loan->loan_type === 'half'){
    		$statements = Apiato::call('Payment@GetAllStatementsAction', [$request])->last();

    		$statement = new PaymentPlan();
	        $statement->setIdAttribute($statements->id);
	        $statement->setAmountAttribute(round($statements->balance, 2));
	        $statement->setAmountPendingAttribute(round($statements->balance, 2));
    	}
    	elseif($loan->loan_type === 'manual'){
    		$statements = Apiato::call('Payment@GetManualLoanStatementsTask', [$loan, $statementDate])->last();

    		$statement = new PaymentPlan();
	        $statement->setIdAttribute($statements->id);
	        $statement->setAmountAttribute(round($statements->balance, 2));
	        $statement->setAmountPendingAttribute(round($statements->balance, 2));
    	}
    	else{
	    	$statements = Apiato::call('Payment@GetPaymentPlanTask', [$request->loanid, $statementDate]);
	    	$statement = $statements->last();
    	}

        return $statement;
    }
}
