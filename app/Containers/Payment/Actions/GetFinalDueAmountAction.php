<?php

namespace App\Containers\Payment\Actions;

use App\Ship\Parents\Actions\Action;
use App\Ship\Parents\Requests\Request;
use Apiato\Core\Foundation\Facades\Apiato;
use App\Containers\Payment\Models\Statement;

class GetFinalDueAmountAction extends Action
{
    public function run(Request $request)
    {
        $loan = Apiato::call('Loan@FindLoanByIdTask', [$request->loanid]);
        
        $loanCharges = Apiato::call('LoanCharge@SumOfLoanChargesTask', [$request->loanid]);
        
        $otherCharges = Apiato::call('OtherCharge@SumOfOtherChargesTask', [$request->loanid]);
        
        $finalAmount =  round($loanCharges + $otherCharges + $loan->net_loan, 2);
        
        $statement = new Statement();
        $statement->setBalanceAttribute($finalAmount);
        $statement->setCommentsAttribute('pending');
        $statement->setCreatedAtAttribute('2019-04-02 11:22:38');
        
        return $statement;
    }
}
