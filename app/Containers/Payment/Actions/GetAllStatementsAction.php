<?php

namespace App\Containers\Payment\Actions;

use App\Ship\Parents\Actions\Action;
use App\Ship\Parents\Requests\Request;
use Apiato\Core\Foundation\Facades\Apiato;

class GetAllStatementsAction extends Action
{
    public function run(Request $request)
    {
        $statements = [];
    	$loan = Apiato::call('Loan@FindLoanByIdTask', [$request->loanid]);
    	$statementDate = isset($request->date ) ? $request->date : false;

    	// if($loan->loan_type === 'serviced')
        if(in_array($loan->loan_type, ['serviced', 'rolled']))
	    	$statements = Apiato::call('Payment@GetStatementsTask', [$loan, $statementDate]);
    	
        elseif($loan->loan_type === 'retained' || $loan->loan_type === 'tranch')
	    	// $statements = Apiato::call('Payment@GetRetainedLoanStatementsTask', [$loan, $statementDate]);
            $statements = Apiato::call('Payment@GetMonthlyRetainedLoanStatementsTask', [$loan, $statementDate]);
        
        elseif($loan->loan_type === 'manual')
            $statements = Apiato::call('Payment@GetManualLoanStatementsTask', [$loan, $statementDate]);
        
        elseif($loan->loan_type === 'half'){
            $loan->loan_type = 'retained';
            $duration = $loan->interest->duration;
            $loan->interest->duration = $loan->interest->half_loan_duration;
            $statementDate = addDecimalMonthsIntoDate($loan->interest->half_loan_duration, $loan->issue_at);
            $retainedStatements = Apiato::call('Payment@GetMonthlyRetainedLoanStatementsTask', [$loan, $statementDate->copy()->subDay()]);
            // $retainedStatements = Apiato::call('Payment@GetRetainedLoanStatementsTask', [$loan, $statementDate->copy()->subDay()]);
            $loan->interest->duration = $duration;
            $loan->issue_at = $halfLoan['date'] = $statementDate;
            $halfLoan['balance'] = $retainedStatements->last()->balance;
            $afterHalfStatements = Apiato::call('Payment@GetStatementsTask', [$loan, $statementDate = false, $virtualPmt = false, $halfLoan]);
            
            $statements = $retainedStatements->merge($afterHalfStatements);
        }

        elseif($loan->loan_type === 'n-tranch'){
            $statements = Apiato::call('Payment@GetNTranchStatementsTask', [$loan, $statementDate]);
        }
        return $statements;
    }
}
