<?php

namespace App\Containers\Payment\Actions;

use App\Ship\Parents\Actions\Action;
use App\Ship\Parents\Requests\Request;
use Apiato\Core\Foundation\Facades\Apiato;
use App\Containers\Payment\Data\Transporters\CreatePaymentTransporter;

class CreatePaymentAction extends Action
{
    public function run(Request $request)
    {
        $paymentRequest = [
            'paid_at'           => $request->paidAt,
            'amount'            => $request->amount,
            'loan_id'           => $request->loanId,
            'description'	    => $request->description,
            'is_capital_reduction' => $request->isCapitalReduction,
            'nominalCode'       => $request->nominalCode,
        ];

        $loan = Apiato::call('Loan@FindLoanByIdTask', [$request->loanId]); 
        Apiato::call('Statement@VerifyStatementDateTask', [$loan->id, $request->paidAt]);
        Apiato::call('Payment@CheckCreatePaymentPreCheckTask', [$loan, $paymentRequest]);
        
        \DB::beginTransaction();
        $payment = Apiato::call('Payment@CreatePaymentTask', [(new CreatePaymentTransporter($paymentRequest))->toArray(), $loan]);
        
        // $paymentDate = \Carbon\Carbon::parse($request->paidAt);
        // $paymentPlan = Apiato::call('Payment@FindPaymentPlanByPaymentDateTask', [$request->loanId, $paymentDate]);
        // if(!$paymentPlan)
        //     $paymentPlan = Apiato::call('Payment@GetPaymentPlanTask', [$request->loanId])->first();
        // $amountPaid = $paymentPlan->amount_paid + $request->amount;
        // $paymentPlanUpdateRequest = [
        //     'amount_paid' => $amountPaid,
        //     'paid_at' => $request->paidAt,
        //     'status' => ($amountPaid >= $paymentPlan->amount) ? 'paid': 'pending',
        // ];
        
        // Apiato::call('Payment@UpdatePaymentPlanTask', [$paymentPlan->id, $paymentPlanUpdateRequest]);
        
        \DB::commit();
        return $payment;
    }
}
