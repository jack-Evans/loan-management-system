<?php

namespace App\Containers\Payment\Actions;

use App\Ship\Parents\Actions\Action;
use App\Ship\Parents\Requests\Request;
use Apiato\Core\Foundation\Facades\Apiato;

class GetAllRefundsAction extends Action
{
    public function run(Request $request)
    {
        $refunds = Apiato::call('Payment@GetAllRefundsTask', [$request->loanid]);

        return $refunds;
    }
}
