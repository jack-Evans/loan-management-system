<?php

namespace App\Containers\Payment\Actions;

use App\Ship\Parents\Actions\Action;
use App\Ship\Parents\Requests\Request;
use Apiato\Core\Foundation\Facades\Apiato;

class GetAllPaymentsAction extends Action
{
    public function run(Request $request)
    {
    	$loan = Apiato::call('Loan@FindLoanByIdTask', [$request->loanid]);
//    	$resp = Apiato::call('Payment@GetStatementsTask', [$loan]);
//    	dd("EXIST");
        $payments = Apiato::call('Payment@GetAllPaymentsTask', [$loan], ['addRequestCriteria']);

        return $payments;
    }
}
