<?php

namespace App\Containers\Payment\Actions;

use App\Ship\Parents\Actions\Action;
use Apiato\Core\Foundation\Facades\Apiato;
use App\Ship\Parents\Requests\Request;
use App\Containers\Payment\Exceptions\HTTPPreConditionFailedException;

class CreateInterestForPaymentPlanAction extends Action
{
    public function run(Request $request, $duration = 0)
    {
        $loan = Apiato::call('Loan@FindLoanByIdTask', [$request->loanId]);
        $statementDate = \Carbon\Carbon::parse($request->date);
        // Apiato::call('Statement@VerifyStatementDateTask', [$loan->id, $statementDate]);
        $statement = Apiato::call('Payment@GetStatementsTask', [$loan, $statementDate]);
        // $lastRealStatementDt = Apiato::call('Payment@GetStatementsTask', [$loan])
        // ->where('description', '!=', 'interest')
        // ->where('description', '!=', 'refund_default_interest')
        // ->where('description', '!=', 'skip')
        // ->last()->created_at;
        
        // if($lastRealStatementDt->gt($statementDate))
        //     throw new HTTPPreConditionFailedException("Payment plan date should be greater than {$lastRealStatementDt->copy()->subDay()->format("M d Y")}.");
        $interestRequest = [
        	'principal_amount'      => $loan->interest->principal_amount,
            'rate'                  => $loan->interest->rate,
            'default_rate'          => $loan->interest->default_rate,
            'duration'              => $duration,
            'loan_id'               => $loan->interest->loan_id,
            'gross_loan'            => $statement->last()->balance,
            'payment_plan_date'     => $statementDate,
        ];

        $interest = Apiato::call('Interest@CreateInterestTask', [$interestRequest]);

        return $interest;
    }
}
