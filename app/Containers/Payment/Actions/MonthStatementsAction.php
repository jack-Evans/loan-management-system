<?php

namespace App\Containers\Payment\Actions;

use App\Ship\Parents\Actions\Action;
use App\Ship\Parents\Requests\Request;
use Apiato\Core\Foundation\Facades\Apiato;
use App\Containers\Payment\Models\MonthStatement;
use App\Containers\Payment\UI\API\Transformers\StatementTransformer;
use Apiato\Core\Traits\ResponseTrait;
use App\Classes\Sharepoint;

class MonthStatementsAction extends Action
{

    use ResponseTrait;

    public function run(Request $request)
    {
    	// $loan = Apiato::call('Loan@FindLoanByIdTask', [$request->loanid]);
        // $folerName = strtotime(now());
        ini_set('memory_limit', '1000M');
        if($request->tagIds){
            $loanTags = Apiato::call('Tag@GetAllLoansBasedOnTagsTask', [$request->tagIds]);
            if(count($loanTags) > 0){
                $loanids = array();
                foreach ($loanTags as $key => $loanTag) {
                    array_push($loanids, \Hashids::encode($loanTag->loan_id));
                }
            } else
                throw new \App\Containers\Payment\Exceptions\HTTPPreConditionFailedException('No any loan found with selected tags.');
        } else {
            $loanids = $request->loanId;
        }
        $archiveName = strtotime(now());
        // Sharepoint::createZip($archiveName, '', true, false);
        // $names = [];
        $createZip = true;
        foreach ($loanids as $loanid) {
            $request->loanid = \Hashids::decode($loanid)[0];
            $loan = Apiato::call('Loan@FindLoanByIdTask', [$request->loanid]);
            if($loan){
                $loan->loanid = $loan->loan_id;
                $statements = Apiato::call('Payment@GetAllStatementsAction', [$request]);
                $statements = $this->transform($statements, StatementTransformer::class)['data'];
                $monthStatements = Apiato::call('Payment@GetMonthStatementsAction', [$request]);
                $statement = collect($monthStatements)->last();
                $fileName = preg_replace('/\s/', '',  $loan->loanid."-".\Carbon\Carbon::parse($request->date)->format('M')."Statement".'.pdf');
                $pdf = new \TCPDF();
                $addMonthStatements = new \App\Classes\MonthStatementPdfV2();
                $addMonthStatements->generateBorrowerHtml($pdf, $statement, $fileName, 'F', $statements, $loan);
                // array_push($names, $fileName);

                // $filePath = public_path()."/$fileName";
                // $sharepoint = new \App\Classes\Sharepoint();
                // $sharepoint->uploadToSharepoint($filePath, "Month Statements/".$fileName);
                // unlink($filePath);
                Sharepoint::createZip($archiveName, $fileName, $createZip);
                $createZip = false;
                $filePath = public_path()."/$fileName";
                unlink($filePath);
            }
        }
        $filePath = public_path()."/".$archiveName.".zip";
        $sharepoint = new \App\Classes\Sharepoint();
        $sharepoint->uploadToSharepoint($filePath, "Month Statements/".$archiveName.".zip");
        $sharepoint->unlinkFile($archiveName.".zip");
        return true;
        // return collect($monthStatements);
    }
}
