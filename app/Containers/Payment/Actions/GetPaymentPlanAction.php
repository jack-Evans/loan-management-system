<?php

namespace App\Containers\Payment\Actions;

use App\Ship\Parents\Actions\Action;
use App\Ship\Parents\Requests\Request;
use Apiato\Core\Foundation\Facades\Apiato;

class GetPaymentPlanAction extends Action
{
    public function run(Request $request)
    {
    	// $virtualPmt = true;
    	$loan = Apiato::call('Loan@FindLoanByIdTask', [$request->loanid]);
    	// if($loan->loan_type == 'retained')
	    // 	$virtualPmt = false;

    	// $paymentPlan = Apiato::call('Payment@GetPaymentPlanTask', [$request->loanid, $request->date, $virtualPmt]);
    	$paymentPlan = Apiato::call('Payment@GetPaymentPlanTask', [$request->loanid, $request->date, $request->date]);

        return $paymentPlan;
    }
}
