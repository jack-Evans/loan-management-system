<?php

namespace App\Containers\Payment\Actions;

use App\Ship\Parents\Actions\Action;
use App\Ship\Parents\Requests\Request;
use Apiato\Core\Foundation\Facades\Apiato;

class UpdateTransactionAction extends Action
{
    public function run(Request $request)
    {
        $data = [];
        
        if(isset($request->hide))
        	$data['hide'] = $request->hide;

        if(isset($request->notes))
        	$data['notes'] = $request->notes;

        if(isset($request->hideTransaction))
            $data['hide_transaction'] = $request->hideTransaction;
// dd($data, $request->journalId);
        $journal = Apiato::call('Payment@UpdateJournalTask', [$request->journalId, $data]);

        return $journal;
    }
}
