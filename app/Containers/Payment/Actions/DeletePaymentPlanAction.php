<?php

namespace App\Containers\Payment\Actions;

use App\Ship\Parents\Actions\Action;
use App\Ship\Parents\Requests\Request;
use Apiato\Core\Foundation\Facades\Apiato;
use App\Containers\Payment\Exceptions\HTTPPreConditionFailedException;

class DeletePaymentPlanAction extends Action
{
    public function run(Request $request)
    {
    	$interests = Apiato::call('Interest@GetInterestsAsPmtPlanVersionsTask', [$request->loanid]);
    	if(count($interests) <= 1){
    		throw new HTTPPreConditionFailedException("Sorry! You cannot delete last payment plan.");
    	}
    	$paymentPlan = Apiato::call('Interest@DeleteInterestTask', [$interests->last()->id]);

        return $paymentPlan;
    }
}
