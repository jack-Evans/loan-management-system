<?php

namespace App\Containers\Payment\Data\Repositories;

use App\Ship\Parents\Repositories\Repository;

/**
 * Class PostRepository
 */
class PostRepository extends Repository
{

    /**
     * @var array
     */
    protected $fieldSearchable = [
        'id' => '=',
        // ...
    ];

}
