<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreatePostTables extends Migration
{

    /**
     * Run the migrations.
     */
    public function up()
    {
        Schema::create('posts', function (Blueprint $table) {

            $table->increments('id');
            $table->unsignedInteger('account_id');
            $table->unsignedInteger('journal_id');
            $table->string('accounting_period');
            $table->string('asset_type');
            $table->float('amount', 12, 4);
            $table->enum('type', ['credit','debit'])->default('credit');
            $table->timestamps();
            $table->softDeletes();

        });
    }

    /**
     * Reverse the migrations.
     */
    public function down()
    {
        Schema::drop('posts');
    }
}
