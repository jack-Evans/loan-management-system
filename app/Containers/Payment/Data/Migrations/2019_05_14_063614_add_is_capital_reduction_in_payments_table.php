<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class AddIsCapitalReductionInPaymentsTable extends Migration
{

    /**
     * Run the migrations.
     */
    public function up()
    {
        Schema::table('payments', function (Blueprint $table) {

            $table->boolean('is_capital_reduction')->default(false)->after('loan_id');

        });
    }

    /**
     * Reverse the migrations.
     */
    public function down()
    {
        Schema::table('payments', function (Blueprint $table){
            $table->dropcolumn('is_capital_reduction');
        });
    }
}
