<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreatePaymentTables extends Migration
{

    /**
     * Run the migrations.
     */
    public function up()
    {
        Schema::create('payments', function (Blueprint $table) {

            $table->increments('id');
            $table->float('amount', 12, 4);
            $table->float('current_balance', 8, 4);
            $table->enum('status', ['pending','paid'])->default('pending');
            $table->date('paid_at')->nullable();
            $table->date('pay_before');
            $table->unsignedInteger('loan_id');
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down()
    {
        Schema::drop('payments');
    }
}
