<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class ChangeInvoiceIdType extends Migration
{

    /**
     * Run the migrations.
     */
    public function up()
    {
        \DB::statement("ALTER TABLE journals CHANGE invoice_id invoice_id varchar(191)");
    }

    /**
     * Reverse the migrations.
     */
    public function down()
    {
        \DB::statement("ALTER TABLE journals CHANGE invoice_id invoice_id int(11)");
    }
}
