<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateAccountTable extends Migration
{

    /**
     * Run the migrations.
     */
    public function up()
    {
        Schema::create('accounts', function (Blueprint $table) {

            $table->increments('id');
            $table->unsignedInteger('customer_id');
            $table->float('credit', 8, 4);
            $table->float('debit', 8, 4);
            $table->float('balance', 8, 4);
            $table->enum('status', ['other','receivable','payable'])->default('payable');
            $table->timestamps();
            $table->softDeletes();

        });
       
    }

    /**
     * Reverse the migrations.
     */
    public function down()
    {
        Schema::drop('accounts');
    }
}
