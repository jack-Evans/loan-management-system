<?php

use Illuminate\Database\Migrations\Migration;

class ChangeJournalTypeEnum extends Migration
{

    /**
     * Run the migrations.
     */
    public function up()
    {
        
        \DB::statement("ALTER TABLE journals CHANGE type type ENUM('charge', 'loan','payment', 'refund', 'refund_default_interest', 'other_charge_refund', 'debit_filler', 'credit_filler', 'renewal_charge', 'skip', 'transfer', 'other_charge_adjustment', 'interest', 'tranch')");
    }

    /**
     * Reverse the migrations.
     */
    public function down()
    {
        \DB::statement("ALTER TABLE journals CHANGE type type ENUM('charge', 'loan','payment', 'refund', 'refund_default_interest', 'other_charge_refund', 'debit_filler', 'credit_filler', 'renewal_charge', 'skip', 'transfer', 'other_charge_adjustment', 'interest')");
    }
}
