<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class DropPaymentCurrentbalancePaybeforeColumns extends Migration
{

    /**
     * Run the migrations.
     */
    public function up()
    {
        Schema::table('payments', function (Blueprint $table) {

            $table->dropColumn(['current_balance', 'pay_before', 'status']);

        });
    }

    /**
     * Reverse the migrations.
     */
    public function down()
    {
        Schema::table('payments', function(Blueprint $table) {
            $table->date('pay_before');
            $table->enum('status', ['pending','paid'])->default('pending');
            $table->float('current_balance', 8, 4);
        });
    }
}
