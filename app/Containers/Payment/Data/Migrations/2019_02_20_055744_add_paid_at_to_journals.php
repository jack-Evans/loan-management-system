<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class AddPaidAtToJournals extends Migration
{

    /**
     * Run the migrations.
     */
    public function up()
    {
        Schema::table('journals', function (Blueprint $table) {

           $table->date('tnx_date')->after('type');

        });

    }

    /**
     * Reverse the migrations.
     */
    public function down()
    {
        Schema::table('journals', function($table) {
            $table->dropColumn('tnx_date');
        });
    }
}
