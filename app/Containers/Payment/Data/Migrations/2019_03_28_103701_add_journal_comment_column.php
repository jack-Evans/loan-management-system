<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class AddJournalCommentColumn extends Migration
{

    /**
     * Run the migrations.
     */
    public function up()
    {
        Schema::table('journals', function (Blueprint $table) {

           $table->text('comment')->nullable()->after('type');

        });
    }

    /**
     * Reverse the migrations.
     */
    public function down()
    {
        Schema::table('journals', function (Blueprint $table) {

            $table->dropColumn(['comment']);
        });
    }
}
