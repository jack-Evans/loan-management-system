<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class AddNotesHideIntoJournal extends Migration
{

    /**
     * Run the migrations.
     */
    public function up()
    {
        Schema::table('journals', function (Blueprint $table) {
            $table->string('hide')->after('payable_type')->nullable();
            $table->string('hide_transaction')->after('hide')->nullable();
            $table->string('notes')->after('hide_transaction')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down()
    {
        Schema::table('journals', function (Blueprint $table) {
            $table->dropColumn(['hide', 'hide_transaction', 'notes']);
        });
    }
}
