<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateDefaultCharge extends Migration
{

    /**
     * Run the migrations.
     */
    public function up()
    {
        Schema::create('default_charges', function (Blueprint $table) {

            $table->increments('id');
            $table->float('value', 8, 4);
            $table->unsignedInteger('days');
            $table->timestamps();
            $table->softDeletes();

        });
    }

    /**
     * Reverse the migrations.
     */
    public function down()
    {
        Schema::drop('default_charges');
    }
}
