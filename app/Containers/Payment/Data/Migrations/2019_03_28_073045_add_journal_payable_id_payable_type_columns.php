<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class AddJournalPayableIdPayableTypeColumns extends Migration
{

    /**
     * Run the migrations.
     */
    public function up()
    {
        Schema::table('journals', function (Blueprint $table) {

            $table->string('payable_type')->after('tnx_date');
            $table->unsignedInteger('payable_id')->after('tnx_date');
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down()
    {
        Schema::table('journals', function (Blueprint $table) {

            $table->dropColumn(['payable_type', 'payable_id']);

        });
    }
}
