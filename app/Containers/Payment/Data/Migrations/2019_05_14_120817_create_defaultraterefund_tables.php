<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateDefaultRateRefundTables extends Migration
{

    /**
     * Run the migrations.
     */
    public function up()
    {
        Schema::create('default_rate_refunds', function (Blueprint $table) {

            $table->increments('id');
            $table->unsignedInteger('loan_id');
            $table->date('refund_at');
            $table->date('start_date');
            $table->date('end_date');
            $table->integer('days');
            $table->float('rate', 12, 4);
            $table->float('daily_rate', 12, 4);
            $table->float('refund_amount', 12, 4);
            $table->float('balance', 12, 4);
            $table->string('description')->nullable();
            $table->timestamps();
            $table->softDeletes();

        });
    }

    /**
     * Reverse the migrations.
     */
    public function down()
    {
        Schema::drop('default_rate_refunds');
    }
}
