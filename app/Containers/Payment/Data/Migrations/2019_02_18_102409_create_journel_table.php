<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateJournelTable extends Migration
{

    /**
     * Run the migrations.
     */
    public function up()
    {
        Schema::create('journals', function (Blueprint $table) {

            $table->increments('id');
            $table->unsignedInteger('default_credit_account_id');
            $table->unsignedInteger('default_debit_account_id');
            $table->unsignedInteger('loan_id');
            $table->unsignedInteger('invoice_id')->nullable();
            $table->enum('type', ['daily','default','charge','loan','interest','payment']);
            $table->timestamps();
            $table->softDeletes();

        });
    }

    /**
     * Reverse the migrations.
     */
    public function down()
    {
        Schema::drop('journals');
    }
}
