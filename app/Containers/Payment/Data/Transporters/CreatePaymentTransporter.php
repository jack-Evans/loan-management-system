<?php

namespace App\Containers\Payment\Data\Transporters;

use App\Ship\Parents\Transporters\Transporter;

class CreatePaymentTransporter extends Transporter
{

    /**
     * @var array
     */
    protected $schema = [
        'type' => 'object',
        'properties' => [
            'paid_at'   => ['type' => 'string'],
            'amount'    => ['type' => 'number'],
            'loan_id'   => ['type' => 'integer'],    
            'is_capital_reduction'   => ['type' => 'string'],    
        ],
        'required'   => [
            'paid_at',
            'amount',
            'loan_id',
        ],
        'default'    => [
            // provide default values for specific properties here
        ]
    ];
}
