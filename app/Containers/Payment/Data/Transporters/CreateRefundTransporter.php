<?php

namespace App\Containers\Payment\Data\Transporters;

use App\Ship\Parents\Transporters\Transporter;

class CreateRefundTransporter extends Transporter
{

    /**
     * @var array
     */
    protected $schema = [
        'type' => 'object',
        'properties' => [
            'name'      => ['type' => 'string'],    
            'refund_at',
            'amount'    => ['type' => 'number'],
            'loan_id'   => ['type' => 'integer'],    
        ],
        'required'   => [
            'refund_at',
            'amount',
            'loan_id',
        ],
        'default'    => [
            'name' => '',
            // provide default values for specific properties here
        ]
    ];
}
