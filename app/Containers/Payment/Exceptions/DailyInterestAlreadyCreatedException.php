<?php

namespace App\Containers\Payment\Exceptions;

use App\Ship\Parents\Exceptions\Exception;
use Symfony\Component\HttpFoundation\Response;

class DailyInterestAlreadyCreatedException extends Exception
{
    public $httpStatusCode = Response::HTTP_LOCKED;

    public $message = 'Daily Interest already created';

    public $code = 423;
}
