<?php

namespace App\Containers\Payment\Exceptions;

use App\Ship\Parents\Exceptions\Exception;
use Symfony\Component\HttpFoundation\Response;

class HTTPPreConditionFailedException extends Exception
{
    public $httpStatusCode = Response::HTTP_PRECONDITION_FAILED;

    public $message = 'Precondition Failed';

    public $code = 412;


}
