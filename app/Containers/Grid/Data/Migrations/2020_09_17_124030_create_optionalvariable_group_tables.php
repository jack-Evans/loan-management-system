<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateOptionalvariableGroupTables extends Migration
{

    /**
     * Run the migrations.
     */
    public function up()
    {
        Schema::create('optional_variable_groups', function (Blueprint $table) {

            $table->increments('id');
            $table->string('group_name');
            $table->timestamps();
            $table->softDeletes();

        });
    }

    /**
     * Reverse the migrations.
     */
    public function down()
    {
        Schema::dropIfExists('optional_variable_groups');
    }
}
