<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateGridOptionalvariablesTables extends Migration
{

    /**
     * Run the migrations.
     */
    public function up()
    {
        Schema::create('grid_optional_variables', function (Blueprint $table) {

            $table->increments('id');
            $table->unsignedInteger('grid_id');
            $table->unsignedInteger('optional_variable_id');
            $table->unsignedInteger('sort_order');
            $table->timestamps();
            //$table->softDeletes();

        });
    }

    /**
     * Reverse the migrations.
     */
    public function down()
    {
        Schema::dropIfExists('grid_optional_variables');
    }
}
