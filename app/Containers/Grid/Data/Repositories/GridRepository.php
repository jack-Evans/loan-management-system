<?php

namespace App\Containers\Grid\Data\Repositories;

use App\Ship\Parents\Repositories\Repository;

/**
 * Class GridRepository
 */
class GridRepository extends Repository
{

    /**
     * @var array
     */
    protected $fieldSearchable = [
        'id' => '=',
        // ...
    ];

}
