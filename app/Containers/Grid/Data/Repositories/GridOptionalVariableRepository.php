<?php

namespace App\Containers\Grid\Data\Repositories;

use App\Ship\Parents\Repositories\Repository;

/**
 * Class GridOptionalVariableRepository
 */
class GridOptionalVariableRepository extends Repository
{

    /**
     * @var array
     */
    protected $fieldSearchable = [
        'id' => '=',
        // ...
    ];

}
