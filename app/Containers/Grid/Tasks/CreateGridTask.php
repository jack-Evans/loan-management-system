<?php

namespace App\Containers\Grid\Tasks;

use App\Containers\Grid\Data\Repositories\GridRepository;
use App\Ship\Exceptions\CreateResourceFailedException;
use App\Ship\Parents\Tasks\Task;
use Exception;

class CreateGridTask extends Task
{

    protected $repository;

    public function __construct(GridRepository $repository)
    {
        $this->repository = $repository;
    }

    public function run(array $data)
    {
        try {
            return $this->repository->create($data);
        }
        catch (Exception $exception) {
            throw new CreateResourceFailedException();
        }
    }
}
