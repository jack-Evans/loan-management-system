<?php

namespace App\Containers\Grid\Tasks;

use App\Containers\Grid\Data\Repositories\GridOptionalVariableRepository;
use App\Ship\Parents\Tasks\Task;

class GridOptionalVariableTask extends Task
{

    protected $repository;

    public function __construct(GridOptionalVariableRepository $repository)
    {
        $this->repository = $repository;
    }

    public function run($gridId)
    {
        // return $this->repository->where('grid_id', $gridId)->orderBy('sort_order')->with('groupByGroupId')->get();
        return $this->repository->where('grid_id', $gridId)->orderBy('sort_order')->get();
    }
}
