<?php

namespace App\Containers\Grid\Tasks;

use App\Containers\Grid\Data\Repositories\GridOptionalVariableRepository;
use App\Ship\Exceptions\CreateResourceFailedException;
use App\Ship\Parents\Tasks\Task;
use Exception;

class CreateGridOptionalVariableTask extends Task
{

    protected $repository;

    public function __construct(GridOptionalVariableRepository $repository)
    {
        $this->repository = $repository;
    }

    public function run(array $data)
    {
        try {
            return $this->repository->create($data);
        }
        catch (Exception $exception) {
            throw new CreateResourceFailedException();
        }
    }
}
