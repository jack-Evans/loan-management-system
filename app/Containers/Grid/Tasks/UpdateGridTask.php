<?php

namespace App\Containers\Grid\Tasks;

use App\Containers\Grid\Data\Repositories\GridRepository;
use App\Ship\Exceptions\UpdateResourceFailedException;
use App\Ship\Parents\Tasks\Task;
use Exception;

class UpdateGridTask extends Task
{

    protected $repository;

    public function __construct(GridRepository $repository)
    {
        $this->repository = $repository;
    }

    public function run($id, array $data)
    {
        try {
            return $this->repository->update($data, $id);
        }
        catch (Exception $exception) {
            throw new UpdateResourceFailedException();
        }
    }
}
