<?php

namespace App\Containers\Grid\Tasks;

use App\Containers\Grid\Data\Repositories\GridRepository;
use App\Ship\Exceptions\NotFoundException;
use App\Ship\Parents\Tasks\Task;
use Exception;

class FindGridByIdTask extends Task
{

    protected $repository;

    public function __construct(GridRepository $repository)
    {
        $this->repository = $repository;
    }

    public function run($id)
    {
        try {
            return $this->repository->find($id);
        }
        catch (Exception $exception) {
            throw new NotFoundException();
        }
    }
}
