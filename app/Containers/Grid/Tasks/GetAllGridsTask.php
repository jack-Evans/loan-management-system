<?php

namespace App\Containers\Grid\Tasks;

use App\Containers\Grid\Data\Repositories\GridRepository;
use App\Ship\Parents\Tasks\Task;

class GetAllGridsTask extends Task
{

    protected $repository;

    public function __construct(GridRepository $repository)
    {
        $this->repository = $repository;
    }

    public function run()
    {
        return $this->repository->paginate();
    }
}
