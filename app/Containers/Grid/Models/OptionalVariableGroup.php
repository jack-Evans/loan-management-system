<?php

namespace App\Containers\Grid\Models;

use App\Ship\Parents\Models\Model;

class OptionalVariableGroup extends Model
{
    protected $fillable = [
        'group_name'
    ];

    protected $attributes = [

    ];

    protected $hidden = [

    ];

    protected $casts = [

    ];

    protected $dates = [
        'created_at',
        'updated_at',
    ];

    /**
     * A resource key to be used by the the JSON API Serializer responses.
     */
    protected $resourceKey = 'optional_variable_groups';
 
    public function optionalVariables() {
        return $this->hasMany(\App\Containers\Optionalvariable\Models\OptionalVariable::class);
    }    
}
