<?php

namespace App\Containers\Grid\Models;

use App\Ship\Parents\Models\Model;

class GridOptionalVariable extends Model
{
    protected $fillable = [
        'grid_id',
        'optional_variable_id',
        'sort_order',
    ];

    protected $attributes = [

    ];

    protected $hidden = [

    ];

    protected $casts = [

    ];

    protected $dates = [
        'created_at',
        'updated_at',
    ];

    /**
     * A resource key to be used by the the JSON API Serializer responses.
     */
    protected $resourceKey = 'gridOptionalVariables';

    public function grid() {
        return $this->belongsTo(\App\Containers\Grid\Models\Grid::class, 'grid_id');
    }

    public function gridOptionalVariable() {
        return $this->belongsTo(\App\Containers\Optionalvariable\Models\Optionalvariable::class, 'optional_variable_id');
    }

    public function groupByGroupId()
    {
        return $this->belongsTo(\App\Containers\Optionalvariable\Models\Optionalvariable::class, 'optional_variable_id')
             ->groupBy('optional_variable_group_id');
             // ->withPivot('gridOptionalVariable')
    }
    
}
