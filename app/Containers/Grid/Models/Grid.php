<?php

namespace App\Containers\Grid\Models;

use App\Ship\Parents\Models\Model;

class Grid extends Model
{
    protected $fillable = [
        'name'
    ];

    protected $attributes = [

    ];

    protected $hidden = [

    ];

    protected $casts = [

    ];

    protected $dates = [
        'created_at',
        'updated_at',
    ];

    /**
     * A resource key to be used by the the JSON API Serializer responses.
     */
    protected $resourceKey = 'grids';

    public function gridOptionalVariables() {
        return $this->hasMany(\App\Containers\Grid\Models\GridOptionalVariable::class);
    }    
}
