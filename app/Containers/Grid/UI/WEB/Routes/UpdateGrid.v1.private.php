<?php

/** @var Route $router */
$router->post('grids/{id}/update', [
    'as' => 'web_grid_update',
    'uses'  => 'Controller@update',
    'middleware' => [
      'auth:web',
    ],
]);
