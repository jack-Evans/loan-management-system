<?php

/** @var Route $router */
$router->post('grids/import-datatapes', [
    'as' => 'web_grid_view',
    'uses'  => 'Controller@importDatatapes',
    'middleware' => [
      'auth:web',
    ],
]);
