<?php

/** @var Route $router */
$router->get('grids/{id}/edit', [
    'as' => 'web_grid_edit',
    'uses'  => 'Controller@edit',
    'middleware' => [
      'auth:web',
    ],
]);
