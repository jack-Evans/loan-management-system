<?php

/** @var Route $router */
$router->post('grid-optional-variable/create', [
    'as' => 'web_create_grid_optional_variable',
    'uses'  => 'Controller@createGridOptionalVariable',
    'middleware' => [
      'auth:web',
    ],
]);
