<?php

/** @var Route $router */
$router->get('grid-optional-variables/{gridId}', [
    'as' => 'web_grid_index',
    'uses'  => 'Controller@gridOptionalvariables',
    'middleware' => [
      'auth:web',
    ],
]);
