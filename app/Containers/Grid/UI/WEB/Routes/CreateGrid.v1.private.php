<?php

/** @var Route $router */
$router->get('grids/create', [
    'as' => 'web_grid_create',
    'uses'  => 'Controller@create',
    'middleware' => [
      'auth:web',
    ],
]);
