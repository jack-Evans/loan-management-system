<?php

/** @var Route $router */
$router->get('grids', [
    'as' => 'web_grid_index',
    'uses'  => 'Controller@index',
    'middleware' => [
      'auth:web',
    ],
]);
