<?php

/** @var Route $router */
$router->post('grids/sort-variables', [
    'as' => 'web_grid_view',
    'uses'  => 'Controller@sortVariables',
    'middleware' => [
      'auth:web',
    ],
]);
