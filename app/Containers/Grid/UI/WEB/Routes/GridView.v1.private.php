<?php

/** @var Route $router */
$router->get('grids/{gridId}/view', [
    'as' => 'web_grid_view',
    'uses'  => 'Controller@gridView',
    'middleware' => [
      'auth:web',
    ],
]);
