<?php

/** @var Route $router */
$router->get('grids/{id}/delete', [
    'as' => 'web_grid_delete',
    'uses'  => 'Controller@delete',
    'middleware' => [
      'auth:web',
    ],
]);
