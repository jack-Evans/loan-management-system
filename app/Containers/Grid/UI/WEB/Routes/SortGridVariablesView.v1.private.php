<?php

/** @var Route $router */
$router->get('grids/sort-variables/{gridId}', [
    'as' => 'web_grid_view',
    'uses'  => 'Controller@gridOptionalvariables',
    'middleware' => [
      'auth:web',
    ],
]);
