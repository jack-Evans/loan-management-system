<?php

/** @var Route $router */
$router->post('grid-optional-variable/delete', [
    'as' => 'web_delete_grid_optional_variable',
    'uses'  => 'Controller@deleteGridOptionalVariable',
    'middleware' => [
      'auth:web',
    ],
]);
