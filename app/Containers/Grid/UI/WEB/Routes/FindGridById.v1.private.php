<?php

/** @var Route $router */
$router->get('grids/{id}', [
    'as' => 'web_grid_show',
    'uses'  => 'Controller@show',
    'middleware' => [
      'auth:web',
    ],
]);
