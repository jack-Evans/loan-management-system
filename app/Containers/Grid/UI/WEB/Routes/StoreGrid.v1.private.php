<?php

/** @var Route $router */
$router->post('grids/store', [
    'as' => 'web_grid_store',
    'uses'  => 'Controller@store',
    'middleware' => [
      'auth:web',
    ],
]);
