<?php

namespace App\Containers\Grid\UI\WEB\Controllers;

use App\Containers\Grid\UI\WEB\Requests\CreateGridRequest;
use App\Containers\Grid\UI\WEB\Requests\DeleteGridRequest;
use App\Containers\Grid\UI\WEB\Requests\GetAllGridsRequest;
use App\Containers\Grid\UI\WEB\Requests\FindGridByIdRequest;
use App\Containers\Grid\UI\WEB\Requests\UpdateGridRequest;
use App\Containers\Grid\UI\WEB\Requests\StoreGridRequest;
use App\Containers\Grid\UI\WEB\Requests\EditGridRequest;
use App\Ship\Parents\Controllers\WebController;
use Apiato\Core\Foundation\Facades\Apiato;

/**
 * Class Controller
 *
 * @package App\Containers\Grid\UI\WEB\Controllers
 */
class Controller extends WebController
{
    /**
     * The assigned API PATH for this Controller
     *
     * @var string
     */
    
    protected $apiPath;
    
    public function __construct(){
        $this->apiPath = config('token-container.WEB_API_URL');
    }

    /**
     * Show all entities
     *
     * @param GetAllGridsRequest $request
     */
    public function index(GetAllGridsRequest $request)
    {
        // grids
        $url = $this->apiPath.'grids';
        try {
            $response = get($url, ['limit' => 0]);
            $grids = $response->data;
        } catch (\Exception $e) {
            return exception($e);
        }
        // if($request->json)
        //     return response()->json(['data' => $optionalVariables]);

        return view('grid::grids', compact('grids'));
    }

    /**
     * Show one entity
     *
     * @param FindGridByIdRequest $request
     */
    public function show(FindGridByIdRequest $request)
    {
    }

    /**
     * Create entity (show UI)
     *
     * @param CreateGridRequest $request
     */
    public function create(CreateGridRequest $request)
    {
        return view('grid::create-update-grids');
    }

    /**
     * Add a new entity
     *
     * @param StoreGridRequest $request
     */
    public function store(StoreGridRequest $request)
    {
        $url = $this->apiPath.'grids';
        $data = [
            'name' => $request->name,
        ];

        try {
            $response = post($url, $data);
        } catch (\Exception $e) {
            return exception($e);
        }

        return redirect('grids');
    }

    /**
     * Edit entity (show UI)
     *
     * @param EditGridRequest $request
     */
    public function edit(EditGridRequest $request)
    {
        $url = $this->apiPath.'grids/'.$request->id;
        try {
            $response = get($url);
            $grid = $response->data;
        } catch (\Exception $e) {
            return exception($e);
        }

        return view('grid::create-update-grids', compact('grid'));
    }

    /**
     * Update a given entity
     *
     * @param UpdateGridRequest $request
     */
    public function update(UpdateGridRequest $request)
    {
        $url = $this->apiPath.'grids/'.$request->id;
        $data = [
            'name'  => $request->name,
        ];
        
        try {
            $response = patch($url, $data);
        } catch (\Exception $e) {
            return exception($e);
        }
        
        return redirect('grids');
    }

    /**
     * Delete a given entity
     *
     * @param DeleteGridRequest $request
     */
    public function delete(DeleteGridRequest $request)
    {
        $url = $this->apiPath.'grids/'.$request->id;
        
        try {
            $response = delete($url);
        } catch (\Exception $e) {
            return exception($e);
        }
        
        return redirect('grids');
    }

    public function gridOptionalvariables(DeleteGridRequest $request)
    {
        if(session('mandatoryCheck') == null)
            session(['mandatoryCheck' => '0']);

        $url = $this->apiPath.'grid-optional-variables/'.$request->gridId;
        
        try {
            $response = get($url);
            $gridOptionalVariables = $response->data;
        } catch (\Exception $e) {
            return exception($e);
        }
        if(isset($_GET['web']) && $_GET['web'] == true){
            // grid
            $url = $this->apiPath.'grids/'.$request->gridId;
            try {
                $response = get($url);
                $grid = $response->data;
            } catch (\Exception $e) {
                return exception($e);
            }

            return view('grid::sort-grid-optional-variables', compact('grid', 'gridOptionalVariables'));
        }
        if(isset($_GET['mandatoryCheck'])){
            $mandatoryCheck = $_GET['mandatoryCheck'];
            $mandatoryCheck = ($mandatoryCheck == '0') ? '1' : '0';
            session(['mandatoryCheck' => $mandatoryCheck]);
            // dd(session('mandatoryCheck'), $gridOptionalVariables);
        }
        if(session('mandatoryCheck') == '1')
            $gridOptionalVariables = collect($gridOptionalVariables)->where('mandatory', 1);
        
        return response()->json(['data' => $gridOptionalVariables]);
    }

    /**
     * View grid optional variables
     *
     * @param DeleteGridRequest $request
     */
    public function gridView(DeleteGridRequest $request)
    {
        // grid
        $url = $this->apiPath.'grids/'.$request->gridId;
        try {
            $response = get($url);
            $grid = $response->data;
        } catch (\Exception $e) {
            return exception($e);
        }

        // optionalvariables
        $url = $this->apiPath.'optionalvariables';
        try {
            $response = get($url, ['limit' => 0]);
            $optionalVariables = $response->data;
        } catch (\Exception $e) {
            return exception($e);
        }

        // gridOptionalVariables
        $url = $this->apiPath.'grid-optional-variables/'.$request->gridId;
        
        try {
            $response = get($url);
            $gridOptionalVariables = $response->data;
        } catch (\Exception $e) {
            return exception($e);
        }

        // dd(collect($gridOptionalVariables)->contains('optionalVariable', '1st charge 2nd charge or equitable'), collect($gridOptionalVariables));

        return view('grid::grid-optional-variables', compact('grid', 'gridOptionalVariables', 'optionalVariables'));
    }

    public function createGridOptionalVariable(UpdateGridRequest $request)
    {
        $url = $this->apiPath.'grid-optional-variables';
        $data = [
            'gridId' => $request->gridId,
            'optionalVariableIds' => $request->optionalVariableIds,
        ];

        try {
            $response = post($url, $data);
        } catch (\Exception $e) {
            return exception($e);
            // return response()->json(['message' => $e->getMessage()]);
        }

        return redirect()->back();
        // return response()->json(['message' => 'Optional Variable assigned successfullly.']);
    }

    public function deleteGridOptionalVariable(UpdateGridRequest $request)
    {
        $url = $this->apiPath.'grid-optional-variables/'.$request->gridId."/".$request->optionalVariableId;

        try {
            $response = delete($url);
        } catch (\Exception $e) {
            return response()->json(['message' => $e->getMessage()]);
        }

        return response()->json(['message' => 'Optional Variable deleted successfullly.']);
    }

    public function sortVariables(UpdateGridRequest $request)
    {
        $url = $this->apiPath.'grid-optional-variables/sort';
        $data = [
            'sortOrder' => explode(',', $request->sortOrder),
        ];

        try {
            $response = post($url, $data);
        } catch (\Exception $e) {
            return response()->json(['data' => $e->getMessage()]);
        }

        return response()->json(['data' => true]);
    }
    public function importDatatapes(UpdateGridRequest $request)
    {
        $url = $this->apiPath.'options/import';

        if(empty($_FILES['file']['name']))
            return response()->json(['code' => 401, 'message' => 'Please Upload A File!']);

        try{
            $response = postWithFile($url, $fileName = "file", []);
            return response()->json($response);
        } catch (\Exception $e) {
            return response()->json(['code' => 400, 'message' => $e->getMessage()]);
        }

        return response()->json(['code' => 400, 'message' => 'Internal server error.']);
    }
}
