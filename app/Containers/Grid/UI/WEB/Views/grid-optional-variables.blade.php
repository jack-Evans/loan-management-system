@extends('layouts.app')

@section('title', 'Grid Optional Variables')

@section('content')
<div class="col-lg-12">
    <div class="has-bg">
        <h3>
            Optional Variables of: {{$grid->name}}
            <a href="{{url('grids')}}" class="pull-right">
                <button class="btn btn-success">Back</button>
            </a>
        </h3>
        <span class="space"></span>
        <form action="{{url('grid-optional-variable/create')}}" method="post">
            @csrf()
            <input type="hidden" name="gridId" value="{{$grid->id}}">
            <div class="row">
                <div class="col-10 offset-2">
                    <button class="btn btn-primary pull-right" type="submit">Save</button>
                </div>
                <div class="col-6 offset-2">
                    <select multiple="multiple" id="my-select" name="optionalVariableIds[]">
                        @foreach($optionalVariables as $key => $variable)
                            <option value="{{$variable->id}}" @if(collect($gridOptionalVariables)->contains('optionalVariable', str_replace(".","",trimedLowerStr($variable->optional_variable)))) selected @endif >{{$variable->optional_variable}}</option>
                        @endforeach
                    </select>
                </div>
            </div>
        </form>
    </div>
</div>
@endsection

@section('pages-js')

<script>
    // $('.draggable-element').arrangeable();
    // var variableIds = [];
    $('#my-select').multiSelect(
    // {
    //     afterSelect: function(value){
    //         // variableIds.push(value[0]);
    //         // var gridId = "{{$grid->id}}";
    //         // console.log(value[0], gridId);
    //         // $.ajax({
    //         //     url : "{{url('grid-optional-variable/create')}}",
    //         //     type: 'POST',
    //         //     data : {gridId:gridId, optionalVariableId:value[0]},
    //         //     success: function(result){
    //         //         console.log(result);
    //         //     },
    //         //     error: function(errors){
    //         //         console.log(errors);
    //         //     },
    //         // });
    //     },
        
    //     afterDeselect: function(value){
    //         // variableIds.push(value[0]);
    //         // var gridId = "{{$grid->id}}";
    //         // console.log(value[0], gridId);
    //         // $.ajax({
    //         //     url : "{{url('grid-optional-variable/delete')}}",
    //         //     type: 'POST',
    //         //     data : {gridId:gridId, optionalVariableId:value[0]},
    //         //     success: function(result){
    //         //         console.log(result);
    //         //     },
    //         //     error: function(errors){
    //         //         console.log(errors);
    //         //     },
    //         // });
    //     }
    // }
    );
</script>

@endsection