@extends('layouts.app')

@section('title', 'Grid')

@section('wrapper1')
<div class="main-wrapper">
    <div class="container">
        <div class="row">
            <div class="col-lg-12">
                <div class="has-bg js-importErrorsAll">
                </div>
                <div class="has-bg">
                    <h3>
                        Grids
                        <a href="{{url('grids/create')}}" class="pull-right">
                            <button class="btn btn-success">Create Grid</button>
                        </a>
                    </h3>
                    <span class="space"></span>
                    <table class="table table-striped table-hover table-bordered text-left MDBootstrapDatatable" cellspacing="0" width="100%">
                        <thead>
                            <tr>
                                <th class="th-sm">Name</th>
                                <th class="th-sm">Created at</th>
                                <th>
                                    <span class="pull-right">Actions</span>
                                </th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach($grids as $key => $grid)
                            <tr>
                                <td>{{$grid->name}}</td>
                                <td>{{\Carbon\Carbon::parse($grid->created_at->date)->format('Y M d')}}</td>
                                <td class="pull-right">
                                    <a href="{{url('datatypes?gridId=').$grid->id}}">
                                        <button class="btn btn-success">View Datatypes</button>
                                    </a>
                                    <a href="{{url('grids/sort-variables').'/'.$grid->id.'?web=true'}}">
                                        <button class="btn btn-success">Sort Datatypes</button>
                                    </a>
                                    <a href="{{url('grids/'.$grid->id.'/view')}}">
                                        <button class="btn btn-success">Assign variables</button>
                                    </a>
                                    <!-- {{--<a href="{{url('grids/'.$grid->id.'/copy')}}">
                                        <button class="btn btn-primary">Copy and Create New</button>
                                    </a>--}} -->
                                    <a href="{{url('grids/'.$grid->id.'/edit')}}">
                                        <button class="btn btn-primary">Edit</button>
                                    </a>
                                    <a href="{{url('grids/'.$grid->id.'/delete')}}">
                                        <button class="btn btn-danger">Delete</button>
                                    </a>
                                </td>
                            </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

@section('content')
    <div class="col-lg-12">
        <div class="has-bg button_holder">
            <div class="left-side-button">

                <button type="button" class="btn btn-primary ml-1 cursor" data-toggle="modal" data-target="#importDataTapesModal">
                  Import Data Tapes
                </button>

            </div>
        </div>
    </div>

    <div class="modal" id="importDataTapesModal" data-backdrop="static">
      <div class="modal-dialog" style="max-width: 1000px">
        <div class="modal-content">
          <div class="modal-body custom_modal">
            <div class="row">
              <div class="col-lg-1">
                <div class="left-side">
                </div>
              </div>
              <div class="col-lg-11">
                
                <form action="{{ url('grids/import-datatapes' )}}" method="post" enctype="multipart/form-data" class="js-importForm">
                  <h4 class="modal-title">Import Data Tapes<button type="button" class="close" data-dismiss="modal">&times;</button></h4>
                  <div class="row">
                    @csrf()
                    <div class="col-6">
                      <div class="form-group">
                        <label for="file">Choose csv/excel file:</label>
                        <input type="file" class="form-control" name="file">
                      </div>
                    </div>
                    <div class="col-lg-12">
                      <p class="text-danger js-importError"></p>
                    </div>
                    <div class="col-6 offset-5">
                      <div class="loader hide"></div>
                    </div> 
                    <div class="col-lg-12">
                      <button type="submit" class="btn pull-right btn-primary custom-padding mt-3 js-importBtn">Import</button>
                    </div>
                  </div>
                </form>

              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
@endsection

@section('pages-js')
    <script>
        $('.js-importForm').on("submit", function(e){
          e.preventDefault();
          $('.js-importBtn').attr('disabled', 'disabled');
          var formData = new FormData(this);
          // console.log(formData);
          $.ajax({
            url : $(this).attr('action'),
            type: $(this).attr('method'),
            beforeSend: function(){
              $('.loader').removeClass('hide');
              $('.js-importError').html('<span class="text-info">Please wait while your file is being processed.</span>');
            },
            complete: function(){$('.loader').addClass('hide');},
            data : formData,
            processData: false,
            contentType: false,
            success: function(result){
              console.log(result);
              if(result.code == 401)
                  $('.js-importError').html('<div class="row js-msgs"><p class="text-danger mb-0">'+result.message+'</p></div>');
              else{
                $('.js-importErrorsAll').html('<div class="row js-msgs"><p class="text-success mb-0">Data tapes uploaded successfully.</p></div>');
                $('#importDataTapesModal').modal('toggle');
                window.scrollTo(0,0);
              }
              $('.js-importBtn').attr('disabled', false);
            },
            error: function(errors){
              $('.js-importError').html('Internal server error.');
              $('.js-importBtn').attr('disabled', false);
            },
          });
        });
    </script>
@endsection