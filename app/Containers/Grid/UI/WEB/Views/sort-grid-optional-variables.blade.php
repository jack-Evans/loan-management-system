@extends('layouts.app')

@section('title', 'Grid Optional Variables')

@section('content')
<style type="text/css">
    #sortable-list      { padding:0; }
    #sortable-list li   { padding:4px 8px; color:#000; cursor:move; list-style:none; width:500px; background:#ddd; margin:10px 0; border:1px solid #999; }
    #message-box        { background:#fffea1; border:2px solid #fc0; padding:4px 8px; margin:0 0 14px 0; width:500px; }
</style>
<div class="col-lg-12">
    <div class="has-bg">
        <div class="row">
            <div class="col-12">
                <h3>
                    Sort Optional Variables Under: {{$grid->name}}
                    <a href="{{url('grids')}}" class="pull-right">
                        <button class="btn btn-success">Back</button>
                    </a>
                </h3>
            </div>
            <div class="clearfix"></div>
            <div class="col-12">
                <p>Drag and drop the elements below and hit sort button to reorder.</p>

                <div id="message-box">
                    <?php // echo $message; ?> 
                    Waiting for sortation submission...
                </div>
                <form id="dd-form" action="" method="post">
                    <p class="hidden">
                        <input type="checkbox" value="1" name="autoSubmit" id="autoSubmit" 
                        <?php // if($_POST['autoSubmit']) { echo 'checked="checked"'; } ?> />
                        <label for="autoSubmit">Automatically submit on drop event</label>
                    </p>

                    <ul id="sortable-list">
                        <?php 
                            $order = array();
                            foreach($gridOptionalVariables as $variable) {
                                echo '<li title="',$variable->id,'">',$variable->optionalVariable,'</li>';
                                $order[] = $variable->id;
                            }
                        ?>
                    </ul>
                    <br />
                    <input type="hidden" name="sort_order" id="sort_order" value="<?php echo implode(',',$order); ?>" />
                    <input type="submit" id="do_submit" name="do_submit" value="Submit Sortation" class="btn btn-primary" style="position: absolute; top: 12px; right: 12px;" />
                </form>
            </div>
        </div>
    </div>
</div>
@endsection

@section('pages-js')
<script src="https://ajax.googleapis.com/ajax/libs/jqueryui/1.10.3/jquery-ui.min.js"></script>
<script>
    // $('.draggable-element').arrangeable();
/* when the DOM is ready */
jQuery(document).ready(function() {
    /* grab important elements */
    var sortInput = jQuery('#sort_order');
    var submit = jQuery('#autoSubmit');
    var messageBox = jQuery('#message-box');
    var list = jQuery('#sortable-list');
    /* create requesting function to avoid duplicate code */
    var request = function() {
        jQuery.ajax({
            beforeSend: function() {
                messageBox.text('Updating the sort order in the database.');
            },
            complete: function() {
                messageBox.text('Database has been updated.');
                $('#do_submit').prop('disabled', false);
            },
            data: 'sortOrder=' + sortInput[0].value, //need [0]?
            type: 'post',
            url: "{{url('grids/sort-variables')}}",
            success: function(result){
                console.log(result);
            },
            error: function(error){
                console.log(error);
            }
        });
    };
    /* worker function */
    var fnSubmit = function(save) {
        var sortOrder = [];
        list.children('li').each(function(){
            sortOrder.push(jQuery(this).data('id'));
        });
        sortInput.val(sortOrder.join(','));
        console.log(sortInput.val());
        if(save) {
            request();
        }
    };
    /* store values */
    list.children('li').each(function() {
        var li = jQuery(this);
        li.data('id',li.attr('title')).attr('title','');
    });
    /* sortables */
    list.sortable({
        opacity: 0.7,
        update: function() {
            fnSubmit(submit[0].checked);
        }
    });
    list.disableSelection();
    /* ajax form submission */
    jQuery('#dd-form').bind('submit',function(e) {
        if(e) e.preventDefault();
        fnSubmit(true);
    });
});
</script>

@endsection