@extends('layouts.app')

@section('title', 'Grid')

@section('content')
<div class="col-lg-12">
    <div class="has-bg">
      <div class="row">
          <div class="col-md-5 col-md-offset-2">
          <h3>{{isset($grid->id) ? 'Edit ' : 'Create '}}Grid</h3>
              <form action="{{url(isset($grid->id) ? 'grids/'.$grid->id.'/update': 'grids/store' )}}" method="post">
                  @csrf()
                  <div class="form-group">
                    <label for="name">Name:</label>
                    <input type="text" class="form-control" name="name"
                           value="{{isset($grid->name) ? $grid->name : old('name')}}">
                  </div>
                  <button type="submit" class="btn btn-primary">Submit</button>
              </form>
          </div>
      </div>
    </div>
</div>
@endsection
