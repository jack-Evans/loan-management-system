<?php

namespace App\Containers\Grid\UI\API\Transformers;

use App\Containers\Grid\Models\Grid;
use App\Ship\Parents\Transformers\Transformer;

class GridTransformer extends Transformer
{
    /**
     * @var  array
     */
    protected $defaultIncludes = [

    ];

    /**
     * @var  array
     */
    protected $availableIncludes = [

    ];

    /**
     * @param Grid $entity
     *
     * @return array
     */
    public function transform(Grid $entity)
    {
        $response = [
            'object' => 'Grid',
            'id' => $entity->getHashedKey(),
            'name' => $entity->name,
            'created_at' => $entity->created_at,
            'updated_at' => $entity->updated_at,

        ];

        $response = $this->ifAdmin([
            'real_id'    => $entity->id,
            // 'deleted_at' => $entity->deleted_at,
        ], $response);

        return $response;
    }
}
