<?php

namespace App\Containers\Grid\UI\API\Transformers;

use App\Containers\Grid\Models\GridOptionalVariable;
use App\Ship\Parents\Transformers\Transformer;

class GridOptionalVariableTransformer extends Transformer
{
    /**
     * @var  array
     */
    protected $defaultIncludes = [

    ];

    /**
     * @var  array
     */
    protected $availableIncludes = [

    ];

    /**
     * @param GridOptionalVariable $entity
     *
     * @return array
     */
    public function transform(GridOptionalVariable $entity)
    {
        // dd($entity);
        $response = [
            'object' => 'Grid',
            'id' => $entity->getHashedKey(),
            'gridName' => $entity->grid->name,
            'optionalVariable' => str_replace(".","",trimedLowerStr($entity->gridOptionalVariable->optional_variable)),
            'mandatory' => $entity->gridOptionalVariable->mandatory,
            'group_name' => !is_null($entity->gridOptionalVariable->group) ? $entity->gridOptionalVariable->group->group_name : null,
            'sort_order' => $entity->sort_order,
            'created_at' => $entity->created_at,
            'updated_at' => $entity->updated_at,

        ];

        $response = $this->ifAdmin([
            'real_id'    => $entity->id,
            // 'deleted_at' => $entity->deleted_at,
        ], $response);

        return $response;
    }
}
