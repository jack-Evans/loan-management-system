<?php

/**
 * @apiGroup           Grid
 * @apiName            getAllGrids
 *
 * @api                {GET} /v1/grids Endpoint title here..
 * @apiDescription     Endpoint description here..
 *
 * @apiVersion         1.0.0
 * @apiPermission      none
 *
 * @apiParam           {String}  parameters here..
 *
 * @apiSuccessExample  {json}  Success-Response:
 * HTTP/1.1 200 OK
{
  // Insert the response of the request here...
}
 */

/** @var Route $router */
$router->get('grids', [
    'as' => 'api_grid_get_all_grids',
    'uses'  => 'Controller@getAllGrids',
    'middleware' => [
      'auth:api',
    ],
]);
