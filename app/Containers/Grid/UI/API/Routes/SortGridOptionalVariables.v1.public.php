<?php

/**
 * @apiGroup           Grid
 * @apiName            getAllGrids
 *
 * @api                {GET} /v1/grids Endpoint title here..
 * @apiDescription     Endpoint description here..
 *
 * @apiVersion         1.0.0
 * @apiPermission      none
 *
 * @apiParam           {String}  parameters here..
 *
 * @apiSuccessExample  {json}  Success-Response:
 * HTTP/1.1 200 OK
{
  // Insert the response of the request here...
}
 */

/** @var Route $router */
$router->post('grid-optional-variables/sort', [
    'as' => 'api_grid_optional_variables',
    'uses'  => 'Controller@sortGridOptionalVariables',
    'middleware' => [
      'auth:api',
    ],
]);
