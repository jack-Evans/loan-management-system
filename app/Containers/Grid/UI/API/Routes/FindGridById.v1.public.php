<?php

/**
 * @apiGroup           Grid
 * @apiName            findGridById
 *
 * @api                {GET} /v1/grids/:id Endpoint title here..
 * @apiDescription     Endpoint description here..
 *
 * @apiVersion         1.0.0
 * @apiPermission      none
 *
 * @apiParam           {String}  parameters here..
 *
 * @apiSuccessExample  {json}  Success-Response:
 * HTTP/1.1 200 OK
{
  // Insert the response of the request here...
}
 */

/** @var Route $router */
$router->get('grids/{id}', [
    'as' => 'api_grid_find_grid_by_id',
    'uses'  => 'Controller@findGridById',
    'middleware' => [
      'auth:api',
    ],
]);
