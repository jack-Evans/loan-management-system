<?php

/**
 * @apiGroup           Grid
 * @apiName            updateGrid
 *
 * @api                {PATCH} /v1/grids/:id Endpoint title here..
 * @apiDescription     Endpoint description here..
 *
 * @apiVersion         1.0.0
 * @apiPermission      none
 *
 * @apiParam           {String}  parameters here..
 *
 * @apiSuccessExample  {json}  Success-Response:
 * HTTP/1.1 200 OK
{
  // Insert the response of the request here...
}
 */

/** @var Route $router */
$router->patch('grids/{id}', [
    'as' => 'api_grid_update_grid',
    'uses'  => 'Controller@updateGrid',
    'middleware' => [
      'auth:api',
    ],
]);
