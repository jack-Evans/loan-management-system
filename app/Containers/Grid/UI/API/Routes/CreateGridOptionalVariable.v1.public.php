<?php

/**
 * @apiGroup           GridOptionalVariables
 * @apiName            createGridOptionalVariables
 *
 * @api                {POST} /v1/grids Endpoint title here..
 * @apiDescription     Endpoint description here..
 *
 * @apiVersion         1.0.0
 * @apiPermission      none
 *
 * @apiParam           {String}  parameters here..
 *
 * @apiSuccessExample  {json}  Success-Response:
 * HTTP/1.1 200 OK
{
  // Insert the response of the request here...
}
 */

/** @var Route $router */
$router->post('grid-optional-variables', [
    'as' => 'api_grid_create_grid_optional_variables',
    'uses'  => 'Controller@createGridOptionalVariables',
    'middleware' => [
      'auth:api',
    ],
]);
