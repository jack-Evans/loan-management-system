<?php

/**
 * @apiGroup           Grid
 * @apiName            deleteGrid
 *
 * @api                {DELETE} /v1/grids/:id Endpoint title here..
 * @apiDescription     Endpoint description here..
 *
 * @apiVersion         1.0.0
 * @apiPermission      none
 *
 * @apiParam           {String}  parameters here..
 *
 * @apiSuccessExample  {json}  Success-Response:
 * HTTP/1.1 200 OK
{
  // Insert the response of the request here...
}
 */

/** @var Route $router */
$router->delete('grids/{id}', [
    'as' => 'api_grid_delete_grid',
    'uses'  => 'Controller@deleteGrid',
    'middleware' => [
      'auth:api',
    ],
]);
