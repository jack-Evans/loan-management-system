<?php

namespace App\Containers\Grid\UI\API\Requests;

use App\Ship\Parents\Requests\Request;

/**
 * Class DeleteGridOptionalVariableRequest.
 */
class DeleteGridOptionalVariableRequest extends Request
{

    /**
     * The assigned Transporter for this Request
     *
     * @var string
     */
    protected $transporter = \App\Containers\Grid\Data\Transporters\CreateGridTransporter::class;

    /**
     * Define which Roles and/or Permissions has access to this request.
     *
     * @var  array
     */
    protected $access = [
        'permissions' => '',
        'roles'       => '',
    ];

    /**
     * Id's that needs decoding before applying the validation rules.
     *
     * @var  array
     */
    protected $decode = [
        'gridId',
        'optionalVariableId',
    ];

    /**
     * Defining the URL parameters (e.g, `/user/{id}`) allows applying
     * validation rules on them and allows accessing them like request data.
     *
     * @var  array
     */
    protected $urlParameters = [
        'gridId',
        'optionalVariableId',
    ];

    /**
     * @return  array
     */
    public function rules()
    {
        return [
            // 'gridId' => 'required',
            // 'optionalVariableId' => 'required',
        ];
    }

    /**
     * @return  bool
     */
    public function authorize()
    {
        return $this->check([
            'hasAccess',
        ]);
    }
}
