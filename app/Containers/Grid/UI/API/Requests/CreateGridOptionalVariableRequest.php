<?php

namespace App\Containers\Grid\UI\API\Requests;

use App\Ship\Parents\Requests\Request;

/**
 * Class CreateGridOptionalVariableRequest.
 */
class CreateGridOptionalVariableRequest extends Request
{

    /**
     * The assigned Transporter for this Request
     *
     * @var string
     */
    protected $transporter = \App\Containers\Grid\Data\Transporters\CreateGridTransporter::class;

    /**
     * Define which Roles and/or Permissions has access to this request.
     *
     * @var  array
     */
    protected $access = [
        'permissions' => '',
        'roles'       => '',
    ];

    /**
     * Id's that needs decoding before applying the validation rules.
     *
     * @var  array
     */
    protected $decode = [
        'gridId',
        'optionalVariableId',
    ];

    /**
     * Defining the URL parameters (e.g, `/user/{id}`) allows applying
     * validation rules on them and allows accessing them like request data.
     *
     * @var  array
     */
    protected $urlParameters = [
        // 'id',
    ];

    /**
     * @return  array
     */
    public function rules()
    {
        return [
            // 'gridId' => 'required',
            'optionalVariableIds' => 'required',
            // 'optionalVariableId' => 'required|unique:grid_optional_variables,optional_variable_id,NULL,grid_id,grid_id,'.$this->gridId,
            // 'gridId' => 'required|unique:grid_optional_variables,grid_id,NULL,optional_variable_id,optional_variable_id,'.$this->optionalVariableId,
            'gridId' => 'required',
            // 'optionalVariableId' => 'required|unique:grid_optional_variables,optional_variable_id,NULL,id,grid_id,' . $this->optionalVariableId,

            // '{user-input}' => 'required|max:255',
        ];
    }

    /**
     * @return  bool
     */
    public function authorize()
    {
        return $this->check([
            'hasAccess',
        ]);
    }
}
