<?php

namespace App\Containers\Grid\UI\API\Controllers;

use App\Containers\Grid\UI\API\Requests\CreateGridRequest;
use App\Containers\Grid\UI\API\Requests\CreateGridOptionalVariableRequest;
use App\Containers\Grid\UI\API\Requests\DeleteGridOptionalVariableRequest;
use App\Containers\Grid\UI\API\Requests\DeleteGridRequest;
use App\Containers\Grid\UI\API\Requests\GetAllGridsRequest;
use App\Containers\Grid\UI\API\Requests\FindGridByIdRequest;
use App\Containers\Grid\UI\API\Requests\UpdateGridRequest;
use App\Containers\Grid\UI\API\Transformers\GridTransformer;
use App\Ship\Parents\Controllers\ApiController;
use Apiato\Core\Foundation\Facades\Apiato;

/**
 * Class Controller
 *
 * @package App\Containers\Grid\UI\API\Controllers
 */
class Controller extends ApiController
{
    /**
     * @param CreateGridRequest $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function createGrid(CreateGridRequest $request)
    {
        $grid = Apiato::call('Grid@CreateGridAction', [$request]);

        return $this->created($this->transform($grid, GridTransformer::class));
    }

    /**
     * @param FindGridByIdRequest $request
     * @return array
     */
    public function findGridById(FindGridByIdRequest $request)
    {
        $grid = Apiato::call('Grid@FindGridByIdAction', [$request]);

        return $this->transform($grid, GridTransformer::class);
    }

    /**
     * @param GetAllGridsRequest $request
     * @return array
     */
    public function getAllGrids(GetAllGridsRequest $request)
    {
        $grids = Apiato::call('Grid@GetAllGridsAction', [$request]);

        return $this->transform($grids, GridTransformer::class);
    }

    /**
     * @param UpdateGridRequest $request
     * @return array
     */
    public function updateGrid(UpdateGridRequest $request)
    {
        $grid = Apiato::call('Grid@UpdateGridAction', [$request]);

        return $this->transform($grid, GridTransformer::class);
    }

    /**
     * @param DeleteGridRequest $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function deleteGrid(DeleteGridRequest $request)
    {
        Apiato::call('Grid@DeleteGridAction', [$request]);

        return $this->noContent();
    }

    /**
     * @param FindGridByIdRequest $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function gridOptionalVariables(FindGridByIdRequest $request)
    {
        $gridOptionalVariables = Apiato::call('Grid@GridOptionalVariableAction', [$request]);

        return $this->transform($gridOptionalVariables, \App\Containers\Grid\UI\API\Transformers\GridOptionalVariableTransformer::class);
    }

    /**
     * @param CreateGridOptionalVariableRequest $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function createGridOptionalVariables(CreateGridOptionalVariableRequest $request)
    {
        $grid = Apiato::call('Grid@CreateGridOptionalVariableAction', [$request]);

        return $this->noContent();
    }
    
    /**
     * @param DeleteGridRequest $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function deleteGridOptionalVariable(DeleteGridOptionalVariableRequest $request)
    {
        Apiato::call('Grid@DeleteGridOptionalVariableAction', [$request]);

        return $this->noContent();
    }

    /**
     * @param SortGridOptionalVariableRequest $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function sortGridOptionalVariables(FindGridByIdRequest $request)
    {
        Apiato::call('Grid@SortGridOptionalVariableAction', [$request]);

        return $this->noContent();
    }
}
