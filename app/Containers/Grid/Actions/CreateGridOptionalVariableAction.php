<?php

namespace App\Containers\Grid\Actions;

use App\Ship\Parents\Actions\Action;
use App\Ship\Parents\Requests\Request;
use Apiato\Core\Foundation\Facades\Apiato;
use App\Containers\Grid\Models\GridOptionalVariable;

class CreateGridOptionalVariableAction extends Action
{
    public function run(Request $request)
    {
        \DB::beginTransaction();
    	$lastGrid = GridOptionalVariable::where('grid_id',$request->gridId)->delete();
		$sortOrder = 1;
        foreach ($request->optionalVariableIds as $key => $optionalVariableId) {
            $data = [
             	'grid_id' => $request->gridId,
             	'optional_variable_id' => \Hashids::decode($optionalVariableId)[0],
             	'sort_order' => $sortOrder++,
            ];

            $grid = Apiato::call('Grid@CreateGridOptionalVariableTask', [$data]);
        }
        \DB::commit();

        return $grid;
    }
}
