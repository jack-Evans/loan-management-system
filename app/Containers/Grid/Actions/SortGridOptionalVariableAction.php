<?php

namespace App\Containers\Grid\Actions;

use App\Ship\Parents\Actions\Action;
use App\Ship\Parents\Requests\Request;
use Apiato\Core\Foundation\Facades\Apiato;
use App\Containers\Grid\Models\GridOptionalVariable;

class SortGridOptionalVariableAction extends Action
{
    public function run(Request $request)
    {
    	$ids = [];

    	foreach($request->sortOrder as $id)
    	{
			$ids[] = \Hashids::decode($id)[0];
    	}
    	$firstVariable = GridOptionalVariable::whereIn('id', $ids)->orderBy('sort_order')->first();
    	$startId = $firstVariable->sort_order;
    	foreach ($ids as $key => $id) {
    		$variable = GridOptionalVariable::where('id', $id)->first();
    		$variable->sort_order = $startId;
    		$variable->save();
    		$startId++;
    	}

        return;
    }
}
