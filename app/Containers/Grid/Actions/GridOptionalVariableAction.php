<?php

namespace App\Containers\Grid\Actions;

use App\Ship\Parents\Actions\Action;
use App\Ship\Parents\Requests\Request;
use Apiato\Core\Foundation\Facades\Apiato;

class GridOptionalVariableAction extends Action
{
    public function run(Request $request)
    {
        $gridOptionalVariables = Apiato::call('Grid@GridOptionalVariableTask', [$request->id]);

        return $gridOptionalVariables;
    }
}
