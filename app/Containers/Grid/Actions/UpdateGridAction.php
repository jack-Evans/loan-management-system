<?php

namespace App\Containers\Grid\Actions;

use App\Ship\Parents\Actions\Action;
use App\Ship\Parents\Requests\Request;
use Apiato\Core\Foundation\Facades\Apiato;

class UpdateGridAction extends Action
{
    public function run(Request $request)
    {
        $data = [
            'name' => $request->name,
        ];

        $grid = Apiato::call('Grid@UpdateGridTask', [$request->id, $data]);

        return $grid;
    }
}
