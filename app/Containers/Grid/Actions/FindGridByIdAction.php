<?php

namespace App\Containers\Grid\Actions;

use App\Ship\Parents\Actions\Action;
use App\Ship\Parents\Requests\Request;
use Apiato\Core\Foundation\Facades\Apiato;

class FindGridByIdAction extends Action
{
    public function run(Request $request)
    {
        $grid = Apiato::call('Grid@FindGridByIdTask', [$request->id]);

        return $grid;
    }
}
