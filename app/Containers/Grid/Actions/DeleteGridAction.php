<?php

namespace App\Containers\Grid\Actions;

use App\Ship\Parents\Actions\Action;
use App\Ship\Parents\Requests\Request;
use Apiato\Core\Foundation\Facades\Apiato;

class DeleteGridAction extends Action
{
    public function run(Request $request)
    {
        return Apiato::call('Grid@DeleteGridTask', [$request->id]);
    }
}
