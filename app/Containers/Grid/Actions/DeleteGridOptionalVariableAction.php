<?php

namespace App\Containers\Grid\Actions;

use App\Ship\Parents\Actions\Action;
use App\Ship\Parents\Requests\Request;
use Apiato\Core\Foundation\Facades\Apiato;
use App\Containers\Grid\Models\GridOptionalVariable;

class DeleteGridOptionalVariableAction extends Action
{
    public function run(Request $request)
    {
        $data = [
         	'grid_id' => $request->gridId,
         	'optional_variable_id' => $request->optionalVariableId,
        ];
        $grid = GridOptionalVariable::where('grid_id', $request->gridId)->where('optional_variable_id', $request->optionalVariableId)->delete();

        return $grid;
    }
}
