<?php

namespace App\Containers\Grid\Actions;

use App\Ship\Parents\Actions\Action;
use App\Ship\Parents\Requests\Request;
use Apiato\Core\Foundation\Facades\Apiato;

class GetAllGridsAction extends Action
{
    public function run(Request $request)
    {
        $grids = Apiato::call('Grid@GetAllGridsTask', [], ['addRequestCriteria']);

        return $grids;
    }
}
