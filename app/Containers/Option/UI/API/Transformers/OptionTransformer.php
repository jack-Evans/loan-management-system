<?php

namespace App\Containers\Option\UI\API\Transformers;

use App\Containers\Option\Models\Option;
use App\Ship\Parents\Transformers\Transformer;

class OptionTransformer extends Transformer
{
    /**
     * @var  array
     */
    protected $defaultIncludes = [

    ];

    /**
     * @var  array
     */
    protected $availableIncludes = [

    ];

    /**
     * @param Option $entity
     *
     * @return array
     */
    public function transform(Option $entity)
    {
        $response = [
            'object' => 'Option',
            'id' => $entity->getHashedKey(),
            'name' => $entity->name,
            'value' => $entity->value,
            'loan_id' => $entity->loan->getHashedKey(),
            'mapped_id' => (isset($entity->mapped_id)) ? $entity->mapped_id : null,
            'create_date' => $entity->create_date,
            'created_at' => $entity->created_at,
            'updated_at' => $entity->updated_at,

        ];

        $response = $this->ifAdmin([
            'real_id'    => $entity->id,
            // 'deleted_at' => $entity->deleted_at,
        ], $response);

        return $response;
    }
}
