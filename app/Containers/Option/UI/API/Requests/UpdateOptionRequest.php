<?php

namespace App\Containers\Option\UI\API\Requests;

use App\Ship\Parents\Requests\Request;

/**
 * Class UpdateOptionRequest.
 */
class UpdateOptionRequest extends Request
{

    /**
     * The assigned Transporter for this Request
     *
     * @var string
     */
    protected $transporter = \App\Containers\Option\Data\Transporters\UpdateOptionTransporter::class;

    /**
     * Define which Roles and/or Permissions has access to this request.
     *
     * @var  array
     */
    protected $access = [
        'permissions' => '',
        'roles'       => '',
    ];

    /**
     * Id's that needs decoding before applying the validation rules.
     *
     * @var  array
     */
    protected $decode = [
        'id',
        'loanid',
    ];

    /**
     * Defining the URL parameters (e.g, `/user/{id}`) allows applying
     * validation rules on them and allows accessing them like request data.
     *
     * @var  array
     */
    protected $urlParameters = [
        'id',
    ];

    /**
     * @return  array
     */
    public function rules()
    {
        return [
            'id'        => 'required|integer|exists:options,id',
            'loanid'    => 'required|integer|exists:loans,id',
            // 'name' => 'required|unique:options,name,NULL,id,loan_id,'.$this->loanid,
            // 'name'      => 'unique:options,name,NULL,id,loan_id,'.$this->loanid,
            // '{user-input}' => 'required|max:255',
        ];
    }

    /**
     * @return  bool
     */
    public function authorize()
    {
        return $this->check([
            'hasAccess',
        ]);
    }
}
