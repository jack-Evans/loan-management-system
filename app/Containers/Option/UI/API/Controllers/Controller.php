<?php

namespace App\Containers\Option\UI\API\Controllers;

use App\Containers\Option\UI\API\Requests\CreateOptionRequest;
use App\Containers\Option\UI\API\Requests\DeleteOptionRequest;
use App\Containers\Option\UI\API\Requests\GetAllOptionsRequest;
use App\Containers\Option\UI\API\Requests\FindOptionByIdRequest;
use App\Containers\Option\UI\API\Requests\UpdateOptionRequest;
use App\Containers\Option\UI\API\Requests\UpdateOptionByLoanIdRequest;
use App\Containers\Option\UI\API\Requests\SearchOptionsByNameRequest;
use App\Containers\Option\UI\API\Transformers\OptionTransformer;
use App\Ship\Parents\Controllers\ApiController;
use Apiato\Core\Foundation\Facades\Apiato;
use App\Containers\Option\Models\Option;

/**
 * Class Controller
 *
 * @package App\Containers\Option\UI\API\Controllers
 */
class Controller extends ApiController
{
    /**
     * @param CreateOptionRequest $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function createOption(CreateOptionRequest $request)
    {
        $option = Apiato::call('Option@CreateOptionAction', [$request]);

        return $this->created($this->transform($option, OptionTransformer::class));
    }

    /**
     * @param FindOptionByIdRequest $request
     * @return array
     */
    public function findOptionById(FindOptionByIdRequest $request)
    {
        $option = Apiato::call('Option@FindOptionByIdAction', [$request]);

        return $this->transform($option, OptionTransformer::class);
    }

    /**
     * @param GetAllOptionsRequest $request
     * @return array
     */
    public function getAllOptions(GetAllOptionsRequest $request)
    {
        $options = Apiato::call('Option@GetAllOptionsAction', [$request]);

        return $this->transform($options, OptionTransformer::class);
    }

    /**
     * @param GetAllOptionsRequest $request
     * @return array
     */
    public function searchOptions(SearchOptionsByNameRequest $request)
    {
        $options = Apiato::call('Option@SearchOptionsAction', [$request]);

        return $this->transform($options, OptionTransformer::class);
    }

    /**
     * @param UpdateOptionRequest $request
     * @return array
     */
    public function updateOption(UpdateOptionRequest $request)
    {
        $option = Apiato::call('Option@UpdateOptionAction', [$request]);

        return $this->transform($option, OptionTransformer::class);
    }

    /**
     * @param UpdateOptionByLoanIdRequest $request
     * @return array
     */
    public function updateOptionByLoanId(UpdateOptionByLoanIdRequest $request)
    {
        $option = Option::where('loan_id', $request->loanId)->where('name', $request->name)->first();
        if($option){
            $request->id = $option->id;
            $option = Apiato::call('Option@UpdateOptionAction', [$request]);
        } else{
            $request->loanid = $request->loanId;
            $request->name = $request->name;
            $option = Apiato::call('Option@CreateOptionAction', [$request]);
        }
        
        return $this->transform($option, OptionTransformer::class);
    }

    /**
     * @param DeleteOptionRequest $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function deleteOption(DeleteOptionRequest $request)
    {
        Apiato::call('Option@DeleteOptionAction', [$request]);

        return $this->noContent();
    }

    /**
     * @param DeleteOptionRequest $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function loanOptions(\App\Containers\Loan\UI\API\Requests\FindLoanByIdRequest $request)
    {
        $options = Apiato::call('Option@LoanOptionAction', [$request]);

        return $this->transform($options, OptionTransformer::class);
    }

    /**
     * @param ImportOptionalVariableRequest $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function importOptions(\App\Containers\Optionalvariable\UI\API\Requests\ImportOptionalVariableRequest $request)
    {
        $options = Apiato::call('Option@ImportOptionsAction', [$request]);

        return $options;
    }
}
