<?php

/**
 * @apiGroup           Option
 * @apiName            updateOption
 *
 * @api                {PATCH} /v1/options/:id Endpoint title here..
 * @apiDescription     Endpoint description here..
 *
 * @apiVersion         1.0.0
 * @apiPermission      none
 *
 * @apiParam           {String}  parameters here..
 *
 * @apiSuccessExample  {json}  Success-Response:
 * HTTP/1.1 200 OK
{
  // Insert the response of the request here...
}
 */

/** @var Route $router */
$router->patch('options/{id}', [
    'as' => 'api_option_update_option',
    'uses'  => 'Controller@updateOption',
    'middleware' => [
      'auth:api',
    ],
]);
