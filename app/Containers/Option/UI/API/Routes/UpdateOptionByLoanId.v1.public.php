<?php

/** @var Route $router */
$router->patch('update-option-by-loanid', [
    'as' => 'api_option_update_option_by_loanid',
    'uses'  => 'Controller@updateOptionByLoanId',
    'middleware' => [
      'auth:api',
    ],
]);
