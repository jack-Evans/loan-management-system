<?php

/**
 * @apiGroup           SearchOption
 * @apiName            searchOptions
 *
 * @api                {GET} /v1/options Endpoint title here..
 * @apiDescription     Endpoint description here..
 *
 * @apiVersion         1.0.0
 * @apiPermission      none
 *
 * @apiParam           {String}  parameters here..
 *
 * @apiSuccessExample  {json}  Success-Response:
 * HTTP/1.1 200 OK
{
  // Insert the response of the request here...
}
 */

/** @var Route $router */
$router->post('options/search', [
    'as' => 'api_option_get_filtered_options',
    'uses'  => 'Controller@searchOptions',
    'middleware' => [
      'auth:api',
    ],
]);
