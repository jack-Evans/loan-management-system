<?php

/** @var Route $router */
$router->post('options/import', [
    'as' => 'api_optionalvariable__import',
    'uses'  => 'Controller@importOptions',
    'middleware' => [
      'auth:api',
    ],
]);

?>