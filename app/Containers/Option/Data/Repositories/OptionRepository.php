<?php

namespace App\Containers\Option\Data\Repositories;

use App\Ship\Parents\Repositories\Repository;

/**
 * Class OptionRepository
 */
class OptionRepository extends Repository
{

    /**
     * @var array
     */
    protected $fieldSearchable = [
        'id' => '=',
        // ...
    ];

}
