<?php

namespace App\Containers\Option\Models;

use App\Ship\Parents\Models\Model;

class Option extends Model
{
    protected $fillable = [
        'loan_id',
        'name',
        'value',
        'create_date',
    ];

    protected $attributes = [

    ];
    
    protected $appends = ['hashid'];
    
    protected $hidden = [

    ];

    protected $casts = [

    ];

    protected $dates = [
        'create_date',
        'created_at',
        'updated_at',
    ];

    /**
     * A resource key to be used by the the JSON API Serializer responses.
     */
    protected $resourceKey = 'options';

    public function loan() {
        return $this->BelongsTo(\App\Containers\Loan\Models\Loan::class, 'loan_id');
    }

    public function getHashidAttribute()
    {
        return $this->encode($this->attributes['id']);
    }
}
