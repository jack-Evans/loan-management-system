<?php

namespace App\Containers\Option\Actions;

use App\Ship\Parents\Actions\Action;
use App\Ship\Parents\Requests\Request;
use Apiato\Core\Foundation\Facades\Apiato;
use App\Containers\Optionalvariable\Models\Optionalvariable;
use App\Containers\Option\Models\Option;

class UpdateOptionAction extends Action
{
    public function run(Request $request)
    {
    	$data = [];
        if(isset($request->name))
        	$data['name'] = $request->name;
        if(isset($request->value))
        	$data['value'] = $request->value;
        if(isset($request->createDate))
        	$data['create_date'] = $request->createDate;
        \DB::beginTransaction();
        $option = Apiato::call('Option@UpdateOptionTask', [$request->id, $data]);
        
        $optionalVariable = Optionalvariable::where('optional_variable', $option->name)->first();
        // if(isset($request->mappedId) && isset($request->value)){
        if($optionalVariable && $optionalVariable->mapped == 1){
            $optionalVariables = Optionalvariable::where('mapped_id', $optionalVariable->mapped_id)->where('optional_variable', '!=', $option->name)->get();
            foreach ($optionalVariables as $key => $variable) {
                $data = [];
                $option = Option::where('name', $variable->optional_variable)->where('loan_id', $option->loan_id)->first();
                if($option){
                    $data['value'] = $request->value;
                    $request->id = $option->id;
                    $option = Apiato::call('Option@UpdateOptionTask', [$request->id, $data]);
                    // var_dump($option->name, $option->value);
                }
            }
        }
        // dd($request->mappedId,$data);
        \DB::commit();

        return $option;
    }
}
