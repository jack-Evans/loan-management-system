<?php

namespace App\Containers\Option\Actions;

use App\Ship\Parents\Actions\Action;
use App\Ship\Parents\Requests\Request;
use Apiato\Core\Foundation\Facades\Apiato;
use App\Containers\Option\Models\Option;
use App\Containers\Optionalvariable\Models\Optionalvariable;

class LoanOptionAction extends Action
{
    public function run(Request $request)
    {
        $options = Option::where('loan_id',$request->id)->get();
		foreach ($options as $key => $option) {

			$optionalVariable = Optionalvariable::where('optional_variable', 'LIKE', "{$option->name}")->first();

			if($optionalVariable)
				$options[$key]['mapped_id'] = $optionalVariable->mapped_id;
			else
				$options[$key]['mapped_id'] = null;
		}

        return $options;
    }
}
