<?php

namespace App\Containers\Option\Actions;

use App\Ship\Parents\Actions\Action;
use App\Ship\Parents\Requests\Request;
use Apiato\Core\Foundation\Facades\Apiato;
use App\Containers\Payment\Exceptions\HTTPPreConditionFailedException;
use App\Containers\Optionalvariable\Models\Optionalvariable;
// excel libraries
use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\Worksheet\Worksheet;
use PhpOffice\PhpSpreadsheet\Reader\Csv;
use PhpOffice\PhpSpreadsheet\Reader\Xlsx;
use Illuminate\Support\Facades\Storage;
use App\Containers\Option\Models\Option;
use App\Containers\Customer\Models\Customer;
use App\Containers\Loan\Models\Loan;

class ImportOptionsAction extends Action
{
  public function run(Request $request)
  {
  	$excel = new \App\Classes\Excel();
		$spreadsheet = $excel->initializeExcel($request);
		if($spreadsheet != false){
		    // $isImport = $request->isImport;
		    $file = $request->file('file');
				// $path = Storage::putFileAs(
				//     'public/excel-imports', $request->file('file'), $_FILES['file']['name']
				// );
				$sheetData = $spreadsheet->getSheet(0)->toArray();
				array_splice($sheetData, 0, 5);
				// dd($sheetData);
				\DB::beginTransaction();
				$optVariables = array_map('trim', removeExtraSpaces($sheetData[0]));
				$optVariables = array_map('strtolower', $optVariables);
				array_splice($sheetData, 0, 1);
				$count = count($sheetData);
				$k = 0;
				$customer = Customer::get()->first();
				$request->customerId = (is_null($customer)) ? 1 : $customer->id;

				do{
					$currentVariable = $sheetData[$k];
					$k++;
					$loanType6 = trimedLowerStr($currentVariable[6]);
					// $borrowerNameDt = $currentVariable[7];
					// $loanType7 = trimedLowerStr($currentVariable[7]);
					// if(strcmp($loanType6, 'open') != 0 && strcmp($loanType7, 'open') != 0)
					// 	continue;
					// var_dump($loanType6);
					$request->loanid = null;
					if(empty($currentVariable[2]) && empty($loanType6))
						continue;
					if(!empty($currentVariable[2]))
						$option = Option::where('name', 'sage ref_dt')->where('value', $currentVariable[2])->first();
					else
						$option = null;
					// $loan = Loan::where('loan_id', $currentVariable[6])->first();
					// dd(is_null($option), is_null($option2));
					if(is_null($option)) {
						// $option = Option::where('name', 'borrower name_dt')->where('value', $currentVariable[6])->first();
						$loan = Loan::where('loan_id', $currentVariable[6])->first();
						if(!is_null($loan))
							$request->loanid = $loan->id;
						else{
							$request->loanId = $currentVariable[6];
							$loan = Loan::where('loan_id', $request->loanId)->first();
							if(is_null($loan)){
			          $request->customerId;
			          $request->netloan = 0;
			          $request->loanType = 'manual';
			          $request->holidayRefund = 0;
			          $request->issueAt = \Carbon\Carbon::now()->format('Y-m-d');
			          $loan = Apiato::call("Loan@CreateLoanAction", [$request]);
		          }
	          	$request->loanid = $loan->id;
	          }
					}else{
						$request->loanid = $option->loan_id;
					}
					foreach ($optVariables as $key => $variable) {
						$request->name = $request->value = $request->id = null;
						if(preg_match('/_dt_ext$/', $variable) || preg_match('/_dt_bridging$/', $variable))
							$request->name = $variable;
						else
							$request->name = $variable.'_dt';
						$request->value = $currentVariable[$key];
						
						$currentOption = Option::where('name', $request->name)->where('loan_id', $request->loanid)->first();
						if(is_null($currentOption)){
							Apiato::call('Option@CreateOptionAction', [$request, false]);
						} else{
							$request->id = $currentOption->id;
							Apiato::call('Option@UpdateOptionAction', [$request]);
						}
						$currentOption = null;
					}

					$loan = $option = null;
					
				} while($count > $k);
					// dd('sdf');
				\DB::commit();
    } else{
			throw new HTTPPreConditionFailedException("Please upload right file of type csv/xlsx.");
		}

		return $k;
  }

}

?>