<?php

namespace App\Containers\Option\Actions;

use App\Ship\Parents\Actions\Action;
use App\Ship\Parents\Requests\Request;
use Apiato\Core\Foundation\Facades\Apiato;
use App\Containers\Option\Models\Option;

class SearchOptionsAction extends Action
{
    public function run(Request $request)
    {
    	// \DB::enableQueryLog();
	    $query = Option::query();
	    if($request->name)
	    	$query->where('name', 'like', "%".$request->name."%");
	    if($request->value)
	    	$query->orWhere('value', 'like', "%".$request->value."%");
	    if($request->loanid)
	    	$query->where('loan_id', $request->loanid);

	    $options = $query->get();
        return $options;
    }
}
