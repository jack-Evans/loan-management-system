<?php

namespace App\Containers\Option\Actions;

use App\Ship\Parents\Actions\Action;
use App\Ship\Parents\Requests\Request;
use Apiato\Core\Foundation\Facades\Apiato;
use App\Containers\Optionalvariable\Models\Optionalvariable;

class CreateOptionAction extends Action
{
    public function run(Request $request, $addOptionalVariable = true)
    {
        $request->name = strtolower(removeExtraSpaces($request->name));
        $data = [
        	'loan_id' => $request->loanid,
         	'name' => $request->name,
         	'value' => $request->value,
         	'create_date' => $request->createDate,
        ];

        \DB::beginTransaction();

        if($addOptionalVariable){
            $optionalVariable = Optionalvariable::where('optional_variable', 'LIKE', "{$request->name}")->first();
            if(is_null($optionalVariable)){
                $request->mappedId = null;
                $request->optionalVariable = $request->name;
                $request->mandatory = $request->mandatory;
                $optionalvariable = Apiato::call('Optionalvariable@CreateOptionalvariableAction', [$request]);
            }
        }

        $option = Apiato::call('Option@CreateOptionTask', [$data]);

        \DB::commit();

        return $option;
    }
}
