<?php

namespace App\Containers\Grace\Models;

use App\Ship\Parents\Models\Model;

class Grace extends Model
{
    protected $fillable = [
        'id',
        'loan_id',
        'days',
        'start_date',
        'end_date',
        'description',
    ];

    protected $attributes = [

    ];

    protected $hidden = [

    ];

    protected $casts = [

    ];

    protected $dates = [
        'start_date',
        'end_date',
        'created_at',
        'updated_at',
    ];

    /**
     * A resource key to be used by the the JSON API Serializer responses.
     */
    protected $resourceKey = 'graces';

    public function loan() {
        return $this->belongsTo(\App\Containers\Loan\Models\Loan::class, 'loan_id');
    }
}
