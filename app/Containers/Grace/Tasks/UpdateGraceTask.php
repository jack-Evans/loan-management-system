<?php

namespace App\Containers\Grace\Tasks;

use App\Containers\Grace\Data\Repositories\GraceRepository;
use App\Ship\Exceptions\UpdateResourceFailedException;
use App\Ship\Parents\Tasks\Task;
use Exception;

class UpdateGraceTask extends Task
{

    protected $repository;

    public function __construct(GraceRepository $repository)
    {
        $this->repository = $repository;
    }

    public function run($id, array $data)
    {
        try {
            return $this->repository->update($data, $id);
        }
        catch (Exception $exception) {
            throw new UpdateResourceFailedException();
        }
    }
}
