<?php

namespace App\Containers\Grace\Tasks;

use App\Containers\Grace\Data\Repositories\GraceRepository;
use App\Ship\Exceptions\DeleteResourceFailedException;
use App\Ship\Parents\Tasks\Task;
use Exception;

class DeleteGraceTask extends Task
{

    protected $repository;

    public function __construct(GraceRepository $repository)
    {
        $this->repository = $repository;
    }

    public function run($id)
    {
        try {
            return $this->repository->delete($id);
        }
        catch (Exception $exception) {
            throw new DeleteResourceFailedException();
        }
    }
}
