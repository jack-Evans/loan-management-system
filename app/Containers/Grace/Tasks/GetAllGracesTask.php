<?php

namespace App\Containers\Grace\Tasks;

use App\Containers\Grace\Data\Repositories\GraceRepository;
use App\Ship\Parents\Tasks\Task;

class GetAllGracesTask extends Task
{

    protected $repository;

    public function __construct(GraceRepository $repository)
    {
        $this->repository = $repository;
    }

    public function run()
    {
        // return $this->repository->paginate();
        return $this->repository->all();
    }
}
