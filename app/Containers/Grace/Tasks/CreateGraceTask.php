<?php

namespace App\Containers\Grace\Tasks;

use App\Containers\Grace\Data\Repositories\GraceRepository;
use App\Ship\Exceptions\CreateResourceFailedException;
use App\Ship\Parents\Tasks\Task;
use Exception;

class CreateGraceTask extends Task
{

    protected $repository;

    public function __construct(GraceRepository $repository)
    {
        $this->repository = $repository;
    }

    public function run(array $data)
    {
        try {
            return $this->repository->create($data);
        }
        catch (Exception $exception) {
            throw new CreateResourceFailedException();
        }
    }
}
