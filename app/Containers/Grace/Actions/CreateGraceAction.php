<?php

namespace App\Containers\Grace\Actions;

use App\Ship\Parents\Actions\Action;
use App\Ship\Parents\Requests\Request;
use Apiato\Core\Foundation\Facades\Apiato;
use App\Containers\Grace\Models\Grace;
use Carbon\Carbon;

class CreateGraceAction extends Action
{
    public function run(Request $request)
    {
		$graceData = [
			'loan_id' => $request->loanId,
			'days' => $request->days,
			'start_date' => $request->startDate,
			'end_date' => $request->endDate,
			'description' => $request->description,
		];

        $previousGrace = Grace::where('loan_id', $request->loanId)->where('end_date', '>=', $request->startDate)->get()->last();
        if($previousGrace){
            throw new \App\Containers\Payment\Exceptions\HTTPPreConditionFailedException("Start date should be greater than previous grace ending date.(".$previousGrace->end_date->format('Y-m-d').")");
        }

        if(Carbon::parse($request->startDate)->gte(Carbon::parse($request->endDate)))
            throw new \App\Containers\Payment\Exceptions\HTTPPreConditionFailedException("End date ($request->endDate) should be greater than start date.($request->startDate)");
        
        $grace = Apiato::call('Grace@CreateGraceTask', [$graceData]);

        return $grace;
    }
}
