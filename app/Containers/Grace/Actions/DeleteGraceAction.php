<?php

namespace App\Containers\Grace\Actions;

use App\Ship\Parents\Actions\Action;
use App\Ship\Parents\Requests\Request;
use Apiato\Core\Foundation\Facades\Apiato;

class DeleteGraceAction extends Action
{
    public function run(Request $request)
    {
        return Apiato::call('Grace@DeleteGraceTask', [$request->id]);
    }
}
