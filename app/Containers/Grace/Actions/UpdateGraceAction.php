<?php

namespace App\Containers\Grace\Actions;

use App\Ship\Parents\Actions\Action;
use App\Ship\Parents\Requests\Request;
use Apiato\Core\Foundation\Facades\Apiato;

class UpdateGraceAction extends Action
{
    public function run(Request $request)
    {
        $data = $request->sanitizeInput([
            // add your request data here
        ]);

        $grace = Apiato::call('Grace@UpdateGraceTask', [$request->id, $data]);

        return $grace;
    }
}
