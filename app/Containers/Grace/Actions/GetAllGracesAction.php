<?php

namespace App\Containers\Grace\Actions;

use App\Ship\Parents\Actions\Action;
use App\Ship\Parents\Requests\Request;
use Apiato\Core\Foundation\Facades\Apiato;

class GetAllGracesAction extends Action
{
    public function run(Request $request)
    {
        $graces = Apiato::call('Grace@GetAllGracesTask', [], ['addRequestCriteria']);

        return $graces;
    }
}
