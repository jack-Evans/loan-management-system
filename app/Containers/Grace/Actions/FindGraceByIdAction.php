<?php

namespace App\Containers\Grace\Actions;

use App\Ship\Parents\Actions\Action;
use App\Ship\Parents\Requests\Request;
use Apiato\Core\Foundation\Facades\Apiato;

class FindGraceByIdAction extends Action
{
    public function run(Request $request)
    {
        $grace = Apiato::call('Grace@FindGraceByIdTask', [$request->id]);

        return $grace;
    }
}
