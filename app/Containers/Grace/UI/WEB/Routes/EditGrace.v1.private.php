<?php

/** @var Route $router */
$router->get('graces/{id}/edit', [
    'as' => 'web_grace_edit',
    'uses'  => 'Controller@edit',
    'middleware' => [
      'auth:web',
    ],
]);
