<?php

/** @var Route $router */
$router->get('graces', [
    'as' => 'web_grace_index',
    'uses'  => 'Controller@index',
    'middleware' => [
      'auth:web',
    ],
]);
