<?php

/** @var Route $router */
$router->get('graces/{id}', [
    'as' => 'web_grace_show',
    'uses'  => 'Controller@show',
    'middleware' => [
      'auth:web',
    ],
]);
