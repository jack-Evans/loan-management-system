<?php

/** @var Route $router */
$router->get('graces/create', [
    'as' => 'web_grace_create',
    'uses'  => 'Controller@create',
    'middleware' => [
      'auth:web',
    ],
]);
