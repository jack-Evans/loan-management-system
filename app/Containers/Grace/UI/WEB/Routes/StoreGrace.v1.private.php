<?php

/** @var Route $router */
$router->post('graces/store', [
    'as' => 'web_grace_store',
    'uses'  => 'Controller@store',
    'middleware' => [
      'auth:web',
    ],
]);
