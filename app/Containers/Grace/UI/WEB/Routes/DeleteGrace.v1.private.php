<?php

/** @var Route $router */
$router->delete('graces/{id}', [
    'as' => 'web_grace_delete',
    'uses'  => 'Controller@delete',
    'middleware' => [
      'auth:web',
    ],
]);
