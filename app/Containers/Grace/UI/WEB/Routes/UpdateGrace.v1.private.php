<?php

/** @var Route $router */
$router->patch('graces/{id}', [
    'as' => 'web_grace_update',
    'uses'  => 'Controller@update',
    'middleware' => [
      'auth:web',
    ],
]);
