<?php

namespace App\Containers\Grace\UI\WEB\Controllers;

use App\Containers\Grace\UI\WEB\Requests\CreateGraceRequest;
use App\Containers\Grace\UI\WEB\Requests\DeleteGraceRequest;
use App\Containers\Grace\UI\WEB\Requests\GetAllGracesRequest;
use App\Containers\Grace\UI\WEB\Requests\FindGraceByIdRequest;
use App\Containers\Grace\UI\WEB\Requests\UpdateGraceRequest;
use App\Containers\Grace\UI\WEB\Requests\StoreGraceRequest;
use App\Containers\Grace\UI\WEB\Requests\EditGraceRequest;
use App\Ship\Parents\Controllers\WebController;
use Apiato\Core\Foundation\Facades\Apiato;

/**
 * Class Controller
 *
 * @package App\Containers\Grace\UI\WEB\Controllers
 */
class Controller extends WebController
{
    /**
     * Show all entities
     *
     * @param GetAllGracesRequest $request
     */
    public function index(GetAllGracesRequest $request)
    {
        $graces = Apiato::call('Grace@GetAllGracesAction', [$request]);

        // ..
    }

    /**
     * Show one entity
     *
     * @param FindGraceByIdRequest $request
     */
    public function show(FindGraceByIdRequest $request)
    {
        $grace = Apiato::call('Grace@FindGraceByIdAction', [$request]);

        // ..
    }

    /**
     * Create entity (show UI)
     *
     * @param CreateGraceRequest $request
     */
    public function create(CreateGraceRequest $request)
    {
        // ..
    }

    /**
     * Add a new entity
     *
     * @param StoreGraceRequest $request
     */
    public function store(StoreGraceRequest $request)
    {
        $grace = Apiato::call('Grace@CreateGraceAction', [$request]);

        // ..
    }

    /**
     * Edit entity (show UI)
     *
     * @param EditGraceRequest $request
     */
    public function edit(EditGraceRequest $request)
    {
        $grace = Apiato::call('Grace@GetGraceByIdAction', [$request]);

        // ..
    }

    /**
     * Update a given entity
     *
     * @param UpdateGraceRequest $request
     */
    public function update(UpdateGraceRequest $request)
    {
        $grace = Apiato::call('Grace@UpdateGraceAction', [$request]);

        // ..
    }

    /**
     * Delete a given entity
     *
     * @param DeleteGraceRequest $request
     */
    public function delete(DeleteGraceRequest $request)
    {
         $result = Apiato::call('Grace@DeleteGraceAction', [$request]);

         // ..
    }
}
