<?php

namespace App\Containers\Grace\UI\API\Controllers;

use App\Containers\Grace\UI\API\Requests\CreateGraceRequest;
use App\Containers\Grace\UI\API\Requests\DeleteGraceRequest;
use App\Containers\Grace\UI\API\Requests\GetAllGracesRequest;
use App\Containers\Grace\UI\API\Requests\FindGraceByIdRequest;
use App\Containers\Grace\UI\API\Requests\UpdateGraceRequest;
use App\Containers\Grace\UI\API\Transformers\GraceTransformer;
use App\Ship\Parents\Controllers\ApiController;
use Apiato\Core\Foundation\Facades\Apiato;

/**
 * Class Controller
 *
 * @package App\Containers\Grace\UI\API\Controllers
 */
class Controller extends ApiController
{
    /**
     * @param CreateGraceRequest $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function createGrace(CreateGraceRequest $request)
    {
        $grace = Apiato::call('Grace@CreateGraceAction', [$request]);

        return $this->created($this->transform($grace, GraceTransformer::class));
    }

    /**
     * @param FindGraceByIdRequest $request
     * @return array
     */
    public function findGraceById(FindGraceByIdRequest $request)
    {
        $grace = Apiato::call('Grace@FindGraceByIdAction', [$request]);

        return $this->transform($grace, GraceTransformer::class);
    }

    /**
     * @param GetAllGracesRequest $request
     * @return array
     */
    public function getAllGraces(GetAllGracesRequest $request)
    {
        $graces = Apiato::call('Grace@GetAllGracesAction', [$request]);

        return $this->transform($graces, GraceTransformer::class);
    }

    /**
     * @param UpdateGraceRequest $request
     * @return array
     */
    public function updateGrace(UpdateGraceRequest $request)
    {
        $grace = Apiato::call('Grace@UpdateGraceAction', [$request]);

        return $this->transform($grace, GraceTransformer::class);
    }

    /**
     * @param DeleteGraceRequest $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function deleteGrace(DeleteGraceRequest $request)
    {
        Apiato::call('Grace@DeleteGraceAction', [$request]);

        return $this->noContent();
    }
}
