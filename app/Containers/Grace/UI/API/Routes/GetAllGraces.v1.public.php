<?php

/**
 * @apiGroup           Grace
 * @apiName            getAllGraces
 *
 * @api                {GET} /v1/graces Endpoint title here..
 * @apiDescription     Endpoint description here..
 *
 * @apiVersion         1.0.0
 * @apiPermission      none
 *
 * @apiParam           {String}  parameters here..
 *
 * @apiSuccessExample  {json}  Success-Response:
 * HTTP/1.1 200 OK
{
  // Insert the response of the request here...
}
 */

/** @var Route $router */
$router->get('graces', [
    'as' => 'api_grace_get_all_graces',
    'uses'  => 'Controller@getAllGraces',
    'middleware' => [
      'auth:api',
    ],
]);
