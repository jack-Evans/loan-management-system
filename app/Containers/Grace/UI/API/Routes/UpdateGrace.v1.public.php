<?php

/**
 * @apiGroup           Grace
 * @apiName            updateGrace
 *
 * @api                {PATCH} /v1/graces/:id Endpoint title here..
 * @apiDescription     Endpoint description here..
 *
 * @apiVersion         1.0.0
 * @apiPermission      none
 *
 * @apiParam           {String}  parameters here..
 *
 * @apiSuccessExample  {json}  Success-Response:
 * HTTP/1.1 200 OK
{
  // Insert the response of the request here...
}
 */

/** @var Route $router */
$router->patch('graces/{id}', [
    'as' => 'api_grace_update_grace',
    'uses'  => 'Controller@updateGrace',
    'middleware' => [
      'auth:api',
    ],
]);
