<?php

/**
 * @apiGroup           Grace
 * @apiName            deleteGrace
 *
 * @api                {DELETE} /v1/graces/:id Endpoint title here..
 * @apiDescription     Endpoint description here..
 *
 * @apiVersion         1.0.0
 * @apiPermission      none
 *
 * @apiParam           {String}  parameters here..
 *
 * @apiSuccessExample  {json}  Success-Response:
 * HTTP/1.1 200 OK
{
  // Insert the response of the request here...
}
 */

/** @var Route $router */
$router->delete('graces/{id}', [
    'as' => 'api_grace_delete_grace',
    'uses'  => 'Controller@deleteGrace',
    'middleware' => [
      'auth:api',
    ],
]);
