<?php

namespace App\Containers\Grace\UI\API\Transformers;

use App\Containers\Grace\Models\Grace;
use App\Ship\Parents\Transformers\Transformer;

class GraceTransformer extends Transformer
{
    /**
     * @var  array
     */
    protected $defaultIncludes = [

    ];

    /**
     * @var  array
     */
    protected $availableIncludes = [

    ];

    /**
     * @param Grace $entity
     *
     * @return array
     */
    public function transform(Grace $entity)
    {
        $response = [
            'object' => 'Grace',
            'id' => $entity->getHashedKey(),
            'loan_id' => $entity->loan_id,
            'days' => $entity->days,
            'start_date' => $entity->start_date,
            'end_date' => $entity->end_date,
            'description' => $entity->description,
            'created_at' => $entity->created_at,
            'updated_at' => $entity->updated_at,
            'loan' => $entity->loan,

        ];

        $response = $this->ifAdmin([
            'real_id'    => $entity->id,
            // 'deleted_at' => $entity->deleted_at,
        ], $response);

        return $response;
    }
}
