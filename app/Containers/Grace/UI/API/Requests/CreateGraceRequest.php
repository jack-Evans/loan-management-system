<?php

namespace App\Containers\Grace\UI\API\Requests;

use App\Ship\Parents\Requests\Request;

/**
 * Class CreateGraceRequest.
 */
class CreateGraceRequest extends Request
{

    /**
     * The assigned Transporter for this Request
     *
     * @var string
     */
    protected $transporter = \App\Containers\Grace\Data\Transporters\CreateGraceTransporter::class;

    /**
     * Define which Roles and/or Permissions has access to this request.
     *
     * @var  array
     */
    protected $access = [
        'permissions' => '',
        'roles'       => '',
    ];

    /**
     * Id's that needs decoding before applying the validation rules.
     *
     * @var  array
     */
    protected $decode = [
        'loanId',
    ];

    /**
     * Defining the URL parameters (e.g, `/user/{id}`) allows applying
     * validation rules on them and allows accessing them like request data.
     *
     * @var  array
     */
    protected $urlParameters = [
        // 'id',
    ];

    /**
     * @return  array
     */
    public function rules()
    {
        return [
            'loanId'        => 'required|integer|exists:loans,id',
            'days'          => 'required|regex:/^\d*(\.\d{1,1})?$/',
            'startDate'     => 'required|date_format:Y-m-d',
            'endDate'       => 'required|date_format:Y-m-d',
            'description'   => 'string|min:3|max:100',
        ];
    }

    /**
     * @return  bool
     */
    public function authorize()
    {
        return $this->check([
            'hasAccess',
        ]);
    }
}
