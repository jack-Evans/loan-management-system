<?php

namespace App\Containers\Grace\Data\Repositories;

use App\Ship\Parents\Repositories\Repository;

/**
 * Class GraceRepository
 */
class GraceRepository extends Repository
{

    /**
     * @var array
     */
    protected $fieldSearchable = [
        'id' => '=',
        // ...
    ];

}
