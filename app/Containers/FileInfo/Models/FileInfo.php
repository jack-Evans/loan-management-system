<?php

namespace App\Containers\FileInfo\Models;

use App\Ship\Parents\Models\Model;

class FileInfo extends Model
{
    protected $fillable = [
        'loan_id',
        'type',
        'create_date',
        'no_of_transactions',
        'tnx_first_date',
        'tnx_last_date',
        'end_balance',
        // 'end_balance_system',
        'calculation_difference',
        'total_interest',
        'interest_days',
        'default_interest',
        'default_interest_days',
    ];

    protected $attributes = [

    ];

    protected $hidden = [

    ];

    protected $casts = [

    ];

    protected $dates = [
        'create_date',
        'tnx_first_date',
        'tnx_last_date',        
        'created_at',
        'updated_at',
    ];

    /**
     * A resource key to be used by the the JSON API Serializer responses.
     */
    protected $resourceKey = 'fileinfos';

    public function loan(){
        return $this->belongsTo(\App\Containers\Loan\Models\Loan::class, 'loan_id');
    }
}
