<?php

namespace App\Containers\FileInfo\Tasks;

use App\Containers\FileInfo\Data\Repositories\FileInfoRepository;
use App\Ship\Exceptions\DeleteResourceFailedException;
use App\Ship\Parents\Tasks\Task;
use Exception;

class DeleteFileInfoTask extends Task
{

    protected $repository;

    public function __construct(FileInfoRepository $repository)
    {
        $this->repository = $repository;
    }

    public function run($id)
    {
        try {
            return $this->repository->delete($id);
        }
        catch (Exception $exception) {
            throw new DeleteResourceFailedException();
        }
    }
}
