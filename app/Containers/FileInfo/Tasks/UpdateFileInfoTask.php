<?php

namespace App\Containers\FileInfo\Tasks;

use App\Containers\FileInfo\Data\Repositories\FileInfoRepository;
use App\Ship\Exceptions\UpdateResourceFailedException;
use App\Ship\Parents\Tasks\Task;
use Exception;

class UpdateFileInfoTask extends Task
{

    protected $repository;

    public function __construct(FileInfoRepository $repository)
    {
        $this->repository = $repository;
    }

    public function run($id, array $data)
    {
        try {
            return $this->repository->update($data, $id);
        }
        catch (Exception $exception) {
            throw new UpdateResourceFailedException();
        }
    }
}
