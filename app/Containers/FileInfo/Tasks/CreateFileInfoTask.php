<?php

namespace App\Containers\FileInfo\Tasks;

use App\Containers\FileInfo\Data\Repositories\FileInfoRepository;
use App\Ship\Exceptions\CreateResourceFailedException;
use App\Ship\Parents\Tasks\Task;
use Exception;

class CreateFileInfoTask extends Task
{

    protected $repository;

    public function __construct(FileInfoRepository $repository)
    {
        $this->repository = $repository;
    }

    public function run(array $data)
    {
        try {
            return $this->repository->create($data);
        }
        catch (Exception $exception) {
            throw new CreateResourceFailedException();
        }
    }
}
