<?php

namespace App\Containers\FileInfo\Tasks;

use App\Containers\FileInfo\Data\Repositories\FileInfoRepository;
use App\Ship\Parents\Tasks\Task;

class GetAllFileInfosTask extends Task
{

    protected $repository;

    public function __construct(FileInfoRepository $repository)
    {
        $this->repository = $repository;
    }

    public function run()
    {
        return $this->repository->paginate();
    }
}
