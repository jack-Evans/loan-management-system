<?php

namespace App\Containers\FileInfo\Data\Repositories;

use App\Ship\Parents\Repositories\Repository;

/**
 * Class FileInfoRepository
 */
class FileInfoRepository extends Repository
{

    /**
     * @var array
     */
    protected $fieldSearchable = [
        'id' => '=',
        // ...
    ];

}
