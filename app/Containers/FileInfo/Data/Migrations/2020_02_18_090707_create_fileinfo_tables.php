<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateFileinfoTables extends Migration
{

    /**
     * Run the migrations.
     */
    public function up()
    {
        Schema::create('file_infos', function (Blueprint $table) {

            $table->increments('id');
            $table->unsignedInteger('loan_id');
            $table->enum('type', ['system', 'file']);
            $table->date('create_date')->nullable();
            $table->string('no_of_transactions')->nullable();
            $table->date('tnx_first_date')->nullable();
            $table->date('tnx_last_date')->nullable();
            $table->float('end_balance', 12, 4)->nullable();
            // $table->float('end_balance_system', 12, 4)->nullable();
            $table->float('calculation_difference', 12, 4)->nullable();
            $table->float('total_interest', 12, 4)->nullable();
            $table->unsignedInteger('interest_days')->nullable();
            $table->float('default_interest', 12, 4)->nullable();
            $table->unsignedInteger('default_interest_days')->nullable();
            $table->timestamps();
            //$table->softDeletes();

        });
    }

    /**
     * Reverse the migrations.
     */
    public function down()
    {
        Schema::dropIfExists('file_infos');
    }
}
