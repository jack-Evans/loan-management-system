<?php

namespace App\Containers\FileInfo\UI\API\Requests;

use App\Ship\Parents\Requests\Request;

/**
 * Class CreateFileInfoRequest.
 */
class CreateFileInfoRequest extends Request
{

    /**
     * The assigned Transporter for this Request
     *
     * @var string
     */
    protected $transporter = \App\Containers\FileInfo\Data\Transporters\CreateFileInfoTransporter::class;

    /**
     * Define which Roles and/or Permissions has access to this request.
     *
     * @var  array
     */
    protected $access = [
        'permissions' => '',
        'roles'       => '',
    ];

    /**
     * Id's that needs decoding before applying the validation rules.
     *
     * @var  array
     */
    protected $decode = [
        'loanid',
    ];

    /**
     * Defining the URL parameters (e.g, `/user/{id}`) allows applying
     * validation rules on them and allows accessing them like request data.
     *
     * @var  array
     */
    protected $urlParameters = [
        // 'id',
    ];

    /**
     * @return  array
     */
    public function rules()
    {
        return [
            'loanid' => 'required|exists:loans,id',
            'type' => 'required|in:system,file',
            'createDate' => 'date_format:Y-m-d',
            'tnxFirstDate' => 'date_format:Y-m-d',
            'tnxLastDate' => 'date_format:Y-m-d',
            // '{user-input}' => 'required|max:255',
        ];
    }

    /**
     * @return  bool
     */
    public function authorize()
    {
        return $this->check([
            'hasAccess',
        ]);
    }
}
