<?php

namespace App\Containers\FileInfo\UI\API\Transformers;

use App\Containers\FileInfo\Models\FileInfo;
use App\Ship\Parents\Transformers\Transformer;

class FileInfoTransformer extends Transformer
{
    /**
     * @var  array
     */
    protected $defaultIncludes = [

    ];

    /**
     * @var  array
     */
    protected $availableIncludes = [

    ];

    /**
     * @param FileInfo $entity
     *
     * @return array
     */
    public function transform(FileInfo $entity)
    {
        $response = [
            // 'object' => 'FileInfo',
            'Loan' => $entity->loan->loan_id,
            'Loan Type' => $entity->loan->loan_type,
            'Type'=> $entity->type,
            'Import date' => $entity->create_date->format('Y-m-d'),
            'Transactions' => $entity->no_of_transactions,
            'Transactions first date' => $entity->tnx_first_date->format('Y-m-d'),
            'Transactions last date' => $entity->tnx_last_date->format('Y-m-d'),
            'End balance' => number_format($entity->end_balance, 2),
            'Calculation difference' => number_format($entity->calculation_difference, 2),
            'Interest to last date' => number_format($entity->total_interest, 2),
            'Interest number of days' => $entity->interest_days,
            'Default interest to last date' => number_format($entity->default_interest, 2),
            'Default interest number of days' => $entity->default_interest_days,

        ];

        $response = $this->ifAdmin([
            'real_id'    => $entity->id,
            // 'deleted_at' => $entity->deleted_at,
        ], $response);

        return $response;
    }
}
