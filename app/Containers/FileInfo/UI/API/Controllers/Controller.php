<?php

namespace App\Containers\FileInfo\UI\API\Controllers;

use App\Containers\FileInfo\UI\API\Requests\CreateFileInfoRequest;
use App\Containers\FileInfo\UI\API\Requests\DeleteFileInfoRequest;
use App\Containers\FileInfo\UI\API\Requests\GetAllFileInfosRequest;
use App\Containers\FileInfo\UI\API\Requests\FindFileInfoByIdRequest;
use App\Containers\FileInfo\UI\API\Requests\UpdateFileInfoRequest;
use App\Containers\FileInfo\UI\API\Transformers\FileInfoTransformer;
use App\Ship\Parents\Controllers\ApiController;
use Apiato\Core\Foundation\Facades\Apiato;

/**
 * Class Controller
 *
 * @package App\Containers\FileInfo\UI\API\Controllers
 */
class Controller extends ApiController
{
    /**
     * @param CreateFileInfoRequest $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function createFileInfo(CreateFileInfoRequest $request)
    {
        $fileinfo = Apiato::call('FileInfo@CreateFileInfoAction', [$request]);

        return $this->created($this->transform($fileinfo, FileInfoTransformer::class));
    }

    /**
     * @param FindFileInfoByIdRequest $request
     * @return array
     */
    public function findFileInfoById(FindFileInfoByIdRequest $request)
    {
        $fileinfo = Apiato::call('FileInfo@FindFileInfoByIdAction', [$request]);

        return $this->transform($fileinfo, FileInfoTransformer::class);
    }

    /**
     * @param GetAllFileInfosRequest $request
     * @return array
     */
    public function getAllFileInfos(GetAllFileInfosRequest $request)
    {
        $fileinfos = Apiato::call('FileInfo@GetAllFileInfosAction', [$request]);

        return $this->transform($fileinfos, FileInfoTransformer::class);
    }

    /**
     * @param GetAllFileInfosRequest $request
     * @return array
     */
    public function exportAllFileInfos(\App\Containers\Loan\UI\API\Requests\ExportLoanRequest $request)
    {
        $fileinfos = Apiato::call('FileInfo@ExportAllFileInfosAction', [$request]);

        return $this->transform($fileinfos, FileInfoTransformer::class);
    }

    /**
     * @param UpdateFileInfoRequest $request
     * @return array
     */
    public function updateFileInfo(UpdateFileInfoRequest $request)
    {
        $fileinfo = Apiato::call('FileInfo@UpdateFileInfoAction', [$request]);

        return $this->transform($fileinfo, FileInfoTransformer::class);
    }

    /**
     * @param DeleteFileInfoRequest $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function deleteFileInfo(DeleteFileInfoRequest $request)
    {
        Apiato::call('FileInfo@DeleteFileInfoAction', [$request]);

        return $this->noContent();
    }
}
