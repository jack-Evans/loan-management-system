<?php

/**
 * @apiGroup           FileInfo
 * @apiName            createFileInfo
 *
 * @api                {POST} /v1/fileinfos Endpoint title here..
 * @apiDescription     Endpoint description here..
 *
 * @apiVersion         1.0.0
 * @apiPermission      none
 *
 * @apiParam           {String}  parameters here..
 *
 * @apiSuccessExample  {json}  Success-Response:
 * HTTP/1.1 200 OK
{
  // Insert the response of the request here...
}
 */

/** @var Route $router */
$router->post('fileinfos', [
    'as' => 'api_fileinfo_create_file_info',
    'uses'  => 'Controller@createFileInfo',
    'middleware' => [
      'auth:api',
    ],
]);
