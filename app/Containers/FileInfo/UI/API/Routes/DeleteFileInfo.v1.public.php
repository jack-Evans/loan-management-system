<?php

/**
 * @apiGroup           FileInfo
 * @apiName            deleteFileInfo
 *
 * @api                {DELETE} /v1/fileinfos/:id Endpoint title here..
 * @apiDescription     Endpoint description here..
 *
 * @apiVersion         1.0.0
 * @apiPermission      none
 *
 * @apiParam           {String}  parameters here..
 *
 * @apiSuccessExample  {json}  Success-Response:
 * HTTP/1.1 200 OK
{
  // Insert the response of the request here...
}
 */

/** @var Route $router */
$router->delete('fileinfos/{id}', [
    'as' => 'api_fileinfo_delete_file_info',
    'uses'  => 'Controller@deleteFileInfo',
    'middleware' => [
      'auth:api',
    ],
]);
