<?php

/**
 * @apiGroup           FileInfo
 * @apiName            findFileInfoById
 *
 * @api                {GET} /v1/fileinfos/:id Endpoint title here..
 * @apiDescription     Endpoint description here..
 *
 * @apiVersion         1.0.0
 * @apiPermission      none
 *
 * @apiParam           {String}  parameters here..
 *
 * @apiSuccessExample  {json}  Success-Response:
 * HTTP/1.1 200 OK
{
  // Insert the response of the request here...
}
 */

/** @var Route $router */
$router->get('fileinfos/{id}', [
    'as' => 'api_fileinfo_find_file_info_by_id',
    'uses'  => 'Controller@findFileInfoById',
    'middleware' => [
      'auth:api',
    ],
]);
