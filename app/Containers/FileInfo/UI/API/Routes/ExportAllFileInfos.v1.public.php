<?php

/**
 * @apiGroup           ExportFileInfo
 * @apiName            getAllFileInfos
 *
 * @api                {GET} /v1/fileinfos Endpoint title here..
 * @apiDescription     Endpoint description here..
 *
 * @apiVersion         1.0.0
 * @apiPermission      none
 *
 * @apiParam           {String}  parameters here..
 *
 * @apiSuccessExample  {json}  Success-Response:
 * HTTP/1.1 200 OK
{
  // Insert the response of the request here...
}
 */

/** @var Route $router */
$router->post('fileinfos/export', [
    'as' => 'api_fileinfo_get_all_file_infos',
    'uses'  => 'Controller@exportAllFileInfos',
    'middleware' => [
      'auth:api',
    ],
]);
