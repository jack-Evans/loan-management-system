<?php

/** @var Route $router */
$router->post('fileinfos/store', [
    'as' => 'web_fileinfo_store',
    'uses'  => 'Controller@store',
    'middleware' => [
      'auth:web',
    ],
]);
