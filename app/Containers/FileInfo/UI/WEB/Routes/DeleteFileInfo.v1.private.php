<?php

/** @var Route $router */
$router->delete('fileinfos/{id}', [
    'as' => 'web_fileinfo_delete',
    'uses'  => 'Controller@delete',
    'middleware' => [
      'auth:web',
    ],
]);
