<?php

/** @var Route $router */
$router->get('fileinfos/{id}/edit', [
    'as' => 'web_fileinfo_edit',
    'uses'  => 'Controller@edit',
    'middleware' => [
      'auth:web',
    ],
]);
