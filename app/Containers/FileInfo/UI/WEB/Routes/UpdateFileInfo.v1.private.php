<?php

/** @var Route $router */
$router->patch('fileinfos/{id}', [
    'as' => 'web_fileinfo_update',
    'uses'  => 'Controller@update',
    'middleware' => [
      'auth:web',
    ],
]);
