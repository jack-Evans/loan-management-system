<?php

/** @var Route $router */
$router->get('fileinfos/create', [
    'as' => 'web_fileinfo_create',
    'uses'  => 'Controller@create',
    'middleware' => [
      'auth:web',
    ],
]);
