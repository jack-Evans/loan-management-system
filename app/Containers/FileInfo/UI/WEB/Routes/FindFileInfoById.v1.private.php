<?php

/** @var Route $router */
$router->get('fileinfos/{id}', [
    'as' => 'web_fileinfo_show',
    'uses'  => 'Controller@show',
    'middleware' => [
      'auth:web',
    ],
]);
