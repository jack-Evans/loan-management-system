<?php

/** @var Route $router */
$router->get('fileinfos', [
    'as' => 'web_fileinfo_index',
    'uses'  => 'Controller@index',
    'middleware' => [
      'auth:web',
    ],
]);
