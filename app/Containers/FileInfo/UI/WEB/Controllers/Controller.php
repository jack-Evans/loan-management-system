<?php

namespace App\Containers\FileInfo\UI\WEB\Controllers;

use App\Containers\FileInfo\UI\WEB\Requests\CreateFileInfoRequest;
use App\Containers\FileInfo\UI\WEB\Requests\DeleteFileInfoRequest;
use App\Containers\FileInfo\UI\WEB\Requests\GetAllFileInfosRequest;
use App\Containers\FileInfo\UI\WEB\Requests\FindFileInfoByIdRequest;
use App\Containers\FileInfo\UI\WEB\Requests\UpdateFileInfoRequest;
use App\Containers\FileInfo\UI\WEB\Requests\StoreFileInfoRequest;
use App\Containers\FileInfo\UI\WEB\Requests\EditFileInfoRequest;
use App\Ship\Parents\Controllers\WebController;
use Apiato\Core\Foundation\Facades\Apiato;

/**
 * Class Controller
 *
 * @package App\Containers\FileInfo\UI\WEB\Controllers
 */
class Controller extends WebController
{
    /**
     * Show all entities
     *
     * @param GetAllFileInfosRequest $request
     */
    public function index(GetAllFileInfosRequest $request)
    {
        $fileinfos = Apiato::call('FileInfo@GetAllFileInfosAction', [$request]);

        // ..
    }

    /**
     * Show one entity
     *
     * @param FindFileInfoByIdRequest $request
     */
    public function show(FindFileInfoByIdRequest $request)
    {
        $fileinfo = Apiato::call('FileInfo@FindFileInfoByIdAction', [$request]);

        // ..
    }

    /**
     * Create entity (show UI)
     *
     * @param CreateFileInfoRequest $request
     */
    public function create(CreateFileInfoRequest $request)
    {
        // ..
    }

    /**
     * Add a new entity
     *
     * @param StoreFileInfoRequest $request
     */
    public function store(StoreFileInfoRequest $request)
    {
        $fileinfo = Apiato::call('FileInfo@CreateFileInfoAction', [$request]);

        // ..
    }

    /**
     * Edit entity (show UI)
     *
     * @param EditFileInfoRequest $request
     */
    public function edit(EditFileInfoRequest $request)
    {
        $fileinfo = Apiato::call('FileInfo@GetFileInfoByIdAction', [$request]);

        // ..
    }

    /**
     * Update a given entity
     *
     * @param UpdateFileInfoRequest $request
     */
    public function update(UpdateFileInfoRequest $request)
    {
        $fileinfo = Apiato::call('FileInfo@UpdateFileInfoAction', [$request]);

        // ..
    }

    /**
     * Delete a given entity
     *
     * @param DeleteFileInfoRequest $request
     */
    public function delete(DeleteFileInfoRequest $request)
    {
         $result = Apiato::call('FileInfo@DeleteFileInfoAction', [$request]);

         // ..
    }
}
