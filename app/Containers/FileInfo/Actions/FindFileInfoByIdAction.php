<?php

namespace App\Containers\FileInfo\Actions;

use App\Ship\Parents\Actions\Action;
use App\Ship\Parents\Requests\Request;
use Apiato\Core\Foundation\Facades\Apiato;

class FindFileInfoByIdAction extends Action
{
    public function run(Request $request)
    {
        $fileinfo = Apiato::call('FileInfo@FindFileInfoByIdTask', [$request->id]);

        return $fileinfo;
    }
}
