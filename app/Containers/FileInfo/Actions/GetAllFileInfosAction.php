<?php

namespace App\Containers\FileInfo\Actions;

use App\Ship\Parents\Actions\Action;
use App\Ship\Parents\Requests\Request;
use Apiato\Core\Foundation\Facades\Apiato;

class GetAllFileInfosAction extends Action
{
    public function run(Request $request)
    {
        $fileinfos = Apiato::call('FileInfo@GetAllFileInfosTask', [], ['addRequestCriteria']);

        return $fileinfos;
    }
}
