<?php

namespace App\Containers\FileInfo\Actions;

use App\Ship\Parents\Actions\Action;
use App\Ship\Parents\Requests\Request;
use Apiato\Core\Foundation\Facades\Apiato;

class UpdateFileInfoAction extends Action
{
    public function run(Request $request)
    {
        $data = $request->sanitizeInput([
            // add your request data here
        ]);

        $fileinfo = Apiato::call('FileInfo@UpdateFileInfoTask', [$request->id, $data]);

        return $fileinfo;
    }
}
