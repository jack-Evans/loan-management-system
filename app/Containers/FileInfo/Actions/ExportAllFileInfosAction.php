<?php

namespace App\Containers\FileInfo\Actions;

use App\Ship\Parents\Actions\Action;
use App\Ship\Parents\Requests\Request;
use Apiato\Core\Foundation\Facades\Apiato;
use App\Containers\FileInfo\Models\FileInfo;
// excel libraries
use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\Writer\Csv;
use PhpOffice\PhpSpreadsheet\Writer\Xlsx;
use PhpOffice\PhpSpreadsheet\Worksheet\Worksheet;
use PhpOffice\PhpSpreadsheet\Style\NumberFormat;

class ExportAllFileInfosAction extends Action
{
    public function run(Request $request)
    {
        // $fileinfos = Apiato::call('FileInfo@GetAllFileInfosTask')->where('type', 'file');
        $colorGray = [
		    'fill' => [
		        'fillType' => \PhpOffice\PhpSpreadsheet\Style\Fill::FILL_SOLID,
		        'color' => array('argb' => 'FFF3F3F3'),
		    ],
		    'borders' => [
		        'top' => [
		            'borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN,
		            'color' => ['argb' => 'C0C0C0'],
		        ],
		        'bottom' => [
		            'borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN,
		            'color' => ['argb' => 'C0C0C0'],
		        ],
		        'left' => [
		            'borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN,
		            'color' => ['argb' => 'C0C0C0'],
		        ],
		        'right' => [
		            'borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN,
		            'color' => ['argb' => 'C0C0C0'],
		        ],
	        ],
		];

		$colorRed = [
		    'fill' => [
		        'fillType' => \PhpOffice\PhpSpreadsheet\Style\Fill::FILL_SOLID,
		        'color' => array('argb' => 'FF9999'),
		    ],
		];

		$colorBlue = [
		    'fill' => [
		        'fillType' => \PhpOffice\PhpSpreadsheet\Style\Fill::FILL_SOLID,
		        'color' => array('argb' => 'CCCCFF'),
		    ],
		];
		$colorG10 = ['fill' => ['fillType' => \PhpOffice\PhpSpreadsheet\Style\Fill::FILL_SOLID,'color' => array('argb' => 'CCFFCC'),],];
		$colorG100 = ['fill' => ['fillType' => \PhpOffice\PhpSpreadsheet\Style\Fill::FILL_SOLID,'color' => array('argb' => 'E5FFCC'),],];
		$colorG500 = ['fill' => ['fillType' => \PhpOffice\PhpSpreadsheet\Style\Fill::FILL_SOLID,'color' => array('argb' => 'FFFFCC'),],];
		$colorG1000 = ['fill' => ['fillType' => \PhpOffice\PhpSpreadsheet\Style\Fill::FILL_SOLID,'color' => array('argb' => 'FFE5CC'),],];
		$colorG1000P = ['fill' => ['fillType' => \PhpOffice\PhpSpreadsheet\Style\Fill::FILL_SOLID,'color' => array('argb' => 'FFCCCC'),],];

        // $fileinfos = Apiato::call('FileInfo@GetAllFileInfosTask');
    	$ids = array();
        if($request->tagIds){
	    	$loanTags = Apiato::call('Tag@GetAllLoansBasedOnTagsTask', [$request->tagIds]);
	    	if(count($loanTags) > 0){
		    	foreach ($loanTags as $key => $loanTag) {
		    		array_push($ids, $loanTag->loan_id);
		    	}
	    	} else
	    		throw new \App\Containers\Payment\Exceptions\HTTPPreConditionFailedException('No any loan found with selected tags.');
    	} else {
	        $loanIds = $request->loanIds;
	    	foreach ($loanIds as $key => $loanId) {
	    		array_push($ids, \Hashids::decode($loanId)[0]);
	    	}
    	}

    	$fileinfos = FileInfo::whereIn('loan_id', $ids)->orderBy('loan_id', 'asc')->get();
        // $fileinfos = Apiato::call('FileInfo@GetAllFileInfosTask');
        // unlink('ImportFileInfo.xlsx');
        $spreadsheet = new Spreadsheet();
		$spreadsheet->getProperties()->setCreator('lms.kuflink')
	      ->setLastModifiedBy('Admin')
	      ->setTitle('Import Info')
	      ->setSubject('Import Info')
	      ->setDescription('Import Info');	    
	    $spreadsheet->removeSheetByIndex(0);
        
        $sheet = new Worksheet($spreadsheet, 'Imported Files Info');
		$spreadsheet->addSheet($sheet, 1);
		$sheet->setCellValue('A1', 'Loan');
		$sheet->setCellValue('B1', 'Loan Type');
		$sheet->setCellValue('C1', 'Type');
		$sheet->setCellValue('D1', 'Import date');
		$sheet->setCellValue('E1', 'Transactions');
		$sheet->setCellValue('F1', 'Transactions first date');
		$sheet->setCellValue('G1', 'Transactions last date');
		$sheet->setCellValue('H1', 'End balance');
		$sheet->setCellValue('I1', 'Calculation difference');
		$sheet->setCellValue('J1', 'Interest to last date');
		$sheet->setCellValue('K1', 'Interest number of days');
		$sheet->setCellValue('L1', 'Default interest to last date');
		$sheet->setCellValue('M1', 'Default interest number of days');

		$r = 2;
		foreach ($fileinfos as $key => $info) {
			$sheet->setCellValue("A$r", $info->loan->loan_id);
			$sheet->setCellValue("B$r", $info->loan->loan_type);
			$sheet->setCellValue("C$r", $info->type);
			$sheet->setCellValue("D$r", $info->create_date->format('Y-m-d'));
			$sheet->setCellValue("E$r", $info->no_of_transactions);
			$sheet->setCellValue("F$r", $info->tnx_first_date->format('Y-m-d'));
			$sheet->setCellValue("G$r", $info->tnx_last_date->format('Y-m-d'));
			$sheet->setCellValue("H$r", number_format($info->end_balance, 2));
			$sheet->setCellValue("I$r", number_format($info->calculation_difference, 2));
			$sheet->setCellValue("J$r", number_format($info->total_interest, 2));
			$sheet->setCellValue("K$r", $info->interest_days);
			$sheet->setCellValue("L$r", number_format($info->default_interest, 2));
			$sheet->setCellValue("M$r", $info->default_interest_days);

			if(($r/2)%2 == 0)
	      		$sheet->getStyle("A$r:M$r")->applyFromArray($colorGray);

	      	$diff = $info->calculation_difference;
	      	if($diff >= -10  && $diff <= 10)
	      		$sheet->getStyle("I$r")->applyFromArray($colorG10);
	      	elseif(($diff > 10 && $diff <= 100) || ($diff < -10 && $diff >= -100))
	      		$sheet->getStyle("I$r")->applyFromArray($colorG100);
	      	elseif(($diff > 100 && $diff <= 500) || ($diff < -100 && $diff >= -500))
	      		$sheet->getStyle("I$r")->applyFromArray($colorG500);
	      	elseif(($diff > 500 && $diff <= 1000) || ($diff < -500 && $diff >= -1000))
	      		$sheet->getStyle("I$r")->applyFromArray($colorG1000);
	      	// elseif($info->calculation_difference >= 100 $info->calculation_difference < 500)
	      		// $sheet->getStyle("I$r")->applyFromArray($colorG1000P);
	      	// elseif($info->calculation_difference >= 500 $info->calculation_difference < 1000)
	      		// $sheet->getStyle("I$r")->applyFromArray($colorG1000);
	      	else
	      		$sheet->getStyle("I$r")->applyFromArray($colorG1000P);
			$r++;
		}

		foreach(range('A', 'M') as $columnID) {
		    $sheet->getColumnDimension($columnID)->setAutoSize(true);
		}
		// after data is filled into 
		$maxWidth = 25;
		$sheet->calculateColumnWidths();
	    foreach ($sheet->getColumnDimensions() as $colDim) {
	        if (!$colDim->getAutoSize()) {
	            continue;
	        }
	        $colWidth = $colDim->getWidth();
	        if ($colWidth > $maxWidth) {
	            $colDim->setAutoSize(false);
	            $colDim->setWidth($maxWidth);
	        }
	    }

        $writer = new Xlsx($spreadsheet);
		$fileName = 'ImportFileInfo.xlsx';
		$writer->save($fileName);

		if($request->uploadAtSharepoint){
			$sharepoint = new \App\Classes\Sharepoint();
			$sharepoint->uploadToSharepoint(public_path()."/".$fileName, $fileName);
		}
		return $fileinfos;
    }
}
