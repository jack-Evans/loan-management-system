<?php

namespace App\Containers\FileInfo\Actions;

use App\Ship\Parents\Actions\Action;
use App\Ship\Parents\Requests\Request;
use Apiato\Core\Foundation\Facades\Apiato;

class DeleteFileInfoAction extends Action
{
    public function run(Request $request)
    {
        return Apiato::call('FileInfo@DeleteFileInfoTask', [$request->id]);
    }
}
