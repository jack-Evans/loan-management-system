<?php

namespace App\Containers\FileInfo\Actions;

use App\Ship\Parents\Actions\Action;
use App\Ship\Parents\Requests\Request;
use Apiato\Core\Foundation\Facades\Apiato;
use App\Containers\FileInfo\Models\FileInfo;

class CreateFileInfoAction extends Action
{
    public function run(Request $request)
    {
        $fileData = [
        	'loan_id' => $request->loanid,
        	'type' => $request->type,
        	'create_date' => $request->createDate,
        	'no_of_transactions' => $request->noOfTransactions,
        	'tnx_first_date' => $request->tnxFirstDate,
        	'tnx_last_date' => $request->tnxLastDate,
        	'end_balance' => $request->endBalance,
        	// 'end_balance_system' => $request->endBalanceSystem,
        	'calculation_difference' => $request->calculationDifference,
        	'total_interest' => $request->totalInterest,
        	'interest_days' => $request->interestDays,
        	'default_interest' => $request->defaultInterest,
        	'default_interest_days' => $request->defaultInterestDays,
        ];
// dd($fileData);
        $fileinfo = FileInfo::where('loan_id', $request->loanid)->where('type', $request->type)->first();
        if($fileinfo)
        	$fileinfo = Apiato::call('FileInfo@UpdateFileInfoTask', [$fileinfo->id, $fileData]);
        else
	        $fileinfo = Apiato::call('FileInfo@CreateFileInfoTask', [$fileData]);
// dd($fileinfo);
        return $fileinfo;
    }
}
