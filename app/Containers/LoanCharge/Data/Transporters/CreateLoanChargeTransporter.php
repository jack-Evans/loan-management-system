<?php

namespace App\Containers\LoanCharge\Data\Transporters;

use App\Ship\Parents\Transporters\Transporter;

class CreateLoanChargeTransporter extends Transporter
{

    /**
     * @var array
     */
    protected $schema = [
        'type' => 'object',
        'properties' => [
            'loan_id'   => ['type' => 'integer'],
            'charge_id' => ['type' => 'integer'],
            'value'     => ['type' => 'number'],

        ],
        'required'   => [
            // define the properties that MUST be set
            'loan_id',
            'charge_id',
            'value',
        ],
        'default'    => [
            // provide default values for specific properties here
        ]
    ];
}
