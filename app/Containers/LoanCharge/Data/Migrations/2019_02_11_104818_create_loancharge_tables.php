<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateLoanchargeTables extends Migration
{

    /**
     * Run the migrations.
     */
    public function up()
    {
        Schema::create('loan_charges', function (Blueprint $table) {

            // $table->increments('id');
            $table->primary(['loan_id', 'charge_id']);
            $table->unsignedInteger('loan_id');
            $table->unsignedInteger('charge_id');
            $table->text('description')->nullable();
            $table->timestamps();
            $table->softDeletes();

        });
    }

    /**
     * Reverse the migrations.
     */
    public function down()
    {
        Schema::drop('loancharges');
    }
}
