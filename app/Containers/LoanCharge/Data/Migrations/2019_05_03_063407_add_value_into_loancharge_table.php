<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class AddValueIntoLoanchargeTable extends Migration
{

    /**
     * Run the migrations.
     */
    public function up()
    {
        Schema::table('loan_charges', function (Blueprint $table) {
            $table->float('value', 12, 2);
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down()
    {
        Schema::table('loan_charges', function($table){
            $table->dropColumn('value');
        });
    }
}
