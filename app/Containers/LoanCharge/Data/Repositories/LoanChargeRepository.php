<?php

namespace App\Containers\LoanCharge\Data\Repositories;

use App\Ship\Parents\Repositories\Repository;

/**
 * Class LoanChargeRepository
 */
class LoanChargeRepository extends Repository
{

    /**
     * @var array
     */
    protected $fieldSearchable = [
        'id' => '=',
        // ...
    ];

}
