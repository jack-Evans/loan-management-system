<?php

namespace App\Containers\LoanCharge\UI\API\Controllers;

use App\Containers\LoanCharge\UI\API\Requests\CreateLoanChargeRequest;
use App\Containers\LoanCharge\UI\API\Requests\DeleteLoanChargeRequest;
use App\Containers\LoanCharge\UI\API\Requests\FindLoanChargeByIdRequest;
use App\Containers\LoanCharge\UI\API\Requests\UpdateLoanChargeRequest;
use App\Containers\LoanCharge\UI\API\Requests\CalculateLoanChargeRequest;
use App\Containers\LoanCharge\UI\API\Transformers\LoanChargeTransformer;
use App\Ship\Parents\Controllers\ApiController;
use Apiato\Core\Foundation\Facades\Apiato;

/**
 * Class Controller
 *
 * @package App\Containers\LoanCharge\UI\API\Controllers
 */
class Controller extends ApiController
{
    /**
     * @param CreateLoanChargeRequest $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function createLoanCharge(CreateLoanChargeRequest $request)
    {
        $loancharge = Apiato::call('LoanCharge@CreateLoanChargeAction', [$request]);

        return $this->created($this->transform($loancharge, LoanChargeTransformer::class));
    }

    /**
     * @param FindLoanChargeByIdRequest $request
     * @return array
     */
    public function findLoanChargeById(FindLoanChargeByIdRequest $request)
    {
        $loancharge = Apiato::call('LoanCharge@FindLoanChargeByIdAction', [$request]);

        return $this->transform($loancharge, LoanChargeTransformer::class);
    }

    /**
     * @param UpdateLoanChargeRequest $request
     * @return array
     */
    public function updateLoanCharge(UpdateLoanChargeRequest $request)
    {
        $loancharge = Apiato::call('LoanCharge@UpdateLoanChargeAction', [$request]);

        return $this->transform($loancharge, LoanChargeTransformer::class);
    }

    /**
     * @param DeleteLoanChargeRequest $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function deleteLoanCharge(DeleteLoanChargeRequest $request)
    {
        Apiato::call('LoanCharge@DeleteLoanChargeAction', [$request]);

        return $this->noContent();
    }
    
    /**
     * @param CalculateLoanCharge $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function calculateLoanCharge(CalculateLoanChargeRequest $request)
    {
        return Apiato::call('LoanCharge@CalculateLoanChargeAction', [$request]);
    }
}
