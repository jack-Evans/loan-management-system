<?php

/**
 * @apiGroup           LoanCharge
 * @apiName            calculateLoanCharge
 *
 * @api                {GET} /v1/calculateloancharge/$:id Endpoint title here..
 * @apiDescription     Endpoint description here..
 *
 * @apiVersion         1.0.0
 * @apiPermission      none
 *
 * @apiParam           {String}  parameters here..
 *
 * @apiSuccessExample  {json}  Success-Response:
 * HTTP/1.1 200 OK
{
  // Insert the response of the request here...
}
 */

/** @var Route $router */
$router->get('calculateloancharges/{id}', [
    'as' => 'api_loancharge_calculate_loan_charge',
    'uses'  => 'Controller@calculateLoanCharge',
    'middleware' => [
      'auth:api',
    ],
]);
