<?php

/**
 * @apiGroup           LoanCharge
 * @apiName            deleteLoanCharge
 *
 * @api                {DELETE} /v1/loancharges/:id Endpoint title here..
 * @apiDescription     Endpoint description here..
 *
 * @apiVersion         1.0.0
 * @apiPermission      none
 *
 * @apiParam           {String}  parameters here..
 *
 * @apiSuccessExample  {json}  Success-Response:
 * HTTP/1.1 200 OK
{
  // Insert the response of the request here...
}
 */

/** @var Route $router */
$router->delete('loancharges/{loan_id}/{charge_id}', [
    'as' => 'api_loancharge_delete_loan_charge',
    'uses'  => 'Controller@deleteLoanCharge',
    'middleware' => [
      'auth:api',
    ],
]);
