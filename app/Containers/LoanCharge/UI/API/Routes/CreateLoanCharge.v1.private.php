<?php

/**
 * @apiGroup           LoanCharge
 * @apiName            createLoanCharge
 *
 * @api                {POST} /v1/loancharges Endpoint title here..
 * @apiDescription     Endpoint description here..
 *
 * @apiVersion         1.0.0
 * @apiPermission      none
 *
 * @apiParam           {String}  parameters here..
 *
 * @apiSuccessExample  {json}  Success-Response:
 * HTTP/1.1 200 OK
{
  // Insert the response of the request here...
}
 */

/** @var Route $router */
$router->post('loancharges', [
    'as' => 'api_loancharge_create_loan_charge',
    'uses'  => 'Controller@createLoanCharge',
    'middleware' => [
      'auth:api',
    ],
]);
