<?php

/**
 * @apiGroup           LoanCharge
 * @apiName            updateLoanCharge
 *
 * @api                {PATCH} /v1/loancharges/:id Endpoint title here..
 * @apiDescription     Endpoint description here..
 *
 * @apiVersion         1.0.0
 * @apiPermission      none
 *
 * @apiParam           {String}  parameters here..
 *
 * @apiSuccessExample  {json}  Success-Response:
 * HTTP/1.1 200 OK
{
  // Insert the response of the request here...
}
 */

/** @var Route $router */
$router->patch('loancharges/{id}', [
    'as' => 'api_loancharge_update_loan_charge',
    'uses'  => 'Controller@updateLoanCharge',
    'middleware' => [
      'auth:api',
    ],
]);
