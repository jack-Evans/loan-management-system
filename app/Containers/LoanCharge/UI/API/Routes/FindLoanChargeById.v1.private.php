<?php

/**
 * @apiGroup           LoanCharge
 * @apiName            findLoanChargeById
 *
 * @api                {GET} /v1/loancharges/:id Endpoint title here..
 * @apiDescription     Endpoint description here..
 *
 * @apiVersion         1.0.0
 * @apiPermission      none
 *
 * @apiParam           {String}  parameters here..
 *
 * @apiSuccessExample  {json}  Success-Response:
 * HTTP/1.1 200 OK
{
  // Insert the response of the request here...
}
 */

/** @var Route $router */
$router->get('loancharges/{id}', [
    'as' => 'api_loancharge_find_loan_charge_by_id',
    'uses'  => 'Controller@findLoanChargeById',
    'middleware' => [
      'auth:api',
    ],
]);
