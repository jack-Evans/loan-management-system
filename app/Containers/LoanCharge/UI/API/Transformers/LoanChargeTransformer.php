<?php

namespace App\Containers\LoanCharge\UI\API\Transformers;

use App\Containers\LoanCharge\Models\LoanCharge;
use App\Ship\Parents\Transformers\Transformer;

class LoanChargeTransformer extends Transformer
{
    /**
     * @var  array
     */
    protected $defaultIncludes = [

    ];

    /**
     * @var  array
     */
    protected $availableIncludes = [

    ];

    /**
     * @param LoanCharge $entity
     *
     * @return array
     */
    public function transform(LoanCharge $entity)
    {
        $response = [
            'object'     => 'LoanCharge',
            'id'         => $entity->charge->getHashedKey(),
            'name'       => $entity->charge->name,
            'value'      => $entity->value,
            'amountType' => $entity->charge->amount_type,
            'chargeType' => $entity->charge->charge_type,
            'created_at' => $entity->created_at,
            'updated_at' => $entity->updated_at,

        ];

        $response = $this->ifAdmin([
            'real_id'    => $entity->id,
            // 'deleted_at' => $entity->deleted_at,
        ], $response);

        return $response;
    }
}
