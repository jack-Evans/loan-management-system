<?php

namespace App\Containers\LoanCharge\UI\API\Requests;

use App\Ship\Parents\Requests\Request;

/**
 * Class CreateLoanChargeRequest.
 */
class CreateLoanChargeRequest extends Request
{

    /**
     * The assigned Transporter for this Request
     *
     * @var string
     */
    protected $transporter = \App\Containers\LoanCharge\Data\Transporters\CreateLoanChargeTransporter::class;

    /**
     * Define which Roles and/or Permissions has access to this request.
     *
     * @var  array
     */
    protected $access = [
        'permissions' => '',
        'roles'       => '',
    ];

    /**
     * Id's that needs decoding before applying the validation rules.
     *
     * @var  array
     */
    protected $decode = [
        'loanid',
        'chargeid',
    ];

    /**
     * Defining the URL parameters (e.g, `/user/{id}`) allows applying
     * validation rules on them and allows accessing them like request data.
     *
     * @var  array
     */
    protected $urlParameters = [
        // 'id',
    ];

    /**
     * @return  array
     */
    public function rules()
    {
        return [
            'loanid'        => 'required|integer|exists:loans,id|unique:loan_charges,loan_id,null,null,charge_id,'.$this->chargeid,
            'chargeid'      => 'required|integer|exists:charges,id',
            // 'date'          => 'required|date_format:Y-m-d',
            'description'   => 'string|min:3'
        ];
    }

    /**
     * @return  bool
     */
    public function authorize()
    {
        return $this->check([
            'hasAccess',
        ]);
    }
}
