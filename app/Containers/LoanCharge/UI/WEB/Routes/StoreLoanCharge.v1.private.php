<?php

/** @var Route $router */
$router->post('loancharges/store', [
    'as' => 'web_loancharge_store',
    'uses'  => 'Controller@store',
    'middleware' => [
      'auth:web',
    ],
]);
