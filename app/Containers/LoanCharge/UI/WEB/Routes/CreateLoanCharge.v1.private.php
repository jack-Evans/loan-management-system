<?php

/** @var Route $router */
$router->get('loancharges/create', [
    'as' => 'web_loancharge_create',
    'uses'  => 'Controller@create',
    'middleware' => [
      'auth:web',
    ],
]);
