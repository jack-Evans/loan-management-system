<?php

/** @var Route $router */
$router->get('deleteLoanCharge/{loan_id}/{charge_id}', [
    'as' => 'web_loancharge_delete',
    'uses'  => 'Controller@delete',
    'middleware' => [
      'auth:web',
    ],
]);
