<?php

/** @var Route $router */
$router->get('loancharges/{id}', [
    'as' => 'web_loancharge_show',
    'uses'  => 'Controller@show',
    'middleware' => [
      'auth:web',
    ],
]);
