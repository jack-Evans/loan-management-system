<?php

/** @var Route $router */
$router->get('loancharges', [
    'as' => 'web_loancharge_index',
    'uses'  => 'Controller@index',
    'middleware' => [
      'auth:web',
    ],
]);
