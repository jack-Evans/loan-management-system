<?php

/** @var Route $router */
$router->get('loancharges/{id}/edit', [
    'as' => 'web_loancharge_edit',
    'uses'  => 'Controller@edit',
    'middleware' => [
      'auth:web',
    ],
]);
