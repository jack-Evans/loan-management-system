<?php

/** @var Route $router */
$router->post('loancharges/{id}/update', [
    'as' => 'web_loancharge_update',
    'uses'  => 'Controller@update',
    'middleware' => [
      'auth:web',
    ],
]);
