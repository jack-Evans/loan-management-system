@extends('layouts.app')

@section('title', 'Initial Charges')

@section('content')
    <div class="row">
        <div class="col-md-5 col-md-offset-2">
            <h3>{{isset($loanCharge->id) ? 'Edit ' : 'Create '}}Loan Charge</h3>
            <form action="{{url(isset($loanCharge->id) ? 'loancharges/'.$loanCharge->id.'/update': 'loancharges/store' )}}" method="post">
                @csrf()

                <div class="form-group">
                  <label for="loanid">Loan Id:</label>
                  <input type="text" class="form-control" name="loanid" 
                         value="{{isset($loanCharge->loanid) ? $loanCharge->loanid : ''}}">
                </div>

                <div class="form-group">
                  <label for="chargeid">Charge Id:</label>
                  <input type="text" class="form-control" name="chargeid" 
                         value="{{isset($loanCharge->chargeid) ? $loanCharge->chargeid : ''}}">
                </div>                                                                                                          

                <button type="submit" class="btn btn-primary">Submit</button>
            </form>
        </div>
    </div>
@endsection    