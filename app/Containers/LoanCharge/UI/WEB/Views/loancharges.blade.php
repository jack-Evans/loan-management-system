@extends('layouts.app')

@section('title', 'Initial Charges')

@section('content')
    <h3>Loan Charges</h3>
    <div class="row">
        <div class="col-sm-8">
            <a href="{{url('loancharges/create')}}">
                <button class="btn btn-success">Create Loan Charge</button>
            </a>
        </div>

        <div class="col-sm-4">                
        </div>
    </div>
@endsection