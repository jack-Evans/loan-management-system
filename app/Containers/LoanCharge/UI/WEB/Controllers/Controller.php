<?php

namespace App\Containers\LoanCharge\UI\WEB\Controllers;

use App\Containers\LoanCharge\UI\WEB\Requests\CreateLoanChargeRequest;
use App\Containers\LoanCharge\UI\WEB\Requests\DeleteLoanChargeRequest;
use App\Containers\LoanCharge\UI\WEB\Requests\GetAllLoanChargesRequest;
use App\Containers\LoanCharge\UI\WEB\Requests\FindLoanChargeByIdRequest;
use App\Containers\LoanCharge\UI\WEB\Requests\UpdateLoanChargeRequest;
use App\Containers\LoanCharge\UI\WEB\Requests\StoreLoanChargeRequest;
use App\Containers\LoanCharge\UI\WEB\Requests\EditLoanChargeRequest;
use App\Ship\Parents\Controllers\WebController;
use Apiato\Core\Foundation\Facades\Apiato;

/**
 * Class Controller
 *
 * @package App\Containers\LoanCharge\UI\WEB\Controllers
 */
class Controller extends WebController
{
    /**
     * Show all entities
     *
     * @param GetAllLoanChargesRequest $request
     */
    public function index(GetAllLoanChargesRequest $request)
    {
//        $loancharges = Apiato::call('LoanCharge@GetAllLoanChargesAction', [$request]);

        return view('loancharge::loancharges');
    }

    /**
     * Show one entity
     *
     * @param FindLoanChargeByIdRequest $request
     */
    public function show(FindLoanChargeByIdRequest $request)
    {
        $loancharge = Apiato::call('LoanCharge@FindLoanChargeByIdAction', [$request]);

        // ..
    }

    /**
     * Create entity (show UI)
     *
     * @param CreateLoanChargeRequest $request
     */
    public function create(CreateLoanChargeRequest $request)
    {
        return view('loancharge::createUpdate');
    }

    /**
     * Add a new entity
     *
     * @param StoreLoanChargeRequest $request
     */
    public function store(StoreLoanChargeRequest $request)
    {
        $url = config('token-container.WEB_API_URL').'loancharges';
        
        $data = [
            'loanid' => $request->loanid,
            'chargeid' => $request->chargeid,
        ];
        
        $response = __post($url, $data);   
        
        return redirect('loancharges');
    }

    /**
     * Edit entity (show UI)
     *
     * @param EditLoanChargeRequest $request
     */
    public function edit(EditLoanChargeRequest $request)
    {
        $loancharge = Apiato::call('LoanCharge@GetLoanChargeByIdAction', [$request]);

        // ..
    }

    /**
     * Update a given entity
     *
     * @param UpdateLoanChargeRequest $request
     */
    public function update(UpdateLoanChargeRequest $request)
    {
        $loancharge = Apiato::call('LoanCharge@UpdateLoanChargeAction', [$request]);

        // ..
    }

    /**
     * Delete a given entity
     *
     * @param DeleteLoanChargeRequest $request
     */
    public function delete(DeleteLoanChargeRequest $request)
    {
        $url = config('token-container.WEB_API_URL').'loancharges/'.$request->loan_id.'/'.$request->charge_id;
        $response = __delete($url);
        
        return redirect()->back();
    }
}
