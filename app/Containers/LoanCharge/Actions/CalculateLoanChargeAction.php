<?php

namespace App\Containers\LoanCharge\Actions;

use App\Ship\Parents\Actions\Action;
use Apiato\Core\Foundation\Facades\Apiato;
use App\Ship\Parents\Requests\Request;

class CalculateLoanChargeAction extends Action
{
    public function run(Request $request)
    {
    	$loan 			= Apiato::call('Loan@FindLoanByIdTask', [$request->id]);
    	$loanCharges 	= $loan->charges;
    	$loanInterest 	= $loan->interest;
        $fixedCharges 	= Apiato::call('LoanCharge@CalculateFixedChargesTask', [$loanCharges]);
        $chargesOnBorrowedAmount = Apiato::call('LoanCharge@CalculateChargeOnNetLoanTask', [$loanCharges, $loan]);

        $chargesBreakdown 	= Apiato::call('LoanCharge@GetLoanChargeBreakDownTask', [$loanCharges, $loan]);
        $totalCharges 		= $fixedCharges + $chargesOnBorrowedAmount;
        $grossLoan = $totalCharges + $loan->net_loan;
        // $grossLoan = $principalAmount   = $totalCharges + $loan->net_loan;
        // $grossLoan	        = interest($principalAmount, $loanInterest->rate, $loanInterest->duration);
        
        return [
        	'netloan'				    => $loan->net_loan,
        	'fixedCharges' 				=> $fixedCharges,
        	'chargesOnBorrowedAmount'	=> $chargesOnBorrowedAmount,
        	'totalCharges'				=> $totalCharges,
        	// 'principalAmount'			=> $principalAmount,
        	'grossLoan'		            => $loan->interest->gross_loan,
        	'chargesBreakdown'			=> $chargesBreakdown,
        	'loanInterest'				=> $loanInterest,
        	'loan'						=> $loan
        ];
    }
}
