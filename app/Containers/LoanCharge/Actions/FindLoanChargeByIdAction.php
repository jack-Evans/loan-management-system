<?php

namespace App\Containers\LoanCharge\Actions;

use App\Ship\Parents\Actions\Action;
use App\Ship\Parents\Requests\Request;
use Apiato\Core\Foundation\Facades\Apiato;

class FindLoanChargeByIdAction extends Action
{
    public function run(Request $request)
    {
        $loancharge = Apiato::call('LoanCharge@FindLoanChargeByIdTask', [$request->id]);

        return $loancharge;
    }
}
