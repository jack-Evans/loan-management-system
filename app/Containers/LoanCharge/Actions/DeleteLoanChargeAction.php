<?php

namespace App\Containers\LoanCharge\Actions;

use App\Ship\Parents\Actions\Action;
use App\Ship\Parents\Requests\Request;
use Apiato\Core\Foundation\Facades\Apiato;

class DeleteLoanChargeAction extends Action
{
    public function run(Request $request)
    {
        return Apiato::call('LoanCharge@DeleteLoanChargeTask', [$request->loan_id, $request->charge_id]);
    }
}
