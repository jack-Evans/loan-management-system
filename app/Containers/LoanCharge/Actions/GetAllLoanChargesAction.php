<?php

namespace App\Containers\LoanCharge\Actions;

use App\Ship\Parents\Actions\Action;
use App\Ship\Parents\Requests\Request;
use Apiato\Core\Foundation\Facades\Apiato;

class GetAllLoanChargesAction extends Action
{
    public function run(Request $request)
    {
        $loancharges = Apiato::call('LoanCharge@GetAllLoanChargesTask', [], ['addRequestCriteria']);

        return $loancharges;
    }
}
