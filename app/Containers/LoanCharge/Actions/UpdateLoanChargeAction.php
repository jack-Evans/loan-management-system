<?php

namespace App\Containers\LoanCharge\Actions;

use App\Ship\Parents\Actions\Action;
use App\Ship\Parents\Requests\Request;
use Apiato\Core\Foundation\Facades\Apiato;

class UpdateLoanChargeAction extends Action
{
    public function run(Request $request)
    {
        $data = $request->sanitizeInput([
            // add your request data here
        ]);

        $loancharge = Apiato::call('LoanCharge@UpdateLoanChargeTask', [$request->id, $data]);

        return $loancharge;
    }
}
