<?php

namespace App\Containers\LoanCharge\Actions;

use App\Ship\Parents\Actions\Action;
use App\Ship\Parents\Requests\Request;
use Apiato\Core\Foundation\Facades\Apiato;
use App\Containers\LoanCharge\Data\Transporters\CreateLoanChargeTransporter;
use App\Containers\Loan\Exceptions\LoadAlreadyIssuedException;
use Carbon\Carbon;

class CreateLoanChargeAction extends Action
{
    public function run(Request $request, $excelRequest = false)
    {
        $loanChargeRequest = [
        	'loan_id' 		=> $request->loanid,
        	'charge_id'		=> $request->chargeid,
        	// 'description'   => $request->description,
        ];

        $request->id = $request->loanid;

        $loan = Apiato::call('Loan@FindLoanByIdTask', [$request->loanid]);

        Apiato::call('LoanCharge@AssignLoanChargePermissionTask', [$loan]);        
//        if($loan->status == 'approved') {
//            throw new LoadAlreadyIssuedException();
//        }
    	\DB::beginTransaction();
        
        $charge = Apiato::call('Charge@FindChargeByIdTask', [$request->chargeid]);
        
        if($request->value)
            $chargeValue = $request->value;
        else
            $chargeValue = $charge->value;
        // if($charge->charge_type === 'percentage' && $loan->loan_type === 'serviced' && $excelRequest == false)
        if($charge->charge_type === 'percentage' && in_array($loan->loan_type, ['serviced', 'rolled']) && $excelRequest == false)
            $chargeValue = round(resolvePercentage($loan->net_loan) * $request->value, 2);
        
        $loanChargeRequest['value'] = $chargeValue;
        $loancharge = Apiato::call('LoanCharge@CreateLoanChargeTask', [(new CreateLoanChargeTransporter($loanChargeRequest))->toArray()]);
        // if($loan->loan_type === 'serviced')
        if(in_array($loan->loan_type, ['serviced', 'rolled']))
            $interestRequest = [
                'gross_loan' => $loan->interest->gross_loan + $chargeValue,
            ];
        else
            $interestRequest = [
                'principal_amount' => $loan->interest->principal_amount - $chargeValue,
            ];
        
        $interest = Apiato::call('Interest@UpdateInterestTask', [($loan->interest->id), $interestRequest]);
        
        $journalPostingRequest = [
            'creditAccountId' => 0,
            'debitAccountId' => $loan->customer_id,
            'type' => 'charge',
            'loanid' => $loan->id,
            'tnxDate' => $loan->issue_at,
            'amount' => $chargeValue,
            'accountingPeriod' => Carbon::parse($request->date)->format('M Y'),            
            'assetType' => 'charge',
            'description' => $charge->name,
            'payableId' => $loancharge->id,
            'payableType' => 'App\Containers\LoanCharge\Models\LoanCharge',
            'nominalCode' => $request->nominalCode,
        ];        
                
        $journal = Apiato::call('Payment@CreateJournalPostingTask', [$journalPostingRequest]);
        
        \DB::commit();

        return $loancharge;
    }
}
