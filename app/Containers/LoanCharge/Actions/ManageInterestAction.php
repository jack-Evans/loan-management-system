<?php

namespace App\Containers\LoanCharge\Actions;

use App\Ship\Parents\Actions\Action;
use App\Ship\Parents\Requests\Request;
use Apiato\Core\Foundation\Facades\Apiato;

class ManageInterestAction extends Action
{
    public function run(Request $request)
    {
        
        $loanChargesDetails = Apiato::call('LoanCharge@CalculateLoanChargeAction', [$request]);
    	                
    	$interestRequest = [
            // 'principal_amount'	    => $loanChargesDetails['principalAmount'],
            'gross_loan'            => $loanChargesDetails['grossLoan'],
    	];
        
        $interest = Apiato::call('interest@UpdateInterestTask', [($loanChargesDetails['loanInterest']->id), $interestRequest]);                

        return $interest;
    }
}