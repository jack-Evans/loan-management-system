<?php

namespace App\Containers\LoanCharge\Tasks;

use App\Ship\Parents\Tasks\Task;
use App\Containers\Loan\Models\Loan;
use Illuminate\Database\Eloquent\Collection;
use App\Containers\LoanCharge\Data\Repositories\LoanChargeRepository;
use App\Containers\Payment\Data\Repositories\PostRepository;
use App\Containers\Payment\Data\Repositories\JournalRepository;

class SumOfLoanChargesTask extends Task
{
    
    protected $repository;
    protected $journalRepository;
    protected $postingRepository;

    public function __construct(LoanChargeRepository $repository, JournalRepository $journalRepository, PostRepository $postingRepository)
    {
        $this->repository = $repository;
        $this->journalRepository = $journalRepository;
        $this->postingRepository = $postingRepository;
    }

    public function run($loanId)
    {
        $debit = 0;
        $charges = $this->repository->scopeQuery(function($q) use($loanId) {
            return $q->where('loan_id', $loanId);
        })->get();
        
        foreach($charges as $charge)
        {
            $journal = $this->journalRepository->with(['posts'])->findWhere(['loan_id' => $charge->loan_id, 'payable_id' => $charge->charge_id]);
            $debit += $journal[0]->posts[0]->amount;
        }
        
        return $debit;
    }
}
