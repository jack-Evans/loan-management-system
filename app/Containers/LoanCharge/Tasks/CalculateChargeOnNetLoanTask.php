<?php

namespace App\Containers\LoanCharge\Tasks;

use App\Ship\Parents\Tasks\Task;
use Illuminate\Database\Eloquent\Collection;
use App\Containers\Loan\Models\Loan;

class CalculateChargeOnNetLoanTask extends Task
{

    public function __construct()
    {
        // ..
    }

    public function run(Collection $loanCharges, Loan $loan)
    {
        return $loanCharges->sum(function($charge) use ($loan){
    		if($charge->charge_type == 'percentage' && $charge->amount_type == 'netloan')
    			return ($charge->value * $loan->net_loan)/100;
    		return 0;
    	});
    }
}
