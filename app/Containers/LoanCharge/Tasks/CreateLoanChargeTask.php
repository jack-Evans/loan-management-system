<?php

namespace App\Containers\LoanCharge\Tasks;

use App\Containers\LoanCharge\Data\Repositories\LoanChargeRepository;
use App\Ship\Exceptions\CreateResourceFailedException;
use App\Ship\Parents\Tasks\Task;
use Exception;

class CreateLoanChargeTask extends Task
{

    protected $repository;

    public function __construct(LoanChargeRepository $repository)
    {
        $this->repository = $repository;
    }

    public function run(array $data)
    {
        try {
            return $this->repository->create($data);
        }
        catch (Exception $exception) {
            throw new CreateResourceFailedException();
        }
    }
}
