<?php

namespace App\Containers\LoanCharge\Tasks;

use App\Containers\LoanCharge\Data\Repositories\LoanChargeRepository;
use App\Ship\Exceptions\DeleteResourceFailedException;
use App\Ship\Parents\Tasks\Task;
use App\Containers\Loan\Models\Loan;
use Exception;
use App\Containers\Payment\Data\Repositories\DefaultRateRefundRepository;
use Apiato\Core\Foundation\Facades\Apiato;
use App\Containers\Payment\Exceptions\HTTPPreConditionFailedException;

class AssignLoanChargePermissionTask extends Task
{

    protected $refaultRateRefundRepository;

    public function __construct(DefaultRateRefundRepository $refaultRateRefundRepository)
    {
        $this->refaultRateRefundRepository = $refaultRateRefundRepository;
    }

    public function run(Loan $loan)
    {
        if($loan->loan_type != 'retained')
        {
            $defaultRateInterest = $this->refaultRateRefundRepository->findWhere(['loan_id' => $loan->id])->first();
            $interests = Apiato::call('Interest@GetInterestsAsPmtPlanVersionsTask', [$loan->id]);
            $otherCharges = Apiato::call('OtherCharge@GetAllOtherChargesTask', [$loan->id]);
            $payments = Apiato::call('Payment@GetAllPaymentsTask', [$loan]);
            // $refunds = Apiato::call('Payment@GetAllRefundsTask', [$loan->id]);

            if($payments->first() || count($interests) > 1 || $otherCharges->first() || $defaultRateInterest)
                throw new HTTPPreConditionFailedException("Initial loan charges can only be applied before a statement has been issued.");
        }
    }
}
