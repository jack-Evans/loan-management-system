<?php

namespace App\Containers\LoanCharge\Tasks;

use App\Ship\Parents\Tasks\Task;
use Illuminate\Database\Eloquent\Collection;

class CalculateFixedChargesTask extends Task
{

    public function __construct()
    {
        // ..
    }

    public function run(Collection $loanCharges)
    {
        return $loanCharges->sum(function($charge){
    		if($charge->charge_type == 'fixed')
    			return $charge->value;
    		return 0;
    	});
    }
}
