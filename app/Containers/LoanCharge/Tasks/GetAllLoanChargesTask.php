<?php

namespace App\Containers\LoanCharge\Tasks;

use App\Containers\LoanCharge\Data\Repositories\LoanChargeRepository;
use App\Ship\Parents\Tasks\Task;

class GetAllLoanChargesTask extends Task
{

    protected $repository;

    public function __construct(LoanChargeRepository $repository)
    {
        $this->repository = $repository;
    }

    public function run()
    {
        return $this->repository->paginate();
    }
}
