<?php

namespace App\Containers\LoanCharge\Tasks;

use App\Containers\LoanCharge\Data\Repositories\LoanChargeRepository;
use App\Ship\Exceptions\UpdateResourceFailedException;
use App\Ship\Parents\Tasks\Task;
use Exception;

class UpdateLoanChargeTask extends Task
{

    protected $repository;

    public function __construct(LoanChargeRepository $repository)
    {
        $this->repository = $repository;
    }

    public function run($id, array $data)
    {
        try {
            return $this->repository->update($data, $id);
        }
        catch (Exception $exception) {
            throw new UpdateResourceFailedException();
        }
    }
}
