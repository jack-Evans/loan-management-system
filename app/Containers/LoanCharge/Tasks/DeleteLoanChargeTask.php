<?php

namespace App\Containers\LoanCharge\Tasks;

use App\Containers\LoanCharge\Data\Repositories\LoanChargeRepository;
use App\Ship\Exceptions\DeleteResourceFailedException;
use App\Ship\Parents\Tasks\Task;
use Exception;

class DeleteLoanChargeTask extends Task
{

    protected $repository;

    public function __construct(LoanChargeRepository $repository)
    {
        $this->repository = $repository;
    }

    public function run($loan_id, $charge_id)
    {
//        dd($loan_id, $charge_id);
        try {
            return $this->repository->deleteWhere(['loan_id' => $loan_id, 'charge_id' => $charge_id]);
        }
        catch (Exception $exception) {
            throw new DeleteResourceFailedException();
        }
    }
}
