<?php

namespace App\Containers\LoanCharge\Tasks;

use App\Ship\Parents\Tasks\Task;
use App\Containers\Loan\Models\Loan;
use Illuminate\Database\Eloquent\Collection;

class GetLoanChargeBreakDownTask extends Task
{

    public function __construct()
    {
        // ..
    }

    public function run(Collection $loanCharges, Loan $loan)
    {
        foreach($loanCharges as $loanCharge) {
        	if($loanCharge->charge_type == 'fixed') {
        		$loanCharge->calculatedChargeAmount = $loanCharge->value;
        	}

        	if($loanCharge->charge_type == 'percentage' && $loanCharge->amount_type == 'netloan') {
        		$loanCharge->calculatedChargeAmount = ($loanCharge->value * $loan->net_loan)/100;
        	}
        }
        return $loanCharges;
    }
}
