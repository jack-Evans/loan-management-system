<?php

namespace App\Containers\LoanCharge\Tasks;

use App\Containers\LoanCharge\Data\Repositories\LoanChargeRepository;
use App\Ship\Exceptions\NotFoundException;
use App\Ship\Parents\Tasks\Task;
use Exception;

class FindLoanChargeByIdTask extends Task
{

    protected $repository;

    public function __construct(LoanChargeRepository $repository)
    {
        $this->repository = $repository;
    }

    public function run($id)
    {
        // dd($this->repository->with(['charge'])->findWhere(['loan_id' => $id])->toArray());
        try {
            return $this->repository->with(['charge'])->findWhere(['loan_id' => $id]);
        }
        catch (Exception $exception) {
            throw new NotFoundException();
        }
    }
}
