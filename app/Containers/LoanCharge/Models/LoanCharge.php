<?php

namespace App\Containers\LoanCharge\Models;

use App\Ship\Parents\Models\Model;

class LoanCharge extends Model
{
    protected $fillable = [
        'loan_id',
        'charge_id',
        'value',
        'description',
    ];

    protected $attributes = [

    ];

    protected $hidden = [

    ];

    protected $casts = [

    ];

    protected $dates = [
        'created_at',
        'updated_at',
    ];

    /**
     * A resource key to be used by the the JSON API Serializer responses.
     */
    protected $resourceKey = 'loancharges';


    public function charge() {
        return $this->belongsTo(\App\Containers\Charge\Models\Charge::class);
    }
    public function journal()
    {
        // return $this->hasOne(\App\Containers\Payment\Models\Journal::class, 'payable');
        return $this->morphOne(\App\Containers\Payment\Models\Journal::class, 'payable');
    }
}
