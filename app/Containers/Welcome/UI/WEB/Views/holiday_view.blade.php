@extends('layouts.app')

@section('title', 'Holidays')

@section('content')
<div class="col-lg-12">
    <div class="has-bg">
    	<h3>
            Holidays
            <a href="{{url('holidays/create')}}" class="pull-right">
              <button class="btn btn-success">Create Holiday</button>
            </a>
        </h3>
        <span class="space"></span>
        <table class="table table-striped table-hover table-bordered text-left MDBootstrapDatatable" cellspacing="0" width="100%">
            <thead>
                <tr>
    	            <th>Name</th>
    	            <th>Date</th>
                </tr>
            </thead>
            <tbody>
                @foreach($holidays as $key => $holiday)
                <tr>
                    <td>{{$holiday->name}}</td>
                    <td>{{\Carbon\Carbon::parse($holiday->holiday_date)->format('M d Y')}}</td>
                    <!-- <td>
                        <a href="{{url('charges/'.$holiday->id.'/edit')}}">
                        <button class="btn btn-primary">Edit</button>
                        </a>
                    </td>
                    <td>
                        <a href="{{ url('charges/'.$holiday->id).'/delete' }}">
                            <button class="btn btn-danger">Delete</button>
                        </a>
                    </td> -->
                </tr>
                @endforeach
            </tbody>
        </table>
    </div>
</div>
@endsection