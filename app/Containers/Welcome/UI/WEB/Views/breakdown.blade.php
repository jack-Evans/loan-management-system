<tr class="show-description{{$key}} font-weight-bold font-italic"  style="display: none; background-color: #efefef; font-size: 12px;">
    <td>From</td>
    <td>To</td>
    <td>Days @if($selectedLoan->loantype === 'retained') (Months) @endif</td>
    <td>Amount</td>
    <td>Interest Rate</td>
    <td>Daily Charges(Per day charge)</td>
    <td>Total(Days * Daily Charges)</td>
    @if(isset($statement->interestBreakDown[0]->rounding_error))
    <td>Rounding Error</td>
    <td></td>
    @else
    <td></td>
    <td></td>
    @endif
    <td></td>
    <td></td>
</tr>
@foreach($interestBreakDown as $breakDown)
	<tr class="font-italic show-description{{$key}} @if($breakDown->rate == $selectedLoan->interest->default_rate && $breakDown->total >0) text-danger @elseif($breakDown->total <= 0) text-success @endif"  style="display: none; background-color: #efefef; font-size: 12px;">
		<td>{{\Carbon\Carbon::parse($breakDown->start->date)->format('Y M d')}}</td>
		<td>{{\Carbon\Carbon::parse($breakDown->end->date)->format('Y M d')}}</td>
		<td>{{$breakDown->days}}
		  @if($selectedLoan->loantype === 'retained')
		    @php
			    $date1 = \Carbon\Carbon::parse($breakDown->start->date);
			    $date2 = \Carbon\Carbon::parse($breakDown->end->date)->copy()->addDay();
			    $interval = date_diff($date1, $date2);
			    // dd($interval->format('%m'));
			    $months = $date1->diffInMonths($date2);
			    $days = $date1->copy()->addMonths($months)->diffInDays($date2);
			    $days += $days ? 1 : 0;
		    @endphp
		    ({{round($days/30.4, 2) + $months}})
		    <!--({{ ($interval->format('%m') == 0) ? ($interval->format('%d') == $date1->copy()->startOfMonth()->diffInDays($date1->copy()->endOfMonth())) ? 1 : 0 : $interval->format('%m') }}) -->
		  @endif
		</td>
		<td>{{number_format($breakDown->balance, 2)}}</td>
		<td>{{(float)$breakDown->rate}}</td>
		<td>{{(float)$breakDown->daily_rate}}</td>
		<td>{{number_format($breakDown->total, 2)}}</td>
		@if(isset($breakDown->rounding_error))
		<!-- {{--<td>{{$breakDown->rounding_error ? $breakDown->rounding_error : null}}</td>--}} -->
		<td>{{$breakDown->rounding_error ? $breakDown->rounding_error : $breakDown->rounding_error}}</td>
		<td></td>
		@else
		<td></td>
		<td></td>
		@endif
		<td></td>
		<td></td>
	</tr>
@endforeach