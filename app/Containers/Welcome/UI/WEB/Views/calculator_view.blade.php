@extends('layouts.app')

@section('title', 'Calculations')

@section('wrapper1')

<!-- if(isset($loans)) -->
<!-- Infinite condition to hide loans at calculations page -->
@if(0)
<div class="main-wrapper">
  <div class="container">
    <div class="row">
      <div class="col-lg-12">
        <div class="has-bg">
            <h5 class="panel-title">
              Service Loans
            </h5>
            <table class="table table-striped table-hover table-bordered no-mar-b text-left">
              <thead>
                <tr>
                  <th>Loan Id</th>
                  <th>Issue Date</th>
                  <th>Net Loan</th>
                  <th>Gross Loan</th>
                  <th>Loan Type</th>
                  <th>Holiday auto refund</th>
                  <!--<th>Balance</th>-->
                  <th></th>
                </tr>
              </thead>
              <tbody>
                @foreach($loans as $key => $loan)
                @if($loan->loantype != 'retained')
                <tr>
                  <td>{{$loan->loanid}}</td>
                  <td>{{\Carbon\Carbon::parse($loan->issueat->date)->format('Y M d')}}</td>
                  <td>{{number_format($loan->netloan, 2)}}</td>
                  <td>{{number_format($loan->interest->gross_loan, 2)}}</td>
                  <td class="text-capitalize">{{$loan->loantype}}</td>
                  <td>{{ ($loan->holidayrefund == 1) ? 'Yes': 'No'}}</td>
                  <!--<td>{{ $loan->interest->gross_loan }}</td>-->
                  <td>
                    <!-- <a href="{{url('calculations/deleteloan/'.$loan->id)}}" class="btn btn-danger ml-1 float-right" >
                            Delete
                    </a> -->
                    <!-- <button type="button" class="btn btn-primary ml-1 float-right loanModal" data-toggle="modal"
                            data-target="#updateLoanModal" data-loanid="{{$loan->id}}" data-netloan="{{$loan->netloan}}" data-rate="{{$loan->interest->rate}}" data-defaultrate="{{$loan->interest->default_rate}}" data-duration="{{$loan->interest->duration}}"
                            data-holidayrefund="{{$loan->holidayrefund}}">
                        Edit
                    </button> -->
                    <a href="{{url('calculations?loanid='.$loan->id)}}" class="btn btn-info ml-1 float-right" >
                        View
                    </a>
                  </td>
                </tr>
                @endif
                @endforeach
              </tbody>
            </table>
        </div>
      </div>
  </div>
</div>

<div class="container">
    <div class="row">
      <div class="col-lg-12">
        <div class="has-bg">
            <h5 class="panel-title">
              Retain Loans
            </h5>
            <table class="table table-striped table-hover table-bordered no-mar-b text-left">
              <thead>
                <tr>
                  <th>Loan Id</th>
                  <th>Issue Date</th>
                  <th>Net Loan</th>
                  <th>Gross Loan</th>
                  <th>Loan Type</th>
                  <th>Holiday auto refund</th>
                  <!--<th>Balance</th>-->
                  <th></th>
                </tr>
              </thead>
              <tbody>
                @foreach($loans as $key => $loan)
                @if($loan->loantype === 'retained')
                <tr>
                  <td>{{$loan->loanid}}</td>
                  <td>{{\Carbon\Carbon::parse($loan->issueat->date)->format('Y M d')}}</td>
                  <td>{{number_format($loan->netloan, 2)}}</td>
                  <td>{{number_format($loan->interest->gross_loan, 2)}}</td>
                  <td class="text-capitalize">{{$loan->loantype}}</td>
                  <td>{{ ($loan->holidayrefund == 1) ? 'Yes': 'No'}}</td>
                  <!--<td>{{ $loan->interest->gross_loan }}</td>-->
                  <td>
                    <!-- <a href="{{url('calculations/deleteloan/'.$loan->id)}}" class="btn btn-danger ml-1 float-right" >
                            Delete
                    </a> -->
                    <!-- <button type="button" class="btn btn-primary ml-1 float-right loanModal" data-toggle="modal"
                            data-target="#updateLoanModal" data-loanid="{{$loan->id}}" data-netloan="{{$loan->netloan}}" data-rate="{{$loan->interest->rate}}" data-defaultrate="{{$loan->interest->default_rate}}" data-duration="{{$loan->interest->duration}}"
                            data-holidayrefund="{{$loan->holidayrefund}}">
                        Edit
                    </button> -->
                    <a href="{{url('calculations?loanid='.$loan->id)}}" class="btn btn-info ml-1 float-right" >
                        View
                    </a>
                  </td>
                </tr>
                @endif
                @endforeach
              </tbody>
            </table>
          @include('includes.pagination', ['page' => 'calculations'])
        </div>
      </div>
  </div>
</div>
@endif

@endsection
@section('content')
@if($selectedLoan)
<div class="col-lg-12 ">
  <div class="has-bg button_holder">
    <div class="left-side-button">
      <!-- <button type="button" class="btn btn-primary ml-1 createnewLoanModal" data-toggle="modal" data-target="#createnewLoanModal">
        Create Serviced Loan
      </button>
      <button type="button" class="btn btn-primary ml-1 createRetainedLoanModal" data-toggle="modal" data-target="#createRetainedLoanModal">
        Create Retained Loan
      </button> -->
      <!-- {{-- @if($selectedLoan->loantype === 'serviced') --}} -->
      @if(in_array($selectedLoan->loantype, ['serviced', 'rolled']))
        <button type="button" class="btn btn-primary ml-1 transferModal" data-toggle="modal"
          data-target="#transferModal">
          Transfer to other Loan
        </button>
        @if($selectedLoan && $selectedLoan->status == 'approved')
        <button type="button" class="btn btn-primary freezeLoanModal ml-1" data-toggle="modal" data-target="#freezeLoanModal">
          Freeze Loan
        </button>
        @elseif($selectedLoan->status == 'freezed')
        <button type="button" class="btn btn-primary ml-1" data-toggle="modal" data-target="#unFreezeLoanModal">
          Un-Freeze Loan
        </button>
        @endif
        <button type="button" class="btn btn-primary createPaymentPlanModal ml-1" data-toggle="modal" data-target="#createPaymentPlanModal" data-loanid="{{session('loanid')}}">
          Create Payment Plan
        </button>
        <button type="button" class="btn btn-primary ml-1 DefaultRateInterests" data-toggle="modal"
          data-target="#DefaultRateInterests">
          Default Rate Interests
        </button>
      @endif

      @if($selectedLoan->loantype === 'half')
        <button type="button" class="btn btn-primary ml-1 DefaultRateInterests" data-toggle="modal"
          data-target="#DefaultRateInterests">
          Default Rate Interests
        </button>
      @endif
        <button type="button" class="btn btn-primary ml-1 otherChargesdisplay" data-toggle="modal" data-target="#otherChargesdisplay">
          Other Charges
        </button>
        <button type="button" class="btn btn-primary ml-1 Paid-Amounts" data-toggle="modal"
          data-target="#Paid-Amounts">
          Payments
        </button>

        <button type="button" class="btn btn-primary ml-1 updateLoanDurationModal" data-toggle="modal"
          data-target="#updateLoanDurationModal">
          Re-new Loan
        </button>

        <button type="button" class="btn btn-primary ml-1 Generate-Statement" data-toggle="modal" data-target="#Generate-Statement">
          Generate Statement
        </button>

        <button type="button" class="btn btn-primary ml-1 ClosingChargesModal" data-toggle="modal" data-target="#ClosingChargesModal">
          Closing Charges
        </button>

        <button type="button" class="btn btn-primary ml-1" data-toggle="modal" data-target="#closeLoanModal">
          Close Loan Calculator
        </button>

        <button type="button" class="btn btn-primary ml-1" data-toggle="modal" data-target="#tranchModal">
          Tranchs
        </button>


      @if($selectedLoan->loantype === 'retained')
        <!-- <button type="button" class="btn btn-primary ml-1 convertToServiceLoan" data-toggle="modal"
          data-target="#convertToServiceLoan">
          Convert to Service loan
        </button> -->
      @endif
      <button type="button" class="btn btn-primary ml-1 Fillers_" data-toggle="modal"
        data-target="#Fillers_">
        Fillers
      </button>
      <button type="button" class="btn btn-primary ml-1" data-toggle="modal"
        data-target="#grace">
        Manage Graces
      </button>

    </div>
    <!-- {{-- @if($selectedLoan->loantype === 'serviced') --}} -->
    @if(in_array($selectedLoan->loantype, ['serviced', 'rolled']))
      <div class="right-side-button">
        <a class="btn btn-danger pull-right js-close-loan ml-1" data-href="{{url('updateloanstatus?loanId='.session('loanid').'&status=closed')}}">
          Close loan
        </a>
        <a class="btn btn-danger pull-right ml-1" href="{{url('deletepaymentplan')}}">
            Delete Last Payment Plan
        </a>
      </div>
    @endif
  </div>
</div>
@endif
<!-- <span class="space padding-10"></span> -->
@if($selectedLoan)

<div class="col-lg-12">
  <div class="has-bg">
    <!-- selected loan -->
    <div class="has-box-shadow">
      <div class="row mb-2">
        <div class="col-6">
          <h6 class="panel-title">
              Selected Loan
              <!-- <div class="target-description" data-key="SelectedLoan">
                <span class="text-primary fas fa-chevron-up cursor"></span>
              </div> -->
          </h6>
        </div>
        <div class="col-6">
          <button type="button" class="btn btn-success ml-1 pull-right" data-toggle="modal"
            data-target="#exportLoanModal">
            Export Loan
          </button>
        </div>
      </div>
      <!-- <div class="show-descriptionSelectedLoan hidden"> -->
        <div class="panel-body">
          <table class="table table-striped table-hover table-bordered text-left m-0">
            <thead>
              <tr>
                <th>Loan Id</th>
                <th>Net Loan</th>
                <th>Gross Loan</th>
                <th>Loan Type</th>
                <th>Rate</th>
                <th>Default Rate</th>
                <th>Issued At</th>
                <th>Duration</th>
                @if($selectedLoan->loantype === 'half')
                <th>Retained loan duration</th>
                <th>Serviced loan duration</th>
                @endif
                <th>Minimum Term</th>
                <th>Holiday Auto Refund</th>
                <th>Customer</th>
              </tr>
            </thead>
            <tbody>
              <tr>
                <td>{{$selectedLoan->loanid}}</td>
                <td>{{number_format($selectedLoan->netloan, 2)}}</td>
                <td>{{number_format($selectedLoan->interest->gross_loan, 2)}}</td>
                <td class="text-capitalize">{{$selectedLoan->loantype}}</td>
                <td>{{number_format($selectedLoan->interest->rate, 2)}}</td>
                <td>{{(float)$selectedLoan->interest->default_rate}}</td>
                <td>{{\Carbon\Carbon::parse($selectedLoan->issueat->date)->format('Y M d')}}</td>
                <td>{{$selectedLoan->interest->duration}}</td>
                @if($selectedLoan->loantype === 'half')
                <td>{{$selectedLoan->interest->half_loan_duration}}</td>
                <td>{{$selectedLoan->interest->duration - $selectedLoan->interest->half_loan_duration}}</td>
                @endif
                <td>{{(float)$selectedLoan->interest->min_term}}</td>
                <td>{{($selectedLoan->holidayrefund == 1) ? 'Yes': 'No'}}</td>
                <td>{{$selectedLoan->customer->username}}</td>
              </tr>
            </tbody>
          </table>
        </div>
        <!-- {{-- @if($selectedLoan->loantype === 'serviced' || $selectedLoan->loantype === 'retained') --}} -->
        @if(in_array($selectedLoan->loantype, ['serviced', 'retained', 'rolled']))
          <!-- Minimum Term Amount -->
          <div class="has-box-shadow mt-2">
            <h6 class="panel-title">
                Minimum Term Amount
            </h6>
            <div class="panel-body">
              <table class="table table-hover table-bordered text-left m-0">
                <thead>
                  <tr>
                    <th>Minimum Term</th>
                    <th>Monthly Payment</th>
                    <th>Amount Paid</th>
                    <th>Amount Pending</th>
                    <th>Total Minimum Term Amount</th>
                    <th></th>
                  </tr>
                </thead>
                @if($minTermBalance)
                <tbody>
                  <tr>
                    <td>{{(float)$minTermBalance->min_term}}</td>
                    <td>{{number_format($minTermBalance->monthly_payment, 2)}}</td>
                    <td>{{number_format($minTermBalance->amount_paid, 2)}}</td>
                    <td>{{number_format($minTermBalance->min_term_balance, 2)}}</td>
                    <td>{{number_format($minTermBalance->total_minterm_amount, 2)}}</td>
                    <td>
                      <button type="button" class="btn btn-primary custom-padding float-right minTermModal" data-toggle="modal" data-target="#minTermModal" data-loanid="{{session('loanid')}}" data-minterm="{{(float)$minTermBalance->min_term}}" data-mpmt="{{(float)$minTermBalance->monthly_payment}}" data-amount="{{(float)$minTermBalance->min_term_balance}}" data-amountpaid="{{(float)$minTermBalance->amount_paid}}">
                        Edit
                      </button>
                    </td>
                  </tr>
                </tbody>
                @endif
              </table>
            </div>
          </div>
          @if(sizeof(collect($statements)->whereNotIn('description', ['loan', 'charge', 'interest', 'refund', 'retained_interest'])) == 0)
            <div class="has-box-shadow mt-2">
              <div class="panel panel-default">
                <h6 class="panel-title">Initial Charges</h6>
              </div>
              @if($charges && (session('loanid') != null))
                <div class="panel-body">
                  <table class="table table-striped table-hover table-bordered text-left m-0">
                    <thead>
                      <tr>
                        <th>Name</th>
                        <th>Value</th>
                        <th>Charge Type</th>
                        <th></th>
                      </tr>
                    </thead>
                    <tbody>
                      @foreach($charges as $key => $charge)
                      <tr>
                        <td>{{$charge->name}}</td>
                        <td>{{number_format($charge->value, 2)}}</td>
                        <td>{{$charge->chargetype}}</td>
                        <td>
                          <button type="button" class="btn btn-primary float-right assignChargeModal" data-toggle="modal" data-target="#assignChargeModal" data-chargeid="{{$charge->id}}" data-loanid="{{session('loanid')}}" data-value="{{$charge->value}}">
                              Add Charge
                          </button>
                            <!-- <a class="btn btn-primary float-right" href="{{url('calculations/assigncharge?chargeId='.$charge->id.'&loanId='.session('loanid'))}}">Assign</a> -->
                        </td>
                      </tr>
                      @endforeach
                    </tbody>
                  </table>
                </div>
              @endif
            </div>
          @endif 
        @endif
        <!-- {{-- @if($selectedLoan->loantype === 'serviced' || $selectedLoan->loantype === 'retained') --}} -->
        @if(in_array($selectedLoan->loantype, ['serviced', 'retained', 'rolled']))
          <div class="has-box-shadow mt-2">
            <div class="panel panel-default">
              <h6 class="panel-title">
                  Added Initial Charges
              </h6>
            </div>
            <div class="panel-body">
              <table class="table table-striped table-hover table-bordered text-left m-0">
                <thead>
                  <tr>
                    <th>Name</th>
                    <th>Value</th>
                    <th>Charge Type</th>
                    <th></th>
                  </tr>
                </thead>
                <tbody>
                  @foreach($assignedCharges as $key => $charge)
                  <tr>
                      <td>{{$charge->name}}</td>
                      <td>{{number_format($charge->value, 2)}}</td>
                      <td>{{$charge->chargeType}}</td>
                      <td>
                        <button type="button" class="btn btn-default float-right refundModal" data-toggle="modal"
                          data-target="#refundModal"
                          data-chargeId="{{$charge->id}}"
                          data-value="{{(float)$charge->value}}"
                          data-chargedate="{{\Carbon\Carbon::parse($selectedLoan->issueat->date)->format('Y-m-d')}}"
                          >
                          Refund
                        </button>
                      </td>
                  </tr>
                  @endforeach
                </tbody>
              </table>
            </div>
          </div>
        @endif
      <!-- </div> -->
    </div>
  </div>
</div>

<div class="col-lg-12">
  <div class="has-bg">
    <div class="has-box-shadow">
        <form action="{{url('calculations')}}" method="get">
            <div class="row">
                <div class="col-3">
                    <b class="text-info">Today made up date:</b>
                    <input class="form-control datepicker4" autocomplete="off" name="madeupdate" value="{{isset($_GET['madeupdate']) ? $_GET['madeupdate'] : ''}}">
                </div>
                <div class="col-1">
                    <button type="submit" class="btn btn-success custom-padding mt-4">Go</button>
                </div>
                <div class="col-2 align-center">
                  <div class="">
                    <b class="text-success">Current Balance:</b>
                    <p class="text-success m-0">{{$currentBalance > 0 ? number_format($currentBalance, 2) : '' }}</p>
                    </div>
                </div>
                @if($selectedLoan && $selectedLoan->loantype == 'manual' && count($statements))
                <div class="col-2 align-center">
                  @php
                    $issueAt = \Carbon\Carbon::parse($selectedLoan->issueat->date);
                    $issueDay = $issueAt->copy()->subDay()->day;
                    $stmtDate = \Carbon\Carbon::parse(collect($statements)->last()->created_at->date);
                    if($issueAt->diffInMonths($stmtDate) >= $selectedLoan->interest->duration)
                      $dueDate = $issueAt->copy()->addMonths($selectedLoan->interest->duration)->subDay()->format('Y M d');
                    else
                      $dueDate = ($issueAt->month < $stmtDate->month || $issueAt->year < $stmtDate->year) ? $stmtDate->format('Y M').' '.$issueDay : $issueAt->copy()->addMonth()->subDay()->format('Y M d');
                  @endphp
                    <b class="text-info">Due Date:</b>
                    <p class="text-info m-0">{{$dueDate}}</p>
                </div>
                @endif
                <div class="col-4 align-right">
                  <p>Allow/Restrict Previous Months Statement</p>
                  <label class="switch">
                    <input class="verifyStatementDate" type="checkbox" value="@if($verifyStatementDate) {{$verifyStatementDate->value}} @else 0 @endif" 
                    @if($verifyStatementDate) {{($verifyStatementDate->value) ? 'checked' : ''}} @else '' @endif>
                    <span class="slider round"></span>
                  </label>
                </div>
                <!-- $stmtDate = \Carbon\Carbon::parse(collect($statements)->last()->created_at->date);
                    $duration = $selectedLoan->interest->duration;
                    $monthsDiff = $issueAt->diffInMonths($stmtDate);
                    $addMonths = ($monthsDiff > $duration) ? $duration : $monthsDiff;
                    dd($addMonths); -->
            </div>
        </form>
    </div>
  </div>
</div>
@endif
<!-- Only if we have a selected loanid -->

<!-- normal transactions -->
<div class="col-lg-12">
  <div class="has-bg">
    <div class="has-box-shadow">
      <h5 class="text-primary panel-title">
          Transactions
          <span class="pull-right ml-1 mb-4">
            <span class="text-dark">{{(session('showAllTnx')) ? 'Hide hidden' : 'Show hidden' }} transactions:</span>
            <label class="switch">
              <input class="js-showAllTnx" type="checkbox" value="{{(session('showAllTnx')) ? '1' : '0' }}" {{(session('showAllTnx')) ? 'checked' : '' }}>
              <span class="slider round"></span>
            </label>
          </span>
          <span class="cursor pull-right mr-3" data-toggle="modal" data-target="#hiddenStatementsModal" aria-hidden="true">
            Hidden transactions: {{count(collect($statements)->where('hide', 1)->where('hideTransaction', 1)->all())}}
          </span>
      </h5>
      <div class="panel-body">
        <table class="table table-striped table-hover table-bordered text-left m-0">
          <thead>
            <tr>
              <th>Date</th>
              <th>Debit</th>
              <th>Credit</th>
              <th>Balance</th>
              <th>Descriptions</th>
              <th colspan="2"></th>
              <th>Nominal Code</th>
              <th>Transaction Id</th>
              <th>Hide(Statement/Transaction)</th>
              <th>Notes</th>
              <th></th>
            </tr>
          </thead>
          <tbody>
            @php $ignores = []; $ignoreKey = 1; @endphp
            @foreach($statements as $key => $statement)
            @if(($statement->hide == 1 && $statement->hideTransaction == 1) && session('showAllTnx') == 0)
            @php continue; @endphp
            @endif
            @if($statement->description == "refund_default_interest")
            @php $ignores[] = $statement; @endphp
            <!-- {{-- <tr>
              <td>
                <div class="target-description" data-key="{{$key}}">
                  <span class="text-primary fas fa-chevron-up cursor"></span>
                </div>
              </td>
            </tr> --}} -->
            @continue;
            @endif
            <tr>
              <td>{{\Carbon\Carbon::parse($statement->created_at->date)->format('Y M d')}}</td>
              <td>{{ !is_null($statement->debit) ? number_format($statement->debit, 2) : null}}</td>
              <td>{{ !is_null($statement->credit) ? number_format($statement->credit, 2) : null}}</td>
              <td>{{number_format($statement->balance, 2)}}</td>
              <td colspan="2" class="text-capitalize">{{$statement->comments}}
                @if($statement->interestBreakDown && count($ignores) == 0)
                <div class="target-description" data-key="{{$key}}">
                  <span class="text-primary fas fa-chevron-up cursor"></span>
                  <!-- <span class="text-primary fa fa-minus"></span> -->
                </div>
                @endif
              </td>
              <td>
                <span class="label text-capitalize
                  @if(($statement->description == 'payment' || $statement->description == 'refund') && $statement->credit > 0)
                      bg-success
                  @elseif($statement->description == 'other_charge_refund' || $statement->description == 'other_charge')
                      bg-primary
                  @elseif($statement->description == 'refund_default_interest')
                      bg-warning
                  @elseif($statement->description == 'interest')
                      bg-info
                  @elseif($statement->description == 'capreduction')
                      bg-danger
                  @endif
                ">{{$statement->explanation}}</span>
              </td>
              <td>{{$statement->nominalCode}}</td>
              <td>{{$statement->transactionId}}</td>
              @if($statement->id)
              <td class="text-capitalize">
                <label class="switch">
                  <input class="hideStatement" data-tnxid="{{$statement->id}}" type="checkbox" value="{{($statement->hide == 1) ? '1' : '0' }}" {{($statement->hide == 1) ? 'checked' : '' }}>
                  <span class="slider round"></span>
                </label>
                <label class="switch">
                  <input class="hideTransaction" data-tnxid="{{$statement->id}}" type="checkbox" value="{{($statement->hideTransaction == 1) ? '1' : '0' }}" {{($statement->hideTransaction == 1) ? 'checked' : '' }}>
                  <span class="slider round"></span>
                </label>
                <!-- <i class="fas fa-edit cursor pull-right hideModal" data-toggle="modal" data-target="#hideModal" data-tnxid="{{$statement->id}}" data-value = "{{($statement->hide) ? $statement->hide : '' }}" aria-hidden="true"></i> -->
              </td>
              <td class="text-capitalize">{{($statement->notes) ? $statement->notes : '' }}
                <i class="fas fa-edit cursor pull-right notesModal" data-toggle="modal" data-target="#notesModal" data-tnxid="{{$statement->id}}" data-value = "{{($statement->notes) ? $statement->notes : '' }}" aria-hidden="true"></i>
              </td>
              <td><a href="{{url('transactions/revert-transaction/'.$statement->id)}}" class="btn btn-danger ml-1 js-delete float-right" data-title="Transaction" >Delete</a></td>
              @else
              <td></td>
              <td></td>
              @endif
            </tr>
              @if($statement->interestBreakDown && count($ignores) == 0)
              @component('welcome::breakdown', ['interestBreakDown' => $statement->interestBreakDown, 'selectedLoan' => $selectedLoan, 'key' => $key])
              @endcomponent
              <!-- {{-- @foreach($statement->interestBreakDown as $breakDown)
              <tr class="font-italic show-description{{$key}} @if($breakDown->rate == $selectedLoan->interest->default_rate && $breakDown->total >0) text-danger @elseif($breakDown->total <= 0) text-success @endif"  style="display: none; background-color: #efefef; font-size: 12px;">
                <td>{{\Carbon\Carbon::parse($breakDown->start->date)->format('Y M d')}}</td>
                <td>{{\Carbon\Carbon::parse($breakDown->end->date)->format('Y M d')}}</td>
                <td>{{$breakDown->days}}
                  @if($selectedLoan->loantype === 'retained')
                    @php
                    $date1 = \Carbon\Carbon::parse($breakDown->start->date);
                    $date2 = \Carbon\Carbon::parse($breakDown->end->date)->copy()->addDay();
                    $interval = date_diff($date1, $date2);
                    // dd($interval->format('%m'));
                    $months = $date1->diffInMonths($date2);
                    $days = $date1->copy()->addMonths($months)->diffInDays($date2);
                    $days += $days ? 1 : 0;
                    @endphp
                    ({{round($days/30.4, 2) + $months}})
                  @endif
                </td>
                <td>{{number_format($breakDown->balance, 2)}}</td>
                <td>{{(float)$breakDown->rate}}</td>
                <td>{{(float)$breakDown->daily_rate}}</td>
                <td>{{number_format($breakDown->total, 2)}}</td>
                @if(isset($breakDown->rounding_error))
                <td>{{$breakDown->rounding_error ? $breakDown->rounding_error : $breakDown->rounding_error}}</td>
                <td></td>
                @else
                <td></td>
                <td></td>
                @endif
                <td></td>
                <td></td>
              </tr>
              @endforeach --}} -->
              @endif
              @if($statement->interestBreakDown && count($ignores))
              <!-- Total interest transaction -->
                <tr>
                  <!-- {{--<td>{{\Carbon\Carbon::parse($statement->created_at->date)->format('Y M d')}}</td>--}} -->
                  <td></td>
                  <td>{{$statement->debit + collect($ignores)->sum('credit')}}</td>
                  <td></td>
                  <td></td>
                  <td colspan="2" class="text-capitalize">Total interest charged {{ltrim($statement->comments, 'Int')}}
                    <div class="target-description" data-key="{{$key}}">
                      <span class="text-primary fas fa-chevron-up cursor"></span>
                      <!-- <span class="text-primary fa fa-minus"></span> -->
                    </div>
                  </td>
                </tr>
                @component('welcome::breakdown', ['interestBreakDown' => $statement->interestBreakDown, 'selectedLoan' => $selectedLoan, 'key' => $key])
                @endcomponent
                <!-- default rate refunds -->
                <tr>
                  <!-- {{--<td>{{\Carbon\Carbon::parse($statement->created_at->date)->format('Y M d')}}</td>--}} -->
                  <td></td>
                  <td></td>
                  <td>{{collect($ignores)->sum('credit')}}</td>
                  <td></td>
                  <td>
                    <div colspan="2" class="text-capitalize target-description font-italic" data-key="{{++$ignoreKey}}ignore">
                      Total default rate interest refunds {{ltrim($statement->comments, 'Int')}}
                      <span class="text-warning fas fa-chevron-up cursor"></span>
                    </div>
                  </td>
                </tr>
                @foreach($ignores as $ignore)
                <tr class="font-italic show-description{{$ignoreKey}}ignore"  style="display: none; background-color: #efefef; font-size: 12px;">
                    <!-- {{--<td>{{\Carbon\Carbon::parse($ignore->created_at->date)->format('Y M d')}}</td>--}} -->
                    <td></td>
                    <td>{{ !is_null($ignore->debit) ? number_format($ignore->debit, 2) : null}}</td>
                    <td>{{ !is_null($ignore->credit) ? number_format($ignore->credit, 2) : null}}</td>
                    <td>{{number_format($ignore->balance, 2)}}</td>
                    <td colspan="2" class="text-capitalize">{{$ignore->comments}}</td>
                    <td>
                      <span class="label text-capitalize bg-warning">{{$ignore->explanation}}</span>
                    </td>
                    <td>{{$ignore->nominalCode}}</td>
                    <td>{{$ignore->transactionId}}</td>
                    @if($ignore->id)
                      <td class="text-capitalize">
                        <label class="switch">
                          <input class="hideStatement" data-tnxid="{{$ignore->id}}" type="checkbox" value="{{($ignore->hide == 1) ? '1' : '0' }}" {{($ignore->hide == 1) ? 'checked' : '' }}>
                          <span class="slider round"></span>
                        </label>
                        <label class="switch">
                          <input class="hideTransaction" data-tnxid="{{$ignore->id}}" type="checkbox" value="{{($ignore->hideTransaction == 1) ? '1' : '0' }}" {{($ignore->hideTransaction == 1) ? 'checked' : '' }}>
                          <span class="slider round"></span>
                        </label>
                      </td>
                      <td class="text-capitalize">{{($ignore->notes) ? $ignore->notes : '' }}
                        <i class="fas fa-edit cursor pull-right notesModal" data-toggle="modal" data-target="#notesModal" data-tnxid="{{$ignore->id}}" data-value = "{{($ignore->notes) ? $ignore->notes : '' }}" aria-hidden="true"></i>
                      </td>
                    @endif
                </tr>
                @endforeach
                @php $ignores = []; @endphp
              @endif
            @endforeach
          </tbody>
        </table>
      </div>
    </div>
  </div>
</div>

<!-- {{-- @if($selectedLoan && ($selectedLoan->loantype === 'serviced' || $selectedLoan->loantype === 'half') ) --}} -->
@if($selectedLoan && in_array($selectedLoan->loantype, ['serviced', 'rolled', 'half']) )
<div class="col-lg-12">
  <div class="has-bg">
    <div class="has-box-shadow">
      <h5 class="panel-title">
          Payment Plan
          <div class="target-description" data-key="PmtPlan">
            <span class="text-primary fas fa-chevron-up cursor"></span>
          </div>
      </h5>
      <div class="show-descriptionPmtPlan hidden">
        <div class="panel-body">
          <table class="table table-striped table-hover table-bordered text-left m-0">
            <thead>
              <tr>
                <th>Sr. No</th>
                <th>Versions</th>
                <th>Amount Due</th>
                <th>Amount Paid</th>
                <th>Amount Pending</th>
                <th>Expected Balance</th>
                <th>Amount Type</th>
                <th>Status</th>
                <th>Due Date</th>
              </tr>
            </thead>
            <tbody>
              @foreach($paymentPlan as $key => $payment)
              <tr>
                <td>{{$key+1}}</td>
                <td>{{$payment->plan_version}}</td>
                <td>{{number_format($payment->amount, 2)}}</td>
                <td>{{number_format($payment->amount_paid, 2)}}</td>
                <td>{{number_format($payment->amount_pending, 2)}}</td>
                <td>{{number_format($payment->expected_balance, 2)}}</td>
                <td class="text-capitalize">{{$payment->type}}</td>
                <td class="text-capitalize">{{$payment->status}}</td>
                <td>{{\Carbon\Carbon::parse($payment->pay_before->date)->format('Y M d')}}
                  @if($payment->payment_break_down)
                  <div class="target-description" data-key="{{$key}}pmtplan">
                    <span class="text-primary fas fa-chevron-up cursor"></span>
                  </div>
                  @endif
                </td>
              </tr>
              @if($payment->payment_break_down)
              <tr class="show-description{{$key}}pmtplan font-weight-bold" style="display: none; background-color: #efefef;">
                <td>Date</td>
                <td>Amount</td>
                <td>Description</td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
              </tr>
              @foreach($payment->payment_break_down as $keybrkdn => $pmtBreakDown)
              <tr class="show-description{{$key}}pmtplan"  style="display: none; background-color: #efefef;">
                <td>{{ isset($pmtBreakDown->created_at) ? \Carbon\Carbon::parse($pmtBreakDown->created_at->date)->format('Y M d') : ''}}</td>
                <td>{{number_format($pmtBreakDown->amount, 2)}}</td>
                <td class="text-capitalize">{{$pmtBreakDown->description}}
                  @if(isset($pmtBreakDown->sub_break_down))
                  <div class="target-description" data-key="{{$keybrkdn}}subbrkdn">
                    <span class="text-primary fas fa-chevron-up cursor"></span>
                  </div>
                  @endif
                </td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
              </tr>
              @if(isset($pmtBreakDown->sub_break_down))
              <tr class="show-description{{$keybrkdn}}subbrkdn font-weight-bold"  style="display: none; background-color: #efefef;">
                <td>From</td>
                <td>To</td>
                <td>Days</td>
                <td>Amount</td>
                <td>Rate</td>
                <td>Daily Rate</td>
                <td>Total</td>
                <td></td>
                <td></td>
              </tr>
              @foreach($pmtBreakDown->sub_break_down as $pmtSubBreakDown)
              @foreach($pmtSubBreakDown as $subBreakDown)
              <tr class="show-description{{$keybrkdn}}subbrkdn"  style="display: none; background-color: #c7bdbd;">
                <td>{{\Carbon\Carbon::parse($subBreakDown->start->date)->format('Y M d')}}</td>
                <td>{{\Carbon\Carbon::parse($subBreakDown->end->date)->format('Y M d')}}</td>
                <td>{{$subBreakDown->days}}</td>
                <td>{{number_format($subBreakDown->balance, 2)}}</td>
                <td>{{(float)$subBreakDown->rate}}</td>
                <td>{{(float)$subBreakDown->daily_rate}}</td>
                <td>{{number_format($subBreakDown->total, 2)}}</td>
                <td></td>
                <td></td>
              </tr>
              @endforeach
              @endforeach
              @endif
              @endforeach
              @endif
              @endforeach
            </tbody>
          </table>
        </div>
      </div>
    </div>
  </div>
</div>
@endif
<!-- if( (isset($renewalCharges) && (session('loanid') != null) ) && ($selectedLoan && $selectedLoan->loantype === 'serviced') ) -->
@if(session('loanid') != null)
<div class="col-lg-12">
  <div class="has-bg">
    <div class="has-box-shadow no-mar-b">
      <h5 class="panel-title">
          Borrower Statements
          <div class="target-description" data-key="BrrStmt">
            <span class="text-primary fas fa-chevron-up cursor"></span>
          </div>
      </h5>
      <div class="show-descriptionBrrStmt hidden">
        <div class="panel-body row m-0">
          @foreach($monthStatements as $mStatement)
            <div class="col-3 p-1">
              <div class="card">
                <div class="card-body">
                  <h6 class="card-title">{{$mStatement->start_date}} - {{$mStatement->end_date}}
                    <a class="pull-right pl-2" href="{{url('monthstatement').'/'.\Carbon\Carbon::parse(str_replace('/', '-', $mStatement->end_date))->format('M d Y').'?monthTnxs=1'}}" target="_blank">
                      <i class="far fa-file-pdf" data-toggle="tooltip" title="Month transactions"></i>
                    </a>

                    <a class="pull-right text-success" href="{{url('monthstatement').'/'.\Carbon\Carbon::parse(str_replace('/', '-', $mStatement->end_date))->format('M d Y').'?monthTnxs=0'}}" target="_blank">
                      <i class="far fa-file-pdf" data-toggle="tooltip" title="All transactions"></i>
                    </a>
                  </h6>
                    <table class="table table-striped table-hover table-bordered text-left m-0">
                      <tbody>
                        <tr>
                            <td>Opening Balance: {{number_format($mStatement->opening_balance, 2)}}</td>
                        </tr>
                        <tr>
                            <td>Interest Retained: {{number_format($mStatement->interest_charged, 2)}}</td>
                        </tr>
                        <tr>
                            <td>Current Interest Rate: {{(float)$mStatement->current_interest_rate}}%</td>
                        </tr>
                        <tr>
                            <td>Interest Due Date: {{$mStatement->interest_due_date}}</td>
                        </tr>
                        <tr>
                            <td>Closing Balance: {{number_format($mStatement->closing_balance, 2)}}</td>
                        </tr>

                        <tr>
                            <td>Gross Loan: {{number_format($mStatement->gross_loan, 2)}}</td>
                        </tr>
                        <tr>
                            <td>Loan End Date: {{$mStatement->loan_end_date}}</td>
                        </tr>
                      </tbody>
                    </table>
                </div>
              </div>
            </div>
          @endforeach
        </div>
      </div>
    </div>
  </div>
</div>

<div class="col-lg-12">
  <div class="has-bg">
    <div class="has-box-shadow">
      <h5 class="text-warning panel-title">
          Statements
          <div class="target-description" data-key="Stmt">
            <span class="text-primary fas fa-chevron-up cursor"></span>
          </div>
          <!-- <span class="pull-right">Hidden Statements: {{count(collect($statements)->where('hide', 1)->all())}}</span> -->
          <span class="cursor pull-right" data-toggle="modal" data-target="#hiddenStatementsModal" aria-hidden="true">
            Hidden Statements: {{count(collect($statements)->where('hide', 1)->all())}}
          </span>
      </h5>
      <div class="show-descriptionStmt hidden">
        <div class="panel-body">
          <table class="table table-striped table-hover table-bordered text-left m-0">
            <thead>
              <tr>
                <th>Date</th>
                <th>Debit</th>
                <th>Credit</th>
                <th>Balance</th>
                <th>Descriptions</th>
                <th colspan="2"></th>
                <th>Transaction Id</th>
                <th></th>
              </tr>
            </thead>
            <tbody>
              @foreach($statements as $key => $statement)
              @if($statement->hide != 1)
              <tr>
                <td>{{\Carbon\Carbon::parse($statement->created_at->date)->format('Y M d')}}</td>
                <td>{{ !is_null($statement->debit) ? number_format($statement->debit, 2) : null}}</td>
                <td>{{ !is_null($statement->credit) ? number_format($statement->credit, 2) : null}}</td>
                <td>{{number_format($statement->balance, 2)}}</td>
                <td colspan="2" class="text-capitalize">{{$statement->comments}}
                  @if($statement->interestBreakDown)
                  <div class="target-description" data-key="{{$key}}">
                    <span class="text-primary fas fa-chevron-up cursor"></span>
                    <!-- <span class="text-primary fa fa-minus"></span> -->
                  </div>
                  @endif
                </td>
                <td>
                  <span class="label text-capitalize
                    @if(($statement->description == 'payment' || $statement->description == 'refund') && $statement->credit > 0)
                        bg-success
                    @elseif($statement->description == 'other_charge_refund' || $statement->description == 'other_charge')
                        bg-primary
                    @elseif($statement->description == 'refund_default_interest')
                        bg-warning
                    @elseif($statement->description == 'interest')
                        bg-info
                    @elseif($statement->description == 'capreduction')
                        bg-danger
                    @endif
                  ">{{$statement->explanation}}</span>
                </td>
                <td>{{$statement->transactionId}}</td>
                <td></td>
                <td></td>
              </tr>
                @if($statement->interestBreakDown)
                <tr class="show-description{{$key}} font-weight-bold font-italic"  style="display: none; background-color: #efefef; font-size: 12px;">
                  <td>From</td>
                  <td>To</td>
                  <td>Days @if($selectedLoan->loantype === 'retained') (Months) @endif</td>
                  <td>Amount</td>
                  <td>Interest Rate</td>
                  <td>Daily Charges(Per day charge)</td>
                  <td>Total(Days * Daily Charges)</td>
                  @if(isset($statement->interestBreakDown[0]->rounding_error))
                  <td>Rounding Error</td>
                  <td></td>
                  @else
                  <td></td>
                  @endif
                </tr>
                @foreach($statement->interestBreakDown as $breakDown)
                <tr class="font-italic show-description{{$key}} @if($breakDown->rate == $selectedLoan->interest->default_rate && $breakDown->total >0) text-danger @elseif($breakDown->total <= 0) text-success @endif"  style="display: none; background-color: #efefef; font-size: 12px;">
                  <td>{{\Carbon\Carbon::parse($breakDown->start->date)->format('Y M d')}}</td>
                  <td>{{\Carbon\Carbon::parse($breakDown->end->date)->format('Y M d')}}</td>
                  <td>{{$breakDown->days}}
                    @if($selectedLoan->loantype === 'retained')
                      @php
                      $date1 = \Carbon\Carbon::parse($breakDown->start->date);
                      $date2 = \Carbon\Carbon::parse($breakDown->end->date)->copy()->addDay();
                      $interval = date_diff($date1, $date2);
                      // dd($interval->format('%m'));
                      $months = $date1->diffInMonths($date2);
                      $days = $date1->copy()->addMonths($months)->diffInDays($date2);
                      $days += $days ? 1 : 0;
                      @endphp
                      ({{round($days/30.4, 2) + $months}})
                      <!--({{ ($interval->format('%m') == 0) ? ($interval->format('%d') == $date1->copy()->startOfMonth()->diffInDays($date1->copy()->endOfMonth())) ? 1 : 0 : $interval->format('%m') }}) -->
                    @endif
                  </td>
                  <td>{{number_format($breakDown->balance, 2)}}</td>
                  <td>{{(float)$breakDown->rate}}</td>
                  <td>{{(float)$breakDown->daily_rate}}</td>
                  <td>{{number_format($breakDown->total, 2)}}</td>
                  @if(isset($breakDown->rounding_error))
                  <td>{{$breakDown->rounding_error ? $breakDown->rounding_error : $breakDown->rounding_error}}</td>
                  <td></td>
                  @else
                  <td></td>
                  @endif
                </tr>
                @endforeach
                @endif
                @endif
              @endforeach
            </tbody>
          </table>
        </div>
      </div>
    </div>
  </div>
</div>

<div class='col-lg-12'>
  @if($selectedLoan && $selectedLoan->loanextras)
  <div class='has-bg'>
    <div class='has-box-shadow no-mar-b'>
      <h5 class="panel-title">
        Notes
        <div class="target-description" data-key="Notes">
          <span class="text-primary fas fa-chevron-up cursor"></span>
        </div>
        <button type="button" class="btn btn-primary ml-1 addNewRowInLoanExtrasModal pull-right" data-toggle="modal" data-target="#addNewRowInLoanExtrasModal">Add new</button>
      </h5>
      <div class="show-descriptionNotes hidden">

        @if($selectedLoan->loanextras->note)
          @foreach(json_decode($selectedLoan->loanextras->note) as $extras)
            <div class="row">
              <?php foreach($extras as $ext){
                if(strcmp(strtolower($ext), 'notes') === 0)
                  continue;
                else
                  echo "<div class='col-12'>$ext</div>";
                }?>
            </div>
          @endforeach
        @endif
        @php $loanBreakdown = $selectedLoan->loanextras->breakdown; @endphp
        @if($loanBreakdown && !is_null(json_decode($loanBreakdown)))
          <h5 class="panel-title">Breakdown</h5>
          @foreach(json_decode($loanBreakdown) as $extras)
            <div class="row">
              <?php foreach($extras as $ext){
                if(strcmp(strtolower($ext), 'breakdown') === 0)
                  continue;
                else
                  echo "<div class='col-2'>$ext</div>";
                }?>
            </div>
          @endforeach
        @endif
      </div>
    </div>
  </div>
  @else
  <div class='has-bg'>
    <div class='has-box-shadow no-mar-b'>
      <h5 class="panel-title">
        Notes/Breakdown
        <button type="button" class="btn btn-primary ml-1 addNewRowInLoanExtrasModal pull-right" data-toggle="modal" data-target="#addNewRowInLoanExtrasModal">Add new</button>
      </h5>
    </div>
  </div>
  @endif
</div>

  <div class='col-lg-12'>
    <div class='has-bg'>
      <div class='has-box-shadow no-mar-b'>
        <div class="row">
          <div class="col-6">
            <h5 class="panel-title">
              Mandatory variables
              <div class="target-description" data-key="MndtryVrbls">
                <span class="text-primary fas fa-chevron-up cursor"></span>
              </div>
            </h5>
          </div>
          <div class="col-4">
            <!-- <button type="button" class="btn btn-primary ml-1 addNewOptionModal pull-right" data-toggle="modal" data-target="#addNewOptionModal">Add New Option</button> -->
          </div>
          <div class="col-2">
            <!-- Search form -->
            <!-- <div class="md-form active-pink active-pink-2 mb-3 mt-0">
              <input class="form-control js-searchName" type="text" placeholder="Search by name..." aria-label="Search">
            </div> -->
          </div>
        </div>
        @if(isset($loanOptions))
        <div class="show-descriptionMndtryVrbls hidden">
          <div class="panel-body">
            <table class="table table-striped table-hover table-bordered text-left m-0">
              <thead>
                <tr>
                  <th>Name</th>
                  <th>Value</th>
                  <!-- <th>Mapped Id</th> -->
                  <!-- <th>Create Date</th> -->
                  <!-- <th>Update Date</th> -->
                  <!-- <th>Actions</th> -->
                </tr>
              </thead>
              <tbody class="js-optionalVariable">
                @foreach($mandatoryOptVariables as $key => $mOptVariable)
                <tr>
                  <td>{{$key}}</td>
                  <td>{{$mOptVariable}}</td>
                  <!-- {{--<td>{{($option->created_at) ? \Carbon\Carbon::parse($option->created_at->date)->format('Y M d') : null}}</td>
                  <td>{{($option->updated_at) ? \Carbon\Carbon::parse($option->updated_at->date)->format('Y M d H:i:s') : null}}</td>
                  <td>
                    <button type="button" class="btn btn-primary ml-1 editOptionModal" data-toggle="modal" data-opt-id="{{$option->id}}" data-name="{{$option->name}}"
                      data-value="{{$option->value}}" data-create-date="{{($option->created_at) ? \Carbon\Carbon::parse($option->created_at->date)->format('Y-m-d') : null}}"                     data-target="#editOptionModal" data-mapped-id="{{$mappedId}}">Edit</button>
                    <a type="button" class="btn btn-danger ml-1" href="{{url('calculations/deleteoption/'.$option->id)}}">Delete</a>
                  </td>--}} -->
                </tr>
                @endforeach
              </tbody>
            </table>
          </div>
        </div>
        @endif
      </div>
    </div>
  </div>

  <div class='col-lg-12'>
    <div class='has-bg'>
      <div class='has-box-shadow no-mar-b'>
        <div class="row">
          <div class="col-6">
            <h5 class="panel-title">
              Optional variable
              <div class="target-description" data-key="OptVrbls">
                <span class="text-primary fas fa-chevron-up cursor"></span>
              </div>
            </h5>
          </div>
          <div class="col-4">
            <button type="button" class="btn btn-primary ml-1 addNewOptionModal pull-right" data-toggle="modal" data-target="#addNewOptionModal">Add New Option</button>
          </div>
          <div class="col-2">
            <!-- Search form -->
            <div class="md-form active-pink active-pink-2 mb-3 mt-0">
              <input class="form-control js-searchName" type="text" placeholder="Search by name..." aria-label="Search">
            </div>
          </div>
        </div>
        @if(isset($loanOptions))
        <div class="show-descriptionOptVrbls hidden">
          <div class="panel-body">
            <table class="table table-striped table-hover table-bordered text-left m-0">
              <thead>
                <tr>
                  <th>Name</th>
                  <th>Value</th>
                  <th>Mapped Id</th>
                  <th>Create Date</th>
                  <th>Update Date</th>
                  <th>Actions</th>
                </tr>
              </thead>
              <tbody class="js-optionalVariable">
                @foreach($loanOptions as $option)
                <tr>
                  <td>{{$option->name}}</td>
                  <td class="{{ (empty($option->value)) ? 'empty-option-border' : '' }}">{{$option->value}}</td>
                  <td>
                    @php $mapped = collect($loanOptions)->where('mapped_id', $option->mapped_id)->all(); 
                      $mappedId = (count($mapped) > 1) ? $option->mapped_id : '';
                    @endphp
                    {{ $mappedId }}
                  </td>
                  <td>{{($option->created_at) ? \Carbon\Carbon::parse($option->created_at->date)->format('Y M d') : null}}</td>
                  <td>{{($option->updated_at) ? \Carbon\Carbon::parse($option->updated_at->date)->format('Y M d H:i:s') : null}}</td>
                  <td>
                    <button type="button" class="btn btn-primary ml-1 editOptionModal" data-toggle="modal" data-opt-id="{{$option->id}}" data-name="{{$option->name}}"
                      data-value="{{$option->value}}" data-create-date="{{($option->created_at) ? \Carbon\Carbon::parse($option->created_at->date)->format('Y-m-d') : null}}"                     data-target="#editOptionModal" data-mapped-id="{{$mappedId}}">Edit</button>
                    <a type="button" class="btn btn-danger ml-1" href="{{url('calculations/deleteoption/'.$option->id)}}">Delete</a>
                  </td>
                </tr>
                @endforeach
              </tbody>
            </table>
          </div>
        </div>
        @endif
      </div>
    </div>
  </div>

</div>
@endif
<!-- End loan id check -->
<!--- ================ popup html start  ============================ --->
<!-- Export Loan Modal start -->
<div class="modal" id="exportLoanModal" data-backdrop="static">
  <div class="modal-dialog" style="max-width:800px">
    <div class="modal-content">
      <div class="modal-body custom_modal">
        <div class="row">
          <div class="col-lg-1">
            <div class="left-side">
            </div>
          </div>
          <div class="col-lg-11">
            <!-- <h4 class="panel-title">
              Export Loan <button type="button" class="close" data-dismiss="modal">&times;</button>
            </h4> -->
            <div class="table-scroll">
              <form action="{{url('loans/exportloans')}}" method="post">
                <h4 class="modal-title">Export {{$selectedLoan->loanid}}<button type="button" class="close" data-dismiss="modal">&times;</button></h4>
                <div class="row">
                  @csrf()
                  <input type="hidden" name="loanIds[]" value="{{$selectedLoan->id}}">
                  <div class="col-2">
                    <p>Backup:</p>
                  </div>

                  <div class="col-2">
                    <div class="form-group">
                      <label class="custom-radio mb-0">
                        <input class="" type="hidden" name="uploadAtSharepoint" value="0">
                        <input class="" type="checkbox" name="uploadAtSharepoint" value="1">
                        <span class="checkmark"></span>
                      </label>
                    </div>
                  </div>

                  <div class="col-4">
                    <p>Interest calculation date:</p>
                  </div>

                  <div class="col-4">
                    <input class="form-control datepicker20" autocomplete="off" name="date" value="{{ old('date')}}">
                  </div>

                  <div class="col-lg-12">
                    <button type="submit" class="btn pull-right btn-primary custom-padding mt-3">Export</button>
                  </div>
                </div>
              </form>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
<!-- Export Loan modal end -->
<!-- Filler_ modal start -->
<div class="modal" id="Fillers_" data-backdrop="static">
  <div class="modal-dialog" style="max-width:700px">
    <div class="modal-content">
      <div class="modal-body custom_modal">
        <div class="row">
          <div class="col-lg-1">
            <div class="left-side">
            </div>
          </div>
          <div class="col-lg-11">
            <h4 class="panel-title">
              Filler <button type="button" class="close" data-dismiss="modal">&times;</button>
              <button type="button" class="btn btn-primary pull-right ml-1 Create-Filler" data-toggle="modal"
                data-target="#Create-Filler">
                Create Filler
              </button>
            </h4>
            <div class="table-scroll">
              <table class="table text-left m-0">
                <thead>
                  <tr>
                    <th>Date</th>
                    <th>Filler Amount</th>
                    <th>Descriptions</th>
                  </tr>
                </thead>
                <tbody>
                  @foreach($refunds as $refund)
                  <tr>
                    <td>{{\Carbon\Carbon::parse($refund->refund_at)->format('Y M d')}}</td>
                    <td>{{number_format($refund->amount, 2)}}</td>
                    <td>{{$refund->journal->comment}}</td>
                  </tr>
                  @endforeach
                </tbody>
              </table>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>

<!-- Filler_ modal end -->
<!-- Create filler modal start -->
<div class="modal" id="Create-Filler" data-backdrop="static">
    <div class="modal-dialog" style="max-width:1000px">
      <div class="modal-content">
        <div class="modal-body custom_modal">
          <div class="row">
            <div class="col-lg-1">
              <div class="left-side">
              </div>
            </div>
            <div class="col-lg-11">
              <h4 class="panel-title">
                Create Filler <button type="button" class="close" data-dismiss="modal">&times;</button>
              </h4>
             <form action="{{ url('calculations/createrefund' )}}" method="post">
                <div class="row">
                  <div class="col-3">
                    @csrf()
                    <div class="form-group">
                      <label for="refundAt">Date:</label>
                      <input type="text" class="form-control datepicker8" autocomplete="off" name="refundAt"
                            value="{{ old('refundAt')}}">
                    </div>
                  </div>
                  <div class="col-3">
                    <div class="form-group">
                      <label for="amount">Amount:</label>
                      <input type="text" class="form-control" name="amount"
                            value="{{old('amount')}}">
                    </div>
                  </div>
                  <div class="col-3">
                    <div class="form-group">
                      <label for="assetType">Filler Type:</label>
                      <select class="form-control" name="assetType">
                        <option value="debit_filler">Debit</option>
                        <option value="credit_filler">Credit</option>
                      </select>
                    </div>
                  </div>
                  <div class="col-3">
                    <div class="form-group">
                      <label for="nominalCode">Nominal Codes:</label>
                      <select class="form-control" name="nominalCode">
                        @foreach($nominalCodes as $nominalCode)
                          <option value="{{$nominalCode->id}}" {{ (old('nominalCode') == $nominalCode->id) ? 'selected' : ''}}>{{$nominalCode->code}} - {{$nominalCode->name}}</option>
                        @endforeach
                      </select>
                    </div>
                  </div>
                  <div class="col-12">
                    <div class="form-group">
                      <label for="description">Description:</label>
                      <!-- <input type="text" class="form-control" name="description" value="{{old('description')}}"> -->
                      <textarea class="form-control" rows="2" name="description">{{ old('description')}}</textarea>
                    </div>
                  </div>
                  <input type="hidden" class="form-control" name="loanId" value="{{ session('loanid') }}">
                  <div class="col-12 mt-2">
                      <button type="submit" class="btn pull-right custom-padding btn-primary mt-3">Submit</button>
                  </div>
                </div>
              </form>
            </div>
          </div>
        </div>
      </div>
    </div>
</div>
<!-- grace modal start -->
<div class="modal" id="grace" data-backdrop="static">
  <div class="modal-dialog" style="max-width:900px">
    <div class="modal-content">
      <div class="modal-body custom_modal">
        <div class="row">
          <div class="col-lg-1">
            <div class="left-side">
            </div>
          </div>
          <div class="col-lg-11">
            <h4 class="panel-title">
              Grace days <button type="button" class="close" data-dismiss="modal">&times;</button>
              <button type="button" class="btn btn-primary pull-right ml-1 create-grace" data-toggle="modal"
                data-target="#create-grace">
                Create Grace
              </button>
            </h4>
            <div class="table-scroll">
              <table class="table text-left m-0">
                <thead>
                  <tr>
                    <th>Grace Days</th>
                    <th>Start Date</th>
                    <th>End Date</th>
                    <th>Descriptions</th>
                  </tr>
                </thead>
                <tbody>
                  @foreach($selectedLoan->grace as $grace)
                  <tr>
                    <td>{{$grace->days}}</td>
                    <td>{{($grace->start_date) ? \Carbon\Carbon::parse($grace->start_date)->format('Y M d') : '-'}}</td>
                    <td>{{($grace->end_date) ? \Carbon\Carbon::parse($grace->end_date)->format('Y M d') : '-'}}</td>
                    <td>{{$grace->description}}</td>
                  </tr>
                  @endforeach
                </tbody>
              </table>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>

<!-- Create filler modal start -->
<div class="modal" id="create-grace" data-backdrop="static">
    <div class="modal-dialog" style="max-width:1000px">
      <div class="modal-content">
        <div class="modal-body custom_modal">
          <div class="row">
            <div class="col-lg-1">
              <div class="left-side">
              </div>
            </div>
            <div class="col-lg-11">
              <h4 class="panel-title">
                Create Grace <button type="button" class="close" data-dismiss="modal">&times;</button>
              </h4>
             <form action="{{url('calculations/create-grace')}}" method="post">
                    @csrf()
                <div class="row">
                  <div class="col-4">
                    <div class="form-group">
                      <label for="amount">Grace Days:</label>
                      <input type="text" class="form-control" name="days" value="{{old('days')}}">
                    </div>
                  </div>
                  <div class="col-4">
                    <div class="form-group">
                      <label>Start Date:</label>
                      <input type="text" class="form-control datepicker24" autocomplete="off" name="startDate" value="{{ old('startDate')}}">
                    </div>
                  </div>
                  <div class="col-4">
                    <div class="form-group">
                      <label>End Date:</label>
                      <input type="text" class="form-control datepicker25" autocomplete="off" name="endDate" value="{{ old('endDate')}}">
                    </div>
                  </div>
                  <div class="col-12">
                    <div class="form-group">
                      <label for="description">Description:</label>
                      <!-- <input type="text" class="form-control" name="description" value="{{old('description')}}"> -->
                      <textarea class="form-control" rows="2" name="description">{{ old('description')}}</textarea>
                    </div>
                  </div>
                  <input type="hidden" class="form-control" name="loanId" value="{{ session('loanid') }}">
                  <div class="col-12 mt-2">
                      <button type="submit" class="btn pull-right custom-padding btn-primary mt-3">Submit</button>
                  </div>
                </div>
              </form>
            </div>
          </div>
        </div>
      </div>
    </div>
</div>
<!-- Create filler modal end -->

<!-- Default Rate Interests Start -->
  <div class="modal" id="DefaultRateInterests" data-backdrop="static">
    <div class="modal-dialog" style="max-width:1300px">
      <div class="modal-content">
        <div class="modal-body custom_modal">
          <div class="row">
            <div class="col-lg-1">
              <div class="left-side">
              </div>
            </div>
            <div class="col-lg-11">
              <h4 class="panel-title">
                Default Rate Interests <button type="button" class="close" data-dismiss="modal">&times;</button>
              </h4>
              <div class="table-scroll">
                <table class="table table-striped table-hover table-bordered text-left m-0">
                  <thead>
                    <tr>
                      <th>Date</th>
                      <!-- <th>End Date</th> -->
                      <th>Days</th>
                      <th>Rate</th>
                      <th>Daily Rate</th>
                      <th>Total</th>
                      <th>Balance</th>
                      <th></th>
                    </tr>
                  </thead>
                  <tbody>
                    @foreach($defaultRateInterestBreakdowns as $key => $breakdown)
                    <tr>
                      <td>{{\Carbon\Carbon::parse($breakdown->start->date)->format('Y M d')}}</td>
                      <td>{{(float)$breakdown->days}}</td>
                      <td>{{(float)$breakdown->rate}}</td>
                      <td>{{(float)$breakdown->daily_rate}}</td>
                      <td>{{number_format($breakdown->total, 2)}}</td>
                      <td>{{number_format($breakdown->balance, 2)}}</td>
                      <td>
                        @if($breakdown->refunded)
                          <span class="text-danger mr-5">Refunded</span>
                          @if($breakdown->hide)
                            <span class="fa fa-eye-slash ml-5 mr-5" data-toggle="tooltip" title="Statement hidden"></span>
                          @endif
                          @if($breakdown->hide_transaction)
                            <span class="fa fa-eye-slash ml-5 mr-5" data-toggle="tooltip" title="Transaction hidden"></span>
                          @endif
                        @else
                          <form action="{{'calculations/createdefaultraterefund'}}" method="post">
                            @csrf()
                            <input type="hidden" class="form-control" name="loanId" value="{{session('loanid')}}" readonly>
                            <input type="hidden" name="startDate" value="{{\Carbon\Carbon::parse($breakdown->start->date)->format('Y-m-d')}}" readonly/>
                            <input type="hidden" name="endDate" value="{{\Carbon\Carbon::parse($breakdown->end->date)->format('Y-m-d')}}" readonly/>
                            <input type="hidden" name="days" value="{{$breakdown->days}}" readonly />
                            <input type="hidden" name="balance" value="{{$breakdown->balance}}" readonly />
                            <input type="hidden" name="description" value="" />
                            <input type="text" name="refundDate" value="{{\Carbon\Carbon::parse($breakdown->start->date)->format('Y-m-d')}}" />
                            <!-- <input type="hidden" name="refundDate" value="" readonly/> -->
                            <button type="submit" class="btn btn-default">Refund</button>

                            <div class="form-group">
                              <label class="custom-radio mb-0 pull-right" style="top: -33px;">
                                <input class="bulkCheckBox" type="checkbox" name="bulk{{$key}}" 
                                data-index="{{$key}}" data-loanid={{session('loanid')}}
                                data-start-date="{{\Carbon\Carbon::parse($breakdown->start->date)->format('Y-m-d')}}"
                                data-end-date="{{\Carbon\Carbon::parse($breakdown->end->date)->format('Y-m-d')}}"
                                data-days="{{$breakdown->days}}" data-balance="{{$breakdown->balance}}"
                                data-refund-date="{{\Carbon\Carbon::parse($breakdown->start->date)->format('Y-m-d')}}"
                                >
                                <span class="checkmark"></span>
                              </label>
                            </div>

                          </form>
                          @if(0)
                          <!-- wrong if to skip this for time being. -->
                          <button type="button" class="btn btn-default defaultRateRefundModal"    data-toggle="modal"
                              data-target="#defaultRateRefundModal"
                              data-startdate="{{\Carbon\Carbon::parse($breakdown->start->date)->format('Y-m-d')}}"
                              data-enddate="{{\Carbon\Carbon::parse($breakdown->end->date)->format('Y-m-d')}}"
                              data-days="{{$breakdown->days}}" data-balance="{{$breakdown->balance}}"
                              >
                              Refund
                          </button>
                          @endif
                        @endif
                      </td>
                    </tr>
                    @endforeach
                  </tbody>
                </table>
              </div>

              <div class="form-group">
                <div class="text-info" id="message-box">
                </div>
                <div class="row mt-3">
                  <!-- <label class="custom-radio mr-2 pull-right">
                    <p class="mt-2">Hide Transaction</p>
                    <input class="js-hideTransaction" type="checkbox">
                    <span class="checkmark"></span>
                  </label> -->
                  <div class="col-2">
                    <p class="mt-2 pull-right">Hide Statement:</p>
                  </div>
                  <div class="col-1">
                    <!-- <p class="js-hide mr-2 pull-right cursor"><span class="js-classEye2 fa fa-eye text-primary"></span> Hide Statement</p> -->
                    <label class="custom-radio mr-2">
                      <input class="js-hide" type="checkbox">
                      <span class="checkmark"></span>
                    </label>
                  </div>
                  <div class="col-2">
                    <p class="mt-2 pull-right">Hide Transaction:</p>
                  </div>
                  <div class="col-1">
                    <label class="custom-radio mr-2">
                      <input class="js-hideTransaction" type="checkbox">
                      <span class="checkmark"></span>
                    </label>
                    <!-- <p class="js-hideTransaction mr-2 pull-right cursor"><span class="js-classEye fa fa-eye text-primary"></span>Hide Transaction</p> -->
                  </div>
                  <div class="col-2">
                    <label>Refund at: <span class="fa fa-info-circle" data-toggle="tooltip" title="Leave empty to apply default refund date"></span></label>
                  </div>
                  <div class="col-2">
                    <input class="form-control datepicker23 pull-right js-bulkRefundDate" autocomplete="off" />
                  </div>

                  <div class="col-2">
                    <button type="submit" class="btn btn-default pull-right mt-2 js-refund-bulk">Bulk Refund</button>
                  </div>
                </div>


<!--                 <label class="custom-radio mr-2 pull-right">
                  <p class="mt-2">Hide Statment</p>
                  <input class="js-hide" type="checkbox">
                  <span class="checkmark"></span>
                </label> -->
              </div>
              <!-- </div> -->
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
<!-- Default Rate Interests end -->
<!-- Renew Chargesmodal start -->
  <div class="modal" id="renew-charges-modal" data-backdrop="static">
    <div class="modal-dialog" style="max-width:700px">
      <div class="modal-content">
        <div class="modal-body custom_modal">
          <div class="row">
            <div class="col-lg-1">
              <div class="left-side">
              </div>
            </div>
            <div class="col-lg-11">
              <h4 class="panel-title">
                  Renew Charges <button type="button" class="close" data-dismiss="modal">&times;</button>
              </h4>
              <div class="table-scroll">
                <table class="table table-striped table-hover table-bordered text-left m-0">
                  <thead>
                    <tr>
                      <th>Value</th>
                      <th>Renew Start Date</th>
                      <th>Renew End Date</th>
                      <th>Duration</th>
                      <th>Description</th>
                      <th></th>
                    </tr>
                  </thead>
                  <tbody>
                    @foreach($renewalCharges as $key => $charge)
                    <tr>
                      <td>{{number_format($charge->value, 2)}}</td>
                      <td>{{\Carbon\Carbon::parse($charge->renew_date->date)->format('Y M d')}}</td>
                      <td>{{\Carbon\Carbon::parse($charge->renew_end_date->date)->format('Y M d')}}</td>
                      <td>{{$charge->duration}}</td>
                      <td>{{$charge->description}}</td>
                      <td>
                        <button type="button" class="btn btn-default refundModal" data-toggle="modal"
                          data-target="#refundModal"
                          data-value="{{$charge->value}}"
                          data-chargedate="{{\Carbon\Carbon::parse($charge->journal->tnx_date)->format('Y-m-d')}}">
                          Refund
                        </button>
                      </td>
                    </tr>
                    @endforeach
                  </tbody>
                </table>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
<!-- Renew Chargesmodal end -->
<!-- Generate Statement Modal start -->
  <div class="modal" id="Generate-Statement" data-backdrop="static">
    <div class="modal-dialog">
      <div class="modal-content">
        <div class="modal-body custom_modal">
          <div class="row">
            <div class="col-lg-1">
              <div class="left-side">
              </div>
            </div>
            <div class="col-lg-11">
              <form action="{{ url('calculations/lockmonth' )}}" method="post">
                <h4 class="panel-title">
                    Generate statement <button type="button" class="close" data-dismiss="modal">&times;</button>
                </h4>
                <div class="row">
                  <div class="col-12">
                    @csrf()
                    <div class="form-group no-mar-b">
                      <select class="form-control" name="month">
                        <option value="">Select Statement Month</option>
                        @foreach($loanMonths as $month)
                        <option value="{{$month->statementDate}}"
                        @if($month->statementGenerated)
                            disabled
                        @endif
                        >@if($month->statementGenerated)
                            Statement generated
                        @else
                            {{$month->statementDate}}
                        @endif
                        </option>
                        @endforeach
                      </select>
                    </div>
                  </div>
                  <div class="col-12">
                    <div class="form-group">
                      <label for="nominalCode">Nominal Codes:</label>
                      <select class="form-control" name="nominalCode">
                        @foreach($nominalCodes as $nominalCode)
                          <option value="{{$nominalCode->id}}" {{ (old('nominalCode') == $nominalCode->id) ? 'selected' : ''}}>{{$nominalCode->code}} - {{$nominalCode->name}}</option>
                        @endforeach
                      </select>
                    </div>
                  </div>

                  <input type="hidden" class="form-control" name="loanId" value="{{ session('loanid') }}">
                  <div class="col-12">
                      <button type="submit" class="btn pull-right custom-padding btn-primary mt-3">Submit</button>
                  </div>
                </div>
              </form>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
<!-- Generate Statement Modal end -->
<!-- other charges Modal Start -->
  <div class="modal" id="otherChargesModal" data-backdrop="static">
    <div class="modal-dialog" style="max-width: 700px">
      <div class="modal-content">
        <div class="modal-body custom_modal">
          <div class="row">
            <div class="col-lg-1">
              <div class="left-side">
              </div>
            </div>
            <div class="col-lg-11">
              <form action="{{ url('calculations/createothercharge' )}}" method="post">
                <h4 class="panel-title">
                    Create Other Charges <button type="button" class="close" data-dismiss="modal">&times;</button>
                </h4>
                <div class="row">
                  <div class="col-6">
                    @csrf()
                    <div class="form-group">
                        <label for="chargedate">Date:</label>
                        <input type="text" class="form-control datepicker6" autocomplete="off" name="chargedate"
                          value="{{ old('chargedate')}}">
                    </div>
                  </div>
                  <div class="col-6">
                    @csrf()
                    <div class="form-group">
                        <label for="name">Name:</label>
                        <input type="text" class="form-control" name="name"
                              value="{{ old('name')}}">
                    </div>
                  </div>
                  <div class="col-6">
                    <div class="form-group">
                      <label for="value">Amount:</label>
                      <input type="text" class="form-control" name="value"
                            value="{{old('value')}}">
                    </div>
                  </div>

                  <div class="col-6">
                    <div class="form-group">
                      <label for="nominalCode">Nominal Codes:</label>
                      <select class="form-control" name="nominalCode">
                        @foreach($nominalCodes as $nominalCode)
                          <option value="{{$nominalCode->id}}" {{ (old('nominalCode') == $nominalCode->id) ? 'selected' : ''}}>{{$nominalCode->code}} - {{$nominalCode->name}}</option>
                        @endforeach
                      </select>
                    </div>
                  </div>
                  <div class="col-6 d-none">
                    <div class="form-group">
                      <label for="chargetype">Charge Type:</label>
                      <select class="form-control" name="chargetype">
                          <option value="fixed" {{ (old('chargetype') == 'fixed') ? 'selected' : ''}}>Fixed</option>
                          <option value="percentage" {{ (old('chargetype') == 'percentage') ? 'selected' : ''}}>Percentage</option>
                      </select>
                    </div>
                  </div>
                  <div class="col-6 d-none">
                    <div class="form-group">
                      <label for="amounttype">Amount Type:</label>
                      <input type="text" class="form-control" name="amounttype"
                            value="netloan">
                    </div>
                  </div>
                  <input type="hidden" class="form-control" name="loanid" value="{{ session('loanid') }}">
                  <div class="col-lg-12">
                      <button type="submit" class="btn pull-right btn-primary custom-padding mt-2">Submit</button>
                  </div>
                </div>
              </form>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
<!-- Other charges Modal End -->
<!-- Other charges display modal start -->
  <div class="modal" id="otherChargesdisplay" data-backdrop="static">
    <div class="modal-dialog" style="max-width: 700px">
      <div class="modal-content">
        <div class="modal-body custom_modal">
          <div class="row">
            <div class="col-lg-1">
              <div class="left-side">
              </div>
            </div>
            <div class="col-lg-11">
              <h4 class="panel-title">
                  Other Charges <button type="button" class="close" data-dismiss="modal">&times;</button>
                  <button type="button" class="btn btn-primary ml-1 otherChargesModal pull-right" data-toggle="modal" data-target="#otherChargesModal">
                    Create Other Charges
                  </button>
              </h4>
              <div class="table-scroll">
                <table class="table table-striped table-hover table-bordered text-left m-0">
                  <thead>
                    <tr>
                      <th>Name</th>
                      <th>Value</th>
                      <th>Charge Type</th>
                      <th class="border-right-n">Charge Date</th>
                      <th class="border-n"></th>
                    </tr>
                  </thead>
                  <tbody>
                    @foreach($otherCharges as $key => $otherCharge)
                    <tr>
                      <td>{{$otherCharge->name}}</td>
                      <td>{{number_format($otherCharge->value, 2)}}</td>
                      <td>{{$otherCharge->chargeType}}</td>
                      <td>{{\Carbon\Carbon::parse($otherCharge->journal->tnx_date)->format('Y M d')}}</td>
                      <td>
                        @if($otherCharge->refunded)
                          <p class="text-danger">Refunded</p>
                        @else
                        <button type="button" class="btn btn-default refundModal" data-toggle="modal"
                          data-target="#refundModal"
                          data-value="{{$otherCharge->value}}"
                          data-chargedate="{{\Carbon\Carbon::parse($otherCharge->journal->tnx_date)->format('Y-m-d')}}" data-otherchargeid="{{$otherCharge->id}}"
                          >
                          Refund
                        </button>
                        @endif
                      </td>
                    </tr>
                    @endforeach
                  </tbody>
                </table>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
<!-- Other charges display modal end -->
<!-- Closing Charges Modal start -->
  <div class="modal" id="ClosingChargesModal" data-backdrop="static">
    <div class="modal-dialog" style="max-width: 700px">
      <div class="modal-content">
        <div class="modal-body custom_modal">
          <div class="row">
            <div class="col-lg-1">
              <div class="left-side">
              </div>
            </div>
            <div class="col-lg-11">
              <h4 class="panel-title">
                  Closing Charges <button type="button" class="close" data-dismiss="modal">&times;</button>
              </h4>
              @if($closingCharges && (session('loanid') != null))
              <div class="panel-body">
                <div class="table-scroll" style="height: inherit;">
                  <table class="table table-striped table-hover table-bordered text-left m-0">
                    <thead>
                      <tr>
                        <th>Name</th>
                        <th>Value</th>
                        <th>Charge Type</th>
                        <th class="border-n"></th>
                      </tr>
                    </thead>
                    <tbody>
                        @foreach($closingCharges as $key => $charge)
                        <tr>
                          <td>{{$charge->name}}</td>
                          <td>{{number_format($charge->value, 2)}}</td>
                          <td>{{$charge->charge_type}}</td>
                          <td>
                            <button type="button" class="btn btn-primary float-right assignClosingChargeModal" data-toggle="modal" data-target="#assignClosingChargeModal" data-chargeid="{{$charge->id}}" data-loanid="{{session('loanid')}}" data-value="{{(float)$charge->value}}">
                            Add Charge
                            </button>
                          </td>
                        </tr>
                        @endforeach
                    </tbody>
                  </table>
                </div>
              </div>
              @endif
              <h4 class="panel-title mt-5">Applied closing charges </h4>
              <table class="table table-striped table-hover table-bordered text-left m-0">
                <thead>
                  <tr>
                    <th>Name</th>
                    <th>Value</th>
                    <th>Charge Type</th>
                    <th></th>
                  </tr>
                </thead>
                <tbody>
                  @foreach($assignedCLCharges as $key => $charge)
                  <tr>
                    <td>{{$charge->name}}</td>
                    <td>{{number_format($charge->value, 2)}}</td>
                    <td>{{$charge->charge_type}}</td>
                    <td>
                      <a class="btn btn-danger float-right" href="{{url('calculations/deleteclosingcharge?chargeId='.$charge->id.'&loanId='.session('loanid'))}}">Remove</a>
                      <button type="button" class="btn btn-primary float-right mr-1 editClosingLoanChargeModal" data-toggle="modal" data-target="#editClosingLoanChargeModal" data-loanid="{{session('loanid')}}" data-chargeid="{{$charge->id}}" data-value="{{(float)$charge->value}}"
                      >
                      Edit
                      </button>
                    </td>
                  </tr>
                  @endforeach
                </tbody>
              </table>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
<!-- Closing Charges Modal End -->
<!-- Paid Amount modal start -->
  <div class="modal" id="Paid-Amounts" data-backdrop="static">
    <div class="modal-dialog" style="max-width:900px">
      <div class="modal-content">
        <div class="modal-body custom_modal">
          <div class="row">
            <div class="col-lg-1">
              <div class="left-side">
              </div>
            </div>
            <div class="col-lg-11">
              <h4 class="panel-title">
                Paid Amounts
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <button type="button" class="btn btn-primary ml-1 pull-right PaymentsModal" data-toggle="modal" data-target="#PaymentsModal">
                  Create Payement
                </button>
              </h4>
              <div class="table-scroll">
                <table class="table table-striped table-hover table-bordered text-left">
                  <thead>
                    <tr>
                      <th>Date</th>
                      <th>Amount Paid</th>
                      <th>Descriptions</th>
                      <th class="border-right-n">Capital Reduction</th>
                      <!-- <th class="border-n"></th>
                      <th class="border-n"></th> -->
                    </tr>
                  </thead>
                  <tbody>
                    @foreach($payments as $key => $payment)
                    <tr>
                      <td>{{\Carbon\Carbon::parse($payment->paid_at)->format('Y M d')}}</td>
                      <td>{{number_format($payment->amount, 2)}}</td>
                      <td>{{$payment->journal->comment}}</td>
                      <td>{{($payment->is_capital_reduction == 1) ? 'Yes' : ''}}</td>
                    </tr>
                    @endforeach
                  </tbody>
                </table>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
<!-- Paid Amount modal end -->
<!-- Payments Modal start -->
  <div class="modal" id="PaymentsModal" data-backdrop="static">
    <div class="modal-dialog" style="max-width: 800px">
      <div class="modal-content">
        <div class="modal-body custom_modal">
          <div class="row">
            <div class="col-lg-1">
              <div class="left-side">
              </div>
            </div>
            <div class="col-lg-11">
              <h4 class="panel-title">
                  Payment <button type="button" class="close" data-dismiss="modal">&times;</button>
              </h4>
              <form action="{{ url('calculations/createpayment' )}}" method="post">
                <div class="row">
                  <div class="col-6">
                    @csrf()
                    <div class="form-group">
                      <label for="paidAt">Date:</label>
                      <input type="text" class="form-control datepicker3" autocomplete="off" name="paidAt"
                            value="{{ old('paidAt')}}">
                    </div>
                  </div>
                  <div class="col-6">
                    <div class="form-group">
                      <label for="amount">Amount:</label>
                      <input type="text" class="form-control" name="amount"
                            value="{{old('amount')}}">
                    </div>
                  </div>
                  <div class="col-12">
                    <div class="form-group">
                      <label for="description">Description:</label>
                      <textarea class="form-control" rows="2" name="description">{{ old('description')}}</textarea>
                    </div>
                  </div>
                  <div class="col-4">
                    <div class="form-group">
                      <span class="radio-text">Capital Reduction:</span>
                      <label class="custom-radio">
                        <input type="hidden" name="isCapitalReduction" value="0">
                        <input type="checkbox" class="" name="isCapitalReduction" value="1">
                        <span class="checkmark"></span>
                      </label>
                    </div>
                  </div>
                  <div class="col-4">
                    <div class="form-group">
                      <label for="nominalCode">Nominal Codes:</label>
                      <select class="form-control" name="nominalCode">
                        @foreach($nominalCodes as $nominalCode)
                          <option value="{{$nominalCode->id}}" {{ (old('nominalCode') == $nominalCode->id) ? 'selected' : ''}}>{{$nominalCode->code}} - {{$nominalCode->name}}</option>
                        @endforeach
                      </select>
                    </div>
                  </div>
                  <input type="hidden" class="form-control" name="loanId" value="{{ session('loanid') }}">
                  <div class="col-12 mt-2">
                      <button type="submit" class="btn pull-right custom-padding btn-primary mt-3">Submit</button>
                  </div>
                </div>
              </form>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
<!-- Payments Modal End -->
<!-- create Serviced loan modal -->
<div class="modal" id="createnewLoanModal" data-backdrop="static">
  <div class="modal-dialog" style="max-width: 800px">
    <div class="modal-content">
      <div class="modal-body custom_modal">
        <div class="row">
          <div class="col-lg-1">
            <div class="left-side">
            </div>
          </div>
          <div class="col-lg-11">
            <form action="{{ url('calculations/store' )}}" method="post">
              <h4 class="modal-title">Create Serviced Loan <button type="button" class="close" data-dismiss="modal">&times;</button></h4>
                <div class="row">
                  <div class="col-6">
                    @csrf()
                    <div class="form-group">
                      <label for="loanid">Loan Id:</label>
                      <input type="text" class="form-control" name="loanid"
                            value="{{old('loanid')}}">
                    </div>
                  </div>
                  <div class="col-6">
                    <div class="form-group">
                      <label for="netloan">Net Loan:</label>
                      <input type="text" class="form-control" name="netloan"
                            value="{{old('netloan')}}">
                    </div>
                  </div>
                  <div class="col-6">
                    <div class="form-group">
                      <label for="rate">Rate:</label>
                      <input type="text" class="form-control" name="rate"
                            value="{{ old('rate')}}">
                    </div>
                  </div>
                  <div class="col-6">
                    <div class="form-group ">
                      <label for="defaultRate">Default Rate:</label>
                      <input type="text" class="form-control" name="defaultRate"
                            value="{{ old('defaultRate')}}">
                    </div>
                  </div>
                  <div class="col-4">
                    <div class="form-group">
                      <label for="duration">Duration(in months):</label>
                      <select class="form-control" name="duration">
                        @for($i = 1; $i <= 24; $i++)
                          <option value="{{$i}}" {{ old('duration') == $i ? 'selected': '' }}>{{$i}}</option>
                        @endfor
                      </select>
                    </div>
                  </div>
                  <div class="col-4">
                    <div class="form-group">
                      <label for="minTerm">Minimum Term:</label>
                      <select class="form-control" name="minTerm">
                        @for($i = 0; $i < 24; $i++)
                          <option value="{{$i}}" {{ old('minTerm') == $i ? 'selected': '' }}>{{$i}}</option>
                        @endfor
                      </select>
                    </div>
                  </div>
                  <div class="col-4">
                    <div class="form-group">
                      <label for="issuedate">Issue Date:</label>
                      <input class="form-control datepicker" autocomplete="off" name="issuedate"
                            value="{{ old('issuedate')}}">
                    </div>
                  </div>
                  <!-- <div class="col-4">
                    <div class="form-group">
                      <label for="loanType">Loan Type:</label>
                      <select class="form-control" name="loanType">
                        <option value="serviced" {{ old('loanType') == "serviced" ? 'selected': '' }} >Serviced</option> -->
                        <!-- <option value="retained" {{ old('loanType') == "retained" ? 'selected': '' }} >Retained</option> -->
                      <!-- </select>
                    </div>
                  </div> -->
                  <input type="hidden" name="loanType" value="serviced">
                  <div class="col-4">
                    <div class="form-group">
                      <label for="customerId">Customers:</label>
                      <select name="customerId" class="form-control">
                          @foreach($customers as $customer)
                            <option value="{{$customer->id}}" {{  old('customerId') == $customer->id ? 'selected': '' }} >{{ $customer->username }}</option>
                          @endforeach
                      </select>
                    </div>
                  </div>
                  <div class="col-4">
                    <div class="form-group">
                      <span class="radio-text">Holiday Refund:</span>
                      <label class="custom-radio">
                        <input class="" type="hidden" name="holidayRefund" value="0">
                        <input class="" type="checkbox" name="holidayRefund"
                          @if(old('holidayRefund') != null && old('holidayRefund') == 0)
                            ''
                          @else
                            checked = "checked"
                          @endif
                          value="1">
                        <span class="checkmark"></span>
                      </label>
                    </div>
                  </div>
                  <div class="col-12">
                    <div class="form-group">
                      <label for="description">Description:</label>
                      <textarea class="form-control" rows="2" name="description">{{ old('description')}}</textarea>
                    </div>
                  </div>
                  <div class="col-lg-12">
                    <button type="submit" class="btn pull-right btn-primary custom-padding mt-3">Create Loan</button>
                  </div>
                </div>
              </form>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
<!-- Add New Charge Modal-->
<div class="modal" id="addNewChargeModal" data-backdrop="static" style="z-index: 1111;">
  <div class="modal-dialog" style="max-width: 700px">
    <div class="modal-content">
      <div class="modal-body custom_modal">
        <div class="row">
          <div class="col-lg-1">
            <div class="left-side">
            </div>
          </div>
          <div class="col-lg-11">
            <h4 class="modal-title">Add Initial Charge <button type="button" class="close" data-dismiss="modal">&times;</button></h4>
            <form action="{{url('calculations/createcharge')}}" method="post" id="addInitialCharge">
              @csrf()
              
              <div class="form-group">
                <label for="name">Name:</label>
                <input type="text" class="form-control" name="name">
              </div>
              
              <!-- <div class="form-group"> -->
                <!-- <label for="value">Value:</label> -->
                <input type="hidden" class="form-control" name="value" value="0">
              <!-- </div> -->

              <div class="form-group">
                <label for="chargetype">Charge Type:</label>
                <select class="form-control" name="chargetype">
                  <option value="fixed">Fixed</option>
                  <option value="percentage">Percentage</option>
                </select> 
              </div>                              
          
              <!-- <div class="form-group">
                <label for="amounttype">Amount Type </label>
                <select class="form-control" name="amounttype">
                  <option value="netloan">Net Loan</option>
                </select>
              </div> -->
              <input type="hidden" class="form-control" name="amounttype" value="netloan">

              <button type="submit" class="btn btn-primary js-addInitialCharge">Submit</button>
            </form>
            <div class="js-errors"></div>
          </div>          
        </div>
      </div>
    </div>
  </div>
</div>
@if($selectedLoan && $selectedLoan->loantype === 'retained')
<!-- convert To Service Loan modal -->
<div class="modal" id="convertToServiceLoan" data-backdrop="static">
  <div class="modal-dialog" style="max-width: 1000px">
    <div class="modal-content">
      <div class="modal-body custom_modal">
        <div class="row">
          <div class="col-lg-1">
            <div class="left-side">
            </div>
          </div>
          <div class="col-lg-11">
            <form action="{{ url('calculations/updateloan' )}}" method="post">
              <h4 class="panel-title">
                <div class="row">
                  <div class="col-4 js-LoanModalTitle">Convert to service loan</div>
                  <div class="col-8">
                    <button type="button" class="close pull-right" data-dismiss="modal">&times;</button>
                  </div>
                </div>
              </h4>
              <div class="row">
                <div class="col-lg-12">
                  <div class="table-scroll">
                    <div class="row">
                      @csrf()

                      <input type="hidden" class="form-control" name="loanId" value="{{$selectedLoan->id}}">
                      <input type="hidden" class="form-control" name="loanType" value="half">
                      
                      <div class="col-4">
                        <div class="form-group">
                          <label>Retain loan issue date:</label>
                          <input class="form-control js-HLstartDate" value="{{ \Carbon\Carbon::parse($selectedLoan->issueat->date)->format('Y-m-d') }}" disabled>
                        </div>
                      </div>

                      <div class="col-4">
                        <div class="form-group">
                          <label>Retain loan duration(in months):</label>
                          <input class="form-control js-HLduration" type="text" name="duration" value="{{ $selectedLoan->interest->duration }}">
                        </div>
                      </div>

                      <div class="col-4">
                        <div class="form-group">
                          <label>Retain loan end date:</label>
                          <input class="form-control datepicker2 js-HLendDate">
                        </div>
                      </div>

                      <div class="col-4">
                        <div class="form-group">
                          <label>Service loan issue date:</label>
                          <input class="form-control js-HLstartDate2" readonly>
                        </div>
                      </div>

                      <div class="col-4">
                        <div class="form-group">
                          <label>Service loan duration(in months):</label>
                          <input class="form-control js-HLduration2" type="text" name="halfLoanDuration">
                        </div>
                      </div>

                      <div class="col-4">
                        <div class="form-group">
                          <label>Service loan end date:</label>
                          <input class="form-control datepicker20 js-HLendDate2" readonly>
                        </div>
                      </div>

                    </div>
                  </div>
                </div>
                <div class="col-lg-12">
                  <button type="submit" class="btn pull-right btn-primary custom-padding mt-3">Submit</button>
                </div>
              </div>
            </form>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
@endif
<!-- create Retained loan modal -->
<div class="modal" id="createRetainedLoanModal" data-backdrop="static">
  <div class="modal-dialog" style="max-width: 1000px">
    <div class="modal-content">
      <div class="modal-body custom_modal">
        <div class="row">
          <div class="col-lg-1">
            <div class="left-side">
            </div>
          </div>
          <div class="col-lg-11">
            <form action="{{ url('calculations/store' )}}" method="post">
              <h4 class="panel-title">Create Retained Loan
                <button type="button" class="close pull-right" data-dismiss="modal">&times;</button>
                <button type="button" class="btn btn-primary pull-right ml-1 addNewChargeModal" data-toggle="modal" data-target="#addNewChargeModal">
                  Add new charge
                </button>
              </h4>
                <div class="row">
                  <div class="col-lg-12">
                    <div class="table-scroll retained-loan">
                      <div class="row">
                        <!-- <div class="col-4"> -->
                          @csrf()
                        <!-- </div> -->
                        <div class="col-4">
                          <div class="form-group">
                            <label for="loanid">Loan Id:</label>
                            <input type="text" class="form-control" name="loanid"
                                  value="{{old('loanid')}}">
                          </div>
                        </div>
                        <div class="col-4">
                          <div class="form-group">
                            <label for="customerId">Customers:</label>
                            <select name="customerId" class="form-control">
                                @foreach($customers as $customer)
                                  <option value="{{$customer->id}}" {{  old('customerId') == $customer->id ? 'selected': '' }} >{{ $customer->username }}</option>
                                @endforeach
                            </select>
                          </div>
                        </div>
                        <div class="col-4">
                          <div class="form-group">
                            <label for="description">Description:</label>
                            <textarea class="form-control" rows="2" name="description">{{ old('description')}}</textarea>
                          </div>
                        </div>
                        <div class="col-4">
                          <div class="form-group">
                            <label for="rate">Rate:</label>
                            <input type="text" class="form-control js-RLRate" name="rate"
                                  value="{{(old('rate')) ? old('rate') : '1'}}" min="0">
                          </div>
                        </div>
                        <div class="col-4">
                          <div class="form-group ">
                            <label for="defaultRate">Default Rate:</label>
                            <input type="text" class="form-control js-RLDefaultRate" name="defaultRate"
                                  value="{{ (old('defaultRate')) ? old('defaultRate') : 2}}" min="0">
                          </div>
                        </div>
                        <input type="hidden" name="holidayRefund" value="1">
                        <!-- <div class="col-4">
                          <div class="form-group">
                            <span class="radio-text">Holiday Refund:</span>
                            <label class="custom-radio">
                              <input class="" type="hidden" name="holidayRefund" value="0">
                              <input class="" type="checkbox" name="holidayRefund"
                                @if(old('holidayRefund') != null && old('holidayRefund') == 0)
                                  ''
                                @else
                                  checked = "checked"
                                @endif
                                value="1">
                              <span class="checkmark"></span>
                            </label>
                          </div>
                        </div> -->
                        <div class="col-4">
                          <div class="form-group">
                            <label for="issuedate">Issue Date:</label>
                            <input class="form-control datepicker1 js-RLIssuedate" autocomplete="off" name="issuedate"
                                  value="{{ old('issuedate')}}">
                          </div>
                        </div>
                        <div class="col-4">
                          <div class="form-group">
                            <label for="duration">Duration(in months):</label>
                            <input class="form-control js-RLDuration" type="text" name="duration" value="{{ (old('duration')) ? old('duration') : 12}}">
                          </div>
                        </div>
                        <div class="col-4">
                          <div class="form-group">
                            <label for="noofdays">Number of Days:</label>
                            <input type="text" class="form-control js-RLNoOfDays" name="noofdays"
                                  value="{{ (old('noofdays')) ? old('noofdays') : 0}}" readonly>
                          </div>
                        </div>
                        <div class="col-4">
                          <div class="form-group">
                            <label for="issuedate">End Date:</label>
                            <input class="form-control datepicker19 js-RLEndDate" autocomplete="off" name="enddate"
                                  value="{{ old('enddate')}}">
                          </div>
                        </div>
                        <input type="hidden" name="minTerm" value="0">
                        <!-- <div class="col-4">
                          <div class="form-group">
                            <label for="minTerm">Minimum Term:</label>
                            <select class="form-control" name="minTerm">
                              @for($i = 0; $i < 24; $i++)
                                <option value="{{$i}}" {{ old('minTerm') == $i ? 'selected': '' }}>{{$i}}</option>
                              @endfor
                            </select>
                          </div>
                        </div> -->
                        
                        <input type="hidden" name="loanType" value="retained">
                        <div class="col-4 offset-4">
                          <div class="form-group">
                            <label for="grossloan">Gross Loan:</label>
                            <input type="text" class="form-control js-RLGrossLoan" name="grossloan"
                                  value="{{ (old('grossloan')) ? old('grossloan') : 0}}" min="0">
                          </div>
                        </div>
                        <div class="col-lg-12">
                          <!-- <div class="js-retainedLoanForm"> -->
                            <div class="row js-retainedLoanForm">
                            @foreach($charges as $key => $charge)
                              <div class="col-6 offset-3">
                                <div class="form-group">
                                  <label class="text-capitalize">{{$charge->name}}:</label>
                                  <input type="hidden" name="RLChargeId[]" value="{{$charge->id}}">
                                  @if($charge->chargetype === 'percentage')
                                  <div class="row">
                                    <div class="col-4">
                                      <input type="text" class="form-control js-percentChange js-valChange{{$key}}" data-key="{{$key}}" value="{{$charge->value}}">
                                    </div>
                                    <div class="col-4">
                                      <p class="small has-no-margin">% of Gross loan = </p>
                                    </div>
                                    <div class="col-4">
                                      <input type="text" class="form-control js-RLInitialCharge{{$key}} js-percentFixedVal js-keyGetter" data-key="{{$key}}" name="initialChargeValue[]" value="0">
                                    </div>
                                  </div>
                                  @else
                                    <input type="text" class="form-control js-fixedValChange js-RLInitialCharge{{$key}} js-keyGetter" data-key="{{$key}}" name="initialChargeValue[]" value="{{$charge->value}}" onchange="adjustLoan();">
                                  @endif
                                </div>
                              </div>
                            @endforeach
                            </div>
                          <!-- </div> -->
                        </div>


                        <div class="col-4">
                          <div class="form-group">
                            <label for="rateperday">Daily Charge:</label>
                            <input type="text" class="form-control js-RLRatePerDay" name="rateperday"
                                  value="{{ (old('rateperday')) ? old('rateperday') : 0}}" readonly>
                          </div>
                        </div>

                        <div class="col-4">
                          <div class="form-group">
                            <label for="ratepermonth">Rate Per Month:</label>
                            <input type="text" class="form-control js-RLRatePerMonth" name="ratepermonth"
                                  value="{{ (old('ratepermonth')) ? old('ratepermonth') : 0}}" readonly>
                          </div>
                        </div>
                        
                        <div class="col-4">
                          <div class="form-group">
                            <label for="interest">Retained Interest:</label>
                            <input type="text" class="form-control js-RLInterest" name="interest"
                                  value="{{ (old('interest')) ? old('interest') : 0}}" readonly>
                          </div>
                        </div>

                        <div class="col-4">
                          <div class="form-group">
                            <label for="roundingError">Rounding Error:</label>
                            <input type="text" class="form-control js-RLRoundingError" name="roundingError"
                                  value="{{ (old('roundingError')) ? old('roundingError') : 0}}" readonly>
                          </div>
                        </div>
                        
                        <div class="col-4">
                          <div class="form-group">
                            <label for="netloan">Net Loan:</label>
                            <input type="text" class="form-control js-RLNetLoan" name="netloan"
                                  value="{{ (old('netloan')) ? old('netloan') : 0}}" readonly>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                  <div class="col-lg-12">
                    <button type="submit" class="btn pull-right btn-primary custom-padding mt-3">Create Loan</button>
                  </div>
                </div>
              </form>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
<!-- create new modal end -->
<!-- Update Loan Modal -->
<!-- <div class="modal" id="updateLoanModal" data-backdrop="static">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-body custom_modal">
        <div class="row">
          <div class="col-lg-1">
            <div class="left-side">
            </div>
          </div>
          <div class="col-lg-11">
            <form action="{{'calculations/updateloan'}}" method="post">
              <h4 class="modal-title">Update Loan</h4>
              <button type="button" class="close" data-dismiss="modal">&times;</button>
                @csrf()
                <input type="hidden" class="form-control" name="loanId" value="{{old('loanId')}}">
                <div class="form-group">
                  <label for="netloan">Net Loan</label>
                  <input class="form-control" name="netloan" value="{{old('netloan')}}"/>
                </div>
                <div class="form-group">
                  <label for="rate">Rate</label>
                  <input class="form-control" name="rate" value="{{old('rate')}}"/>
                </div>
                <div class="form-group">
                  <label for="defaultRate">Default Rate</label>
                  <input class="form-control" name="defaultRate" value="{{old('defaultRate')}}"/>
                </div>
                <div class="form-group">
                  <label for="duration">Duration</label>
                  <select class="form-control" name="duration">
                    @for($i = 1; $i <= 24; $i++)
                      <option value="{{$i}}">{{$i}}</option>
                    @endfor
                  </select>
                  <input class="form-control" name="duration" value="{{old('duration')}}"/>
                </div>
                <div class="form-group">
                    <label for="holidayRefund">Holiday Refund</label>
                    <input type="hidden" name="holidayRefund" value="0">
                    <input type="checkbox" name="holidayRefund" value="1" />
                </div>
                <button type="submit" class="custom-padding pull-right btn btn-primary">Submit</button>
            </form>
          </div>
        </div>
      </div>
    </div>
  </div>
</div> -->
<!-- Update Loan Duration Modal -->
<div class="modal" id="updateLoanDurationModal" data-backdrop="static">
  <div class="modal-dialog" style="max-width: 900px;">
    <div class="modal-content">
      <div class="modal-body custom_modal">
        <div class="row">
        <div class="col-lg-1">
          <div class="left-side">
          </div>
        </div>
        <div class="col-lg-11">
          <form action="{{'calculations/renewloanduration'}}" method="post">
            <h4 class="modal-title">Renew Loan
              <button type="button" class="close" data-dismiss="modal">&times;</button>
              <button type="button" class="btn btn-primary ml-1 pull-right renew-charges-modal" data-toggle="modal"
                data-target="#renew-charges-modal">
                Renew Charges
              </button>
            </h4>
            <div class="row">
                @csrf()
                <input type="hidden" class="form-control" name="loanId" value="{{ session('loanid') }}">
                <div class="col-3">
                  <div class="form-group">
                      <label for="duration">Renew Loan Duration</label>
                      <select class="form-control" name="duration">
                        @for($i = 1; $i <= 24; $i++)
                          <option value="{{$i}}">{{$i}}</option>
                        @endfor
                      </select>
                  </div>
                </div>
                <div class="col-3">
                  <div class="form-group">
                    <label>Renew Date</label>
                    <input class="form-control datepicker15" autocomplete="off" name="renewDate" value="{{old('renewDate')}}"/>
                  </div>
                </div>
                <div class="col-3">
                  <div class="form-group">
                    <label>Renew Charge Value</label>
                    <input type="text" class="form-control" name="value">
                  </div>
                </div>
                <div class="col-3">
                  <div class="form-group">
                    <label for="nominalCode">Nominal Codes:</label>
                    <select class="form-control" name="nominalCode">
                      @foreach($nominalCodes as $nominalCode)
                        <option value="{{$nominalCode->id}}" {{ (old('nominalCode') == $nominalCode->id) ? 'selected' : ''}}>{{$nominalCode->code}} - {{$nominalCode->name}}</option>
                      @endforeach
                    </select>
                  </div>
                </div>
                <div class="col-lg-12">
                  <div class="form-group">
                    <label>Description</label>
                    <textarea class="form-control" rows="1" name="description"></textarea>
                  </div>
                </div>
                <div class="col-lg-12">
                    <button type="submit" class="custom-padding  pull-right btn btn-primary">Submit</button>
                </div>
              </div>
            </form>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
<!-- Transfer Modal -->
<div class="modal" id="transferModal" data-backdrop="static">
  <div class="modal-dialog" style="max-width: 700px;">
    <div class="modal-content">
      <div class="modal-body custom_modal">
        <div class="row">
          <div class="col-lg-1">
            <div class="left-side">
            </div>
          </div>
          <div class="col-lg-11">
            <form action="{{'calculations/createtransfer'}}" method="post">
              <h4 class="modal-title">Transfer to other loan from current loan(service to service only)<button type="button" class="close" data-dismiss="modal">&times;</button></h4>
              <div class="row">
                @csrf()
                <input type="hidden" class="form-control" name="senderLoanId" value="{{ session('loanid') }}">
                <div class="col-lg-6">
                  <div class="form-group">
                    <label for="duration">Receiver Loan</label>
                    <select class="form-control" name="receiverLoanId">
                      @php $servicedLoans = collect($loans)->where('loantype', '=', 'serviced'); @endphp
                      @foreach($servicedLoans as $key => $loan)
                      <option value="{{$loan->id}}">{{$loan->loanid}}</option>
                      @endforeach
                    </select>
                  </div>
                </div>
                <div class="col-lg-6">
                  <div class="form-group">
                      <label for="duration">Amount</label>
                      <input class="form-control" type="text" name="amount" value="{{old('amount')}}">
                  </div>
                </div>
                <div class="col-lg-6">
                  <div class="form-group">
                      <label for="chargeValue">Charge Value</label>
                      <input class="form-control" type="text" name="chargeValue" value="{{old('chargeValue')}}">
                  </div>
                </div>
                <div class="col-lg-6">
                  <div class="form-group">
                    <label>Sender Transaction Date</label>
                    <input class="form-control datepicker16" autocomplete="off" name="senderTnxDate" value="{{old('senderTnxDate')}}"/>
                  </div>
                </div>
                <div class="col-lg-6">
                  <div class="form-group">
                    <label>Receiver Transaction Date</label>
                    <input class="form-control datepicker17" autocomplete="off" name="receiverTnxDate" value="{{old('receiverTnxDate')}}"/>
                  </div>
                </div>
                <div class="col-lg-6">
                  <div class="form-group">
                    <label>Description</label>
                    <textarea class="form-control" rows="1" name="description">{{old('description')}}</textarea>
                  </div>
                </div>
                <div class="col-lg-6">
                  <div class="form-group mb-5">
                    <span class="radio-text">Capital Reduction:</span>
                    <label class="custom-radio">
                      <input type="hidden" name="isCapitalReduction" value="0">
                      <input type="checkbox" class="" name="isCapitalReduction" value="1">
                      <span class="checkmark"></span>
                    </label>
                  </div>
                </div>
                <div class="col-lg-6">
                  <div class="form-group">
                    <label for="nominalCode">Nominal Codes:</label>
                    <select class="form-control" name="nominalCode">
                      @foreach($nominalCodes as $nominalCode)
                        <option value="{{$nominalCode->id}}" {{ (old('nominalCode') == $nominalCode->id) ? 'selected' : ''}}>{{$nominalCode->code}} - {{$nominalCode->name}}</option>
                      @endforeach
                    </select>
                  </div>
                </div>
                <div class="col-lg-12">
                  <button type="submit" class="custom-padding pull-right btn btn-primary">Submit</button>
                </div>
              </div>
            </form>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
<!-- Update Renewal Charge Modal -->
<div class="modal" id="editRenewalChargeModal" data-backdrop="static">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-body custom_modal">
        <div class="row">
          <div class="col-lg-1">
            <div class="left-side">
            </div>
          </div>
          <div class="col-lg-11">
            <form action="{{'calculations/updaterenewalcharge'}}" method="post">
            <h4 class="modal-title">Edit Renewal Charge <button type="button" class="close" data-dismiss="modal">&times;</button></h4>
              @csrf()
              <div class="form-group">
                  <label for="RNDuration">Renew Loan Duration</label>
                  <select class="form-control" name="RNDuration">
                    @for($i = 1; $i <= 24; $i++)
                      <option value="{{$i}}">{{$i}}</option>
                    @endfor
                  </select>
              </div>
              <div class="form-group">
                <label>Renew Date</label>
                <input class="form-control datepicker18" autocomplete="off" name="renewDate"/>
              </div>
              <div class="form-group">
                <label>Renew Charge Value</label>
                <input type="text" class="form-control" name="value">
              </div>
              <div class="form-group">
                <label>Description</label>
                <textarea class="form-control" rows="1" name="description"></textarea>
              </div>
              <button type="submit" class="custom-padding btn pull-right btn-primary">Submit</button>
            </form>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
<!-- Assign Charge Modal-->
<div class="modal" id="assignChargeModal" data-backdrop="static">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-body custom_modal">
        <div class="row">
          <div class="col-lg-1">
            <div class="left-side">
            </div>
          </div>
          <div class="col-lg-11">
            <form action="{{'calculations/assigncharge'}}" method="get">
            <h4 class="modal-title">Add Initial Charge <button type="button" class="close" data-dismiss="modal">&times;</button></h4>
              <input type="hidden" class="form-control" name="loanId" value="{{old('loanId')}}">
              <input type="hidden" class="form-control" name="chargeId" value="{{old('chargeId')}}">
              <div class="form-group">
                  <label for="value">Value:</label>
                  <input class="form-control" name="value"/>
              </div>
              <div class="form-group">
                <label for="nominalCode">Nominal Codes:</label>
                <select class="form-control" name="nominalCode">
                  @foreach($nominalCodes as $nominalCode)
                    <option value="{{$nominalCode->id}}" {{ (old('nominalCode') == $nominalCode->id) ? 'selected' : ''}}>{{$nominalCode->code}} - {{$nominalCode->name}}</option>
                  @endforeach
                </select>
              </div>
              <div class="row">
                <div class="col-lg-12">
                  <button type="submit" class="custom-padding btn pull-right custom-padding btn-primary">Submit</button>
                </div>
              </div>
            </form>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
<!-- Assign Closing Charge Modal-->
<div class="modal" id="assignClosingChargeModal" data-backdrop="static">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-body custom_modal">
        <div class="row">
          <div class="col-lg-1">
            <div class="left-side">
            </div>
          </div>
          <div class="col-lg-11">
            <form action="{{'calculations/assignclosingcharge'}}" method="get">
              <h4 class="modal-title">Add Closing Charge <button type="button" class="close" data-dismiss="modal">&times;</button></h4>
                <input type="hidden" class="form-control" name="loanId" value="{{old('loanId')}}">
                <input type="hidden" class="form-control" name="chargeId" value="{{old('chargeId')}}">
                <div class="form-group">
                    <label for="value">Value:</label>
                    <input class="form-control" name="value"/>
                </div>
                <div class="row">
                  <div class="col-lg-12">
                    <button type="submit" class="custom-padding btn pull-right custom-padding btn-primary">Submit</button>
                  </div>
                </div>
            </form>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
<!-- Update Payment Modal -->
<div class="modal" id="updatePaymentModal" data-backdrop="static">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-body custom_modal">
        <div class="row">
          <div class="col-lg-1">
            <div class="left-side">
            </div>
          </div>
          <div class="col-lg-11">
            <form action="{{'calculations/updatepayment'}}" method="post">
              <h4 class="modal-title">Edit Payment <button type="button" class="close" data-dismiss="modal">&times;</button></h4>
                @csrf()
                <input type="hidden" class="form-control" name="paymentId" value="{{old('paymentId')}}">
                <div class="form-group">
                  <label for="amount">Amount:</label>
                  <input class="form-control" name="amount" value="{{old('amount')}}"/>
                </div>
                <div class="form-group">
                  <label for="paidAt">Date:</label>
                  <input class="form-control datepicker5" autocomplete="off" name="paidAt" value="{{old('paidAt')}}"/>
                </div>
                <div class="row">
                  <div class="col-lg-12">
                    <button type="submit" class="custom-padding btn pull-right custom-padding btn-primary">Submit</button>
                  </div>
                </div>
            </form>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
<div class="modal" id="refundModal" data-backdrop="static">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-body custom_modal">
        <div class="row">
          <div class="col-lg-1">
            <div class="left-side">
            </div>
          </div>
          <div class="col-lg-11">
            <form action="{{'calculations/createrefund'}}" method="post">
            <h4 class="modal-title">Refund Charge <button type="button" class="close" data-dismiss="modal">&times;</button></h4>
                @csrf()
                <input type="hidden" class="form-control" name="loanId" value="{{session('loanid')}}">
                <input type="hidden" class="form-control" name="chargeId">
                <input type="hidden" class="form-control" name="otherChargeId">
                <div class="form-group">
                  <label for="amount">Amount:</label>
                  <input class="form-control" name="amount"/>
                </div>
                <div class="form-group">
                  <label for="refundAt">Refund Date:</label>
                  <input class="form-control datepicker10" name="refundAt"/>
                </div>
                <div class="form-group">
                  <label for="nominalCode">Nominal Codes:</label>
                  <select class="form-control" name="nominalCode">
                    @foreach($nominalCodes as $nominalCode)
                      <option value="{{$nominalCode->id}}" {{ (old('nominalCode') == $nominalCode->id) ? 'selected' : ''}}>{{$nominalCode->code}} - {{$nominalCode->name}}</option>
                    @endforeach
                  </select>
                </div>
                <div class="form-group">
                  <label for="description">Description:</label>
                  <!-- <input class="form-control" name="description"  /> -->
                  <textarea class="form-control" rows="2" name="description"></textarea>
                </div>
                <div class="row">
                  <div class="col-lg-12">
                    <button type="submit" class="custom-padding btn pull-right custom-padding btn-primary">Submit</button>
                  </div>
                </div>
            </form>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
<div class="modal" id="defaultRateRefundModal" data-backdrop="static">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-body custom_modal">
        <div class="row">
          <div class="col-lg-1">
            <div class="left-side">
            </div>
          </div>
          <div class="col-lg-11">
            <form action="{{'calculations/createdefaultraterefund'}}" method="post">
            <h4 class="modal-title">Refund Default Rate Interest <button type="button" class="close" data-dismiss="modal">&times;</button></h4>
                @csrf()
                <input type="hidden" class="form-control" name="loanId" value="{{session('loanid')}}">
                <div class="form-group">
                  <label for="startDate">Date:</label>
                  <input class="form-control" name="startDate" readonly />
                </div>
                <!-- <div class="form-group"> -->
                  <!-- <label for="endDate">End Date:</label> -->
                  <input type="hidden" class="form-control" name="endDate" readonly />
                <!-- </div> -->
                <div class="form-group">
                  <label for="refundDate">Refund Date:</label>
                  <input class="form-control datepicker11" autocomplete="off" name="refundDate" />
                </div>
                <div class="form-group">
                  <label for="days">Days:</label>
                  <input class="form-control" name="days" readonly />
                </div>
                <div class="form-group">
                  <label for="balance">Balance:</label>
                  <input class="form-control" name="balance" readonly />
                </div>
                <div class="form-group">
                  <label for="description">Description:</label>
                  <!-- <input class="form-control" name="description"  /> -->
                  <textarea class="form-control" rows="2" name="description"></textarea>
                </div>
                <div class="row">
                  <div class="col-lg-12">
                    <button type="submit" class="custom-padding btn pull-right custom-padding btn-primary">Submit</button>
                  </div>
                </div>
            </form>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
<!-- Update Other Charge Modal (not in use now) -->
<div class="modal" id="updateOtherChargeModal" data-backdrop="static">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-body custom_modal">
        <div class="row">
          <div class="col-lg-1">
            <div class="left-side">
            </div>
          </div>
          <div class="col-lg-11">
            <form action="{{'calculations/updateothercharge'}}" method="post">
            <h4 class="modal-title">Edit Other Charge <button type="button" class="close" data-dismiss="modal">&times;</button></h4>
                @csrf()
                <input type="hidden" class="form-control" name="otherchargeid" value="{{old('otherchargeid')}}">
                <div class="form-group">
                  <label for="name">Name:</label>
                  <input class="form-control" name="name" value="{{old('name')}}"/>
                </div>
                <div class="form-group">
                  <label for="value">Amount:</label>
                  <input class="form-control" name="value" value="{{old('value')}}"/>
                </div>
                <select class="form-control d-none" name="chargetype">
                    <option value="fixed">Fixed</option>
                    <option value="percentage">Percentage</option>
                </select>
                <div class="form-group d-none">
                  <label for="amounttype">Amount Type:</label>
                  <input class="form-control" name="amounttype" value="{{old('amounttype')}}"/>
                </div>
                <div class="form-group">
                  <label for="chargedate">Charge Date:</label>
                  <input class="form-control datepicker7" autocomplete="off" name="chargedate" value="{{old('chargedate')}}"/>
                </div>
                <div class="row">
                  <div class="col-lg-12">
                    <button type="submit" class="custom-padding btn pull-right custom-padding btn-primary">Submit</button>
                  </div>
                </div>
            </form>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
<!-- Create Payment Plan Modal Modal -->
<div class="modal" id="createPaymentPlanModal" data-backdrop="static">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-body custom_modal">
        <div class="row">
          <div class="col-lg-1">
            <div class="left-side">
            </div>
          </div>
          <div class="col-lg-11">
            <form action="{{'calculations/createpaymentplan'}}" method="get">
            <h4 class="modal-title">Create Payment Plan <button type="button" class="close" data-dismiss="modal">&times;</button></h4>
                <input type="hidden" class="form-control" name="loanId">
                <div class="form-group">
                  <label for="date">Payment Plan Date:</label>
                  <input class="form-control datepicker9" name="date" autocomplete="off" />
                </div>
                <div class="row">
                  <div class="col-lg-12">
                    <button type="submit" class="custom-padding btn pull-right custom-padding btn-primary">Submit</button>
                  </div>
                </div>
            </form>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
<!-- Freeze Loan Modal -->
<div class="modal" id="freezeLoanModal" data-backdrop="static">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-body custom_modal">
        <div class="row">
          <div class="col-lg-1">
            <div class="left-side">
            </div>
          </div>
          <div class="col-lg-11">
            <form class="freezeForm" action="{{'updateloanstatus'}}" method="get"
              data-href="{{url('updateloanstatus?loanId='.session('loanid').'&status=freezed')}}">
              <h4 class="modal-title">Freeze Loan <button type="button" class="close" data-dismiss="modal">&times;</button></h4>
                <div class="form-group">
                  <label for="freezeDate">Freeze Date:</label>
                  <input class="form-control datepicker12 freeze-date" name="freezeDate" autocomplete="off" />
                </div>
                <div class="row">
                  <div class="col-lg-12">
                    <button type="submit" class="btn pull-right custom-padding btn-primary">Submit</button>
                  </div>
                </div>
            </form>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
<!-- Un-Freeze Loan Modal -->
<div class="modal" id="unFreezeLoanModal" data-backdrop="static">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <h4 class="modal-title">Un-Freeze Loan</h4>
        <button type="button" class="close" data-dismiss="modal">&times;</button>
      </div>
      <div class="modal-body custom_modal">
        <div class="row">
          <div class="col-lg-1">
            <div class="left-side">
            </div>
          </div>
          <div class="col-lg-11">
            <form class="unFreezeForm" action="{{'updateloanstatus'}}" method="get"data-href="{{url('updateloanstatus?loanId='.session('loanid').'&status=approved')}}">
            <h4 class="modal-title">Un-Freeze Loan  <button type="button" class="close" data-dismiss="modal">&times;</button></h4>
              <input type="hidden" class="form-control" name="loanId">
              <div class="form-group">
                <label for="freezeDate">Un-Freeze Date:</label>
                <input class="form-control datepicker13 un-freeze-date" name="freezeDate" autocomplete="off" />
              </div>
              <div class="row">
                <div class="col-lg-12">
                  <button type="submit" class="btn pull-right custom-padding btn-primary">Submit</button>
                </div>
              </div>
            </form>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
<!-- Close Loan Modal -->
  <div class="modal" id="closeLoanModal" data-backdrop="static">
    <div class="modal-dialog" style="max-width: 1200px;">
      <div class="modal-content">
        <div class="modal-body custom_modal">
          <div class="row">
            <div class="col-lg-1">
              <div class="left-side">
              </div>
            </div>
            <div class="col-lg-11">
              <form class="closeLoanModal" action="{{'updateloanstatus'}}" method="get"
                data-href="{{url('updateloanstatus?loanId='.session('loanid').'&status=closed')}}">
                <h4 class="modal-title">Close Loan Calculator <button type="button" class="close" data-dismiss="modal">&times;</button></h4>
                <input type="hidden" class="form-control" name="loanId">
                <div class="js-closing-balance">
                  <p>Please pick a date for closing balance.</p>
                </div>
                <div class="row">
                  <div class="col-6 form-group">
                    <label for="freezeDate">Closing Date:</label>
                    <input class="closeLoanCalculator form-control datepicker14 closing-date" name="freezeDate" id="CLFreezeDate" autocomplete="off" />
                  </div>
                  <div class="col-6 form-group">
                    <label for="virtualPmt">Virtual Payment:</label>
                    <select class="closeLoanCalculator form-control" name="virtualPmt" id="CLVirtualPmt">
                      <option value="false">Without Payment</option>
                      <option value="true">Including Payment</option>
                    </select>
                  </div>
                </div>
                <div class="row">
                  <div class="col-lg-12">
                    <!-- <button type="submit" class="btn custom-padding btn-primary pull-right js-closeloan-btn">Close loan</button> -->
                  </div>
                </div>
              </form>
              <div class="js-final-breakdown">
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>

<!-- Create Tranch Modal -->
  <div class="modal" id="createTranchModal" data-backdrop="static">
    <div class="modal-dialog" style="max-width: 800px;">
      <div class="modal-content">
        <div class="modal-body custom_modal">
          <div class="row">
            <div class="col-lg-1">
              <div class="left-side">
              </div>
            </div>
            <div class="col-lg-11">
              <form action="{{url('calculations/createtranch')}}" method="post">
                <h4 class="modal-title">Create Tranche <button type="button" class="close" data-dismiss="modal">&times;</button></h4>
                <div class="row">
                  @csrf()
                  <div class="col-4 form-group">
                    <label>Tranche Date:</label>
                    <input class="form-control datepicker22" name="tranchDate" autocomplete="off" />
                  </div>
                  <div class="col-4 form-group">
                    <label>Net loan:</label>
                    <input class="form-control" type="text" name="amount">
                  </div>
                  @if($selectedLoan && (strtolower($selectedLoan->loantype) == 'tranch' || strtolower($selectedLoan->loantype) == 'retained'))
                  <div class="col-4 form-group">
                    <label>Gross Loan:</label>
                    <input class="form-control" type="text" name="grossLoan">
                  </div>
                  @endif
                  <div class="col-4 form-group">
                    <div class="form-group">
                      <label for="nominalCode">Nominal Codes:</label>
                      <select class="form-control" name="nominalCode">
                        @foreach($nominalCodes as $nominalCode)
                          <option value="{{$nominalCode->id}}" {{ (old('nominalCode') == $nominalCode->id) ? 'selected' : ''}}>{{$nominalCode->code}} - {{$nominalCode->name}}</option>
                        @endforeach
                      </select>
                    </div>
                  </div>
                  <div class="col-8 form-group">
                    <label>Description:</label>
                    <textarea class="form-control" name="description"></textarea>
                  </div>
                </div>
                <div class="row">
                  <div class="col-lg-12">
                    <button type="submit" class="btn custom-padding btn-primary pull-right js-closeloan-btn">Create Tranche</button>
                  </div>
                </div>
              </form>
              <div class="js-final-breakdown">
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
<!-- Tranch Modal -->
  <div class="modal" id="tranchModal" data-backdrop="static">
    <div class="modal-dialog" style="max-width: 800px;">
      <div class="modal-content">
        <div class="modal-body custom_modal">
          <div class="row">
            <div class="col-lg-1">
              <div class="left-side">
              </div>
            </div>
            <div class="col-lg-11">
              <h4 class="panel-title">
                All Tranches
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <button type="button" class="btn btn-primary ml-1 pull-right createTranchModal custom-button" data-toggle="modal" data-target="#createTranchModal">
                  Create Tranch
                </button>
              </h4>
              @if($selectedLoan)
              <div class="table-scroll">
                <table class="table table-striped table-hover table-bordered text-left">
                  <thead>
                    <tr>
                      <th>Date</th>
                      <th>Tranch Amount</th>
                      <th>Descriptions</th>
                    </tr>
                  </thead>
                  <tbody>
                    @foreach($selectedLoan->tranch as $key => $tranch)
                    <tr>
                      <td>{{($tranch->tranch_date) ? \Carbon\Carbon::parse($tranch->tranch_date)->format('Y M d') : null}}</td>
                      <td>{{number_format($tranch->amount, 2)}}</td>
                      <td>{{$tranch->description}}</td>
                    </tr>
                    @endforeach
                  </tbody>
                </table>
              </div>
              @endif
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
<!-- Edit Closing Loan Charge Modal -->
<div class="modal" id="editClosingLoanChargeModal" data-backdrop="static">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-body custom_modal">
        <div class="row">
          <div class="col-lg-1">
            <div class="left-side">
            </div>
          </div>
          <div class="col-lg-11">
            <form class="closeLoanModal" action="{{'calculations/updateclosingloancharge'}}" method="post">
            <h4 class="modal-title">Edit Closing Loan Charge <button type="button" class="close" data-dismiss="modal">&times;</button></h4>
              @csrf()
              <input type="hidden" class="form-control" name="loanId">
              <input type="hidden" class="form-control" name="chargeId">
              <div class="form-group">
                <label for="value">Value:</label>
                <input class="form-control" type="text" name="value" id="clc-value" autocomplete="off" />
              </div>
              <div class="row">
                <div class="col-lg-12">
                  <button type="submit" class="btn pull-right custom-padding btn-primary">Submit</button>
                </div>
              </div>
            </form>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
<!-- Edit Min Term Modal Modal -->
<div class="modal" id="minTermModal" data-backdrop="static">
  <div class="modal-dialog" style="max-width: 700px">
    <div class="modal-content">
      <div class="modal-body custom_modal">
        <div class="row">
          <div class="col-lg-1">
            <div class="left-side">
            </div>
          </div>
          <div class="col-lg-11">
            <form action="{{'calculations/updateminterm'}}" method="post">
            <h4 class="modal-title">Edit Min Term <button type="button" class="close" data-dismiss="modal">&times;</button></h4>
              @csrf()

              <div class="row">
                <div class="col-6 form-group">
                  <label for="minTerm">Minimum Term:</label>
                  <input class="form-control" type="text" step="any" name="minTerm" id="mt-minTerm" autocomplete="off" />
                </div>

                <div class="col-6 form-group">
                  <label for="monthlyPmt">Monthly Payment:</label>
                  <input class="form-control" type="text" step="any" name="monthlyPmt" id="mt-monthlyPmt" autocomplete="off" />
                </div>

                <div class="col-6 form-group">
                  <label for="amount">Amount Pending:</label>
                  <input class="form-control" type="text" step="any" name="amount" id="mt-amount" autocomplete="off" />
                </div>

                <div class="col-6 form-group">
                  <label for="amountPaid">Amount Paid:</label>
                  <input class="form-control" type="text" step="any" name="amountPaid" id="mt-amountPaid" disabled="disabled" autocomplete="off" />
                </div>

                <div class="col-12">
                  <button type="submit" class="btn custom-padding pull-right btn-primary">Submit</button>
                </div>
              </div>
            </form>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
<!-- Edit Min Term Modal Modal -->
  <div class="modal" id="addNewRowInLoanExtrasModal" data-backdrop="static">
    <div class="modal-dialog" style="max-width: 1100px">
      <div class="modal-content">
        <div class="modal-body custom_modal">
          <div class="row">
            <div class="col-lg-1">
              <div class="left-side">
              </div>
            </div>
            <div class="col-lg-11">
              <form action="{{'calculations/addnewrowinloanextras'}}" method="post">
              <h4 class="modal-title">Add notes/breakdown <button type="button" class="close" data-dismiss="modal">&times;</button></h4>
                @csrf()

                <h6>Notes:</h6>
                <div class="row">
                  <div class="col-12 form-group">
                    <!-- <label for="minTerm">Notes:</label> -->
                    <textarea class="form-control" name="note[]"></textarea>
                  </div>
                </div>

                <h6>Breakdown</h6>
                <div class="row">  
                  <div class="col-2 form-group">
                    <label>From:</label>
                    <input class="form-control" type="text" name="breakdown[]"/>
                  </div>

                  <div class="col-2 form-group">
                    <label>To:</label>
                    <input class="form-control" type="text" name="breakdown[]"/>
                  </div>

                  <div class="col-2 form-group">
                    <label>Days:</label>
                    <input class="form-control" type="text" name="breakdown[]"/>
                  </div>

                  <div class="col-2 form-group">
                    <label>Amount:</label>
                    <input class="form-control" type="text" name="breakdown[]"/>
                  </div>

                  <div class="col-2 form-group">
                    <label>Daily:</label>
                    <input class="form-control" type="text" name="breakdown[]"/>
                  </div>

                  <div class="col-2 form-group">
                    <label>Total:</label>
                    <input class="form-control" type="text" name="breakdown[]"/>
                  </div>

                  <div class="col-12">
                    <button type="submit" class="btn custom-padding pull-right btn-primary">Submit</button>
                  </div>
                </div>
              </form>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>

<!-- Add new option Modal Modal -->
  <div class="modal" id="addNewOptionModal" data-backdrop="static">
    <div class="modal-dialog" style="max-width: 1100px">
      <div class="modal-content">
        <div class="modal-body custom_modal">
          <div class="row">
            <div class="col-lg-1">
              <div class="left-side">
              </div>
            </div>
            <div class="col-lg-11">
              <form action="{{url('calculations/createoption')}}" method="post">
              <h4 class="modal-title"><span class="optModalTitle">Add New Option</span> <button type="button" class="close" data-dismiss="modal">&times;</button></h4>
                @csrf()
                <div class="row">  
                  <div class="optId">
                    <input type="hidden" name="optId">
                  </div>
                  <div class="col-4 form-group">
                    <label>Name:</label>
                    <input class="form-control" type="text" name="name"/>
                  </div>

                  <div class="col-4 form-group">
                    <label>Value:</label>
                    <input class="form-control" type="text" name="value"/>
                  </div>

                  <div class="col-4 form-group">
                    <label>Create Date:</label>
                    <input class="form-control datepicker21" autocomplete="off" name="createDate"/>
                  </div>

                  <input type="hidden" name="mappedId">
                  
                  <div class="col-12">
                    <button type="submit" class="btn custom-padding pull-right btn-primary">Submit</button>
                  </div>
                </div>
              </form>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
<!-- Edit option Modal Modal -->
  <div class="modal" id="editOptionModal" data-backdrop="static">
    <div class="modal-dialog" style="max-width: 1100px">
      <div class="modal-content">
        <div class="modal-body custom_modal">
          <div class="row">
            <div class="col-lg-1">
              <div class="left-side">
              </div>
            </div>
            <div class="col-lg-11">
              <form action="{{url('calculations/createoption')}}" method="post" class="js-edit-option">
              <h4 class="modal-title"><span class="optModalTitle">Add New Option</span> <button type="button" class="close" data-dismiss="modal">&times;</button></h4>
                @csrf()
                <div class="row">  
                  <div class="optId">
                    <input type="hidden" name="optId">
                  </div>
                  <div class="col-4 form-group">
                    <label>Name:</label>
                    <input class="form-control" type="text" name="name"/>
                  </div>

                  <div class="col-4 form-group">
                    <label>Value:</label>
                    <input class="form-control" type="text" name="value"/>
                  </div>

                  <div class="col-4 form-group">
                    <label>Create Date:</label>
                    <input class="form-control datepicker21" autocomplete="off" name="createDate"/>
                  </div>

                  <input type="hidden" name="mappedId">
                  
                  <div class="col-12">
                    <button type="submit" class="btn custom-padding pull-right btn-primary optionBtn">Submit</button>
                  </div>

                  <p class="text-warning js-option-response"></p>

                </div>
              </form>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>

<!-- Hide Text Modal-->
  <div class="modal" id="hideModal" data-backdrop="static" style="z-index: 1111;">
    <div class="modal-dialog" style="max-width: 700px">
      <div class="modal-content">
        <div class="modal-body custom_modal">
          <div class="row">
            <div class="col-lg-1">
              <div class="left-side">
              </div>
            </div>
            <div class="col-lg-11">
              <h4 class="modal-title text-capitalize">Transaction hide text<button type="button" class="close" data-dismiss="modal">&times;</button></h4>
              <form action="{{url('update-tnx')}}" method="get" class="js-ajax-form">
                @csrf()
                <input type="hidden" class="form-control" name="tnxId">
                <div class="form-group">
                  <label for="type">Hide Text:</label>
                  <input type="text" class="form-control" name="hide">
                </div>
                <button type="submit" class="btn btn-primary">Submit</button>
                <div class="js-ajax-response"></div>
              </form>
            </div>          
          </div>
        </div>
      </div>
    </div>
  </div>
<!-- Add Notes Modal-->
  <div class="modal" id="notesModal" data-backdrop="static" style="z-index: 1111;">
    <div class="modal-dialog" style="max-width: 700px">
      <div class="modal-content">
        <div class="modal-body custom_modal">
          <div class="row">
            <div class="col-lg-1">
              <div class="left-side">
              </div>
            </div>
            <div class="col-lg-11">
              <h4 class="modal-title text-capitalize">Transaction notes <button type="button" class="close" data-dismiss="modal">&times;</button></h4>
              <form action="{{url('update-tnx')}}" method="get" class="js-ajax-form">
                @csrf()
                <input type="hidden" class="form-control" name="tnxId">
                <div class="form-group">
                  <label for="type">Notes:</label>
                  <textarea class="form-control js-tnx-notes" name="notes"></textarea>
                  <!-- <input type="text" class="form-control" name="type"> -->
                </div>
                <button type="submit" class="btn btn-primary">Submit</button>
                <div class="js-ajax-response"></div>
              </form>
            </div>          
          </div>
        </div>
      </div>
    </div>
  </div>
<!-- Tags Modal-->
  <div class="modal" id="tagsModal" data-backdrop="static" style="z-index: 1041;">
    <div class="modal-dialog" style="max-width: 80%">
      <div class="modal-content">
        <div class="modal-body custom_modal">
          <div class="row">
            <div class="col-lg-1">
              <div class="left-side">
              </div>
            </div>
            <div class="col-lg-11">
              <h4 class="modal-title text-capitalize">Transaction Tags <button type="button" class="close" data-dismiss="modal">&times;</button></h4>
              <form action="{{url('transaction-tags')}}" method="post">
                @csrf()
                <input type="hidden" class="form-control" name="tnxid">
                <div class="form-group">
                  <label for="type">Select Tags:</label>
                  @if($tags)
                  <select class="js-multi-search" name="tagid[]" multiple="multiple" style="width: 30%;">
                    @foreach($tags as $tag)
                    <option value="{{$tag->id}}">{{$tag->name}}</option>
                    @endforeach
                  </select>
                  @endif
                </div>
                <button type="submit" class="btn btn-primary">Submit</button>
                <div class="js-ajax-response"></div>
              </form>
            </div>          
          </div>
        </div>
      </div>
    </div>
  </div>
<!-- Hidden Statements Modal -->
  <div class="modal" id="hiddenStatementsModal" data-backdrop="static" style="z-index: 1111;">
    <div class="modal-dialog" style="max-width: 80%">
      <div class="modal-content">
        <div class="modal-body custom_modal">
          <div class="row">
            <div class="col-lg-1">
              <div class="left-side">
              </div>
            </div>
            <div class="col-lg-11">
              <h4 class="modal-title text-capitalize">Hidden Statements/Transactions <button type="button" class="close" data-dismiss="modal">&times;</button></h4>
              <div class="panel-body">
                <table class="table table-striped table-hover table-bordered text-left m-0">
                  <thead>
                    <tr>
                      <th>Date</th>
                      <th>Debit</th>
                      <th>Credit</th>
                      <th>Balance</th>
                      <th>Descriptions</th>
                      <th colspan="2"></th>
                      <th>Nominal Code</th>
                      <th>Transaction Id</th>
                      <th>Hide(Statement/Transaction)</th>
                      <th>Notes</th>
                    </tr>
                  </thead>
                  <tbody>
                    @foreach(collect($statements)->where('hide', 1)->all() as $key => $statement)
                    <tr>
                      <td>{{\Carbon\Carbon::parse($statement->created_at->date)->format('Y M d')}}</td>
                      <td>{{ !is_null($statement->debit) ? number_format($statement->debit, 2) : null}}</td>
                      <td>{{ !is_null($statement->credit) ? number_format($statement->credit, 2) : null}}</td>
                      <td>{{number_format($statement->balance, 2)}}</td>
                      <td colspan="2" class="text-capitalize">{{$statement->comments}}
                        @if($statement->interestBreakDown)
                        <div class="target-description" data-key="{{$key}}">
                          <span class="text-primary fas fa-chevron-up cursor"></span>
                          <!-- <span class="text-primary fa fa-minus"></span> -->
                        </div>
                        @endif
                      </td>
                      <td>
                        <span class="label text-capitalize
                          @if(($statement->description == 'payment' || $statement->description == 'refund') && $statement->credit > 0)
                              bg-success
                          @elseif($statement->description == 'other_charge_refund' || $statement->description == 'other_charge')
                              bg-primary
                          @elseif($statement->description == 'refund_default_interest')
                              bg-warning
                          @elseif($statement->description == 'interest')
                              bg-info
                          @elseif($statement->description == 'capreduction')
                              bg-danger
                          @endif
                        ">{{$statement->explanation}}</span>
                      </td>
                      <td>{{$statement->nominalCode}}</td>
                      <td>{{$statement->transactionId}}</td>
                      @if($statement->id)
                      <td class="text-capitalize">
                        <label class="switch">
                          <input class="hideStatement" data-tnxid="{{$statement->id}}" type="checkbox" value="{{($statement->hide == 1) ? '1' : '0' }}" {{($statement->hide == 1) ? 'checked' : '' }}>
                          <span class="slider round"></span>
                        </label>
                        <label class="switch">
                          <input class="hideTransaction" data-tnxid="{{$statement->id}}" type="checkbox" value="{{($statement->hideTransaction == 1) ? '1' : '0' }}" {{($statement->hideTransaction == 1) ? 'checked' : '' }}>
                          <span class="slider round"></span>
                        </label>
                        <!-- <i class="fas fa-edit cursor pull-right hideModal" data-toggle="modal" data-target="#hideModal" data-tnxid="{{$statement->id}}" data-value = "{{($statement->hide) ? $statement->hide : '' }}" aria-hidden="true"></i> -->
                      </td>
                      <td class="text-capitalize">{{($statement->notes) ? $statement->notes : '' }}
                        <!-- <i class="fas fa-edit cursor pull-right notesModal" data-toggle="modal" data-target="#notesModal" data-tnxid="{{$statement->id}}" data-value = "{{($statement->notes) ? $statement->notes : '' }}" aria-hidden="true"></i> -->
                      </td>
                      @else
                      <td></td>
                      <td></td>
                      @endif
                    </tr>
                      @if($statement->interestBreakDown)
                      <tr class="show-description{{$key}} font-weight-bold font-italic"  style="display: none; background-color: #efefef; font-size: 12px;">
                        <td>From</td>
                        <td>To</td>
                        <td>Days @if($selectedLoan->loantype === 'retained') (Months) @endif</td>
                        <td>Amount</td>
                        <td>Interest Rate</td>
                        <td>Daily Charges(Per day charge)</td>
                        <td>Total(Days * Daily Charges)</td>
                        @if(isset($statement->interestBreakDown[0]->rounding_error))
                        <td>Rounding Error</td>
                        <td></td>
                        @else
                        <td></td>
                        <td></td>
                        @endif
                        <td></td>
                        <td></td>
                      </tr>
                      @foreach($statement->interestBreakDown as $breakDown)
                      <tr class="font-italic show-description{{$key}} @if($breakDown->rate == $selectedLoan->interest->default_rate && $breakDown->total >0) text-danger @elseif($breakDown->total <= 0) text-success @endif"  style="display: none; background-color: #efefef; font-size: 12px;">
                        <td>{{\Carbon\Carbon::parse($breakDown->start->date)->format('Y M d')}}</td>
                        <td>{{\Carbon\Carbon::parse($breakDown->end->date)->format('Y M d')}}</td>
                        <td>{{$breakDown->days}}
                          @if($selectedLoan->loantype === 'retained')
                            @php
                            $date1 = \Carbon\Carbon::parse($breakDown->start->date);
                            $date2 = \Carbon\Carbon::parse($breakDown->end->date)->copy()->addDay();
                            $interval = date_diff($date1, $date2);
                            // dd($interval->format('%m'));
                            $months = $date1->diffInMonths($date2);
                            $days = $date1->copy()->addMonths($months)->diffInDays($date2);
                            $days += $days ? 1 : 0;
                            @endphp
                            ({{round($days/30.4, 2) + $months}})
                            <!--({{ ($interval->format('%m') == 0) ? ($interval->format('%d') == $date1->copy()->startOfMonth()->diffInDays($date1->copy()->endOfMonth())) ? 1 : 0 : $interval->format('%m') }}) -->
                          @endif
                        </td>
                        <td>{{number_format($breakDown->balance, 2)}}</td>
                        <td>{{(float)$breakDown->rate}}</td>
                        <td>{{(float)$breakDown->daily_rate}}</td>
                        <td>{{number_format($breakDown->total, 2)}}</td>
                        @if(isset($breakDown->rounding_error))
                        <td>{{$breakDown->rounding_error ? $breakDown->rounding_error : $breakDown->rounding_error}}</td>
                        <td></td>
                        @else
                        <td></td>
                        <td></td>
                        @endif
                        <td></td>
                        <td></td>
                      </tr>
                      @endforeach
                      @endif
                    @endforeach
                  </tbody>
                </table>
              </div>
            </div>          
          </div>
        </div>
      </div>
    </div>
  </div>
@endsection
@php 
$loanDate = null;
if(count($statements) > 0){
  $lastStatement = collect($statements)->last();
  if($lastStatement->interestBreakDown != null)
    $loanDate = \Carbon\Carbon::parse(collect($lastStatement->interestBreakDown)->last()->end->date);
  else
    $loanDate = \Carbon\Carbon::parse($lastStatement->created_at->date);

  if($loanDate->gt(\Carbon\Carbon::now()))
    $loanDate = $loanDate->format('Y-m-d');
  else
    $loanDate = null;
}

@endphp
@section('pages-js')
<script src="{{url('assets/js').'/calculator-view.js'}}"></script>
<script>
// pick up loan date here and set in app file.
// var date =  "{{(count($statements) > 0) ? ( \Carbon\Carbon::parse(collect($statements)->last()->created_at->date)->gt(\Carbon\Carbon::now()) ) ? \Carbon\Carbon::parse(collect($statements)->last()->created_at->date)->format('Y-m-d'): null : null }}";
var date = "{{$loanDate}}";

$(".PaymentsModal").click(function () {
    $("#Paid-Amounts").modal('toggle');
});

$(".createTranchModal").click(function () {
    $("#tranchModal").modal('toggle');
});
$(".renew-charges-modal").click(function () {
    $("#updateLoanDurationModal").modal('toggle');
});
$(".Create-Filler").click(function () {
    $("#Fillers_").modal('toggle');
});
$(".otherChargesModal").click(function () {
    $("#otherChargesdisplay").modal('toggle');
});
    $(document).ready(function(){
        $(document).on('click', '.target-description', function() {
          var key = $(this).data('key');
          $('.show-description'+key).toggle("slide");
          $(this).find(".svg-inline--fa").toggleClass("fa-chevron-up fa-chevron-down" );
        });
    });

    $(document).on("click", ".loanModal", function () {
        // var loanId = $(this).data('loanid');
        $("input[name=loanId]").val($(this).data('loanid'));
        $("input[name=netloan]").val($(this).data('netloan'));
        $("input[name=rate]").val($(this).data('rate'));
        $("input[name=defaultRate]").val($(this).data('defaultrate'));
        $("input[name=duration]").val($(this).data('duration'));
        $('input[name=holidayRefund]').prop('checked', false);
        if($(this).data('holidayrefund')){
            $('input[name=holidayRefund]').prop('checked', true);
        }
    });

    $(document).on("click", ".assignChargeModal", function () {
        var loanId = $(this).data('loanid');
        var chargeId = $(this).data('chargeid');

        $("input[name=loanId]").val(loanId);
        $("input[name=chargeId]").val(chargeId);
        $("input[name=value]").val($(this).data('value'));
    });

    $(document).on("click", ".assignClosingChargeModal", function () {
        var loanId = $(this).data('loanid');
        var chargeId = $(this).data('chargeid');

        $("input[name=loanId]").val(loanId);
        $("input[name=chargeId]").val(chargeId);
        $("input[name=value]").val($(this).data('value'));
    });

    $(document).on("click", ".paymentModal", function () {
        var paymentId = $(this).data('paymentid');
        var amount = $(this).data('amount');
        var paymentDate = $(this).data('paymentdate');

        $("input[name=paymentId]").val(paymentId);
        $("input[name=amount]").val(amount);
        $("input[name=paidAt]").val(paymentDate);
    });

    $(document).on("click", ".otherChargeModal", function () {
        $("input[name=otherchargeid]").val($(this).data('otherchargeid'));
        $("input[name=name]").val($(this).data('name'));
        $("input[name=value]").val($(this).data('value'));
        $("[name=chargetype]").val($(this).data('chargetype'));
        $("input[name=amounttype]").val($(this).data('amounttype'));
        $("input[name=chargedate]").val($(this).data('chargedate'));
    });

    $(document).on("click", ".refundModal", function () {
        $("input[name=amount]").val($(this).data('value'));
        $("input[name=refundAt]").val($(this).data('chargedate'));
        $("input[name=chargeId]").val($(this).data('chargeid'));
        $("input[name=otherChargeId]").val($(this).data('otherchargeid'));
    });

    $(document).on("click", ".defaultRateRefundModal", function () {
        $("input[name=startDate]").val($(this).data('startdate'));
        $("input[name=endDate]").val($(this).data('enddate'));
        $("input[name=days]").val($(this).data('days'));
        $("input[name=balance]").val($(this).data('balance'));
    });

    $(document).on("click", ".createPaymentPlanModal", function () {
        $("input[name=loanId]").val($(this).data('loanid'));
    });

    $(document).on("click", ".editClosingLoanChargeModal", function () {
        $("input[name=loanId]").val($(this).data('loanid'));
        $("input[name=chargeId]").val($(this).data('chargeid'));
        $("input[name=value]").val($(this).data('value'));
    });

    $(document).on("click", ".minTermModal", function () {
        $("input[name=loanId]").val($(this).data('loanid'));
        $("input[name=minTerm]").val($(this).data('minterm'));
        $("input[name=monthlyPmt]").val($(this).data('mpmt'));
        $("input[name=amountPaid]").val($(this).data('amountpaid'));
        $("#mt-amount").val($(this).data('amount'));
    });

    $(document).on("click", ".addNewOptionModal", function () {
      $('.js-option-response').html('');
      // $("input[name=optId]").val('');
      $("input[name=name]").val('');
      $("input[name=value]").val('');
      $("input[name=createDate]").val('');
      $("input[name=mappedId]").val('');
      $(".optModalTitle").text('Add Optional Variable');
    });

    $(document).on("click", ".editOptionModal", function () {
      $('.js-option-response').html('');
      $("input[name=optId]").val($(this).data('opt-id'));
      $("input[name=name]").val($(this).data('name'));
      $("input[name=value]").val($(this).data('value'));
      $("input[name=createDate]").val($(this).data('create-date'));
      $("input[name=mappedId]").val($(this).data('mapped-id'));
      $(".optModalTitle").text('Edit Optional Variable');
    });

    $(document).ready(function(){
      $('.collapse').on('shown.bs.collapse', function(){
          $(this).parent().find(".fa-chevron-up").removeClass("fa-chevron-up").addClass("fa-chevron-down");
      }).on('hidden.bs.collapse', function(){
          $(this).parent().find(".chevron-down").removeClass("chevron-down").addClass("chevron-up");
      });
    });

    $(document).on('click', '.convertToServiceLoan', function(){
      adjustDates();
    });

    $('.js-HLduration, .js-HLduration2').on('change paste keyup', function(){
      adjustDates();
    });

    $('.js-HLendDate').on('change paste keyup', function(){
      setHLDuration();
      adjustDates();
    });
    $('.js-HLendDate2').on('change paste keyup', function(){
      setHLDuration2();
    });

    function setHLDuration()
    {
      var fromDate = $('.js-HLstartDate').val();
      var toDate = $('.js-HLendDate').val();
      var months = getDuration(fromDate, toDate);
      $('.js-HLduration').val(months);
    }

    function setHLDuration2()
    {
      var fromDate = $('.js-HLstartDate2').val(), toDate = $('.js-HLendDate2').val(), from, to, months, days, totalNoOfDays;
      from = moment(fromDate); // format in which you have the date
      to = moment(toDate).add(1, 'days');     // format in which you have the date
      /* using diff */
      months = to.diff(from, 'months');
      totalNoOfDays = to.diff(from, 'days');
      months = to.diff(from, 'months', true).toFixed(2);
      $('.js-HLduration2').val(months);
    }

    function getDuration(fromDate, toDate)
    {
      from = moment(fromDate); // format in which you have the date
      to = moment(toDate).add(1, 'days');     // format in which you have the date
      /* using diff */
      months = to.diff(from, 'months');
      totalNoOfDays = to.diff(from, 'days');
      return months = to.diff(from, 'months', true).toFixed(2);
      
    }

    function adjustDates() {
      var issuedate = $('.js-HLstartDate').val();
      var duration = $('.js-HLduration').val();
      var duration2 = $('.js-HLduration2').val();

      var endDate = getEndDate(issuedate, duration);

      $('.js-HLendDate').val(moment(endDate).subtract(1, 'd').format('YYYY-MM-DD'));

      var halfStartDate = endDate;
      var endDate = getEndDate(halfStartDate, duration2);
      $('.js-HLstartDate2').val(moment(halfStartDate).format('YYYY-MM-DD'));
      $('.js-HLendDate2').val(moment(endDate).subtract(1, 'd').format('YYYY-MM-DD'));
      // $('.js-ServiceLoanEndDate').val(endDate.format('YYYY-MM-DD'));
    }

    function getEndDate(issuedate, duration)
    {
      var monthNumber = duration.toString().split(".")[0];
      var daysOfDec = parseFloat(0+"."+(duration.toString().split(".")[1])) * 30.4;
      var endDate = moment(issuedate).add(monthNumber, 'M');
      endDate = moment(endDate).add(daysOfDec, 'd');
      return endDate;
    }

    // $(document).on("click", ".editOptionModal", function () {
    //     $("input[name=optId]").val($(this).data('opt-id'));
    //     $("input[name=name]").val($(this).data('name'));
    //     $("input[name=value]").val($(this).data('value'));
    //     $("input[name=createDate]").val($(this).data('create-date'));
    //     $("input[name=mappedId]").val($(this).data('mapped-id'));
    //     $(".optModalTitle").text('Edit Optional Variable');
    // });

    $(document).on("click", ".hideModal", function() {
      $("input[name=tnxId").val($(this).data('tnxid'));
      $("input[name=hide").val($(this).data('value'));
    });

    $(document).on('click', ".notesModal", function(){
      $("input[name=tnxId]").val($(this).data('tnxid'));
      $(".js-tnx-notes").val($(this).data('value'));
    });

    $(document).on('click', ".tagsModal", function(){
      $("input[name=tnxid]").val($(this).data('tnxid'));
      // $(".js-tnx-notes").val($(this).data('value'));
    });

    $(".js-ajax-form").submit(function(e){
      e.preventDefault();
      var action = $(this).attr("action"); //get form action url
      var requestMethod = $(this).attr("method"); //get form GET/POST method
      var formData = $(this).serialize(); //Encode form elements for submission
      console.log(action, requestMethod, formData);
      $.ajax({
        url : action,
        type: requestMethod,
        data : formData,
        success: function(result){
          console.log('success');
          if(result.code == 400){
            $(".js-ajax-response").html('<p class="text-danger">Error updating transaction info. Please try again later.</p>');
          }
          else 
          window.location.reload();
        }, 
        error: function(errors){
          $(".js-ajax-response").html('<p class="text-danger">Error updating transaction info. Please try again later.</p>');
        }
      });
    });

    $(".js-edit-option").submit(function(e){
      e.preventDefault();
      var action = $(this).attr("action"); //get form action url
      var requestMethod = $(this).attr("method"); //get form GET/POST method
      var formData = $(this).serialize();
      console.log(formData);
      if(jQuery('input[name="mappedId"]').val()){
        $('.js-option-response').html('There is another variable mapped to this variable. Please sync both values together.');
      }
      $.ajax({
        url : action,
        type: requestMethod,
        data : formData,
        // beforeSend: function(){
        // },
        success: function(result){
          if(result.code == 400)
          {
            $('.js-option-response').html('<span class="text-danger">Error creating Optional Variable. It may be the variable name has already taken or internal server error.</span>');
          }
          window.location.reload();
        },
        error: function(errors){
          $('.js-option-response').html('An internal server error has been occurred.');
        }
      });
      $('.optionBtn').prop('disabled', false);
    });

    $(".hideStatement").on('change', function(){
      var hide = $(this).val();
      if(hide == 1){
        hide = 0;
        $(this).val('0');
      }
      else{
        hide = 1;
        $(this).val('1');
      }
      var tnxId = $(this).data('tnxid');
      console.log(hide, tnxId);
      $.ajax({
        url : "{{url('update-tnx')}}",
        type: 'get',
        data : {hide: hide, tnxId: tnxId},
        success: function(result){
          console.log('success');
          window.location.reload();
        }, 
        error: function(errors){
          console.log('error');
          window.location.reload();
          // $(".js-ajax-response").html('<p class="text-danger">Error updating transaction info. Please try again later.</p>');
        }
      });
    });

    $(".hideTransaction").on('change', function(){
      var hideTransaction = $(this).val();
      if(hideTransaction == 1){
        hideTransaction = 0;
        $(this).val('0');
      }
      else{
        hideTransaction = 1;
        $(this).val('1');
      }
      var tnxId = $(this).data('tnxid');
      console.log(hideTransaction, tnxId);
      $.ajax({
        url : "{{url('update-tnx')}}",
        type: 'get',
        data : {hideTransaction: hideTransaction, tnxId: tnxId},
        success: function(result){
          console.log('success');
          window.location.reload();
        }, 
        error: function(errors){
          console.log('error');
          window.location.reload();
          // $(".js-ajax-response").html('<p class="text-danger">Error updating transaction info. Please try again later.</p>');
        }
      });
    });

    $(".verifyStatementDate").on('change', function(){
      var val = $(this).val();
      if(val == 1){
        // val = 0;
        $(this).val('0');
      }
      else{
        // val = 1;
        $(this).val('1');
      }
      $.ajax({
        url : "{{url('verify-statement-date')}}",
        type: 'get',
        // data : {hideTransaction: hideTransaction, tnxId: tnxId},
        success: function(result){
          console.log(result);
          // window.location.reload();
        }, 
        error: function(errors){
          console.log(errors);
          // window.location.reload();
        }
      });
    });

    // ClassicEditor.create( document.querySelector( '.ckeditor' ) )
    //             .then( editor => {
    //                     console.log( editor );
    //             } )
    //             .catch( error => {
    //               console.error( error );
    //             } );
</script>
@endsection
