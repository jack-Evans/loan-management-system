@extends('layouts.app')

@section('title', 'Calculations')

@section('wrapper1')

@if(isset($loans))
<div class="main-wrapper">
  <div class="container">
    <div class="row">
        <div class="col-lg-12">
          <div class="has-bg">
              <h5 class="panel-title">
                  All Loans
              </h5>
              <table class="table table-striped table-hover table-bordered no-mar-b text-left">
                  <thead>
                      <tr>
                          <th>Loan Id</th>
                          <th>Issue Date</th>
                          <th>Net Loan</th>
                          <th>Gross Loan</th>
                          <th>Loan Type</th>
                          <th>Holiday auto refund</th>
                          <!--<th>Balance</th>-->
                          <th></th>
                      </tr>
                  </thead>
                  <tbody>
                      @foreach($loans as $key => $loan)
                      <tr>
                          <td>{{$loan->loanid}}</td>
                          <td>{{\Carbon\Carbon::parse($loan->issueat->date)->format('M d Y')}}</td>
                          <td>{{(float)$loan->netloan}}</td>
                          <td>{{(float)$loan->interest->gross_loan}}</td>
                          <td class="text-capitalize">{{$loan->loantype}}</td>
                          <td>{{ ($loan->holidayrefund == 1) ? 'Yes': 'No'}}</td>
                          <!--<td>{{ $loan->interest->gross_loan }}</td>-->
                          <td>
                              <a href="{{url('calculations/deleteloan/'.$loan->id)}}" class="btn btn-danger ml-1 float-right" >
                                      Delete
                              </a>
                              <!-- <button type="button" class="btn btn-primary ml-1 float-right loanModal" data-toggle="modal"
                                      data-target="#updateLoanModal" data-loanid="{{$loan->id}}" data-netloan="{{$loan->netloan}}" data-rate="{{$loan->interest->rate}}" data-defaultrate="{{$loan->interest->default_rate}}" data-duration="{{$loan->interest->duration}}"
                                      data-holidayrefund="{{$loan->holidayrefund}}">
                                  Edit
                              </button> -->
                              <a href="{{url('calculations?loanid='.$loan->id)}}" class="btn btn-info ml-1 float-right" >
                                  View
                              </a>
                          </td>
                      </tr>
                      @endforeach
                  </tbody>
              </table>
          </div>
      </div>
  </div>
</div>
@endif

@endsection
@section('content')
<div class="col-lg-12 ">
  <div class="has-bg hide">
    <div class="has-box-shadow">
        <h5 class="panel-title">
            Create A New Loan
        </h5>
    <div>
      <!-- <h6>Create Loan</h6> -->
          <form action="{{ url('calculations/store' )}}" method="post">
              <div class="row">
                  <div class="col-3">
                      @csrf()
                      <div class="form-group">
                        <label for="loanid">Loan Id:</label>
                        <input type="text" class="form-control" name="loanid"
                              value="{{old('loanid')}}">
                      </div>
                  </div>
                  <div class="col-3">
                      <div class="form-group">
                        <label for="netloan">Net Loan:</label>
                        <input type="text" class="form-control" name="netloan"
                              value="{{old('netloan')}}">
                      </div>
                  </div>
                  <div class="col-3">
                      <div class="form-group">
                        <label for="rate">Rate:</label>
                        <input type="text" class="form-control" name="rate"
                              value="{{ old('rate')}}">
                      </div>
                  </div>
                  <div class="col-3">
                      <div class="form-group ">
                          <label for="defaultRate">Default Rate:</label>
                          <input type="text" class="form-control" name="defaultRate"
                                value="{{ old('defaultRate')}}">
                      </div>
                  </div>
                  <div class="col-3">
                      <div class="form-group">
                        <label for="duration">Duration:</label>
                        <select class="form-control" name="duration">
                          @for($i = 1; $i <= 24; $i++)
                            <option value="{{$i}}" {{ old('duration') == $i ? 'selected': '' }}>{{$i}}</option>
                          @endfor
                        </select>
                      </div>
                  </div>
                  <div class="col-3">
                      <div class="form-group">
                        <label for="minTerm">Minimum Term:</label>
                        <select class="form-control" name="minTerm">
                          @for($i = 0; $i < 24; $i++)
                            <option value="{{$i}}" {{ old('minTerm') == $i ? 'selected': '' }}>{{$i}}</option>
                          @endfor
                        </select>
                      </div>
                  </div>
                  <div class="col-3">
                      <div class="form-group">
                          <label for="issuedate">Issue Date:</label>
                          <input class="form-control datepicker" autocomplete="off" name="issuedate"
                                value="{{ old('issuedate')}}">
                      </div>
                  </div>
                  <div class="col-3">
                      <div class="form-group">
                        <label for="loanType">Loan Type:</label>
                        <select class="form-control" name="loanType">
                          <option value="serviced" {{ old('loanType') == "serviced" ? 'selected': '' }} >Serviced</option>
                          <option value="retained" {{ old('loanType') == "retained" ? 'selected': '' }} >Retained</option>
                        </select>
                      </div>
                  </div>
                  <div class="col-3">
                      <div class="form-group">
                          <label for="customerId">Customers:</label>
                          <select name="customerId" class="form-control">
                              @foreach($customers as $customer)
                                <option value="{{$customer->id}}" {{  old('customerId') == $customer->id ? 'selected': '' }} >{{ $customer->username }}</option>
                              @endforeach
                          </select>
                      </div>
                  </div>
                  <div class="col-3">
                      <div class="form-group">
                          <span class="radio-text">Holiday Refund:</span>
                          <label class="custom-radio">
                            <input class="" type="hidden" name="holidayRefund" value="0">
                            <input class="" type="checkbox" name="holidayRefund"
                              @if(old('holidayRefund') != null && old('holidayRefund') == 0)
                                ''
                              @else
                                checked = "checked"
                              @endif
                              value="1">
                            <span class="checkmark"></span>
                          </label>
                      </div>
                  </div>
                  <div class="col-3">
                      <div class="form-group">
                          <label for="description">Description:</label>
                          <textarea class="form-control" rows="2" name="description">{{ old('description')}}</textarea>
                      </div>
                  </div>
                  <div class="col-3 mt-3">
                      <button type="submit" class="btn pull-right btn-primary custom-padding mt-3">Create Loan</button>
                  </div>
              </div>
          </form>
        </div>
    </div>
  </div>
  <div class="has-bg">
  <button type="button" class="btn btn-primary ml-1 createnewLoanModal" data-toggle="modal" data-target="#createnewLoanModal">
    Create A New Loan
  </button>
  <button type="button" class="btn btn-primary ml-1 AllLoanModal" data-toggle="modal" data-target="#AllLoanModal">
    All Loan
  </button>
  </div>
</div>

<!-- <span class="space padding-10"></span> -->
@if($selectedLoan)
<div class="col-lg-12">
  <div class="has-bg">
    <div class="has-box-shadow">
      <div class="row mb-2">
        <div class="col-6">
          <h6 class="panel-title">
              Selected Loan
          </h6>
        </div>
        <div class="col-6">
          <button type="button" class="btn btn-info ml-1 float-right transferModal" data-toggle="modal"
              data-target="#transferModal">
              Transfer to other Loan
          </button>
          <button type="button" class="btn btn-primary ml-1 float-right updateLoanDurationModal" data-toggle="modal"
              data-target="#updateLoanDurationModal">
              Re-new Loan
          </button>

          <a class="btn btn-danger js-close-loan float-right" data-href="{{url('updateloanstatus?loanId='.session('loanid').'&status=closed')}}">Close loan</a>
        </div>
      </div>
      <div class="panel-body">
          <table class="table table-striped table-hover table-bordered text-left m-0">
              <thead>
                  <tr>
                      <th>Loan Id</th>
                      <th>Net Loan</th>
                      <th>Gross Loan</th>
                      <th>Loan Type</th>
                      <th>Rate</th>
                      <th>Default Rate</th>
                      <th>Issued At</th>
                      <th>Duration</th>
                      <th>Minimum Term</th>
                      <th>Holiday Auto Refund</th>
                      <th>Customer</th>
                  </tr>
              </thead>
              <tbody>
                  <tr>
                      <td>{{$selectedLoan->loanid}}</td>
                      <td>{{(float)$selectedLoan->netloan}}</td>
                      <td>{{(float)$selectedLoan->interest->gross_loan}}</td>
                      <td class="text-capitalize">{{$selectedLoan->loantype}}</td>
                      <td>{{(float)$selectedLoan->interest->rate}}</td>
                      <td>{{(float)$selectedLoan->interest->default_rate}}</td>
                      <td>{{\Carbon\Carbon::parse($selectedLoan->issueat->date)->format('M d Y')}}</td>
                      <td>{{$selectedLoan->interest->duration}}</td>
                      <td>{{(float)$selectedLoan->interest->min_term}}</td>
                      <td>{{($selectedLoan->holidayrefund == 1) ? 'Yes': 'No'}}</td>
                      <td>{{$selectedLoan->customer->username}}</td>
                  </tr>
              </tbody>
          </table>
      </div>
    </div>

    <div class="has-box-shadow mt-2">
      <h6 class="panel-title">
          Minimum Term Amount
      </h6>
      <div class="panel-body">
          <table class="table table-hover table-bordered text-left m-0">
              <thead>
                  <tr>
                      <th>Minimum Term</th>
                      <th>Monthly Payment</th>
                      <th>Amount Paid</th>
                      <th>Amount Pending</th>
                      <th>Total Minimum Term Amount</th>
                      <th></th>
                  </tr>
              </thead>
              @if($minTermBalance)
              <tbody>
                  <tr>
                      <td>{{(float)$minTermBalance->min_term}}</td>
                      <td>{{(float)$minTermBalance->monthly_payment}}</td>
                      <td>{{(float)$minTermBalance->amount_paid}}</td>
                      <td>{{(float)$minTermBalance->min_term_balance}}</td>
                      <td>{{(float)$minTermBalance->total_minterm_amount}}</td>
                      <td>
                        <button type="button" class="btn btn-primary custom-padding float-right minTermModal" data-toggle="modal" data-target="#minTermModal" data-loanid="{{session('loanid')}}" data-minterm="{{(float)$minTermBalance->min_term}}" data-mpmt="{{(float)$minTermBalance->monthly_payment}}" data-amount="{{(float)$minTermBalance->min_term_balance}}" data-amountpaid="{{(float)$minTermBalance->amount_paid}}">
                          Edit
                        </button>
                      </td>
                  </tr>
              </tbody>
              @endif
          </table>
      </div>
    </div>
    @if(sizeof(collect($statements)->whereNotIn('description', ['issue loan', 'charge', 'interest', 'refund', 'retained_interest'])) == 0)
    <div class="has-box-shadow mt-2">
      <div class="panel panel-default">
          <h6 class="panel-title">
              Initial Charges
          </h6>
      </div>
      @if($charges && (session('loanid') != null))
      <div class="panel-body">
          <table class="table table-striped table-hover table-bordered text-left m-0">
              <thead>
                  <tr>
                      <th>Name</th>
                      <th>Value</th>
                      <th>Charge Type</th>
                      <th></th>
                  </tr>
              </thead>
              <tbody>
                  @foreach($charges as $key => $charge)
                  <tr>
                      <td>{{$charge->name}}</td>
                      <td>{{(float)$charge->value}}</td>
                      <td>{{$charge->chargetype}}</td>
                      <td>
                          <button type="button" class="btn btn-primary float-right assignChargeModal" data-toggle="modal" data-target="#assignChargeModal" data-chargeid="{{$charge->id}}" data-loanid="{{session('loanid')}}" data-value="{{$charge->value}}">
                              Add Charge
                          </button>
                          <!-- <a class="btn btn-primary float-right" href="{{url('calculations/assigncharge?chargeId='.$charge->id.'&loanId='.session('loanid'))}}">Assign</a> -->
                      </td>
                  </tr>
                  @endforeach
              </tbody>
          </table>
      </div>
      @endif
    </div>
    <div class="has-box-shadow mt-2">
        <div class="panel panel-default">
            <h6 class="panel-title">
                Added Initial Charges
            </h6>
        </div>
        <div class="panel-body">
          <table class="table table-striped table-hover table-bordered text-left m-0">
            <thead>
              <tr>
                <th>Name</th>
                <th>Value</th>
                <th>Charge Type</th>
                <th></th>
              </tr>
            </thead>
            <tbody>
              @foreach($assignedCharges as $key => $charge)
              @php // dd($charge) @endphp
              <tr>
                  <td>{{$charge->name}}</td>
                  <td>{{(float)$charge->value}}</td>
                  <td>{{$charge->chargeType}}</td>
                  <td>
                    <button type="button" class="btn btn-default float-right refundModal" data-toggle="modal"
                      data-target="#refundModal"
                      data-chargeId="{{$charge->id}}"
                      data-value="{{$charge->value}}"
                      data-chargedate="{{\Carbon\Carbon::parse($selectedLoan->issueat->date)->format('Y-m-d')}}"
                      >
                      Refund
                    </button>
                  </td>
              </tr>
              @endforeach
            </tbody>
          </table>
        </div>
    </div>
    @endif
  </div>
</div>
<div class="col-lg-12">
  <div class="has-bg">
    <div class="has-box-shadow">
        <form action="{{url('calculations')}}" method="get">
            <div class="row">
                <div class="col-3">
                    <b class="text-info">Today made up date:</b>
                    <input class="form-control datepicker4" autocomplete="off" name="madeupdate" value="{{isset($_GET['madeupdate']) ? $_GET['madeupdate'] : ''}}">
                </div>
                <div class="col-1">
                    <button type="submit" class="btn btn-success custom-padding mt-4">Go</button>
                </div>
                <div class="col-2 align-center">
                  <div class="">
                    <b class="text-success">Current Balance:</b>
                    <p class="text-success m-0">{{$currentBalance > 0 ? $currentBalance : '' }}</p>
                    </div>
                </div>
            </div>
        </form>
    </div>
  </div>
</div>
@endif
<!-- Only if we have a selected loanid -->
@if(session('loanid') != null)
<!-- Charges collapsable -->
<div class="col-lg-12">
  <div class="has-bg">
    <div class="has-box-shadow">
        <div class="panel panel-default">
            <div class="panel-heading" data-target="#collapseCharge" data-toggle="collapse" data-parent="#accordion">
                <h5 class="panel-title">
                    Other Charges
                    <span class="fa fa-plus cursor"></span>
                </h5>
            </div>
            <div class="panel-collapse collapse in" id="collapseCharge">
                <div class="has-box-shadow">
                    <form action="{{ url('calculations/createothercharge' )}}" method="post">
                        <div class="row">
                            <div class="col-2">
                                @csrf()
                                <div class="form-group">
                                    <label for="chargedate">Date:</label>
                                    <input type="text" class="form-control datepicker6" autocomplete="off" name="chargedate"
                                          value="{{ old('chargedate')}}">
                                </div>
                            </div>
                            <div class="col-2">
                                @csrf()
                                <div class="form-group">
                                    <label for="name">Name:</label>
                                    <input type="text" class="form-control" name="name"
                                          value="{{ old('name')}}">
                                </div>
                            </div>
                            <div class="col-2">
                                <div class="form-group">
                                  <label for="value">Amount:</label>
                                  <input type="text" class="form-control" name="value"
                                        value="{{old('value')}}">
                                </div>
                            </div>
                            <div class="col-2 d-none">
                                <div class="form-group">
                                    <label for="chargetype">Charge Type:</label>
                                    <select class="form-control" name="chargetype">
                                        <option value="fixed" {{ (old('chargetype') == 'fixed') ? 'selected' : ''}}>Fixed</option>
                                        <option value="percentage" {{ (old('chargetype') == 'percentage') ? 'selected' : ''}}>Percentage</option>
                                    </select>
                                </div>
                            </div>
                            <div class="col-2 d-none">
                                <div class="form-group">
                                  <label for="amounttype">Amount Type:</label>
                                  <input type="text" class="form-control" name="amounttype"
                                        value="netloan">
                                </div>
                            </div>
                            <input type="hidden" class="form-control" name="loanid" value="{{ session('loanid') }}">
                            <div class="col-6 mt-3">
                                <button type="submit" class="btn pull-right btn-primary custom-padding mt-3">Submit</button>
                            </div>
                        </div>
                    </form>
                </div>
                <div class="has-box-shadow">
                    <div class="panel panel-default">
                        <h6 class="panel-title">
                            Other Charges
                        </h6>
                    </div>
                    <div class="panel-body">
                        <table class="table table-striped table-hover table-bordered text-left m-0">
                            <thead>
                                <tr>
                                    <th>Name</th>
                                    <th>Value</th>
                                    <th>Charge Type</th>
                                    <th class="border-right-n">Charge Date</th>
                                    <th class="border-n"></th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach($otherCharges as $key => $otherCharge)
                                <tr>
                                    <td>{{$otherCharge->name}}</td>
                                    <td>{{(float)$otherCharge->value}}</td>
                                    <td>{{$otherCharge->chargeType}}</td>
                                    <td>{{\Carbon\Carbon::parse($otherCharge->journal->tnx_date)->format('M d Y')}}</td>
                                    <td>
                                        @if($otherCharge->refunded)
                                            <p class="text-danger">Refunded</p>
                                        @else
                                            <button type="button" class="btn btn-default refundModal" data-toggle="modal"
                                                data-target="#refundModal"
                                                data-value="{{$otherCharge->value}}"
                                                data-chargedate="{{\Carbon\Carbon::parse($otherCharge->journal->tnx_date)->format('Y-m-d')}}" data-otherchargeid="{{$otherCharge->id}}"
                                                >
                                                Refund
                                            </button>
                                        @endif
                                    </td>
                                </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
  </div>
</div>
<!-- Payments collapsable -->
<div class="col-lg-12">
  <div class="has-bg">
    <div class="has-box-shadow">
        <div class="panel panel-default">
            <div class="panel-heading" data-target="#collapsePayment" data-toggle="collapse" data-parent="#accordion">
                <h5 class="panel-title">
                    Payments
                    <span class="fa fa-plus cursor"></span>
                </h5>
            </div>
            <div class="panel-collapse collapse in" id="collapsePayment">
                <div class="has-box-shadow">
                    <form action="{{ url('calculations/createpayment' )}}" method="post">
                        <div class="row">

                            <div class="col-3">
                                @csrf()
                                <div class="form-group">
                                    <label for="paidAt">Date:</label>
                                    <input type="text" class="form-control datepicker3" autocomplete="off" name="paidAt"
                                          value="{{ old('paidAt')}}">
                                </div>
                            </div>

                            <div class="col-2">
                                <div class="form-group">
                                  <label for="amount">Amount:</label>
                                  <input type="text" class="form-control" name="amount"
                                        value="{{old('amount')}}">
                                </div>
                            </div>

                            <div class="col-3">
                                <div class="form-group">
                                  <label for="description">Description:</label>
                                  <!-- <input type="text" class="form-control" name="description" value="{{old('description')}}"> -->
                                  <textarea class="form-control" rows="2" name="description">{{ old('description')}}</textarea>
                                </div>
                            </div>

                            <div class="col-2">
                                <div class="form-group">
                                  <!-- <label for="isCapitalReduction">Capital Reduction</label>
                                  <input type="hidden" name="isCapitalReduction" value="0">
                                  <input type="checkbox" class="" name="isCapitalReduction" value="1"> -->
                                    <!-- input type="checkbox" class="form-check-input" id="materialUnchecked"> -->
                                    <span class="radio-text">Capital Reduction:</span>
                                    <label class="custom-radio">
                                      <input type="hidden" name="isCapitalReduction" value="0">
                                      <input type="checkbox" class="" name="isCapitalReduction" value="1">
                                      <span class="checkmark"></span>
                                    </label>
                                </div>
                            </div>

                            <input type="hidden" class="form-control" name="loanId" value="{{ session('loanid') }}">

                            <div class="col-2 mt-3">
                                <button type="submit" class="btn pull-right custom-padding btn-primary mt-3">Submit</button>
                            </div>
                        </div>
                    </form>
                </div>

              <div class="has-box-shadow">
                    <h6 class="panel-title">
                        Paid Amounts
                    </h6>

                    <div class="panel-body">
                        <table class="table table-striped table-hover table-bordered text-left m-0">
                            <thead>
                                <tr>
                                    <th>Date</th>
                                    <th>Amount Paid</th>
                                    <th>Descriptions</th>
                                    <th class="border-right-n">Capital Reduction</th>
                                    <th class="border-n"></th>
                                    <th class="border-n"></th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach($payments as $key => $payment)
                                <tr>
                                    <td>{{\Carbon\Carbon::parse($payment->paid_at)->format('M d Y')}}</td>
                                    <td>{{(float)$payment->amount}}</td>
                                    <td>{{$payment->journal->comment}}</td>
                                    <td>{{($payment->is_capital_reduction == 1) ? 'Yes' : ''}}</td>
                                    <!-- <td>
                                        <button type="button" class="btn btn-default float-right paymentModal" data-toggle="modal"
                                                data-target="#updatePaymentModal" data-paymentid="{{$payment->id}}" data-amount="{{(float)$payment->amount}}"
                                                data-paymentdate="{{\Carbon\Carbon::parse($payment->paid_at)->format('Y-m-d')}}">
                                            Edit
                                        </button>
                                    </td>
                                    <td>
                                        <a href="{{url('calculations/deletepayment/'.$payment->id)}}" class="btn btn-danger">Ignore</a>
                                    </td> -->
                                </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>

                <div class="has-box-shadow mt-3">
                    <h6 class="panel-title">
                        Create Filler
                    </h6>
                    <form action="{{ url('calculations/createrefund' )}}" method="post">
                        <div class="row">

                            <div class="col-3">
                                @csrf()
                                <div class="form-group">
                                    <label for="refundAt">Date:</label>
                                    <input type="text" class="form-control datepicker8" autocomplete="off" name="refundAt"
                                          value="{{ old('refundAt')}}">
                                </div>
                            </div>

                            <div class="col-2">
                                <div class="form-group">
                                  <label for="amount">Amount:</label>
                                  <input type="text" class="form-control" name="amount"
                                        value="{{old('amount')}}">
                                </div>
                            </div>

                            <div class="col-3">
                                <div class="form-group">
                                  <label for="description">Description:</label>
                                  <!-- <input type="text" class="form-control" name="description" value="{{old('description')}}"> -->
                                  <textarea class="form-control" rows="2" name="description">{{ old('description')}}</textarea>
                                </div>
                            </div>

                            <div class="col-3">
                                <div class="form-group">
                                  <label for="assetType">Filler Type:</label>
                                  <select class="form-control" name="assetType">
                                    <option value="credit_filler">Credit</option>
                                    <option value="debit_filler">Debit</option>
                                  </select>
                                </div>
                            </div>

                            <input type="hidden" class="form-control" name="loanId" value="{{ session('loanid') }}">

                            <div class="col-1 mt-3">
                                <button type="submit" class="btn pull-right custom-padding btn-primary mt-3">Submit</button>
                            </div>
                        </div>
                    </form>
                </div>

                <div class="has-box-shadow">
                    <h6 class="panel-title">
                        Fillers
                    </h6>

                    <div class="panel-body">
                        <table class="table text-left m-0">
                            <thead>
                                <tr>
                                    <th>Date</th>
                                    <th>Filler Amount</th>
                                    <th>Descriptions</th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach($refunds as $refund)
                                <tr>
                                    <td>{{\Carbon\Carbon::parse($refund->refund_at)->format('M d Y')}}</td>
                                    <td>{{(float)$refund->amount}}</td>
                                    <td>{{$refund->journal->comment}}</td>
                                </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>

            </div>
        </div>
    </div>
  </div>
</div>
<!-- DefaultRateInterests collapsable -->
<div class="col-lg-12">
  <div class="has-bg">
    <div class="has-box-shadow">
        <div class="panel panel-default">
            <div class="panel-heading" data-target="#collapseDefaultRateInterests" data-toggle="collapse" data-parent="#accordion">
                <h5 class="panel-title">
                    Default Rate Interests
                    <span class="fa fa-plus cursor"></span>
                </h5>
            </div>
            <div class="panel-collapse collapse in" id="collapseDefaultRateInterests">
                <div class="has-box-shadow">
                    <div class="panel-body">
                        <table class="table table-striped table-hover table-bordered text-left m-0">
                            <thead>
                                <tr>
                                    <th>Date</th>
                                    <!-- <th>End Date</th> -->
                                    <th>Days</th>
                                    <th>Rate</th>
                                    <th>Daily Rate</th>
                                    <th>Total</th>
                                    <th>Balance</th>
                                    <th></th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach($defaultRateInterestBreakdowns as $key => $breakdown)
                                <tr>
                                    <td>{{\Carbon\Carbon::parse($breakdown->start->date)->format('M d Y')}}</td>
                                    <td>{{(float)$breakdown->days}}</td>
                                    <td>{{(float)$breakdown->rate}}</td>
                                    <td>{{(float)$breakdown->daily_rate}}</td>
                                    <td>{{(float)$breakdown->total}}</td>
                                    <td>{{(float)$breakdown->balance}}</td>
                                    <td>
                                        @if($breakdown->refunded)
                                            <p class="text-danger">Refunded</p>
                                        @else
                                            <form action="{{'calculations/createdefaultraterefund'}}" method="post">
                                                @csrf()

                                                <input type="hidden" class="form-control" name="loanId" value="{{session('loanid')}}" readonly>
                                                <input type="hidden" name="startDate" value="{{\Carbon\Carbon::parse($breakdown->start->date)->format('Y-m-d')}}" readonly/>
                                                <input type="hidden" name="endDate" value="{{\Carbon\Carbon::parse($breakdown->end->date)->format('Y-m-d')}}" readonly/>
                                                <input type="hidden" name="days" value="{{$breakdown->days}}" readonly />
                                                <input type="hidden" name="balance" value="{{$breakdown->balance}}" readonly />
                                                <input type="hidden" name="description" value="" />
                                                <!-- <input type="hidden" name="refundDate" value="" readonly/> -->
                                                <button type="submit" class="btn btn-default">Refund</button>
                                            </form>
                                            @if(0)
                                            <!-- wrong if to skip this for time being. -->
                                            <button type="button" class="btn btn-default defaultRateRefundModal"    data-toggle="modal"
                                                data-target="#defaultRateRefundModal"
                                                data-startdate="{{\Carbon\Carbon::parse($breakdown->start->date)->format('Y-m-d')}}"
                                                data-enddate="{{\Carbon\Carbon::parse($breakdown->end->date)->format('Y-m-d')}}"
                                                data-days="{{$breakdown->days}}" data-balance="{{$breakdown->balance}}"
                                                >
                                                Refund
                                            </button>
                                            @endif
                                        @endif
                                    </td>
                                </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
  </div>
</div>
<div class="col-lg-12">
  <div class="has-bg">
    <div class="has-box-shadow">
        <h5 class="panel-title">
            Transactions
        </h5>

        <div class="panel-body">
            <table class="table table-striped table-hover table-bordered text-left m-0">
                <thead>
                    <tr>
                        <th>Date</th>
                        <th>Debit</th>
                        <th>Credit</th>
                        <th>Balance</th>
                        <th></th>
                        <th>Descriptions</th>
                        <th></th>
                    </tr>
                </thead>
                <tbody>
                    @foreach($statements as $key => $statement)
                    <tr>
                        <td>{{\Carbon\Carbon::parse($statement->created_at->date)->format('M d Y')}}</td>
                        <td>{{$statement->debit}}</td>
                        <td>{{$statement->credit}}</td>
                        <td>{{$statement->balance}}</td>
                        <td></td>
                        <td class="text-capitalize">{{$statement->comments}}
                            @if($statement->interestBreakDown)
                            <div class="target-description" data-key="{{$key}}">
                              <span class="text-primary fa fa-plus cursor"></span>
                              <!-- <span class="text-primary fa fa-minus"></span> -->
                            </div>
                            @endif
                        </td>
                        <td>
                            <span class="label text-capitalize
                                @if(($statement->description == 'payment' || $statement->description == 'refund') && $statement->credit > 0)
                                    bg-success
                                @elseif($statement->description == 'other_charge_refund' || $statement->description == 'othercharge')
                                    bg-primary
                                @elseif($statement->description == 'ignore')
                                    bg-warning
                                @elseif($statement->description == 'interest')
                                    bg-info
                                @elseif($statement->description == 'capreduction')
                                    bg-danger
                                @endif
                            ">{{$statement->explanation}}</span>
                        </td>
                    </tr>
                        @if($statement->interestBreakDown)
                        <tr class="show-description{{$key}} font-weight-bold font-italic"  style="display: none; background-color: #efefef; font-size: 12px;">
                            <td>From</td>
                            <td>To</td>
                            <td>Days</td>
                            <td>Amount</td>
                            <td>Rate</td>
                            <td>Daily Rate</td>
                            <td>Total</td>
                        </tr>
                        @foreach($statement->interestBreakDown as $breakDown)
                        <tr class="font-italic show-description{{$key}} @if($breakDown->rate == $selectedLoan->interest->default_rate && $breakDown->total >0) text-danger @elseif($breakDown->total <= 0) text-success @endif"  style="display: none; background-color: #efefef; font-size: 12px;">
                            <td>{{\Carbon\Carbon::parse($breakDown->start->date)->format('M d Y')}}</td>
                            <td>{{\Carbon\Carbon::parse($breakDown->end->date)->format('M d Y')}}</td>
                            <td>{{$breakDown->days}}</td>
                            <td>{{$breakDown->balance}}</td>
                            <td>{{(float)$breakDown->rate}}</td>
                            <td>{{(float)$breakDown->daily_rate}}</td>
                            <td>{{$breakDown->total}}</td>
                        </tr>
                        @endforeach
                        @endif
                    @endforeach
                </tbody>
            </table>
        </div>
    </div>
  </div>
</div>
<div class="col-lg-12">
  <div class="has-bg">
    <div class="has-box-shadow">
        <h5 class="panel-title">
            Payment Plan
            <span class="float-right">
                @if($selectedLoan && $selectedLoan->status == 'approved')
                <button type="button" class="btn btn-warning mar-b-3 freezeLoanModal" data-toggle="modal" data-target="#freezeLoanModal">
                  Freeze Loan
                </button>
                @elseif($selectedLoan && $selectedLoan->status == 'freezed')
                <button type="button" class="btn btn-warning mar-b-3" data-toggle="modal" data-target="#unFreezeLoanModal">
                  Un-Freeze Loan
                </button>
                @endif
                <!-- <a href="{{url('updateloanstatus?loanId='.session('loanid').'&status=freezed')}}"
                data-text="The loan will be freezed and you won't undo this action!"
                data-confirm-btn-txt="Yes, Freeze loan!"
                data-success-txt="Your freeze loan request is processing.!"
                >
                    Freeze Loan
                </a> -->

                <!-- <a class="btn btn-danger mar-b-3" href="{{url('updateloanstatus?loanId='.session('loanid').'&status=closed')}}"
                data-text="The loan will be closed and you won't undo this action!"
                data-confirm-btn-txt="Yes, Close loan!"
                data-success-txt="Your close loan request is processing.!"
                >
                    Close Loan
                </a> -->
                <!-- <span class="btn btn-danger mar-b-3">Closed</span> -->
                <button type="button" class="btn btn-info mar-b-3" data-toggle="modal" data-target="#closeLoanModal">
                  Close Loan Calculator
                </button>

                <!-- <a class="btn btn-danger mar-b-3" href="{{url('updateloanstatus?loanId='.session('loanid').'&status=closed')}}"
                data-text="The loan will be closed and you won't undo this action!"
                data-confirm-btn-txt="Yes, Close loan!"
                data-success-txt="Your close loan request is processing.!"
                >
                  Close Loan
                </a> -->

                <button type="button" class="btn mar-b-3 btn-primary createPaymentPlanModal" data-toggle="modal" data-target="#createPaymentPlanModal" data-loanid="{{session('loanid')}}">
                    Create Payment Plan
                </button>

                <a class="btn mar-b-3 btn-danger" href="{{url('deletepaymentplan')}}">
                    Delete Last Payment Plan
                </a>
            </span>
        </h5>

        <div class="panel-body">
            <table class="table table-striped table-hover table-bordered text-left m-0">
                <thead>
                    <tr>
                        <th>Sr. No</th>
                        <th>Versions</th>
                        <th>Amount Due</th>
                        <th>Amount Paid</th>
                        <th>Amount Pending</th>
                        <th>Expected Balance</th>
                        <th>Amount Type</th>
                        <th>Status</th>
                        <th>Due Date</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach($paymentPlan as $key => $payment)
                    <tr>
                        <td>{{$key+1}}</td>
                        <td>{{$payment->plan_version}}</td>
                        <td>{{(float)$payment->amount}}</td>
                        <td>{{(float)$payment->amount_paid}}</td>
                        <td>{{(float)$payment->amount_pending}}</td>
                        <td>{{(float)$payment->expected_balance}}</td>
                        <td class="text-capitalize">{{$payment->type}}</td>
                        <td class="text-capitalize">{{$payment->status}}</td>
                        <td>{{\Carbon\Carbon::parse($payment->pay_before->date)->format('M d Y')}}
                            @if($payment->payment_break_down)
                            <div class="target-description" data-key="{{$key}}pmtplan">
                              <span class="text-primary fa fa-plus cursor"></span>
                            </div>
                            @endif
                        </td>
                    </tr>
                        @if($payment->payment_break_down)
                            <tr class="show-description{{$key}}pmtplan font-weight-bold" style="display: none; background-color: #efefef;">
                                <td>Date</td>
                                <td>Amount</td>
                                <td>Description</td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                            </tr>
                            @foreach($payment->payment_break_down as $keybrkdn => $pmtBreakDown)
                            <tr class="show-description{{$key}}pmtplan"  style="display: none; background-color: #efefef;">
                                <td>{{ isset($pmtBreakDown->created_at) ? \Carbon\Carbon::parse($pmtBreakDown->created_at->date)->format('M d Y') : ''}}</td>
                                <td>{{(float)$pmtBreakDown->amount}}</td>
                                <td class="text-capitalize">{{$pmtBreakDown->description}}
                                    @if(isset($pmtBreakDown->sub_break_down))
                                    <div class="target-description" data-key="{{$keybrkdn}}subbrkdn">
                                      <span class="text-primary fa fa-plus cursor"></span>
                                    </div>
                                    @endif
                                </td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                            </tr>
                                @if(isset($pmtBreakDown->sub_break_down))
                                    <tr class="show-description{{$keybrkdn}}subbrkdn font-weight-bold"  style="display: none; background-color: #efefef;">
                                        <td>From</td>
                                        <td>To</td>
                                        <td>Days</td>
                                        <td>Amount</td>
                                        <td>Rate</td>
                                        <td>Daily Rate</td>
                                        <td>Total</td>
                                        <td></td>
                                        <td></td>
                                    </tr>
                                    @foreach($pmtBreakDown->sub_break_down as $pmtSubBreakDown)
                                        @foreach($pmtSubBreakDown as $subBreakDown)
                                        <tr class="show-description{{$keybrkdn}}subbrkdn"  style="display: none; background-color: #c7bdbd;">
                                            <td>{{\Carbon\Carbon::parse($subBreakDown->start->date)->format('M d Y')}}</td>
                                            <td>{{\Carbon\Carbon::parse($subBreakDown->end->date)->format('M d Y')}}</td>
                                            <td>{{$subBreakDown->days}}</td>
                                            <td>{{(float)$subBreakDown->balance}}</td>
                                            <td>{{(float)$subBreakDown->rate}}</td>
                                            <td>{{(float)$subBreakDown->daily_rate}}</td>
                                            <td>{{(float)$subBreakDown->total}}</td>
                                            <td></td>
                                            <td></td>
                                        </tr>
                                        @endforeach
                                    @endforeach
                                @endif
                            @endforeach
                        @endif
                    @endforeach
                </tbody>
            </table>
        </div>
    </div>
  </div>
</div>
<div class="col-lg-12">
  <div class="has-bg">
    <div class="has-box-shadow">
        <h5 class="panel-title">
            Generate statement
        </h5>
        <div class="panel-body">
            <form action="{{ url('calculations/lockmonth' )}}" method="post">
                <div class="row">
                    <div class="col-3">
                        @csrf()
                        <div class="form-group no-mar-b">
                            <!-- <label for="month"></label> -->
                            <select class="form-control" name="month">
                                <option value="">Select Statement Month</option>
                                @foreach($loanMonths as $month)
                                <option value="{{$month->statementDate}}"
                                    @if($month->statementGenerated)
                                        disabled
                                    @endif
                                    >@if($month->statementGenerated)
                                        Statement generated
                                    @else
                                        {{$month->statementDate}}
                                    @endif
                                    </option>
                                @endforeach
                            </select>
                        </div>
                    </div>
                    <input type="hidden" class="form-control" name="loanId" value="{{ session('loanid') }}">
                    <div class="col-9">
                        <button type="submit" class="btn pull-right custom-padding btn-primary">Submit</button>
                    </div>
                </div>
            </form>
        </div>
    </div>
  </div>
</div>

<div class="col-lg-12">
  <div class="has-bg">
    <div class="has-box-shadow">
        <div class="panel panel-default">
            <div class="panel-heading" data-target="#collapseClosingCharges" data-toggle="collapse" data-parent="#accordion">
                <h5 class="panel-title">
                    Closing Charges
                    <span class="fa fa-plus cursor"></span>
                </h5>
            </div>
            <div class="panel-collapse collapse in" id="collapseClosingCharges">
                <div class="has-box-shadow">
                   @if($closingCharges && (session('loanid') != null))
                    <div class="panel-body">
                        <table class="table table-striped table-hover table-bordered text-left m-0">
                            <thead>
                                <tr>
                                    <th>Name</th>
                                    <th>Value</th>
                                    <th>Charge Type</th>
                                    <th></th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach($closingCharges as $key => $charge)
                                <tr>
                                    <td>{{$charge->name}}</td>
                                    <td>{{(float)$charge->value}}</td>
                                    <td>{{$charge->charge_type}}</td>
                                    <td>
                                      <button type="button" class="btn btn-primary float-right assignClosingChargeModal" data-toggle="modal" data-target="#assignClosingChargeModal" data-chargeid="{{$charge->id}}" data-loanid="{{session('loanid')}}" data-value="{{(float)$charge->value}}">
                                      Add Charge
                                      </button>
                                    </td>
                                </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                    @endif
                </div>

                <div class="has-box-shadow">
                  <!-- <h6 class="panel-title">
                    Added Closing Charges
                  </h6> -->
                    <div class="panel-body">
                        <table class="table table-striped table-hover table-bordered text-left m-0">
                            <thead>
                                <tr>
                                    <th>Name</th>
                                    <th>Value</th>
                                    <th>Charge Type</th>
                                    <th></th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach($assignedCLCharges as $key => $charge)
                                <tr>
                                    <td>{{$charge->name}}</td>
                                    <td>{{(float)$charge->value}}</td>
                                    <td>{{$charge->charge_type}}</td>
                                    <td>
                                      <a class="btn btn-danger float-right" href="{{url('calculations/deleteclosingcharge?chargeId='.$charge->id.'&loanId='.session('loanid'))}}">Remove</a>

                                      <button type="button" class="btn btn-primary float-right mr-1 editClosingLoanChargeModal" data-toggle="modal" data-target="#editClosingLoanChargeModal" data-loanid="{{session('loanid')}}" data-chargeid="{{$charge->id}}" data-value="{{(float)$charge->value}}"
                                      >
                                      Edit
                                      </button>
                                    </td>
                                </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
  </div>
</div>

@if(isset($renewalcharges) && (session('loanid') != null))
<div class="col-lg-12">
  <div class="has-bg">
    <div class="has-box-shadow">
        <div class="panel panel-default">
            <div class="panel-heading" data-target="#collapseRenewalCharges" data-toggle="collapse" data-parent="#accordion">
                <h5 class="panel-title">
                    Renew Charges
                    <span class="fa fa-plus cursor"></span>
                </h5>
            </div>
            <div class="panel-collapse collapse in" id="collapseRenewalCharges">
                <div class="has-box-shadow">
                    <div class="panel-body">
                        <table class="table table-striped table-hover table-bordered text-left m-0">
                            <thead>
                                <tr>
                                    <th>Value</th>
                                    <th>Renew Start Date</th>
                                    <th>Renew End Date</th>
                                    <th>Duration</th>
                                    <th>Description</th>
                                    <th></th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach($renewalcharges as $key => $charge)
                                <tr>
                                    <td>{{(float)$charge->value}}</td>
                                    <td>{{\Carbon\Carbon::parse($charge->renew_date->date)->format('M d Y')}}</td>
                                    <td>{{\Carbon\Carbon::parse($charge->renew_end_date->date)->format('M d Y')}}</td>
                                    <td>{{$charge->duration}}</td>
                                    <td>{{$charge->description}}</td>
                                    <td>
                                    <!--<a class="btn btn-primary mar-b-3 pull-right editRenewalChargeModal" data-toggle="modal" data-target="#editRenewalChargeModal" data-rn-charge-id="{{$charge->id}}" data-value="{{(float)$charge->value}}" data-renew-date="{{\Carbon\Carbon::parse($charge->renew_date->date)->format('Y-m-d')}}" data-description = "{{$charge->description}}">
                                        Edit
                                      </a> -->
                                    </td>
                                </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
  </div>
</div>
@endif
<div class="col-lg-12">
  <div class="has-bg">
    <div class="has-box-shadow no-mar-b">
        <h5 class="panel-title">
            Borrower Statements
        </h5>

        <div class="panel-body row m-0">
        @foreach($monthStatements as $mStatement)
            <div class="col-3 p-1">
                <div class="card">
                <div class="card-body">
                    <h5 class="card-title">{{$mStatement->start_date}} - {{$mStatement->end_date}}</h5>
                    <table class="table table-striped table-hover table-bordered text-left m-0">
                        <tbody>
                            <tr>
                                <td>Opening Balance: {{$mStatement->opening_balance}}</td>
                            </tr>
                            <tr>
                                <td>Interest Charged: {{$mStatement->interest_charged}}</td>
                            </tr>
                            <tr>
                                <td>Current Interest Rate: {{(float)$mStatement->current_interest_rate}}%</td>
                            </tr>
                            <tr>
                                <td>Interest Due Date: {{$mStatement->interest_due_date}}</td>
                            </tr>
                            <tr>
                                <td>Closing Balance: {{(float)$mStatement->closing_balance}}</td>
                            </tr>
                            <tr>
                                <td>Loan End Date: {{$mStatement->loan_end_date}}</td>
                            </tr>
                        </tbody>
                    </table>
                </div>
                </div>
            </div>
        @endforeach
        </div>
    </div>
  </div>
</div>
@endif
<!-- End loan id check -->
<!-- All Loan Modal Start -->
  <div class="modal" id="AllLoanModal">
    <div class="modal-dialog" style="max-width: 800px">
    <div class="modal-content">
      <div class="modal-body custom_modal">
        <div class="row">
          <div class="col-lg-1">
            <div class="left-side">
            </div>
          </div>
          <div class="col-lg-11">
            <form>
            <h5 class="panel-title">
                All Loans
            </h5>
            <table class="table table-striped table-hover table-bordered no-mar-b text-left">
              <thead>
                <tr>
                  <th>Loan Id</th>
                  <th>Issue Date</th>
                  <th>Net Loan</th>
                  <th>Gross Loan</th>
                  <th>Loan Type</th>
                  <th>Holiday auto refund</th>
                  <!--<th>Balance</th>-->
                  <th></th>
                </tr>
              </thead>
              <tbody>
                @foreach($loans as $key => $loan)
                <tr>
                    <td>{{$loan->loanid}}</td>
                    <td>{{\Carbon\Carbon::parse($loan->issueat->date)->format('M d Y')}}</td>
                    <td>{{(float)$loan->netloan}}</td>
                    <td>{{(float)$loan->interest->gross_loan}}</td>
                    <td class="text-capitalize">{{$loan->loantype}}</td>
                    <td>{{ ($loan->holidayrefund == 1) ? 'Yes': 'No'}}</td>
                    <!--<td>{{ $loan->interest->gross_loan }}</td>-->
                    <td>
                        <a href="{{url('calculations/deleteloan/'.$loan->id)}}" class="btn btn-danger ml-1 float-right" >
                                Delete
                        </a>
                        <!-- <button type="button" class="btn btn-primary ml-1 float-right loanModal" data-toggle="modal"
                                data-target="#updateLoanModal" data-loanid="{{$loan->id}}" data-netloan="{{$loan->netloan}}" data-rate="{{$loan->interest->rate}}" data-defaultrate="{{$loan->interest->default_rate}}" data-duration="{{$loan->interest->duration}}"
                                data-holidayrefund="{{$loan->holidayrefund}}">
                            Edit
                        </button> -->
                        <a href="{{url('calculations?loanid='.$loan->id)}}" class="btn btn-info ml-1 float-right" >
                            View
                        </a>
                    </td>
                </tr>
                @endforeach
              </tbody>
            </table>
          </form>
        </div>
      </div>
    </div>
  </div>
</div>
<!-- All Loan Modal End -->

<!-- create ne loan modal -->
<div class="modal" id="createnewLoanModal">
  <div class="modal-dialog" style="max-width: 800px">
    <div class="modal-content">
      <div class="modal-body custom_modal">
        <div class="row">
          <div class="col-lg-1">
            <div class="left-side">
            </div>
          </div>
          <div class="col-lg-11">
            <form action="{{ url('calculations/store' )}}" method="post">
              <h4 class="modal-title">Create A New Loan <button type="button" class="close" data-dismiss="modal">&times;</button></h4>
                <div class="row">
                  <div class="col-6">
                    @csrf()
                    <div class="form-group">
                      <label for="loanid">Loan Id:</label>
                      <input type="text" class="form-control" name="loanid"
                            value="{{old('loanid')}}">
                    </div>
                  </div>
                  <div class="col-6">
                    <div class="form-group">
                      <label for="netloan">Net Loan:</label>
                      <input type="text" class="form-control" name="netloan"
                            value="{{old('netloan')}}">
                    </div>
                  </div>
                  <div class="col-6">
                    <div class="form-group">
                      <label for="rate">Rate:</label>
                      <input type="text" class="form-control" name="rate"
                            value="{{ old('rate')}}">
                    </div>
                  </div>
                  <div class="col-6">
                    <div class="form-group ">
                      <label for="defaultRate">Default Rate:</label>
                      <input type="text" class="form-control" name="defaultRate"
                            value="{{ old('defaultRate')}}">
                    </div>
                  </div>
                  <div class="col-4">
                    <div class="form-group">
                      <label for="duration">Duration:</label>
                      <select class="form-control" name="duration">
                        @for($i = 1; $i <= 24; $i++)
                          <option value="{{$i}}" {{ old('duration') == $i ? 'selected': '' }}>{{$i}}</option>
                        @endfor
                      </select>
                    </div>
                  </div>
                  <div class="col-4">
                    <div class="form-group">
                      <label for="minTerm">Minimum Term:</label>
                      <select class="form-control" name="minTerm">
                        @for($i = 0; $i < 24; $i++)
                          <option value="{{$i}}" {{ old('minTerm') == $i ? 'selected': '' }}>{{$i}}</option>
                        @endfor
                      </select>
                    </div>
                  </div>
                  <div class="col-4">
                    <div class="form-group">
                      <label for="issuedate">Issue Date:</label>
                      <input class="form-control datepicker" autocomplete="off" name="issuedate"
                            value="{{ old('issuedate')}}">
                    </div>
                  </div>
                  <div class="col-4">
                    <div class="form-group">
                      <label for="loanType">Loan Type:</label>
                      <select class="form-control" name="loanType">
                        <option value="serviced" {{ old('loanType') == "serviced" ? 'selected': '' }} >Serviced</option>
                        <option value="retained" {{ old('loanType') == "retained" ? 'selected': '' }} >Retained</option>
                      </select>
                    </div>
                  </div>
                  <div class="col-4">
                    <div class="form-group">
                      <label for="customerId">Customers:</label>
                      <select name="customerId" class="form-control">
                          @foreach($customers as $customer)
                            <option value="{{$customer->id}}" {{  old('customerId') == $customer->id ? 'selected': '' }} >{{ $customer->username }}</option>
                          @endforeach
                      </select>
                    </div>
                  </div>
                  <div class="col-4">
                    <div class="form-group">
                      <span class="radio-text">Holiday Refund:</span>
                      <label class="custom-radio">
                        <input class="" type="hidden" name="holidayRefund" value="0">
                        <input class="" type="checkbox" name="holidayRefund"
                          @if(old('holidayRefund') != null && old('holidayRefund') == 0)
                            ''
                          @else
                            checked = "checked"
                          @endif
                          value="1">
                        <span class="checkmark"></span>
                      </label>
                    </div>
                  </div>
                  <div class="col-12">
                    <div class="form-group">
                      <label for="description">Description:</label>
                      <textarea class="form-control" rows="2" name="description">{{ old('description')}}</textarea>
                    </div>
                  </div>
                  <div class="col-lg-12">
                    <button type="submit" class="btn pull-right btn-primary custom-padding mt-3">Create Loan</button>
                  </div>
                </div>
              </form>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
<!-- create new modal end -->
<!-- Update Loan Modal -->
<div class="modal" id="updateLoanModal">
  <div class="modal-dialog">
    <div class="modal-content">
      <!-- Modal Header -->
      <!-- <div class="modal-header">
        <h4 class="modal-title">Update Loan</h4>
        <button type="button" class="close" data-dismiss="modal">&times;</button>
      </div> -->
      <!-- Modal body -->
      <div class="modal-body custom_modal">
        <div class="row">
          <div class="col-lg-1">
            <div class="left-side">
            </div>
          </div>
          <div class="col-lg-11">
            <form action="{{'calculations/updateloan'}}" method="post">
              <h4 class="modal-title">Update Loan</h4>
              <button type="button" class="close" data-dismiss="modal">&times;</button>
                @csrf()
                <input type="hidden" class="form-control" name="loanId" value="{{old('loanId')}}">
                <div class="form-group">
                  <label for="netloan">Net Loan</label>
                  <input class="form-control" name="netloan" value="{{old('netloan')}}"/>
                </div>
                <div class="form-group">
                  <label for="rate">Rate</label>
                  <input class="form-control" name="rate" value="{{old('rate')}}"/>
                </div>
                <div class="form-group">
                  <label for="defaultRate">Default Rate</label>
                  <input class="form-control" name="defaultRate" value="{{old('defaultRate')}}"/>
                </div>
                <div class="form-group">
                  <label for="duration">Duration</label>
                  <select class="form-control" name="duration">
                    @for($i = 1; $i <= 24; $i++)
                      <option value="{{$i}}">{{$i}}</option>
                    @endfor
                  </select>
                  <input class="form-control" name="duration" value="{{old('duration')}}"/>
                </div>
                <div class="form-group">
                    <label for="holidayRefund">Holiday Refund</label>
                    <input type="hidden" name="holidayRefund" value="0">
                    <input type="checkbox" name="holidayRefund" value="1" />
                </div>
                <button type="submit" class="custom-padding pull-right btn btn-primary">Submit</button>
            </form>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>

<!-- Update Loan Duration Modal -->
<div class="modal" id="updateLoanDurationModal">
  <div class="modal-dialog" style="max-width: 700px;">
    <div class="modal-content">
      <div class="modal-body custom_modal">
        <div class="row">
        <div class="col-lg-1">
          <div class="left-side">
          </div>
        </div>
        <div class="col-lg-11">
          <form action="{{'calculations/renewloanduration'}}" method="post">
            <h4 class="modal-title">Renew Loan <button type="button" class="close" data-dismiss="modal">&times;</button></h4>
            <div class="row">
                @csrf()
                <input type="hidden" class="form-control" name="loanId" value="{{ session('loanid') }}">
                <div class="col-lg-4">
                  <div class="form-group">
                      <label for="duration">Renew Loan Duration</label>
                      <select class="form-control" name="duration">
                        @for($i = 1; $i <= 24; $i++)
                          <option value="{{$i}}">{{$i}}</option>
                        @endfor
                      </select>
                  </div>
                </div>
                <div class="col-lg-4">
                  <div class="form-group">
                    <label>Renew Date</label>
                    <input class="form-control datepicker15" autocomplete="off" name="renewDate" value="{{old('renewDate')}}"/>
                  </div>
                </div>
                <div class="col-lg-4">
                  <div class="form-group">
                    <label>Renew Charge Value</label>
                    <input type="text" class="form-control" name="value">
                  </div>
                </div>
                <div class="col-lg-12">
                  <div class="form-group">
                    <label>Description</label>
                    <textarea class="form-control" rows="1" name="description"></textarea>
                  </div>
                </div>
                <div class="col-lg-12">
                    <button type="submit" class="custom-padding  pull-right btn btn-primary">Submit</button>
                </div>
              </div>
            </form>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>

<!-- Transfer Modal -->
<div class="modal" id="transferModal">
  <div class="modal-dialog" style="max-width: 700px;">
    <div class="modal-content">
      <div class="modal-body custom_modal">
        <div class="row">
          <div class="col-lg-1">
            <div class="left-side">
            </div>
          </div>
          <div class="col-lg-11">
            <form action="{{'calculations/createtransfer'}}" method="post">
              <h4 class="modal-title">Transfer to other loan<button type="button" class="close" data-dismiss="modal">&times;</button></h4>
              <div class="row">
                @csrf()
                <input type="hidden" class="form-control" name="senderLoanId" value="{{ session('loanid') }}">
                <div class="col-lg-6">
                  <div class="form-group">
                    <label for="duration">Loans</label>
                    <select class="form-control" name="receiverLoanId">
                      @foreach($loans as $key => $loan)
                      <option value="{{$loan->id}}">{{$loan->loanid}}</option>
                      @endforeach
                    </select>
                  </div>
                </div>
                <div class="col-lg-6">
                  <div class="form-group">
                      <label for="duration">Amount</label>
                      <input class="form-control" type="number" name="amount" value="{{old('amount')}}">
                  </div>
                </div>
                <div class="col-lg-6">
                  <div class="form-group">
                      <label for="chargeValue">Charge Value</label>
                      <input class="form-control" type="number" name="chargeValue" value="{{old('chargeValue')}}">
                  </div>
                </div>
                <div class="col-lg-6">
                  <div class="form-group">
                    <label>Sender Transaction Date</label>
                    <input class="form-control datepicker16" autocomplete="off" name="senderTnxDate" value="{{old('senderTnxDate')}}"/>
                  </div>
                </div>
                <div class="col-lg-6">
                  <div class="form-group">
                    <label>Receiver Transaction Date</label>
                    <input class="form-control datepicker17" autocomplete="off" name="receiverTnxDate" value="{{old('receiverTnxDate')}}"/>
                  </div>
                </div>
                <div class="col-lg-6">
                  <div class="form-group">
                    <label>Description</label>
                    <textarea class="form-control" rows="1" name="description">{{old('description')}}</textarea>
                  </div>
                </div>
                <div class="col-lg-6">
                  <div class="form-group mb-5">
                    <span class="radio-text">Capital Reduction:</span>
                    <label class="custom-radio">
                      <input type="hidden" name="isCapitalReduction" value="0">
                      <input type="checkbox" class="" name="isCapitalReduction" value="1">
                      <span class="checkmark"></span>
                    </label>
                  </div>
                </div>
                <div class="col-lg-12">
                  <button type="submit" class="custom-padding pull-right btn btn-primary">Submit</button>
                </div>
              </div>
            </form>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>

<!-- Update Renewal Charge Modal -->
<div class="modal" id="editRenewalChargeModal">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-body custom_modal">
        <div class="row">
          <div class="col-lg-1">
            <div class="left-side">
            </div>
          </div>
          <div class="col-lg-11">
            <form action="{{'calculations/updaterenewalcharge'}}" method="post">
            <h4 class="modal-title">Edit Renewal Charge <button type="button" class="close" data-dismiss="modal">&times;</button></h4>
              @csrf()
              <div class="form-group">
                  <label for="RNDuration">Renew Loan Duration</label>
                  <select class="form-control" name="RNDuration">
                    @for($i = 1; $i <= 24; $i++)
                      <option value="{{$i}}">{{$i}}</option>
                    @endfor
                  </select>
              </div>
              <div class="form-group">
                <label>Renew Date</label>
                <input class="form-control datepicker16" autocomplete="off" name="renewDate"/>
              </div>
              <div class="form-group">
                <label>Renew Charge Value</label>
                <input type="text" class="form-control" name="value">
              </div>
              <div class="form-group">
                <label>Description</label>
                <textarea class="form-control" rows="1" name="description"></textarea>
              </div>
              <button type="submit" class="custom-padding btn pull-right btn-primary">Submit</button>
            </form>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>

<!-- Assign Charge Modal-->
<div class="modal" id="assignChargeModal">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-body custom_modal">
        <div class="row">
          <div class="col-lg-1">
            <div class="left-side">
            </div>
          </div>
          <div class="col-lg-11">
            <form action="{{'calculations/assigncharge'}}" method="get">
            <h4 class="modal-title">Add Initial Charge <button type="button" class="close" data-dismiss="modal">&times;</button></h4>
              <input type="hidden" class="form-control" name="loanId" value="{{old('loanId')}}">
              <input type="hidden" class="form-control" name="chargeId" value="{{old('chargeId')}}">
              <div class="form-group">
                  <label for="value">Value:</label>
                  <input class="form-control" name="value"/>
              </div>
              <div class="row">
                <div class="col-lg-12">
                  <button type="submit" class="custom-padding btn pull-right custom-padding btn-primary">Submit</button>
                </div>
              </div>
            </form>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>

<!-- Assign Closing Charge Modal-->
<div class="modal" id="assignClosingChargeModal">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-body custom_modal">
        <div class="row">
          <div class="col-lg-1">
            <div class="left-side">
            </div>
          </div>
          <div class="col-lg-11">
            <form action="{{'calculations/assignclosingcharge'}}" method="get">
              <h4 class="modal-title">Add Closing Charge <button type="button" class="close" data-dismiss="modal">&times;</button></h4>
                <input type="hidden" class="form-control" name="loanId" value="{{old('loanId')}}">
                <input type="hidden" class="form-control" name="chargeId" value="{{old('chargeId')}}">
                <div class="form-group">
                    <label for="value">Value:</label>
                    <input class="form-control" name="value"/>
                </div>
                <div class="row">
                  <div class="col-lg-12">
                    <button type="submit" class="custom-padding btn pull-right custom-padding btn-primary">Submit</button>
                  </div>
                </div>
            </form>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
<!-- Update Payment Modal -->
<div class="modal" id="updatePaymentModal">
  <div class="modal-dialog">
    <div class="modal-content">
      <!-- <div class="modal-header">
        <h4 class="modal-title">Edit Payment</h4>
        <button type="button" class="close" data-dismiss="modal">&times;</button>
      </div> -->
      <div class="modal-body custom_modal">
        <div class="row">
          <div class="col-lg-1">
            <div class="left-side">
            </div>
          </div>
          <div class="col-lg-11">
            <form action="{{'calculations/updatepayment'}}" method="post">
              <h4 class="modal-title">Edit Payment <button type="button" class="close" data-dismiss="modal">&times;</button></h4>
                @csrf()
                <input type="hidden" class="form-control" name="paymentId" value="{{old('paymentId')}}">
                <div class="form-group">
                  <label for="amount">Amount:</label>
                  <input class="form-control" name="amount" value="{{old('amount')}}"/>
                </div>
                <div class="form-group">
                  <label for="paidAt">Date:</label>
                  <input class="form-control datepicker5" autocomplete="off" name="paidAt" value="{{old('paidAt')}}"/>
                </div>
                <div class="row">
                  <div class="col-lg-12">
                    <button type="submit" class="custom-padding btn pull-right custom-padding btn-primary">Submit</button>
                  </div>
                </div>
            </form>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
<div class="modal" id="refundModal">
  <div class="modal-dialog">
    <div class="modal-content">
      <!-- <div class="modal-header">
        <h4 class="modal-title">Refund Charge</h4>
        <button type="button" class="close" data-dismiss="modal">&times;</button>
      </div> -->
      <div class="modal-body custom_modal">
        <div class="row">
          <div class="col-lg-1">
            <div class="left-side">
            </div>
          </div>
          <div class="col-lg-11">
            <form action="{{'calculations/createrefund'}}" method="post">
            <h4 class="modal-title">Refund Charge <button type="button" class="close" data-dismiss="modal">&times;</button></h4>
                @csrf()
                <input type="hidden" class="form-control" name="loanId" value="{{session('loanid')}}">
                <input type="hidden" class="form-control" name="chargeId">
                <input type="hidden" class="form-control" name="otherChargeId">
                <div class="form-group">
                  <label for="amount">Amount:</label>
                  <input class="form-control" name="amount"/>
                </div>
                <div class="form-group">
                  <label for="refundAt">Refund Date:</label>
                  <input class="form-control datepicker10" name="refundAt"/>
                </div>
                <div class="form-group">
                  <label for="description">Description:</label>
                  <!-- <input class="form-control" name="description"  /> -->
                  <textarea class="form-control" rows="2" name="description"></textarea>
                </div>
                <div class="row">
                  <div class="col-lg-12">
                    <button type="submit" class="custom-padding btn pull-right custom-padding btn-primary">Submit</button>
                  </div>
                </div>
            </form>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
<div class="modal" id="defaultRateRefundModal">
  <div class="modal-dialog">
    <div class="modal-content">
      <!-- <div class="modal-header">
        <h4 class="modal-title">Refund Default Rate Interest</h4>
        <button type="button" class="close" data-dismiss="modal">&times;</button>
      </div> -->
      <div class="modal-body custom_modal">
        <div class="row">
          <div class="col-lg-1">
            <div class="left-side">
            </div>
          </div>
          <div class="col-lg-11">
            <form action="{{'calculations/createdefaultraterefund'}}" method="post">
            <h4 class="modal-title">Refund Default Rate Interest <button type="button" class="close" data-dismiss="modal">&times;</button></h4>
                @csrf()
                <input type="hidden" class="form-control" name="loanId" value="{{session('loanid')}}">
                <div class="form-group">
                  <label for="startDate">Date:</label>
                  <input class="form-control" name="startDate" readonly />
                </div>
                <!-- <div class="form-group"> -->
                  <!-- <label for="endDate">End Date:</label> -->
                  <input type="hidden" class="form-control" name="endDate" readonly />
                <!-- </div> -->
                <div class="form-group">
                  <label for="refundDate">Refund Date:</label>
                  <input class="form-control datepicker11" autocomplete="off" name="refundDate" />
                </div>
                <div class="form-group">
                  <label for="days">Days:</label>
                  <input class="form-control" name="days" readonly />
                </div>
                <div class="form-group">
                  <label for="balance">Balance:</label>
                  <input class="form-control" name="balance" readonly />
                </div>
                <div class="form-group">
                  <label for="description">Description:</label>
                  <!-- <input class="form-control" name="description"  /> -->
                  <textarea class="form-control" rows="2" name="description"></textarea>
                </div>
                <div class="row">
                  <div class="col-lg-12">
                    <button type="submit" class="custom-padding btn pull-right custom-padding btn-primary">Submit</button>
                  </div>
                </div>
            </form>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
<!-- Update Other Charge Modal (not in use now) -->
<div class="modal" id="updateOtherChargeModal">
  <div class="modal-dialog">
    <div class="modal-content">
      <!-- <div class="modal-header">
        <h4 class="modal-title">Edit Other Charge</h4>
        <button type="button" class="close" data-dismiss="modal">&times;</button>
      </div> -->
      <div class="modal-body custom_modal">
        <div class="row">
          <div class="col-lg-1">
            <div class="left-side">
            </div>
          </div>
          <div class="col-lg-11">
            <form action="{{'calculations/updateothercharge'}}" method="post">
            <h4 class="modal-title">Edit Other Charge <button type="button" class="close" data-dismiss="modal">&times;</button></h4>
                @csrf()
                <input type="hidden" class="form-control" name="otherchargeid" value="{{old('otherchargeid')}}">
                <div class="form-group">
                  <label for="name">Name:</label>
                  <input class="form-control" name="name" value="{{old('name')}}"/>
                </div>
                <div class="form-group">
                  <label for="value">Amount:</label>
                  <input class="form-control" name="value" value="{{old('value')}}"/>
                </div>
                <select class="form-control d-none" name="chargetype">
                    <option value="fixed">Fixed</option>
                    <option value="percentage">Percentage</option>
                </select>
                <div class="form-group d-none">
                  <label for="amounttype">Amount Type:</label>
                  <input class="form-control" name="amounttype" value="{{old('amounttype')}}"/>
                </div>
                <div class="form-group">
                  <label for="chargedate">Charge Date:</label>
                  <input class="form-control datepicker7" autocomplete="off" name="chargedate" value="{{old('chargedate')}}"/>
                </div>
                <div class="row">
                  <div class="col-lg-12">
                    <button type="submit" class="custom-padding btn pull-right custom-padding btn-primary">Submit</button>
                  </div>
                </div>
            </form>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
<!-- Create Payment Plan Modal Modal -->
<div class="modal" id="createPaymentPlanModal">
  <div class="modal-dialog">
    <div class="modal-content">
      <!-- <div class="modal-header">
        <h4 class="modal-title">Create Payment Plan</h4>
        <button type="button" class="close" data-dismiss="modal">&times;</button>
      </div> -->
      <div class="modal-body custom_modal">
        <div class="row">
          <div class="col-lg-1">
            <div class="left-side">
            </div>
          </div>
          <div class="col-lg-11">
            <form action="{{'calculations/createpaymentplan'}}" method="get">
            <h4 class="modal-title">Create Payment Plan <button type="button" class="close" data-dismiss="modal">&times;</button></h4>
                <input type="hidden" class="form-control" name="loanId">
                <div class="form-group">
                  <label for="date">Payment Plan Date:</label>
                  <input class="form-control datepicker9" name="date" autocomplete="off" />
                </div>
                <div class="row">
                  <div class="col-lg-12">
                    <button type="submit" class="custom-padding btn pull-right custom-padding btn-primary">Submit</button>
                  </div>
                </div>
            </form>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>

<!-- Freeze Loan Modal -->
<div class="modal" id="freezeLoanModal">
  <div class="modal-dialog">
    <div class="modal-content">
      <!-- <div class="modal-header">
        <h4 class="modal-title">Freeze Loan</h4>
        <button type="button" class="close" data-dismiss="modal">&times;</button>
      </div> -->
      <div class="modal-body custom_modal">
        <div class="row">
          <div class="col-lg-1">
            <div class="left-side">
            </div>
          </div>
          <div class="col-lg-11">
            <form class="freezeForm" action="{{'updateloanstatus'}}" method="get"
              data-href="{{url('updateloanstatus?loanId='.session('loanid').'&status=freezed')}}">
              <h4 class="modal-title">Freeze Loan <button type="button" class="close" data-dismiss="modal">&times;</button></h4>
                <div class="form-group">
                  <label for="freezeDate">Freeze Date:</label>
                  <input class="form-control datepicker12 freeze-date" name="freezeDate" autocomplete="off" />
                </div>
                <div class="row">
                  <div class="col-lg-12">
                    <button type="submit" class="btn pull-right custom-padding btn-primary">Submit</button>
                  </div>
                </div>
            </form>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>

<!-- Un-Freeze Loan Modal -->
<div class="modal" id="unFreezeLoanModal">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <h4 class="modal-title">Un-Freeze Loan</h4>
        <button type="button" class="close" data-dismiss="modal">&times;</button>
      </div>
      <div class="modal-body custom_modal">
        <div class="row">
          <div class="col-lg-1">
            <div class="left-side">
            </div>
          </div>
          <div class="col-lg-11">
            <form class="unFreezeForm" action="{{'updateloanstatus'}}" method="get"data-href="{{url('updateloanstatus?loanId='.session('loanid').'&status=approved')}}">
            <h4 class="modal-title">Un-Freeze Loan  <button type="button" class="close" data-dismiss="modal">&times;</button></h4>
              <input type="hidden" class="form-control" name="loanId">
              <div class="form-group">
                <label for="freezeDate">Un-Freeze Date:</label>
                <input class="form-control datepicker13 un-freeze-date" name="freezeDate" autocomplete="off" />
              </div>
              <div class="row">
                <div class="col-lg-12">
                  <button type="submit" class="btn pull-right custom-padding btn-primary">Submit</button>
                </div>
              </div>
            </form>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>

<!-- Close Loan Modal -->
<div class="modal" id="closeLoanModal">
  <div class="modal-dialog" style="max-width: 700px;">
    <div class="modal-content">
      <!-- <div class="modal-header">
        <h4 class="modal-title">Close Loan Calculator</h4>
        <button type="button" class="close" data-dismiss="modal">&times;</button>
      </div> -->
      <div class="modal-body custom_modal">
        <div class="row">
          <div class="col-lg-1">
            <div class="left-side">
            </div>
          </div>
          <div class="col-lg-11">
            <form class="closeLoanModal" action="{{'updateloanstatus'}}" method="get"
              data-href="{{url('updateloanstatus?loanId='.session('loanid').'&status=closed')}}">
              <h4 class="modal-title">Close Loan Calculator <button type="button" class="close" data-dismiss="modal">&times;</button></h4>
              <input type="hidden" class="form-control" name="loanId">
              <div class="js-closing-balance">
                <p>Please pick a date for closing balance.</p>
              </div>
              <div class="row">
                <div class="col-6 form-group">
                  <label for="freezeDate">Closing Date:</label>
                  <input class="closeLoanCalculator form-control datepicker14 closing-date" name="freezeDate" id="CLFreezeDate" autocomplete="off" />
                </div>
                <div class="col-6 form-group">
                  <label for="virtualPmt">Virtual Payment:</label>
                  <select class="closeLoanCalculator form-control" name="virtualPmt" id="CLVirtualPmt">
                    <option value="false">Without Payment</option>
                    <option value="true">Including Payment</option>
                  </select>
                </div>
              </div>
              <div class="row">
                <div class="col-lg-12">
                  <button type="submit" class="btn custom-padding btn-primary pull-right js-closeloan-btn">Close loan</button>
                </div>
              </div>
            </form>
            <div class="js-final-breakdown">
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>

<!-- Edit Closing Loan Charge Modal -->
<div class="modal" id="editClosingLoanChargeModal">
  <div class="modal-dialog">
    <div class="modal-content">
      <!-- <div class="modal-header">
        <h4 class="modal-title">Edit Closing Loan Charge</h4>
        <button type="button" class="close" data-dismiss="modal">&times;</button>
      </div> -->
      <div class="modal-body custom_modal">
        <div class="row">
          <div class="col-lg-1">
            <div class="left-side">
            </div>
          </div>
          <div class="col-lg-11">
            <form class="closeLoanModal" action="{{'calculations/updateclosingloancharge'}}" method="post">
            <h4 class="modal-title">Edit Closing Loan Charge <button type="button" class="close" data-dismiss="modal">&times;</button></h4>
              @csrf()
              <input type="hidden" class="form-control" name="loanId">
              <input type="hidden" class="form-control" name="chargeId">
              <div class="form-group">
                <label for="value">Value:</label>
                <input class="form-control" type="number" name="value" id="clc-value" autocomplete="off" />
              </div>
              <div class="row">
                <div class="col-lg-12">
                  <button type="submit" class="btn pull-right custom-padding btn-primary">Submit</button>
                </div>
              </div>
            </form>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>

<!-- Edit Min Term Modal Modal -->
<div class="modal" id="minTermModal">
  <div class="modal-dialog" style="max-width: 700px">
    <div class="modal-content">
      <!-- <div class="modal-header">
        <h4 class="modal-title">Edit Min Term</h4>
        <button type="button" class="close" data-dismiss="modal">&times;</button>
      </div> -->
      <div class="modal-body custom_modal">
        <div class="row">
          <div class="col-lg-1">
            <div class="left-side">
            </div>
          </div>
          <div class="col-lg-11">
            <form action="{{'calculations/updateminterm'}}" method="post">
            <h4 class="modal-title">Edit Min Term <button type="button" class="close" data-dismiss="modal">&times;</button></h4>
              @csrf()

              <div class="row">
                <div class="col-6 form-group">
                  <label for="minTerm">Minimum Term:</label>
                  <input class="form-control" type="number" step="any" name="minTerm" id="mt-minTerm" autocomplete="off" />
                </div>

                <div class="col-6 form-group">
                  <label for="monthlyPmt">Monthly Payment:</label>
                  <input class="form-control" type="number" step="any" name="monthlyPmt" id="mt-monthlyPmt" autocomplete="off" />
                </div>

                <div class="col-6 form-group">
                  <label for="amount">Amount Pending:</label>
                  <input class="form-control" type="number" step="any" name="amount" id="mt-amount" autocomplete="off" />
                </div>

                <div class="col-6 form-group">
                  <label for="amountPaid">Amount Paid:</label>
                  <input class="form-control" type="number" step="any" name="amountPaid" id="mt-amountPaid" disabled="disabled" autocomplete="off" />
                </div>

                <div class="col-12">
                  <button type="submit" class="btn custom-padding pull-right btn-primary">Submit</button>
                </div>
              </div>
            </form>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
@endsection

@section('pages-js')
<script>
    $(document).ready(function(){
        $(document).on('click', '.target-description', function() {
            var key = $(this).data('key');
          $('.show-description'+key).toggle("slide");
          $(this).find("span.fa").toggleClass( "fa-minus" );
        });
    });

    $(document).on("click", ".loanModal", function () {
        // var loanId = $(this).data('loanid');
        $("input[name=loanId]").val($(this).data('loanid'));
        $("input[name=netloan]").val($(this).data('netloan'));
        $("input[name=rate]").val($(this).data('rate'));
        $("input[name=defaultRate]").val($(this).data('defaultrate'));
        $("input[name=duration]").val($(this).data('duration'));
        $('input[name=holidayRefund]').prop('checked', false);
        if($(this).data('holidayrefund')){
            $('input[name=holidayRefund]').prop('checked', true);
        }
    });

    $(document).on("click", ".assignChargeModal", function () {
        var loanId = $(this).data('loanid');
        var chargeId = $(this).data('chargeid');

        $("input[name=loanId]").val(loanId);
        $("input[name=chargeId]").val(chargeId);
        $("input[name=value]").val($(this).data('value'));
    });

    $(document).on("click", ".assignClosingChargeModal", function () {
        var loanId = $(this).data('loanid');
        var chargeId = $(this).data('chargeid');

        $("input[name=loanId]").val(loanId);
        $("input[name=chargeId]").val(chargeId);
        $("input[name=value]").val($(this).data('value'));
    });

    $(document).on("click", ".paymentModal", function () {
        var paymentId = $(this).data('paymentid');
        var amount = $(this).data('amount');
        var paymentDate = $(this).data('paymentdate');

        $("input[name=paymentId]").val(paymentId);
        $("input[name=amount]").val(amount);
        $("input[name=paidAt]").val(paymentDate);
    });

    $(document).on("click", ".otherChargeModal", function () {
        $("input[name=otherchargeid]").val($(this).data('otherchargeid'));
        $("input[name=name]").val($(this).data('name'));
        $("input[name=value]").val($(this).data('value'));
        $("[name=chargetype]").val($(this).data('chargetype'));
        $("input[name=amounttype]").val($(this).data('amounttype'));
        $("input[name=chargedate]").val($(this).data('chargedate'));
    });

    $(document).on("click", ".refundModal", function () {
        $("input[name=amount]").val($(this).data('value'));
        $("input[name=refundAt]").val($(this).data('chargedate'));
        $("input[name=chargeId]").val($(this).data('chargeid'));
        $("input[name=otherChargeId]").val($(this).data('otherchargeid'));
    });

    $(document).on("click", ".defaultRateRefundModal", function () {
        $("input[name=startDate]").val($(this).data('startdate'));
        $("input[name=endDate]").val($(this).data('enddate'));
        $("input[name=days]").val($(this).data('days'));
        $("input[name=balance]").val($(this).data('balance'));
    });

    $(document).on("click", ".createPaymentPlanModal", function () {
        $("input[name=loanId]").val($(this).data('loanid'));
    });

    $(document).on("click", ".editClosingLoanChargeModal", function () {
        $("input[name=loanId]").val($(this).data('loanid'));
        $("input[name=chargeId]").val($(this).data('chargeid'));
        $("input[name=value]").val($(this).data('value'));
    });

    $(document).on("click", ".minTermModal", function () {
        $("input[name=loanId]").val($(this).data('loanid'));
        $("input[name=minTerm]").val($(this).data('minterm'));
        $("input[name=monthlyPmt]").val($(this).data('mpmt'));
        $("input[name=amountPaid]").val($(this).data('amountpaid'));
        $("#mt-amount").val($(this).data('amount'));
    });

    $(document).ready(function(){
      $('.collapse').on('shown.bs.collapse', function(){
          $(this).parent().find(".fa-plus").removeClass("fa-plus").addClass("fa-minus");
      }).on('hidden.bs.collapse', function(){
          $(this).parent().find(".fa-minus").removeClass("fa-minus").addClass("fa-plus");
      });
    });
</script>
@endsection
