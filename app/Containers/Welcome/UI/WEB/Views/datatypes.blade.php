<!DOCTYPE html>
<html>
<head>
    <title>{{env('app_name')}} - @yield('title')</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="csrf-token" content="{{ csrf_token() }}" />
</head>
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
<!-- <link href="https://unpkg.com/gijgo@1.9.11/css/gijgo.min.css" rel="stylesheet" type="text/css" /> -->
<link href="{{url('assets/css').'/style.css'}}" rel="stylesheet" type="text/css" />
<link href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet" type="text/css" />
<link href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.11.2/css/all.css" rel="stylesheet" type="text/css" />
<!-- MDBootstrap Datatables  -->
<!-- <link href="{{url('assets/css').'/datatables.min.css'}}" rel="stylesheet"> -->
<!-- <link rel="stylesheet" href="{{url('assets/css').'/jquery.dropdown.css'}}"> -->
<!-- <link href="{{url('assets/css').'/multiselect.css'}}" rel="stylesheet"> -->
<!-- For jquery select box -->
<!-- <link href="{{url('assets/css').'/multi-select.css'}}" rel="stylesheet"> -->
<link rel="stylesheet" href="https://unpkg.com/ag-grid-community/dist/styles/ag-grid.css">
<link rel="stylesheet" href="https://unpkg.com/ag-grid-community/dist/styles/ag-theme-balham.css">
<link rel="stylesheet" href="https://unpkg.com/ag-grid-community/dist/styles/ag-theme-alpine.css">
<link rel="stylesheet" href="https://unpkg.com/ag-grid-community/dist/styles/ag-theme-material.css">
<!-- <link href="https://cdn.jsdelivr.net/npm/select2@4.0.13/dist/css/select2.min.css" rel="stylesheet" /> -->
    <link href="{{url('assets/css').'/ag-grid-header.css'}}" rel="stylesheet" type="text/css" />
<script>var __basePath = './';</script>
    <style media="only screen">
        html, body {
            height: 100%;
            width: 100%;
            margin: 0;
            box-sizing: border-box;
            -webkit-overflow-scrolling: touch;
        }

        html {
            position: absolute;
            top: 0;
            left: 0;
            padding: 0;
            overflow: auto;
        }

        body {
            padding: 1rem;
            overflow: auto;
        }
    </style>
<body>
<!-- <div class="col-lg-12"> -->
    <div class="has-bg">
    	<h3>Data Types of: {{$grid->name}}
            <div class="target-description">
                <span class="text-primary fas fa-chevron-up cursor"></span>
            </div>

            <a href="{{url('grids')}}" class="pull-right">
                <button class="btn btn-success">Back</button>
            </a>
        </h3>
        <span class="space"></span>
        <div class="show-description hidden">
        <span class="pull-right ml-1 mb-4">
            <span class="text-dark">Show Only Mandatory:</span>
            <label class="switch">
              <input class="js-mandatoryCheck" type="checkbox" value="{{(session('mandatoryCheck') == '1') ? '1' : '0' }}" {{(session('mandatoryCheck') == '1') ? 'checked' : '' }}>
              <span class="slider round"></span>
            </label>
        </span>
        <span class="pull-right ml-1 mb-4">
            <span class="text-dark">{{(session('editable') == '1') ? 'Disable' : 'Enable' }} Editing:</span>
            <label class="switch">
              <input class="js-editable" type="checkbox" value="{{ session('editable') }}" {{(session('editable') == '1') ? 'checked' : '' }}>
              <span class="slider round"></span>
            </label>
        </span>
        <!-- <div id="myGrid" style="height: 600px;width:100%;" class="ag-theme-alpine"></div> -->

        <div class="container">
            <div class="row">
                <div class="col-7">
                    <div class="columns">
                        <div class="column">
                            <label class="option">
                                <input type="checkbox" id="columnWidth">
                                columnWidth =
                                <select id="columnWidthValue">
                                    <option>100</option>
                                    <option>200</option>
                                    <option>myColumnWidthCallback</option>
                                </select>
                            </label>
                            <label class="option">
                                <input type="checkbox" id="sheetName">
                                sheetName =
                                <input type="text" id="sheetNameValue" value="custom-name" maxlength="31">
                            </label>
                            <label class="option">
                                <input type="checkbox" id="exportModeXml">
                                <span class="option-name">exportMode = "xml"</span>
                            </label>
                        </div>
                        <div class="column" style="margin-left: 30px">
                            <label class="option">
                                <input type="checkbox" id="suppressTextAsCDATA">
                                <span class="option-name">suppressTextAsCDATA</span>
                            </label>
                            <div class="option">
                                <label>
                                    <input type="checkbox" id="rowHeight">
                                    rowHeight =
                                </label>
                                <input type="text" id="rowHeightValue" value="30" style="width: 40px;">
                            </div>
                            <div class="option">
                                <label>
                                    <input type="checkbox" id="headerRowHeight">
                                    headerRowHeight =
                                </label>
                                <input type="text" id="headerRowHeightValue" value="40" style="width: 40px;">
                            </div>
                        </div>
                    </div>
                    <div style="margin: 5px;">
                        <label>
                            <button class="btn btn-primary" onclick="onBtExport()" style="margin: 5px; font-weight: bold;">Export to Excel</button>
                        </label>
                    </div>
                
                </div>
                <div class="col-5">
                    <h3>Search By Loan Tags</h3>
                    <h6>Select Tags:</h6>
                    <form id="main-form" class="navbar-form" action="{{url('datatypes')}}" method="get">
                        <div class="input_fields_wrap">
                          
                        </div>
                        
                        <div class="row mb-3 mt-2">
                            <div class="col-6">
                                <input type="hidden" name="gridId" value="{{$_GET['gridId']}}" >
                                <label>Save this search:
                                    <label class="custom-radio mb-2">
                                        <input class="" type="hidden" name="saveSearch" value="0">
                                        <input class="" type="checkbox" name="saveSearch" value="1">
                                        <span class="checkmark" style="top:-22px; left: 120px"></span>
                                    </label>
                                </label>
                            </div>
                            <div class="col-6">
                                <input class="btn btn-secondary mt-2 mb-2 pull-right" type="button" value="Add New Field" id="form-add">
                            </div>
                        </div>

                        <h6>OR Select Previous Search:</h6>
                        <select class="form-control" name="searchByQuery">
                            <option value="0">Select</option>
                            @foreach($searches as $search)
                                <option value="{{$search->id}}" {{(isset($_GET['searchByQuery']) && $_GET['searchByQuery'] == $search->id) ? 'selected':''}}>{{$search->search_query}}</option>
                            @endforeach
                        </select>
                        <input class="btn btn-primary js-search-submit mt-2 mb-1 pull-right" type="submit" value="Search">
                    </form>
                </div>
            </div>
        </div>
        </div>
        <!-- <h3 class="mr-1 mb-4">
            <span class="js-expand">
                <i class="fa fa-expand text-primary cursor pull-right" aria-hidden="true"></i>
            </span>
        </h3> -->
        <div class="grid-wrapper">
            <!-- <div id="myGrid" class="ag-theme-material" style="height: 100%;width:100%;"></div> -->
        </div>

    </div>
<!-- </div> -->
<div id="myGrid" class="ag-theme-material" style="height: 85%"></div>
<!-- <footer>
  <div class="copyright-section align-center" style="margin: 0px">
    <p>© {{\Carbon\Carbon::now()->format('Y')}} Kuflink All right Reserved </p>
  </div>
</footer> -->
<!-- <script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script> -->
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.11.2/js/all.js"></script>
<!-- <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script> -->
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>
<!-- <script src="https://unpkg.com/gijgo@1.9.11/js/gijgo.min.js" type="text/javascript"></script> -->
<!-- <script src="https://cdn.jsdelivr.net/npm/sweetalert2@8"></script> -->
<!-- <script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.18.1/moment.min.js"></script> -->
<!-- MDBootstrap Datatables  -->
<!-- <script type="text/javascript" src="{{url('assets/js').'/datatables.min.js'}}"></script> -->
<!-- <script src="{{url('assets/js').'/jquery.dropdown.js'}}"></script> -->
<!-- <script src="{{url('assets/js').'/multiselect.min.js'}}"></script> -->
<!-- <script src="https://unpkg.com/ag-grid-community/dist/ag-grid-community.min.js"></script> -->
<!-- <script src="https://unpkg.com/ag-grid-community/dist/ag-grid-community.min.noStyle.js"></script> -->
<script src="https://unpkg.com/ag-grid-enterprise/dist/ag-grid-enterprise.min.noStyle.js"></script>
<!-- <script src="https://cdn.ckeditor.com/ckeditor5/16.0.0/classic/ckeditor.js"></script> -->
<!-- <script src="https://cdn.jsdelivr.net/npm/select2@4.0.13/dist/js/select2.min.js"></script> -->
<!-- For jquery select box -->
<!-- <script src="{{url('assets/js').'/jquery.multi-select.js'}}"></script> -->
<script src="{{url('assets/js/search.js')}}" type="text/javascript" charset="utf-8" async defer></script>
<script src="{{url('assets/js/ag-grid/datatape-header.js')}}" type="text/javascript" charset="utf-8" ></script>

<script type="text/template" id="content-template">
<div class="repeat-container">
    <div class="form-field">
        <span class="row mt-1">
            <span class="col-5">
                <select class="form-control alarm_action removeDuplication" name='searchTags[]' required>
                    @foreach($tags as $tag)
                        <option value="{{$tag->id}}">{{$tag->name}}</option>
                    @endforeach
                </select>
            </span>
            <span class="col-5">
                <body>
                    <!-- <select name="searchtermorg" class="showForProg"></select> -->
                    <select class="form-control" name="operators[]">
                        <option value="and">AND</option>
                        <option value="or">OR</option>
                        <option value="not">NOT</option>
                    </select>
                    
                </body>
                <!-- <input class="filefield" type="file" name="foofile" style="display: none;"/>
                <textarea class="myTextarea" name="something" style="display: none;"></textarea> -->
            </span>
            <a href="#" class="remove_field mt-2">Remove</a>
        </span>
    </div>
</div>
</script>

<script>

    $(document).ready(function(){
        $(document).on('click', '.target-description', function() {
        $('.show-description').toggle("slide");
        $(this).find(".svg-inline--fa").toggleClass("fa-chevron-up fa-chevron-down" );
      });

        $('.js-expand').on('click', function(){
            $(this).find(".svg-inline--fa").toggleClass('fa-expand fa-compress');
            $('.main-wrapper').children('div').toggleClass('container container-fluid');
            $('header').toggleClass('hidden');
        });
    });

    agGrid.LicenseManager.setLicenseKey("{{env('AG_GRID_LICENCE_KEY')}}");


    // var editable = "{{session('editable')}}";
    // editable = (editable == 1) ? true : false;
    var editable = $('.js-editable').val();
    editable = (editable == 1) ? true : false;
    // console.log(editable, $('.js-editable').val());
    // $('.js-editable').val();
    $('.js-editable').on('change', function(){
        // console.log(editable);
        "@php session(['editable' => (session('editable') == 1) ? 0 : 1 ]) @endphp";
        // editable = $(this).val();
        // var newVal = (editable == true) ? false : true;
        // $(this).val(newVal);
        window.location.reload();
    });

    var columnDefs = [
            {field: 'loan id',editable: false, enableRowGroup: true}
        ];

    var columnDefs = [
        {
            headerName: 'Loan Details',
            headerGroupComponent: 'customHeaderGroupComponent',
            children: [
              { field: 'loan id' },
              { field: 'deal id_dt', columnGroupShow: 'open' },
              { field: 'id_dt', columnGroupShow: 'open', },
            ],
        },
        {
            headerName: 'Medal details',
            headerGroupComponent: 'customHeaderGroupComponent',
            children: [
                { headerName: 'Year', field: 'year' },
                // { headerName: 'Date', field: 'date', width: 110 },
                { headerName: 'Sport', field: 'sport', columnGroupShow: 'open', },
                // { headerName: 'Gold', field: 'gold', width: 100, columnGroupShow: 'open', },
                // { headerName: 'Silver', field: 'silver', width: 100, columnGroupShow: 'open', },
                // { headerName: 'Bronze', field: 'bronze', width: 100, columnGroupShow: 'open', },
                // { headerName: 'Total', field: 'total', width: 100, columnGroupShow: 'open', },
            ],
        },
    ];
    // console.log(columnDefs);
    var columnDefs = [];
    var mandatoryCheck = null;
    $('.js-mandatoryCheck').on('change', function(){
        mandatoryCheck = $('.js-mandatoryCheck').val();
        $.ajax({
            url: "{{url('grid-optional-variables').'/'.$_GET['gridId'].'?mandatoryCheck='}}"+mandatoryCheck,
            type: 'get',
            async: false,
            success: function(result){
                // console.log(result);
                window.location.reload();
            }, error: function(errors){
                // console.log(errors);
            }
        });
    });
    $.ajax({
        url: "{{url('grid-optional-variables').'/'.$_GET['gridId']}}",
        type: 'get',
        async: false,
        success: function(result){
            // var groups = [];
            $.map(result.data, function(item){
                if(item.group_name && item.group_name != 'Default'){
                    var found = $.map(columnDefs, function(def,key) {
                        // var index = def.headerName.indexOf(item.group_name);
                        // if (def.headerName == item.group_name) {
                            // console.log(def.headerName);
                        if (def.headerName.indexOf(item.group_name) >= 0) {
                            // console.log(columnDefs[key]);
                            // console.log(columnDefs[key].children.push(
                            //   { field: item.optionalVariable },
                            // ));
                            return key;
                        }
                    });
                    if(found.length > 0){
                        // console.log(found);
                        console.log(columnDefs[found[0]], found);
                            columnDefs[found[0]].children.push(
                              { field: item.optionalVariable, columnGroupShow: 'open' },
                            );
                    } else {
                        // console.log(item.group_name);
                        var newColumnDef = {
                            headerName: item.group_name,
                            headerGroupComponent: 'customHeaderGroupComponent',
                            children: [
                                {field: item.optionalVariable }
                            ],
                        }
                        // console.log(newColumnDef);
                        columnDefs.push(newColumnDef);
                    }
                    // columnDefs.push({
                    //     headerName: 'Group test',
                    //     headerGroupComponent: 'customHeaderGroupComponent',
                    //     children: [
                    //       { field: 'loan id', width: 150 },
                    //       { field: 'deal id_dt', width: 90, columnGroupShow: 'open' },
                    //       { field: 'id_dt', width: 120, columnGroupShow: 'open', },
                    //     ],
                    // });
                }
                else
                    columnDefs.push({headerName: item.optionalVariable, field: item.optionalVariable, enableRowGroup: true});
            });
            // console.log(groups);
            return columnDefs;
        }, error: function(errors){
            // console.log('columnDefs error');
        }
    });
    var gridOptions = {
        defaultColDef: {
            cellClassRules: {
                lightGreyBackground: function(params) {
                    return params.rowIndex % 2 == 0;
                }
            },
            sortable: true,
            editable: editable,
            filter: 'agSetColumnFilter',
            // rowDrag: true,
            filterParams: {
                applyMiniFilterWhileTyping: true,
            },
            flex: 1,
            minWidth: 200,
            resizable: true,
            floatingFilter: true,
            // filter: true,
            filter: 'agTextColumnFilter',
            autoGroupColumnDef: {
                minWidth: 250,
            },
            pivotMode: true,
        },
        
        suppressDragLeaveHidesColumns: true,
        suppressMakeColumnVisibleAfterUnGroup: true,
        rowGroupPanelShow: 'always',

        columnDefs: columnDefs,
        animateRows: true,
        rowSelection: 'multiple',
        sideBar: true,
        onCellValueChanged: function(e) {
            // console.log(e.value, e.data.loan_real_id, e.colDef.field);
          $.ajax({
            url: "{{url('calculations/createoption')}}",
            type: 'post',
            data: {loanId: e.data.loan_real_id, name: e.colDef.field, value: e.value, updateByLoanId: true},
            success: function(result){
                // console.log(result);
            }, error: function(errors){
                // console.log(errors);
            }
          });
        },
        components: {
            customHeaderGroupComponent: CustomHeaderGroup,
        },
        // onCellEditingStopped(e) {
        //     console.log(e.value, e.data.loan_real_id, e.colDef.field);
        // },
        // onSelectionChanged(){
        //     console.log('asdf');
        // },

        excelStyles: [
            {
                id: 'greenBackground',
                interior: {
                    color: '#b5e6b5',
                    pattern: 'Solid'
                }
            },
            {
                id: 'redFont',
                font: {
                    fontName: 'Calibri Light',
                    underline: 'Single',
                    italic: true,
                    color: '#ff0000'
                }
            },
            {
                id: 'lightGreyBackground',
                interior: {
                    color: '#e8ebec',
                    pattern: 'Solid'
                },
                font: {
                    fontName: 'Calibri Light',
                    // color: '#ffffff'
                }
            },
            {
                id: 'boldBorders',
                borders: {
                    borderBottom: {
                        color: '#000000',
                        lineStyle: 'Continuous',
                        weight: 3
                    },
                    borderLeft: {
                        color: '#000000',
                        lineStyle: 'Continuous',
                        weight: 3
                    },
                    borderRight: {
                        color: '#000000',
                        lineStyle: 'Continuous',
                        weight: 3
                    },
                    borderTop: {
                        color: '#000000',
                        lineStyle: 'Continuous',
                        weight: 3
                    }
                }
            },
            {
                id: 'header',
                interior: {
                    color: '#CCCCCC',
                    pattern: 'Solid'
                },
                borders: {
                    borderBottom: {
                        color: '#5687f5',
                        lineStyle: 'Continuous',
                        weight: 1
                    },
                    borderLeft: {
                        color: '#5687f5',
                        lineStyle: 'Continuous',
                        weight: 1
                    },
                    borderRight: {
                        color: '#5687f5',
                        lineStyle: 'Continuous',
                        weight: 1
                    },
                    borderTop: {
                        color: '#5687f5',
                        lineStyle: 'Continuous',
                        weight: 1
                    }
                }
            },
            {
                id: 'dateFormat',
                dataType: 'dateTime',
                numberFormat: {
                    format: 'mm/dd/yyyy;@'
                }
            },
            {
                id: 'twoDecimalPlaces',
                numberFormat: {
                    format: '#,##0.00'
                }
            },
            {
                id: 'textFormat',
                dataType: 'string'
            },
            {
                id: 'bigHeader',
                font: {
                    size: 25
                }
            }
        ]
    };
    // gridOptions.columnDefs = [
    //     { field: "loan", rowGroup: true },
    //     { field: "sport", rowGroup: true },
    // ];
    // console.log(gridOptions.defaultColDef.editable);

    function getBooleanValue(cssSelector) {
        return document.querySelector(cssSelector).checked === true;
    }

    function getTextValue(cssSelector) {
        return document.querySelector(cssSelector).value;
    }

    function getNumericValue(cssSelector) {
        var value = parseFloat(getTextValue(cssSelector));
        if (isNaN(value)) {
            var message = "Invalid number entered in " + cssSelector + " field";
            alert(message);
            throw new Error(message);
        }
        return value;
    }

    function myColumnWidthCallback(params) {
        var originalWidth = params.column.getActualWidth();
        if (params.index < 7) {
            return originalWidth;
        }
        return 30;
    }

    function onBtExport() {
        var columnWidth = getBooleanValue('#columnWidth') ? getTextValue('#columnWidthValue') : undefined;
        var params = {
            columnWidth: columnWidth === 'myColumnWidthCallback' ? myColumnWidthCallback : parseFloat(columnWidth),
            sheetName: getBooleanValue('#sheetName') && getTextValue('#sheetNameValue'),
            exportMode: getBooleanValue('#exportModeXml') ? "xml" : undefined,
            suppressTextAsCDATA: getBooleanValue('#suppressTextAsCDATA'),
            rowHeight: getBooleanValue('#rowHeight') ? getNumericValue('#rowHeightValue') : undefined,
            headerRowHeight: getBooleanValue('#headerRowHeight') ? getNumericValue('#headerRowHeightValue') : undefined,
        };

        gridOptions.api.exportDataAsExcel(params);
    }

    // setup the grid after the page has finished loading
    document.addEventListener('DOMContentLoaded', function() {
        var gridDiv = document.querySelector('#myGrid');
        new agGrid.Grid(gridDiv, gridOptions);

        $.ajax({
            url: "{{url('datatypeapi')}}",
            type: 'get',
            async: false,
            success: function(result){
            // agGrid.simpleHttpRequest({url: "{{url('datatype?limit=0')}}"}).then(function(data) {
                // console.log(result);
                // console.log(gridOptions);
                gridOptions.api.setRowData(result);
            }, error: function(errors){
                // console.log('error');
            }
        });
    });

</script>

</body>
</html>