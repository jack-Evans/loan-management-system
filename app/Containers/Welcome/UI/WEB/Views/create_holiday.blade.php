@extends('layouts.app')

@section('title', 'Holiday')

@section('content')
<div class="col-lg-12">
  <div class="has-bg">
    <div class="row">
        <div class="col-md-5 col-md-offset-2">
        <h3>{{isset($holiday->id) ? 'Edit ' : 'Create '}}Holiday</h3>
            <form action="{{url(isset($holiday->id) ? 'holidays/'.$holiday->id.'/update': 'holidays/store' )}}" method="post">
                @csrf()
                <div class="form-group">
                  <label for="name">Name:</label>
                  <input type="text" class="form-control" name="name"
                         value="{{isset($holiday->name) ? $holiday->name : old('name')}}">
                </div>
                <div class="form-group">
                  <label for="holidayDate">Holiday Date:</label>
                  <input type="text" class="form-control datepicker" autocomplete="off" name="holidayDate"
                         value="{{isset($holiday->holiday_date) ? $holiday->holiday_date : old('holidayDate')}}">
                </div>
                <button type="submit" class="btn btn-primary">Submit</button>
            </form>
        </div>
    </div>
  </div>
</div>
@endsection
