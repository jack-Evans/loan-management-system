<?php

namespace App\Containers\Calculator\Actions;

use App\Ship\Parents\Actions\Action;
use App\Ship\Parents\Requests\Request;
use Apiato\Core\Foundation\Facades\Apiato;

class UpdateCalculatorAction extends Action
{
    public function run(Request $request)
    {
        $data = $request->sanitizeInput([
            // add your request data here
        ]);

        $calculator = Apiato::call('Calculator@UpdateCalculatorTask', [$request->id, $data]);

        return $calculator;
    }
}
