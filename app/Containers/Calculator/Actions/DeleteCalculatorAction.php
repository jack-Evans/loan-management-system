<?php

namespace App\Containers\Calculator\Actions;

use App\Ship\Parents\Actions\Action;
use App\Ship\Parents\Requests\Request;
use Apiato\Core\Foundation\Facades\Apiato;

class DeleteCalculatorAction extends Action
{
    public function run(Request $request)
    {
        return Apiato::call('Calculator@DeleteCalculatorTask', [$request->id]);
    }
}
