<?php

namespace App\Containers\Calculator\Actions;

use App\Ship\Parents\Actions\Action;
use App\Ship\Parents\Requests\Request;
use Apiato\Core\Foundation\Facades\Apiato;

class FindCalculatorByIdAction extends Action
{
    public function run(Request $request)
    {
        $calculator = Apiato::call('Calculator@FindCalculatorByIdTask', [$request->id]);

        return $calculator;
    }
}
