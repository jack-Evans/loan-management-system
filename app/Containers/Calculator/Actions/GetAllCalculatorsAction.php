<?php

namespace App\Containers\Calculator\Actions;

use App\Ship\Parents\Actions\Action;
use App\Ship\Parents\Requests\Request;
use Apiato\Core\Foundation\Facades\Apiato;

class GetAllCalculatorsAction extends Action
{
    public function run(Request $request)
    {
        $calculators = Apiato::call('Calculator@GetAllCalculatorsTask', [], ['addRequestCriteria']);

        return $calculators;
    }
}
