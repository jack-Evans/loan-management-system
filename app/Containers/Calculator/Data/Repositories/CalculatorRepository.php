<?php

namespace App\Containers\Calculator\Data\Repositories;

use App\Ship\Parents\Repositories\Repository;

/**
 * Class CalculatorRepository
 */
class CalculatorRepository extends Repository
{

    /**
     * @var array
     */
    protected $fieldSearchable = [
        'id' => '=',
        // ...
    ];

}
