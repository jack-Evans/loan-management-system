<?php

namespace App\Containers\Calculator\Tasks;

use App\Containers\Calculator\Data\Repositories\CalculatorRepository;
use App\Ship\Exceptions\NotFoundException;
use App\Ship\Parents\Tasks\Task;
use Exception;

class FindCalculatorByIdTask extends Task
{

    protected $repository;

    public function __construct(CalculatorRepository $repository)
    {
        $this->repository = $repository;
    }

    public function run($id)
    {
        try {
            return $this->repository->find($id);
        }
        catch (Exception $exception) {
            throw new NotFoundException();
        }
    }
}
