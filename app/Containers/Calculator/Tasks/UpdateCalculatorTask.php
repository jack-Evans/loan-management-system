<?php

namespace App\Containers\Calculator\Tasks;

use App\Containers\Calculator\Data\Repositories\CalculatorRepository;
use App\Ship\Exceptions\UpdateResourceFailedException;
use App\Ship\Parents\Tasks\Task;
use Exception;

class UpdateCalculatorTask extends Task
{

    protected $repository;

    public function __construct(CalculatorRepository $repository)
    {
        $this->repository = $repository;
    }

    public function run($id, array $data)
    {
        try {
            return $this->repository->update($data, $id);
        }
        catch (Exception $exception) {
            throw new UpdateResourceFailedException();
        }
    }
}
