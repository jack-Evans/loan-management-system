<?php

namespace App\Containers\Calculator\Tasks;

use App\Containers\Calculator\Data\Repositories\CalculatorRepository;
use App\Ship\Parents\Tasks\Task;

class GetAllCalculatorsTask extends Task
{

    protected $repository;

    public function __construct(CalculatorRepository $repository)
    {
        $this->repository = $repository;
    }

    public function run()
    {
        return $this->repository->paginate();
    }
}
