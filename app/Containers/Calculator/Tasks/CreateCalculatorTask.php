<?php

namespace App\Containers\Calculator\Tasks;

use App\Containers\Calculator\Data\Repositories\CalculatorRepository;
use App\Ship\Exceptions\CreateResourceFailedException;
use App\Ship\Parents\Tasks\Task;
use Exception;

class CreateCalculatorTask extends Task
{

    protected $repository;

    public function __construct(CalculatorRepository $repository)
    {
        $this->repository = $repository;
    }

    public function run(array $data)
    {
        try {
            return $this->repository->create($data);
        }
        catch (Exception $exception) {
            throw new CreateResourceFailedException();
        }
    }
}
