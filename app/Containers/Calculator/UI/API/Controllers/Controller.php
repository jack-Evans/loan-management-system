<?php

namespace App\Containers\Calculator\UI\API\Controllers;

use App\Containers\Calculator\UI\API\Requests\CreateCalculatorRequest;
use App\Containers\Calculator\UI\API\Requests\DeleteCalculatorRequest;
use App\Containers\Calculator\UI\API\Requests\GetAllCalculatorsRequest;
use App\Containers\Calculator\UI\API\Requests\FindCalculatorByIdRequest;
use App\Containers\Calculator\UI\API\Requests\UpdateCalculatorRequest;
use App\Containers\Calculator\UI\API\Transformers\CalculatorTransformer;
use App\Ship\Parents\Controllers\ApiController;
use Apiato\Core\Foundation\Facades\Apiato;
use App\Containers\Loan\UI\API\Requests\CreateLoanRequest;
use App\Containers\Loan\UI\API\Transformers\LoanTransformer;
use App\Containers\Calculator\UI\API\Requests\CreateChargeRequest;
use App\Containers\Charge\UI\API\Transformers\ChargeTransformer;

/**
 * Class Controller
 *
 * @package App\Containers\Calculator\UI\API\Controllers
 */
class Controller extends ApiController
{
    /**
     * UPDATED METHOD
     * @param CreateLoanRequest $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function createLoan(CreateLoanRequest $request)
    {
        $loanCreateData = Apiato::call('Loan@CreateLoanAction', [$request]);
        
        $request->id = $loanCreateData->id;
        $loanIssueData = Apiato::call('Loan@IssueLoanAction', [$request]);
        
        return $this->transform($loanIssueData, LoanTransformer::class);
    }

    /**
     * @param FindCalculatorByIdRequest $request
     * @return array
     */
    public function findCalculatorById(FindCalculatorByIdRequest $request)
    {
        $calculator = Apiato::call('Calculator@FindCalculatorByIdAction', [$request]);

        return $this->transform($calculator, CalculatorTransformer::class);
    }

    /**
     * UPDATED METHOD
     * @param GetAllCalculatorsRequest $request
     * @return array
     */
    public function getAllLoans(GetAllCalculatorsRequest $request)
    {
        $loans = Apiato::call('Loan@GetAllLoansAction', [$request]);

        return $this->transform($loans, LoanTransformer::class);
    }

    /**
     * @param UpdateCalculatorRequest $request
     * @return array
     */
    public function updateCalculator(UpdateCalculatorRequest $request)
    {
        $calculator = Apiato::call('Calculator@UpdateCalculatorAction', [$request]);

        return $this->transform($calculator, CalculatorTransformer::class);
    }

    /**
     * @param DeleteCalculatorRequest $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function deleteCalculator(DeleteCalculatorRequest $request)
    {
        Apiato::call('Calculator@DeleteCalculatorAction', [$request]);

        return $this->noContent();
    }
    
    public function createCharge(CreateChargeRequest $request)
    {
        \DB::beginTransaction();

        $charge = Apiato::call('Charge@CreateChargeAction', [$request]);
        $request->chargeid = $charge->id;

        $loancharge = Apiato::call('LoanCharge@CreateLoanChargeAction', [$request]);
        
        \DB::commit();
        return $this->created($this->transform($charge, ChargeTransformer::class));
    }
}
