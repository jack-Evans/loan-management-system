<?php

/**
 * @apiGroup           Calculator
 * @apiName            createLoan
 *
 * @api                {POST} /v1/calculations Endpoint title here..
 * @apiDescription     Endpoint description here..
 *
 * @apiVersion         1.0.0
 * @apiPermission      none
 *
 * @apiParam           {String}  parameters here..
 *
 * @apiSuccessExample  {json}  Success-Response:
 * HTTP/1.1 200 OK
{
  // Insert the response of the request here...
}
 */

/** @var Route $router */
$router->post('calculations/loan/create', [
    'as' => 'api_calculator_create_loan',
    'uses'  => 'Controller@createLoan',
    'middleware' => [
      'auth:api'
    ],
]);
