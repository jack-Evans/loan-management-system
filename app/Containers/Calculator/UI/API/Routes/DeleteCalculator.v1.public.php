<?php

/**
 * @apiGroup           Calculator
 * @apiName            deleteCalculator
 *
 * @api                {DELETE} /v1/calculations/:id Endpoint title here..
 * @apiDescription     Endpoint description here..
 *
 * @apiVersion         1.0.0
 * @apiPermission      none
 *
 * @apiParam           {String}  parameters here..
 *
 * @apiSuccessExample  {json}  Success-Response:
 * HTTP/1.1 200 OK
{
  // Insert the response of the request here...
}
 */

/** @var Route $router */
$router->delete('calculations/{id}', [
    'as' => 'api_calculator_delete_calculator',
    'uses'  => 'Controller@deleteCalculator',
    'middleware' => [
      'auth:api',
    ],
]);
