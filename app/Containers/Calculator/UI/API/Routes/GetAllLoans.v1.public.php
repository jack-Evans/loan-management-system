<?php

/**
 * @apiGroup           Calculator
 * @apiName            getAllCalculators
 *
 * @api                {GET} /v1/calculations Endpoint title here..
 * @apiDescription     Endpoint description here..
 *
 * @apiVersion         1.0.0
 * @apiPermission      none
 *
 * @apiParam           {String}  parameters here..
 *
 * @apiSuccessExample  {json}  Success-Response:
 * HTTP/1.1 200 OK
{
  // Insert the response of the request here...
}
 */

/** @var Route $router */
$router->get('calculations/loans', [
    'as' => 'api_calculator_get_all_loans',
    'uses'  => 'Controller@getAllLoans',
    'middleware' => [
      'auth:api'
    ],
]);
