<?php

/**
 * @apiGroup           Calculator
 * @apiName            findCalculatorById
 *
 * @api                {GET} /v1/calculations/:id Endpoint title here..
 * @apiDescription     Endpoint description here..
 *
 * @apiVersion         1.0.0
 * @apiPermission      none
 *
 * @apiParam           {String}  parameters here..
 *
 * @apiSuccessExample  {json}  Success-Response:
 * HTTP/1.1 200 OK
{
  // Insert the response of the request here...
}
 */

/** @var Route $router */
$router->get('calculations/loan/{id}', [
    'as' => 'api_calculator_find_calculator_by_id',
    'uses'  => 'Controller@findCalculatorById',
    'middleware' => [
      'auth:api',
    ],
]);
