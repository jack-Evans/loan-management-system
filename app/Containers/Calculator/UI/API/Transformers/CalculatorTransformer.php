<?php

namespace App\Containers\Calculator\UI\API\Transformers;

use App\Containers\Calculator\Models\Calculator;
use App\Ship\Parents\Transformers\Transformer;

class CalculatorTransformer extends Transformer
{
    /**
     * @var  array
     */
    protected $defaultIncludes = [

    ];

    /**
     * @var  array
     */
    protected $availableIncludes = [

    ];

    /**
     * @param Calculator $entity
     *
     * @return array
     */
    public function transform(Calculator $entity)
    {
        $response = [
            'object' => 'Calculator',
            'id' => $entity->getHashedKey(),
            'created_at' => $entity->created_at,
            'updated_at' => $entity->updated_at,

        ];

        $response = $this->ifAdmin([
            'real_id'    => $entity->id,
            // 'deleted_at' => $entity->deleted_at,
        ], $response);

        return $response;
    }
}
