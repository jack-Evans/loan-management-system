<?php

namespace App\Containers\Calculator\UI\WEB\Controllers;

use App\Containers\Calculator\UI\WEB\Requests\CreateCalculatorRequest;
use App\Containers\Calculator\UI\WEB\Requests\DeleteCalculatorRequest;
use App\Containers\Calculator\UI\WEB\Requests\GetAllCalculatorsRequest;
use App\Containers\Calculator\UI\WEB\Requests\FindCalculatorByIdRequest;
use App\Containers\Calculator\UI\WEB\Requests\UpdateCalculatorRequest;
use App\Containers\Calculator\UI\WEB\Requests\StoreCalculatorRequest;
use App\Containers\Calculator\UI\WEB\Requests\EditCalculatorRequest;
use App\Ship\Parents\Controllers\WebController;
use Apiato\Core\Foundation\Facades\Apiato;
use Carbon\Carbon;
/**
 * Class Controller
 *
 * @package App\Containers\Calculator\UI\WEB\Controllers
 */
class Controller extends WebController
{
    
    /**
     * The assigned API PATH for this Controller
     *
     * @var string
     */
    
    protected $apiPath;
    
    public function __construct(){
        $this->apiPath = config('token-container.WEB_API_URL');
    }
    /**
     * Show all entities
     *
     * @param GetAllCalculatorsRequest $request
     */
    public function index(GetAllCalculatorsRequest $request)
    {
        if(isset($request->loanid))
            session(['loanid' => $request->loanid]);

        // session(['page' => isset($request->page) ? $request->page : ((session('page') != null) ? session('page') : null) ]);
// dd(session('page'), isset($request->page) ? $request->page : (session('page') != null) ? session('page') : null);
        $pagination = $loans = [];
        $charges = [];
        $closingCharges = [];
        $customers = [];
        $nominalCodes = [];
        $tags = [];
 
        // loans
        // $loansUrl = $this->apiPath.'loans';
        // try {
        //     // $response = get($loansUrl, ['page' => session('page')]);
        //     $response = get($loansUrl, ['limit' => 0]);
        //     $loans = $response->data;
        //     // $pagination = $response->meta->pagination;
        // } catch (\Exception $e) {
        //     return exception($e);
        // }

        // // customers
        // $customersUrl = $this->apiPath.'customers';
        // try {
        //     $response = get($customersUrl);
        //     $customers = $response->data;
        // } catch (\Exception $e) {
        //     return exception($e);
        // }

        // charges
        $chargesUrl = $this->apiPath.'charges';
        try {
            $chargesResponse = get($chargesUrl);
            $charges = $chargesResponse->data;
        } catch (\Exception $e) {
            return exception($e);
        }
        // charges
        $closingChargesUrl = $this->apiPath.'closingcharges';
        try {
            $closingChargesResponse = get($closingChargesUrl);
            $closingCharges = $closingChargesResponse->data;
        } catch (\Exception $e) {
            return exception($e);
        }
        
        $currentBalance     =  0;
        $payments           =  [];
        $selectedLoan       =  [];
        $assignedCharges    =  [];
        $otherCharges       =  [];
        $paymentPlan        =  [];
        $statements         =  [];
        $refunds            =  [];
        $loanMonths            =  [];
        $defaultRateInterestBreakdowns         =  [];
        $monthStatements         =  [];
        $assignedCLCharges         =  [];
        $renewalCharges         =  [];
        $minTermBalance         =  [];
        $loanOptions            =  [];
        
        if(session('loanid') != null)
        {
            $selectedLoanUrl = $this->apiPath.'loans/'.session('loanid');
            try{
                $response = get($selectedLoanUrl);
                $selectedLoan = $response->data;
            } catch (\Exception $e) {
                return exception($e);
            }

            $statementsUrl = $this->apiPath.'getallstatements/'.session('loanid')."?date=".$request->madeupdate;
            try{
                $response = get($statementsUrl);
                $statements = $response->data;
                // $currentBalance = end($statements)->balance;
            } catch (\Exception $e) {
                return exception($e);
            }
            
            $nominalCodesUrl = $this->apiPath.'nominalcodes';
            try{
                $response = get($nominalCodesUrl, ['limit' => 0]);
                $nominalCodes = $response->data;
            } catch (\Exception $e) {
                return exception($e);
            }

            $tagsUrl = $this->apiPath.'tags';
            try{
                $response = get($tagsUrl);
                $tags = $response->data;
            } catch (\Exception $e) {
                return exception($e);
            }

            $currentBalanceUrl = $this->apiPath.'currentbalance/'.session('loanid')."?date=".$request->madeupdate;
            try{
                $response = get($currentBalanceUrl);
                $currentBalance = $response->data->amount_pending;
            } catch (\Exception $e) {
                return exception($e);
            }
            
            $refundsUrl = $this->apiPath.'refunds/'.session('loanid');
            try{
                $response = get($refundsUrl);
                $refunds = $response->data;
            } catch (\Exception $e) {
                return exception($e);
            }

            $loanOptionsUrl = $this->apiPath.'loan-options/'.session('loanid');
            try{
                $response = get($loanOptionsUrl);
                $loanOptions = $response->data;
            } catch (\Exception $e) {
                return exception($e);
            }

            $monthStatementsUrl = $this->apiPath.'getmonthstatements/'.session('loanid');
            try{
                $response = get($monthStatementsUrl);
                $monthStatements = $response->data;
            } catch (\Exception $e) {
                return exception($e);
            }
            if( in_array($selectedLoan->loantype, ['serviced', 'rolled', 'retained', 'half']) )
            {
                $defaultRateInterestBreakdownsUrl = $this->apiPath.'getdefaultrateinterests/'.session('loanid')."?date=".$request->madeupdate;
                try{
                    $response = get($defaultRateInterestBreakdownsUrl);
                    $defaultRateInterestBreakdowns = $response->data;
                } catch (\Exception $e) {
                    return exception($e);
                }
                // payments
                $paymentsUrl = $this->apiPath.'payments?loanid='.session('loanid');
                try {
                    $response = get($paymentsUrl);
                    $payments = $response->data;
                } catch (\Exception $e) {
                    return exception($e);
                }        
                // loan charges
                $loanChargesUrl = $this->apiPath.'loancharges/'.session('loanid');
                try{
                    $response = get($loanChargesUrl);
                    $assignedCharges = $response->data;
                } catch (\Exception $e) {
                    return exception($e);
                }
                // assigned closing loan charges
                $closingLoanChargesUrl = $this->apiPath.'closingloancharge/'.session('loanid');
                try{
                    $response = get($closingLoanChargesUrl);
                    $assignedCLCharges = $response->data;
                } catch (\Exception $e) {
                    return exception($e);
                }

                $renewalChargesUrl = $this->apiPath.'renewalcharges/'.session('loanid');
                try{
                    $response = get($renewalChargesUrl);
                    $renewalCharges = $response->data;
                } catch (\Exception $e) {
                    return exception($e);
                }
                // other charges
                $otherChargesUrl = $this->apiPath.'othercharges?loanid='.session('loanid');
                try{
                    $response = get($otherChargesUrl);
                    $otherCharges = $response->data;
                } catch (\Exception $e) {
                    return exception($e);
                }

                $loanMonthsUrl = $this->apiPath.'loanmonths/'.session('loanid');
                try{
                    $loanMonths = get($loanMonthsUrl);
                } catch (\Exception $e) {
                    return exception($e);
                }

                $minTermBalanceUrl = $this->apiPath.'mintermbalance/'.session('loanid');
                try{
                    $response = get($minTermBalanceUrl);
                    $minTermBalance = $response->data;
                } catch (\Exception $e) {
                    return exception($e);
                }
            }

            if( in_array($selectedLoan->loantype, ['serviced', 'rolled', 'half']) )
            {
                $paymentPlanUrl = $this->apiPath.'getpaymentplan/'.session('loanid')."?date=".$request->madeupdate;
                try{
                    $response = get($paymentPlanUrl);
                    $paymentPlan = $response->data;
                } catch (\Exception $e) {
                    return exception($e);
                }
            }
            $mandatoryOptVariables = [];
            if($selectedLoan)
            {
                $mandatoryOptVariables['Name'] = $selectedLoan->loanid;
                $mandatoryOptVariables['Loan term'] = $selectedLoan->interest->duration." Months";
                $mandatoryOptVariables['Loan Start Date'] = formatedDate($selectedLoan->issueat->date);
                $mandatoryOptVariables['Rate'] = number_format($selectedLoan->interest->rate, 2)."%";
                $mandatoryOptVariables['Expiry Date'] = formatedDate(addDecimalMonthsIntoDate($selectedLoan->interest->duration, $selectedLoan->issueat->date)->subDay());
                $mandatoryOptVariables['Default rate'] = number_format($selectedLoan->interest->default_rate, 2)."%";
                $mandatoryOptVariables['Interest'] = $selectedLoan->loantype;
                $mandatoryOptVariables['Gross Loan'] = $selectedLoan->interest->gross_loan;
                $mandatoryOptVariables['Holiday Refund'] = ($selectedLoan->holidayrefund) ? 'Yes' : 'No';

                if($selectedLoan->loantype == 'half'){
                    $mandatoryOptVariables['Retained loan duration'] = $selectedLoan->interest->half_loan_duration." Months";
                    $mandatoryOptVariables['Serviced loan duration'] = ($selectedLoan->interest->duration - $selectedLoan->interest->half_loan_duration)." Months";
                }

                if($selectedLoan->renewalCharge)
                {
                    $v = 1;
                    foreach ($selectedLoan->renewalCharge as $key => $renewalCharge) {
                        $mandatoryOptVariables["Renewal Date $v"]   = formatedDate($renewalCharge->renew_date);
                        $mandatoryOptVariables["Renewal Term $v"]   = $renewalCharge->duration." Months";
                        $mandatoryOptVariables["Renewal Amount $v"] = $renewalCharge->value;
                        $mandatoryOptVariables["Renewal Expiry Date $v"] = formatedDate($renewalCharge->renew_end_date);

                        $v++;
                    }
                }
                
                // dd($mandatoryOptVariables['Interest']);
                
                // $mandatoryOptVariables['loan type']   = $selectedLoan->loantype;
                // $mandatoryOptVariables['Loan status'] = ($selectedLoan->status == 'closed') ? "SETTLED" : "OPEN";
                // $mandatoryOptVariables['holidayrefund'] = $selectedLoan->holidayrefund;
                // $mandatoryOptVariables['completion date']  = addDecimalMonthsIntoDate($selectedLoan->interest->duration, $selectedLoan->issueat->date)->subDay()->format('d/m/Y');
                // $mandatoryOptVariables['contractual term in months'] = $selectedLoan->interest->duration;
                // if(collect($selectedLoan->renewalCharge)->last()){
                //     $renewEndDate = collect($selectedLoan->renewalCharge)->last()->renew_end_date;
                //     $mandatoryOptVariables['agreed extended repayment date'] = \Carbon\Carbon::parse($renewEndDate)->format('d/m/Y');
                // }
                // $mandatoryOptVariables['number of extensions'] = count($selectedLoan->renewalCharge);

                // $mandatoryOptVariables['monthly interest rate'] = $selectedLoan->interest->rate;
                // $mandatoryOptVariables['original gross loan amount'] = number_format($selectedLoan->interest->gross_loan, 2);
                // $mandatoryOptVariables['original principal loan amount'] = number_format($selectedLoan->netloan, 2);
                // $mandatoryOptVariables['monthly payment due'] = number_format(calculateMonthlyPMT($selectedLoan->interest->rate, $selectedLoan->interest->gross_loan), 2);
            }

        }

        $verifyStatementDate = \App\Containers\Loan\Models\Extras::where('name', 'VerifyStatementDate')->first();

        return view('welcome::calculator_view', compact('loans', 'pagination', 'charges', 'customers', 'selectedLoan', 'payments', 
                'assignedCharges', 'otherCharges', 'paymentPlan', 'statements', 'currentBalance', 'refunds', 'defaultRateInterestBreakdowns', 'loanMonths', 'monthStatements', 'closingCharges', 'assignedCLCharges', 'minTermBalance', 'renewalCharges', 'nominalCodes', 'tags', 'loanOptions', 'mandatoryOptVariables', 'verifyStatementDate'));
    }

    /**
     * Show one entity
     *
     * @param FindCalculatorByIdRequest $request
     */
    public function show(FindCalculatorByIdRequest $request)
    {
        $calculator = Apiato::call('Calculator@FindCalculatorByIdAction', [$request]);

        // ..
    }

    /**
     * Create entity (show UI)
     *
     * @param CreateCalculatorRequest $request
     */
    public function create(CreateCalculatorRequest $request)
    {
        // ..
    }

    /**
     * Add a new entity
     *
     * @param StoreCalculatorRequest $request
     */
    public function store(StoreCalculatorRequest $request)
    {
        $url = $this->apiPath.'loans';
        
        $loanRequest = [
            'loanId'            => $request->loanid,
            'netloan'           => $request->netloan,
            'loanType'          => $request->loanType,
            'rate'              => $request->rate,
            'defaultRate'       => $request->defaultRate,
            'duration'          => $request->duration,
            'minTerm'           => $request->minTerm,
            'customerId'        => $request->customerId,
            'description'       => $request->description,
            'issueAt'           => $request->issuedate,
            'holidayRefund'     => $request->holidayRefund,
            'nominalCode'       => $request->nominalCode,
    	];

        if($loanRequest['loanType'] === 'retained' || $loanRequest['loanType'] === 'half'){
            $loanRequest['grossloan'] = ($request->grossloan) ? $request->grossloan : null;
            $loanRequest['chargeId'] = ($request->RLChargeId) ? $request->RLChargeId : null;
            $loanRequest['initialChargeValue'] = ($request->initialChargeValue) ? $request->initialChargeValue : null;
            $loanRequest['nominalCodeForCharges'] = ($request->nominalCodeForCharges) ? $request->nominalCodeForCharges : null;
            $loanRequest['duration2'] = ($request->duration2) ? $request->duration2 : 0;
        }
// dd($loanRequest);
        try {
            $response = post($url, $loanRequest);
        } catch (\Exception $e) {
            return exception($e);
        }

        return redirect('calculations?loanid='.$response->data->id);
    }

    /**
     * Update Loan
     *
     * @param UpdateCalculatorRequest $request
     */
    public function updateLoan(UpdateCalculatorRequest $request)
    {
        $updateLoanUrl = $this->apiPath.'loans/'.$request->loanId;
        
        $updateLoanRequest = [
            'name'              => $request->loanName,
            'netloan'           => $request->netloan,
            'type'              => $request->type,
            'loanType'          => $request->loanType,
            'rate'              => $request->rate,
            'defaultRate'       => $request->defaultRate,
            'duration'          => $request->duration,
            'halfLoanDuration'  => $request->halfLoanDuration,
            'holidayRefund'     => $request->holidayRefund,
        ];
        
        try {
            $response = patch($updateLoanUrl, $updateLoanRequest);
        } catch (\Exception $e) {
            return exception($e);
        }
        return redirect()->back();
    }

    /**
     * Update LoanStatus
     *
     * @param UpdateCalculatorRequest $request
     */
    public function updateLoanStatus(UpdateCalculatorRequest $request)
    {
        $updateloanstatusUrl = $this->apiPath.'updateloanstatus';
        $updateloanstatusRequest = [
            'loanId'                => $request->loanId,
            'status'                => $request->status,
            'updateDate'                => $request->freezeDate,
        ];
        
        try {
            $response = post($updateloanstatusUrl, $updateloanstatusRequest);
        } catch (\Exception $e) {
            return exception($e);
        }
        return redirect('calculations');
    }

    /**
     * Delete a given entity
     *
     * @param DeleteCalculatorRequest $request
     */
    public function deleteLoan(DeleteCalculatorRequest $request)
    {
        $deleteLoanUrl = $this->apiPath.'loans/'.$request->id;
        try {
            $response = delete($deleteLoanUrl);
        } catch (\Exception $e) {
            return exception($e);
        }
        session(['loanid' => null]);
        return redirect()->back();
    }
    /*
     * Add new payment
     * 
     * @param StoreCalculatorRequest $request
     */
    public function createPayment(StoreCalculatorRequest $request)
    {
        $paymentRequest = [
            'amount'        => $request->amount,
            'paidAt'        => $request->paidAt,
            'loanId'        => $request->loanId,
            'description'   => $request->description,
            'isCapitalReduction'   => $request->isCapitalReduction,
            'nominalCode' => $request->nominalCode,
        ];


        $url = $this->apiPath.'payments';
        try {
            $response = post($url, $paymentRequest);
        } catch (\Exception $e) {
            return exception($e);
        }    
        
        return redirect('calculations');
    }
    
        /*
     * Add new payment
     * 
     * @param StoreCalculatorRequest $request
     */
    
    public function updatePayment(StoreCalculatorRequest $request)
    {
        $chargeRequest = [
            'amount'            => $request->amount,
            'paidAt'              => $request->paidAt,
        ];

        $url = $this->apiPath.'payments/'.$request->paymentId;
        
        try {
            $response = patch($url, $chargeRequest);
        } catch (\Exception $e) {
            return exception($e);
        }    
        
        return redirect('calculations');
    }
    
    /*
     * Delete payment
     * 
     * @param DeleteCalculatorRequest $request
     */
    
    public function deletePayment(DeleteCalculatorRequest $request)
    {
        $url = $this->apiPath.'payments/'.$request->paymentId;
        
        try {
            $response = delete($url);
        } catch (\Exception $e) {
            return exception($e);
        }    
        
        return redirect('calculations');
    }
    
    public function createCharge(StoreCalculatorRequest $request)
    {
        $url = $this->apiPath.'charges';
            
        $data = [
            'name' => $request->name,
            'value' => $request->value,
            'chargetype' => $request->chargetype,
            'amounttype' => $request->amounttype,
        ];
        
        // try {
            $response = post($url, $data);
            $data['data'] = $response->data;
        // } catch(\Exception $e) {
            // return exception($e);
        // }

        return $data;
    }

    /*
     * Add new payment
     * 
     * @param StoreCalculatorRequest $request
     */
    public function assignCharge(StoreCalculatorRequest $request)
    {
        $chargeRequest = [
            'loanid'            => $request->loanId,
            'chargeid'          => $request->chargeId,
            'value'             => $request->value,
            'nominalCode'       => $request->nominalCode,
        ];

        $url = $this->apiPath.'loancharges';
        
        try {
            $response = post($url, $chargeRequest);
        } catch (\Exception $e) {
            return exception($e);
        }    
        
        return redirect('calculations');
    }

    /*
     * Assign closing charge
     * 
     * @param StoreCalculatorRequest $request
     */
    public function assignClosingCharge(StoreCalculatorRequest $request)
    {
        $chargeRequest = [
            'loanId'            => $request->loanId,
            'closingChargeId'          => $request->chargeId,
            'value'              => $request->value,
        ];

        $url = $this->apiPath.'closingloancharge';
        
        try {
            $response = post($url, $chargeRequest);
        } catch (\Exception $e) {
            return exception($e);
        }    
        
        return redirect('calculations');
    }

    /*
     * Remove closing charge
     * 
     * @param StoreCalculatorRequest $request
     */
    public function deleteClosingCharge(StoreCalculatorRequest $request)
    {
        $url = $this->apiPath.'closingloancharge/'.$request->loanId.'/'.$request->chargeId;
        try {
            $response = delete($url);
        } catch (\Exception $e) {
            return exception($e);
        }    
        
        return redirect('calculations');
    }

    /*
     * Remove updateClosingLoanCharge
     * 
     * @param StoreCalculatorRequest $request
     */
    public function updateClosingLoanCharge(StoreCalculatorRequest $request)
    {
        $chargeRequest = [
            'value' => $request->value,
        ];

        $url = $this->apiPath.'closingloancharge/'.$request->loanId.'/'.$request->chargeId;
        try {
            $response = patch($url, $chargeRequest);
        } catch (\Exception $e) {
            return exception($e);
        }    
        
        return redirect('calculations');
    }
    
    /*
     * Create other charge api
     */
    
    public function createOtherCharge(StoreCalculatorRequest $request)
    {
        $othereChargeRequest = [
            'loanid' => $request->loanid,
            'chargedate' => $request->chargedate,
            'name' => $request->name,
            'value' => $request->value,
            'chargetype' => $request->chargetype,
            'amounttype' => $request->amounttype,
            'nominalCode' => $request->nominalCode,
        ];
        
        $createOtherChargeUrl = $this->apiPath.'othercharges';
        
        try{
            $request = post($createOtherChargeUrl, $othereChargeRequest);
        } catch (\Exception $e) {
            return exception($e);
        }

        return redirect('calculations');
    }
    
    /*
     * Update other charge api
     */
    
    public function updateOtherCharge(StoreCalculatorRequest $request)
    {
        $updateOtherChargeRequest = [
            'name' => $request->name,
            'value' => $request->value,
            'chargetype' => $request->chargetype,
            'amounttype' => $request->amounttype,
            'chargedate' => $request->chargedate,
        ];
        $updateOtherChargeUrl = $this->apiPath.'othercharges/'.$request->otherchargeid;
        
        try{
            $request = patch($updateOtherChargeUrl, $updateOtherChargeRequest);
        } catch (\Exception $e) {
            return exception($e);
        }

        return redirect('calculations');
    }
    /*
     * Delete other charge api
     */
    public function deleteOtherCharge(DeleteCalculatorRequest $request)
    {
        $deleteOthereChargeUrl = $this->apiPath.'othercharges/'.$request->otherchargeid;
        
        try{
            $request = delete($deleteOthereChargeUrl);
        } catch (\Exception $e) {
            return exception($e);
        }

        return redirect('calculations');
    }

    /*
     * Create refund api
     */
    
    public function createRefund(StoreCalculatorRequest $request)
    {
        $refundRequest = [
            'loanId'      => $request->loanId,
            'amount'      => $request->amount,
            'refundAt'    => $request->refundAt,
            'description' => $request->description,
            'chargeId'    => $request->chargeId,
            'otherChargeId'    => $request->otherChargeId,
            'assetType'    => $request->assetType,
            'nominalCode'    => $request->nominalCode,
        ];

        $refundUrl = $this->apiPath.'refunds';
        // dd($refundRequest);
        try{
            $request = post($refundUrl, $refundRequest);
        } catch (\Exception $e) {
            return exception($e);
        }

        return redirect('calculations');
    }

    /*
     * Create DefaultRateRefund api
     */
    
    public function createDefaultRateRefund(StoreCalculatorRequest $request)
    {
        $refundDefaultRateRequest = [
            'loanId'        => $request->loanId,
            'startDate'     => $request->startDate,
            'endDate'       => $request->endDate,
            'refundDate'    => $request->refundDate,
            'days'          => $request->days,
            'balance'       => $request->balance,
            'description'   => $request->description,
            'refundDate'    => $request->refundDate,
            'hide'          => $request->hide,
            'hideTransaction'=> $request->hideTransaction,
        ];
// dd($refundDefaultRateRequest);
        $refundDefaultRateUrl = $this->apiPath.'createdefaultraterefund';
        try{
            $refundRequest = post($refundDefaultRateUrl, $refundDefaultRateRequest);
        } catch (\Exception $e) {
            if($request->json){
                $res = json_decode((string)$e->getResponse()->getBody(true));
                return response()->json(['code' => 400, 'message' => $res->message]);
            }
            return exception($e);
        }
        if($request->json == true)
            return response()->json(['code' => 200, 'message' => 'Default rate refund successful.']);
        return redirect('calculations');
    }

    /*
     * Create PaymentPlan api
     */
    
    public function createPaymentPlan(StoreCalculatorRequest $request)
    {
        $createPaymentPlanRequest = [
            'loanId'      => $request->loanId,
            'date'      => $request->date,
        ];

        $createPaymentPlanUrl = $this->apiPath.'createpaymentplan';
        // dd($refundRequest);
        try{
            $request = post($createPaymentPlanUrl, $createPaymentPlanRequest);
        } catch (\Exception $e) {
            return exception($e);
        }

        return redirect('calculations');
    }

    /*
     * Create DeletePaymentPlan api
     */
    
    public function deletePaymentPlan(StoreCalculatorRequest $request)
    {
        $deletePaymentPlanUrl = $this->apiPath.'deletepaymentplan/'.session('loanid');
        try{
            $request = get($deletePaymentPlanUrl);
        } catch (\Exception $e) {
            return exception($e);
        }

        return redirect('calculations');
    }    

    /*
     * Create lockMonth api
     */
    
    public function lockMonth(StoreCalculatorRequest $request)
    {
        $lockMonthRequest = [
            'loanId'            => $request->loanId,
            'month'             => $request->month,
            'nominalCode'       => $request->nominalCode,
        ];

        $lockMonthUrl = $this->apiPath.'lockmonth';
        try{
            $request = post($lockMonthUrl, $lockMonthRequest);
        } catch (\Exception $e) {
            return exception($e);
        }

        return redirect('calculations');
    }

    /*
     * Current balance api
     */
    
    public function currentBalance(StoreCalculatorRequest $request)
    {
        $currentBalanceUrl = $this->apiPath.'closingbalance/'.session('loanid')."?date=".$request->madeupdate."&virtualPmt=".$request->virtualPmt;
        try{
            $response = get($currentBalanceUrl);
            $closingBalance['closing_balance'] = $response->data->amount_pending;
            $closingBalance['breakdown'] = $response->data;
        } catch (\Exception $e) {
            return exception($e);
        }
        return $closingBalance;
    }

    /*
     * Update Min Term api
     */
    
    public function updateMinTerm(StoreCalculatorRequest $request)
    {
        $minTermRequest = [
            'minTerm'      => $request->minTerm,
            'monthlyPayment'      => $request->monthlyPmt,
        ];

        $minTermUrl = $this->apiPath.'updatemintermcharge/'.session('loanid');
        
        try{
            $request = patch($minTermUrl, $minTermRequest);
        } catch (\Exception $e) {
            return exception($e);
        }

        return redirect('calculations');
    }

    /*
     * Update renewLoanDuration api
     */
    
    public function renewLoanDuration(StoreCalculatorRequest $request)
    {
        $renewChargeRequest = [
            'loanId'      => $request->loanId,
            'value'      => $request->value,
            'duration'      => $request->duration,
            'renewDate'      => $request->renewDate,
            'description'      => $request->description,
            'nominalCode'      => $request->nominalCode
        ];

        $renewChargeUrl = $this->apiPath.'renewalcharges';
        
        try{
            $request = post($renewChargeUrl, $renewChargeRequest);
        } catch (\Exception $e) {
            return exception($e);
        }

        return redirect('calculations');
    }

    /*
     * Create Transfer api
     */
    
    public function createTransfer(StoreCalculatorRequest $request)
    {
        $transferRequest = [
            'senderLoanId'      => $request->senderLoanId,
            'receiverLoanId'      => $request->receiverLoanId,
            'amount'      => $request->amount,
            'chargeValue'      => $request->chargeValue,
            'senderTnxDate'      => $request->senderTnxDate,
            'receiverTnxDate'      => $request->receiverTnxDate,
            'description'      => $request->description,
            'isCapitalReduction'      => $request->isCapitalReduction,
            'nominalCode'      => $request->nominalCode,
        ];


        $transferUrl = $this->apiPath.'createtransfer';
        
        try{
            $request = post($transferUrl, $transferRequest);
        } catch (\Exception $e) {
            return exception($e);
        }

        return redirect('calculations');
    }

    /**
    * Generate pdf for monthly statement
    * @param StoreCalculatorRequest $request
    */
    public function monthStatementPdf(StoreCalculatorRequest $request)
    {

        $monthStatementsUrl = $this->apiPath.'getmonthstatements/'.session('loanid').'?date='.$request->date;
        $statementsUrl = $this->apiPath.'getallstatements/'.session('loanid')."?date=".$request->date;
        $loanUrl = $this->apiPath.'loans/'.session('loanid');
        $requestDate = \Carbon\Carbon::parse($request->date);
        try{
            $response = get($monthStatementsUrl);
            $monthStatements = $response->data;

            $response = get($statementsUrl);
            $statements = $response->data;

            $response = get($loanUrl);
            $loan = $response->data;
        } catch (\Exception $e) {
            return exception($e);
        }

        $fileName = preg_replace('/\s/', '',  $loan->loanid."-".$requestDate->format('M')."Statement".'.pdf');

        $statement = collect($monthStatements)->last();
        $pdf = new \App\Classes\PdfHeader();
        $addMonthStatements = new \App\Classes\MonthStatementPdfV2();
        $addMonthStatements->generateBorrowerHtml($pdf, $statement, $fileName, 'FI', $statements, $loan, $request->monthTnxs);

        // Sharepoint::createZip($fileName, $fileName, true);
        // $filePath = public_path()."/$fileName";
        $sharepoint = new \App\Classes\Sharepoint();

        // $zip_file = public_path()."/".$fileName.".zip"; // Name of our archive to download
        // Initializing PHP class
        // $zip = new \ZipArchive();
        // $zip->open($zip_file, \ZipArchive::CREATE | \ZipArchive::OVERWRITE);

        // $invoice_file = $fileName;
        // Adding file: second parameter is what will the path inside of the archive
        // So it will create another folder called "storage/" inside ZIP, and put the file there.
        // $zip->addFile(public_path($invoice_file), $invoice_file);
        // $zip->close();
        $sharepoint->uploadToSharepoint(public_path()."/".$fileName, "Month Statements/".$fileName);
        $sharepoint->unlinkFile($fileName);
    }

    /**
    * Generate pdf for monthly statement
    * @param StoreCalculatorRequest $request
    */
    public function addNewRowInLoanExtras(StoreCalculatorRequest $request)
    {
        $addNewRowUrl = $this->apiPath.'loanextras/addnewrow';
        $addNewRowRequest = [
            'loanid' => session('loanid'),
            'note' => $request->note,
            'breakdown' => $request->breakdown,
        ];

        try{
            $response = post($addNewRowUrl, $addNewRowRequest);
        } catch (\Exception $e) {
            return exception($e);
        }
        return redirect()->back();
    }

    public function createOption(StoreCalculatorRequest $request)
    {
        $optionRequest = [
            'loanid'    => session('loanid'),
            'name'      => $request->name,
            'value'     => $request->value,
            'mappedId'     => $request->mappedId,
            'createDate'=> $request->createDate,
        ];
        if($request->updateByLoanId){

            $optionRequest['loanId'] = $request->loanId;

            $optionsUrl = $this->apiPath.'update-option-by-loanid';

            try{
                $response = patch($optionsUrl, $optionRequest);
                return response()->json(['code' => 200, 'response' => $response]);
            } catch (\Exception $e) {
                return response()->json(['code' => 400, 'message' => $e->getMessage()]);
            }
            return response()->json(['code' => 200, 'response' => 'Un-expected error occurred.']);
        }

        if(isset($request->optId)){
            $optionsUrl = $this->apiPath.'options/'.$request->optId;

            try{
                $response = patch($optionsUrl, $optionRequest);
            } catch (\Exception $e) {
                if($request->web)
                    return exception($e);
                return response()->json(['code' => 400, 'message' => $e->getMessage()]);
            }
            if($request->web)
                return redirect()->back();
            return response()->json(['code' => 200, 'response' => $response->data]);
        }

        $optionsUrl = $this->apiPath.'options';
        try{
            $response = post($optionsUrl, $optionRequest);
        } catch (\Exception $e) {
            return exception($e);
        }

        return redirect()->back();
        // return response()->json(['code' => 200, 'response' => $response->data]);
    }

    public function deleteOption(StoreCalculatorRequest $request)
    {
        $delOptionUrl = $this->apiPath.'options/'.$request->optId;

        try{
            $response = delete($delOptionUrl);
        } catch (\Exception $e) {
            return exception($e);
        }
        return redirect()->back();
    }

    public function createTranch(StoreCalculatorRequest $request)
    {
        $createTranchUrl = $this->apiPath.'tranches';

        $tranchRequest = [
            'loanid'        => session('loanid'),
            'amount'        => $request->amount,
            'grossLoan'     => $request->grossLoan,
            'tranchDate'    => $request->tranchDate,
            'description'   => $request->description,
            'nominalCode'   => $request->nominalCode,
        ];

        try{
            $response = post($createTranchUrl, $tranchRequest);
        } catch (\Exception $e) {
            return exception($e);
        }
        return redirect()->back();
    }
    
    /**
     * Search Option (Ajax)
     *
     * @param EditCalculatorRequest $request
     */
    public function searchOption(EditCalculatorRequest $request)
    {
        $searchOptionUrl = $this->apiPath.'options/search';

        $searchOptionRequest = [
            'loanid'        => session('loanid'),
            'name'        => $request->name,
        ];

        try{
            $response = post($searchOptionUrl, $searchOptionRequest);
            return response()->json(['code' => 200, 'message' => $response]);
        } catch (\Exception $e) {
            return response()->json(['code' => 400, 'message' => $e->getMessage()]);
            // return exception($e);
        }

        return response()->json(['code' => 400, 'message' => "No results found for $this->name"]);
    }

    /**
     * Edit entity (show UI)
     *
     * @param EditCalculatorRequest $request
     */
    public function edit(EditCalculatorRequest $request)
    {
        $calculator = Apiato::call('Calculator@GetCalculatorByIdAction', [$request]);

        // ..
    }

    /**
     * Edit entity (show UI)
     *
     * @param EditCalculatorRequest $request
     */
    public function datatypes(EditCalculatorRequest $request)
    {
        if(!isset($_GET['gridId']))
            return redirect()->back();
        if(isset($_GET['searchTags'])){
            session(['searchTags' => $request->searchTags]);
            session(['operators' => $request->operators]);
            session(['searchByQuery' => false]);
        } elseif($request->searchByQuery) {
            session(['searchByQuery' => $request->searchByQuery]);
            session(['searchTags' => array()]);
            session(['operators' => array()]);
        } else {
            session(['searchTags' => array()]);
            session(['operators' => array()]);
            session(['searchByQuery' => false]);
        }
        if($request->saveSearch && $request->searchByQuery == '0'){
            $url = $this->apiPath.'searches';
            $searchRequest = [
                'tags' => session('searchTags'),
                'operators'     => session('operators'),
            ];
            // dd($url, $searchRequest);
            try{
                $response = post($url, $searchRequest);
            } catch (\Exception $e) {
                return exception($e);
            }
        }
        // dd('ásdf');
// dd(isset($_GET['searchTags']), isset($_GET['operators']), $request->all());
        // grid
        $url = $this->apiPath.'tags';
        try {
            $response = get($url);
            $tags = $response->data;
        } catch (\Exception $e) {
            return exception($e);
        }

        $url = $this->apiPath.'grids/'.$request->gridId;
        try {
            $response = get($url);
            $grid = $response->data;
        } catch (\Exception $e) {
            return exception($e);
        }
        
        $url = $this->apiPath.'searches';
        try {
            $response = get($url);
            $searches = $response->data;
        } catch (\Exception $e) {
            return exception($e);
        }

        return view('welcome::datatypes', compact('tags', 'grid', 'searches'));
    }

    /**
     * Edit entity (show UI)
     *
     * @param EditCalculatorRequest $request
     */
    public function datatypeApi(EditCalculatorRequest $request)
    {
        if(count(session('searchTags')) > 0 && session('searchByQuery') == false){
            $datatypeApiUrl = $this->apiPath.'datatype?limit=0&searchTags='.json_encode(session('searchTags')).'&operators='.json_encode(session('operators'));
        } elseif(session('searchByQuery') != false){
            $datatypeApiUrl = $this->apiPath.'datatype?limit=0&searchByQuery='.session('searchByQuery');
        } else{
            $datatypeApiUrl = $this->apiPath.'datatype?limit=0';
        }
        try{
            $response = get($datatypeApiUrl);
            return response()->json($response);
        } catch (\Exception $e) {
            return response()->json(['code' => 400, 'message' => $e->getMessage()]);
            // return exception($e);
        }

        return response()->json(['code' => 400, 'message' => "No results found for $this->name"]);
    }

    /**
     * Update a given entity
     *
     * @param UpdateCalculatorRequest $request
     */
    public function updateTransaction(UpdateCalculatorRequest $request)
    {
        $updateTnxUrl = $this->apiPath."update-transaction?journalId=$request->tnxId&hide=$request->hide&hideTransaction=$request->hideTransaction&notes=$request->notes";
        
        try{
            $response = get($updateTnxUrl);
            return response()->json($response);
        } catch (\Exception $e) {
            return response()->json(['code' => 400, 'message' => $e->getMessage()]);
        }

        return response()->json(['code' => 400, 'message' => "No results found for $this->name"]);
    }

    /**
     * Update a given entity
     *
     * @param UpdateCalculatorRequest $request
     */
    public function fileinfo(UpdateCalculatorRequest $request)
    {
        return view('welcome::fileinfo');
    }

    public function fileinfoApi(UpdateCalculatorRequest $request)
    {
        $datatypeApiUrl = $this->apiPath.'fileinfos/export?limit=0';

        try{
            $response = get($datatypeApiUrl);
            return response()->json($response);
        } catch (\Exception $e) {
            return response()->json(['code' => 400, 'message' => $e->getMessage()]);
        }

        // return response()->json(['code' => 400, 'message' => "No results found for $this->name"]);
    }

    public function transactionsTags(UpdateCalculatorRequest $request)
    {
        $createJournalTagUrl = $this->apiPath.'createjournaltag';
        $createJournalTagRequest = [
            'journalid' => $request->tnxid,
            'tagid'     => $request->tagid,
        ];
        // dd($createJournalTagRequest);
        try{
            $response = post($createJournalTagUrl, $createJournalTagRequest);
        } catch (\Exception $e) {
            return exception($e);
        }
            return redirect()->back();

        // return response()->json(['code' => 400, 'message' => "No results found for $this->name"]);
    }

    public function showAllTnx(UpdateCalculatorRequest $request)
    {
        session(['showAllTnx' => $request->showAllTnx]);
        return response()->json(['code' => 200, 'message' => "Show all success."]);
    }

    public function createGrace(StoreCalculatorRequest $request)
    {
        $createTranchUrl = $this->apiPath.'graces';

        $graceRequest = [
            'loanId'        => session('loanid'),
            'days'          => $request->days,
            'startDate'     => $request->startDate,
            'endDate'       => $request->endDate,
            'description'   => $request->description,
        ];
        // dd($graceRequest);
        try{
            $response = post($createTranchUrl, $graceRequest);
        } catch (\Exception $e) {
            return exception($e);
        }
        return redirect()->back();
    }

}
