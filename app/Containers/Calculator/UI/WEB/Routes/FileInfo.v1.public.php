<?php

/** @var Route $router */
$router->get('fileinfo', [
    'as' => 'web_calculator_file_info',
    'uses'  => 'Controller@fileInfo',
    'middleware' => [
      'auth:web',
    ],
]);
