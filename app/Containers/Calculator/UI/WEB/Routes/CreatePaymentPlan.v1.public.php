<?php

/** @var Route $router */
$router->get('calculations/createpaymentplan', [
    'as' => 'web_calculator_createpaymentplan',
    'uses'  => 'Controller@createPaymentPlan',
    'middleware' => [
      'auth:web',
    ],
]);
