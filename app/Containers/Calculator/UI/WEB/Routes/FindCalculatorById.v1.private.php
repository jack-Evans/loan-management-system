<?php

/** @var Route $router */
$router->get('calculations/{id}', [
    'as' => 'web_calculator_show',
    'uses'  => 'Controller@show',
    'middleware' => [
      'auth:web',
    ],
]);
