
<?php

/** @var Route $router */
$router->post('calculations/renewloanduration', [
    'as' => 'web_calculator_renew_loan_duration',
    'uses'  => 'Controller@renewLoanDuration',
    'middleware' => [
      'auth:web',
    ],
]);
