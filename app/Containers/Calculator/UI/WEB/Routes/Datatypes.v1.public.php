<?php

/** @var Route $router */
$router->get('datatypes', [
    'as' => 'web_calculator_datatypes',
    'uses'  => 'Controller@datatypes',
    'middleware' => [
      'auth:web',
    ],
]);
