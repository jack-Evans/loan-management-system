<?php

/** @var Route $router */
$router->get('show-all-tnx', [
    'as' => 'web_calculator_show_all_tnx',
    'uses'  => 'Controller@showAllTnx',
    'middleware' => [
      'auth:web',
    ],
]);
