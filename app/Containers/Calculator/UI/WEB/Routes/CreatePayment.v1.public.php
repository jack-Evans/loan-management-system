<?php

/** @var Route $router */
$router->post('calculations/createpayment', [
    'as' => 'web_calculator_create_payment',
    'uses'  => 'Controller@createPayment',
    'middleware' => [
      'auth:web',
    ],
]);
