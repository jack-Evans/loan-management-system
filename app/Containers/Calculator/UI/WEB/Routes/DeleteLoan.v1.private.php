<?php

/** @var Route $router */
$router->get('calculations/deleteloan/{id}', [
    'as' => 'web_calculator_delete',
    'uses'  => 'Controller@deleteLoan',
    'middleware' => [
      'auth:web',
    ],
]);
