
<?php

/** @var Route $router */
$router->get('searchoption', [
    'as' => 'web_calculator_search_option',
    'uses'  => 'Controller@searchOption',
    'middleware' => [
      'auth:web',
    ],
]);
