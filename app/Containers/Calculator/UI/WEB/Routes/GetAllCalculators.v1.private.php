<?php

/** @var Route $router */
$router->get('calculations', [
    'as' => 'web_calculator_index',
    'uses'  => 'Controller@index',
    'middleware' => [
      'auth:web',
    ],
]);
