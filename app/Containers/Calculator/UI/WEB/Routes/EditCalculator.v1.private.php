<?php

/** @var Route $router */
$router->get('calculations/{id}/edit', [
    'as' => 'web_calculator_edit',
    'uses'  => 'Controller@edit',
    'middleware' => [
      'auth:web',
    ],
]);
