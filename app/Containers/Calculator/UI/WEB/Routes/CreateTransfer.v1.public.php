<?php

/** @var Route $router */
$router->post('calculations/createtransfer', [
    'as' => 'web_calculator_createtransfer',
    'uses'  => 'Controller@createTransfer',
    'middleware' => [
      'auth:web',
    ],
]);
