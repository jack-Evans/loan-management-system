<?php

/** @var Route $router */
$router->post('calculations/createtranch', [
    'as' => 'web_calculator_create_tranch',
    'uses'  => 'Controller@createTranch',
    'middleware' => [
      'auth:web',
    ],
]);
