<?php

/** @var Route $router */
$router->get('datatypeapi', [
    'as' => 'web_calculator_datatypes_api',
    'uses'  => 'Controller@datatypeApi',
    'middleware' => [
      'auth:web',
    ],
]);
