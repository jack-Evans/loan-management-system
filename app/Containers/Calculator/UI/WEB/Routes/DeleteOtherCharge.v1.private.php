<?php

/** @var Route $router */
$router->get('calculations/deleteothercharge/{otherchargeid}', [
    'as' => 'web_calculator_delete_other_charge',
    'uses'  => 'Controller@deleteOtherCharge',
    'middleware' => [
      'auth:web',
    ],
]);
