<?php

/** @var Route $router */
$router->post('calculations/updateloan', [
    'as' => 'web_calculator_update_loan',
    'uses'  => 'Controller@updateLoan',
    'middleware' => [
      'auth:web',
    ],
]);
