<?php

/** @var Route $router */
$router->get('calculations/assignclosingcharge', [
    'as' => 'web_calculator_assign_charge',
    'uses'  => 'Controller@assignClosingCharge',
    'middleware' => [
      'auth:web',
    ],
]);
