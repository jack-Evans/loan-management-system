<?php

/** @var Route $router */
$router->get('calculations/assigncharge', [
    'as' => 'web_calculator_assign_charge',
    'uses'  => 'Controller@assignCharge',
    'middleware' => [
      'auth:web',
    ],
]);
