<?php

/** @var Route $router */
$router->post('calculations/createothercharge', [
    'as' => 'web_calculator_create_other_charge',
    'uses'  => 'Controller@createOtherCharge',
    'middleware' => [
      'auth:web',
    ],
]);
