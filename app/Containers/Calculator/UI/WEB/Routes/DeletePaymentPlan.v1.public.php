<?php

/** @var Route $router */
$router->get('deletepaymentplan', [
    'as' => 'web_calculator_delete_pmtplan',
    'uses'  => 'Controller@deletePaymentPlan',
    'middleware' => [
      'auth:web',
    ],
]);
