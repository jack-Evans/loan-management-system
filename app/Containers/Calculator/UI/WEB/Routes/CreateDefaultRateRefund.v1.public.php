<?php

/** @var Route $router */
$router->post('calculations/createdefaultraterefund', [
    'as' => 'web_calculator_create_default_rate_refund',
    'uses'  => 'Controller@createDefaultRateRefund',
    'middleware' => [
      'auth:web',
    ],
]);
