<?php

/** @var Route $router */
$router->get('calculations/deleteoption/{optId}', [
    'as' => 'web_calculator_delete_option',
    'uses'  => 'Controller@deleteOption',
    'middleware' => [
      'auth:web',
    ],
]);
