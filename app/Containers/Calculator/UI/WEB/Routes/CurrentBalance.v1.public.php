<?php

/** @var Route $router */
$router->get('calculations/currentbalance/{madeupdate}/{virtualPmt}', [
    'as' => 'web_calculator_current_balance',
    'uses'  => 'Controller@currentBalance',
    'middleware' => [
      'auth:web',
    ],
]);
