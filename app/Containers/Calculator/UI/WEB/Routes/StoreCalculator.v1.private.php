<?php

/** @var Route $router */
$router->post('calculations/store', [
    'as' => 'web_calculator_store',
    'uses'  => 'Controller@store',
    'middleware' => [
      'auth:web',
    ],
]);
