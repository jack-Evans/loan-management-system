<?php

/** @var Route $router */
$router->get('update-tnx', [
    'as' => 'web_calculator_update_tnx',
    'uses'  => 'Controller@updateTransaction',
    'middleware' => [
      'auth:web',
    ],
]);
