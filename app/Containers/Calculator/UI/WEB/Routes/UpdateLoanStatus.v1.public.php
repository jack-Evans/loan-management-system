<?php

/** @var Route $router */
$router->get('updateloanstatus', [
    'as' => 'web_calculator_update_loan_status',
    'uses'  => 'Controller@updateLoanStatus',
    'middleware' => [
      'auth:web',
    ],
]);
