<?php

/** @var Route $router */
$router->post('calculations/updateothercharge', [
    'as' => 'web_calculator_update_other_charge',
    'uses'  => 'Controller@updateOtherCharge',
    'middleware' => [
      'auth:web',
    ],
]);
