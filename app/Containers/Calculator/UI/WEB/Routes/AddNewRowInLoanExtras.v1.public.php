<?php

/** @var Route $router */
$router->post('calculations/addnewrowinloanextras', [
    'as' => 'web_calculator_assign_charge',
    'uses'  => 'Controller@addNewRowInLoanExtras',
    'middleware' => [
      'auth:web',
    ],
]);
