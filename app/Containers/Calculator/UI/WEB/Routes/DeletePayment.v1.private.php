<?php

/** @var Route $router */
$router->get('calculations/deletepayment/{paymentId}', [
    'as' => 'web_calculator_delete_payment',
    'uses'  => 'Controller@deletePayment',
    'middleware' => [
      'auth:web',
    ],
]);
