<?php

/** @var Route $router */
$router->post('transaction-tags', [
    'as' => 'web_calculator_transactions_tags',
    'uses'  => 'Controller@transactionsTags',
    'middleware' => [
      'auth:web',
    ],
]);
