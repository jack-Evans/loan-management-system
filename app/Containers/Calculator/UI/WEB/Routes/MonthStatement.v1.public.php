<?php

/** @var Route $router */
$router->get('monthstatement/{date}', [
    'as' => 'web_calculator_assign_charge',
    'uses'  => 'Controller@monthStatementPdf',
    'middleware' => [
      'auth:web',
    ],
]);
