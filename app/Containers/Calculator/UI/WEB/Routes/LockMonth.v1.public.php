<?php

/** @var Route $router */
$router->post('calculations/lockmonth', [
    'as' => 'web_calculator_lockmonth',
    'uses'  => 'Controller@lockMonth',
    'middleware' => [
      'auth:web',
    ],
]);
