<?php

/** @var Route $router */
$router->post('calculations/createoption', [
    'as' => 'web_calculator_assign_charge',
    'uses'  => 'Controller@createOption',
    'middleware' => [
      'auth:web',
    ],
]);
