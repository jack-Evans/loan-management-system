<?php

/** @var Route $router */
$router->post('calculations/updateclosingloancharge', [
    'as' => 'web_calculator_update_closing_loan_charge',
    'uses'  => 'Controller@updateClosingLoanCharge',
    'middleware' => [
      'auth:web',
    ],
]);
