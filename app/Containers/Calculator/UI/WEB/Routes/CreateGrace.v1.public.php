<?php

/** @var Route $router */
$router->post('calculations/create-grace', [
    'as' => 'web_calculator_create_grace',
    'uses'  => 'Controller@createGrace',
    'middleware' => [
      'auth:web',
    ],
]);
