<?php

/** @var Route $router */
$router->post('calculations/createrefund', [
    'as' => 'web_calculator_create_refund',
    'uses'  => 'Controller@createRefund',
    'middleware' => [
      'auth:web',
    ],
]);
