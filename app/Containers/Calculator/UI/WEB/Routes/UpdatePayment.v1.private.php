<?php

/** @var Route $router */
$router->post('calculations/updatepayment', [
    'as' => 'web_calculator_update_payment',
    'uses'  => 'Controller@updatePayment',
    'middleware' => [
      'auth:web',
    ],
]);
