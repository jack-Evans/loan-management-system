<?php

/** @var Route $router */
$router->get('calculations/deleteclosingcharge', [
    'as' => 'web_calculator_assign_charge',
    'uses'  => 'Controller@deleteClosingCharge',
    'middleware' => [
      'auth:web',
    ],
]);
