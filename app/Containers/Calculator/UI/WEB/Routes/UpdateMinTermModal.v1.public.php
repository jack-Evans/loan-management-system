<?php

/** @var Route $router */
$router->post('calculations/updateminterm', [
    'as' => 'web_calculator_update_min_term',
    'uses'  => 'Controller@updateMinTerm',
    'middleware' => [
      'auth:web',
    ],
]);
