<?php

/** @var Route $router */
$router->post('calculations/createcharge', [
    'as' => 'web_calculator_create',
    'uses'  => 'Controller@createCharge',
    'middleware' => [
      'auth:web',
    ],
]);
