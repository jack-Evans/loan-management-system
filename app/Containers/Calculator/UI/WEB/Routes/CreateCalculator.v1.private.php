<?php

/** @var Route $router */
$router->get('calculations/create', [
    'as' => 'web_calculator_create',
    'uses'  => 'Controller@create',
    'middleware' => [
      'auth:web',
    ],
]);
