<?php

/** @var Route $router */
$router->get('fileinfoapi', [
    'as' => 'web_calculator_file_infoapi',
    'uses'  => 'Controller@fileinfoApi',
    'middleware' => [
      'auth:web',
    ],
]);
