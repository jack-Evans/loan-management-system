@extends('layouts.app')

@section('title', 'Calculations')

@section('content')

    <form action="{{url('tempcalculations')}}" method="get">
        <div class="row">
            <div class="col-3">
                <b class="text-info">Today made up date:</b>
                <input class="form-control datepicker4" autocomplete="off" name="currentdate" value="{{isset($_GET['currentdate']) ? $_GET['currentdate'] : ''}}">
            </div>
<!--            <div class="col-2">
                <button type="submit" class="btn btn-success mt-4">Go</button>
            </div>-->
            <div class="col-2">
              <div class="mt-4">
                <b class="text-success">Current Balance:</b>
                <p class="text-success">{{$currentBalance > 0 ? $currentBalance : '' }}</p>
                </div>
            </div>
         </div>
    </form>
    <span class="space padding-10"></span>
    <h6>Create Loan</h6>
    <form action="{{ url('calculations/store' )}}" method="post">
        <div class="row">
            <div class="col-2">
                @csrf()
                <div class="form-group">
                  <label for="loanid">Loan Id:</label>
                  <input type="text" class="form-control" name="loanid"
                    value="{{old('loanid')}}">
                </div>
            </div>
            <div class="col-2">
                <div class="form-group">
                  <label for="netloan">Net Loan:</label>
                  <input type="text" class="form-control" name="netloan"
                    value="{{old('netloan')}}">
                </div>
            </div>
            <div class="col-1">
                <div class="form-group">
                  <label for="rate">Rate:</label>
                  <input type="text" class="form-control" name="rate"
                         value="{{ old('rate')}}">
                </div>
            </div>
            <div class="col-2">
                <div class="form-group ">
                    <label for="defaultRate">Default Rate:</label>
                    <input type="text" class="form-control" name="defaultRate"
                           value="{{ old('defaultRate')}}">
                </div>
            </div>
            <div class="col-2">
                <div class="form-group">
                    <label for="duration">Duration:</label>
                    <input type="text" class="form-control" name="duration"
                           value="{{ old('duration')}}">
                </div>
            </div>
            <div class="col-2">
                <div class="form-group">
                    <label for="issuedate">Issue Date:</label>
                    <input class="form-control datepicker" autocomplete="off" name="issuedate"
                           value="{{ old('issuedate')}}">
                </div>
            </div>
            <div class="col-2">
                <div class="form-group">
                    <label for="description">Description:</label>
                    <input class="form-control" name="description"
                           value="{{ old('description')}}">
                </div>
            </div>

            <div class="col-2">
                <div class="form-group">
                    <label for="customerId">Customers:</label>
                    <select name="customerId" class="form-control">
                        @foreach($customers as $customer)
                           <option value="{{$customer->id}}" {{  old('customerId') == $customer->id ? 'selected': '' }} >{{ $customer->username }}</option>
                        @endforeach
                    </select>
                </div>
            </div>

            <div class="col-1 mt-3">
                <button type="submit" class="btn btn-primary mt-3 float-right">Submit</button>
            </div>
        </div>
    </form>

    @if(isset($loans))
    <div class="panel-group has-box-shadow" id="accordion">
        <div class="panel panel-default">
            <div class="panel-heading" data-target="#collapseLoan" data-toggle="collapse" data-parent="#accordion">
                <h5 class="text-info panel-title">
                    All Loans
                    <span class="text-info fa fa-plus"></span>
                </h5>
            </div>
            <div class="panel-collapse collapse in" id="collapseLoan">
                <div class="panel-body">
                    <table class="table text-left">
                        <thead>
                            <tr>
                                <th>Loan Id</th>
                                <th>Issue Date</th>
                                <th>Net Loan</th>
                                <!--<th>Status</th>-->
                                <!--<th>Balance</th>-->
                                <th></th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach($loans as $key => $loan)
                            <tr>
                                <td>{{$loan->loanid}}</td>
                                <td>{{\Carbon\Carbon::parse($loan->issueat->date)->format('M d Y')}}</td>
                                <td>{{$loan->interest->principal_amount}}</td>
                                <!--<td>{{ $loan->status }}</td>-->
                                <!--<td>{{ $loan->interest->principal_amount }}</td>-->
                                <td>
<!--                                    <a href="{{url('calculations/deleteloan/'.$loan->id)}}" class="btn btn-danger ml-1 float-right" >
                                            Delete
                                    </a>-->
                                    <a href="{{url('calculations?loanid='.$loan->id)}}" class="btn btn-info ml-1 float-right" >
                                            View
                                    </a>
                                </td>
                            </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
    @endif
    
    @if($selectedLoan)
        <div class="has-box-shadow">
        <h6 class="panel-title">
            Selected Loan
        </h6>

        <div class="panel-body">
            <table class="table text-left">
                <thead>
                    <tr>
                        <th>Loan Id</th>
                        <th>Net Loan</th>
                        <th>Rate</th>
                        <th>Default Rate</th>
                        <th>Due Day</th>
                        <th>Issued At</th>
                        <th>Duration</th>
                        <th>Customer</th>
                    </tr>
                </thead>
                <tbody>
                    <tr>
                        <td>{{$selectedLoan->loanid}}</td>
                        <td>{{$selectedLoan->netloan}}</td>
                        <td>{{$selectedLoan->interest->rate}}</td>
                        <td>{{$selectedLoan->interest->default_rate}}</td>
                        <td>{{\Carbon\Carbon::parse($selectedLoan->issueat->date)->format('M d Y')}}</td>
                        <td>{{$selectedLoan->interest->duration}}</td>
                        <td>{{$selectedLoan->customer->username}}</td>
                    </tr>
                </tbody>
            </table>
        </div>
    </div>
    @endif
<!-- Only if we have a selected loanid -->
@if(session('loanid') != null)

@if($charges)
    <div class="has-box-shadow">
        <div class="panel panel-default">
            <div class="panel-heading" data-target="#collapseCharge" data-toggle="collapse" data-parent="#accordion">
                <h5 class="text-primary panel-title">
                    All Charges
                    <span class="text-primary fa fa-plus"></span>
                </h5>
            </div>
            <div class="panel-collapse collapse in" id="collapseCharge">
                <div class="panel-body">
                    <table class="table text-left">
                        <thead>
                            <tr>
                                <th>Name</th>
                                <th>Value</th>
                                <th>Charge Type</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach($charges as $key => $charge)
                            <tr>
                                <td>{{$charge->name}}</td>
                                <td>{{$charge->value}}</td>
                                <td>{{$charge->chargetype}}</td>
                                <td>
                                    <button type="button" class="btn btn-primary float-right chargeModal" data-toggle="modal"
                                            data-target="#assignChargeModal" data-chargeid="{{$charge->id}}" data-loanid="{{session('loanid')}}">
                                        Assign
                                    </button>
                                </td>
                            </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
@endif

    <div class="has-box-shadow">
        <div class="panel panel-default">
            <h6 class="panel-title">
                Assigned Charges
            </h6>
        </div>
        <div class="panel-body">
            <table class="table text-left">
                <thead>
                    <tr>
                        <th>Name</th>
                        <th>Value</th>
                        <th>Charge Type</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach($assignedCharges as $key => $charge)
                    @php // dd($charge) @endphp
                    <tr>
                        <td>{{$charge->name}}</td>
                        <td>{{$charge->value}}</td>
                        <td>{{$charge->chargeType}}</td>
                    </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
    </div>

    <div class="has-box-shadow">
        <h6 class="panel-title">
            Add Other Charges
        </h6>
        <form action="{{ url('calculations/createothercharge' )}}" method="post">
            <div class="row">

                <div class="col-2">
                    @csrf()
                    <div class="form-group">
                        <label for="chargedate">Date:</label>
                        <input type="text" class="form-control datepicker6" autocomplete="off" name="chargedate"
                               value="{{ old('chargedate')}}">
                    </div>
                </div>

                <div class="col-2">
                    @csrf()
                    <div class="form-group">
                        <label for="name">Name:</label>
                        <input type="text" class="form-control" name="name"
                               value="{{ old('name')}}">
                    </div>
                </div>

                <div class="col-2">
                    <div class="form-group">
                      <label for="value">Amount:</label>
                      <input type="text" class="form-control" name="value"
                             value="{{old('value')}}">
                    </div>
                </div>

                <div class="col-2 d-none">
                    <div class="form-group">
                        <label for="chargetype">Charge Type:</label>

                        <select class="form-control" name="chargetype">
                            <option value="fixed" {{ (old('chargetype') == 'fixed') ? 'selected' : ''}}>Fixed</option>
                            <option value="percentage" {{ (old('chargetype') == 'percentage') ? 'selected' : ''}}>Percentage</option>
                        </select>
                    </div>
                </div>

                <div class="col-2 d-none">
                    <div class="form-group">
                      <label for="amounttype">Amount Type:</label>
                      <input type="text" class="form-control" name="amounttype"
                             value="netloan">
                    </div>
                </div>

                <input type="hidden" class="form-control" name="loanid" value="{{ session('loanid') }}">

                <div class="col-1 mt-3">
                    <button type="submit" class="btn btn-primary mt-3 float-right">Submit</button>
                </div>
            </div>
        </form>
    </div>


    <div class="has-box-shadow">
        <div class="panel panel-default">
            <h6 class="panel-title">
                Other Charges
            </h6>
        </div>
        <div class="panel-body">
            <table class="table text-left">
                <thead>
                    <tr>
                        <th>Name</th>
                        <th>Value</th>
                        <th>Charge Type</th>
                        <th>Charge Date</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach($otherCharges as $key => $otherCharge)
                    <tr>
                        <td>{{$otherCharge->name}}</td>
                        <td>{{$otherCharge->value}}</td>
                        <td>{{$otherCharge->chargeType}}</td>
                        <td>{{\Carbon\Carbon::parse($otherCharge->journal->tnx_date)->format('M d Y')}}</td>
                        <td>
                            <button type="button" class="btn btn-default float-right otherChargeModal" data-toggle="modal"
                                    data-target="#updateOtherChargeModal" data-otherchargeid="{{$otherCharge->id}}"
                                    data-name="{{$otherCharge->name}}" data-value="{{$otherCharge->value}}"
                                    data-chargetype="{{$otherCharge->chargeType}}" data-amounttype="{{$otherCharge->amountType}}"
                                    data-chargedate="{{\Carbon\Carbon::parse($otherCharge->journal->tnx_date)->format('Y-m-d')}}">
                                Edit
                            </button>
                        </td>
                        <td>
                            <a class="btn btn-danger" href="{{ url('calculations/deleteothercharge/'.$otherCharge->id) }}">Delete</a>
                        </td>
                    </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
    </div>

    <div class="has-box-shadow">
        <h5 class="text-success panel-title">
            Payments
        </h5>

        <div class="panel-body">
            <table class="table text-left">
                <thead>
                    <tr>
                        <th>Date</th>
                        <th>Amount Paid</th>
                        <th>Descriptions</th>
                        <th></th>
                        <th></th>
                    </tr>
                </thead>
                <tbody>
                    @foreach($payments as $key => $payment)
                    <tr>
                        <td>{{\Carbon\Carbon::parse($payment->paid_at)->format('M d Y')}}</td>
                        <td>{{$payment->amount}}</td>
                        <td>{{$payment->journal->comment}}</td>
                        <td>
                            <button type="button" class="btn btn-default float-right paymentModal" data-toggle="modal"
                                    data-target="#updatePaymentModal" data-paymentid="{{$payment->id}}" data-amount="{{$payment->amount}}"
                                    data-paymentdate="{{\Carbon\Carbon::parse($payment->paid_at)->format('Y-m-d')}}">
                                Edit
                            </button>
                        </td>
                        <td>
                            <a href="{{url('calculations/deletepayment/'.$payment->id)}}" class="btn btn-danger">Ignore</a>
                        </td>
                    </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
    </div>

    <div class="has-box-shadow">
        <h6 class="panel-title">
            Pay amount
        </h6>
        <form action="{{ url('calculations/createpayment' )}}" method="post">
            <div class="row">

                <div class="col-3">
                    @csrf()
                    <div class="form-group">
                        <label for="paidAt">Date:</label>
                        <input type="text" class="form-control datepicker3" autocomplete="off" name="paidAt"
                               value="{{ old('paidAt')}}">
                    </div>
                </div>

                <div class="col-2">
                    <div class="form-group">
                      <label for="amount">Amount:</label>
                      <input type="text" class="form-control" name="amount"
                             value="{{old('amount')}}">
                    </div>
                </div>

                <div class="col-2">
                    <div class="form-group">
                      <label for="description">Description:</label>
                      <input type="text" class="form-control" name="description"
                             value="{{old('description')}}">
                    </div>
                </div>

                <input type="hidden" class="form-control" name="loanId" value="{{ session('loanid') }}">

                <div class="col-1 mt-3">
                    <button type="submit" class="btn btn-primary mt-3 float-right">Submit</button>
                </div>
            </div>
        </form>
    </div>

    <div class="has-box-shadow">
        <h5 class="text-warning panel-title">
            Statements
        </h5>

        <div class="panel-body">
            <table class="table text-left">
                <thead>
                    <tr>
                        <th>Date</th>
                        <th>Debit</th>
                        <th>Credit</th>
                        <th>Balance</th>
                        <th>Arrears</th>
                        <th>Descriptions</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach($statements as $key => $statement)
                    <tr>
                        <td>{{\Carbon\Carbon::parse($statement->created_at->date)->format('M d Y')}}</td>
                        <td>{{$statement->debit}}</td>
                        <td>{{$statement->credit}}</td>
                        <td>{{$statement->balance}}</td>
                        <td>{{$statement->arrears}}</td>
                        <td>{{$statement->comments}}
                            @if($statement->interestBreakDown)
                            <div class="target-description" data-key="{{$key}}">
                              <span class="text-primary fa fa-plus"></span>
                              <!-- <span class="text-primary fa fa-minus"></span> -->
                            </div>
                            @endif
                        </td>
                    </tr>
                        @if($statement->interestBreakDown)
                        <tr class="show-description{{$key}} font-weight-bold"  style="display: none; background-color: #efefef;">
                            <td>From</td>
                            <td>To</td>
                            <td>Days</td>
                            <td>Amount</td>
                            <td>Rate</td>
                            <td>Daily Rate</td>
                            <td>Total</td>
                        </tr>
                        @foreach($statement->interestBreakDown as $breakDown)
                        <tr class="show-description{{$key}}"  style="display: none; background-color: #efefef;">
                            <td>{{\Carbon\Carbon::parse($breakDown->start->date)->format('M d Y')}}</td>
                            <td>{{\Carbon\Carbon::parse($breakDown->end->date)->format('M d Y')}}</td>
                            <td>{{$breakDown->days}}</td>
                            <td>{{$breakDown->balance}}</td>
                            <td>{{$breakDown->rate}}</td>
                            <td>{{$breakDown->daily_rate}}</td>
                            <td>{{$breakDown->total}}</td>
                        </tr>
                        @endforeach
                        @endif
                    @endforeach
                </tbody>
            </table>
        </div>
    </div>

    <div class="has-box-shadow">
        <h6 class="panel-title">
            Payment Plan
        </h6>

        <div class="panel-body">
            <table class="table text-left">
                <thead>
                    <tr>
                        <th>Amount Due</th>
                        <th>Amount Paid</th>
                        <th>Amount Type</th>
                        <th>Status</th>
                        <th>Due Date</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach($paymentPlan as $key => $payment)
                    <tr>
                        <td>{{$payment->amount}}</td>
                        <td>{{$payment->amount_paid}}</td>
                        <td class="text-capitalize">{{$payment->type}}</td>
                        <td class="text-capitalize">{{$payment->status}}</td>
                        <td>{{$payment->status}}</td>
                        <td>{{\Carbon\Carbon::parse($payment->pay_before)->format('M d Y')}}</td>
                    </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
    </div>

@endif
<!-- End loan id check -->

<!-- Assign Charge Modal -->
<div class="modal" id="assignChargeModal">
  <div class="modal-dialog">
    <div class="modal-content">

      <!-- Modal Header -->
      <div class="modal-header">
        <h4 class="modal-title">Assign Charge</h4>
        <button type="button" class="close" data-dismiss="modal">&times;</button>
      </div>

      <!-- Modal body -->
      <div class="modal-body">
        <form action="{{'calculations/assigncharge'}}" method="post">
            @csrf()

            <input type="hidden" class="form-control" name="loanId" value="{{old('loanId')}}">
            <input type="hidden" class="form-control" name="chargeId" value="{{old('chargeId')}}">

            <div class="form-group">
                <label for="date">Date:</label>
                <input class="form-control datepicker2" autocomplete="off" name="date" value="{{old('date')}}"/>
            </div>

            <button type="submit" class="btn btn-primary">Submit</button>
        </form>
      </div>

      <!-- Modal footer -->
      <div class="modal-footer">
        <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
      </div>

    </div>
  </div>
</div>

<!-- Update Payment Modal -->
<div class="modal" id="updatePaymentModal">
  <div class="modal-dialog">
    <div class="modal-content">

      <div class="modal-header">
        <h4 class="modal-title">Edit Payment</h4>
        <button type="button" class="close" data-dismiss="modal">&times;</button>
      </div>

      <div class="modal-body">
        <form action="{{'calculations/updatepayment'}}" method="post">
            @csrf()

            <input type="hidden" class="form-control" name="paymentId" value="{{old('paymentId')}}">

            <div class="form-group">
              <label for="amount">Amount:</label>
              <input class="form-control" name="amount" value="{{old('amount')}}"/>
            </div>

            <div class="form-group">
              <label for="paidAt">Date:</label>
              <input class="form-control datepicker5" autocomplete="off" name="paidAt" value="{{old('paidAt')}}"/>
            </div>

            <button type="submit" class="btn btn-primary">Submit</button>
        </form>
      </div>

      <div class="modal-footer">
        <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
      </div>

    </div>
  </div>
</div>

<!-- Update Other Charge Modal -->
<div class="modal" id="updateOtherChargeModal">
  <div class="modal-dialog">
    <div class="modal-content">

      <div class="modal-header">
        <h4 class="modal-title">Edit Other Charge</h4>
        <button type="button" class="close" data-dismiss="modal">&times;</button>
      </div>

      <div class="modal-body">
        <form action="{{'calculations/updateothercharge'}}" method="post">
            @csrf()

            <input type="hidden" class="form-control" name="otherchargeid" value="{{old('otherchargeid')}}">

            <div class="form-group">
              <label for="name">Name:</label>
              <input class="form-control" name="name" value="{{old('name')}}"/>
            </div>

            <div class="form-group">
              <label for="value">Amount:</label>
              <input class="form-control" name="value" value="{{old('value')}}"/>
            </div>

            <select class="form-control d-none" name="chargetype">
                <option value="fixed">Fixed</option>
                <option value="percentage">Percentage</option>
            </select>

            <div class="form-group d-none">
              <label for="amounttype">Amount Type:</label>
              <input class="form-control" name="amounttype" value="{{old('amounttype')}}"/>
            </div>

            <div class="form-group">
              <label for="chargedate">Charge Date:</label>
              <input class="form-control datepicker7" autocomplete="off" name="chargedate" value="{{old('chargedate')}}"/>
            </div>

            <button type="submit" class="btn btn-primary">Submit</button>
        </form>
      </div>

      <div class="modal-footer">
        <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
      </div>

    </div>
  </div>
</div>

@endsection

@section('pages-js')
<script>
    $(document).ready(function(){
        $('.target-description').click(function() {
            var key = $(this).data('key');
          $('.show-description'+key).toggle("slide");
          $(this).find("span.fa").toggleClass( "fa-minus" );
        });
    });

    $(document).on("click", ".loanModal", function () {
        var loanId = $(this).data('loanid');
        $("input[name=loanId]").val(loanId);
    });

    $(document).on("click", ".chargeModal", function () {
        var loanId = $(this).data('loanid');
        var chargeId = $(this).data('chargeid');

        $("input[name=loanId]").val(loanId);
        $("input[name=chargeId]").val(chargeId);
    });

    $(document).on("click", ".paymentModal", function () {
        var paymentId = $(this).data('paymentid');
        var amount = $(this).data('amount');
        var paymentDate = $(this).data('paymentdate');

        $("input[name=paymentId]").val(paymentId);
        $("input[name=amount]").val(amount);
        $("input[name=paidAt]").val(paymentDate);
    });

    $(document).on("click", ".otherChargeModal", function () {
        $("input[name=otherchargeid]").val($(this).data('otherchargeid'));
        $("input[name=name]").val($(this).data('name'));
        $("input[name=value]").val($(this).data('value'));
        $("[name=chargetype]").val($(this).data('chargetype'));
        $("input[name=amounttype]").val($(this).data('amounttype'));
        $("input[name=chargedate]").val($(this).data('chargedate'));
    });

    $(document).ready(function(){
        $('.collapse').on('shown.bs.collapse', function(){
            $(this).parent().find(".fa-plus").removeClass("fa-plus").addClass("fa-minus");
        }).on('hidden.bs.collapse', function(){
            $(this).parent().find(".fa-minus").removeClass("fa-minus").addClass("fa-plus");
        });
    });

</script>
@endsection
