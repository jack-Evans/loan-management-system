<?php

namespace App\Containers\Loan\Exceptions;

use App\Ship\Parents\Exceptions\Exception;
use Symfony\Component\HttpFoundation\Response;

class LoanNotIssuedException extends Exception
{
    public $httpStatusCode = Response::HTTP_PRECONDITION_FAILED;

    public $message = 'Loan not issued yet';

    public $code = 412;
}
