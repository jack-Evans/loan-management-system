<?php

namespace App\Containers\Loan\Exceptions;

use App\Ship\Parents\Exceptions\Exception;
use Symfony\Component\HttpFoundation\Response;

class LoadAlreadyIssuedException extends Exception
{
    public $httpStatusCode = Response::HTTP_LOCKED;

    public $message = 'Loan has been already issued';

    public $code = 423;
}
