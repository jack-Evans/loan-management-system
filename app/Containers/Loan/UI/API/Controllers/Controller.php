<?php

namespace App\Containers\Loan\UI\API\Controllers;

use App\Containers\Loan\UI\API\Requests\CreateLoanRequest;
use App\Containers\Loan\UI\API\Requests\DeleteLoanRequest;
use App\Containers\Loan\UI\API\Requests\GetAllLoansRequest;
use App\Containers\Loan\UI\API\Requests\FindLoanByIdRequest;
use App\Containers\Loan\UI\API\Requests\UpdateLoanRequest;
use App\Containers\Loan\UI\API\Requests\IssueLoanRequest;
use App\Containers\Loan\UI\API\Requests\LockMonthRequest;
use App\Containers\Loan\UI\API\Requests\UpdateLoanStatusRequest;
use App\Containers\Loan\UI\API\Requests\ExportLoanRequest;
use App\Containers\Loan\UI\API\Transformers\LoanTransformer;
use App\Containers\Loan\UI\API\Transformers\DatatypeTransformer;
use App\Ship\Parents\Controllers\ApiController;
use Apiato\Core\Foundation\Facades\Apiato;
use App\Containers\Payment\UI\API\Transformers\JournalTransformer;

/**
 * Class Controller
 *
 * @package App\Containers\Loan\UI\API\Controllers
 */
class Controller extends ApiController
{
    /**
     * @param CreateLoanRequest $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function createLoan(CreateLoanRequest $request)
    {        
        $loan = Apiato::call('Loan@CreateLoanAction', [$request]);
                
        return $this->created($this->transform($loan, LoanTransformer::class));
    }

    /**
     * @param FindLoanByIdRequest $request
     * @return array
     */
    public function findLoanById(FindLoanByIdRequest $request)
    {
        $loan = Apiato::call('Loan@FindLoanByIdAction', [$request]);

        return $this->transform($loan, LoanTransformer::class);
    }

    /**
     * @param GetAllLoansRequest $request
     * @return array
     */
    public function getAllLoans(GetAllLoansRequest $request)
    {
        $loans = Apiato::call('Loan@GetAllLoansAction', [$request]);

        return $this->transform($loans, LoanTransformer::class);
    }

    /**
     * @param UpdateLoanRequest $request
     * @return array
     */
    public function updateLoan(UpdateLoanRequest $request)
    {
        $loan = Apiato::call('Loan@UpdateLoanAction', [$request]);

        return $this->transform($loan, LoanTransformer::class);
    }

    /**
     * @param DeleteLoanRequest $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function deleteLoan(DeleteLoanRequest $request)
    {
        Apiato::call('Loan@DeleteLoanAction', [$request]);

        return $this->noContent();
    }


    /**
     * @param issueLoanRequest $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function issueLoan(IssueLoanRequest $request)
    {
        $loan = Apiato::call('Loan@IssueLoanAction', [$request]);
        return $this->transform($loan, LoanTransformer::class);
    }

    /**
     * @param FindLoanByIdRequest $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function getLoanMonths(FindLoanByIdRequest $request)
    {
        $loanMonths = Apiato::call('Loan@GetLoanMonthsAction', [$request]);
        return $loanMonths;
    }

    /**
     * @param FindLoanByIdRequest $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function lockMonth(LockMonthRequest $request)
    {
        $lockMonth = Apiato::call('Loan@LockMonthAction', [$request]);
        return $this->transform($lockMonth, JournalTransformer::class);
    }

    /**
     * @param LoanStatusRequest $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function updateLoanStatus(UpdateLoanStatusRequest $request)
    {
        $loanStatus = Apiato::call('Loan@UpdateLoanStatusAction', [$request]);
        return $this->transform($loanStatus, LoanTransformer::class);
    }

    /**
     * @param CreateTransferRequest $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function exportLoans(ExportLoanRequest $request)
    {
        $fileName = Apiato::call('Loan@ExportLoansAction', [$request]);
        return $this->noContent();
    }

    /**
     * @param GetAllLoansRequest $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function getDatatype(GetAllLoansRequest $request)
    {
        $datatypes = Apiato::call('Loan@GetDatatypeAction', [$request]);
        // return $datatypes;
        // dd($datatypes);
        $data = $this->transform($datatypes, DatatypeTransformer::class);
        // dd($data['data']);
        // return $this->transform($datatypes, DatatypeTransformer::class);
        // dd($data['data']);
        return $data['data'];
    }

    /**
     * @param GetAllLoansRequest $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function monthStartingBalance(GetAllLoansRequest $request)
    {
        $fileName = Apiato::call('Loan@GetMonthStartingBalanceAction', [$request]);
        return $fileName;
    }

    /**
     * @param GetAllLoansRequest $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function verifyStatementDateStatus(GetAllLoansRequest $request)
    {
        $verifyStatementDate = \App\Containers\Loan\Models\Extras::where('name', 'VerifyStatementDate')->first();
        if(is_null($verifyStatementDate)){
            $extra = new \App\Containers\Loan\Models\Extras();
            $extra->name = 'VerifyStatementDate';
            $extra->value = 1;
            $extra->save();
        } elseif($verifyStatementDate->value == 1){
            $verifyStatementDate->value = 0;
            $verifyStatementDate->save();
        } else{
            $verifyStatementDate->value = 1;
            $verifyStatementDate->save();
        }

        return $this->noContent();
    }

    /**
     * @param GetAllLoansRequest $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function retainedLoanSummary(GetAllLoansRequest $request)
    {
        $loans = Apiato::call('Loan@GetRetainedLoanSummaryAction', [$request]);

        return $this->transform($loans, \App\Containers\Loan\UI\API\Transformers\RetainedLoanSummaryTransformer::class);
    }

    /**
     * @param GetAllLoansRequest $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function statementEndingBalance(GetAllLoansRequest $request)
    {
        $statements = Apiato::call('Loan@GetStatementEndingBalanceAction', [$request]);
        
        return $this->transform(collect($statements), \App\Containers\Loan\UI\API\Transformers\StatementEndingBalanceTransformer::class);
    }
}
