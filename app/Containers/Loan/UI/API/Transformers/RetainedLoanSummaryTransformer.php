<?php

namespace App\Containers\Loan\UI\API\Transformers;

use App\Containers\Loan\Models\Loan;
use App\Ship\Parents\Transformers\Transformer;
use Apiato\Core\Traits\HashIdTrait;

class RetainedLoanSummaryTransformer extends Transformer
{
    use HashIdTrait;
    
    /**
     * @var  array
     */
    protected $defaultIncludes = [

    ];

    /**
     * @var  array
     */
    protected $availableIncludes = [

    ];

    /**
     * @param Loan $entity
     *
     * @return array
     */
    public function transform(Loan $entity)
    {
        $response = [
            'object' => 'Loan',
            'id'            => $entity->getHashedKey(),
            'loan id'        => $entity->loan_id,
            'net amount'       => number_format($entity->net_loan, 2),
        ];
        foreach ($entity->charges as $key => $charge) {
            $value = collect($entity->loanCharge)->where('charge_id', $charge->id)->first()->value;
            $value = number_format($value, 2);
            if(strtolower($charge->name) == 'arrangement fee')
                $response['arrangement fee'] = $value;
            elseif(strtolower($charge->name) == 'admin fee')
                $response['admin fee'] = $value;
            elseif(strtolower($charge->name) == 'broker fee')
                $response['broker fee'] = $value;
        }

        $response['gross amount'] = number_format($entity->interest->gross_loan, 2);
        $monthlyPmt = resolvePercentage($entity->interest->rate) * $entity->interest->gross_loan;
        $retainedInterest = number_format($entity->interest->duration * $monthlyPmt, 2);
        $response['retained interest'] = $retainedInterest;
        $response['start date']   = $entity->issue_at->format('d/m/Y');
        $response['end date']   = addDecimalMonthsIntoDate($entity->interest->duration, $entity->issue_at)->copy()->subDay()->format('d/m/Y');
        $response['duration']   = $entity->interest->duration;

        $response = $this->ifAdmin([
            'real_id'    => $entity->id,
            // 'deleted_at' => $entity->deleted_at,
        ], $response);

        return $response;
    }
}
