<?php

namespace App\Containers\Loan\UI\API\Transformers;

use App\Containers\Loan\Models\Loan;
use App\Ship\Parents\Transformers\Transformer;
use Apiato\Core\Traits\HashIdTrait;

class DatatypeTransformer extends Transformer
{
    use HashIdTrait;
    
    /**
     * @var  array
     */
    protected $defaultIncludes = [

    ];

    /**
     * @var  array
     */
    protected $availableIncludes = [

    ];

    /**
     * @param Loan $entity
     *
     * @return array
     */
    public function transform(Loan $entity)
    {
        $response = [];
        $response['loan_real_id']   = $entity->id;
        foreach ($entity->options as $key => $option) {
            // $name = strtolower(preg_replace('/\s+/', ' ', trim($option->name)));
            // $value = preg_replace('/\s+/', ' ', trim($option->value));
            // if($name == 'sage ref' || $name == 'sage user id' || $name == 'kbl borrower id ref (sage)' || $name == 'kuflink borrower id ref')
            //     $response['sage ref'] = $value;
            // elseif($name == 'kuflink customer reference' || $name == 'kl deal id')
            //     $response['kuflink customer reference'] = $value;
            // elseif($name == 'property type' || $name == 'security type')
            //     $response['property type'] = $value;
            // elseif($name == 'orig contractual maturity date' || $name == 'contractual maturity date')
            //     $response['orig contractual maturity date'] = $value;
            // elseif($name == 'total debt - orig gross' || $name == 'kuflink total debt' || $name == 'kuflink total debt(gross)')
            //     $response['total debt - orig gross'] = $value;
            // elseif($name == 'ltv orig gross vs omv' || $name == 'ltv vs omv (gross)' || $name == 'ltv')
            //     $response['ltv orig gross vs omv'] = $value;
            // elseif($name == 'property value available equity' || $name == 'property value - available equity')
            //     $response['property value available equity'] = $value;
            // else
                // $response[strtolower(trim($name))] = $value;
            $response[str_replace(".","",trimedLowerStr($option->name))] = $option->value;
        }
        // $response['borrower name'] = $entity->loan_id;
        $response['loan id']   = $entity->loan_id;
        $response['loan type']   = $entity->loan_type;
        // $response['type']        = $entity->type;
        $response['Loan status'] = ($entity->status == 'closed') ? "SETTLED" : "OPEN";
        $response['holidayrefund'] = $entity->holiday_refund;
        $response['completion date']  = addDecimalMonthsIntoDate($entity->interest->duration, $entity->issue_at)->subDay()->format('d/m/Y');
        $response['contractual term in months'] = $entity->interest->duration;
        $response['agreed extended repayment date'] = ($entity->renewalCharge->last()) ? $entity->renewalCharge->last()->renew_end_date->format('d/m/Y') : '';
        $response['number of extensions'] = count($entity->renewalCharge);

        $response['monthly interest rate'] = $entity->interest->rate;
        $response['original gross loan amount'] = number_format($entity->interest->gross_loan, 2);
        $response['original principal loan amount'] = number_format($entity->net_loan, 2);
        $response['monthly payment due'] = number_format(calculateMonthlyPMT($entity->interest->rate, $entity->interest->gross_loan), 2);
// dd(collect($entity->options)->where('name', "Loan status")->first());

        foreach ($entity->charges as $key => $charge) {
            $response[strtolower(trim($charge->name))]=$charge->pivot->value;
        }

        // dd($response);
        $response = $this->ifAdmin([
            'real_id'    => $entity->id,
            // 'deleted_at' => $entity->deleted_at,
        ], $response);

        return $response;
    }
}
