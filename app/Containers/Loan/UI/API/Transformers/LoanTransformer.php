<?php

namespace App\Containers\Loan\UI\API\Transformers;

use App\Containers\Loan\Models\Loan;
use App\Ship\Parents\Transformers\Transformer;
use Apiato\Core\Traits\HashIdTrait;

class LoanTransformer extends Transformer
{
    use HashIdTrait;
    
    /**
     * @var  array
     */
    protected $defaultIncludes = [

    ];

    /**
     * @var  array
     */
    protected $availableIncludes = [

    ];

    /**
     * @param Loan $entity
     *
     * @return array
     */
    public function transform(Loan $entity)
    {
        $response = [
            'object' => 'Loan',
            'id'            => $entity->getHashedKey(),
            'loanid'        => $entity->loan_id,
            'customerid'    => $this->encode($entity->customer_id),
            'netloan'       => $entity->net_loan,
            'loantype'      => $entity->loan_type,
            'type'          => $entity->type,
            'status'        => $entity->status,
            'issueat'       => $entity->issue_at,
            'holidayrefund' => $entity->holiday_refund,
            // 'statementdate' => $entity->statement_date,
            'monthlyPMT'    => round(calculateMonthlyPMT($entity->interest->rate, $entity->interest->gross_loan), 2),
            'created_at'    => $entity->created_at,
            'updated_at'    => $entity->updated_at,
            'charges'       => $entity->charges,
            'interest'      => $entity->interest,
            'customer'      => $entity->customer,
            'loanextras'    => $entity->loanextras,
            'options'       => $entity->options,
            'tranch'        => $entity->tranch,
            'fileinfo'      => $entity->fileInfo,
            'tags'          => $entity->tags,
            'renewalCharge' => $entity->renewalCharge,
            'grace'         => $entity->grace,
        ];

        $response = $this->ifAdmin([
            'real_id'    => $entity->id,
            // 'deleted_at' => $entity->deleted_at,
        ], $response);

        return $response;
    }
}
