<?php

namespace App\Containers\Loan\UI\API\Transformers;

use App\Containers\Payment\Models\Statement;
use App\Ship\Parents\Transformers\Transformer;
use Carbon\Carbon;

class StatementEndingBalanceTransformer extends Transformer
{
    /**
     * @var  array
     */
    protected $defaultIncludes = [

    ];

    /**
     * @var  array
     */
    protected $availableIncludes = [

    ];

    /**
     * @param Post $entity
     *
     * @return array
     */
    public function transform(Statement $entity)
    {
        $response = [
            'object' => 'Statements',
            'sage id' => $entity->sageId,
            'customer name' => $entity->name,
            'loan type' => $entity->loan_type,
            'balance' => number_format($entity->balance, 2),
            'expiry date balance' => number_format($entity->expiryDateBalance, 2),
            // 'description' => $entity->comments,
            'calculation date' => $entity->created_at->format('d/m/Y'),
            'start date' => $entity->issue_at,
            'expiry date' => $entity->expiry_date,
            'calculated interest' => number_format($entity->totalInterest, 2),
            'interest days' => $entity->interestDays,
            'interest amount' => number_format($entity->interestAmount, 2),
            'default interest days' => $entity->defaultInterestDays,
            'default interest amount' => number_format($entity->defaultInterestAmount, 2),

        ];

        $response = $this->ifAdmin([
            'real_id'    => $entity->id,
            // 'deleted_at' => $entity->deleted_at,
        ], $response);

        return $response;
    }
}
