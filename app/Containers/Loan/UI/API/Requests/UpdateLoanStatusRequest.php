<?php

namespace App\Containers\Loan\UI\API\Requests;

use App\Ship\Parents\Requests\Request;

/**
 * Class UpdateLoanStatusRequest.
 */
class UpdateLoanStatusRequest extends Request
{

    /**
     * The assigned Transporter for this Request
     *
     * @var string
     */
    protected $transporter = \App\Containers\Loan\Data\Transporters\FindLoanByIdTransporter::class;

    /**
     * Define which Roles and/or Permissions has access to this request.
     *
     * @var  array
     */
    protected $access = [
        'permissions' => '',
        'roles'       => '',
    ];

    /**
     * Id's that needs decoding before applying the validation rules.
     *
     * @var  array
     */
    protected $decode = [
        'loanId',
    ];

    /**
     * Defining the URL parameters (e.g, `/user/{id}`) allows applying
     * validation rules on them and allows accessing them like request data.
     *
     * @var  array
     */
    protected $urlParameters = [
        'id',
    ];

    /**
     * @return  array
     */
    public function rules()
    {
        return [
            'loanId'           => 'required|integer|exists:loans,id',
            'status'           => 'required|in:approved,processing,rejected,freezed,closed',
            'updateDate'       => 'date_format:Y-m-d',
        ];
    }

    /**
     * @return  bool
     */
    public function authorize()
    {
        return $this->check([
            'hasAccess',
        ]);
    }
}
