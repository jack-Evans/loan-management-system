<?php

namespace App\Containers\Loan\UI\API\Requests;

use App\Ship\Parents\Requests\Request;

/**
 * Class CreateLoanRequest.
 */
class CreateLoanRequest extends Request
{

    /**
     * The assigned Transporter for this Request
     *
     * @var string
     */
    protected $transporter = \App\Containers\Loan\Data\Transporters\CreateLoanTransporter::class;

    /**
     * Define which Roles and/or Permissions has access to this request.
     *
     * @var  array
     */
    protected $access = [
        'permissions' => '',
        'roles'       => '',
    ];

    /**
     * Id's that needs decoding before applying the validation rules.
     *
     * @var  array
     */
    protected $decode = [
        'customerId',
    ];

    /**
     * Defining the URL parameters (e.g, `/user/{id}`) allows applying
     * validation rules on them and allows accessing them like request data.
     *
     * @var  array
     */
    protected $urlParameters = [
        // 'id',
    ];

    /**
     * @return  array
     */
    public function rules()
    {
        return [
            'loanId'            => 'required|max:50|string|unique:loans,loan_id',
            'netloan'           => 'required|regex:/^\d*(\.\d{1,2})?$/',
            'grossloan'         => 'required_if:loanType,retained|required_if:loanType,half',
            'duration2'         => 'required_if:loanType,half',
            // 'chargeId'          => 'required_if:loanType,retained',
            // 'initialChargeValue'=> 'required_if:loanType,retained',
            'loanType'          => 'required|in:retained,serviced,manual,half,rolled,tranch,n-tranch',
            // 'status'            => 'in:approved,processing,rejected',
            'rate'              => 'required|regex:/^\d*(\.\d{1,2})?$/',
            'defaultRate'       => 'required|different:rate|regex:/^\d*(\.\d{1,2})?$/',
            'duration'          => 'required|regex:/^\d*(\.\d{1,2})?$/',
            'minTerm'           => 'integer|min:0',
            'customerId'        => 'required|integer|min:1',
            'issueAt'           => 'required|date_format:Y-m-d',
            'description'       => 'required|string|min:3|max:100',
            'holidayRefund'     => 'required',
        ];
    }

    /**
     * @return  bool
     */
    public function authorize()
    {
        return $this->check([
            'hasAccess',
        ]);
    }
}
