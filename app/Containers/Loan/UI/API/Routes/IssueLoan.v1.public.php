<?php

/**
 * @apiGroup           Loan
 * @apiName            issueLoan
 *
 * @api                {POST} /v1/ Endpoint title here..
 * @apiDescription     Endpoint description here..
 *
 * @apiVersion         1.0.0
 * @apiPermission      none
 *
 * @apiParam           {String}  parameters here..
 *
 * @apiSuccessExample  {json}  Success-Response:
 * HTTP/1.1 200 OK
{
  // Insert the response of the request here...
}
 */

/** @var Route $router */
$router->post('issueloan/{id}', [
    'as' => 'api_loan_issue_loan',
    'uses'  => 'Controller@issueLoan',
    'middleware' => [
      'auth:api',
    ],
]);
