<?php

/**
 * @apiGroup           Loan
 * @apiName            datatype
 *
 * @api                {GET} /v1/datatype/:id Endpoint title here..
 * @apiDescription     Endpoint description here..
 *
 * @apiVersion         1.0.0
 * @apiPermission      none
 *
 * @apiParam           {String}  parameters here..
 *
 * @apiSuccessExample  {json}  Success-Response:
 * HTTP/1.1 200 OK
{
  // Insert the response of the request here...
}
 */

/** @var Route $router */
$router->get('retained-loan-summary', [
    'as' => 'api_get_data_type',
    'uses'  => 'Controller@retainedLoanSummary',
    'middleware' => [
      'auth:api',
    ],
]);
