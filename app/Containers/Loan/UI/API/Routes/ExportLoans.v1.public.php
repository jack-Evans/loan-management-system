<?php

/**
 * @apiGroup           exportLoans
 * @apiName            exportLoans
 *
 * @api                {PATCH} /v1/statements/:id Endpoint title here..
 * @apiDescription     Endpoint description here..
 *
 * @apiVersion         1.0.0
 * @apiPermission      none
 *
 * @apiParam           {String}  parameters here..
 *
 * @apiSuccessExample  {json}  Success-Response:
 * HTTP/1.1 200 OK
{
  // Insert the response of the request here...
}
 */

/** @var Route $router */
$router->post('exportloans', [
    'as' => 'api_statement_export_loans',
    'uses'  => 'Controller@exportLoans',
    'middleware' => [
      'auth:api',
    ],
]);
