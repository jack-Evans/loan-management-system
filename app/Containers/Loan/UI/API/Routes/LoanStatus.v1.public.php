<?php

/**
 * @apiGroup           Loan
 * @apiName            updateloanstatus
 *
 * @api                {POST} /v1/loans Endpoint title here..
 * @apiDescription     Endpoint description here..
 *
 * @apiVersion         1.0.0
 * @apiPermission      none
 *
 * @apiParam           {String}  parameters here..
 *
 * @apiSuccessExample  {json}  Success-Response:
 * HTTP/1.1 200 OK
{
  // Insert the response of the request here...
}
 */

/** @var Route $router */
$router->post('updateloanstatus', [
    'as' => 'api_loan_status',
    'uses'  => 'Controller@updateLoanStatus',
    'middleware' => [
      'auth:api',
    ],
]);
