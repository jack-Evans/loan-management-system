<?php

/**
 * @apiGroup           Loan
 * @apiName            updateLoan
 *
 * @api                {PATCH} /v1/loans/:id Endpoint title here..
 * @apiDescription     Endpoint description here..
 *
 * @apiVersion         1.0.0
 * @apiPermission      none
 *
 * @apiParam           {String}  parameters here..
 *
 * @apiSuccessExample  {json}  Success-Response:
 * HTTP/1.1 200 OK
{
  // Insert the response of the request here...
}
 */

/** @var Route $router */
$router->patch('loans/{id}', [
    'as' => 'api_loan_update_loan',
    'uses'  => 'Controller@updateLoan',
    'middleware' => [
      'auth:api',
    ],
]);
