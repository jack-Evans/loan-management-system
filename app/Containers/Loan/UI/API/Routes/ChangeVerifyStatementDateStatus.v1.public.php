<?php

/**
 * @apiGroup           Loan
 * @apiName            loanmonths
 *
 * @api                {GET} /v1/loans/:id Endpoint title here..
 * @apiDescription     Endpoint description here..
 *
 * @apiVersion         1.0.0
 * @apiPermission      none
 *
 * @apiParam           {String}  parameters here..
 *
 * @apiSuccessExample  {json}  Success-Response:
 * HTTP/1.1 200 OK
{
  // Insert the response of the request here...
}
 */

/** @var Route $router */
$router->get('verify-statement-date', [
    'as' => 'api_get_loan_months',
    'uses'  => 'Controller@verifyStatementDateStatus',
    'middleware' => [
      'auth:api',
    ],
]);
