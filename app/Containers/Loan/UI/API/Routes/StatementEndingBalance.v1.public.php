<?php

$router->get('statement-ending-balance', [
    'as' => 'api_statement_month_end_balance',
    'uses'  => 'Controller@statementEndingBalance',
    'middleware' => [
      'auth:api',
    ],
]);
