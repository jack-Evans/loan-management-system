@extends('layouts.app')

@section('title', 'Loans')

@section('wrapper1')

@if(isset($loans))
<div class="main-wrapper">
  <div class="container">
    <div class="row">
      <div class="col-lg-12">
        <div class="has-bg js-importErrorsAll">
          @if(session()->has('importResponse'))
            @php 
              $message = session()->get('importResponse');
              $success = $message->success;
              $errors = $message->errors;
              $ignored = $message->ignored;
            @endphp
            
            @if(count($success))
              <div class="row">
                @foreach($success as $msg)
                  <div class="col-3">
                    <h5>{{ $msg->file }}</h5>
                    @foreach($msg->message as $sc)
                      <p class="text-success mb-0">{{$sc}}</p>
                    @endforeach
                  </div>
                @endforeach
              </div>
            @endif

            @if(count($ignored))
              <div class="row">
                <div class="col-3">
                  <h5>Ignored Hidden Sheets</h5>
                  @foreach($ignored as $msg)
                    <p class="text-info mb-0">{{$msg}}</p>
                  @endforeach
                </div>
              </div>
            @endif

            @if(count($errors))
              <div class="row">
                @foreach($errors as $msg)
                  <div class="col-3">
                    <h5>{{ $msg->file }}</h5>
                    @foreach($msg->message as $err)
                      <p class="text-info mb-0">{{$err}}</p>
                    @endforeach
                  </div>
                @endforeach
              </div>
            @endif

          @endif
        </div>
      </div>
    </div>
  </div>

  <div class="container">
    <div class="row">
      <div class="col-lg-12">
        <div class="has-bg">
          <h3>Loans
            <div class="target-description" data-key="Loans">
              <span class="text-primary fas fa-chevron-up cursor"></span>
            </div>
          </h3>
          <div class="show-descriptionLoans hidden">
            <div class="grid-wrapper">
              <div id="myGrid" class="ag-theme-material" style="height: 1000px;width:100%;"></div>
            </div>
          </div>
          <table class="table table-striped table-hover table-bordered no-mar-b text-left MDBootstrapDatatable" cellspacing="0" width="100%">
          <!-- <table class="table table-striped table-bordered table-sm MDBootstrapDatatable" cellspacing="10"> -->
            <thead>
              <tr>
                <th class="th-sm">Loan Id</th>
                <th class="th-sm">Issue Date</th>
                <th class="th-sm">Net Loan</th>
                <th class="th-sm">Gross Loan</th>
                <th class="th-sm">Loan Type</th>
                <!-- <th class="th-sm">Status</th> -->
                <th class="th-sm">Tags</th>
                <!-- <th class="th-sm">Holiday refund</th> -->
                <th class="th-sm">Last File Import Date</th>
                <!-- <th class="th-sm">No. Of Transactions (file/system)</th>
                <th class="th-sm">Ending Balance (file/system)</th>
                <th class="th-sm">Calculation Difference</th>
                <th class="th-sm">Interests (file/system)</th>
                <th class="th-sm">Interest Days (file/system)</th>
                <th class="th-sm">Default Interests (file/system)</th>
                <th class="th-sm">Default Interest Days (file/system)</th> -->
                <th></th>
              </tr>
            </thead>
            <tbody>
              @foreach($loans as $key => $loan)
              <!-- if($loan->loantype === 'serviced') -->
              <tr>
                <td><a href="{{url('calculations?loanid='.$loan->id)}}">{{$loan->loanid}}</a></td>
                <td>{{\Carbon\Carbon::parse($loan->issueat->date)->format('Y M d')}}</td>
                <td>{{number_format($loan->netloan, 2)}}</td>
                <td>{{number_format($loan->interest->gross_loan, 2)}}</td>
                <td class="text-capitalize">{{$loan->loantype}}</td>
                <!-- <td class="text-capitalize">{{$loan->status}}  -->
                <td class="text-capitalize">
                  @php echo $loanTags = implode(',', array_map(function($tag) { return $tag->name; }, $loan->tags)); @endphp
                  <i class="fas fa-edit cursor loanTagModal" data-toggle="modal" data-target="#loanTagModal" data-loanid="{{$loan->id}}" data-tags="{{$loanTags}}" aria-hidden="true"></i>
                </td>
               <!-- {{-- <td class="text-capitalize">{{$loan->type}} 
                    <i class="fas fa-edit cursor editTypeModal" data-toggle="modal" data-target="#editTypeModal" data-loanid="{{$loan->id}}" data-type="{{$loan->type}}" aria-hidden="true"></i>
                </td> --}} -->
                <!-- {{-- <td>{{ ($loan->holidayrefund == 1) ? 'Yes': 'No'}}</td> --}} -->
                <!-- File info display -->
                @php 
                $file = collect($loan->fileinfo)->where('type', 'file')->last(); 
                $system = collect($loan->fileinfo)->where('type', 'system')->last(); 
                @endphp
                <td>{{isset($file->create_date) ? \Carbon\Carbon::parse($file->create_date)->format('Y M d') : ''}}</td>
                <!-- <td>{{isset($file->no_of_transactions) ? $file->no_of_transactions : ''}}/{{isset($system->no_of_transactions) ? $system->no_of_transactions : ''}}</td>
                <td>{{isset($file->end_balance) ? number_format($file->end_balance, 2) : ''}}/{{isset($system->end_balance) ? number_format($system->end_balance, 2) : ''}}</td>
                <td class="
                @if(isset($file->calculation_difference) && $file->calculation_difference < -1) text-primary
                @elseif(isset($file->calculation_difference) && $file->calculation_difference > 1) text-danger
                @else text-success
                @endif
                ">
                {{isset($file->calculation_difference) ? number_format($file->calculation_difference, 2) : ''}}</td>
                <td>{{isset($file->total_interest) ? number_format($file->total_interest, 2) : 0}}/{{isset($system->total_interest) ? number_format($system->total_interest, 2) : 0}}</td>
                <td>{{isset($file->interest_days) ? $file->interest_days : 0}}/{{isset($system->interest_days) ? $system->interest_days : 0}}</td>
                <td>{{isset($file->default_interest) ? number_format($file->default_interest, 2) : 0}}/{{isset($system->default_interest) ? number_format($system->default_interest, 2) : 0}}</td>
                <td>{{isset($file->default_interest_days) ? $file->default_interest_days : 0}}/{{isset($system->default_interest_days) ? $system->default_interest_days : 0}}</td> -->
                
                <td>
                  <a href="{{url('calculations/deleteloan/'.$loan->id)}}" class="btn btn-danger ml-1 delete-loan float-right" >Delete</a> 
                  <button type="button" class="btn btn-primary ml-1 float-right updateLoanModal" data-toggle="modal"
                    data-target="#updateLoanModal" data-loanid="{{$loan->id}}" data-name="{{$loan->loanid}}">
                    Edit Name
                  </button>
                  <a href="{{url('calculations?loanid='.$loan->id)}}" class="btn btn-info ml-1 float-right" >
                      View
                  </a>
                </td>
              </tr>
              <!-- endif -->
              @endforeach
            </tbody>
          </table>

          <h6 class="text-primary cursor" data-toggle="modal" data-target="#queueJobsModal">Remaining Queue Jobs: <span class="queue-count">Loading...</span> </h6>

        </div>
      </div>
    </div>
  </div>
@endif
@endsection

    @section('content')
    <div class="col-lg-12">
        <div class="has-bg button_holder">
            <div class="left-side-button">
                <button type="button" class="btn btn-primary ml-1 createnewLoanModal" data-toggle="modal" data-target="#createnewLoanModal">
                  Create Service Loan
                </button>
                
                <button type="button" class="btn btn-primary ml-1 createRetainedLoanModal" data-toggle="modal" data-target="#createRetainedLoanModal">
                  Create Retain Loan
                </button>

                <button type="button" class="btn btn-primary ml-1 createHalfLoanModal" data-toggle="modal" data-target="#createRetainedLoanModal">
                  Create Half Half Loan
                </button>

                <button type="button" class="btn btn-primary ml-1 createNTranchLoanModal" data-toggle="modal" data-target="#createNTranchLoanModal">
                  Create N-Tranche Loan
                </button>

                <button type="button" class="btn btn-primary ml-1 createManualLoanModal" data-toggle="modal" data-target="#createManualLoanModal">
                  Create Manual Loan
                </button>

                <button type="button" class="btn btn-primary ml-1 importLoanModal" data-toggle="modal" data-target="#importLoanModal">
                  Import Loans
                </button>

                <button type="button" class="btn btn-primary ml-1 exportLoanModal" data-toggle="modal" data-target="#exportLoanModal">
                  Export Loans
                </button>

                <button type="button" class="btn btn-primary ml-1 exportFileinfoModal" data-toggle="modal" data-target="#exportFileinfoModal">
                  Export File Info
                </button>

                <button type="button" class="btn btn-primary ml-1 monthStatementsPdfModal" data-toggle="modal" data-target="#monthStatementsPdfModal">
                  Generate Month Statements
                </button>

                <button type="button" class="btn btn-primary ml-1 compareBalanceModal" data-toggle="modal" data-target="#compareBalanceModal">
                  Compare month ending balances
                </button>

                <!-- <a type="button" class="btn btn-primary ml-1" href="{{url('month-starting-balance')}}">
                  Month Starting Balance
                </a> -->

                <a type="button" class="btn btn-primary ml-1" href="{{url('month-end-balance')}}">
                  Month End Balance
                </a>
            </div>
        </div>
    </div>
  <!-- Update Loan Modal -->
  <div class="modal" id="updateLoanModal" data-backdrop="static">
    <div class="modal-dialog">
      <div class="modal-content">
        <div class="modal-body custom_modal">
          <div class="row">
            <div class="col-lg-1">
              <div class="left-side">
              </div>
            </div>
            <div class="col-lg-11">
              <form action="{{'calculations/updateloan'}}" method="post">
                <h4 class="modal-title">Update Loan Name</h4>
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                  @csrf()
                  <input type="hidden" class="form-control" name="loanId" >
                  <div class="form-group">
                    <label for="netloan">Loan Name</label>
                    <input class="form-control" name="loanName" value="{{old('loanName')}}" />
                  </div>
                  <button type="submit" class="custom-padding pull-right btn btn-primary">Submit</button>
              </form>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
  <!-- import Loan Modal -->
    <div class="modal" id="importLoanModal" data-backdrop="static">
      <div class="modal-dialog" style="max-width: 1000px">
        <div class="modal-content">
          <div class="modal-body custom_modal">
            <div class="row">
              <div class="col-lg-1">
                <div class="left-side">
                </div>
              </div>
              <div class="col-lg-11">
                <form action="{{ url('calculations/importloans' )}}" method="post" enctype="multipart/form-data" class="js-importForm">
                  <h4 class="modal-title">Import Loans<button type="button" class="close" data-dismiss="modal">&times;</button></h4>
                  <div class="row">
                    @csrf()
                    <div class="col-6">
                      <div class="form-group">
                        <label for="file">Choose csv/excel file:</label>
                        <input type="file" class="form-control" name="file">
                      </div>
                    </div>
                    <div class="col-6">
                      <div class="form-group">
                        <label for="file">Choose from Sharepoint:</label>
                        <button type="button" class="button" onclick="launchOneDrivePicker()">Choose File</button>
                        <input type="hidden" class="form-control" name="oneDriveFile">
                        <input type="text" class="form-control" name="oneDriveFileName">
                      </div>
                    </div>
                    <div class="col-4">
                      <div class="form-group">
                        <label for="file">Leave unchecked for test run</label>
                        <label class="custom-radio mb-0">
                          <input class="" type="hidden" name="isImport" value="0">
                            <input class="" type="checkbox" name="isImport" value="1">
                            <span class="checkmark"></span>
                          </label>
                      </div>
                    </div>
                    <div class="col-4">
                      <div class="form-group">
                        <label for="uploadAtSharepoint">Backup</label>
                        <label class="custom-radio mb-0">
                          <input class="" type="hidden" name="uploadAtSharepoint" value="0">
                            <input class="" type="checkbox" name="uploadAtSharepoint" value="1">
                            <span class="checkmark"></span>
                          </label>
                      </div>
                    </div>

                    <div class="col-4">
                      <div class="form-group">
                        <label for="byQueue">Upload By Queue</label>
                        <label class="custom-radio mb-0">
                          <input class="" type="hidden" name="byQueue" value="0">
                            <input class="" type="checkbox" name="byQueue" value="1">
                            <span class="checkmark"></span>
                          </label>
                      </div>
                    </div>
                    <p>&nbsp;</p>
                    <div class="col-lg-12">
                      <p class="text-danger js-importError"></p>
                    </div>
                    <div class="col-6 offset-5">
                      <div class="loader hide"></div>
                    </div> 
                    <div class="col-lg-12">
                      <button type="submit" class="btn pull-right btn-primary custom-padding mt-3 js-importBtn">Import</button>
                    </div>
                  </div>
                </form>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>

  <!-- export Loan Modal -->
    <div class="modal" id="exportLoanModal" data-backdrop="static">
      <div class="modal-dialog" style="max-width: 1200px">
        <div class="modal-content">
          <div class="modal-body custom_modal">
            <div class="row">
              <div class="col-lg-1">
                <div class="left-side">
                </div>
              </div>
              <div class="col-lg-11">
                <form action="{{url('loans/exportloans')}}" method="post">
                  <h4 class="modal-title">Export Loans<button type="button" class="close" data-dismiss="modal">&times;</button></h4>
                  <div class="row">
                    @csrf()
                    <div class="col-4">
                      <p>Select export loans:</p>
                    </div>
                    <div class="col-4">
                      <p>OR Select loans by tags:</p>
                    </div>
                    <div class="col-2">
                      <p>Backup</p>
                    </div>
                    <div class="col-2">
                      <p>Export Ignored Sheets</p>
                    </div>

                    <div class="col-4">
                      <div class="form-group">
                          <select class="form-control" id="multiSelect" name="loanIds[]" multiple>
                            @foreach($loans as $key => $loan)
                              <option value="{{$loan->id}}">{{$loan->loanid}}</option>
                            @endforeach
                          </select>
                      </div>
                    </div>
                    <div class="col-4">
                      <div class="form-group">
                        <select class="js-loan-tags" name="tagIds[]" multiple="multiple" style="width: 30%;">
                          @foreach($tags as $tag)
                          <option value="{{$tag->id}}">{{$tag->name}}</option>
                          @endforeach
                        </select>
                      </div>
                    </div>
                    <div class="col-2">
                      <div class="form-group">
                        <label class="custom-radio mb-0">
                          <input class="" type="hidden" name="uploadAtSharepoint" value="0">
                            <input class="" type="checkbox" name="uploadAtSharepoint" value="1">
                            <span class="checkmark"></span>
                          </label>
                      </div>
                    </div>
                    <div class="col-2">
                      <div class="form-group">
                        <label class="custom-radio mb-0">
                          <input class="" type="hidden" name="ignoredSheets" value="0">
                            <input class="" type="checkbox" name="ignoredSheets" value="1">
                            <span class="checkmark"></span>
                          </label>
                      </div>
                    </div>

                    <div class="col-3">
                      <p>Interest calculation date</p>
                    </div>

                    <div class="col-3">
                      <input class="form-control datepicker20" autocomplete="off" name="date" value="{{ old('date')}}">
                    </div>

                    <div class="col-lg-12">
                      <button type="submit" class="btn pull-right btn-primary custom-padding mt-3">Export</button>
                    </div>
                  </div>
                </form>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>

    <!-- export Loan Modal -->
    <div class="modal" id="monthStatementsPdfModal" data-backdrop="static">
      <div class="modal-dialog" style="max-width: 1200px">
        <div class="modal-content">
          <div class="modal-body custom_modal">
            <div class="row">
              <div class="col-lg-1">
                <div class="left-side">
                </div>
              </div>
              <div class="col-lg-11">
                <form action="{{url('monthstatements')}}" method="post">
                  <h4 class="modal-title">Generate Month Statements<button type="button" class="close" data-dismiss="modal">&times;</button></h4>
                  <div class="row">
                    @csrf()
                    <div class="col-4">
                      <p>Select Statement Loans:</p>
                    </div>
                    <div class="col-4">
                      <p>OR Select loans by tags</p>
                    </div>
                    <div class="col-4">
                      <p>Statement Month</p>
                    </div>
                    <div class="col-4">
                      <div class="form-group">
                          <select class="form-control" id="multiSelect2" name="loanId[]" multiple>
                            @foreach($loans as $key => $loan)
                              <option value="{{$loan->id}}">{{$loan->loanid}}</option>
                            @endforeach
                          </select>
                      </div>
                    </div>
                    <div class="col-4">
                      <div class="form-group">
                          <select class="js-loan-tags" name="tagIds[]" multiple="multiple" style="width: 30%;">
                            @foreach($tags as $tag)
                            <option value="{{$tag->id}}">{{$tag->name}}</option>
                            @endforeach
                          </select>
                      </div>
                    </div>
                    <div class="col-4">
                      <div class="form-group">
                          <input class="form-control datepicker5" autocomplete="off" name="date"
                                value="{{ old('date')}}">
                        </div>
                    </div>
                    <div class="col-lg-12">
                      <button type="submit" class="btn pull-right btn-primary custom-padding mt-3">Generate Statements</button>
                    </div>
                  </div>
                </form>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>

    <!-- Compare Balance Modal -->
    <div class="modal" id="compareBalanceModal" data-backdrop="static">
      <div class="modal-dialog" style="max-width: 1200px">
        <div class="modal-content">
          <div class="modal-body custom_modal">
            <div class="row">
              <div class="col-lg-1">
                <div class="left-side">
                </div>
              </div>
              <div class="col-lg-11">
                <form action="{{url('compare-month-balances')}}" method="post" enctype="multipart/form-data">
                  <h4 class="modal-title">Compare month ending balances<button type="button" class="close" data-dismiss="modal">&times;</button></h4>
                  <div class="row">
                    @csrf()

                    <div class="col-6">
                      <div class="form-group">
                        <label for="file">Choose csv/excel file:</label>
                        <input type="file" class="form-control" name="file">
                      </div>
                    </div>
                    
                    <div class="col-lg-12">
                      <button type="submit" class="btn pull-right btn-primary custom-padding mt-3">Submit</button>
                    </div>
                  </div>
                </form>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>

    <!-- export fileinfo Modal -->
    <div class="modal" id="exportFileinfoModal" data-backdrop="static">
      <div class="modal-dialog" style="max-width: 1200px">
        <div class="modal-content">
          <div class="modal-body custom_modal">
            <div class="row">
              <div class="col-lg-1">
                <div class="left-side">
                </div>
              </div>
              <div class="col-lg-11">
                <form action="{{url('loans/exportfileinfo')}}" method="post">
                  <h4 class="modal-title">Export File Info<button type="button" class="close" data-dismiss="modal">&times;</button></h4>
                  <div class="row">
                    @csrf()
                    <div class="col-4">
                      <p>Select file info loans:</p>
                    </div>
                    <div class="col-4">
                      <p>OR Select loans by tags:</p>
                    </div>
                    <div class="col-4">
                      <p>Backup</p>
                    </div>
                    <div class="col-4">
                      <div class="form-group">
                        <!-- <div class="js-export-loans"> -->
                          <select class="form-control" id="multiSelect1" name="loanIds[]" multiple>
                            @foreach($loans as $key => $loan)
                              <option value="{{$loan->id}}">{{$loan->loanid}}</option>
                            @endforeach
                          </select>
                      </div>
                    </div>
                    <div class="col-4">
                      <div class="form-group">
                        <select class="js-loan-tags" name="tagIds[]" multiple="multiple" style="width: 30%;">
                          @foreach($tags as $tag)
                          <option value="{{$tag->id}}">{{$tag->name}}</option>
                          @endforeach
                        </select>
                      </div>
                    </div>
                    <div class="col-4">
                      <div class="form-group">
                        <label class="custom-radio mb-0">
                          <input class="" type="hidden" name="uploadAtSharepoint" value="0">
                            <input class="" type="checkbox" name="uploadAtSharepoint" value="1">
                            <span class="checkmark"></span>
                          </label>
                      </div>
                    </div>
                    <div class="col-lg-12">
                      <button type="submit" class="btn pull-right btn-primary custom-padding mt-3">Export</button>
                    </div>
                  </div>
                </form>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>

    <!-- create Serviced loan modal -->
    <div class="modal" id="createnewLoanModal" data-backdrop="static">
      <div class="modal-dialog" style="max-width: 800px">
        <div class="modal-content">
          <div class="modal-body custom_modal">
            <div class="row">
              <div class="col-lg-1">
                <div class="left-side">
                </div>
              </div>
              <div class="col-lg-11">
                <form action="{{ url('calculations/store' )}}" method="post">
                  <h4 class="modal-title">Create Serviced Loan<button type="button" class="close" data-dismiss="modal">&times;</button></h4>
                    <div class="row">
                      <div class="col-6">
                        @csrf()
                        <div class="form-group">
                          <label for="loanid">Loan Id:</label>
                          <input type="text" class="form-control" name="loanid"
                                value="{{old('loanid')}}">
                        </div>
                      </div>
                      <div class="col-6">
                        <div class="form-group">
                          <label for="netloan">Net Loan:</label>
                          <input type="text" class="form-control" name="netloan"
                                value="{{old('netloan')}}">
                        </div>
                      </div>
                      <div class="col-6">
                        <div class="form-group">
                          <label for="rate">Rate:</label>
                          <input type="text" class="form-control" name="rate"
                                value="{{ old('rate') ? old('rate') : 1}}">
                        </div>
                      </div>
                      <div class="col-6">
                        <div class="form-group ">
                          <label for="defaultRate">Default Rate:</label>
                          <input type="text" class="form-control" name="defaultRate"
                                value="{{ old('defaultRate') ? old('defaultRate') : 2}}">
                        </div>
                      </div>
                      <div class="col-4">
                        <div class="form-group">
                          <label for="duration">Duration(in months):</label>
                          <select class="form-control" name="duration">
                            @for($i = 1; $i <= 24; $i++)
                              <option value="{{$i}}" {{ old('duration') == $i ? 'selected': '' }}>{{$i}}</option>
                            @endfor
                          </select>
                        </div>
                      </div>
                      <div class="col-4">
                        <div class="form-group">
                          <label for="minTerm">Minimum Term(in months):</label>
                          <select class="form-control" name="minTerm">
                            @for($i = 0; $i < 24; $i++)
                              <option value="{{$i}}" {{ old('minTerm') == $i ? 'selected': '' }}>{{$i}}</option>
                            @endfor
                          </select>
                        </div>
                      </div>
                      <div class="col-4">
                        <div class="form-group">
                          <label for="issuedate">Issue Date:</label>
                          <input class="form-control datepicker" autocomplete="off" name="issuedate"
                                value="{{ old('issuedate')}}">
                        </div>
                      </div>
                      <div class="col-4">
                        <div class="form-group">
                          <label for="loanType">Loan Type:</label>
                          <select class="form-control" name="loanType">
                            <option value="serviced" {{ old('loanType') == "serviced" ? 'selected': '' }} >Serviced</option>
                            <option value="rolled" {{ old('loanType') == "rolled" ? 'selected': '' }} >Rolled</option>
                          </select>
                        </div>
                      </div>
                      <!-- <input type="hidden" name="loanType" value="serviced"> -->
                      <div class="col-4">
                        <div class="form-group">
                          <label for="customerId">Customers:</label>
                          <select name="customerId" class="form-control">
                              @foreach($customers as $customer)
                                <option value="{{$customer->id}}" {{  old('customerId') == $customer->id ? 'selected': '' }} >{{ $customer->username }}</option>
                              @endforeach
                          </select>
                        </div>
                      </div>
                      <div class="col-4">
                        <div class="form-group">
                          <span class="radio-text">Holiday Refund:</span>
                          <label class="custom-radio mb-0">
                            <input class="" type="hidden" name="holidayRefund" value="0">
                            <input class="" type="checkbox" name="holidayRefund"
                              @if(old('holidayRefund') != null && old('holidayRefund') == 0)
                                ''
                              @else
                                checked = "checked"
                              @endif
                              value="1">
                            <span class="checkmark"></span>
                          </label>
                        </div>
                      </div>

                      <div class="col-4">
                        <div class="form-group">
                          <label for="nominalCode">Nominal Codes:</label>
                          <select class="form-control" name="nominalCode">
                            @foreach($nominalCodes as $nominalCode)
                              <option value="{{$nominalCode->id}}" {{ (old('nominalCode') == $nominalCode->id) ? 'selected' : ''}}>{{$nominalCode->code}} - {{$nominalCode->name}}</option>
                            @endforeach
                          </select>
                        </div>
                      </div>

                      <div class="col-12">
                        <div class="form-group">
                          <label for="description">Description:</label>
                          <textarea class="form-control" rows="2" name="description">{{ old('description')}}</textarea>
                        </div>
                      </div>
                      <div class="col-lg-12">
                        <button type="submit" class="btn pull-right btn-primary custom-padding mt-3">Create Loan</button>
                      </div>
                    </div>
                  </form>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>

    <!-- create N-Tranch loan modal -->
    <div class="modal" id="createNTranchLoanModal" data-backdrop="static">
      <div class="modal-dialog" style="max-width: 800px">
        <div class="modal-content">
          <div class="modal-body custom_modal">
            <div class="row">
              <div class="col-lg-1">
                <div class="left-side">
                </div>
              </div>
              <div class="col-lg-11">
                <form action="{{ url('calculations/store' )}}" method="post">
                  <h4 class="modal-title">Create N-Tranch Loan<button type="button" class="close" data-dismiss="modal">&times;</button></h4>
                    <div class="row">
                      <div class="col-6">
                        @csrf()
                        <div class="form-group">
                          <label for="loanid">Loan Id:</label>
                          <input type="text" class="form-control" name="loanid"
                                value="{{old('loanid')}}">
                        </div>
                      </div>
                      <div class="col-6">
                        <div class="form-group">
                          <label for="netloan">Net Loan:</label>
                          <input type="text" class="form-control" name="netloan"
                                value="{{old('netloan')}}">
                        </div>
                      </div>
                      <div class="col-6">
                        <div class="form-group">
                          <label for="rate">Rate:</label>
                          <input type="text" class="form-control" name="rate"
                                value="{{ old('rate') ? old('rate') : 1}}">
                        </div>
                      </div>
                      <div class="col-6">
                        <div class="form-group ">
                          <label for="defaultRate">Default Rate:</label>
                          <input type="text" class="form-control" name="defaultRate"
                                value="{{ old('defaultRate') ? old('defaultRate') : 2}}">
                        </div>
                      </div>
                      <div class="col-4">
                        <div class="form-group">
                          <label for="duration">Duration(in months):</label>
                          <select class="form-control" name="duration">
                            @for($i = 1; $i <= 24; $i++)
                              <option value="{{$i}}" {{ old('duration') == $i ? 'selected': '' }}>{{$i}}</option>
                            @endfor
                          </select>
                        </div>
                      </div>
                      <div class="col-4">
                        <div class="form-group">
                          <label for="minTerm">Minimum Term(in months):</label>
                          <select class="form-control" name="minTerm">
                            @for($i = 0; $i < 24; $i++)
                              <option value="{{$i}}" {{ old('minTerm') == $i ? 'selected': '' }}>{{$i}}</option>
                            @endfor
                          </select>
                        </div>
                      </div>
                      <div class="col-4">
                        <div class="form-group">
                          <label for="issuedate">Issue Date:</label>
                          <input class="form-control datepicker21" autocomplete="off" name="issuedate"
                                value="{{ old('issuedate')}}">
                        </div>
                      </div>
                      <input type="hidden" name="loanType" value="n-tranch">
                      
                      <div class="col-4">
                        <div class="form-group">
                          <label for="customerId">Customers:</label>
                          <select name="customerId" class="form-control">
                              @foreach($customers as $customer)
                                <option value="{{$customer->id}}" {{  old('customerId') == $customer->id ? 'selected': '' }} >{{ $customer->username }}</option>
                              @endforeach
                          </select>
                        </div>
                      </div>
                      <div class="col-4">
                        <div class="form-group">
                          <span class="radio-text">Holiday Refund:</span>
                          <label class="custom-radio mb-0">
                            <input class="" type="hidden" name="holidayRefund" value="0">
                            <input class="" type="checkbox" name="holidayRefund"
                              @if(old('holidayRefund') != null && old('holidayRefund') == 0)
                                ''
                              @else
                                checked = "checked"
                              @endif
                              value="1">
                            <span class="checkmark"></span>
                          </label>
                        </div>
                      </div>

                      <div class="col-4">
                        <div class="form-group">
                          <label for="nominalCode">Nominal Codes:</label>
                          <select class="form-control" name="nominalCode">
                            @foreach($nominalCodes as $nominalCode)
                              <option value="{{$nominalCode->id}}" {{ (old('nominalCode') == $nominalCode->id) ? 'selected' : ''}}>{{$nominalCode->code}} - {{$nominalCode->name}}</option>
                            @endforeach
                          </select>
                        </div>
                      </div>

                      <div class="col-12">
                        <div class="form-group">
                          <label for="description">Description:</label>
                          <textarea class="form-control" rows="2" name="description">{{ old('description')}}</textarea>
                        </div>
                      </div>
                      <div class="col-lg-12">
                        <button type="submit" class="btn pull-right btn-primary custom-padding mt-3">Create Loan</button>
                      </div>
                    </div>
                  </form>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>

    <!-- create Retained loan modal -->
    <div class="modal" id="createRetainedLoanModal" data-backdrop="static">
      <div class="modal-dialog" style="max-width: 1000px">
        <div class="modal-content">
          <div class="modal-body custom_modal">
            <div class="row">
              <div class="col-lg-1">
                <div class="left-side">
                </div>
              </div>
              <div class="col-lg-11">
                <form action="{{ url('calculations/store' )}}" method="post">
                  <h4 class="panel-title">
                    <div class="row">
                      <div class="col-4 js-LoanModalTitle">Create Retained Loan</div>
                      <div class="col-8">
                        <button type="button" class="close pull-right" data-dismiss="modal">&times;</button>
                        <button type="button" class="btn btn-primary pull-right ml-1 addNewChargeModal" data-toggle="modal" data-target="#addNewChargeModal">
                          Add new charge
                        </button>
                      </div>
                    </div>
                  </h4>
                    <div class="row">
                      <div class="col-lg-12">
                        <div class="table-scroll retained-loan">
                          <div class="row">
                            <!-- <div class="col-4"> -->
                              @csrf()
                            <!-- </div> -->
                            <div class="col-4">
                              <div class="form-group">
                                <label for="loanid">Loan Id:</label>
                                <input type="text" class="form-control" name="loanid"
                                      value="{{old('loanid')}}">
                              </div>
                            </div>
                            <div class="col-4">
                              <div class="form-group">
                                <label for="customerId">Customers:</label>
                                <select name="customerId" class="form-control">
                                    @foreach($customers as $customer)
                                      <option value="{{$customer->id}}" {{  old('customerId') == $customer->id ? 'selected': '' }} >{{ $customer->username }}</option>
                                    @endforeach
                                </select>
                              </div>
                            </div>
                            <div class="col-4">
                              <div class="form-group">
                                <label for="description">Description:</label>
                                <textarea class="form-control" rows="2" name="description">{{ old('description')}}</textarea>
                              </div>
                            </div>
                            <div class="col-4">
                              <div class="form-group">
                                <label for="rate">Rate:</label>
                                <input type="text" class="form-control js-RLRate" name="rate"
                                      value="{{(old('rate')) ? old('rate') : '1'}}" min="0">
                              </div>
                            </div>
                            <div class="col-4">
                              <div class="form-group ">
                                <label for="defaultRate">Default rate:</label>
                                <input type="text" class="form-control js-RLDefaultRate" name="defaultRate"
                                      value="{{ (old('defaultRate')) ? old('defaultRate') : 2}}" min="0">
                              </div>
                            </div>
                            
                            <div class="col-4">
                              <div class="form-group">
                                <label class="js-issuedate">Issue date:</label>
                                <input class="form-control datepicker1 js-RLIssuedate" autocomplete="off" name="issuedate"
                                      value="{{ old('issuedate')}}">
                              </div>
                            </div>
                            <div class="col-4">
                              <div class="form-group">
                                <label class="js-duration">Duration(in months):</label>
                                <input class="form-control js-RLDuration" type="text" name="duration" value="{{ (old('duration')) ? old('duration') : 12}}">
                              </div>
                            </div>
                            <div class="col-4">
                              <div class="form-group">
                                <label for="noofdays">Number of days:</label>
                                <input type="text" class="form-control js-RLNoOfDays" name="noofdays"
                                      value="{{ (old('noofdays')) ? old('noofdays') : 0}}" readonly>
                              </div>
                            </div>
                            <div class="col-4">
                              <div class="form-group">
                                <label class="js-endDate">End date:</label>
                                <input class="form-control datepicker19 js-RLEndDate" autocomplete="off" name="enddate"
                                      value="{{ old('enddate')}}">
                              </div>
                            </div>

                            <div class="col-4">
                              <div class="form-group">
                                <label for="nominalCode">Nominal codes:</label>
                                <select class="form-control" name="nominalCode">
                                  @foreach($nominalCodes as $nominalCode)
                                    <option value="{{$nominalCode->id}}" {{ (old('nominalCode') == $nominalCode->id) ? 'selected' : ''}}>{{$nominalCode->code}} - {{$nominalCode->name}}</option>
                                  @endforeach
                                </select>
                              </div>
                            </div>
                            
                            <div class="col-4">
                              <div class="form-group">
                                <label for="grossloan">Gross loan:</label>
                                <input type="text" class="form-control js-RLGrossLoan" name="grossloan"
                                      value="{{ (old('grossloan')) ? old('grossloan') : 0}}" min="0">
                              </div>
                            </div>
                            <!-- <input type="hidden" name="holidayRefund" value="1"> -->
                            <div class="col-4">
                              <div class="form-group">
                                <span class="radio-text">Holiday refund:</span>
                                <label class="custom-radio mb-0">
                                  <input class="" type="hidden" name="holidayRefund" value="0">
                                  <input class="" type="checkbox" name="holidayRefund"
                                    @if(old('holidayRefund') != null && old('holidayRefund') == 0)
                                      ''
                                    @else
                                      checked = "checked"
                                    @endif
                                    value="1">
                                  <span class="checkmark"></span>
                                </label>
                              </div>
                            </div>
                            <!-- <input type="hidden" name="minTerm" value="0"> -->
                            <div class="col-4">
                              <div class="form-group">
                                <label for="minTerm">Minimum Term(in months):</label>
                                <select class="form-control" name="minTerm">
                                  @for($i = 0; $i < 24; $i++)
                                    <option value="{{$i}}" {{ old('minTerm') == $i ? 'selected': '' }}>{{$i}}</option>
                                  @endfor
                                </select>
                              </div>
                            </div>
                            
                            <input type="hidden" name="loanType" value="retained">
                            <div class="col-12 js-ServiceDuration">
                              
                            </div>

                            <div class="col-lg-12">
                              <!-- <div class="js-retainedLoanForm"> -->
                                <div class="row js-retainedLoanForm">
                                @foreach($charges as $key => $charge)
                                  <div class="col-8 offset-2">
                                    <div class="form-group">
                                      <div class="row">
                                        <div class="col-9">
                                          <label class="text-capitalize">{{$charge->name}}:</label>
                                        </div>
                                        <div class="col-3">
                                            <label for="nominalCode">Nominal Codes:</label>
                                        </div>
                                      </div>
                                      <input type="hidden" name="RLChargeId[]" value="{{$charge->id}}">
                                      @if($charge->chargetype === 'percentage')
                                      <div class="row">
                                        <div class="col-3">
                                          <input type="text" class="form-control js-percentChange js-valChange{{$key}}" data-key="{{$key}}" value="{{$charge->value}}">
                                        </div>
                                        <div class="col-3">
                                          <p class="small has-no-margin">% of Gross loan = </p>
                                        </div>
                                        <div class="col-3">
                                          <input type="text" class="form-control js-RLInitialCharge{{$key}} js-percentFixedVal js-keyGetter" data-key="{{$key}}" name="initialChargeValue[]" value="0">
                                        </div>
                                        <div class="col-3">
                                            <select class="form-control" name="nominalCodeForCharges[]">
                                            @foreach($nominalCodes as $nominalCode)
                                              <option value="{{$nominalCode->id}}" {{ (old('nominalCodeForCharges[]') == $nominalCode->id) ? 'selected' : ''}}>{{$nominalCode->code}} - {{$nominalCode->name}}</option>
                                            @endforeach
                                          </select>
                                        </div>
                                      </div>
                                      @else
                                        <div class="row">
                                          <div class="col-7">
                                            <input type="text" class="form-control js-fixedValChange js-RLInitialCharge{{$key}} js-keyGetter" data-key="{{$key}}" name="initialChargeValue[]" value="{{$charge->value}}" onchange="adjustLoan();">
                                          </div>
                                          <div class="col-3 offset-2">
                                            <select class="form-control" name="nominalCodeForCharges[]">
                                              @foreach($nominalCodes as $nominalCode)
                                                <option value="{{$nominalCode->id}}" {{ (old('nominalCodeForCharges[]') == $nominalCode->id) ? 'selected' : ''}}>{{$nominalCode->code}} - {{$nominalCode->name}}</option>
                                              @endforeach
                                            </select>
                                          </div>
                                        </div>
                                      @endif
                                    </div>
                                  </div>
                                @endforeach
                                </div>
                              <!-- </div> -->
                            </div>


                            <div class="col-4">
                              <div class="form-group">
                                <label for="rateperday">Daily Charge:</label>
                                <input type="text" class="form-control js-RLRatePerDay" name="rateperday"
                                      value="{{ (old('rateperday')) ? old('rateperday') : 0}}" readonly>
                              </div>
                            </div>

                            <div class="col-4">
                              <div class="form-group">
                                <label for="ratepermonth">Rate Per Month:</label>
                                <input type="text" class="form-control js-RLRatePerMonth" name="ratepermonth"
                                      value="{{ (old('ratepermonth')) ? old('ratepermonth') : 0}}" readonly>
                              </div>
                            </div>
                            
                            <div class="col-4">
                              <div class="form-group">
                                <label for="interest">Retained Interest:</label>
                                <input type="text" class="form-control js-RLInterest" name="interest"
                                      value="{{ (old('interest')) ? old('interest') : 0}}" readonly>
                              </div>
                            </div>

                            <div class="col-4">
                              <div class="form-group">
                                <label for="roundingError">Rounding Error:</label>
                                <input type="text" class="form-control js-RLRoundingError" name="roundingError"
                                      value="{{ (old('roundingError')) ? old('roundingError') : 0}}" readonly>
                              </div>
                            </div>
                            
                            <div class="col-4">
                              <div class="form-group">
                                <label for="netloan">Net Loan:</label>
                                <input type="text" class="form-control js-RLNetLoan" name="netloan"
                                      value="{{ (old('netloan')) ? old('netloan') : 0}}" readonly>
                              </div>
                            </div>
                          </div>
                        </div>
                      </div>
                      <div class="col-lg-12">
                        <button type="submit" class="btn pull-right btn-primary custom-padding mt-3">Create Loan</button>
                      </div>
                    </div>
                  </form>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>

    <!-- create Manual loan modal -->
    <div class="modal" id="createManualLoanModal" data-backdrop="static">
      <div class="modal-dialog" style="max-width: 800px">
        <div class="modal-content">
          <div class="modal-body custom_modal">
            <div class="row">
              <div class="col-lg-1">
                <div class="left-side">
                </div>
              </div>
              <div class="col-lg-11">
                <form action="{{ url('calculations/store' )}}" method="post">
                  <h4 class="modal-title">Create Manual Loan <button type="button" class="close" data-dismiss="modal">&times;</button></h4>
                    <div class="row">
                      <div class="col-6">
                        @csrf()
                        <div class="form-group">
                          <label for="loanid">Loan Id:</label>
                          <input type="text" class="form-control" name="loanid"
                                value="{{old('loanid')}}">
                        </div>
                      </div>
                      <div class="col-6">
                        <div class="form-group">
                          <label for="netloan">Net Loan:</label>
                          <input type="text" class="form-control" name="netloan"
                                value="{{old('netloan')}}">
                        </div>
                      </div>
                      <div class="col-6">
                        <div class="form-group">
                          <label for="rate">Rate:</label>
                          <input type="text" class="form-control" name="rate"
                                value="{{ old('rate') ? old('rate') : 1}}">
                        </div>
                      </div>
                      <div class="col-6">
                        <div class="form-group ">
                          <label for="defaultRate">Default Rate:</label>
                          <input type="text" class="form-control" name="defaultRate"
                                value="{{ old('defaultRate') ? old('defaultRate') : 2}}">
                        </div>
                      </div>
                      <div class="col-4">
                        <div class="form-group">
                          <label for="duration">Duration(in months):</label>
                          <select class="form-control" name="duration">
                            @for($i = 1; $i <= 24; $i++)
                              <option value="{{$i}}" {{ old('duration') == $i ? 'selected': '' }}>{{$i}}</option>
                            @endfor
                          </select>
                        </div>
                      </div>
                      <div class="col-4">
                        <div class="form-group">
                          <label for="minTerm">Minimum Term(in months):</label>
                          <select class="form-control" name="minTerm">
                            @for($i = 0; $i < 24; $i++)
                              <option value="{{$i}}" {{ old('minTerm') == $i ? 'selected': '' }}>{{$i}}</option>
                            @endfor
                          </select>
                        </div>
                      </div>
                      <div class="col-4">
                        <div class="form-group">
                          <label for="issuedate">Issue Date:</label>
                          <input class="form-control datepicker2" autocomplete="off" name="issuedate"
                                value="{{ old('issuedate')}}">
                        </div>
                      </div>
                      <!-- <div class="col-4">
                        <div class="form-group">
                          <label for="loanType">Loan Type:</label>
                          <select class="form-control" name="loanType">
                            <option value="serviced" {{ old('loanType') == "serviced" ? 'selected': '' }} >Serviced</option>
                            <option value="manual" {{ old('loanType') == "manual" ? 'selected': '' }} >Manual</option>
                          </select>
                        </div>
                      </div> -->
                      <input type="hidden" name="loanType" value="manual">
                      <div class="col-4">
                        <div class="form-group">
                          <label for="customerId">Customers:</label>
                          <select name="customerId" class="form-control">
                              @foreach($customers as $customer)
                                <option value="{{$customer->id}}" {{  old('customerId') == $customer->id ? 'selected': '' }} >{{ $customer->username }}</option>
                              @endforeach
                          </select>
                        </div>
                      </div>
                      <div class="col-4">
                        <div class="form-group">
                          <span class="radio-text">Holiday Refund:</span>
                          <label class="custom-radio mb-0">
                            <input class="" type="hidden" name="holidayRefund" value="0">
                            <input class="" type="checkbox" name="holidayRefund"
                              @if(old('holidayRefund') != null && old('holidayRefund') == 0)
                                ''
                              @else
                                checked = "checked"
                              @endif
                              value="1">
                            <span class="checkmark"></span>
                          </label>
                        </div>
                      </div>
                      <div class="col-4">
                        <div class="form-group">
                          <label for="nominalCode">Nominal Codes:</label>
                          <select class="form-control" name="nominalCode">
                            @foreach($nominalCodes as $nominalCode)
                              <option value="{{$nominalCode->id}}" {{ (old('nominalCode') == $nominalCode->id) ? 'selected' : ''}}>{{$nominalCode->code}} - {{$nominalCode->name}}</option>
                            @endforeach
                          </select>
                        </div>
                      </div>
                      <div class="col-12">
                        <div class="form-group">
                          <label for="description">Description:</label>
                          <textarea class="form-control" rows="2" name="description">{{ old('description')}}</textarea>
                        </div>
                      </div>
                      <div class="col-lg-12">
                        <button type="submit" class="btn pull-right btn-primary custom-padding mt-3">Create Loan</button>
                      </div>
                    </div>
                  </form>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>

    <!-- Add New Charge Modal-->
    <div class="modal" id="addNewChargeModal" data-backdrop="static" style="z-index: 1111;">
      <div class="modal-dialog" style="max-width: 700px">
        <div class="modal-content">
          <div class="modal-body custom_modal">
            <div class="row">
              <div class="col-lg-1">
                <div class="left-side">
                </div>
              </div>
              <div class="col-lg-11">
                <h4 class="modal-title">Add Initial Charge <button type="button" class="close" data-dismiss="modal">&times;</button></h4>
                <form action="{{url('calculations/createcharge')}}" method="post" id="addInitialCharge">
                  @csrf()
                  
                  <div class="form-group">
                    <label for="name">Name:</label>
                    <input type="text" class="form-control" name="name">
                  </div>
                  
                  <!-- <div class="form-group"> -->
                    <!-- <label for="value">Value:</label> -->
                    <input type="hidden" class="form-control" name="value" value="0">
                  <!-- </div> -->

                  <div class="form-group">
                    <label for="chargetype">Charge Type:</label>
                    <select class="form-control" name="chargetype">
                      <option value="fixed">Fixed</option>
                      <option value="percentage">Percentage</option>
                    </select> 
                  </div>                              
              
                  <input type="hidden" class="form-control" name="amounttype" value="netloan">

                  <button type="submit" class="btn btn-primary js-addInitialCharge">Submit</button>
                </form>
                <div class="js-errors"></div>
              </div>          
            </div>
          </div>
        </div>
      </div>
    </div>

    <!-- Edit Tag Modal-->
    <div class="modal" id="loanTagModal" data-backdrop="static" style="z-index: 1050;">
      <div class="modal-dialog" style="max-width: 700px">
        <div class="modal-content">
          <div class="modal-body custom_modal">
            <div class="row">
              <div class="col-lg-1">
                <div class="left-side">
                </div>
              </div>
              <div class="col-lg-11">
                <h4 class="modal-title">Loan Tags <button type="button" class="close" data-dismiss="modal">&times;</button></h4>
                <form action="{{url('loan-tags')}}" method="post">
                  @csrf()
                  <input type="hidden" class="form-control" name="loanId">
                  <div class="form-group">
                    <label for="type">Tags:</label>
                    <!-- <input type="text" class="form-control js-loan-tags" name="tags"> -->
                    <select class="js-loan-tags" name="tags[]" multiple="multiple" style="width: 30%;">
                      @foreach($tags as $tag)
                      <option value="{{$tag->id}}">{{$tag->name}}</option>
                      @endforeach
                    </select>
                  </div>
                  <button type="submit" class="btn btn-primary">Submit</button>
                </form>
              </div>          
            </div>
          </div>
        </div>
      </div>
    </div>

    <!-- Queue Jobs Modal-->
    <div class="modal" id="queueJobsModal" data-backdrop="static" style="z-index: 1111;">
      <div class="modal-dialog" style="max-width: 700px">
        <div class="modal-content">
          <div class="modal-body custom_modal">
            <div class="row">
              <div class="col-lg-1">
                <div class="left-side">
                </div>
              </div>
              <div class="col-lg-11">
                <h4 class="modal-title">Remaining Queue Jobs/Sheets info: <button type="button" class="close" data-dismiss="modal">&times;</button></h4>
                <a type="button" class="btn btn-primary pull-right" onclick="queueCount()">Refresh</a>
                <div class="queue-details"></div>
              </div>          
            </div>
          </div>
        </div>
      </div>
    </div>
</div>

@endsection

@section('pages-js')
<script type="text/javascript" src="https://js.live.net/v7.2/OneDrive.js"></script>
<script>

  $(document).ready(function(){
    $(document).on('click', '.target-description', function() {
      var key = $(this).data('key');
      $('.show-description'+key).toggle("slide");
      $(this).find(".svg-inline--fa").toggleClass("fa-chevron-up fa-chevron-down" );
    });
    queueCount();
  });

  function queueCount(){
    $(".queue-count").html('Loading...');
    $('.queue-details').html('Loading...');

    $.ajax({
      url : "{{url('import-queue-logs')}}",
      type: 'GET',
      processData: false,
      contentType: false,
      success: function(result){
        $(".queue-count").html(result.length);
        var queues = '';
        $.each(result, function(i, job){
          queues += '<p class="text-info mb-0">Job Id: '+job.job_id+': &nbsp;&nbsp;&nbsp; Sheet Name: '+job.sheet_name+'</p>';
        });
        $('.queue-details').html(queues);
      },
      error: function(error){
        console.log(error);
      }
    });
  }

  function launchOneDrivePicker() {

    var odOptions = {
      clientId: "37182860-f46f-4d2f-876d-6b261301609b",
      action: "download",
      multiSelect: false,
      oauth2AllowUrlPathMatching: true,
      advanced: {},
      success: function(files) { 
        var selectedFile = files.value[0];
        var downloadLink = selectedFile["@microsoft.graph.downloadUrl"];
        $("input[name=oneDriveFile]").val(selectedFile["@microsoft.graph.downloadUrl"]);
        $("input[name=oneDriveFileName]").val(selectedFile.name);
        $("input[name=oneDriveFileName]").prop("readonly", true);;
      },
      cancel: function() { /* cancel handler */ },
      error: function(error) { 
        console.log(error);
       }
    }
    OneDrive.open(odOptions);
  }

  $(document).on("click", ".loanTagModal", function () {
      $("input[name=loanId]").val($(this).data('loanid'));
      $("input[name=tags]").val($(this).data('tags'));
  });

  $('.js-importForm').on("submit", function(e){
    e.preventDefault();
    $('.js-importBtn').attr('disabled', 'disabled');
    var formData = new FormData(this);
    // console.log(formData);
    $.ajax({
      url : $(this).attr('action'),
      type: $(this).attr('method'),
      beforeSend: function(){
        $('.loader').removeClass('hide');
        $('.js-importError').html('<span class="text-info">Please wait while your file is being processed.</span>');
      },
      complete: function(){$('.loader').addClass('hide');},
      data : formData,
      processData: false,
      contentType: false,
      success: function(result){
        if(result.code == 200){
          var success = errors = ignored = '';
          if(result.success.length){
            $('.js-importErrorsAll').html('<div class="row js-msgs"></div>');
            $.each(result.errors, function(f, files){
              success += '<div class="col-3"><h5>'+files.file+'</h5>';
              $.each(files.message, function(m, msg){
                success += '<p class="text-success mb-0">'+msg+'</p>';
              });
              $('.js-msgs').append(success);
              success = '';
            });
          }
         if(result.ignored.length){
          console.log(result.ignored.length, result.ignored);
            $('.js-importErrorsAll').html('<div class="row js-msgs"></div>');
              errors += '<div class="col-3"><h5>Ignored Hidden Sheets</h5>';
                $.each(result.ignored, function(i, msg){
                  errors += '<p class="text-info mb-0">'+msg+'</p>';
                });
              errors += '</div>';
              // $('.js-msgs').append(ignored);
              // errors = '';
          }

          if(result.errors.length){
            $('.js-importErrorsAll').html('<div class="row js-msgs"></div>');
            $.each(result.errors, function(f, files){
              errors += '<div class="col-3"><h5>'+files.file+'</h5>';
              $.each(files.message, function(m, msg){
                // console.log(msg.endsWith("error"), msg.endsWith("error."));
                if(msg.endsWith("error")){
                  msg = msg.slice(0, -5);
                  errors += '<p class="text-danger mb-0">'+msg+'</p>';
                }
                else if(msg.endsWith("warning")){
                  msg = msg.slice(0, -7);
                  errors += '<p class="text-warning mb-0">'+msg+'</p>';
                }
                else{
                  msg = msg.slice(0, -4);
                  errors += '<p class="text-info mb-0">'+msg+'</p>';
                }
              });
              errors += '</div>';
              $('.js-msgs').append(errors);
              errors = '';
            });
          }
          // scroll window to the top
          $('#importLoanModal').modal('toggle');
          $('.js-importError').html('');
        }
        else if(result.code == 400)
          $('.js-importError').html('Un-expected error occurred while importing loan data.');
        else if(result.code == 401)
          $('.js-importError').html(result.message);
        else
          $('.js-importError').html('Internal server error. Please refresh the page and try fixing loan data.');
        window.scrollTo(0,0);
        $('.js-importBtn').attr('disabled', false);
        queueCount();
      },
      error: function(errors){
        $('.js-importError').html('Internal server error.');
        $('.js-importBtn').attr('disabled', false);
      },
    });
  });

  $(document).on("click", ".updateLoanModal", function () {
      $("input[name=loanId]").val($(this).data('loanid'));
      $("input[name=loanName]").val($(this).data('name'));
    });

    var columnDefs = [
      // {field: 'loan id'},
      {field: 'loanId'},
      {field: 'issueDate'},
      {field: 'netLoan'},
      {field: 'grossLoan'},
      {field: 'loanType'},
      {field: 'tags'},
      {field: 'lastFileImportDate'},
    ];
    var gridOptions = {
        defaultColDef: {
            cellClassRules: {
                lightGreyBackground: function(params) {
                    return params.rowIndex % 2 == 0;
                }
            },
            sortable: true,
            editable: false,
            filter: 'agSetColumnFilter',
            filterParams: {
                applyMiniFilterWhileTyping: true,
            },
            flex: 1,
            minWidth: 200,
            resizable: true,
            floatingFilter: true,
            filter: 'agTextColumnFilter',
        },
        // enables pagination in the grid
        // pagination: true,
        // sets 20 rows per page (default is 100)
        // paginationPageSize: 20,
        columnDefs: columnDefs,
        animateRows: true,
        // headerHeight: 80,
        rowSelection: 'multiple',
        onCellValueChanged: function(e) {
          // console.log('datatape after changes is: ', event.data);
            console.log(e.value, e.data.loan_real_id, e.colDef.field);
          // var data = event.data;
          $.ajax({
            url: "{{url('calculations/createoption')}}",
            type: 'post',
            data: {loanId: e.data.loan_real_id, name: e.colDef.field, value: e.value, updateByLoanId: true},
            success: function(result){
                console.log(result);
            }, error: function(errors){
                console.log(errors);
            }
          });
        },
        // onCellEditingStopped(e) {
        //     console.log(e.value, e.data.loan_real_id, e.colDef.field);
        // },
        // onSelectionChanged(){
        //     console.log('asdf');
        // },

        excelStyles: [
            {
                id: 'greenBackground',
                interior: {
                    color: '#b5e6b5',
                    pattern: 'Solid'
                }
            },
            {
                id: 'redFont',
                font: {
                    fontName: 'Calibri Light',
                    underline: 'Single',
                    italic: true,
                    color: '#ff0000'
                }
            },
            {
                id: 'lightGreyBackground',
                interior: {
                    color: '#e8ebec',
                    pattern: 'Solid'
                },
                font: {
                    fontName: 'Calibri Light',
                    // color: '#ffffff'
                }
            },
            {
                id: 'boldBorders',
                borders: {
                    borderBottom: {
                        color: '#000000',
                        lineStyle: 'Continuous',
                        weight: 3
                    },
                    borderLeft: {
                        color: '#000000',
                        lineStyle: 'Continuous',
                        weight: 3
                    },
                    borderRight: {
                        color: '#000000',
                        lineStyle: 'Continuous',
                        weight: 3
                    },
                    borderTop: {
                        color: '#000000',
                        lineStyle: 'Continuous',
                        weight: 3
                    }
                }
            },
            {
                id: 'header',
                interior: {
                    color: '#CCCCCC',
                    pattern: 'Solid'
                },
                borders: {
                    borderBottom: {
                        color: '#5687f5',
                        lineStyle: 'Continuous',
                        weight: 1
                    },
                    borderLeft: {
                        color: '#5687f5',
                        lineStyle: 'Continuous',
                        weight: 1
                    },
                    borderRight: {
                        color: '#5687f5',
                        lineStyle: 'Continuous',
                        weight: 1
                    },
                    borderTop: {
                        color: '#5687f5',
                        lineStyle: 'Continuous',
                        weight: 1
                    }
                }
            },
            {
                id: 'dateFormat',
                dataType: 'dateTime',
                numberFormat: {
                    format: 'mm/dd/yyyy;@'
                }
            },
            {
                id: 'twoDecimalPlaces',
                numberFormat: {
                    format: '#,##0.00'
                }
            },
            {
                id: 'textFormat',
                dataType: 'string'
            },
            {
                id: 'bigHeader',
                font: {
                    size: 25
                }
            }
        ]
    };
    // console.log(gridOptions.defaultColDef.editable);

    function getBooleanValue(cssSelector) {
        return document.querySelector(cssSelector).checked === true;
    }

    function getTextValue(cssSelector) {
        return document.querySelector(cssSelector).value;
    }

    function getNumericValue(cssSelector) {
        var value = parseFloat(getTextValue(cssSelector));
        if (isNaN(value)) {
            var message = "Invalid number entered in " + cssSelector + " field";
            alert(message);
            throw new Error(message);
        }
        return value;
    }

    function myColumnWidthCallback(params) {
        var originalWidth = params.column.getActualWidth();
        if (params.index < 7) {
            return originalWidth;
        }
        return 30;
    }

    function onBtExport() {
        var columnWidth = getBooleanValue('#columnWidth') ? getTextValue('#columnWidthValue') : undefined;
        var params = {
            columnWidth: columnWidth === 'myColumnWidthCallback' ? myColumnWidthCallback : parseFloat(columnWidth),
            sheetName: getBooleanValue('#sheetName') && getTextValue('#sheetNameValue'),
            exportMode: getBooleanValue('#exportModeXml') ? "xml" : undefined,
            suppressTextAsCDATA: getBooleanValue('#suppressTextAsCDATA'),
            rowHeight: getBooleanValue('#rowHeight') ? getNumericValue('#rowHeightValue') : undefined,
            headerRowHeight: getBooleanValue('#headerRowHeight') ? getNumericValue('#headerRowHeightValue') : undefined,
        };

        gridOptions.api.exportDataAsExcel(params);
    }

    // setup the grid after the page has finished loading
    document.addEventListener('DOMContentLoaded', function() {
        var gridDiv = document.querySelector('#myGrid');
        new agGrid.Grid(gridDiv, gridOptions);

        $.ajax({
            url: "{{url('loans?json=true')}}",
            type: 'get',
            async: false,
            success: function(result){
            // agGrid.simpleHttpRequest({url: "{{url('datatype?limit=0')}}"}).then(function(data) {
                // console.log(result);
                // var defs = [];
                // var box = {};
                // $(result).each(function(item, loan){
                //   defs['loan id'] = loan.loanid;
                //   defs['net loan'] = loan.interest.netloan;
                //   box[item] = {
                //       loanid: loan.loanid,
                //   }
                // });
                // console.log(box);
                console.log(result);
                gridOptions.api.setRowData(result);
                // gridOptions.api.setRowData(result);
            }, error: function(errors){
                console.log('error');
            }
        });
    });
</script>
@endsection