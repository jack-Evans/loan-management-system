@extends('layouts.app')

@section('title', 'Issue Loan')

@section('content')
    <div class="row">
        <div class="col-md-5 col-md-offset-2">
            <h3>Issue Loan</h3>
            <form action="{{'issueloan'}}" method="post">
                @csrf()

                <div class="form-group">
                  <label for="loanId">Loan Id:</label>
                  <input type="text" class="form-control" name="loanId" value="{{old('loanId')}}">
                </div>

                <div class="form-group">
                  <label for="issueAt">Issue At:</label>
                  <input class="form-control datepicker" name="issueAt" value="{{old('issueAt')}}"/>
                </div>

                <button type="submit" class="btn btn-primary">Submit</button>
            </form>
        </div>
    </div>
@endsection