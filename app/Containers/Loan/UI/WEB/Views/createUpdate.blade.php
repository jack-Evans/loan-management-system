@extends('layouts.app')

@section('title', 'Loans')

@section('content')
    <div class="row">
        <div class="col-md-5 col-md-offset-2">
            <h3>{{isset($loan->id) ? 'Edit ' : 'Create '}}Loan</h3>             
            <form action="{{url(isset($loan->id) ? 'loans/'.$loan->id.'/update': 'loans/store' )}}" method="post">
                @csrf()

                <div class="form-group">
                  <label for="loanId">Loan Id:</label>
                  <input type="text" class="form-control" name="loanId" 
                         value="{{isset($loan->loanid) ? $loan->loanid : old('loanId') }}">
                </div>

                <div class="form-group">
                  <label for="customerId">Customer Id:</label>
                  {{-- <input type="text" class="form-control" name="customerId" 
                         value="{{isset($loan->customerid) ? $loan->customerid : old('customerId') }}"> --}}
                         <select name="customerId" class="form-control">                         
                          @foreach($customers as $customer)
                           <option value="{{ $customer->id }}" {{  old('customerId') == $customer->id ? 'selected': '' }} >{{ $customer->username }}</option>
                          @endforeach
                         </select>
                </div>    

                <div class="form-group">
                  <label for="netloan">Net Loan:</label>
                  <input type="text" class="form-control" name="netloan" 
                         value="{{isset($loan->netloan) ? $loan->netloan : old('netloan')}}">
                </div>

                <div class="form-group">
                  <label for="rate">Rate:</label>
                  <input type="text" class="form-control" name="rate" 
                         value="{{isset($loan->rate) ? $loan->rate : old('rate')}}">
                </div>

                <div class="form-group">
                  <label for="defaultRate">Default Rate:</label>
                  <input type="text" class="form-control" name="defaultRate" 
                         value="{{isset($loan->defaultRate) ? $loan->defaultRate : old('defaultRate')}}">
                </div>                                                          

                <div class="form-group">
                  <label for="duration">Duration:</label>
                  <input type="text" class="form-control" name="duration" 
                         value="{{isset($loan->duration) ? $loan->duration : old('duration')}}">
                </div>                                                          


                <button type="submit" class="btn btn-primary">Submit</button>
            </form>
        </div>
    </div>
@endsection