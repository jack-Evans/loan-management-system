@extends('layouts.app')

@section('title', 'Loans')

@section('content')

    <h3>Charges
        <a href="{{url('calculateloancharges/'.$_GET['loanId'])}}" class="btn btn-success float-right">Back</a>
        <form action="{{url('createloancharges')}}" method="get">
            <input type="hidden" class="form-control" name="loanId" value="{{$_GET['loanId']}}">

            <input type="hidden" class="form-control" name="date" value="{{$_GET['date']}}"/>

            <button type="submit" class="btn btn-primary">Create Charge</button>
        </form>
    </h3>

    <div class="col-12">
        <table class="table text-left">
            <thead>
                <tr>
                    <th>Total</th>
                    <th>Total before payment</th>
                    <th>Total after payment</th>
                    <th>Rate before payment</th>
                    <th>Rate after payment</th>
                </tr>
            </thead>

            <tbody>
                <tr>
                    <td>{{$dailyCharge->total}}</td>
                    <td>{{$dailyCharge->totalBeforePayment}}</td>
                    <td>{{$dailyCharge->totalAfterPayment}}</td>
                    <td>{{$dailyCharge->rateBeforePayment}}</td>
                    <td>{{$dailyCharge->rateAfterPayment}}</td>
                </tr>
            </tbody>
        </table>

        <h3>Balance map after payment</h3>
        <table class="table text-left">
            <thead>
                <tr>
                    <th>Total debit</th>
                    <th>Total credit</th>
                    <th>Balance</th>
                </tr>
            </thead>

            <tbody>
                <tr>
                    <td>{{$dailyCharge->balanceMapAfterPayment->totalDebit}}</td>
                    <td>{{$dailyCharge->balanceMapAfterPayment->totalCredit}}</td>
                    <td>{{$dailyCharge->balanceMapAfterPayment->balance}}</td>
                </tr>
            </tbody>
        </table>

        <h3>Balance map before payment</h3>
        <table class="table text-left">
            <thead>
                <tr>
                    <th>Total debit</th>
                    <th>Total credit</th>
                    <th>Balance</th>
                </tr>
            </thead>

            <tbody>
                <tr>
                    <td>{{$dailyCharge->balanceMapBeforePayment->totalDebit}}</td>
                    <td>{{$dailyCharge->balanceMapBeforePayment->totalCredit}}</td>
                    <td>{{$dailyCharge->balanceMapBeforePayment->balance}}</td>
                </tr>
            </tbody>
        </table>

        <h3>Billing period</h3>
        <table class="table text-left">
            <thead>
                <tr>
                    <th>Days before payment</th>
                    <th>Days after payment</th>
                    <!--<th>Accounting Period</th>-->
                </tr>
            </thead>

            <tbody>

                <tr>
                    <td>{{$dailyCharge->daysBeforePayment}}</td>
                    <td>{{$dailyCharge->daysAfterPayment}}</td>
                    <!--<td> $dailyCharge->firstMonthInterest->accountingPeriod</td>-->
                </tr>
            </tbody>
        </table>
        @if(isset($dailyCharge->firstMonthInterest))
        <h3>First Month payment</h3>
        <table class="table text-left">
            <thead>
                <tr>
                    <th>Total</th>
                    <th>Rate</th>
                    <th>Days</th>
                    <th>Accounting Period</th>
                </tr>
            </thead>

            <tbody>

                <tr>
                    <td>{{$dailyCharge->firstMonthInterest->total}}</td>
                    <td>{{$dailyCharge->firstMonthInterest->rate}}</td>
                    <td>{{$dailyCharge->firstMonthInterest->days}}</td>
                    <td>{{$dailyCharge->firstMonthInterest->accountingPeriod }}</td>
                </tr>
            </tbody>
        </table>
        @endif

        @php // dd($dailyCharge) @endphp
    </div>
@endsection
