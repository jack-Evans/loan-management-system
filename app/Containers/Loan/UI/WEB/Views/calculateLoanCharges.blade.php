@extends('layouts.app')

@section('title', 'Loans')

@section('content')
    <h3>Calculated loan charges
        <a href="{{url('loans')}}" class="btn btn-success float-right">Back</a>
    </h3>

    <div class="">
        <table class="table text-left">
            <thead>
                <tr>
                    <th>Net loan</th>
                    <th>Fixed Charges</th>
                    <th>Charges on borrowed amount</th>
                    <th>Total charges</th>
                    <th>Principal amount</th>
                    <th>Gross Amount</th>
                </tr>
            </thead>

            <tbody>
                @php //dd($charges); @endphp
                <tr>
                    <td>{{$charges->netloan}}</td>
                    <td>{{$charges->fixedCharges}}</td>
                    <td>{{$charges->chargesOnBorrowedAmount}}</td>
                    <td>{{$charges->totalCharges}}</td>
                    <td>{{$charges->principalAmount}}</td>
                    <td>{{$charges->grossLoan}}</td>
                </tr>
            </tbody>
        </table>
    </div>

    <h3>Charges breakdown</h3>

    <div class="">
        <table class="table text-left">
            <thead>
                <tr>
                    <th>Charge name</th>
                    <th>Charge value</th>
                    <th>Charge type</th>
                    <th>Calculated charge amount</th>
                </tr>
            </thead>

            <tbody>
                @foreach($charges->chargesBreakdown as $breakdown)
                @php //dd($breakdown); @endphp
                <tr>
                    <td>{{$breakdown->name}}</td>
                    <td>{{$breakdown->value}}</td>
                    <td>{{$breakdown->charge_type}}</td>
                    <td>{{$breakdown->calculatedChargeAmount}}</td>
                </tr>
                @endforeach
            </tbody>
        </table>
    </div>

    <h3>Loan interest</h3>

    <div class="">
        <table class="table text-left">
            <thead>
                <tr>
                    <th>Principal amount</th>
                    <th>Gross loan</th>
                    <th>Rate</th>
                    <th>Default rate</th>
                    <th>Duration</th>
                </tr>
            </thead>

            <tbody>
                @php //dd($charges); @endphp
                <tr>
                    <td>{{$charges->loanInterest->principal_amount}}</td>
                    <td>{{$charges->loanInterest->gross_loan}}</td>
                    <td>{{$charges->loanInterest->rate}}</td>
                    <td>{{$charges->loanInterest->default_rate}}</td>
                    <td>{{$charges->loanInterest->duration}}</td>
                </tr>
            </tbody>
        </table>
    </div>

    @if($charges->loan->status == 'approved')
    <h3>
        Payment details
        <button type="button" class="btn btn-success float-right loanModal" data-toggle="modal"
                data-target="#payInterestModal" data-loanid="{{Request::segment(2)}}">
            Pay
        </button>
    </h3>

    <div class="">
        <table class="table text-left">
            <thead>
                <tr>
                    <th>Accounting period</th>
                    <th>Amount</th>
                    <th>Transaction date</th>
                </tr>
            </thead>

            <tbody>
                @foreach($payments as $payment)
                @php // dd($payment); @endphp
                <tr>
                    <td>{{$payment->posts[0]->accountingPeriod }}</td>
                    <td>{{$payment->posts[0]->amount }}</td>
                    <td>{{$payment->tnx_date->date }}</td>
                </tr>
                @endforeach
            </tbody>
        </table>
        <button type="button" class="btn btn-info float-right loanModal" data-toggle="modal"
                data-target="#dailyChargeModal" data-loanid="{{Request::segment(2)}}">
            Add charge
        </button>
    </div>

    @endif
<!-- Pay Interest Modal -->
<div class="modal" id="payInterestModal">
  <div class="modal-dialog">
    <div class="modal-content">

      <div class="modal-header">
        <h4 class="modal-title">Pay Interest</h4>
        <button type="button" class="close" data-dismiss="modal">&times;</button>
      </div>

      <div class="modal-body">
        <form action="{{url('payments/store')}}" method="post">
            @csrf()

            <input type="hidden" class="form-control" name="loanId" value="{{old('loanId')}}">

            <div class="form-group">
              <label for="amount">Amount:</label>
              <input type="text" class="form-control" name="amount" value="{{old('amount')}}"/>
            </div>

            <div class="form-group">
              <label for="paidAt">Paid At:</label>
              <input class="form-control datepicker" name="paidAt" value="{{old('paidAt')}}"/>
            </div>

            <button type="submit" class="btn btn-primary">Submit</button>
        </form>
      </div>

      <div class="modal-footer">
        <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
      </div>

    </div>
  </div>
</div>

<!-- Daily Charge Modal -->
<div class="modal" id="dailyChargeModal">
  <div class="modal-dialog">
    <div class="modal-content">

      <div class="modal-header">
        <h4 class="modal-title">Daily Charge</h4>
        <button type="button" class="close" data-dismiss="modal">&times;</button>
      </div>

      <div class="modal-body">
        <form action="{{url('getChargeByDate')}}" method="get">

            <input type="hidden" class="form-control" name="loanId" value="{{old('loanId')}}">

            <div class="form-group">
              <label for="date">Date:</label>
              <input class="form-control datepicker2" name="date" value="{{old('date')}}"/>
            </div>

            <button type="submit" class="btn btn-primary">Submit</button>
        </form>
      </div>

      <div class="modal-footer">
        <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
      </div>

    </div>
  </div>
</div>
@endsection

@section('pages-js')
<script>
    $(document).on("click", ".loanModal", function () {
        var loanId = $(this).data('loanid');
        $("input[name=loanId]").val(loanId);
    });

</script>
@endsection
