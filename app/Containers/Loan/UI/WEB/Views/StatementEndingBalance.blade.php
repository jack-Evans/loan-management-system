@extends('layouts.app')

@section('title', 'Month ending balances')

@section('content')
<div class="col-lg-12">
    <div class="has-bg">
    	<h3>Month ending balances</h3>
        <span class="space"></span>
        <!-- <div id="myGrid" style="height: 600px;width:100%;" class="ag-theme-material"></div> -->

        <div class="container">
        <div class="row">
            <div class="col-8">
                <div class="columns">
                    <div class="column">
                        <label class="option">
                            <input type="checkbox" id="columnWidth">
                            columnWidth =
                            <select id="columnWidthValue">
                                <option>100</option>
                                <option>200</option>
                                <option>myColumnWidthCallback</option>
                            </select>
                        </label>
                        <label class="option">
                            <input type="checkbox" id="sheetName">
                            sheetName =
                            <input type="text" id="sheetNameValue" value="custom-name" maxlength="31">
                        </label>
                        <label class="option">
                            <input type="checkbox" id="exportModeXml">
                            <span class="option-name">exportMode = "xml"</span>
                        </label>
                    </div>
                    <div class="column" style="margin-left: 30px">
                        <label class="option">
                            <input type="checkbox" id="suppressTextAsCDATA">
                            <span class="option-name">suppressTextAsCDATA</span>
                        </label>
                        <div class="option">
                            <label>
                                <input type="checkbox" id="rowHeight">
                                rowHeight =
                            </label>
                            <input type="text" id="rowHeightValue" value="30" style="width: 40px;">
                        </div>
                        <div class="option">
                            <label>
                                <input type="checkbox" id="headerRowHeight">
                                headerRowHeight =
                            </label>
                            <input type="text" id="headerRowHeightValue" value="40" style="width: 40px;">
                        </div>
                    </div>
                </div>
                <div style="margin: 5px;">
                    <label>
                        <button class="btn btn-primary" onclick="onBtExport()" style="margin: 5px; font-weight: bold;">Export to Excel</button>
                    </label>
                </div>
            </div>
            
            <div class="col-4">
                <form action="{{url('statement-ending-balance')}}" method="get">

                    <b class="text-info">Today made up date:</b>
                    <input class="form-control datepicker4 madeupdate" autocomplete="off" name="madeupdate" value="{{isset($_GET['madeupdate']) ? $_GET['madeupdate'] : ''}}">
                    
                    <button class="btn btn-primary mt-2" type="submit">Submit</button>         
                </form>
            </div>
        </div>

            <div class="grid-wrapper">
                <div id="myGrid" class="ag-theme-material" style="height: 600px;width:100%;"></div>
            </div>
        </div>
    </div>
</div>
@endsection

@section('pages-js')

<script>

    agGrid.LicenseManager.setLicenseKey("[TRIAL]_24_April_2020_[v2]_MTU4NzY4NjQwMDAwMA==4403d049826997713de83933aa326f78");
    // specify the columns
    var columnDefs = [
        {field: "sage id"},
        {field: "customer name"},
        {field: "loan type"},
        {field: "start date"},
        {field: "expiry date"},
        {field: "balance"},
        {field: "expiry date balance"},
        {field: "calculation date"},
        {field: "calculated interest"},
        {field: "interest days"},
        {field: "interest amount"},
        {field: "default interest days"},
        {field: "default interest amount"},
        // {field: "description"},
    ];

    var gridOptions = {
        defaultColDef: {
            cellClassRules: {
                lightGreyBackground: function(params) {
                    return params.rowIndex % 2 == 0;
                }
            },
            sortable: true,
            filter: true
        },
        columnDefs: columnDefs,
        rowSelection: 'multiple',

        excelStyles: [
            {
                id: 'greenBackground',
                interior: {
                    color: '#b5e6b5',
                    pattern: 'Solid'
                }
            },
            {
                id: 'redFont',
                font: {
                    fontName: 'Calibri Light',
                    underline: 'Single',
                    italic: true,
                    color: '#ff0000'
                }
            },
            {
                id: 'lightGreyBackground',
                interior: {
                    color: '#e8ebec',
                    pattern: 'Solid'
                },
                font: {
                    fontName: 'Calibri Light',
                    // color: '#ffffff'
                }
            },
            {
                id: 'boldBorders',
                borders: {
                    borderBottom: {
                        color: '#000000',
                        lineStyle: 'Continuous',
                        weight: 3
                    },
                    borderLeft: {
                        color: '#000000',
                        lineStyle: 'Continuous',
                        weight: 3
                    },
                    borderRight: {
                        color: '#000000',
                        lineStyle: 'Continuous',
                        weight: 3
                    },
                    borderTop: {
                        color: '#000000',
                        lineStyle: 'Continuous',
                        weight: 3
                    }
                }
            },
            {
                id: 'header',
                interior: {
                    color: '#CCCCCC',
                    pattern: 'Solid'
                },
                borders: {
                    borderBottom: {
                        color: '#5687f5',
                        lineStyle: 'Continuous',
                        weight: 1
                    },
                    borderLeft: {
                        color: '#5687f5',
                        lineStyle: 'Continuous',
                        weight: 1
                    },
                    borderRight: {
                        color: '#5687f5',
                        lineStyle: 'Continuous',
                        weight: 1
                    },
                    borderTop: {
                        color: '#5687f5',
                        lineStyle: 'Continuous',
                        weight: 1
                    }
                }
            },
            {
                id: 'dateFormat',
                dataType: 'dateTime',
                numberFormat: {
                    format: 'mm/dd/yyyy;@'
                }
            },
            {
                id: 'twoDecimalPlaces',
                numberFormat: {
                    format: '#,##0.00'
                }
            },
            {
                id: 'textFormat',
                dataType: 'string'
            },
            {
                id: 'bigHeader',
                font: {
                    size: 20
                }
            }
        ]
    };

    function getBooleanValue(cssSelector) {
        return document.querySelector(cssSelector).checked === true;
    }

    function getTextValue(cssSelector) {
        return document.querySelector(cssSelector).value;
    }

    function getNumericValue(cssSelector) {
        var value = parseFloat(getTextValue(cssSelector));
        if (isNaN(value)) {
            var message = "Invalid number entered in " + cssSelector + " field";
            alert(message);
            throw new Error(message);
        }
        return value;
    }

    function myColumnWidthCallback(params) {
        var originalWidth = params.column.getActualWidth();
        if (params.index < 7) {
            return originalWidth;
        }
        return 50;
    }

    function onBtExport() {
        var columnWidth = getBooleanValue('#columnWidth') ? getTextValue('#columnWidthValue') : undefined;
        var params = {
            columnWidth: columnWidth === 'myColumnWidthCallback' ? myColumnWidthCallback : parseFloat(columnWidth),
            sheetName: getBooleanValue('#sheetName') && getTextValue('#sheetNameValue'),
            exportMode: getBooleanValue('#exportModeXml') ? "xml" : undefined,
            suppressTextAsCDATA: getBooleanValue('#suppressTextAsCDATA'),
            rowHeight: getBooleanValue('#rowHeight') ? getNumericValue('#rowHeightValue') : undefined,
            headerRowHeight: getBooleanValue('#headerRowHeight') ? getNumericValue('#headerRowHeightValue') : undefined,
        };

        gridOptions.api.exportDataAsExcel(params);
    }

    // setup the grid after the page has finished loading
    document.addEventListener('DOMContentLoaded', function() {
        var gridDiv = document.querySelector('#myGrid');
        new agGrid.Grid(gridDiv, gridOptions);
        var url      = window.location.href;
        var dtype = url.split("madeupdate=");
        dtype = dtype.length > 0 ? dtype[1] : "";
        if(typeof dtype === "undefined")
            dtype = '';
        
        $('.madeupdate').val(dtype);

        $.ajax({
            url: "{{url('statement-ending-balance')}}?json=1&&madeupdate="+dtype,
            type: 'get',
            success: function(result){
                var arr = [];
                console.log(result);
                $(result.data).each(function(i, val) { 
                   arr.push(val);
                });
                console.log(arr);
                gridOptions.api.setRowData(arr);
            }, error: function(errors){
                console.log('error');
            }
        });
    });

    // $('.madeupdate').bind('datepicker4 change', function(){
    //     console.log($(this).val());
    //     window.location.href = "{{url('statement-ending-balance')}}?madeupdate=" + $(this).val();
    // });

</script>

@endsection