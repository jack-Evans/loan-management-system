@extends('layouts.app')

@section('title', 'Loan charges')

@section('content')
    <h3>Assign charges</h3>

            <table class="table text-left">
                <thead>
                    <tr>
                    <th>Name</th>
                    <th>Value</th>
                    <th>Charge Type</th>
                    <th>Amount Type</th>
                    <th colspan="2">Actions</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach($charges as $key => $charge)
                    <tr>
                        <td>{{$charge->name}}</td>
                        <td>{{$charge->value}}</td>
                        <td>{{$charge->chargetype}}</td>
                        <td>{{$charge->amounttype}}</td>
                        <td>
                            <a href="{{url('assignCharges?loan_id='.$_GET['loan_id'].'&charge_id='.$charge->id)}}">
                            <button class="btn btn-primary">Assign</button>
                            </a>
                        </td>
                    </tr>
                    @endforeach
                </tbody>
            </table>
            @include('includes.pagination', ['page' => 'charges'])
            <a href="{{url('loans')}}">
                <button class="btn btn-success">Back</button>
            </a>
            <span class="space"></span>
        <h3>Already assigned charges</h3>
            <table class="table text-left">
                <thead>
                    <tr>
                        <th>Name</th>
                        <th>Value</th>
                        <th>Charge Type</th>
                        <th>Amount Type</th>

                        <th colspan="2">Actions</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach($loancharges as $loancharge)
                    <tr>

                        <td>{{$loancharge->name}}</td>
                        <td>{{$loancharge->value}}</td>
                        <td>{{$loancharge->chargeType}}</td>
                        <td>{{$loancharge->amountType}}</td>
                        <td>
                            <a href="{{url('deleteLoanCharge/'.$_GET['loan_id'].'/'.$loancharge->id)}}">
                            <button class="btn btn-primary">Delete</button>
                            </a>
                        </td>

                    </tr>
                    @endforeach
                </tbody>
            </table>
            @include('includes.pagination', ['page' => 'charges'])
@endsection
