<?php

namespace App\Containers\Loan\UI\WEB\Controllers;

use App\Containers\Loan\UI\WEB\Requests\CreateLoanRequest;
use App\Containers\Loan\UI\WEB\Requests\DeleteLoanRequest;
use App\Containers\Loan\UI\WEB\Requests\GetAllLoansRequest;
use App\Containers\Loan\UI\WEB\Requests\FindLoanByIdRequest;
use App\Containers\Loan\UI\WEB\Requests\UpdateLoanRequest;
use App\Containers\Loan\UI\WEB\Requests\StoreLoanRequest;
use App\Containers\Loan\UI\WEB\Requests\EditLoanRequest;
use App\Containers\Loan\UI\WEB\Requests\IssueLoanRequest;
use App\Ship\Parents\Controllers\WebController;
use Apiato\Core\Foundation\Facades\Apiato;
use App\Containers\Loan\UI\WEB\Requests\CalculateLoanChargesRequest;
use App\Containers\Loan\UI\WEB\Requests\AssignLoanChargesRequest;
use App\Containers\Loan\UI\WEB\Requests\DailyChargeRequest;

/**
 * Class Controller
 *
 * @package App\Containers\Loan\UI\WEB\Controllers
 */
class Controller extends WebController
{
    /**
     * The assigned API PATH for this Controller
     *
     * @var string
     */
    
    protected $apiPath;
    
    public function __construct(){
        $this->apiPath = config('token-container.WEB_API_URL');
    }

    /**
     * Show all entities
     *
     * @param GetAllLoansRequest $request
     */
    public function index(GetAllLoansRequest $request)
    {
        $loans = $uniqueTypeLoans = $pagination = $charges = $closingCharges = $customers = $tags = [];
        // if(isset($request->loanType))
        //     session(['type' => $request->loanType]);

        // loans
        // $loansUrl = $this->apiPath.'loans';
        // try {
        //     $response = get($loansUrl, ['page' => $request->page, 'search' => 'type:'.session('type')]);
        //     $loans = $response->data;
        //     $pagination = $response->meta->pagination;
        // } catch (\Exception $e) {
        //     return exception($e);
        // }
        
        $tagsUrl = $this->apiPath.'tags';
        try{
            $response = get($tagsUrl, ['limit' => 0]);
            $tags = $response->data;
        } catch (\Exception $e) {
            return exception($e);
        }

        // all loans
        $loansUrl = $this->apiPath.'loans';
        try {
            $response = get($loansUrl, ['limit' => 0]);
            $loans = $response->data;
            if($request->json){
                $loansArray = [];
                // dd($loans);
                foreach ($loans as $key => $loan) {
                    $loan->loanId = $loan->loanid;
                    $loan->issueDate = \Carbon\Carbon::parse($loan->issueat->date)->format('Y M d');
                    $loan->netLoan = $loan->netloan;
                    $loan->grossLoan = $loan->interest->gross_loan;
                    $file = collect($loan->fileinfo)->where('type', 'file')->last(); 
                    $loan->lastFileImportDate = isset($file->create_date) ? \Carbon\Carbon::parse($file->create_date)->format('Y M d') : '';
                    $loan->loanType = $loan->loantype;
                    $loan->tags = $loanTags = implode(',', array_map(function($tag) { return $tag->name; }, $loan->tags));
                }
                return response()->json($loans);
            }
            // $allLoans = $response->data;
        } catch (\Exception $e) {
            return exception($e);
        }
        // $uniqueTypeLoans = collect($allLoans)->unique('type');

        // customers
        $customersUrl = $this->apiPath.'customers';
        try {
            $response = get($customersUrl, ['limit' => 0]);
            $customers = $response->data;
        } catch (\Exception $e) {
            return exception($e);
        }

        // charges
        $chargesUrl = $this->apiPath.'charges';
        try {
            $chargesResponse = get($chargesUrl, ['limit' => 0]);
            $charges = $chargesResponse->data;
        } catch (\Exception $e) {
            return exception($e);
        }

        $nominalCodesUrl = $this->apiPath.'nominalcodes';
        try{
            $response = get($nominalCodesUrl, ['limit' => 0]);
            $nominalCodes = $response->data;
        } catch (\Exception $e) {
            return exception($e);
        }

        return view('loan::loans', compact('loans', 'pagination', 'customers', 'charges', 'uniqueTypeLoans', 'nominalCodes', 'tags'));
    }

    /**
     * Show one entity
     *
     * @param FindLoanByIdRequest $request
     */
    public function show(FindLoanByIdRequest $request)
    {
        $loan = Apiato::call('Loan@FindLoanByIdAction', [$request]);

        // ..
    }

    /**
     * Create entity (show UI)
     *
     * @param CreateLoanRequest $request
     */
    public function create(CreateLoanRequest $request)
    {
        $url = config('token-container.WEB_API_URL').'customers';
        try {
            $response = get($url);
            $customers = $response->data;
        } catch (\Exception $e) {
            return exception($e);
        }
        return view('loan::createUpdate', compact('customers'));
    }

    /**
     * Add a new entity
     *
     * @param StoreLoanRequest $request
     */
    public function store(StoreLoanRequest $request)
    {
        $url = config('token-container.WEB_API_URL').'loans';
        
        $loanRequest = [
            'loanId'        => $request->loanId,
            'customerId'    => $request->customerId,
            'netloan'       => $request->netloan,
            'rate'          => $request->rate,
            'defaultRate'   => $request->defaultRate,
            'duration'      => $request->duration,
            'nominalCode'   => $request->nominalCode,
    	];
        
        try {
            $response = post($url, $loanRequest);
        } catch (\Exception $e) {
            return exception($e);
        }
        


        return redirect('loans');
    }

    /**
     * Edit entity (show UI)
     *
     * @param EditLoanRequest $request
     */
    public function edit(EditLoanRequest $request)
    {
        $url = config('token-container.WEB_API_URL').'loans/'.$request->id;
        $response = get($url);    
                
        $loan = isset($response->data) ? $response->data : array();

        return view('loan::createUpdate', compact('loan'));
    }

    /**
     * Update a given entity
     *
     * @param UpdateLoanRequest $request
     */
    public function update(UpdateLoanRequest $request)
    {
        $url = config('token-container.WEB_API_URL').'loans/'.$request->id;
        
        $loanRequest = [
            'loanId'        => $request->loanId,
            'customerId'    => $request->customerId,
            'netloan'       => $request->netloan,
            'rate'          => $request->rate,
            'defaultRate'   => $request->defaultRate,
            'duration'      => $request->duration,
    	];
        
        try{
            $response = patch($url, $loanRequest);
        } catch (\Exception $e) {
            return exception($e);
        }

        return redirect('loans/'.$request->id.'/edit');
    }

    /**
     * Delete a given entity
     *
     * @param DeleteLoanRequest $request
     */
    public function delete(DeleteLoanRequest $request)
    {
        $url = config('token-container.WEB_API_URL').'loans/'.$request->id;
        $response = delete($url);
        
        return redirect('loans');
    }
    
    /**
     * 
     * @return view
     */
    
    public function issueLoanView(){
        
        return view('loan::issueLoan');
        
    }

        /**
     * @param issueLoanRequest $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function issueLoanPost(IssueLoanRequest $request)
    {
        $url = config('token-container.WEB_API_URL').'issueloan/'.$request->loanId;        
        $loanRequest = [
            'issueAt' => $request->issueAt,
    	];
        
        try{
            $response = post($url, $loanRequest);
        } catch (\Exception $e) {
            return exception($e);
        }

        return redirect('loans');
    }
    
    public function calculateLoanCharge(CalculateLoanChargesRequest $request)
    {
        $url = config('token-container.WEB_API_URL').'calculateloancharges/'.$request->id;
        $charges = get($url);

        $payments_url = config('token-container.WEB_API_URL').'paymentdetails/'.$request->id;
        $payments_rsp = get($payments_url);
        $payments = $payments_rsp->data;
        
        return view('loan::calculateLoanCharges', compact('charges', 'payments'));
    }
    
    public function getChargeByDate(DailyChargeRequest $request)
    {
        $url = config('token-container.WEB_API_URL').'dailyinterest/'.$request->loanId.'?date='.$request->date;

        try{
            $dailyCharge = get($url);
        } catch (\Exception $e) {
            return exception($e);
        }
        
        return view('loan::dailyChargeView', compact('dailyCharge'));
    }
    
    public function createLoanCharges(DailyChargeRequest $request)
    {
        $url = config('token-container.WEB_API_URL').'dailyinterest/'.$request->loanId;
        $dailyChargeRequest = [
            'date' => $request->date,
        ];

        try{
            $response = post($url, $dailyChargeRequest);
        } catch (\Exception $e) {
            return exception($e);
        }
        
        return redirect('calculateloancharges/'.$request->loanId);
    }

    public function assignLoanCharge(AssignLoanChargesRequest $request)
    {
        $url = config('token-container.WEB_API_URL').'charges';        
        $response = get($url);
        $charges = $response->data;
        $pagination = $response->meta->pagination;
        
        $loancharges_url = config('token-container.WEB_API_URL').'loancharges/'.$_GET['loan_id'];
        $loancharges_rsp = get($loancharges_url);
        $loancharges = $loancharges_rsp->data;
        
        return view('loan::assignLoanCharges', compact('charges', 'pagination', 'loancharges'));
    }
    
    public function assignLoanChargePost(AssignLoanChargesRequest $request)
    {        
        $url = config('token-container.WEB_API_URL').'loancharges';        
        $assignChargeRequest = [
            'loanid' => $_GET['loan_id'],
            'chargeid' => $_GET['charge_id'],
    	];
        
        try{
            $response = post($url, $assignChargeRequest);
        } catch (\Exception $e) {
            return exception($e);
        }
        
        return redirect()->back();
    }

    public function importLoans(AssignLoanChargesRequest $request)
    {
        $url = config('token-container.WEB_API_URL').'importloans';
        $importRequest = [
            'isImport' => $request->isImport,
            'uploadAtSharepoint' => $request->uploadAtSharepoint,
            'byQueue' => $request->byQueue,
        ];

        // if upload from onedrive/sharepoint
        if(!empty($request->oneDriveFile)){
            $oneDriveFile = file_get_contents($request->oneDriveFile);

             $arr = [
                'name' => 'file',
                'filename' => $request->oneDriveFileName,
                // 'filename' => 'text.xlsx',
                'Mime-Type' => "a8jupplication/vnd.openxmlformats-officedocument.spreadsheetml.sheet",
                'contents' => $oneDriveFile,
                'headers' => ['Content-Type' => 'multipart/form-data']
            ];
            try{
                $params = $importRequest;
                $apiEndPoint = $url;
                if(!empty($params)) {
                    $encoded = http_build_query($params);
                    $apiEndPoint = $apiEndPoint .'?'.$encoded;
                }
                $client = new \GuzzleHttp\Client();
                $response_raw = $client->post($apiEndPoint, [
                    'multipart' => [$arr],
                    'headers' => [
                        'Accept' => 'application/json',
                        'Authorization' => session('auth_token'),
                    ],
                ]);
                $response = (string)$response_raw->getBody();
                $response = json_decode((string) $response);
                return response()->json($response);
            } catch (\Exception $e) {
                return response()->json(['code' => 400, 'message' => $e->getMessage()]);
            }
        }

        if(empty($_FILES['file']['name']))
            return response()->json(['code' => 401, 'message' => 'Please Upload A File!']);

        try{
            $response = postWithFile($url, $fileName = "file", $importRequest);
            return response()->json($response);
        } catch (\Exception $e) {
            return response()->json(['code' => 400, 'message' => $e->getMessage()]);
        }

        return response()->json(['code' => 400, 'message' => 'Internal server error.']);
    }

    public function exportLoansWeb(AssignLoanChargesRequest $request)
    {
        // $loan = Apiato::call('Statement@ExportLoansAction', [$request]);
        $url = config('token-container.WEB_API_URL').'exportloans';
        $exportRequest = [
            'loanIds' => $request->loanIds,
            'tagIds' => $request->tagIds,
            'uploadAtSharepoint' => $request->uploadAtSharepoint,
            'ignoredSheets' => $request->ignoredSheets,
            'date' => $request->date,
        ];
        
        try{
            post($url, $exportRequest);
            $file = 'KuflinkLoans.xlsx';
            if (file_exists($file)) {
                header('Content-Description: File Transfer');
                header('Content-Type: application/octet-stream');
                header('Content-Disposition: attachment; filename='.basename($file));
                header('Content-Transfer-Encoding: binary');
                header('Expires: 0');
                header('Cache-Control: must-revalidate, post-check=0, pre-check=0');
                header('Pragma: public');
                header('Content-Length: ' . filesize($file));
                ob_clean();
                flush();
                readfile($file);
                unlink($file);
                exit;
                return redirect()->back();
            } else {
                return redirect()->back()->withErrors('Error exporting loans. Please try again later.');
            }
        } catch (\Exception $e) {
            return exception($e);
        }
    }

    public function exportFileInfo(AssignLoanChargesRequest $request)
    {
        $url = $this->apiPath.'fileinfos/export';
        $exportFileinfoRequest = [
            'loanIds' => $request->loanIds,
            'tagIds' => $request->tagIds,
            'uploadAtSharepoint' => $request->uploadAtSharepoint,
        ];
        try{
            post($url, $exportFileinfoRequest);
            $file = 'ImportFileInfo.xlsx';
            if (file_exists($file)) {
                header('Content-Description: File Transfer');
                header('Content-Type: application/octet-stream');
                header('Content-Disposition: attachment; filename='.basename($file));
                header('Content-Transfer-Encoding: binary');
                header('Expires: 0');
                header('Cache-Control: must-revalidate, post-check=0, pre-check=0');
                header('Pragma: public');
                header('Content-Length: ' . filesize($file));
                ob_clean();
                flush();
                readfile($file);
                unlink($file);
                // exit;
                return redirect()->back();
            } else {
                return redirect()->back()->withErrors('Error exporting file info. Please try again later.');
            }
        } catch (\Exception $e) {
            return exception($e);
        }
    }

    public function monthStatements(AssignLoanChargesRequest $request)
    {
        $url = $this->apiPath.'monthstatementspdf';
        $request = [
            'loanId' => $request->loanId,
            'tagIds' => $request->tagIds,
            'date' => ($request->date) ? \Carbon\Carbon::parse($request->date)->format('M d Y') : false,
        ];
        try{
            post($url, $request);
        } catch (\Exception $e) {
            return exception($e);
        }
        return redirect()->back();
    }

    public function importQueueLogs(AssignLoanChargesRequest $request)
    {
        ini_set('memory_limit', '256M');
        $response = $remainingJobs = [];
        $jobs = \DB::table('jobs')->get();
        if(count($jobs) > 0){
            foreach ($jobs as $key => $job) {
                $payload = json_decode($job->payload);
                if($payload->displayName == 'App\Jobs\ProcessImport'){
                    $remainingJobs['job_id'] = $job->id;
                    $remainingJobs['sheet_name'] = unserialize($payload->data->command)->sheetName;
                    $response[] = $remainingJobs;
                }
            }
        }
        return response()->json($response);
    }

    public function loanTags(AssignLoanChargesRequest $request)
    {
        $url = $this->apiPath.'createloantag';
        $request = [
            'loanid' => $request->loanId,
            'tagid' => $request->tags,
        ];

        try{
            post($url, $request);
        } catch (\Exception $e) {
            return exception($e);
        }
        return redirect()->back();
    }
    public function monthStartingBalance(AssignLoanChargesRequest $request)
    {
        // $loan = Apiato::call('Statement@ExportLoansAction', [$request]);
        $url = config('token-container.WEB_API_URL').'month-starting-balance';
        // $exportRequest = [
        //     'loanIds' => $request->loanIds,
        //     'tagIds' => $request->tagIds,
        //     'uploadAtSharepoint' => $request->uploadAtSharepoint,
        //     'ignoredSheets' => $request->ignoredSheets,
        // ];
        
        try{
            get($url);
            // post($url, $exportRequest);
            $file = 'MonthStartingBalance.xlsx';
            if (file_exists($file)) {
                header('Content-Description: File Transfer');
                header('Content-Type: application/octet-stream');
                header('Content-Disposition: attachment; filename='.basename($file));
                header('Content-Transfer-Encoding: binary');
                header('Expires: 0');
                header('Cache-Control: must-revalidate, post-check=0, pre-check=0');
                header('Pragma: public');
                header('Content-Length: ' . filesize($file));
                ob_clean();
                flush();
                readfile($file);
                unlink($file);
                exit;
                return redirect()->back();
            } else {
                return redirect()->back()->withErrors('Error exporting month starting balance. Please try again later.');
            }
        } catch (\Exception $e) {
            return exception($e);
        }
    }

    public function verifyStatementDateStatus(AssignLoanChargesRequest $request)
    {
        $url = $this->apiPath.'verify-statement-date';

        try{
            get($url);
        } catch (\Exception $e) {
            return response()->json( ['code' => 200, 'message' => $e->getMessage()] );
        }
        return response()->json(['code' => 200, 'message' => 'success']);
    }

    public function retainedLoanSummary(AssignLoanChargesRequest $request)
    {
        return view('loan::retainedLoanSummary');
    }

    public function retainedLoanSummaryApi(AssignLoanChargesRequest $request)
    {
        $url = $this->apiPath.'retained-loan-summary';

        try{
            $response = get($url);
            return response()->json($response);
        } catch (\Exception $e) {
            return response()->json(['code' => 400, 'message' => $e->getMessage()]);
        }
    }
    
    public function statementEndingBalance(AssignLoanChargesRequest $request)
    {
        // dd($request->madeupdate);
        if($request->json){
            $url = $this->apiPath.'statement-ending-balance?date='.$request->madeupdate;

            try{
                $response = get($url);
                return response()->json($response);
            } catch (\Exception $e) {
                return response()->json(['code' => 400, 'message' => $e->getMessage()]);
            }
        }

        return view('loan::StatementEndingBalance');
    }
}
