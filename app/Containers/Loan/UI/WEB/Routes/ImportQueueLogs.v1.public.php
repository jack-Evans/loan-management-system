<?php

/** @var Route $router */
$router->get('import-queue-logs', [
    'as' => 'web_loan_import_queue_logs',
    'uses'  => 'Controller@importQueueLogs',
    'middleware' => [
      'auth:web',
    ],
]);
