<?php

/** @var Route $router */
$router->get('createloancharges', [
    'as' => 'web_loan_create',
    'uses'  => 'Controller@createLoanCharges',
    'middleware' => [
      'auth:web',
    ],
]);
