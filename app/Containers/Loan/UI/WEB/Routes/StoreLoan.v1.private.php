<?php

/** @var Route $router */
$router->post('loans/store', [
    'as' => 'web_loan_store',
    'uses'  => 'Controller@store',
    'middleware' => [
      'auth:web',
    ],
]);
