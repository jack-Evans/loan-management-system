<?php

/** @var Route $router */
$router->post('saveloancharge', [
    'as' => 'web_payment_store',
    'uses'  => 'Controller@saveLoanCharge',
    'middleware' => [
      'auth:web',
    ],
]);
