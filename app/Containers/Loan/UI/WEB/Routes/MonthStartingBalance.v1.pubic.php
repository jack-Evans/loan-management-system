<?php

/** @var Route $router */
$router->get('month-starting-balance', [
    'as' => 'web_loan_monthStatements',
    'uses'  => 'Controller@monthStartingBalance',
    'middleware' => [
      'auth:web',
    ],
]);
