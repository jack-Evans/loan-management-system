<?php

/** @var Route $router */
$router->get('loans/{id}', [
    'as' => 'web_loan_show',
    'uses'  => 'Controller@show',
    'middleware' => [
      'auth:web',
    ],
]);
