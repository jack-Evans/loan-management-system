<?php

/** @var Route $router */
$router->post('issueloan', [
    'as' => 'web_loan_create',
    'uses'  => 'Controller@issueLoanPost',
    'middleware' => [
      'auth:web',
    ],
]);
