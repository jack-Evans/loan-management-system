<?php

/** @var Route $router */
$router->get('calculateloancharges/{id}', [
    'as' => 'web_loan_create',
    'uses'  => 'Controller@calculateLoanCharge',
    'middleware' => [
      'auth:web',
    ],
]);
