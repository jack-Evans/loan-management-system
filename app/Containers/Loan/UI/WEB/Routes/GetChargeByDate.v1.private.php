<?php

/** @var Route $router */
$router->get('getChargeByDate', [
    'as' => 'web_payment_store',
    'uses'  => 'Controller@getChargeByDate',
    'middleware' => [
      'auth:web',
    ],
]);
