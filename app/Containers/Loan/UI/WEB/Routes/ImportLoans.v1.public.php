<?php

/** @var Route $router */
$router->post('calculations/importloans', [
    'as' => 'web_payment_store',
    'uses'  => 'Controller@importLoans',
    'middleware' => [
      'auth:web',
    ],
]);
