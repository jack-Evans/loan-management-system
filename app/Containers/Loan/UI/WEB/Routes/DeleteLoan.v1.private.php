<?php

/** @var Route $router */
$router->delete('loans/{id}/delete', [
    'as' => 'web_loan_delete',
    'uses'  => 'Controller@delete',
    'middleware' => [
      'auth:web',
    ],
]);
