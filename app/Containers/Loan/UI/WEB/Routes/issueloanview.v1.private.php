<?php

/** @var Route $router */
$router->get('issueloan', [
    'as' => 'web_loan_create',
    'uses'  => 'Controller@issueLoanView',
    'middleware' => [
      'auth:web',
    ],
]);
