<?php

/** @var Route $router */
$router->get('loans/create', [
    'as' => 'web_loan_create',
    'uses'  => 'Controller@create',
    'middleware' => [
      'auth:web',
    ],
]);
