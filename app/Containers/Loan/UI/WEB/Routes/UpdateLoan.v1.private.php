<?php

/** @var Route $router */
$router->post('loans/{id}/update', [
    'as' => 'web_loan_update',
    'uses'  => 'Controller@update',
    'middleware' => [
      'auth:web',
    ],
]);
