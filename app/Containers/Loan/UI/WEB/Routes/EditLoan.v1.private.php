<?php

/** @var Route $router */
$router->get('loans/{id}/edit', [
    'as' => 'web_loan_edit',
    'uses'  => 'Controller@edit',
    'middleware' => [
      'auth:web',
    ],
]);
