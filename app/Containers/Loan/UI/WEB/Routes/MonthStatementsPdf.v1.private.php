<?php

/** @var Route $router */
$router->post('monthstatements', [
    'as' => 'web_loan_monthStatements',
    'uses'  => 'Controller@monthStatements',
    'middleware' => [
      'auth:web',
    ],
]);
