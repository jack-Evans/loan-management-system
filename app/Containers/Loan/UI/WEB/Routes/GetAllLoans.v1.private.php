<?php

/** @var Route $router */
$router->get('loans', [
    'as' => 'web_loan_index',
    'uses'  => 'Controller@index',
    'middleware' => [
      'auth:web',
    ],
]);
