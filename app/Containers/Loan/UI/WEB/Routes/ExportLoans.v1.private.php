<?php

/** @var Route $router */
$router->post('loans/exportloans', [
    'as' => 'web_calculator_exportloans',
    'uses'  => 'Controller@exportLoansWeb',
    'middleware' => [
      'auth:web',
    ],
]);
