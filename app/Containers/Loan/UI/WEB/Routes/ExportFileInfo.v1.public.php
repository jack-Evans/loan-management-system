<?php

/** @var Route $router */
$router->post('loans/exportfileinfo', [
    'as' => 'web_calculator_edit',
    'uses'  => 'Controller@exportFileInfo',
    'middleware' => [
      'auth:web',
    ],
]);
