<?php

namespace App\Containers\Loan\Tasks;

use App\Containers\Loan\Data\Repositories\LoanRepository;
use App\Containers\Interest\Data\Repositories\InterestRepository;
use App\Containers\LoanCharge\Data\Repositories\LoanChargeRepository;
use App\Containers\Payment\Data\Repositories\JournalRepository;
use App\Containers\Payment\Data\Repositories\PostRepository;
use App\Containers\Payment\Data\Repositories\PaymentRepository;
use App\Containers\Payment\Data\Repositories\PaymentPlanRepository;
use App\Containers\Payment\Data\Repositories\DefaultRateRefundRepository;
use App\Containers\Payment\Data\Repositories\RefundRepository;
use App\Containers\OtherCharge\Data\Repositories\OtherChargeRepository;
use App\Containers\Statement\Data\Repositories\BreakdownRepository;
use App\Containers\RenewalCharge\Data\Repositories\RenewalChargeRepository;
use App\Containers\ClosingCharge\Data\Repositories\ClosingLoanChargeRepository;
use App\Containers\LoanExtras\Data\Repositories\LoanExtrasRepository;
use App\Containers\Option\Data\Repositories\OptionRepository;
use App\Containers\FileInfo\Data\Repositories\FileInfoRepository;
use App\Containers\Tag\Data\Repositories\LoanTagRepository;
use App\Containers\Tag\Data\Repositories\JournalTagRepository;
use App\Containers\Tranch\Data\Repositories\TranchRepository;
use App\Containers\Grace\Data\Repositories\GraceRepository;
use App\Ship\Exceptions\DeleteResourceFailedException;
use App\Ship\Parents\Tasks\Task;
use Exception;

class DeleteLoanTask extends Task
{

    protected $loanRepository;
    protected $interestRepository;
    protected $loanChargeRepository;
    protected $journalRepository;
    protected $postRepository;
    protected $paymentRepository;
    protected $paymentPlanRepository;
    protected $otherChargeRepository;
    protected $defaultRateRefundRepository;
    protected $refundRepository;
    protected $breakdownRepository;
    protected $renewalChargeRepository;
    protected $closingLoanChargeRepository;
    protected $loanExtrasRepository;
    protected $optionRepository;
    protected $fileInfoRepository;
    protected $loanTagRepository;
    protected $tranchRepository;
    protected $journalTagRepository;
    protected $graceRepository;

    public function __construct(LoanRepository $loanRepository, InterestRepository $interestRepository, LoanChargeRepository $loanChargeRepository, JournalRepository $journalRepository, PostRepository $postRepository, PaymentRepository $paymentRepository, PaymentPlanRepository $paymentPlanRepository, OtherChargeRepository $otherChargeRepository, DefaultRateRefundRepository $defaultRateRefundRepository, RefundRepository $refundRepository, BreakdownRepository $breakdownRepository, RenewalChargeRepository $renewalChargeRepository, ClosingLoanChargeRepository $closingLoanChargeRepository, LoanExtrasRepository $loanExtrasRepository, OptionRepository $optionRepository, FileInfoRepository $fileInfoRepository, LoanTagRepository $loanTagRepository, TranchRepository $tranchRepository, JournalTagRepository $journalTagRepository, GraceRepository $graceRepository)
    {
        $this->loanRepository           = $loanRepository;
        $this->interestRepository       = $interestRepository;
        $this->loanChargeRepository     = $loanChargeRepository;
        $this->journalRepository        = $journalRepository;
        $this->postRepository           = $postRepository;
        $this->paymentRepository        = $paymentRepository;
        $this->paymentPlanRepository    = $paymentPlanRepository;
        $this->otherChargeRepository    = $otherChargeRepository;
        $this->defaultRateRefundRepository    = $defaultRateRefundRepository;
        $this->refundRepository         = $refundRepository;
        $this->breakdownRepository      = $breakdownRepository;
        $this->renewalChargeRepository  = $renewalChargeRepository;
        $this->closingLoanChargeRepository    = $closingLoanChargeRepository;
        $this->loanExtrasRepository     = $loanExtrasRepository;
        $this->optionRepository         = $optionRepository;
        $this->fileInfoRepository         = $fileInfoRepository;
        $this->loanTagRepository         = $loanTagRepository;
        $this->tranchRepository         = $tranchRepository;
        $this->journalTagRepository         = $journalTagRepository;
        $this->graceRepository         = $graceRepository;
    }

    public function run($id)
    {
        try {
            \DB::beginTransaction();

            $this->loanRepository->delete($id);
            // $this->interestRepository->deleteWhere(['loan_id' => $id]);
            // $this->loanChargeRepository->deleteWhere(['loan_id' => $id]);
            // $journals = $this->journalRepository->findWhere(['loan_id' => $id]);
            // foreach ($journals as $journal) {
            //     $this->postRepository->deleteWhere(['journal_id' => $journal->id]);
            //     $this->journalTagRepository->deleteWhere(['journal_id' => $journal->id]);
            // }
            // $this->journalRepository->deleteWhere(['loan_id' => $id]);            
            // $this->paymentRepository->deleteWhere(['loan_id' => $id]);
            // $this->paymentPlanRepository->deleteWhere(['loan_id' => $id]);
            // $this->otherChargeRepository->deleteWhere(['loan_id' => $id]);
            // // $this->otherChargeRepository->deleteWhere(['loan_id' => $id]);
            // $this->defaultRateRefundRepository->deleteWhere(['loan_id' => $id]);
            // $this->refundRepository->deleteWhere(['loan_id' => $id]);
            // $this->breakdownRepository->deleteWhere(['loan_id' => $id]);
            // $this->renewalChargeRepository->deleteWhere(['loan_id' => $id]);
            // $this->closingLoanChargeRepository->deleteWhere(['loan_id' => $id]);
            // $this->loanExtrasRepository->deleteWhere(['loan_id' => $id]);
            // $this->optionRepository->deleteWhere(['loan_id' => $id]);
            // $this->fileInfoRepository->deleteWhere(['loan_id' => $id]);
            // $this->loanTagRepository->deleteWhere(['loan_id' => $id]);
            // $this->tranchRepository->deleteWhere(['loan_id' => $id]);
            // $this->graceRepository->deleteWhere(['loan_id' => $id]);
                        
            \DB::commit();
        }
        catch (Exception $exception) {
            throw new DeleteResourceFailedException();
        }
    }
}
