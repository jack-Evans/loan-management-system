<?php

namespace App\Containers\Loan\Tasks;

use App\Ship\Exceptions\CreateResourceFailedException;
use App\Ship\Parents\Tasks\Task;
use Exception;
use App\Containers\Loan\Models\Loan;
use Apiato\Core\Foundation\Facades\Apiato;
use App\Ship\Parents\Requests\Request;

class IssueLoanTask extends Task
{
    public function run(Loan $loan, Request $request)
    {        
        $debitAccountId     = $loan->customer->account->id;

        $dt                 = $loan->issue_at;
        $accountingPeriod   = "{$dt->format('M')} {$dt->year}";
   
        try {
            \DB::beginTransaction();
            $journalPosting = [
                'creditAccountId' => 0,
                'debitAccountId' => $debitAccountId,
                'type' => 'loan',
                'loanid' => $loan->id,
                'tnxDate' => $loan->issue_at,
                'amount' => $loan->net_loan,
                'accountingPeriod' => $accountingPeriod,
                'assetType' => 'loan',
                'description' => $request->description,
                'nominalCode' => $request->nominalCode,
                'payableId' => $loan->id,
                'payableType' => 'App\Containers\Loan\Models\Loan'
            ];
            $issueLoan = Apiato::call('Payment@CreateJournalPostingTask', [$journalPosting]);
            \DB::commit();

        }
        catch (Exception $exception) {
            \DB::rollback();
            throw new CreateResourceFailedException();
        }
    }
}
