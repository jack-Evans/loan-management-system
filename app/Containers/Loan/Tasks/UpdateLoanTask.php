<?php

namespace App\Containers\Loan\Tasks;

use App\Containers\Loan\Data\Repositories\LoanRepository;
use App\Containers\Interest\Data\Repositories\InterestRepository;
use App\Containers\Payment\Data\Repositories\JournalRepository;
use App\Containers\Payment\Data\Repositories\PostRepository;
use App\Containers\Payment\Models\Post;
use App\Containers\Payment\Models\PaymentPlan;
use App\Ship\Exceptions\UpdateResourceFailedException;
use App\Ship\Parents\Tasks\Task;
use Exception;

class UpdateLoanTask extends Task
{

    protected $loanRepository;
    protected $interestRepository;
    protected $journalRepository;
    protected $postRepository;

    public function __construct(LoanRepository $loanRepository, InterestRepository $interestRepository, JournalRepository $journalRepository, PostRepository $postRepository)
    {
        $this->loanRepository = $loanRepository;
        $this->interestRepository = $interestRepository;
        $this->journalRepository = $journalRepository;
        $this->postRepository = $postRepository;
    }

    public function run($id, array $loanData, array $interestData = [], array $postData = [])
    {
        try {
            \DB::beginTransaction();
            
            if(count($interestData)){
                $interest = $this->interestRepository->findWhere(['loan_id' => $id])->first();
                $this->interestRepository->update($interestData, $interest->id);
            }

            if(count($loanData))
                $this->loanRepository->update($loanData, $id);

            if(count($postData)){
                $journal = $this->journalRepository->findWhere(['loan_id' => $id, 'type' => 'loan'])->first();
                Post::where(['journal_id' => $journal->id])->update($postData);
            }

            $loan = $this->loanRepository->with('interest')->find($id);
            // $monthlyPMT = calculateMonthlyPMT($loan->interest->rate, $loan->net_loan);
            // PaymentPlan::where(['loan_id' => $id, 'type' => 'payment'])->update(['amount' => $monthlyPMT]);

            \DB::commit();
            return $loan;
        }
        catch (Exception $exception) {
            \DB::rollback();
            throw new UpdateResourceFailedException();
        }
    }
}
