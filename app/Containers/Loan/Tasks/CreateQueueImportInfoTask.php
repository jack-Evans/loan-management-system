<?php

namespace App\Containers\Loan\Tasks;

use App\Containers\Loan\Data\Repositories\QueueImportInfoRepository;
use App\Ship\Exceptions\CreateResourceFailedException;
use App\Ship\Parents\Tasks\Task;
use Exception;

class CreateQueueImportInfoTask extends Task
{

    protected $repository;

    public function __construct(QueueImportInfoRepository $repository)
    {
        $this->repository = $repository;
    }

    public function run(array $data)
    {
        try {
            return $this->repository->create($data);
        }
        catch (Exception $exception) {
            \DB::rollback();
            throw new CreateResourceFailedException();
        }
    }
}
