<?php

namespace App\Containers\Loan\Data\Repositories;

use App\Ship\Parents\Repositories\Repository;

/**
 * Class LoanRepository
 */
class LoanRepository extends Repository
{

    /**
     * @var array
     */
    protected $fieldSearchable = [
        'id' => '=',
        'type' => 'like',
        'loan_type' => 'like',
        // ...
    ];

}
