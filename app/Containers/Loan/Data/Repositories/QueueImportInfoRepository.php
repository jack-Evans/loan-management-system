<?php

namespace App\Containers\Loan\Data\Repositories;

use App\Ship\Parents\Repositories\Repository;

/**
 * Class QueueImportInfoRepository
 */
class QueueImportInfoRepository extends Repository
{

    /**
     * @var array
     */
    protected $fieldSearchable = [
        'id' => '=',
        // ...
    ];

}
