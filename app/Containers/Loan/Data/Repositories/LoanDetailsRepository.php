<?php

namespace App\Containers\Loan\Data\Repositories;

use App\Ship\Parents\Repositories\Repository;

/**
 * Class LoanDetailsRepository
 */
class LoanDetailsRepository extends Repository
{

    /**
     * @var array
     */
    protected $fieldSearchable = [
        'id' => '=',
        // ...
    ];

}
