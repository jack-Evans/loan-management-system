<?php

namespace App\Containers\Loan\Data\Repositories;

use App\Ship\Parents\Repositories\Repository;

/**
 * Class LoanStatusLogRepository
 */
class LoanStatusLogRepository extends Repository
{

    /**
     * @var array
     */
    protected $fieldSearchable = [
        'id' => '=',
        'loan_id' => '=',
        // ...
    ];

}
