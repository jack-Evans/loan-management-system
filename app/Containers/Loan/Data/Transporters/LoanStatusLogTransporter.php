<?php

namespace App\Containers\Loan\Data\Transporters;

use App\Ship\Parents\Transporters\Transporter;

class LoanStatusLogTransporter extends Transporter
{

    /**
     * @var array
     */
    protected $schema = [
        'type' => 'object',
        'properties' => [
            'loan_id'           => ['type' => 'integer'],
            'status'            => ['type' => 'string'],
            'status_change_date'=> ['type' => 'string'],
        ],
        'required'   => [
            // define the properties that MUST be set
            'loan_id',
            'status',
            'status_change_date',
        ],
        'default'    => [
            // provide default values for specific properties here
        ]
    ];
}
