<?php

namespace App\Containers\Loan\Data\Transporters;

use App\Ship\Parents\Transporters\Transporter;

class CreateLoanTransporter extends Transporter
{

    /**
     * @var array
     */
    protected $schema = [
        'type' => 'object',
        'properties' => [
            'loan_id'           => ['type' => 'string'],
            'customer_id'       => ['type' => 'integer'],
            'net_loan'          => ['type' => 'number'],
            'status'            => ['type' => 'string'],
        ],
        'required'   => [
            // define the properties that MUST be set
            'loan_id',
            'customer_id',
            'net_loan',
            'status',
        ],
        'default'    => [
            // provide default values for specific properties here
        ]
    ];
}
