<?php

namespace App\Containers\Loan\Data\Transporters;

use App\Ship\Parents\Transporters\Transporter;

class IssueLoanTransporter extends Transporter
{

    /**
     * @var array
     */
    protected $schema = [
        'type' => 'object',
        'properties' => [
            'id'                => ['type' => 'integer'],
            'issued_at'         => ['type' => 'string'],
            // 'statement_date'    => ['type' => 'string'],
            'status'            => ['type' => 'string'],
        ],
        'required'   => [
            // define the properties that MUST be set
        ],
        'default'    => [
            // provide default values for specific properties here
        ]
    ];
}
