<?php

namespace App\Containers\Loan\Data\Seeders;

use App\Ship\Parents\Seeders\Seeder;
use Apiato\Core\Foundation\Facades\Apiato;
use App\Containers\Loan\Models\Extras;

class ExtrasSeeder extends Seeder
{

    public function run()
    {	
        $verifyStatementDate = Extras::where('name', 'VerifyStatementDate')->first();
        if(is_null($verifyStatementDate)){
        	$extra = new Extras();
        	$extra->name = 'VerifyStatementDate';
        	$extra->value = 1;
        	$extra->save();
        }
    }
}