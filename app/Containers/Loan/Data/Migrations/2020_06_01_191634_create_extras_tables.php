<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateExtrasTables extends Migration
{

    /**
     * Run the migrations.
     */
    public function up()
    {
        Schema::create('extras', function (Blueprint $table) {

            $table->increments('id');
            $table->string('name')->default('name');
            $table->string('value')->default('value');
            $table->timestamps();
            //$table->softDeletes();

        });
    }

    /**
     * Reverse the migrations.
     */
    public function down()
    {
        Schema::dropIfExists('extras');
    }
}
