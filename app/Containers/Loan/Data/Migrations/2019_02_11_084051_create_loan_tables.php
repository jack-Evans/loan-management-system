<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateLoanTables extends Migration
{

    /**
     * Run the migrations.
     */
    public function up()
    {
        Schema::create('loans', function (Blueprint $table) {

            $table->increments('id');
            $table->string('loan_id')->unique();
            $table->unsignedInteger('customer_id');
            $table->float('net_loan', 12, 4);
            $table->enum('status', ['approved','processing', 'rejected'])->default('processing');
            $table->date('issue_at')->nullable();
            $table->timestamps();
            $table->softDeletes();
        });    }

    /**
     * Reverse the migrations.
     */
    public function down()
    {
        Schema::drop('loans');
    }
}
