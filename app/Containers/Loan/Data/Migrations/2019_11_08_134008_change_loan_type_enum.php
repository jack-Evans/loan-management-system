<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class ChangeLoanTypeEnum extends Migration
{

    /**
     * Run the migrations.
     */
    public function up()
    {
        \DB::statement("ALTER TABLE loans CHANGE loan_type loan_type ENUM('serviced', 'retained','manual', 'half', 'rolled', 'tranch', 'n-tranch')");
    }

    /**
     * Reverse the migrations.
     */
    public function down()
    {
        \DB::statement("ALTER TABLE loans CHANGE loan_type loan_type ENUM('serviced', 'retained','manual', 'half', 'rolled', 'tranch')");
    }
}
