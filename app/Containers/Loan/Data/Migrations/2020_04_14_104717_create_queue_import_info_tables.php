<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateQueueImportInfoTables extends Migration
{

    /**
     * Run the migrations.
     */
    public function up()
    {
        Schema::create('queue_import_infos', function (Blueprint $table) {

            $table->increments('id');
            $table->unsignedInteger('user_id')->nullable();
            $table->string('sheet_name')->nullable();
            $table->text('messages')->nullable();
            $table->timestamps();
            //$table->softDeletes();

        });
    }

    /**
     * Reverse the migrations.
     */
    public function down()
    {
        Schema::dropIfExists('queue_import_infos');
    }
}
