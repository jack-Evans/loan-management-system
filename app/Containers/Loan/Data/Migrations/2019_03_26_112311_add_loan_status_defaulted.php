<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class AddLoanStatusDefaulted extends Migration
{

    /**
     * Run the migrations.
     */
    public function up()
    {
         \DB::statement("ALTER TABLE loans CHANGE status status ENUM('approved','processing','rejected','defaulted', 'freezed', 'closed')");
     //     \DB::connection('tempdb')->statement("ALTER TABLE loans CHANGE status status ENUM('approved','processing','rejected','defaulted')");
    }

    /**
     * Reverse the migrations.
     */
    public function down()
    {
         \DB::statement("ALTER TABLE loans CHANGE status status ENUM('approved','processing','rejected')");
     //     \DB::connection('tempdb')->statement("ALTER TABLE loans CHANGE status status ENUM('approved','processing','rejected')");
    }
}
