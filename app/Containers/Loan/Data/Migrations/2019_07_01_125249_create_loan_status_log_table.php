<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateLoanStatusLogTable extends Migration
{

    /**
     * Run the migrations.
     */
    public function up()
    {
        Schema::create('loan_status_logs', function (Blueprint $table) {

            $table->increments('id');
            $table->string('loan_id');
            $table->enum('status', ['approved','processing', 'rejected', 'freezed', 'closed'])->default('approved');
            $table->date('status_change_date')->nullable();
            $table->timestamps();
            $table->softDeletes();
        });    }

    /**
     * Reverse the migrations.
     */
    public function down()
    {
        Schema::drop('loan_status_logs');
    }
}
