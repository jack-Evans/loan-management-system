<?php

namespace App\Containers\Loan\Actions;

use App\Ship\Parents\Actions\Action;
use App\Ship\Parents\Requests\Request;
use Apiato\Core\Foundation\Facades\Apiato;
use App\Containers\Loan\Data\Transporters\IssueLoanTransporter;
use App\Containers\Loan\Exceptions\LoadAlreadyIssuedException;

class IssueLoanAction extends Action
{
    public function run(Request $request)
    {

    	$loanOriginal = Apiato::call('Loan@FindLoanByIdTask', [$request->id]);
        
    	\DB::beginTransaction();

        if($loanOriginal->status != 'approved') {
            
            $issueLoanRequest = [
    		'issue_at' 	=> \Carbon\Carbon::parse($request->issueAt)->format('Y-m-d'),
    		'status'	=> 'approved',
            ];
            
            $loanUpdated = Apiato::call('Loan@UpdateLoanTask', [$request->id, (new IssueLoanTransporter($issueLoanRequest))->toArray()]);
//    		throw new LoadAlreadyIssuedException();
    
            // Issue loan journal and posting + assign charges       
            $journal = Apiato::call('Loan@IssueLoanTask', [$loanUpdated, $request]);
    	}
        
        $interest = Apiato::call('LoanCharge@ManageInterestAction', [$request]);
              
        // $paymentPlan = Apiato::call('Payment@CreatePaymentPlanTask', [$loan]);        
    	
    	\DB::commit();
    	return $interest;
    }
}
