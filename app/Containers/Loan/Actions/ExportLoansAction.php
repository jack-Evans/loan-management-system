<?php

namespace App\Containers\Loan\Actions;

use App\Ship\Parents\Actions\Action;
use App\Ship\Parents\Requests\Request;
use Apiato\Core\Foundation\Facades\Apiato;
use App\Containers\Payment\Exceptions\HTTPPreConditionFailedException;
// excel libraries
use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\Writer\Csv;
use PhpOffice\PhpSpreadsheet\Writer\Xlsx;
use PhpOffice\PhpSpreadsheet\Worksheet\Worksheet;
use PhpOffice\PhpSpreadsheet\Style\NumberFormat;
use App\Containers\Loan\Models\Loan;
use App\Containers\Tag\Models\LoanTag;
use App\Containers\LoanExtras\Models\IgnoredSheet;

class ExportLoansAction extends Action
{
    public function run(Request $request)
    {
    	if($request->tagIds){
	    	$loanTags = Apiato::call('Tag@GetAllLoansBasedOnTagsTask', [$request->tagIds]);
	    	if(count($loanTags) > 0){
	    		$ids = array();
		    	foreach ($loanTags as $key => $loanTag) {
		    		array_push($ids, $loanTag->loan_id);
		    	}
		    	$loans = Loan::whereIn('id', $ids)->orderBy('id', 'asc')->get();
	    	} else
	    		throw new \App\Containers\Payment\Exceptions\HTTPPreConditionFailedException('No any loan found with selected tags.');
    	} elseif($request->loanIds) {
	    	$loanIds = $request->loanIds;
	    	$ids = array();
	    	foreach ($loanIds as $key => $loanId) {
	    		array_push($ids, \Hashids::decode($loanId)[0]);
	    	}
	    	$loans = Loan::whereIn('id', $ids)->orderBy('id', 'asc')->get();
    	} else {
	    	$loans = Loan::all();
    	}

    	$fontBold = [
		    'font' => [
		        'bold' => true,
		    ],
		];

		$colorGray = [
		    'fill' => [
		        'fillType' => \PhpOffice\PhpSpreadsheet\Style\Fill::FILL_SOLID,
		        'color' => array('argb' => 'FFF3F3F3'),
		    ],
		];

    	$spreadsheet = new Spreadsheet();
		$spreadsheet->getProperties()->setCreator('lms.kuflink')
	      ->setLastModifiedBy('Admin')
	      ->setTitle('Exported Loans')
	      ->setSubject('Exported Loans')
	      ->setDescription('Exported Loans');	    
	    $spreadsheet->removeSheetByIndex(0);

    	if($request->ignoredSheets) {
    		$ignoredSheets = IgnoredSheet::all();
    		foreach ($ignoredSheets as $ignored) {
				
				if(strlen($ignored->sheet_name) > 30)
					$sheet = new Worksheet($spreadsheet, substr($ignored->sheet_name, 0, 30));
				else
					$sheet = new Worksheet($spreadsheet, $ignored->sheet_name);

				$spreadsheet->addSheet($sheet);

				$sheetData = json_decode($ignored->sheet_data);
			 	$sheet->fromArray($sheetData, NULL, "A1");
			}
		}

    	foreach ($loans as $key => $loan) {
    		$loanId = $loan->id;
    		$monthlyPmt = number_format(calculateMonthlyPMT($loan->interest->rate, $loan->interest->gross_loan), 2);
			// add loan info.
			if(strlen($loan->loan_id) > 30)
				$sheet = new Worksheet($spreadsheet, substr($loan->loan_id, 0, 30));
			else
				$sheet = new Worksheet($spreadsheet, $loan->loan_id);
			$spreadsheet->addSheet($sheet, $key);
			$sheet->setCellValue('A1', 'Sheet number');
			$sheet->setCellValue('B1', isset($loan->loanExtras) ? $loan->loanExtras->sheet_no : null);
			$sheet->setCellValue('A3', 'Variables');
			$sheet->setCellValue('A4', 'Name');
			$sheet->setCellValue('B4', $loan->loan_id);
			$sheet->setCellValue('A5', 'Loan term');
			$sheet->setCellValue('B5', $loan->interest->duration);
			$sheet->setCellValue('A6', 'Loan Start Date');
			$sheet->setCellValue('B6', $loan->issue_at->format('d/m/Y'))->getStyle("B6")->getNumberFormat()->setFormatCode(NumberFormat::FORMAT_DATE_DDMMYYYY);
			$sheet->setCellValue('A7', 'Rate');
			$sheet->setCellValue('B7', $loan->interest->rate."%");
			$sheet->setCellValue('A8', 'Expiry Date');
			$sheet->setCellValue('B8', addDecimalMonthsIntoDate($loan->interest->duration, $loan->issue_at)->copy()->subDay()->format('d/m/Y'))->getStyle("B8")->getNumberFormat()->setFormatCode(NumberFormat::FORMAT_DATE_DDMMYYYY);
			$sheet->setCellValue('A9', 'Exit fee');
			$sheet->setCellValue('B9', 'n/a');
			$sheet->setCellValue('A10', 'PMT Date');
			$sheet->setCellValue('B10', 'n/a');
			$sheet->setCellValue('A11', 'Monthly Pmt');
			$sheet->setCellValue('B11', $monthlyPmt);
			$sheet->setCellValue('A12', 'Renewal Fee');
			$sheet->setCellValue('B12', $monthlyPmt);
			$sheet->setCellValue('A13', 'Default rate');
			$sheet->setCellValue('B13', $loan->interest->default_rate."%");
			$sheet->setCellValue('A14', 'Interest');
			$sheet->setCellValue('B14', ucfirst($loan->loan_type));
			$sheet->setCellValue('A15', 'Holiday Auto Refund');
			$sheet->setCellValue('B15', ($loan->holiday_refund == 1) ? 'Yes' : 'No');
			$sheet->setCellValue('A17', 'Gross loan');
			$sheet->setCellValue('B17', $loan->interest->gross_loan)->getStyle("B17")->getNumberFormat()->setFormatCode(NumberFormat::FORMAT_NUMBER_COMMA_SEPARATED1);

			$renewals = Apiato::call('RenewalCharge@GetRenewalChargesByLoanIdTask', [$loanId]);
			$pmtPlans = Apiato::call('Interest@GetInterestsAsPmtPlanVersionsTask', [$loanId])->where('payment_plan_date', '!=', null);
			$merged = $renewals->merge($pmtPlans);
			$sorted = $merged->sortBy(function($item){
						if(isset($item->renew_date))
							return $item->renew_date;
						else
							return $item->payment_plan_date;
					});
			$r = 19; # r for row
			$vp = $vr = 1; # vr version renewal, vp version pmtPlan
			foreach ($sorted as $value) {
				if(isset($value->renew_date)){
					$sheet->setCellValue("A$r", "Renewal Date $vr");
					$sheet->setCellValue("B$r", $value->renew_date->format('d/m/Y'))->getStyle("B$r")->getNumberFormat()->setFormatCode(NumberFormat::FORMAT_DATE_DDMMYYYY);
					++$r;
					
					$sheet->setCellValue("A$r", "Renewal Term $vr");
					$sheet->setCellValue("B$r", $value->duration);
					++$r;
					
					$sheet->setCellValue("A$r", "Renewal Amount $vr");
					$sheet->setCellValue("B$r", number_format($value->value, 2));
					++$r;
					
					$sheet->setCellValue("A$r", "Renewal Expiry Date $vr");
					$sheet->setCellValue("B$r", $value->renew_end_date->format('d/m/Y'))->getStyle("B$r")->getNumberFormat()->setFormatCode(NumberFormat::FORMAT_DATE_DDMMYYYY);

					$vr++;
				} else {
					++$r;
					$sheet->setCellValue("A$r", "Payment Plan Amount $vp");
					$sheet->setCellValue("B$r", number_format(calculateMonthlyPMT($value->rate, $value->gross_loan), 2));
					++$r;

					$sheet->setCellValue("A$r", "Payment Plan Date $vp");
					$sheet->setCellValue("B$r", $value->payment_plan_date->format('d/m/Y'))->getStyle("B$r")->getNumberFormat()->setFormatCode(NumberFormat::FORMAT_DATE_DDMMYYYY);
					++$r;

					$sheet->setCellValue("A$r", "Payment Plan Term $vp");
					$sheet->setCellValue("B$r", $value->duration);
					++$r;

					$sheet->setCellValue("A$r", "Payment Plan Gross Amount $vp");
					$sheet->setCellValue("B$r", number_format($value->gross_loan, 2));
					++$r;

					$vp++;
				}
				$r++;
			}

			$vr = 1;
			foreach ($loan->grace as $grace) {
				$sheet->setCellValue("A$r", "Grace Days $vr");
				$sheet->setCellValue("B$r", $grace->days);
				++$r;
				
				$sheet->setCellValue("A$r", "Grace Start Date $vr");
				$sheet->setCellValue("B$r", $grace->start_date->format('d/m/Y'))->getStyle("B$r")->getNumberFormat()->setFormatCode(NumberFormat::FORMAT_DATE_DDMMYYYY);
				++$r;
				
				$sheet->setCellValue("A$r", "Grace End Date $vr");
				$sheet->setCellValue("B$r", $grace->end_date->format('d/m/Y'))->getStyle("B$r")->getNumberFormat()->setFormatCode(NumberFormat::FORMAT_DATE_DDMMYYYY);
				++$r;
				
				$sheet->setCellValue("A$r", "Grace Description $vr");
				$sheet->setCellValue("B$r", $grace->description);
				++$r;

				$vr++;
			}
			
			++$r;
			
			$sheet->setCellValue("A$r", 'Transactions');
			++$r;
			$sheet->setCellValue("A$r", 'Date');
			$sheet->setCellValue("B$r", 'Debit');
			$sheet->setCellValue("C$r", 'Credit');
			$sheet->setCellValue("D$r", 'Balance');
			$sheet->setCellValue("E$r", 'Description');
			$sheet->setCellValue("F$r", 'Type');
			$sheet->setCellValue("G$r", 'Nominal Code');
			$sheet->setCellValue("I$r", 'Transaction Id');
			$sheet->setCellValue("J$r", 'Hidden');
			$sheet->setCellValue("K$r", 'Tags');
			$sheet->setCellValue("L$r", 'Notes');
    		++$r;
    		$request->loanid = $loanId;
    		$statements = Apiato::call('Payment@GetAllStatementsAction', [$request]);
    		// here $r stands for $row
    		// $r = 18;
    		$refundDefaultInterest = 0;
    		foreach ($statements as $key => $statement) {
    			if($statement->description == "refund_default_interest")
            		$refundDefaultInterest += $statement->credit;

    			if($statement->description == "interest" && $refundDefaultInterest){
    				$statement->debit += $refundDefaultInterest;
    				$refundDefaultInterest = 0;
    			}

				$sheet->setCellValue("A$r", $statement->created_at->format('d/m/Y'))->getStyle("A$r")->getNumberFormat()->setFormatCode(NumberFormat::FORMAT_DATE_DDMMYYYY);
				$sheet->setCellValue("B$r", (!is_null($statement->debit)) ? $statement->debit : null)->getStyle("B$r")->getNumberFormat()->setFormatCode(NumberFormat::FORMAT_NUMBER_COMMA_SEPARATED1);
				$sheet->setCellValue("C$r", (!is_null($statement->credit)) ? $statement->credit : null)->getStyle("C$r")->getNumberFormat()->setFormatCode(NumberFormat::FORMAT_NUMBER_COMMA_SEPARATED1);
				$sheet->setCellValue("E$r", $statement->comments);
				$sheet->setCellValue("F$r", $statement->description);
				$sheet->setCellValue("G$r", $statement->nominalCode);
				if($key === 0) /* set balance as loan amount.(debit - credit) */
					$sheet->setCellValue("D$r", "=B$r-C$r")->getStyle("D$r")->getNumberFormat()->setFormatCode(NumberFormat::FORMAT_NUMBER_COMMA_SEPARATED1);
				else
					$sheet->setCellValue("D$r", "=D$p+B$r-C$r")->getStyle("D$r")->getNumberFormat()->setFormatCode(NumberFormat::FORMAT_NUMBER_COMMA_SEPARATED1);
				// $sheet->setCellValue("H$r", $statement->transactionId)->getStyle("H$r")->getNumberFormat()->setFormatCode(NumberFormat::FORMAT_NUMBER_COMMA_SEPARATED2);
				$sheet->setCellValue("I$r", $statement->transactionId);
				$sheet->setCellValue("J$r", ($statement->hide == 1 && $statement->hideTransaction == 1) ? 'Transaction' : ($statement->hide == 1 && $statement->hideTransaction != 1) ? 'Statement' : '' );
				if($statement->tags){
					$tags = [];
					foreach ($statement->tags as $tag) {
						array_push($tags, $tag->name);
					}
					$sheet->setCellValue("K$r", implode(',', $tags));
				}
				$sheet->setCellValue("L$r", $statement->notes);

				if($r%2 == 1)
		      		$spreadsheet->getActiveSheet()->getStyle("A$r:H$r")->applyFromArray($colorGray);

				$p = $r;
				$r++;
    		}
			
			if($loan->loanExtras){
				
				if($loan->loanExtras->note) {
					$loanExtras = json_decode($loan->loanExtras->note);
					foreach ($loanExtras as $extra) {
					 	$r++;
					 	$sheet->fromArray($extra, NULL, "A$r");
					}
				}

				if($loan->loanExtras->breakdown) {
					$loanExtras = json_decode($loan->loanExtras->breakdown);
					foreach ($loanExtras as $extra) {
					 	$r++;
					 	$sheet->fromArray($extra, NULL, "A$r");
					}
				}

				// dd(json_decode($loan->loanExtras->breakdown));
				// $sheet->fromArray([json_decode($loan->loanExtras->description)], NULL, "A$r");
				// dd($r, $loan->loanExtras);
			}

			if(count($loan->options)){
				++$r;
				$sheet->setCellValue("A$r", "Optional variable");
				$r++;
				foreach ($loan->options as $key => $option) {
					$sheet->setCellValue("A$r", $option->name);
					$sheet->setCellValue("B$r", $option->value);
					$sheet->setCellValue("C$r", ($option->create_date) ? $option->create_date->format('d/m/Y') : null)->getStyle("C$r")->getNumberFormat()->setFormatCode(NumberFormat::FORMAT_DATE_DDMMYYYY);
					
					$r++;
				}
			}
      		
      		$spreadsheet->getActiveSheet()->getStyle('A1')->applyFromArray($fontBold);
      		$spreadsheet->getActiveSheet()->getStyle('B1')->applyFromArray($fontBold);
      		$spreadsheet->getActiveSheet()->getStyle('A3')->applyFromArray($fontBold);
      		$spreadsheet->getActiveSheet()->getStyle('B4')->applyFromArray($fontBold);
      		// $spreadsheet->getActiveSheet()->getStyle('A16')->applyFromArray($fontBold);
      		// $spreadsheet->getActiveSheet()->getStyle('A17:H17')->applyFromArray($fontBold);

			foreach(range('A', 'H') as $columnID) {
			    $spreadsheet->getActiveSheet()->getColumnDimension($columnID)->setAutoSize(true);
			}
    	}
    	
		// after data is filled into 
		$maxWidth = 30;
		foreach ($spreadsheet->getAllSheets() as $sheet) {
		    $sheet->calculateColumnWidths();
		    foreach ($sheet->getColumnDimensions() as $colDim) {
		        if (!$colDim->getAutoSize()) {
		            continue;
		        }
		        $colWidth = $colDim->getWidth();
		        if ($colWidth > $maxWidth) {
		            $colDim->setAutoSize(false);
		            $colDim->setWidth($maxWidth);
		        }
		    }
		}

// dd('stop');
		$writer = new Xlsx($spreadsheet);
		$fileName = 'KuflinkLoans.xlsx';
		$writer->save($fileName);

		if($request->uploadAtSharepoint){
			$sharepoint = new \App\Classes\Sharepoint();
			$sharepoint->uploadToSharepoint(public_path()."/".$fileName, $fileName);
		}
		return $fileName;
    }
}