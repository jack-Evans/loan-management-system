<?php

namespace App\Containers\Loan\Actions;

use App\Ship\Parents\Actions\Action;
use App\Ship\Parents\Requests\Request;
use Apiato\Core\Foundation\Facades\Apiato;

class GetLoanMonthsAction extends Action
{
    public function run(Request $request)
    {
        $loan = Apiato::call('Loan@FindLoanByIdTask', [$request->id]);
        $request->loanid = $request->id;
        $lastStatement = Apiato::call('Payment@GetAllStatementsAction', [$request])->last();
        // $lastStatement = Apiato::call('Payment@GetStatementsTask', [$loan])->last();
        $loanMonths = [];
        $closingDate = addDecimalMonthsIntoDate($loan->interest->duration, $loan->issue_at)->copy()->subDay();
        $duration = $loan->interest->duration;
        if($closingDate->lt($lastStatement->created_at)){
            $closingDate = $lastStatement->created_at->copy()->addDay();
            $duration = $loan->issue_at->diffInMonths($closingDate) + 1;
        }
        for($i = 0; $i < $duration; $i++){
        	$statementDate = $loan->issue_at->copy()->startOfMonth()->addMonths($i);
        	$loanMonths[] = [
        		'statementDate' => $statementDate->format('M Y'),
        		'statementGenerated' => $lastStatement->created_at->gt($statementDate->copy()->endOfMonth()),
        	];
        }
        return $loanMonths;
    }
}
