<?php

namespace App\Containers\Loan\Actions;

use App\Ship\Parents\Actions\Action;
use App\Ship\Parents\Requests\Request;
use Apiato\Core\Foundation\Facades\Apiato;

class UpdateLoanAction extends Action
{
    public function run(Request $request)
    {
        $updateLoan = [];
        $updateInterest = [];
        $updatePost  = [];

        if($request->name)
            $updateLoan['loan_id'] = $request->name;

        if($request->netloan)
        	$updateLoan['net_loan'] = $updateInterest['principal_amount'] = $updatePost['amount'] =  $request->netloan;
        
        if($request->loanType){
            $updateLoan['loan_type'] = $request->loanType;
            $updateInterest['half_loan_duration'] = $request->halfLoanDuration;
        }

        if($request->type)
            $updateLoan['type'] = $request->type;
        
        if($request->holidayRefund != null)
            $updateLoan['holiday_refund'] = $request->holidayRefund;

        if($request->rate)
        	$updateInterest['rate'] = $request->rate;

        if($request->defaultRate)
        	$updateInterest['default_rate'] = $request->defaultRate;

        if($request->duration)
        	$updateInterest['duration'] = $request->duration;

        $loan = Apiato::call('Loan@UpdateLoanTask', [$request->id, $updateLoan, $updateInterest, $updatePost]);

        return $loan;
    }
}
