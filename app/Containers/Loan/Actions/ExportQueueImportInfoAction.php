<?php

namespace App\Containers\Loan\Actions;

use App\Ship\Parents\Actions\Action;
// use App\Ship\Parents\Requests\Request;
use Apiato\Core\Foundation\Facades\Apiato;
use App\Containers\Payment\Exceptions\HTTPPreConditionFailedException;
use App\Containers\Loan\Models\QueueImportInfo;
// excel libraries
use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\Writer\Csv;
use PhpOffice\PhpSpreadsheet\Writer\Xlsx;
use PhpOffice\PhpSpreadsheet\Worksheet\Worksheet;

class ExportQueueImportInfoAction extends Action
{
    public function run()
    {
  //   	$fontBold = [
		//     'font' => [
		//         'bold' => true,
		//     ],
		// ];

		// $colorGray = [
		//     'fill' => [
		//         'fillType' => \PhpOffice\PhpSpreadsheet\Style\Fill::FILL_SOLID,
		//         'color' => array('argb' => 'FFF3F3F3'),
		//     ],
		// ];

    	$spreadsheet = new Spreadsheet();
		$spreadsheet->getProperties()->setCreator('lms.kuflink')
	      ->setLastModifiedBy('LMS')
	      ->setTitle('Export Queue Import Info')
	      ->setSubject('Export Queue Import Info')
	      ->setDescription('Export Queue Import Info');
	    $spreadsheet->removeSheetByIndex(0);

	    // $queueImportInfos =  Apiato::call('Loan@GetAllQueueImportInfoTask');
	    $queueImportInfos = QueueImportInfo::all();
	    $dumySheetNumber = 1;
    	foreach ($queueImportInfos as $key => $info) {
			// add info.
			if(strlen($info->sheet_name) > 30)
				$sheet = new Worksheet($spreadsheet, substr($info->sheet_name, 0, 30));
			elseif(is_null($info->sheet_name)){
				$sheet = new Worksheet($spreadsheet, "$dumySheetNumber");
				$dumySheetNumber++;
			}
			else
				$sheet = new Worksheet($spreadsheet, $info->sheet_name);
			
			$spreadsheet->addSheet($sheet, $key);
			$sheet->setCellValue('A1', $info->sheet_name);
			// $sheet->setCellValue('A2', 'Messages');
			$messages = json_decode($info->messages);
			$r = 3;
			if(!empty($messages) && isset($messages->message)){
				foreach ($messages->message as $key => $msg) {
					$sheet->setCellValue("A$r", $msg);
					$r++;
				}
			}
		}
		$writer = new Xlsx($spreadsheet);
		$fileName = 'QueueImportInfo.xlsx';
		$writer->save("public/".$fileName);

		$sharepoint = new \App\Classes\Sharepoint();
		$sharepoint->uploadToSharepoint(public_path()."/$fileName", "Queue Import Info/$fileName");
		$sharepoint->unlinkFile($fileName);
		QueueImportInfo::truncate();

		return $fileName;
    }
}