<?php

namespace App\Containers\Loan\Actions;

use App\Ship\Parents\Actions\Action;
use App\Ship\Parents\Requests\Request;
use Apiato\Core\Foundation\Facades\Apiato;
use App\Containers\Loan\Models\Loan;
// excel libraries
use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\Writer\Csv;
use PhpOffice\PhpSpreadsheet\Writer\Xlsx;
use PhpOffice\PhpSpreadsheet\Worksheet\Worksheet;
use PhpOffice\PhpSpreadsheet\Style\NumberFormat;

class GetMonthStartingBalanceAction extends Action
{
    public function run(Request $request)
    {
    	// try{
	        $fontBold = [
			    'font' => [
			        'bold' => true,
			    ],
			];
	    	$spreadsheet = new Spreadsheet();
			$spreadsheet->getProperties()->setCreator('lms.kuflink')
		      ->setLastModifiedBy('Admin')
		      ->setTitle('Month Starting Balance')
		      ->setSubject('Month Starting Balance')
		      ->setDescription('Month Starting Balance Excel');
		    // $spreadsheet->removeSheetByIndex(0);
	        
	        $loans = Loan::all();
	        foreach($loans as $key => $loan){
	            $request->loanid = $loan->id;
	            $monthStatements = Apiato::call('Payment@GetMonthStatementsAction', [$request]);

	            if(strlen($loan->loan_id) > 30)
					$sheet = new Worksheet($spreadsheet, substr($loan->loan_id, 0, 30));
				else
					$sheet = new Worksheet($spreadsheet, $loan->loan_id);
				
				$spreadsheet->addSheet($sheet, $key);
				$sheet->setCellValue('A1', "Name");
				$sheet->setCellValue('B1', $loan->loan_id);
				$sheet->setCellValue('A2', "Start Date");
				$sheet->setCellValue('B2', "End Date");
				$sheet->setCellValue('C2', "Opening Balance");
				$sheet->setCellValue('D2', "Closing Balance");
	            
	            $r = 3;
	            foreach ($monthStatements as $statement) {
					$sheet->setCellValue("A$r", $statement->start_date);
					$sheet->setCellValue("B$r", $statement->end_date);
					$sheet->setCellValue("C$r", $statement->opening_balance);
					$sheet->setCellValue("D$r", $statement->closing_balance);
					$r++;
	            }

      			$spreadsheet->getActiveSheet()->getStyle('A1')->applyFromArray($fontBold);
      			$spreadsheet->getActiveSheet()->getStyle('A2')->applyFromArray($fontBold);
      			$spreadsheet->getActiveSheet()->getStyle('B2')->applyFromArray($fontBold);
      			$spreadsheet->getActiveSheet()->getStyle('C2')->applyFromArray($fontBold);
      			$spreadsheet->getActiveSheet()->getStyle('D2')->applyFromArray($fontBold);
	            
	            foreach(range('A', 'D') as $columnID) {
			    	$spreadsheet->getActiveSheet()->getColumnDimension($columnID)->setAutoSize(true);
				}

	        }
	        
	        // after data is filled into 
			$maxWidth = 50;
			foreach ($spreadsheet->getAllSheets() as $sheet) {
			    $sheet->calculateColumnWidths();
			    foreach ($sheet->getColumnDimensions() as $colDim) {
			        if (!$colDim->getAutoSize()) {
			            continue;
			        }
			        $colWidth = $colDim->getWidth();
			        if ($colWidth > $maxWidth) {
			            $colDim->setAutoSize(false);
			            $colDim->setWidth($maxWidth);
			        }
			    }
			}

			$sheetIndex = $spreadsheet->getIndex(
			    $spreadsheet->getSheetByName('Worksheet')
			);
			$spreadsheet->removeSheetByIndex($sheetIndex);

			$writer = new Xlsx($spreadsheet);
			$fileName = 'MonthStartingBalance.xlsx';
			$writer->save($fileName);

			if($request->uploadAtSharepoint){
				$sharepoint = new \App\Classes\Sharepoint();
				$sharepoint->uploadToSharepoint(public_path()."/".$fileName, $fileName);
			}
			return $fileName;
        // } catch (Exception $e) {
        // 	$e->getMessage();
        // }

    }
}