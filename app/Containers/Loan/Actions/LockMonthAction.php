<?php

namespace App\Containers\Loan\Actions;

use App\Ship\Parents\Actions\Action;
use App\Ship\Parents\Requests\Request;
use Apiato\Core\Foundation\Facades\Apiato;
use Carbon\Carbon;
use App\Containers\Payment\Data\Transporters\CreatePaymentTransporter;

class LockMonthAction extends Action
{
    public function run(Request $request)
    {
    	$paidAt = Carbon::parse($request->month)->addMonth()->startOfMonth();
    	// $paymentRequest = [
     //        'paid_at'           => $paidAt->format('Y-m-d'),
     //        'amount'            => 0,
     //        'loan_id'           => $request->loanId,
     //        'description'	    => "Statement for ".$request->month,
     //        'is_capital_reduction' => 0,
     //        'nominalCode'       => $request->nominalCode,
     //    ];

        $loan = Apiato::call('Loan@FindLoanByIdTask', [$request->loanId]); 
        Apiato::call('Statement@VerifyStatementDateTask', [$loan->id, $paidAt]);

        $debitAccountId  = $loan->customer->account->id;
        
        $request->loanid = $request->loanId;
        $request->date = $paidAt;
        $statements = Apiato::call('Payment@GetAllStatementsAction', [$request])->where('description', '=', 'interest')->all();
        foreach ($statements as $key => $statement) {
            if(is_null($statement->transactionId)){
                $tnxDate = $statement->created_at;
                $journalPosting = [
                    'creditAccountId' => $debitAccountId,
                    'debitAccountId' => 0,
                    'loanid' => $loan->id,
                    'tnxDate' => $tnxDate,
                    'amount' => $statement->debit,
                    'accountingPeriod' => $tnxDate->format('M Y'),
                    'type' => 'interest',
                    'assetType' => 'interest',
                    'description' => $statement->comments,
                    'nominalCode' => 1,
                    'payableId' => 0,
                    'payableType' => '',
                    'hide'  => 1
                ];
                $journalEntry = Apiato::call('Payment@CreateJournalPostingTask', [$journalPosting]);                
            } else
                continue;
        }

        $journalPosting = [
            'creditAccountId' => $debitAccountId,
            'debitAccountId' => 0,
            'loanid' => $loan->id,
            'tnxDate' => $paidAt,
            'amount' => 0,
            'accountingPeriod' => $paidAt->format('M Y'),
            'type' => 'credit_filler',
            'assetType' => 'credit_filler',
            'description' => "Statement for ".$request->month,
            'nominalCode' => $request->nominalCode,
            'payableId' => 0,
            'payableType' => '',
            'hide'  => 1
        ];
        $journalEntry = Apiato::call('Payment@CreateJournalPostingTask', [$journalPosting]);

        // $payment = Apiato::call('Payment@CreatePaymentTask', [(new CreatePaymentTransporter($paymentRequest))->toArray(), $loan]);
        
        return $journalEntry;
    }
}
