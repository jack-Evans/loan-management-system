<?php

namespace App\Containers\Loan\Actions;

use App\Ship\Parents\Actions\Action;
use App\Ship\Parents\Requests\Request;
use Apiato\Core\Foundation\Facades\Apiato;
use App\Containers\Loan\Data\Transporters\CreateLoanTransporter;
use App\Containers\Interest\Data\Transporters\CreateInterestTransporter;

class CreateLoanAction extends Action
{
    public function run(Request $request, $nonExcelRequest = true)
    {
    	$loanRequest = [
            'loan_id' 		=> $request->loanId,
            'customer_id'	=> $request->customerId,
            'net_loan'	    => $request->netloan,
            'loan_type'     => $request->loanType,
            'issue_at'		=> $request->issueAt,
            // 'statement_date'=> $request->statement_date,
            'holiday_refund'   => $request->holidayRefund,
            // 'status'		=> array_get($request, 'status', 'processing'),
            'status'        => 'processing',
    	];
// dd($loanRequest, $request->chargeId, $request->nominalCodeForCharges);
        if((int)$request->minTerm > (int)$request->duration)
            throw new \App\Containers\Payment\Exceptions\HTTPPreConditionFailedException("Minimum term can't be greater than loan duration.");
            
    	\DB::beginTransaction();

        $loan = Apiato::call('Loan@CreateLoanTask', [(new CreateLoanTransporter($loanRequest))->toArray()]);        

        $interestRequest = [
    		'duration'		    => $request->duration,
            'half_loan_duration'=> $request->duration2,
            'min_term'          => $request->minTerm,
            'monthly_payment'   => calculateMonthlyPMT($request->rate, $request->netloan),
    		'rate'			    => $request->rate,
    		'principal_amount'	=> $request->netloan,
    		'default_rate'		=> $request->defaultRate,
    		'loan_id'		    => $loan->id,
            // 'gross_loan'        => ($request->loanType === 'serviced' || $request->loanType === 'manual') ? $request->netloan : $request->grossloan,
            'gross_loan'        => in_array($request->loanType, ['serviced', 'rolled', 'manual']) ? $request->netloan : $request->grossloan,
    	];
        
        $interest = Apiato::call('interest@CreateInterestTask', [(new CreateInterestTransporter($interestRequest))->toArray()]);
        
        $request->id = $loan->id;
        Apiato::call('Loan@IssueLoanAction', [$request]);
        
        // $paymentPlan = Apiato::call('Payment@CreatePaymentPlanAction', [$loan]);
        if($nonExcelRequest){
            if($loanRequest['loan_type'] === 'retained' || $loanRequest['loan_type'] === 'half')
            {
                $chargeIds = $request->chargeId;
                $initialChargeValues = $request->initialChargeValue;
                $nominalCodeForCharges = $request->nominalCodeForCharges;
                $request->loanid = $loan->id;            
                foreach ($chargeIds as $key => $cId) {
                    if($initialChargeValues[$key] > 0){
                        $request->chargeid = \Hashids::decode($cId)[0];
                        $request->value = $initialChargeValues[$key];
                        $request->nominalCode = $nominalCodeForCharges[$key];
                        Apiato::call('LoanCharge@CreateLoanChargeAction', [$request]);
                    }
                }
            }

            $closingCharges = Apiato::call('ClosingCharge@GetAllClosingChargesTask')->where('default_apply', '=', 'apply');
            foreach ($closingCharges as $key => $charge) {
                $data = [
                    'loan_id' => $loan->id,
                    'closing_charge_id' => $charge->id,
                    'description' => $charge->name,
                    'value' => $charge->value,
                ];
                $closingloancharge = Apiato::call('ClosingCharge@CreateClosingLoanChargeTask', [$data]);
            }
            
            // Assign mandatory optional variables

            Apiato::call('Optionalvariable@AssignMandatoryOptionalVariablesAction', [$loan, $request]);

        }

        \DB::commit();
        
        return $loan;
    }
}
