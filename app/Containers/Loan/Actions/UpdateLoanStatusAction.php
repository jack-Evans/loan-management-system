<?php

namespace App\Containers\Loan\Actions;

use App\Ship\Parents\Actions\Action;
use App\Ship\Parents\Requests\Request;
use Apiato\Core\Foundation\Facades\Apiato;
use App\Containers\Payment\Exceptions\HTTPPreConditionFailedException;
use App\Containers\Loan\Data\Transporters\LoanStatusLogTransporter;
use Carbon\Carbon;
use App\Containers\Payment\Data\Transporters\CreatePaymentTransporter;

class UpdateLoanStatusAction extends Action
{
    public function run(Request $request)
    {
        $updateLoan = [
            'status' => $request->status,
            'status_change_date' => $request->updateDate,
        ];

        if($request->status != 'closed')
            Apiato::call('Statement@VerifyStatementDateTask', [$request->loanId, $request->updateDate]);
        $loan = Apiato::call('Loan@FindLoanByIdTask', [$request->loanId]);
        // if($loan->status == $request->status)
        //     throw new HTTPPreConditionFailedException("The loan is already ". $request->status.".");
        if($loan->status == 'closed')
            throw new HTTPPreConditionFailedException("This loan is closed.");
        $statements = Apiato::call('Payment@GetStatementsTask', [$loan]);
        \DB::beginTransaction();

        if($request->status == 'approved' && $loan->status == 'freezed'){
           $lastStatement = $statements->where('description', '!=', 'interest')->last();
            if(Carbon::parse($request->updateDate)->format('m') != $loan->status_change_date->format('m'))
                throw new HTTPPreConditionFailedException("You can only un-freeze loan within ".$loan->status_change_date->format('M Y'));
        }
        if($request->status == 'closed'){
            $lastPmtPlan = Apiato::call('Payment@GetPaymentPlanTask', [$request->loanId])->last();
            if(floatgte($lastPmtPlan->amount_paid, $lastPmtPlan->amount))
                $updateLoan['status_change_date'] = $statements->last()->created_at;
            else
                throw new HTTPPreConditionFailedException("You can't close this loan as the balance is greater than 0.");
            // $paymentRequest = [
            // 'paid_at'           => $request->updateDate,
            // 'amount'            => $pmtPlans->amount_pending,
            // 'loan_id'           => $request->loanId,
            // 'description'       => 'Final Payment',
            // 'is_capital_reduction' => 1
            // ];

            // $payment = Apiato::call('Payment@CreatePaymentTask', [(new CreatePaymentTransporter($paymentRequest))->toArray(), $loan]);
            // $updateLoan['status_change_date'] = $request->updateDate;
        }

        if($request->status == 'freezed')
            $updateLoan['status_change_date'] = Carbon::parse($updateLoan['status_change_date'])->copy()->startOfMonth()->format('Y-m-d');

        $loan = Apiato::call('Loan@UpdateLoanTask', [$request->loanId, $updateLoan]);
        // $updateLoan['loan_id'] = $request->loanId;
        // $loanStatusLog = Apiato::call('Loan@CreateLoanStatusLogTask', [(new LoanStatusLogTransporter($updateLoan))->toArray()]);

        \DB::commit();

        return $loan;
    }
}
