<?php

namespace App\Containers\Loan\Actions;

use App\Ship\Parents\Actions\Action;
use App\Ship\Parents\Requests\Request;
use Apiato\Core\Foundation\Facades\Apiato;

class GetRetainedLoanSummaryAction extends Action
{
    public function run(Request $request)
    {
        $loans = \App\Containers\Loan\Models\Loan::where('loan_type', 'retained')->get();

        return $loans;
    }
}
