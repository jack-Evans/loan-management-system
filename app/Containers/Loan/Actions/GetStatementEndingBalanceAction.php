<?php

namespace App\Containers\Loan\Actions;

use App\Ship\Parents\Actions\Action;
use App\Ship\Parents\Requests\Request;
use Apiato\Core\Foundation\Facades\Apiato;

class GetStatementEndingBalanceAction extends Action
{
    public function run(Request $request)
    {
        // $loans = \App\Containers\Loan\Models\Loan::where('loan_type', '!=', 'manual')->get();
        $loans = \App\Containers\Loan\Models\Loan::all();
        $requestDate = $request->date;
        $statements = [];

        foreach ($loans as $key => $loan) {
        	try {
	        	$request->loanid = $loan->id;
	        	$rate = $loan->interest->rate;
	        	$defaultRate = $loan->interest->default_rate;
	        	$request->date = $requestDate;
		    	$statement = Apiato::call('Payment@GetAllStatementsAction', [$request])->where('description', 'interest')->last();

		    	$statement->sageId = '';
		    	$sageId = $loan->options->where('name', 'sage user id')->first();
		    	if($sageId)
					$statement->sageId = $sageId->value;

		    	// $firstName = $loan->options->where('name', 'first name')->first();
		    	// $lastName = $loan->options->where('name', 'last name')->first();
		    	// $statement->name = '';
		    	// if($firstName)
		    	// 	$statement->name = $firstName->value;
		    	// if($lastName)
		    	// 	$statement->name = $statement->name.' '.$lastName->value;

		    	$statement->name = $loan->loan_id;

		    	$statement->loan_type = $loan->loan_type;
		    	$statement->issue_at = $loan->issue_at->format('m/d/Y');
		    	$statement->expiry_date = addDecimalMonthsIntoDate($loan->interest->duration, $loan->issue_at)->copy()->subDay()->format('m/d/Y');
		    	
		    	$request->date = $statement->expiry_date;
		    	
		    	$interestBreakDown = collect($statement->interestBreakDown);
		    	$statement->totalInterest = $interestBreakDown->sum('total');
		    	$statement->interestDays = $interestBreakDown->where('rate', $rate)->sum('days');
		    	$statement->interestAmount = $interestBreakDown->where('rate', $rate)->sum('total');

		    	$statement->defaultInterestDays = $interestBreakDown->where('rate', $defaultRate)->sum('days');
		    	$statement->defaultInterestAmount = $interestBreakDown->where('rate', $defaultRate)->sum('total');

		    	$expiryDateStatement = Apiato::call('Payment@GetAllStatementsAction', [$request])->where('description', 'interest')->last();
		    	$statement->expiryDateBalance = $expiryDateStatement->balance;
		    	array_push($statements, $statement);
        	} catch (Exception $e) {
        		continue;
        	}
        }

        return $statements;
    }
}
