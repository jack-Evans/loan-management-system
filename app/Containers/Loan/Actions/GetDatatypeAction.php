<?php

namespace App\Containers\Loan\Actions;

use App\Ship\Parents\Actions\Action;
use App\Ship\Parents\Requests\Request;
use Apiato\Core\Foundation\Facades\Apiato;
use App\Containers\Loan\Models\Loan;
use App\Containers\Tag\Models\LoanTag;
use App\Containers\Search\Models\Search;

class GetDatatypeAction extends Action
{
    public function run(Request $request)
    {
    	try{
	    	$loans = array();
	      if(is_null($request->searchTags) && !is_null($request->searchByQuery)){
	      	$searchId = $request->searchByQuery;
	      	$searchId = \Hashids::decode($searchId)[0];
	      	$search = Search::where('id', $searchId)->first();
	      	if(!is_null($search)){
	      		$request->searchTags = $search->tags;
	      		$request->operators = $search->operators;
	      	}
	      }

	    	if(is_null($request->searchTags))
	        $loans = Apiato::call('Loan@GetAllLoansTask', [], ['addRequestCriteria']);
	      else{
	      	$searchTags = (is_array($request->searchTags)) ? $request->searchTags : json_decode($request->searchTags);
	      	$operators = (is_array($request->operators)) ? $request->operators : json_decode($request->operators);
	      	$loanTags = LoanTag::all();
	    		
	    		$previousTag = array_splice($searchTags, 0, 1);
	    		$previousTag = \Hashids::decode($previousTag[0])[0];

	    		if(count($searchTags)>0){
		      	do{
		      		$firstTag = array_splice($searchTags, 0, 1);
		      		$tag1 = \Hashids::decode($firstTag[0])[0];
		      		// $tag2 = \Hashids::decode($firstTag[1])[0];
		      		$operator = array_splice($operators, 0, 1)[0];
							// var_dump($tag1);
		      		if($operator == 'and'){
		      			// $loanTags = $loanTags->where('tag_id', $previousTag)->where('tag_id', $tag1);
		      			$loanTags = $loanTags->filter(function($loanTags) use ($previousTag, $tag1) {
		      				return ($loanTags->tag_id == $previousTag) && ($loanTags->tag_id == $tag1);
		      			});
		      		}
		      		elseif($operator == 'or'){
		      			$loanTags = $loanTags->filter(function($loanTags) use ($previousTag, $tag1) {
		      				return ($loanTags->tag_id == $previousTag) || ($loanTags->tag_id == $tag1);
		      			});
		    			}
		      		else{
		      			$loanTags = $loanTags->filter(function($loanTags) use ($previousTag, $tag1) {
		      				return ($loanTags->tag_id == $previousTag) && ($loanTags->tag_id != $tag1);
		      			});
		      		}

		      		$previousTag = $tag1;
		      	} while (count($searchTags) > 0);
	      	} else {
	      		$loanTags = $loanTags->where('tag_id', $previousTag);
	      	}
	      	if(count($loanTags)){
	      		$loanIds = $loanTags->pluck('loan_id');
	      		$loans = Loan::distinct('id')->whereIn('id', $loanIds)->get();
	      		// dd($loans);
	      	}
	      }
	      	// dd($searchTags, $operators, $loanTags);
	        // dd($loans[0]->charges);
	      return $loans;
      } catch (\Exception $e) {
          throw new \Exception($e->getMessage(), 1);
      }
    }
}
