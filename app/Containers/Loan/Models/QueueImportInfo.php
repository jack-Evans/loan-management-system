<?php

namespace App\Containers\Loan\Models;

use App\Ship\Parents\Models\Model;

class QueueImportInfo extends Model
{
    protected $fillable = [
        'user_id',
        'sheet_name',
        'messages',
    ];

    protected $attributes = [

    ];

    protected $hidden = [

    ];

    protected $casts = [

    ];

    protected $dates = [
        'created_at',
        'updated_at',
    ];

    /**
     * A resource key to be used by the the JSON API Serializer responses.
     */
    protected $resourceKey = 'queue_import_infos';

}
