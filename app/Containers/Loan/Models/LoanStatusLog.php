<?php

namespace App\Containers\Loan\Models;

use App\Ship\Parents\Models\Model;

class LoanStatusLog extends Model
{
    protected $fillable = [
        'loan_id',
        'status',
        'status_change_date',
    ];

    protected $attributes = [

    ];

    protected $hidden = [

    ];

    protected $casts = [

    ];

    protected $dates = [
        'created_at',
        'updated_at',
        'status_change_date',
    ];

    /**
     * A resource key to be used by the the JSON API Serializer responses.
     */
    protected $resourceKey = 'loanstatuslogs';

    public function loan() {
        return $this->belongsTo(\App\Containers\Loan\Models\Loan::class);
    }
}
