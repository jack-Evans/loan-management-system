<?php

namespace App\Containers\Loan\Models;

use App\Ship\Parents\Models\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Loan extends Model
{
    use SoftDeletes;
    
    protected $fillable = [
        'loan_id',
        'customer_id',
        'net_loan',
        'loan_type',
        'type',
        'status',
        'issue_at',
        'holiday_refund',
        'status_change_date',
    ];

    protected $attributes = [

    ];

    protected $hidden = [

    ];

    protected $casts = [

    ];

    protected $dates = [
        'created_at',
        'updated_at',
        'issue_at',
        'status_change_date',
    ];

    /**
     * A resource key to be used by the the JSON API Serializer responses.
     */
    protected $resourceKey = 'loans';


    public function charges() {
        return $this->belongsToMany(\App\Containers\Charge\Models\Charge::class, 'loan_charges')->withPivot('value');
    }

    public function interest() {
        return $this->hasOne(\App\Containers\Interest\Models\Interest::class);
    }

    public function loanStatusLog() {
        return $this->hasMany(\App\Containers\Loan\Models\LoanStatusLog::class);
    }

    public function LoanDetail() {
        return $this->hasOne(\App\Containers\Loan\Models\LoanDetail::class);
    }
    
    public function customer() {
        return $this->belongsTo(\App\Containers\Customer\Models\Customer::class);
    }
    
    public function journal() {
        return $this->hasOne(\App\Containers\Payment\Models\Journal::class, 'loan_id')->with(['posts']);
    }

    public function loanExtras() {
        return $this->hasOne(\App\Containers\LoanExtras\Models\LoanExtras::class, 'loan_id');
    }
    
    public function tags() {
        return $this->belongsToMany(\App\Containers\Tag\Models\Tag::class, 'loan_tags', 'loan_id', 'tag_id');
    }

    public function options() {
        return $this->hasMany(\App\Containers\Option\Models\Option::class, 'loan_id');
    }

    public function tranch() {
        return $this->hasMany(\App\Containers\Tranch\Models\Tranch::class, 'loan_id');
    }

    public function fileInfo() {
        return $this->hasMany(\App\Containers\FileInfo\Models\FileInfo::class, 'loan_id');
    }

    public function renewalCharge() {
        return $this->hasMany(\App\Containers\RenewalCharge\Models\RenewalCharge::class, 'loan_id');
    }
    public function loanCharge() {
        return $this->hasMany(\App\Containers\LoanCharge\Models\LoanCharge::class, 'loan_id');
    }
    public function grace() {
        return $this->hasMany(\App\Containers\Grace\Models\Grace::class, 'loan_id');
    }

}
