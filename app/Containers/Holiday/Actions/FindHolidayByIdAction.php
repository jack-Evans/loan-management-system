<?php

namespace App\Containers\Holiday\Actions;

use App\Ship\Parents\Actions\Action;
use App\Ship\Parents\Requests\Request;
use Apiato\Core\Foundation\Facades\Apiato;

class FindHolidayByIdAction extends Action
{
    public function run(Request $request)
    {
        $holiday = Apiato::call('Holiday@FindHolidayByIdTask', [$request->id]);

        return $holiday;
    }
}
