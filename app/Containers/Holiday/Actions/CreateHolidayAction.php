<?php

namespace App\Containers\Holiday\Actions;

use App\Ship\Parents\Actions\Action;
use App\Ship\Parents\Requests\Request;
use Apiato\Core\Foundation\Facades\Apiato;

class CreateHolidayAction extends Action
{
    public function run(Request $request)
    {
        $holidayRequest = [
            'name' => $request->name,
            'holiday_date' => $request->holidayDate
        ];

        $holiday = Apiato::call('Holiday@CreateHolidayTask', [$holidayRequest]);

        return $holiday;
    }
}
