<?php

namespace App\Containers\Holiday\Actions;

use App\Ship\Parents\Actions\Action;
use App\Ship\Parents\Requests\Request;
use Apiato\Core\Foundation\Facades\Apiato;

class GetAllHolidaysAction extends Action
{
    public function run(Request $request)
    {
        $holidays = Apiato::call('Holiday@GetAllHolidaysTask', [], ['addRequestCriteria']);

        return $holidays;
    }
}
