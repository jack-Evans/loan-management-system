<?php

namespace App\Containers\Holiday\Actions;

use App\Ship\Parents\Actions\Action;
use App\Ship\Parents\Requests\Request;
use Apiato\Core\Foundation\Facades\Apiato;

class DeleteHolidayAction extends Action
{
    public function run(Request $request)
    {
        return Apiato::call('Holiday@DeleteHolidayTask', [$request->id]);
    }
}
