<?php

namespace App\Containers\Holiday\Actions;

use App\Ship\Parents\Actions\Action;
use App\Ship\Parents\Requests\Request;
use Apiato\Core\Foundation\Facades\Apiato;

class UpdateHolidayAction extends Action
{
    public function run(Request $request)
    {
        $data = $request->sanitizeInput([
            // add your request data here
        ]);

        $holiday = Apiato::call('Holiday@UpdateHolidayTask', [$request->id, $data]);

        return $holiday;
    }
}
