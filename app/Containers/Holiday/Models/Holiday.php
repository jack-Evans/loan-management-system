<?php

namespace App\Containers\Holiday\Models;

use App\Ship\Parents\Models\Model;

class Holiday extends Model
{
    protected $fillable = [
        'name',
        'holiday_date',
    ];

    protected $attributes = [

    ];

    protected $hidden = [

    ];

    protected $casts = [

    ];

    protected $dates = [
        'created_at',
        'updated_at',
    ];

    /**
     * A resource key to be used by the the JSON API Serializer responses.
     */
    protected $resourceKey = 'holidays';
}
