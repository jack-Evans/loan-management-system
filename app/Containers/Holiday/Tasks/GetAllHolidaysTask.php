<?php

namespace App\Containers\Holiday\Tasks;

use App\Containers\Holiday\Data\Repositories\HolidayRepository;
use App\Ship\Parents\Tasks\Task;

class GetAllHolidaysTask extends Task
{

    protected $repository;

    public function __construct(HolidayRepository $repository)
    {
        $this->repository = $repository;
    }

    public function run()
    {
        return $this->repository->paginate();
    }
}
