<?php

namespace App\Containers\Holiday\Tasks;

use App\Containers\Holiday\Data\Repositories\HolidayRepository;
use App\Ship\Exceptions\NotFoundException;
use App\Ship\Parents\Tasks\Task;
use Exception;

class FindHolidayByDateTask extends Task
{

    protected $repository;

    public function __construct(HolidayRepository $repository)
    {
        $this->repository = $repository;
    }

    public function run($date)
    {
        return $this->repository->findWhere(['holiday_date' => $date])->all();
    }
}
