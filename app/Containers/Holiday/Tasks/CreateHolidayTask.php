<?php

namespace App\Containers\Holiday\Tasks;

use App\Containers\Holiday\Data\Repositories\HolidayRepository;
use App\Ship\Exceptions\CreateResourceFailedException;
use App\Ship\Parents\Tasks\Task;
use Exception;

class CreateHolidayTask extends Task
{

    protected $repository;

    public function __construct(HolidayRepository $repository)
    {
        $this->repository = $repository;
    }

    public function run(array $data)
    {
        try {
            return $this->repository->create($data);
        }
        catch (Exception $exception) {
            throw new CreateResourceFailedException();
        }
    }
}
