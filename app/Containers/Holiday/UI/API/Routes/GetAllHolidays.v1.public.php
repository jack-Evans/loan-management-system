<?php

/**
 * @apiGroup           Holiday
 * @apiName            getAllHolidays
 *
 * @api                {GET} /v1/holidays Endpoint title here..
 * @apiDescription     Endpoint description here..
 *
 * @apiVersion         1.0.0
 * @apiPermission      none
 *
 * @apiParam           {String}  parameters here..
 *
 * @apiSuccessExample  {json}  Success-Response:
 * HTTP/1.1 200 OK
{
  // Insert the response of the request here...
}
 */

/** @var Route $router */
$router->get('holidays', [
    'as' => 'api_holiday_get_all_holidays',
    'uses'  => 'Controller@getAllHolidays',
    'middleware' => [
      'auth:api',
    ],
]);
