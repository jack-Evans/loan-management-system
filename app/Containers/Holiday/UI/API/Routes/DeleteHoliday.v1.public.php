<?php

/**
 * @apiGroup           Holiday
 * @apiName            deleteHoliday
 *
 * @api                {DELETE} /v1/holidays/:id Endpoint title here..
 * @apiDescription     Endpoint description here..
 *
 * @apiVersion         1.0.0
 * @apiPermission      none
 *
 * @apiParam           {String}  parameters here..
 *
 * @apiSuccessExample  {json}  Success-Response:
 * HTTP/1.1 200 OK
{
  // Insert the response of the request here...
}
 */

/** @var Route $router */
$router->delete('holidays/{id}', [
    'as' => 'api_holiday_delete_holiday',
    'uses'  => 'Controller@deleteHoliday',
    'middleware' => [
      'auth:api',
    ],
]);
