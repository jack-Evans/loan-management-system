<?php

/**
 * @apiGroup           Holiday
 * @apiName            findHolidayById
 *
 * @api                {GET} /v1/holidays/:id Endpoint title here..
 * @apiDescription     Endpoint description here..
 *
 * @apiVersion         1.0.0
 * @apiPermission      none
 *
 * @apiParam           {String}  parameters here..
 *
 * @apiSuccessExample  {json}  Success-Response:
 * HTTP/1.1 200 OK
{
  // Insert the response of the request here...
}
 */

/** @var Route $router */
$router->get('holidays/{id}', [
    'as' => 'api_holiday_find_holiday_by_id',
    'uses'  => 'Controller@findHolidayById',
    'middleware' => [
      'auth:api',
    ],
]);
