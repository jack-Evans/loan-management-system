<?php

/**
 * @apiGroup           Holiday
 * @apiName            updateHoliday
 *
 * @api                {PATCH} /v1/holidays/:id Endpoint title here..
 * @apiDescription     Endpoint description here..
 *
 * @apiVersion         1.0.0
 * @apiPermission      none
 *
 * @apiParam           {String}  parameters here..
 *
 * @apiSuccessExample  {json}  Success-Response:
 * HTTP/1.1 200 OK
{
  // Insert the response of the request here...
}
 */

/** @var Route $router */
$router->patch('holidays/{id}', [
    'as' => 'api_holiday_update_holiday',
    'uses'  => 'Controller@updateHoliday',
    'middleware' => [
      'auth:api',
    ],
]);
