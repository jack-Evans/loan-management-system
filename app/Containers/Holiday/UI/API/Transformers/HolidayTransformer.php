<?php

namespace App\Containers\Holiday\UI\API\Transformers;

use App\Containers\Holiday\Models\Holiday;
use App\Ship\Parents\Transformers\Transformer;

class HolidayTransformer extends Transformer
{
    /**
     * @var  array
     */
    protected $defaultIncludes = [

    ];

    /**
     * @var  array
     */
    protected $availableIncludes = [

    ];

    /**
     * @param Holiday $entity
     *
     * @return array
     */
    public function transform(Holiday $entity)
    {
        $response = [
            'object' => 'Holiday',
            'id' => $entity->getHashedKey(),
            'name' => $entity->name,
            'holiday_date' => $entity->holiday_date,
            'created_at' => $entity->created_at,
            'updated_at' => $entity->updated_at,

        ];

        $response = $this->ifAdmin([
            'real_id'    => $entity->id,
            // 'deleted_at' => $entity->deleted_at,
        ], $response);

        return $response;
    }
}
