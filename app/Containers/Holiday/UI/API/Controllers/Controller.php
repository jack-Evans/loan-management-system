<?php

namespace App\Containers\Holiday\UI\API\Controllers;

use App\Containers\Holiday\UI\API\Requests\CreateHolidayRequest;
use App\Containers\Holiday\UI\API\Requests\DeleteHolidayRequest;
use App\Containers\Holiday\UI\API\Requests\GetAllHolidaysRequest;
use App\Containers\Holiday\UI\API\Requests\FindHolidayByIdRequest;
use App\Containers\Holiday\UI\API\Requests\UpdateHolidayRequest;
use App\Containers\Holiday\UI\API\Transformers\HolidayTransformer;
use App\Ship\Parents\Controllers\ApiController;
use Apiato\Core\Foundation\Facades\Apiato;

/**
 * Class Controller
 *
 * @package App\Containers\Holiday\UI\API\Controllers
 */
class Controller extends ApiController
{
    /**
     * @param CreateHolidayRequest $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function createHoliday(CreateHolidayRequest $request)
    {
        $holiday = Apiato::call('Holiday@CreateHolidayAction', [$request]);

        return $this->created($this->transform($holiday, HolidayTransformer::class));
    }

    /**
     * @param FindHolidayByIdRequest $request
     * @return array
     */
    public function findHolidayById(FindHolidayByIdRequest $request)
    {
        $holiday = Apiato::call('Holiday@FindHolidayByIdAction', [$request]);

        return $this->transform($holiday, HolidayTransformer::class);
    }

    /**
     * @param GetAllHolidaysRequest $request
     * @return array
     */
    public function getAllHolidays(GetAllHolidaysRequest $request)
    {
        $holidays = Apiato::call('Holiday@GetAllHolidaysAction', [$request]);

        return $this->transform($holidays, HolidayTransformer::class);
    }

    /**
     * @param UpdateHolidayRequest $request
     * @return array
     */
    public function updateHoliday(UpdateHolidayRequest $request)
    {
        $holiday = Apiato::call('Holiday@UpdateHolidayAction', [$request]);

        return $this->transform($holiday, HolidayTransformer::class);
    }

    /**
     * @param DeleteHolidayRequest $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function deleteHoliday(DeleteHolidayRequest $request)
    {
        Apiato::call('Holiday@DeleteHolidayAction', [$request]);

        return $this->noContent();
    }
}
