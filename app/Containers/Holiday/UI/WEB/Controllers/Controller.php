<?php

namespace App\Containers\Holiday\UI\WEB\Controllers;

use App\Containers\Holiday\UI\WEB\Requests\CreateHolidayRequest;
use App\Containers\Holiday\UI\WEB\Requests\DeleteHolidayRequest;
use App\Containers\Holiday\UI\WEB\Requests\GetAllHolidaysRequest;
use App\Containers\Holiday\UI\WEB\Requests\FindHolidayByIdRequest;
use App\Containers\Holiday\UI\WEB\Requests\UpdateHolidayRequest;
use App\Containers\Holiday\UI\WEB\Requests\StoreHolidayRequest;
use App\Containers\Holiday\UI\WEB\Requests\EditHolidayRequest;
use App\Ship\Parents\Controllers\WebController;
use Apiato\Core\Foundation\Facades\Apiato;

/**
 * Class Controller
 *
 * @package App\Containers\Holiday\UI\WEB\Controllers
 */
class Controller extends WebController
{
    /**
     * The assigned API PATH for this Controller
     *
     * @var string
     */

    protected $apiPath;
    
    public function __construct(){
        $this->apiPath = config('token-container.WEB_API_URL');
    }

    /**
     * Show all entities
     *
     * @param GetAllHolidaysRequest $request
     */
    public function index(GetAllHolidaysRequest $request)
    {
        // holidays
        $holidays = [];

        $holidaysUrl = $this->apiPath.'holidays';
        try {
            $response = get($holidaysUrl, ['limit' => 0]);
            $holidays = $response->data;
        } catch (\Exception $e) {
            return exception($e);
        }

        return view('welcome::holiday_view', compact('holidays'));
    }

    /**
     * Show one entity
     *
     * @param FindHolidayByIdRequest $request
     */
    public function show(FindHolidayByIdRequest $request)
    {
        $holiday = Apiato::call('Holiday@FindHolidayByIdAction', [$request]);

        // ..
    }

    /**
     * Create entity (show UI)
     *
     * @param CreateHolidayRequest $request
     */
    public function create(CreateHolidayRequest $request)
    {
        $holiday = [];
        return view('welcome::create_holiday', compact('holiday'));
    }

    /**
     * Add a new entity
     *
     * @param StoreHolidayRequest $request
     */
    public function store(StoreHolidayRequest $request)
    {
        $holidayData = [
            'name' => $request->name,
            'holidayDate' => $request->holidayDate,
        ];
        $holidaysUrl = $this->apiPath.'holidays';
        try {
            $response = post($holidaysUrl, $holidayData);
        } catch (\Exception $e) {
            return exception($e);
        }

        return redirect('holidays');
    }

    /**
     * Edit entity (show UI)
     *
     * @param EditHolidayRequest $request
     */
    public function edit(EditHolidayRequest $request)
    {
        $holiday = Apiato::call('Holiday@GetHolidayByIdAction', [$request]);

        // ..
    }

    /**
     * Update a given entity
     *
     * @param UpdateHolidayRequest $request
     */
    public function update(UpdateHolidayRequest $request)
    {
        $holiday = Apiato::call('Holiday@UpdateHolidayAction', [$request]);

        // ..
    }

    /**
     * Delete a given entity
     *
     * @param DeleteHolidayRequest $request
     */
    public function delete(DeleteHolidayRequest $request)
    {
         $result = Apiato::call('Holiday@DeleteHolidayAction', [$request]);

         // ..
    }
}
