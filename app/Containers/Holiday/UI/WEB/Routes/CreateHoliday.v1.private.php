<?php

/** @var Route $router */
$router->get('holidays/create', [
    'as' => 'web_holiday_create',
    'uses'  => 'Controller@create',
    'middleware' => [
      'auth:web',
    ],
]);
