<?php

/** @var Route $router */
$router->post('holidays/store', [
    'as' => 'web_holiday_store',
    'uses'  => 'Controller@store',
    'middleware' => [
      'auth:web',
    ],
]);
