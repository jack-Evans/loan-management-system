<?php

/** @var Route $router */
$router->delete('holidays/{id}', [
    'as' => 'web_holiday_delete',
    'uses'  => 'Controller@delete',
    'middleware' => [
      'auth:web',
    ],
]);
