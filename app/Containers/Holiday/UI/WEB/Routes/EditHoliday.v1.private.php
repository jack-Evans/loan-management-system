<?php

/** @var Route $router */
$router->get('holidays/{id}/edit', [
    'as' => 'web_holiday_edit',
    'uses'  => 'Controller@edit',
    'middleware' => [
      'auth:web',
    ],
]);
