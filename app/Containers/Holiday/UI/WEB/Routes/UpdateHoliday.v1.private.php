<?php

/** @var Route $router */
$router->patch('holidays/{id}', [
    'as' => 'web_holiday_update',
    'uses'  => 'Controller@update',
    'middleware' => [
      'auth:web',
    ],
]);
