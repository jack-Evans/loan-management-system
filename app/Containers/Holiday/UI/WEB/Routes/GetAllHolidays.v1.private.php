<?php

/** @var Route $router */
$router->get('holidays', [
    'as' => 'web_holiday_index',
    'uses'  => 'Controller@index',
    'middleware' => [
      'auth:web',
    ],
]);
