<?php

/** @var Route $router */
$router->get('holidays/{id}', [
    'as' => 'web_holiday_show',
    'uses'  => 'Controller@show',
    'middleware' => [
      'auth:web',
    ],
]);
