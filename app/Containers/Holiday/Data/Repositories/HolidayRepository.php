<?php

namespace App\Containers\Holiday\Data\Repositories;

use App\Ship\Parents\Repositories\Repository;

/**
 * Class HolidayRepository
 */
class HolidayRepository extends Repository
{

    /**
     * @var array
     */
    protected $fieldSearchable = [
        'id' => '=',
        // ...
    ];

}
