<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Token Container
    |--------------------------------------------------------------------------
    |
    |
    |
    */
    'CLIENT_ID'     => env('CLIENT_ID'),
    'CLIENT_SECRET' => env('CLIENT_SECRET'),
    'WEB_API_URL'       => env('WEB_API_URL'),

];
