<?php

namespace App\Containers\Token\Actions;

use App\Ship\Parents\Actions\Action;
use App\Ship\Parents\Requests\Request;
use Apiato\Core\Foundation\Facades\Apiato;

class TokenAction extends Action
{
    public function run(Request $request)
    {
        $data = $request->sanitizeInput([
            'email',
            'password',
        ]);

        $requestData = [
        	'grant_type'    => 'password',
        	'username' 		=> $data['email'],
        	'password'		=> $data['password'],
        	'client_id'		=> config('token-container.CLIENT_ID'),
        	'client_secret' => config('token-container.CLIENT_SECRET'),
        	'scope' 		=> '*',
        ];
		
        return Apiato::call('Authentication@CallOAuthServerTask', [$requestData]);
    }
}
