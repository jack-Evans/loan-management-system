<?php

namespace App\Containers\Token\UI\API\Transformers;

use App\Containers\Token\Models\Token;
use App\Ship\Parents\Transformers\Transformer;

class TokenTransformer extends Transformer
{
    /**
     * @var  array
     */
    protected $defaultIncludes = [

    ];

    /**
     * @var  array
     */
    protected $availableIncludes = [

    ];

    /**
     * @param Token $entity
     *
     * @return array
     */
    public function transform(Token $entity)
    {
        $response = [
            'object' => 'Token',
            'id' => $entity->getHashedKey(),
            'created_at' => $entity->created_at,
            'updated_at' => $entity->updated_at,

        ];

        $response = $this->ifAdmin([
            'real_id'    => $entity->id,
            // 'deleted_at' => $entity->deleted_at,
        ], $response);

        return $response;
    }
}
