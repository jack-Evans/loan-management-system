<?php

namespace App\Containers\Token\UI\API\Controllers;

use App\Containers\Token\UI\API\Requests\TokenRequest;
use App\Ship\Parents\Controllers\ApiController;
use Apiato\Core\Foundation\Facades\Apiato;

/**
 * Class Controller
 *
 * @package App\Containers\Token\UI\API\Controllers
 */
class Controller extends ApiController
{
    /**
     * @param CreateTokenRequest $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function token(TokenRequest $request)
    {
        $token = Apiato::call('Token@TokenAction', [$request]);

        return $token;
    }
}
