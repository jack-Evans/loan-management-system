<?php

/**
 * @apiGroup           Token
 * @apiName            token
 *
 * @api                {POST} /v1/tokens grant user  token
 * @apiDescription     API will used for authentication
 *
 * @apiVersion         1.0.0
 * @apiPermission      none
 *
 * @apiParam           {String}  parameters here..
 *
 * @apiSuccessExample  {json}  Success-Response:
 * HTTP/1.1 200 OK
{
  // Insert the response of the request here...
}
 */

/** @var Route $router */
$router->post('tokens', [
    'as' => 'api_token_create_token',
    'uses'  => 'Controller@token',
    /*'middleware' => [
      'auth:api',
    ],*/
]);
