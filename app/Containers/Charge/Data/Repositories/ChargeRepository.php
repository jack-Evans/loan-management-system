<?php

namespace App\Containers\Charge\Data\Repositories;

use App\Ship\Parents\Repositories\Repository;

/**
 * Class ChargeRepository
 */
class ChargeRepository extends Repository
{

    /**
     * @var array
     */
    protected $fieldSearchable = [
        'id' => '=',
        // ...
    ];

}
