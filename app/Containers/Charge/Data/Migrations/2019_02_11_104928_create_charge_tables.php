<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateChargeTables extends Migration
{

    /**
     * Run the migrations.
     */
    public function up()
    {
        Schema::create('charges', function (Blueprint $table) {

            $table->increments('id');
            $table->string('name')->unique();
            $table->float('value', 12, 2);
            $table->enum('amount_type', ['netloan', 'principal_amount', 'grossloan'])->default('netloan')->nullable();
            $table->enum('charge_type', ['fixed', 'percentage'])->default('fixed');
            $table->timestamps();
            $table->softDeletes();

        });
             
    }

    /**
     * Reverse the migrations.
     */
    public function down()
    {
        Schema::drop('charges');
    }
}
