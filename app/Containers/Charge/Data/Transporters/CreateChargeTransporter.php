<?php

namespace App\Containers\Charge\Data\Transporters;

use App\Ship\Parents\Transporters\Transporter;

class CreateChargeTransporter extends Transporter
{

    /**
     * @var array
     */
    protected $schema = [
        'type' => 'object',
        'properties' => [
            'name'          => ['type' => 'string'],
            'value'         => ['type' => 'number'],
            'amount_type'   => ['type' => 'string'],
            'charge_type'   => ['type' => 'string'],
        ],
        'required'   => [
            // define the properties that MUST be set
            'name',
            'value',
            'amount_type',
            'charge_type',
        ],
        'default'    => [
            // provide default values for specific properties here
        ]
    ];
}
