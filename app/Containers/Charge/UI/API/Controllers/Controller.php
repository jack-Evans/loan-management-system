<?php

namespace App\Containers\Charge\UI\API\Controllers;

use App\Containers\Charge\UI\API\Requests\CreateChargeRequest;
use App\Containers\Charge\UI\API\Requests\DeleteChargeRequest;
use App\Containers\Charge\UI\API\Requests\GetAllChargesRequest;
use App\Containers\Charge\UI\API\Requests\FindChargeByIdRequest;
use App\Containers\Charge\UI\API\Requests\UpdateChargeRequest;
use App\Containers\Charge\UI\API\Transformers\ChargeTransformer;
use App\Ship\Parents\Controllers\ApiController;
use Apiato\Core\Foundation\Facades\Apiato;

/**
 * Class Controller
 *
 * @package App\Containers\Charge\UI\API\Controllers
 */
class Controller extends ApiController
{
    /**
     * @param CreateChargeRequest $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function createCharge(CreateChargeRequest $request)
    {
        $charge = Apiato::call('Charge@CreateChargeAction', [$request]);

        return $this->created($this->transform($charge, ChargeTransformer::class));
    }

    /**
     * @param FindChargeByIdRequest $request
     * @return array
     */
    public function findChargeById(FindChargeByIdRequest $request)
    {
        $charge = Apiato::call('Charge@FindChargeByIdAction', [$request]);

        return $this->transform($charge, ChargeTransformer::class);
    }

    /**
     * @param GetAllChargesRequest $request
     * @return array
     */
    public function getAllCharges(GetAllChargesRequest $request)
    {
        $charges = Apiato::call('Charge@GetAllChargesAction', [$request]);

        return $this->transform($charges, ChargeTransformer::class);
    }

    /**
     * @param UpdateChargeRequest $request
     * @return array
     */
    public function updateCharge(UpdateChargeRequest $request)
    {
        $charge = Apiato::call('Charge@UpdateChargeAction', [$request]);

        return $this->transform($charge, ChargeTransformer::class);
    }

    /**
     * @param DeleteChargeRequest $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function deleteCharge(DeleteChargeRequest $request)
    {
        Apiato::call('Charge@DeleteChargeAction', [$request]);

        return $this->noContent();
    }
}
