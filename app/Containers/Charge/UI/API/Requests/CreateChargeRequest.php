<?php

namespace App\Containers\Charge\UI\API\Requests;

use App\Ship\Parents\Requests\Request;

/**
 * Class CreateChargeRequest.
 */
class CreateChargeRequest extends Request
{

    /**
     * The assigned Transporter for this Request
     *
     * @var string
     */
    protected $transporter = \App\Containers\Charge\Data\Transporters\CreateChargeTransporter::class;

    /**
     * Define which Roles and/or Permissions has access to this request.
     *
     * @var  array
     */
    protected $access = [
        'permissions' => '',
        'roles'       => '',
    ];

    /**
     * Id's that needs decoding before applying the validation rules.
     *
     * @var  array
     */
    protected $decode = [
        // 'id',
    ];

    /**
     * Defining the URL parameters (e.g, `/user/{id}`) allows applying
     * validation rules on them and allows accessing them like request data.
     *
     * @var  array
     */
    protected $urlParameters = [
        // 'id',
    ];

    /**
     * @return  array
     */
    public function rules()
    {
        return [
            'name'             => 'required|max:50|string|unique:charges,name',
            'value'            => 'required|regex:/^\d*(\.\d{1,2})?$/',
            'amounttype'       => 'required_if:chargetype,percentage|in:netloan,principal_amount,grossloan',
            'chargetype'       => 'required|in:fixed,percentage',
        ];
    }

    /**
     * @return  bool
     */
    public function authorize()
    {
        return $this->check([
            'hasAccess',
        ]);
    }
}
