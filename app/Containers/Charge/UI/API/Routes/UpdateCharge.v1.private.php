<?php

/**
 * @apiGroup           Charge
 * @apiName            updateCharge
 *
 * @api                {PATCH} /v1/charges/:id Endpoint title here..
 * @apiDescription     Endpoint description here..
 *
 * @apiVersion         1.0.0
 * @apiPermission      none
 *
 * @apiParam           {String}  parameters here..
 *
 * @apiSuccessExample  {json}  Success-Response:
 * HTTP/1.1 200 OK
{
  // Insert the response of the request here...
}
 */

/** @var Route $router */
$router->patch('charges/{id}', [
    'as' => 'api_charge_update_charge',
    'uses'  => 'Controller@updateCharge',
    'middleware' => [
      'auth:api',
    ],
]);
