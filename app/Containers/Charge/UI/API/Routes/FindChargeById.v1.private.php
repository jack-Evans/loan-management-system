<?php

/**
 * @apiGroup           Charge
 * @apiName            findChargeById
 *
 * @api                {GET} /v1/charges/:id Endpoint title here..
 * @apiDescription     Endpoint description here..
 *
 * @apiVersion         1.0.0
 * @apiPermission      none
 *
 * @apiParam           {String}  parameters here..
 *
 * @apiSuccessExample  {json}  Success-Response:
 * HTTP/1.1 200 OK
{
  // Insert the response of the request here...
}
 */

/** @var Route $router */
$router->get('charges/{id}', [
    'as' => 'api_charge_find_charge_by_id',
    'uses'  => 'Controller@findChargeById',
    'middleware' => [
      'auth:api',
    ],
]);
