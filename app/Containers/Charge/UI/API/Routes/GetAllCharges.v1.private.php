<?php

/**
 * @apiGroup           Charge
 * @apiName            getAllCharges
 *
 * @api                {GET} /v1/charges Endpoint title here..
 * @apiDescription     Endpoint description here..
 *
 * @apiVersion         1.0.0
 * @apiPermission      none
 *
 * @apiParam           {String}  parameters here..
 *
 * @apiSuccessExample  {json}  Success-Response:
 * HTTP/1.1 200 OK
{
  // Insert the response of the request here...
}
 */

/** @var Route $router */
$router->get('charges', [
    'as' => 'api_charge_get_all_charges',
    'uses'  => 'Controller@getAllCharges',
    'middleware' => [
      'auth:api',
    ],
]);
