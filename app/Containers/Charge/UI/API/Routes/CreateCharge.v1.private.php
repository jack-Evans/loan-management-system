<?php

/**
 * @apiGroup           Charge
 * @apiName            createCharge
 *
 * @api                {POST} /v1/charges Endpoint title here..
 * @apiDescription     Endpoint description here..
 *
 * @apiVersion         1.0.0
 * @apiPermission      none
 *
 * @apiParam           {String}  parameters here..
 *
 * @apiSuccessExample  {json}  Success-Response:
 * HTTP/1.1 200 OK
{
  // Insert the response of the request here...
}
 */

/** @var Route $router */
$router->post('charges', [
    'as' => 'api_charge_create_charge',
    'uses'  => 'Controller@createCharge',
    'middleware' => [
      'auth:api',
    ],
]);
