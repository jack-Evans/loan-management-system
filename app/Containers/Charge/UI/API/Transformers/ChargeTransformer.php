<?php

namespace App\Containers\Charge\UI\API\Transformers;

use App\Containers\Charge\Models\Charge;
use App\Ship\Parents\Transformers\Transformer;

class ChargeTransformer extends Transformer
{
    /**
     * @var  array
     */
    protected $defaultIncludes = [

    ];

    /**
     * @var  array
     */
    protected $availableIncludes = [

    ];

    /**
     * @param Charge $entity
     *
     * @return array
     */
    public function transform(Charge $entity)
    {
        $response = [
            'object'        => 'Charge',
            'id'            => $entity->getHashedKey(),
            'name'         => $entity->name,
            'value'         => $entity->value,
            'amounttype'    => $entity->amount_type,
            'chargetype'    => $entity->charge_type,
            'created_at'    => $entity->created_at,
            'updated_at'    => $entity->updated_at,

        ];

        $response = $this->ifAdmin([
            'real_id'    => $entity->id,
            'deleted_at' => $entity->deleted_at,
        ], $response);

        return $response;
    }
}
