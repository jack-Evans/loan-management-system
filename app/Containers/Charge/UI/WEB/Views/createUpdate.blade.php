@extends('layouts.app')

@section('title', 'Initial Charges')

@section('content')
<div class="col-lg-12">
    <div class="has-bg">
      <div class="row">
          <div class="col-md-5 col-md-offset-2">
          <h3>{{isset($charge->id) ? 'Edit ' : 'Create '}}Initial Charge</h3>
              <form action="{{url(isset($charge->id) ? 'charges/'.$charge->id.'/update': 'charges/store' )}}" method="post">
                  @csrf()
                  <div class="form-group">
                    <label for="name">Name:</label>
                    <input type="text" class="form-control" name="name"
                           value="{{isset($charge->name) ? $charge->name : old('name')}}">
                  </div>
                  <div class="form-group">
                    <label for="value">Value:</label>
                    <input type="text" class="form-control" name="value"
                           value="{{isset($charge->value) ? (float)$charge->value : old('value')}}">
                  </div>
                  <div class="form-group">
                    
                    <label for="chargetype">Charge Type:</label>
                    <!-- <input type="text" class="form-control" name="chargetype"
                           value="{{isset($charge->chargetype) ? $charge->chargetype : old('chargetype')}}"> -->
                    
                    <select class="form-control" name="chargetype">
                      <option value="fixed">Fixed</option>
                      <option value="percentage">Percentage</option>
                    </select> 

                  </div>                              
              
                  <div class="form-group">
                    <label for="amounttype">Amount Type </label>
                    <!-- <input type="text" class="form-control" name="amounttype"
                           value="{{isset($charge->amounttype) ? $charge->amounttype : old('amounttype')}}"> -->

                    <select class="form-control" name="amounttype">
                      <option value="netloan">Net Loan</option>
                    </select>

                  </div>
                  <button type="submit" class="btn btn-primary">Submit</button>
              </form>
          </div>
      </div>
    </div>
</div>
@endsection
