@extends('layouts.app')

@section('title', 'Initial Charges')

@section('content')
<div class="col-lg-12">
    <div class="has-bg">
        <h3>
            Initial Charges
            <a href="{{url('charges/create')}}" class="pull-right">
                <button class="btn btn-success">Create Initial Charge</button>
            </a>
        </h3>
        <span class="space"></span>
        <table class="table table-striped table-hover table-bordered text-left MDBootstrapDatatable" cellspacing="0" width="100%">
            <thead>
                <tr>
                    <th>Name</th>
                    <th>Value</th>
                    <th>Charge Type</th>
                    <th>Amount Type</th>
                    <th>Actions</th>
                </tr>
            </thead>
            <tbody>
                @foreach($charges as $key => $charge)
                <tr>
                    <td class="text-capitalize">{{$charge->name}}</td>
                    <td>{{(float)$charge->value}}</td>
                    <td>{{$charge->chargetype}}</td>
                    <td>{{$charge->amounttype}}</td>
                    <td>
                        <a href="{{url('charges/'.$charge->id.'/edit')}}">
                        <button class="btn btn-primary">Edit</button>
                        </a>
                        <a href="{{ url('charges/'.$charge->id).'/delete' }}">
                            <button class="btn btn-danger">Delete</button>
                        </a>
                    </td>
                </tr>
                @endforeach
            </tbody>
        </table>
    </div>
</div>
@endsection
