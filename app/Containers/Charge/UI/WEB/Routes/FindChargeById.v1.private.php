<?php

/** @var Route $router */
$router->get('charges/{id}', [
    'as' => 'web_charge_show',
    'uses'  => 'Controller@show',
    'middleware' => [
      'auth:web',
    ],
]);
