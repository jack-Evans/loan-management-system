<?php

/** @var Route $router */
$router->get('charges/{id}/delete', [
    'as' => 'web_charge_delete',
    'uses'  => 'Controller@delete',
    'middleware' => [
      'auth:web',
    ],
]);
