<?php

/** @var Route $router */
$router->get('charges', [
    'as' => 'web_charge_index',
    'uses'  => 'Controller@index',
    'middleware' => [
      'auth:web',
    ],
]);
