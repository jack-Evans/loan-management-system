<?php

/** @var Route $router */
$router->get('charges/create', [
    'as' => 'web_charge_create',
    'uses'  => 'Controller@create',
    'middleware' => [
      'auth:web',
    ],
]);
