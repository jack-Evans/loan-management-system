<?php

/** @var Route $router */
$router->post('charges/store', [
    'as' => 'web_charge_store',
    'uses'  => 'Controller@store',
    'middleware' => [
      'auth:web',
    ],
]);
