<?php

/** @var Route $router */
$router->post('charges/{id}/update', [
    'as' => 'web_charge_update',
    'uses'  => 'Controller@update',
    'middleware' => [
      'auth:web',
    ],
]);
