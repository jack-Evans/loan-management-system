<?php

/** @var Route $router */
$router->get('charges/{id}/edit', [
    'as' => 'web_charge_edit',
    'uses'  => 'Controller@edit',
    'middleware' => [
      'auth:web',
    ],
]);
