<?php

namespace App\Containers\Charge\UI\WEB\Controllers;

use App\Containers\Charge\UI\WEB\Requests\CreateChargeRequest;
use App\Containers\Charge\UI\WEB\Requests\DeleteChargeRequest;
use App\Containers\Charge\UI\WEB\Requests\GetAllChargesRequest;
use App\Containers\Charge\UI\WEB\Requests\FindChargeByIdRequest;
use App\Containers\Charge\UI\WEB\Requests\UpdateChargeRequest;
use App\Containers\Charge\UI\WEB\Requests\StoreChargeRequest;
use App\Containers\Charge\UI\WEB\Requests\EditChargeRequest;
use App\Ship\Parents\Controllers\WebController;
use Apiato\Core\Foundation\Facades\Apiato;

/**
 * Class Controller
 *
 * @package App\Containers\Charge\UI\WEB\Controllers
 */
class Controller extends WebController
{
    /**
     * Show all entities
     *
     * @param GetAllChargesRequest $request
     */
    public function index(GetAllChargesRequest $request)
    {
        $url = config('token-container.WEB_API_URL').'charges';
        
        try{
            $response = get($url, ['limit' => 0]);
            // $pagination = isset($response->meta->pagination) ? $response->meta->pagination : array();
            $charges = isset($response->data) ? $response->data : array();        
        } catch (\Exception $e) {
            return exception($e);
        }
        
        return view('charge::charges', compact('charges'));
    }

    /**
     * Show one entity
     *
     * @param FindChargeByIdRequest $request
     */
    public function show(FindChargeByIdRequest $request)
    {
        $charge = Apiato::call('Charge@FindChargeByIdAction', [$request]);

        // ..
    }

    /**
     * Create entity (show UI)
     *
     * @param CreateChargeRequest $request
     */
    public function create(CreateChargeRequest $request)
    {
        return view('charge::createUpdate');
    }

    /**
     * Add a new entity
     *
     * @param StoreChargeRequest $request
     */
    public function store(StoreChargeRequest $request)
    {
        $url = config('token-container.WEB_API_URL').'charges';
        
        $data = [
            'name' => $request->name,
            'value' => $request->value,
            'chargetype' => $request->chargetype,
            'amounttype' => $request->amounttype,
        ];
        
        try {
             $response = post($url, $data);
        } catch(\Exception $e) {
            return exception($e);
        }
                  
        return redirect('charges');
    }

    /**
     * Edit entity (show UI)
     *
     * @param EditChargeRequest $request
     */
    public function edit(EditChargeRequest $request)
    {
        $url = config('token-container.WEB_API_URL').'charges/'.$request->id;
        $response = get($url);    
                
        $charge = isset($response->data) ? $response->data : array();

        return view('charge::createUpdate', compact('charge'));
    }

    /**
     * Update a given entity
     *
     * @param UpdateChargeRequest $request
     */
    public function update(UpdateChargeRequest $request)
    {
        $url = config('token-container.WEB_API_URL').'charges/'.$request->id;
        
        $data = [
            'name' => $request->name,
            'value' => $request->value,
            'chargetype' => $request->chargetype,
            'amounttype' => $request->amounttype,
        ];
        
        try{
            $response = patch($url, $data);
        } catch (\Exception $e) {
            return exception($e);
        }

        return redirect('charges');
    }

    /**
     * Delete a given entity
     *
     * @param DeleteChargeRequest $request
     */
    public function delete(DeleteChargeRequest $request)
    {
        $url = config('token-container.WEB_API_URL').'charges/'.$request->id;
        
        try{
            $response = delete($url);
        } catch (\Exception $e) {
            return exception($e);
        }

        return redirect('charges');
    }
}
