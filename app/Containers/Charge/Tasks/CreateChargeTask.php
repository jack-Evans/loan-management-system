<?php

namespace App\Containers\Charge\Tasks;

use App\Containers\Charge\Data\Repositories\ChargeRepository;
use App\Ship\Exceptions\CreateResourceFailedException;
use App\Ship\Parents\Tasks\Task;
use Exception;

class CreateChargeTask extends Task
{

    protected $repository;

    public function __construct(ChargeRepository $repository)
    {
        $this->repository = $repository;
    }

    public function run(array $data)
    {
        try {
            return $this->repository->create($data);
        }
        catch (Exception $exception) {
            throw new CreateResourceFailedException();
        }
    }
}
