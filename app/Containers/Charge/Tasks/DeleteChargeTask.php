<?php

namespace App\Containers\Charge\Tasks;

use App\Containers\Charge\Data\Repositories\ChargeRepository;
use App\Containers\LoanCharge\Data\Repositories\LoanChargeRepository;
use App\Ship\Exceptions\DeleteResourceFailedException;
use App\Ship\Parents\Tasks\Task;
use Exception;

class DeleteChargeTask extends Task
{

    protected $repository;
    protected $loanChargeRepository;

    public function __construct(ChargeRepository $repository, LoanChargeRepository $loanChargeRepository)
    {
        $this->repository = $repository;
        $this->loanChargeRepository = $loanChargeRepository;
    }

    public function run($id)
    {
        try {
            \DB::beginTransaction();
            $this->loanChargeRepository->deleteWhere(['charge_id' => $id]);
            $del = $this->repository->delete($id);
            \DB::commit();
            return $del;
        }
        catch (Exception $exception) {
            throw new DeleteResourceFailedException();
        }
    }
}
