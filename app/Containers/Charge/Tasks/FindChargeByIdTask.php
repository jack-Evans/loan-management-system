<?php

namespace App\Containers\Charge\Tasks;

use App\Containers\Charge\Data\Repositories\ChargeRepository;
use App\Ship\Exceptions\NotFoundException;
use App\Ship\Parents\Tasks\Task;
use Exception;

class FindChargeByIdTask extends Task
{

    protected $repository;

    public function __construct(ChargeRepository $repository)
    {
        $this->repository = $repository;
    }

    public function run($id)
    {
        try {
            return $this->repository->find($id);
        }
        catch (Exception $exception) {
            throw new NotFoundException();
        }
    }
}
