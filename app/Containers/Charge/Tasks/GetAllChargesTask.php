<?php

namespace App\Containers\Charge\Tasks;

use App\Containers\Charge\Data\Repositories\ChargeRepository;
use App\Ship\Parents\Tasks\Task;

class GetAllChargesTask extends Task
{

    protected $repository;

    public function __construct(ChargeRepository $repository)
    {
        $this->repository = $repository;
    }

    public function run()
    {
        return $this->repository->paginate();
    }
}
