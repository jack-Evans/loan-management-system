<?php

namespace App\Containers\Charge\Models;

use App\Ship\Parents\Models\Model;

class Charge extends Model
{
    protected $fillable = [
        'name',
        'value',
        'amount_type',
        'charge_type',
    ];

    protected $attributes = [

    ];

    protected $hidden = [

    ];

    protected $casts = [

    ];

    protected $dates = [
        'created_at',
        'updated_at',
    ];

    /**
     * A resource key to be used by the the JSON API Serializer responses.
     */
    protected $resourceKey = 'charges';
    
    // public function journal()
    // {
    //     return $this->hasOne(\App\Containers\Payment\Models\Journal::class, 'loan_id')->with(['posts']);
    // }
}
