<?php

namespace App\Containers\Charge\Actions;

use App\Ship\Parents\Actions\Action;
use App\Ship\Parents\Requests\Request;
use Apiato\Core\Foundation\Facades\Apiato;

class FindChargeByIdAction extends Action
{
    public function run(Request $request)
    {
        $charge = Apiato::call('Charge@FindChargeByIdTask', [$request->id]);

        return $charge;
    }
}
