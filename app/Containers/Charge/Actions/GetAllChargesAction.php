<?php

namespace App\Containers\Charge\Actions;

use App\Ship\Parents\Actions\Action;
use App\Ship\Parents\Requests\Request;
use Apiato\Core\Foundation\Facades\Apiato;

class GetAllChargesAction extends Action
{
    public function run(Request $request)
    {
        $charges = Apiato::call('Charge@GetAllChargesTask', [], ['addRequestCriteria']);

        return $charges;
    }
}
