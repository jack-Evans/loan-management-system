<?php

namespace App\Containers\Charge\Actions;

use App\Ship\Parents\Actions\Action;
use App\Ship\Parents\Requests\Request;
use Apiato\Core\Foundation\Facades\Apiato;
use App\Containers\Charge\Data\Transporters\CreateChargeTransporter;

class CreateChargeAction extends Action
{
    public function run(Request $request)
    {
    	$data = [
            'name'		=> $request->name,
            'value'		=> $request->value,
            'amount_type'	=> $request->amounttype,
            'charge_type'	=> $request->chargetype,
    	];

        $charge = Apiato::call('Charge@CreateChargeTask', [(new CreateChargeTransporter($data))->toArray()]);

        return $charge;
    }
}
