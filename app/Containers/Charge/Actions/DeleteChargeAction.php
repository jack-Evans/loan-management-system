<?php

namespace App\Containers\Charge\Actions;

use App\Ship\Parents\Actions\Action;
use App\Ship\Parents\Requests\Request;
use Apiato\Core\Foundation\Facades\Apiato;

class DeleteChargeAction extends Action
{
    public function run(Request $request)
    {
        return Apiato::call('Charge@DeleteChargeTask', [$request->id]);
    }
}
