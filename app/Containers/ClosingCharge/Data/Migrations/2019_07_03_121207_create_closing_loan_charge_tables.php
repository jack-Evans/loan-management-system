<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateClosingLoanChargeTables extends Migration
{

    /**
     * Run the migrations.
     */
    public function up()
    {
        Schema::create('closing_loan_charges', function (Blueprint $table) {

            // $table->increments('id');
            $table->unsignedInteger('loan_id');
            $table->unsignedInteger('closing_charge_id');
            $table->float('value', 12, 4);
            $table->text('description')->nullable();
            $table->timestamps();
            $table->softDeletes();
            $table->primary(['loan_id', 'closing_charge_id']);

        });
    }

    /**
     * Reverse the migrations.
     */
    public function down()
    {
        Schema::drop('closing_loan_charges');
    }
}
