<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateClosingchargeTables extends Migration
{

    /**
     * Run the migrations.
     */
    public function up()
    {
        Schema::create('closing_charges', function (Blueprint $table) {

            $table->increments('id');
            $table->string('name')->unique();
            $table->float('value', 12, 4);
            $table->enum('amount_type', ['netloan', 'principal_amount', 'grossloan'])->default('netloan')->nullable();
            $table->enum('charge_type', ['fixed', 'percentage'])->default('fixed');
            $table->enum('default_apply', ['apply', 'not-apply'])->default('not-apply')->nullable();
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down()
    {
        Schema::drop('closing_charges');
    }
}
