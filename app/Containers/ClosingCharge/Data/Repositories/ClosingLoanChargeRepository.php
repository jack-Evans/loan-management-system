<?php

namespace App\Containers\ClosingCharge\Data\Repositories;

use App\Ship\Parents\Repositories\Repository;

/**
 * Class ClosingLoanChargeRepository
 */
class ClosingLoanChargeRepository extends Repository
{

    /**
     * @var array
     */
    protected $fieldSearchable = [
        'id' => '=',
        // ...
    ];

}
