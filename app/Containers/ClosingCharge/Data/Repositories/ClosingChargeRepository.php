<?php

namespace App\Containers\ClosingCharge\Data\Repositories;

use App\Ship\Parents\Repositories\Repository;

/**
 * Class ClosingChargeRepository
 */
class ClosingChargeRepository extends Repository
{

    /**
     * @var array
     */
    protected $fieldSearchable = [
        'id' => '=',
        // ...
    ];

}
