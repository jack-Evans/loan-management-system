<?php

namespace App\Containers\ClosingCharge\Models;

use App\Ship\Parents\Models\Model;

class ClosingLoanCharge extends Model
{
    protected $fillable = [
        'loan_id',
        'closing_charge_id',
        'value',
        'description',
    ];

    protected $attributes = [

    ];

    protected $hidden = [

    ];

    protected $casts = [

    ];

    protected $dates = [
        'created_at',
        'updated_at',
    ];

    /**
     * A resource key to be used by the the JSON API Serializer responses.
     */
    protected $resourceKey = 'ClosingLoancharges';

    public function closingCharge() {
        return $this->belongsTo(\App\Containers\ClosingCharge\Models\ClosingCharge::class);
    }

    public function setMinTermAttribute($value){
        $this->attributes['min_term'] = $value;
    }
    public function setMinTermBalanceAttribute($value){
        $this->attributes['min_term_balance'] = $value;
    }
    public function setAmountPaidAttribute($value){
        $this->attributes['amount_paid'] = $value;
    }
    public function setMonthlyPaymentAttribute($value){
        $this->attributes['monthly_payment'] = $value;
    }
    public function setTotalMinTermAmountAttribute($value){
        $this->attributes['total_minterm_amount'] = $value;
    }
}
