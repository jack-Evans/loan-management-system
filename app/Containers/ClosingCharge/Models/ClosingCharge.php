<?php

namespace App\Containers\ClosingCharge\Models;

use App\Ship\Parents\Models\Model;

class ClosingCharge extends Model
{
    protected $fillable = [
        'name',
        'value',
        'amount_type',
        'charge_type',
        'default_apply',
    ];

    protected $attributes = [

    ];

    protected $hidden = [

    ];

    protected $casts = [

    ];

    protected $dates = [
        'created_at',
        'updated_at',
    ];

    /**
     * A resource key to be used by the the JSON API Serializer responses.
     */
    protected $resourceKey = 'closingcharges';
}
