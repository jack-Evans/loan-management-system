<?php

namespace App\Containers\ClosingCharge\Tasks;

use App\Containers\ClosingCharge\Data\Repositories\ClosingLoanChargeRepository;
use App\Ship\Exceptions\NotFoundException;
use App\Ship\Parents\Tasks\Task;
use Exception;

class FindClosingLoanChargeByIdTask extends Task
{

    protected $repository;

    public function __construct(ClosingLoanChargeRepository $repository)
    {
        $this->repository = $repository;
    }

    public function run($id)
    {
        try {
            return $this->repository->with('closingCharge')->findWhere(['loan_id' => $id]);
        }
        catch (Exception $exception) {
            throw new NotFoundException();
        }
    }
}
