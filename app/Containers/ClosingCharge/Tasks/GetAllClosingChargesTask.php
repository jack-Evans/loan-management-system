<?php

namespace App\Containers\ClosingCharge\Tasks;

use App\Containers\ClosingCharge\Data\Repositories\ClosingChargeRepository;
use App\Ship\Parents\Tasks\Task;

class GetAllClosingChargesTask extends Task
{

    protected $repository;

    public function __construct(ClosingChargeRepository $repository)
    {
        $this->repository = $repository;
    }

    public function run()
    {
        return $this->repository->all();
    }
}
