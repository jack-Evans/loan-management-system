<?php

namespace App\Containers\ClosingCharge\Tasks;

use App\Containers\ClosingCharge\Data\Repositories\ClosingLoanChargeRepository;
use App\Ship\Exceptions\DeleteResourceFailedException;
use App\Ship\Parents\Tasks\Task;
use Exception;

class DeleteClosingLoanChargeTask extends Task
{

    protected $repository;

    public function __construct(ClosingLoanChargeRepository $repository)
    {
        $this->repository = $repository;
    }

    public function run($loanId, $chargeId)
    {
        try {
            return $this->repository->deleteWhere(['loan_id' => $loanId, 'closing_charge_id' => $chargeId]);
        }
        catch (Exception $exception) {
            throw new DeleteResourceFailedException();
        }
    }
}
