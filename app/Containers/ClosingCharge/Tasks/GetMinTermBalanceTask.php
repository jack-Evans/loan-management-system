<?php

namespace App\Containers\ClosingCharge\Tasks;

use App\Ship\Parents\Tasks\Task;
use Apiato\Core\Foundation\Facades\Apiato;
use App\Containers\ClosingCharge\Models\ClosingLoanCharge;

class GetMinTermBalanceTask extends Task
{

    protected $repository;

    public function run($loanId)
    {
        $loan = Apiato::call('Loan@FindLoanByIdTask', [$loanId]);
        $paidAmount = Apiato::call('Payment@GetAllPaymentsTask', [$loan])->where('is_capital_reduction', '!=', 1)->sum('amount');
        $monthlyPmt = calculateMonthlyPMT($loan->interest->rate, $loan->interest->gross_loan);
        $requiredAmount = round(($loan->interest->min_term * $monthlyPmt) - $paidAmount, 2);
        
        $closingLoanCharge = new ClosingLoanCharge;
        $closingLoanCharge->setMinTermAttribute($loan->interest->min_term);
        $closingLoanCharge->setMinTermBalanceAttribute(($requiredAmount > 0) ? $requiredAmount : 0);
        $closingLoanCharge->setAmountPaidAttribute($paidAmount);
        $closingLoanCharge->setMonthlyPaymentAttribute($monthlyPmt);
        $closingLoanCharge->setTotalMinTermAmountAttribute(round($requiredAmount + $paidAmount, 2));

        return $closingLoanCharge;
    }
}
