<?php

namespace App\Containers\ClosingCharge\Tasks;

use App\Containers\ClosingCharge\Data\Repositories\ClosingChargeRepository;
use App\Containers\ClosingCharge\Data\Repositories\ClosingLoanChargeRepository;
use App\Ship\Exceptions\DeleteResourceFailedException;
use App\Ship\Parents\Tasks\Task;
use Exception;

class DeleteClosingChargeTask extends Task
{

    protected $repository;
    protected $closingLoanChargeRepository;

    public function __construct(ClosingChargeRepository $repository, ClosingLoanChargeRepository $closingLoanChargeRepository)
    {
        $this->repository = $repository;
        $this->closingLoanChargeRepository = $closingLoanChargeRepository;
    }

    public function run($id)
    {
        try {
            \DB::beginTransaction();
            $this->closingLoanChargeRepository->deleteWhere(['closing_charge_id' => $id]);
            $del = $this->repository->delete($id);
            \DB::commit();
            return $del;
        }
        catch (Exception $exception) {
            throw new DeleteResourceFailedException();
        }
    }
}
