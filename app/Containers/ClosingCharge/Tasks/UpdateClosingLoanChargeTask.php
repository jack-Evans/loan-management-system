<?php

namespace App\Containers\ClosingCharge\Tasks;

use App\Containers\ClosingCharge\Data\Repositories\ClosingLoanChargeRepository;
use App\Ship\Exceptions\UpdateResourceFailedException;
use App\Ship\Parents\Tasks\Task;
use Exception;
use App\Containers\ClosingCharge\Models\ClosingLoanCharge;

class UpdateClosingLoanChargeTask extends Task
{

    protected $repository;

    public function __construct(ClosingLoanChargeRepository $repository)
    {
        $this->repository = $repository;
    }

    public function run($loanId, $chargeId, array $data)
    {        
        try {
            return $result = ClosingLoanCharge::where(['closing_charge_id' => $chargeId])->where(['loan_id' => $loanId])->update($data);
        }
        catch (Exception $exception) {
            throw new UpdateResourceFailedException();
        }
    }
}
