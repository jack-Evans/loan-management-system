<?php

namespace App\Containers\ClosingCharge\Tasks;

use App\Containers\ClosingCharge\Data\Repositories\ClosingChargeRepository;
use App\Ship\Exceptions\CreateResourceFailedException;
use App\Ship\Parents\Tasks\Task;
use Exception;

class CreateClosingChargeTask extends Task
{

    protected $repository;

    public function __construct(ClosingChargeRepository $repository)
    {
        $this->repository = $repository;
    }

    public function run(array $data)
    {
        try {
            return $this->repository->create($data);
        }
        catch (Exception $exception) {
            throw new CreateResourceFailedException();
        }
    }
}
