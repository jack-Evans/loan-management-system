<?php

namespace App\Containers\ClosingCharge\UI\WEB\Controllers;

use App\Containers\ClosingCharge\UI\WEB\Requests\CreateClosingChargeRequest;
use App\Containers\ClosingCharge\UI\WEB\Requests\DeleteClosingChargeRequest;
use App\Containers\ClosingCharge\UI\WEB\Requests\GetAllClosingChargesRequest;
use App\Containers\ClosingCharge\UI\WEB\Requests\FindClosingChargeByIdRequest;
use App\Containers\ClosingCharge\UI\WEB\Requests\UpdateClosingChargeRequest;
use App\Containers\ClosingCharge\UI\WEB\Requests\StoreClosingChargeRequest;
use App\Containers\ClosingCharge\UI\WEB\Requests\EditClosingChargeRequest;
use App\Ship\Parents\Controllers\WebController;
use Apiato\Core\Foundation\Facades\Apiato;

/**
 * Class Controller
 *
 * @package App\Containers\ClosingCharge\UI\WEB\Controllers
 */
class Controller extends WebController
{
    /**
     * Show all entities
     *
     * @param GetAllClosingChargesRequest $request
     */
    protected $apiPath;

    public function __construct(){
        $this->apiPath = config('token-container.WEB_API_URL');
    }

    public function index(GetAllClosingChargesRequest $request)
    {
        $url = $this->apiPath.'closingcharges';
        try{
            $response = get($url, ['limit' => 0]);
            // $pagination = isset($response->meta->pagination) ? $response->meta->pagination : array();
            $charges = isset($response->data) ? $response->data : array();        
        } catch (\Exception $e) {
            return exception($e);
        }
        return view('closingcharge::charges', compact('charges'));
    }

    /**
     * Show one entity
     *
     * @param FindClosingChargeByIdRequest $request
     */
    public function show(FindClosingChargeByIdRequest $request)
    {
        $closingcharge = Apiato::call('ClosingCharge@FindClosingChargeByIdAction', [$request]);

        // ..
    }

    /**
     * Create entity (show UI)
     *
     * @param CreateClosingChargeRequest $request
     */
    public function create(CreateClosingChargeRequest $request)
    {
        return view('closingcharge::createUpdate');
    }

    /**
     * Add a new entity
     *
     * @param StoreClosingChargeRequest $request
     */
    public function store(StoreClosingChargeRequest $request)
    {
        $url = config('token-container.WEB_API_URL').'closingcharges';
        
        $data = [
            'name' => $request->name,
            'value' => $request->value,
            'chargeType' => $request->chargetype,
            'amountType' => $request->amounttype,
            'defaultApply' => $request->defaultapply,
        ];
        
        try {
             $response = post($url, $data);
        } catch(\Exception $e) {
            return exception($e);
        }
                  
        return redirect('closingcharges');
    }

    /**
     * Edit entity (show UI)
     *
     * @param EditClosingChargeRequest $request
     */
    public function edit(EditClosingChargeRequest $request)
    {
        $url = $this->apiPath.'closingcharges/'.$request->id;
        try{
            $response = get($url, ['page' => $request->page]);
            $charge = isset($response->data) ? $response->data : array();        
        } catch (\Exception $e) {
            return exception($e);
        }

        return view('closingcharge::createUpdate', compact('charge'));
    }

    /**
     * Update a given entity
     *
     * @param UpdateClosingChargeRequest $request
     */
    public function update(UpdateClosingChargeRequest $request)
    {
        $url = config('token-container.WEB_API_URL').'closingcharges/'.$request->id;
        
        $data = [
            'name' => $request->name,
            'value' => $request->value,
            'chargeType' => $request->chargetype,
            'amountType' => $request->amounttype,
            'defaultApply' => $request->defaultapply,
        ];
        
        try {
             $response = patch($url, $data);
        } catch(\Exception $e) {
            return exception($e);
        }
        return redirect('closingcharges');
    }

    /**
     * Delete a given entity
     *
     * @param DeleteClosingChargeRequest $request
     */
    public function delete(DeleteClosingChargeRequest $request)
    {
        $url = config('token-container.WEB_API_URL').'closingcharges/'.$request->id;
        
        try{
            $response = delete($url);
        } catch (\Exception $e) {
            return exception($e);
        }

        return redirect('closingcharges');
    }
}
