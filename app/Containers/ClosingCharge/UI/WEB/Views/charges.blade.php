@extends('layouts.app')

@section('title', 'Closing Charges')

@section('content')
<div class="col-lg-12">
    <div class="has-bg">
        <h3>
            Closing Charges
            <a href="{{url('closingcharges/create')}}" class="pull-right">
                <button class="btn btn-success">Create Closing Charge</button>
            </a>
        </h3>
        <span class="space"></span>
        <table class="table table-striped table-hover table-bordered text-left MDBootstrapDatatable" cellspacing="0" width="100%">
            <thead>
                <tr>
                    <th>Name</th>
                    <th>Value</th>
                    <th>Charge Type</th>
                    <th>Amount Type</th>
                    <th>Default Apply</th>
                    <th>Actions</th>
                </tr>
            </thead>
            <tbody>
                @foreach($charges as $key => $charge)
                <tr>
                    <td class="text-capitalize">{{$charge->name}}</td>
                    <td>{{(float)$charge->value}}</td>
                    <td>{{$charge->charge_type}}</td>
                    <td>{{$charge->amount_type}}</td>
                    <td>{{($charge->default_apply == 'apply') ? 'Yes' : 'No'}}</td>
                    <td>
                        <a href="{{url('closingcharges').'/'.$charge->id.'/edit'}}">
                            <button class="btn btn-primary">Edit</button>
                        </a>

                        <a href="{{url('closingcharges').'/'.$charge->id.'/delete'}}">
                            <button class="btn btn-danger">Delete</button>
                        </a>
                    </td>
                </tr>
                @endforeach
            </tbody>
        </table>
    </div>
</div>
@endsection
