@extends('layouts.app')

@section('title', 'Closing charges')

@section('content')
<div class="col-lg-12">
  <div class="has-bg">
    <div class="row">
        <div class="col-md-5 col-md-offset-2">
        <h3>{{isset($charge->id) ? 'Edit ' : 'Create '}}Closing Charge</h3>
            <form action="{{url(isset($charge->id) ? 'closingcharges/'.$charge->id.'/update': 'closingcharges/store' )}}" method="post">
                @csrf()
                <div class="form-group">
                  <label for="name">Name:</label>
                  <input type="text" class="form-control" name="name"
                         value="{{isset($charge->name) ? $charge->name : old('name')}}">
                </div>
                <div class="form-group">
                  <label for="value">Value:</label>
                  <input type="text" class="form-control" name="value"
                         value="{{isset($charge->value) ? (float)$charge->value : old('value')}}">
                </div>
                <div class="form-group">
                  
                  <label for="chargetype">Charge Type:</label>
                  <!-- <input type="text" class="form-control" name="chargetype"
                         value="{{isset($charge->chargetype) ? $charge->chargetype : old('chargetype')}}"> -->
                  
                  <select class="form-control" name="chargetype">
                    <option value="fixed">Fixed</option>
                  </select> 

                </div>                              
            
                <div class="form-group">
                  <label for="amounttype">Amount Type </label>
                  <!-- <input type="text" class="form-control" name="amounttype"
                         value="{{isset($charge->amounttype) ? $charge->amounttype : old('amounttype')}}"> -->

                  <select class="form-control" name="amounttype">
                    <option value="netloan">Net Loan</option>
                  </select>

                </div>

                <div class="form-group">
                  <label for="amounttype">Default apply</label>
                  <select class="form-control" name="defaultapply">
                    <option value="apply" {{(isset($charge->default_apply)) ? ($charge->default_apply == 'apply' ? 'selected' : '') : '' }}>Apply</option>
                    <option value="not-apply" {{(isset($charge->default_apply)) ? ($charge->default_apply == 'not-apply' ? 'selected' : '') : '' }}>Not-Apply</option>
                  </select>

                </div>
                <button type="submit" class="btn btn-primary">Submit</button>
            </form>
        </div>
    </div>
  </div>
</div>
@endsection
