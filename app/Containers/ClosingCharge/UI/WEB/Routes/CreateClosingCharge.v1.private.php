<?php

/** @var Route $router */
$router->get('closingcharges/create', [
    'as' => 'web_closingcharge_create',
    'uses'  => 'Controller@create',
    'middleware' => [
      'auth:web',
    ],
]);
