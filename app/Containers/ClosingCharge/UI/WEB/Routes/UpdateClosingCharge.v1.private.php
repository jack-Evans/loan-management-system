<?php

/** @var Route $router */
$router->post('closingcharges/{id}/update', [
    'as' => 'web_closingcharge_update',
    'uses'  => 'Controller@update',
    'middleware' => [
      'auth:web',
    ],
]);
