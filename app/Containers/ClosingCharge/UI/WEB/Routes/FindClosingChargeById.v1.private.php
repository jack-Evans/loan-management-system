<?php

/** @var Route $router */
$router->get('closingcharges/{id}', [
    'as' => 'web_closingcharge_show',
    'uses'  => 'Controller@show',
    'middleware' => [
      'auth:web',
    ],
]);
