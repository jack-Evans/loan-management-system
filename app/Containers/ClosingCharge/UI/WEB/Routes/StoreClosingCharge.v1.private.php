<?php

/** @var Route $router */
$router->post('closingcharges/store', [
    'as' => 'web_closingcharge_store',
    'uses'  => 'Controller@store',
    'middleware' => [
      'auth:web',
    ],
]);
