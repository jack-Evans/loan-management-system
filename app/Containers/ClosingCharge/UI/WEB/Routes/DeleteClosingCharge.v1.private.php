<?php

/** @var Route $router */
$router->get('closingcharges/{id}/delete', [
    'as' => 'web_closingcharge_delete',
    'uses'  => 'Controller@delete',
    'middleware' => [
      'auth:web',
    ],
]);
