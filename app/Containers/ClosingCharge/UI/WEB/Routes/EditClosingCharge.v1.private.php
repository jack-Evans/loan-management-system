<?php

/** @var Route $router */
$router->get('closingcharges/{id}/edit', [
    'as' => 'web_closingcharge_edit',
    'uses'  => 'Controller@edit',
    'middleware' => [
      'auth:web',
    ],
]);
