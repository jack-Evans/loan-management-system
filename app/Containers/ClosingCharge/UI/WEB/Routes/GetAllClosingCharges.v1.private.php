<?php

/** @var Route $router */
$router->get('closingcharges', [
    'as' => 'web_closingcharge_index',
    'uses'  => 'Controller@index',
    'middleware' => [
      'auth:web',
    ],
]);
