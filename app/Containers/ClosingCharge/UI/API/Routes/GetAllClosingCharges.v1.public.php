<?php

/**
 * @apiGroup           ClosingCharge
 * @apiName            getAllClosingCharges
 *
 * @api                {GET} /v1/closingcharges Endpoint title here..
 * @apiDescription     Endpoint description here..
 *
 * @apiVersion         1.0.0
 * @apiPermission      none
 *
 * @apiParam           {String}  parameters here..
 *
 * @apiSuccessExample  {json}  Success-Response:
 * HTTP/1.1 200 OK
{
  // Insert the response of the request here...
}
 */

/** @var Route $router */
$router->get('closingcharges', [
    'as' => 'api_closingcharge_get_all_closing_charges',
    'uses'  => 'Controller@getAllClosingCharges',
    'middleware' => [
      'auth:api',
    ],
]);
