<?php

/**
 * @apiGroup           ClosingCharge
 * @apiName            findClosingLoanChargeById
 *
 * @api                {GET} /v1/findclosingloanchargebyid Endpoint title here..
 * @apiDescription     Endpoint description here..
 *
 * @apiVersion         1.0.0
 * @apiPermission      none
 *
 * @apiParam           {String}  parameters here..
 *
 * @apiSuccessExample  {json}  Success-Response:
 * HTTP/1.1 200 OK
{
  // Insert the response of the request here...
}
 */

/** @var Route $router */
$router->get('closingloancharge/{loanId}', [
    'as' => 'api_closingcharge_find_closing_loan_charge_by_id',
    'uses'  => 'Controller@findClosingLoanChargeById',
    'middleware' => [
      'auth:api',
    ],
]);
