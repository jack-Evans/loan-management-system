<?php

/**
 * @apiGroup           ClosingCharge
 * @apiName            updateClosingCharge
 *
 * @api                {PATCH} /v1/closingcharges/:id Endpoint title here..
 * @apiDescription     Endpoint description here..
 *
 * @apiVersion         1.0.0
 * @apiPermission      none
 *
 * @apiParam           {String}  parameters here..
 *
 * @apiSuccessExample  {json}  Success-Response:
 * HTTP/1.1 200 OK
{
  // Insert the response of the request here...
}
 */

/** @var Route $router */
$router->patch('updatemintermcharge/{loanId}', [
    'as' => 'api_closingcharge_update_minterm_charge',
    'uses'  => 'Controller@updateMinTermCharge',
    'middleware' => [
      'auth:api',
    ],
]);
