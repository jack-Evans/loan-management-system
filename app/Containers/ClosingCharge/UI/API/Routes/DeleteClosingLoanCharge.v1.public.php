<?php

/**
 * @apiGroup           ClosingCharge
 * @apiName            findClosingLoanChargeById
 *
 * @api                {GET} /v1/findclosingloanchargebyid Endpoint title here..
 * @apiDescription     Endpoint description here..
 *
 * @apiVersion         1.0.0
 * @apiPermission      none
 *
 * @apiParam           {String}  parameters here..
 *
 * @apiSuccessExample  {json}  Success-Response:
 * HTTP/1.1 200 OK
{
  // Insert the response of the request here...
}
 */

/** @var Route $router */
$router->delete('closingloancharge/{loanId}/{chargeId}', [
    'as' => 'api_delete_closing_loan_charge',
    'uses'  => 'Controller@deleteClosingLoanCharge',
    'middleware' => [
      'auth:api',
    ],
]);
