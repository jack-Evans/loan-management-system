<?php

/**
 * @apiGroup           ClosingCharge
 * @apiName            deleteClosingCharge
 *
 * @api                {DELETE} /v1/closingcharges/:id Endpoint title here..
 * @apiDescription     Endpoint description here..
 *
 * @apiVersion         1.0.0
 * @apiPermission      none
 *
 * @apiParam           {String}  parameters here..
 *
 * @apiSuccessExample  {json}  Success-Response:
 * HTTP/1.1 200 OK
{
  // Insert the response of the request here...
}
 */

/** @var Route $router */
$router->delete('closingcharges/{id}', [
    'as' => 'api_closingcharge_delete_closing_charge',
    'uses'  => 'Controller@deleteClosingCharge',
    'middleware' => [
      'auth:api',
    ],
]);
