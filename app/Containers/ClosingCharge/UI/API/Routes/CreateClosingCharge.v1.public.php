<?php

/**
 * @apiGroup           ClosingCharge
 * @apiName            createClosingCharge
 *
 * @api                {POST} /v1/closingcharges Endpoint title here..
 * @apiDescription     Endpoint description here..
 *
 * @apiVersion         1.0.0
 * @apiPermission      none
 *
 * @apiParam           {String}  parameters here..
 *
 * @apiSuccessExample  {json}  Success-Response:
 * HTTP/1.1 200 OK
{
  // Insert the response of the request here...
}
 */

/** @var Route $router */
$router->post('closingcharges', [
    'as' => 'api_closingcharge_create_closing_charge',
    'uses'  => 'Controller@createClosingCharge',
    'middleware' => [
      'auth:api',
    ],
]);
