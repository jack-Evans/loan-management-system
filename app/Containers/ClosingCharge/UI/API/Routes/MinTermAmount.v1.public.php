<?php

/**
 * @apiGroup           MinTermBalance
 * @apiName            getMinTermBalance
 *
 * @api                {GET} /v1/closingcharges Endpoint title here..
 * @apiDescription     Endpoint description here..
 *
 * @apiVersion         1.0.0
 * @apiPermission      none
 *
 * @apiParam           {String}  parameters here..
 *
 * @apiSuccessExample  {json}  Success-Response:
 * HTTP/1.1 200 OK
{
  // Insert the response of the request here...
}
 */

/** @var Route $router */
$router->get('mintermbalance/{loanId}', [
    'as' => 'api_closingcharge_get_min_term_balance',
    'uses'  => 'Controller@getMinTermBalance',
    'middleware' => [
      'auth:api',
    ],
]);
