<?php

/**
 * @apiGroup           ClosingCharge
 * @apiName            findClosingChargeById
 *
 * @api                {GET} /v1/closingcharges/:id Endpoint title here..
 * @apiDescription     Endpoint description here..
 *
 * @apiVersion         1.0.0
 * @apiPermission      none
 *
 * @apiParam           {String}  parameters here..
 *
 * @apiSuccessExample  {json}  Success-Response:
 * HTTP/1.1 200 OK
{
  // Insert the response of the request here...
}
 */

/** @var Route $router */
$router->get('closingcharges/{id}', [
    'as' => 'api_closingcharge_find_closing_charge_by_id',
    'uses'  => 'Controller@findClosingChargeById',
    'middleware' => [
      'auth:api',
    ],
]);
