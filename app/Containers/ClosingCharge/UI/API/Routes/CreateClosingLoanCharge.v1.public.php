<?php

/**
 * @apiGroup           ClosingCharge
 * @apiName            createClosingLoanCharge
 *
 * @api                {POST} /v1/createclosingloancharge Endpoint title here..
 * @apiDescription     Endpoint description here..
 *
 * @apiVersion         1.0.0
 * @apiPermission      none
 *
 * @apiParam           {String}  parameters here..
 *
 * @apiSuccessExample  {json}  Success-Response:
 * HTTP/1.1 200 OK
{
  // Insert the response of the request here...
}
 */

/** @var Route $router */
$router->post('closingloancharge', [
    'as' => 'api_closingcharge_create_closing_loan_charge',
    'uses'  => 'Controller@createClosingLoanCharge',
    'middleware' => [
      'auth:api',
    ],
]);
