<?php

namespace App\Containers\ClosingCharge\UI\API\Controllers;

use App\Containers\ClosingCharge\UI\API\Requests\CreateClosingChargeRequest;
use App\Containers\ClosingCharge\UI\API\Requests\DeleteClosingChargeRequest;
use App\Containers\ClosingCharge\UI\API\Requests\GetAllClosingChargesRequest;
use App\Containers\ClosingCharge\UI\API\Requests\FindClosingChargeByIdRequest;
use App\Containers\ClosingCharge\UI\API\Requests\UpdateClosingChargeRequest;
use App\Containers\ClosingCharge\UI\API\Requests\CreateClosingLoanChargeRequest;
use App\Containers\ClosingCharge\UI\API\Requests\FindClosingLoanChargeByIdRequest;
use App\Containers\ClosingCharge\UI\API\Requests\UpdateClosingLoanChargeRequest;
use App\Containers\ClosingCharge\UI\API\Requests\DeleteClosingLoanChargeRequest;
use App\Containers\ClosingCharge\UI\API\Requests\UpdateMinTermChargeRequest;
use App\Containers\ClosingCharge\UI\API\Transformers\ClosingChargeTransformer;
use App\Containers\ClosingCharge\UI\API\Transformers\ClosingLoanChargeTransformer;
use App\Containers\ClosingCharge\UI\API\Transformers\MinTermBalanceTransformer;
use App\Ship\Parents\Controllers\ApiController;
use Apiato\Core\Foundation\Facades\Apiato;

/**
 * Class Controller
 *
 * @package App\Containers\ClosingCharge\UI\API\Controllers
 */
class Controller extends ApiController
{
    /**
     * @param CreateClosingChargeRequest $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function createClosingCharge(CreateClosingChargeRequest $request)
    {
        $closingcharge = Apiato::call('ClosingCharge@CreateClosingChargeAction', [$request]);

        return $this->created($this->transform($closingcharge, ClosingChargeTransformer::class));
    }

    /**
     * @param FindClosingChargeByIdRequest $request
     * @return array
     */
    public function findClosingChargeById(FindClosingChargeByIdRequest $request)
    {
        $closingcharge = Apiato::call('ClosingCharge@FindClosingChargeByIdAction', [$request]);

        return $this->transform($closingcharge, ClosingChargeTransformer::class);
    }

    /**
     * @param GetAllClosingChargesRequest $request
     * @return array
     */
    public function getAllClosingCharges(GetAllClosingChargesRequest $request)
    {
        $closingcharges = Apiato::call('ClosingCharge@GetAllClosingChargesAction', [$request]);

        return $this->transform($closingcharges, ClosingChargeTransformer::class);
    }

    /**
     * @param UpdateClosingChargeRequest $request
     * @return array
     */
    public function updateClosingCharge(UpdateClosingChargeRequest $request)
    {
        $closingcharge = Apiato::call('ClosingCharge@UpdateClosingChargeAction', [$request]);

        return $this->transform($closingcharge, ClosingChargeTransformer::class);
    }

    /**
     * @param DeleteClosingChargeRequest $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function deleteClosingCharge(DeleteClosingChargeRequest $request)
    {
        Apiato::call('ClosingCharge@DeleteClosingChargeAction', [$request]);

        return $this->noContent();
    }

    /**
     * @param CreateClosingLoanChargeRequest $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function createClosingLoanCharge(CreateClosingLoanChargeRequest $request)
    {
        Apiato::call('ClosingCharge@CreateClosingLoanChargeAction', [$request]);

        return $this->noContent();
    }

    /**
     * @param FindClosingLoanChargeByIdRequest $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function findClosingLoanChargeById(FindClosingLoanChargeByIdRequest $request)
    {
        $closingLoanCharge = Apiato::call('ClosingCharge@FindClosingLoanChargeByIdAction', [$request]);

        return $this->transform($closingLoanCharge, ClosingLoanChargeTransformer::class);
    }

    /**
     * @param FindClosingLoanChargeByIdRequest $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function updateClosingLoanCharge(UpdateClosingLoanChargeRequest $request)
    {
        $closingLoanCharge = Apiato::call('ClosingCharge@UpdateClosingLoanChargeAction', [$request]);

        return $closingLoanCharge;
    }

    /**
     * @param DeleteClosingLoanChargeRequest $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function deleteClosingLoanCharge(DeleteClosingLoanChargeRequest $request)
    {
        Apiato::call('ClosingCharge@DeleteClosingLoanChargeAction', [$request]);

        return $this->noContent();
    }

    /**
     * @param DeleteClosingLoanChargeRequest $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function getMinTermBalance(DeleteClosingLoanChargeRequest $request)
    {
        $minTermBalance = Apiato::call('ClosingCharge@GetMinTermBalanceAction', [$request]);

        return $this->transform($minTermBalance, MinTermBalanceTransformer::class);
    }

    /**
     * @param DeleteClosingLoanChargeRequest $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function updateMinTermCharge(UpdateMinTermChargeRequest $request)
    {
        $updateInterest = Apiato::call('ClosingCharge@UpdateMinTermChargeAction', [$request]);
        $minTermBalance = Apiato::call('ClosingCharge@GetMinTermBalanceAction', [$request]);

        return $this->transform($minTermBalance, MinTermBalanceTransformer::class);
    }
}
