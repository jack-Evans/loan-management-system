<?php

namespace App\Containers\ClosingCharge\UI\API\Requests;

use App\Ship\Parents\Requests\Request;

/**
 * Class UpdateClosingChargeRequest.
 */
class UpdateClosingChargeRequest extends Request
{

    /**
     * The assigned Transporter for this Request
     *
     * @var string
     */
    // protected $transporter = \App\Ship\Transporters\DataTransporter::class;

    /**
     * Define which Roles and/or Permissions has access to this request.
     *
     * @var  array
     */
    protected $access = [
        'permissions' => '',
        'roles'       => '',
    ];

    /**
     * Id's that needs decoding before applying the validation rules.
     *
     * @var  array
     */
    protected $decode = [
        'id',
    ];

    /**
     * Defining the URL parameters (e.g, `/user/{id}`) allows applying
     * validation rules on them and allows accessing them like request data.
     *
     * @var  array
     */
    protected $urlParameters = [
        'id',
    ];

    /**
     * @return  array
     */
    public function rules()
    {
        return [
            'name'             => 'required|max:50|string|unique:closing_charges,name,'.$this->id,
            'value'            => 'required|regex:/^\d*(\.\d{1,2})?$/',
            'amountType'       => 'required_if:chargetype,percentage|in:netloan,principal_amount,grossloan',
            'chargeType'       => 'required|in:fixed,percentage',
            'defaultApply'     => 'required|in:apply,not-apply',
        ];
    }

    /**
     * @return  bool
     */
    public function authorize()
    {
        return $this->check([
            'hasAccess',
        ]);
    }
}
