<?php

namespace App\Containers\ClosingCharge\UI\API\Requests;

use App\Ship\Parents\Requests\Request;

/**
 * Class CreateClosingLoanChargeRequest.
 */
class CreateClosingLoanChargeRequest extends Request
{

    /**
     * The assigned Transporter for this Request
     *
     * @var string
     */
    // protected $transporter = \App\Containers\ClosingCharge\Data\Transporters\::class;

    /**
     * Define which Roles and/or Permissions has access to this request.
     *
     * @var  array
     */
    protected $access = [
        'permissions' => '',
        'roles'       => '',
    ];

    /**
     * Id's that needs decoding before applying the validation rules.
     *
     * @var  array
     */
    protected $decode = [
        'loanId',
        'closingChargeId',
    ];

    /**
     * Defining the URL parameters (e.g, `/user/{id}`) allows applying
     * validation rules on them and allows accessing them like request data.
     *
     * @var  array
     */
    protected $urlParameters = [
        // 'id',
    ];

    /**
     * @return  array
     */
    public function rules()
    {
        return [
            'loanId'        => 'required|integer|exists:loans,id|unique:closing_loan_charges,loan_id,null,null,closing_charge_id,'.$this->closingChargeId,
            'closingChargeId'      => 'required|integer|exists:closing_charges,id',
            'description'   => 'string|min:3',
            'value'            => 'regex:/^\d*(\.\d{1,2})?$/',
        ];
    }

    /**
     * @return  bool
     */
    public function authorize()
    {
        return $this->check([
            'hasAccess',
        ]);
    }
}
