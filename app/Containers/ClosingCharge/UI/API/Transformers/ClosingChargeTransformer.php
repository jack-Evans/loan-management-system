<?php

namespace App\Containers\ClosingCharge\UI\API\Transformers;

use App\Containers\ClosingCharge\Models\ClosingCharge;
use App\Ship\Parents\Transformers\Transformer;

class ClosingChargeTransformer extends Transformer
{
    /**
     * @var  array
     */
    protected $defaultIncludes = [

    ];

    /**
     * @var  array
     */
    protected $availableIncludes = [

    ];

    /**
     * @param ClosingCharge $entity
     *
     * @return array
     */
    public function transform(ClosingCharge $entity)
    {
        $response = [
            'object' => 'ClosingCharge',
            'id' => $entity->getHashedKey(),
            'name' => $entity->name,
            'value' => $entity->value,
            'amount_type' => $entity->amount_type,
            'charge_type' => $entity->charge_type,
            'default_apply' => $entity->default_apply,
            'created_at' => $entity->created_at,
            'updated_at' => $entity->updated_at,

        ];

        $response = $this->ifAdmin([
            'real_id'    => $entity->id,
            // 'deleted_at' => $entity->deleted_at,
        ], $response);

        return $response;
    }
}
