<?php

namespace App\Containers\ClosingCharge\UI\API\Transformers;

use App\Containers\ClosingCharge\Models\ClosingLoanCharge;
use App\Ship\Parents\Transformers\Transformer;

class ClosingLoanChargeTransformer extends Transformer
{
    /**
     * @var  array
     */
    protected $defaultIncludes = [

    ];

    /**
     * @var  array
     */
    protected $availableIncludes = [

    ];

    /**
     * @param ClosingLoanCharge $entity
     *
     * @return array
     */
    public function transform(ClosingLoanCharge $entity)
    {
        $response = [
            'object' => 'ClosingLoanCharge',
            'id' => $entity->closingCharge->getHashedKey(),
            'value' => $entity->value,
            'description' => $entity->description,
            'name' => $entity->closingCharge->name,
            'charge_type' => $entity->closingCharge->charge_type,
            'created_at' => $entity->created_at,
            'updated_at' => $entity->updated_at,

        ];

        $response = $this->ifAdmin([
            'real_id'    => $entity->id,
            // 'deleted_at' => $entity->deleted_at,
        ], $response);

        return $response;
    }
}
