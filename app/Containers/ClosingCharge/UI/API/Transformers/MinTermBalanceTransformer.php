<?php

namespace App\Containers\ClosingCharge\UI\API\Transformers;

use App\Containers\ClosingCharge\Models\ClosingLoanCharge;
use App\Ship\Parents\Transformers\Transformer;

class MinTermBalanceTransformer extends Transformer
{
    /**
     * @var  array
     */
    protected $defaultIncludes = [

    ];

    /**
     * @var  array
     */
    protected $availableIncludes = [

    ];

    /**
     * @param ClosingLoanCharge $entity
     *
     * @return array
     */
    public function transform(ClosingLoanCharge $entity)
    {
        $response = [
            'object' => 'MinimumTermBalance',
            'min_term' => $entity->min_term,
            'min_term_balance' => $entity->min_term_balance,
            'amount_paid' => $entity->amount_paid,
            'monthly_payment' => $entity->monthly_payment,
            'total_minterm_amount' => $entity->total_minterm_amount,
        ];

        $response = $this->ifAdmin([
            // 'real_id'    => $entity->id,
            // 'deleted_at' => $entity->deleted_at,
        ], $response);

        return $response;
    }
}
