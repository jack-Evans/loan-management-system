<?php

namespace App\Containers\ClosingCharge\Actions;

use App\Ship\Parents\Actions\Action;
use App\Ship\Parents\Requests\Request;
use Apiato\Core\Foundation\Facades\Apiato;

class FindClosingLoanChargeByIdAction extends Action
{
    public function run(Request $request)
    {
        $closingLoanCharge = Apiato::call('ClosingCharge@FindClosingLoanChargeByIdTask', [$request->loanId]);

        return $closingLoanCharge;
    }
}
