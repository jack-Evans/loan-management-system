<?php

namespace App\Containers\ClosingCharge\Actions;

use App\Ship\Parents\Actions\Action;
use App\Ship\Parents\Requests\Request;
use Apiato\Core\Foundation\Facades\Apiato;

class DeleteClosingLoanChargeAction extends Action
{
    public function run(Request $request)
    {
        return Apiato::call('ClosingCharge@DeleteClosingLoanChargeTask', [$request->loanId, $request->chargeId]);
    }
}
