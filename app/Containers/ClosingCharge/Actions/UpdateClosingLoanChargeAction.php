<?php

namespace App\Containers\ClosingCharge\Actions;

use App\Ship\Parents\Actions\Action;
use App\Ship\Parents\Requests\Request;
use Apiato\Core\Foundation\Facades\Apiato;

class UpdateClosingLoanChargeAction extends Action
{
    public function run(Request $request)
    {
        $data = [
         	'value' => $request->value,
        ];

        $closingLoanCharge = Apiato::call('ClosingCharge@UpdateClosingLoanChargeTask', [$request->loanId, $request->chargeId, $data]);

        return $closingLoanCharge;
    }
}
