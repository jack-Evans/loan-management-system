<?php

namespace App\Containers\ClosingCharge\Actions;

use App\Ship\Parents\Actions\Action;
use App\Ship\Parents\Requests\Request;
use Apiato\Core\Foundation\Facades\Apiato;

class GetAllClosingChargesAction extends Action
{
    public function run(Request $request)
    {
        $closingcharges = Apiato::call('ClosingCharge@GetAllClosingChargesTask', [], ['addRequestCriteria']);

        return $closingcharges;
    }
}
