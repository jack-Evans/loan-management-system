<?php

namespace App\Containers\ClosingCharge\Actions;

use App\Ship\Parents\Actions\Action;
use App\Ship\Parents\Requests\Request;
use Apiato\Core\Foundation\Facades\Apiato;

class FindClosingChargeByIdAction extends Action
{
    public function run(Request $request)
    {
        $closingcharge = Apiato::call('ClosingCharge@FindClosingChargeByIdTask', [$request->id]);

        return $closingcharge;
    }
}
