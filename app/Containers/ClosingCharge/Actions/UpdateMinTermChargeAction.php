<?php

namespace App\Containers\ClosingCharge\Actions;

use App\Ship\Parents\Actions\Action;
use App\Ship\Parents\Requests\Request;
use Apiato\Core\Foundation\Facades\Apiato;

class UpdateMinTermChargeAction extends Action
{
    public function run(Request $request)
    {
    	$loan = Apiato::call('Loan@FindLoanByIdTask', [$request->loanId]);
        $data = [];

        if($request->minTerm) {
            if($request->minTerm > $loan->interest->duration)
                throw new \App\Containers\Payment\Exceptions\HTTPPreConditionFailedException('Minimum term duration should be less than loan duration.');
        	$data['min_term'] = $request->minTerm;
        }
        if($request->monthlyPayment)
        	$data['monthly_payment'] = $request->monthlyPayment;

        $interest = Apiato::call('Interest@UpdateInterestTask', [$loan->interest->id, $data]);

        return $interest;
    }
}
