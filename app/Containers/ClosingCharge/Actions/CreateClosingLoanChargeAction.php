<?php

namespace App\Containers\ClosingCharge\Actions;

use App\Ship\Parents\Actions\Action;
use App\Ship\Parents\Requests\Request;
use Apiato\Core\Foundation\Facades\Apiato;

class CreateClosingLoanChargeAction extends Action
{
    public function run(Request $request)
    {
        $data = [
            'loan_id' => $request->loanId,
            'closing_charge_id' => $request->closingChargeId,
            'description' => $request->description,
        ];

        $closingCharge = Apiato::call('ClosingCharge@FindClosingChargeByIdTask', [$request->closingChargeId]);

        if($request->value)
        	$data['value'] = $request->value;
        else
        {
        	$data['value'] = $closingCharge->value;
        }

        $data['description'] = $closingCharge->name;
        
        $closingloancharge = Apiato::call('ClosingCharge@CreateClosingLoanChargeTask', [$data]);

        return $closingloancharge;
    }
}
