<?php

namespace App\Containers\ClosingCharge\Actions;

use App\Ship\Parents\Actions\Action;
use App\Ship\Parents\Requests\Request;
use Apiato\Core\Foundation\Facades\Apiato;

class CreateClosingChargeAction extends Action
{
    public function run(Request $request)
    {
        $data = [
         	'name' => $request->name,   
         	'value' => $request->value,
         	'amount_type' => $request->amountType,
         	'charge_type' => $request->chargeType,
         	'default_apply' => $request->defaultApply,
        ];

        $closingcharge = Apiato::call('ClosingCharge@CreateClosingChargeTask', [$data]);

        return $closingcharge;
    }
}
