<?php

namespace App\Containers\Interest\Models;

use App\Ship\Parents\Models\Model;

class Interest extends Model
{
    protected $fillable = [
        'loan_id',
        'principal_amount',
        'rate',
        'default_rate',
        'duration',
        'half_loan_duration',
        'min_term',
        'monthly_payment',
        'gross_loan',
        'payment_plan_date',
    ];

    protected $attributes = [

    ];

    protected $hidden = [

    ];

    protected $casts = [

    ];

    protected $dates = [
        'payment_plan_date',
        'created_at',
        'updated_at',
    ];

    /**
     * A resource key to be used by the the JSON API Serializer responses.
     */
    protected $resourceKey = 'interests';
}
