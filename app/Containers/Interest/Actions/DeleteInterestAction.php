<?php

namespace App\Containers\Interest\Actions;

use App\Ship\Parents\Actions\Action;
use App\Ship\Parents\Requests\Request;
use Apiato\Core\Foundation\Facades\Apiato;

class DeleteInterestAction extends Action
{
    public function run(Request $request)
    {
        return Apiato::call('Interest@DeleteInterestTask', [$request->id]);
    }
}
