<?php

namespace App\Containers\Interest\Actions;

use App\Ship\Parents\Actions\Action;
use App\Ship\Parents\Requests\Request;
use Apiato\Core\Foundation\Facades\Apiato;

class FindInterestByIdAction extends Action
{
    public function run(Request $request)
    {
        $interest = Apiato::call('Interest@FindInterestByIdTask', [$request->id]);

        return $interest;
    }
}
