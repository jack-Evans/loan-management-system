<?php

namespace App\Containers\Interest\Actions;

use App\Ship\Parents\Actions\Action;
use App\Ship\Parents\Requests\Request;
use Apiato\Core\Foundation\Facades\Apiato;

class UpdateInterestAction extends Action
{
    public function run(Request $request)
    {
        $data = $request->sanitizeInput([
            // add your request data here
        ]);

        $interest = Apiato::call('Interest@UpdateInterestTask', [$request->id, $data]);

        return $interest;
    }
}
