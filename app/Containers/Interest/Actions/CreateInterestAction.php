<?php

namespace App\Containers\Interest\Actions;

use App\Ship\Parents\Actions\Action;
use App\Ship\Parents\Requests\Request;
use Apiato\Core\Foundation\Facades\Apiato;
use App\Containers\Interest\Data\Transporters\CreateInterestTransporter;

class CreateInterestAction extends Action
{
    public function run(Request $request)
    {
        $interest = Apiato::call('Interest@CreateInterestTask', [(new CreateInterestTransporter($request))->toArray()]);

        return $interest;
    }
}
