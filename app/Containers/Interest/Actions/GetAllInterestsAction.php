<?php

namespace App\Containers\Interest\Actions;

use App\Ship\Parents\Actions\Action;
use App\Ship\Parents\Requests\Request;
use Apiato\Core\Foundation\Facades\Apiato;

class GetAllInterestsAction extends Action
{
    public function run(Request $request)
    {
        $interests = Apiato::call('Interest@GetAllInterestsTask', [], ['addRequestCriteria']);

        return $interests;
    }
}
