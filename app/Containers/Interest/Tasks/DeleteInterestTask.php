<?php

namespace App\Containers\Interest\Tasks;

use App\Containers\Interest\Data\Repositories\InterestRepository;
use App\Ship\Exceptions\DeleteResourceFailedException;
use App\Ship\Parents\Tasks\Task;
use Exception;

class DeleteInterestTask extends Task
{

    protected $repository;

    public function __construct(InterestRepository $repository)
    {
        $this->repository = $repository;
    }

    public function run($id)
    {
        try {
            return $this->repository->delete($id);
        }
        catch (Exception $exception) {
            throw new DeleteResourceFailedException();
        }
    }
}
