<?php

namespace App\Containers\Interest\Tasks;

use App\Containers\Interest\Data\Repositories\InterestRepository;
use App\Ship\Exceptions\CreateResourceFailedException;
use App\Ship\Parents\Tasks\Task;
use Exception;

class CreateInterestTask extends Task
{

    protected $repository;

    public function __construct(InterestRepository $repository)
    {
        $this->repository = $repository;
    }

    public function run(array $data)
    {
        try {
            return $this->repository->create($data);
        }
        catch (Exception $exception) {
             \DB::rollback();
            throw new CreateResourceFailedException();
        }
    }
}
