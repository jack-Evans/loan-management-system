<?php

namespace App\Containers\Interest\Tasks;

use App\Containers\Interest\Data\Repositories\InterestRepository;
use App\Ship\Exceptions\NotFoundException;
use App\Ship\Parents\Tasks\Task;
use Exception;

class FindInterestByIdTask extends Task
{

    protected $repository;

    public function __construct(InterestRepository $repository)
    {
        $this->repository = $repository;
    }

    public function run($id)
    {
        try {
            return $this->repository->find($id);
        }
        catch (Exception $exception) {
            throw new NotFoundException();
        }
    }
}
