<?php

namespace App\Containers\Interest\Tasks;

use App\Containers\Interest\Data\Repositories\InterestRepository;
use App\Ship\Exceptions\NotFoundException;
use App\Ship\Parents\Tasks\Task;
use Exception;

class GetInterestsAsPmtPlanVersionsTask extends Task
{

    protected $repository;

    public function __construct(InterestRepository $repository)
    {
        $this->repository = $repository;
    }

    public function run($loanId, $payBeforeDate = null)
    {
        return $this->repository->scopeQuery(function($q) use($payBeforeDate, $loanId){
            if($payBeforeDate)
                return $q->where('payment_plan_date', '<', $payBeforeDate)->where('loan_id', $loanId);
            else
                return $q->where('loan_id', $loanId);
        })->orderBy('payment_plan_date', 'ASC')->get();
    }
}
