<?php

namespace App\Containers\Interest\Tasks;

use App\Containers\Interest\Data\Repositories\InterestRepository;
use App\Ship\Exceptions\UpdateResourceFailedException;
use App\Ship\Parents\Tasks\Task;
use Exception;

class UpdateInterestTask extends Task
{

    protected $repository;

    public function __construct(InterestRepository $repository)
    {
        $this->repository = $repository;
    }

    public function run($id, array $data)
    {
        try {
            return $this->repository->update($data, $id);
        }
        catch (Exception $exception) {
            \DB::rollback();
            throw new UpdateResourceFailedException();
        }
    }
}
