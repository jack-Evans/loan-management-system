<?php

namespace App\Containers\Interest\Tasks;

use App\Containers\Interest\Data\Repositories\InterestRepository;
use App\Ship\Parents\Tasks\Task;

class GetAllInterestsTask extends Task
{

    protected $repository;

    public function __construct(InterestRepository $repository)
    {
        $this->repository = $repository;
    }

    public function run()
    {
        return $this->repository->paginate();
    }
}
