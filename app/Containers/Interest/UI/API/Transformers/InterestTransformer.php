<?php

namespace App\Containers\Interest\UI\API\Transformers;

use App\Containers\Interest\Models\Interest;
use App\Ship\Parents\Transformers\Transformer;

class InterestTransformer extends Transformer
{
    /**
     * @var  array
     */
    protected $defaultIncludes = [

    ];

    /**
     * @var  array
     */
    protected $availableIncludes = [

    ];

    /**
     * @param Interest $entity
     *
     * @return array
     */
    public function transform(Interest $entity)
    {
        $response = [
            'object' => 'Interest',
            'id' => $entity->getHashedKey(),
            'created_at' => $entity->created_at,
            'updated_at' => $entity->updated_at,

        ];

        $response = $this->ifAdmin([
            'real_id'    => $entity->id,
            // 'deleted_at' => $entity->deleted_at,
        ], $response);

        return $response;
    }
}
