<?php

namespace App\Containers\Interest\UI\API\Requests;

use App\Ship\Parents\Requests\Request;

/**
 * Class CreateInterestRequest.
 */
class CreateInterestRequest extends Request
{

    /**
     * The assigned Transporter for this Request
     *
     * @var string
     */
    protected $transporter = \App\Containers\Interest\Data\Transporters\CreateInterestTransporter::class;

    /**
     * Define which Roles and/or Permissions has access to this request.
     *
     * @var  array
     */
    protected $access = [
        'permissions' => '',
        'roles'       => '',
    ];

    /**
     * Id's that needs decoding before applying the validation rules.
     *
     * @var  array
     */
    protected $decode = [
        // 'id',
    ];

    /**
     * Defining the URL parameters (e.g, `/user/{id}`) allows applying
     * validation rules on them and allows accessing them like request data.
     *
     * @var  array
     */
    protected $urlParameters = [
        // 'id',
    ];

    /**
     * @return  array
     */
    public function rules()
    {
        return [
            'loanid'                => 'required|unique:interest,loan_id|exists:loans,id',
            'duration'              => 'required|integer|min:1',
            'principal_amount'      => 'required|regex:/^\d*(\.\d{1,2})?$/',
            'rate'                  => 'required|regex:/^\d*(\.\d{1,2})?$/',
            'default_rate'          => 'required|regex:/^\d*(\.\d{1,2})?$/',
            'grossloan'             => 'required|regex:/^\d*(\.\d{1,2})?$/',
        ];
    }

    /**
     * @return  bool
     */
    public function authorize()
    {
        return $this->check([
            'hasAccess',
        ]);
    }
}
