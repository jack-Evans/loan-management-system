<?php

namespace App\Containers\Interest\UI\API\Controllers;

use App\Containers\Interest\UI\API\Requests\CreateInterestRequest;
use App\Containers\Interest\UI\API\Requests\DeleteInterestRequest;
use App\Containers\Interest\UI\API\Requests\GetAllInterestsRequest;
use App\Containers\Interest\UI\API\Requests\FindInterestByIdRequest;
use App\Containers\Interest\UI\API\Requests\UpdateInterestRequest;
use App\Containers\Interest\UI\API\Transformers\InterestTransformer;
use App\Ship\Parents\Controllers\ApiController;
use Apiato\Core\Foundation\Facades\Apiato;

/**
 * Class Controller
 *
 * @package App\Containers\Interest\UI\API\Controllers
 */
class Controller extends ApiController
{
    /**
     * @param CreateInterestRequest $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function createInterest(CreateInterestRequest $request)
    {
        $interest = Apiato::call('Interest@CreateInterestAction', [$request]);

        return $this->created($this->transform($interest, InterestTransformer::class));
    }

    /**
     * @param FindInterestByIdRequest $request
     * @return array
     */
    public function findInterestById(FindInterestByIdRequest $request)
    {
        $interest = Apiato::call('Interest@FindInterestByIdAction', [$request]);

        return $this->transform($interest, InterestTransformer::class);
    }

    /**
     * @param GetAllInterestsRequest $request
     * @return array
     */
    public function getAllInterests(GetAllInterestsRequest $request)
    {
        $interests = Apiato::call('Interest@GetAllInterestsAction', [$request]);

        return $this->transform($interests, InterestTransformer::class);
    }

    /**
     * @param UpdateInterestRequest $request
     * @return array
     */
    public function updateInterest(UpdateInterestRequest $request)
    {
        $interest = Apiato::call('Interest@UpdateInterestAction', [$request]);

        return $this->transform($interest, InterestTransformer::class);
    }

    /**
     * @param DeleteInterestRequest $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function deleteInterest(DeleteInterestRequest $request)
    {
        Apiato::call('Interest@DeleteInterestAction', [$request]);

        return $this->noContent();
    }
}
