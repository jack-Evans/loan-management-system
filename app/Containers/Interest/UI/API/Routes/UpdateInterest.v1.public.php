<?php

/**
 * @apiGroup           Interest
 * @apiName            updateInterest
 *
 * @api                {PATCH} /v1/interests/:id Endpoint title here..
 * @apiDescription     Endpoint description here..
 *
 * @apiVersion         1.0.0
 * @apiPermission      none
 *
 * @apiParam           {String}  parameters here..
 *
 * @apiSuccessExample  {json}  Success-Response:
 * HTTP/1.1 200 OK
{
  // Insert the response of the request here...
}
 */

/** @var Route $router */
$router->patch('interests/{id}', [
    'as' => 'api_interest_update_interest',
    'uses'  => 'Controller@updateInterest',
    'middleware' => [
      'auth:api',
    ],
]);
