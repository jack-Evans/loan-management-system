<?php

/**
 * @apiGroup           Interest
 * @apiName            getAllInterests
 *
 * @api                {GET} /v1/interests Endpoint title here..
 * @apiDescription     Endpoint description here..
 *
 * @apiVersion         1.0.0
 * @apiPermission      none
 *
 * @apiParam           {String}  parameters here..
 *
 * @apiSuccessExample  {json}  Success-Response:
 * HTTP/1.1 200 OK
{
  // Insert the response of the request here...
}
 */

/** @var Route $router */
$router->get('interests', [
    'as' => 'api_interest_get_all_interests',
    'uses'  => 'Controller@getAllInterests',
    'middleware' => [
      'auth:api',
    ],
]);
