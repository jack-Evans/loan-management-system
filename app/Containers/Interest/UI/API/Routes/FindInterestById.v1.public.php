<?php

/**
 * @apiGroup           Interest
 * @apiName            findInterestById
 *
 * @api                {GET} /v1/interests/:id Endpoint title here..
 * @apiDescription     Endpoint description here..
 *
 * @apiVersion         1.0.0
 * @apiPermission      none
 *
 * @apiParam           {String}  parameters here..
 *
 * @apiSuccessExample  {json}  Success-Response:
 * HTTP/1.1 200 OK
{
  // Insert the response of the request here...
}
 */

/** @var Route $router */
$router->get('interests/{id}', [
    'as' => 'api_interest_find_interest_by_id',
    'uses'  => 'Controller@findInterestById',
    'middleware' => [
      'auth:api',
    ],
]);
