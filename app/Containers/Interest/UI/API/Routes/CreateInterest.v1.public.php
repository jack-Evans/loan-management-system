<?php

/**
 * @apiGroup           Interest
 * @apiName            createInterest
 *
 * @api                {POST} /v1/interests Endpoint title here..
 * @apiDescription     Endpoint description here..
 *
 * @apiVersion         1.0.0
 * @apiPermission      none
 *
 * @apiParam           {String}  parameters here..
 *
 * @apiSuccessExample  {json}  Success-Response:
 * HTTP/1.1 200 OK
{
  // Insert the response of the request here...
}
 */

/** @var Route $router */
$router->post('interests', [
    'as' => 'api_interest_create_interest',
    'uses'  => 'Controller@createInterest',
    'middleware' => [
      'auth:api',
    ],
]);
