<?php

/**
 * @apiGroup           Interest
 * @apiName            deleteInterest
 *
 * @api                {DELETE} /v1/interests/:id Endpoint title here..
 * @apiDescription     Endpoint description here..
 *
 * @apiVersion         1.0.0
 * @apiPermission      none
 *
 * @apiParam           {String}  parameters here..
 *
 * @apiSuccessExample  {json}  Success-Response:
 * HTTP/1.1 200 OK
{
  // Insert the response of the request here...
}
 */

/** @var Route $router */
$router->delete('interests/{id}', [
    'as' => 'api_interest_delete_interest',
    'uses'  => 'Controller@deleteInterest',
    'middleware' => [
      'auth:api',
    ],
]);
