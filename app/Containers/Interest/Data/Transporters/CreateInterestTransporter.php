<?php

namespace App\Containers\Interest\Data\Transporters;

use App\Ship\Parents\Transporters\Transporter;

class CreateInterestTransporter extends Transporter
{

    /**
     * @var array
     */
    protected $schema = [
        'type' => 'object',
        'properties' => [
            'principal_amount'      => ['type' => 'number'],
            'rate'                  => ['type' => 'number'],
            'default_rate'          => ['type' => 'number'],
            'duration'              => ['type' => 'number'],
            'half_loan_duration'    => ['type' => 'number'],
            'min_term'              => ['type' => 'integer'],
            'loan_id'               => ['type' => 'integer'],
            'gross_loan'            => ['type' => 'number'],
            'monthly_payment'       => ['type' => 'number'],
        ],
        'required'   => [
            'principal_amount',
            'rate',
            'default_rate',
            'duration',
            // 'half_loan_duration',
            'min_term',
            // 'monthly_payment',
            'loan_id',
            'gross_loan'
        ],
        'default'    => [
            // provide default values for specific properties here
        ]
    ];
}
