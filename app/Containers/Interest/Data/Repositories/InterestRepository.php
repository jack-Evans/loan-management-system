<?php

namespace App\Containers\Interest\Data\Repositories;

use App\Ship\Parents\Repositories\Repository;

/**
 * Class InterestRepository
 */
class InterestRepository extends Repository
{

    /**
     * @var array
     */
    protected $fieldSearchable = [
        'id' => '=',
        // ...
    ];

}
