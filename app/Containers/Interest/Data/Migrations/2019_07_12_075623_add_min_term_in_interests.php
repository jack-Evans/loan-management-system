<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class AddMinTermInInterests extends Migration
{

    /**
     * Run the migrations.
     */
    public function up()
    {
        Schema::table('interests', function (Blueprint $table) {
            $table->float('min_term', 12, 4)->nullable()->default(0)->after('duration');
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down()
    {
        Schema::table('interests', function($table) {
            $table->dropcolumn('min_term');
        });
    }
}
