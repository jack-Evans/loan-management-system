<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class AddMonthlyPaymentInInterests extends Migration
{

    /**
     * Run the migrations.
     */
    public function up()
    {
        Schema::table('interests', function (Blueprint $table) {
            $table->float('monthly_payment', 12, 4)->nullable()->default(0)->after('min_term');
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down()
    {
        Schema::table('interests', function($table) {
            $table->dropcolumn('monthly_payment');
        });
    }
}
