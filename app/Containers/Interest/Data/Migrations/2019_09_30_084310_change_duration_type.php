<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class ChangeDurationType extends Migration
{

    /**
     * Run the migrations.
     */
    public function up()
    {
        Schema::table('interests', function (Blueprint $table) {
            $table->float('duration', 12, 4)->change();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down()
    {
        Schema::table('interests', function (Blueprint $table) {
            $table->unsignedInteger('duration')->change();
        });
    }
}
