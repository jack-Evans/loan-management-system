<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateInterestTables extends Migration
{

    /**
     * Run the migrations.
     */
    public function up()
    {
        Schema::create('interests', function (Blueprint $table) {

            $table->increments('id');
            $table->float('principal_amount', 12, 4);
            $table->float('gross_loan', 12, 4);
            $table->float('rate', 12, 4);
            $table->float('default_rate', 12, 4);
            $table->unsignedInteger('duration');
            $table->unsignedInteger('loan_id');
            $table->timestamps();
            $table->softDeletes();

        });

    }

    /**
     * Reverse the migrations.
     */
    public function down()
    {
        Schema::drop('interests');
    }
}
