<?php

namespace App\Containers\Tag\Actions;

use App\Ship\Parents\Actions\Action;
use App\Ship\Parents\Requests\Request;
use Apiato\Core\Foundation\Facades\Apiato;

class CreateJournalTagAction extends Action
{
    public function run(Request $request)
    {
    	$tagids = $request->tagid;
    	foreach ($tagids as $key => $tagid) {
	        $data = [
	        	'journal_id' => $request->journalid,
	        	'tag_id' => \Hashids::decode($tagid)[0],
	        ];
        	$journalTag = Apiato::call('Tag@CreateJournalTagTask', [$data]);
    	}


        return $journalTag;
    }
}
