<?php

namespace App\Containers\Tag\Actions;

use App\Ship\Parents\Actions\Action;
use App\Ship\Parents\Requests\Request;
use Apiato\Core\Foundation\Facades\Apiato;
use App\Containers\Tag\Models\LoanTag;

class CreateLoanTagAction extends Action
{
    public function run(Request $request)
    {
        $loanId = $request->loanid;
        $existingIds = [];
        $previousTags = LoanTag::where('loan_id', $loanId)->get();
    	$hashedIds = $request->tagid;
        
        \DB::beginTransaction();
    	foreach ($hashedIds as $key => $hashId) {
            
            $tagId = \Hashids::decode($hashId)[0];

            $loanTag = $previousTags->where('loan_id', $loanId)->where('tag_id', $tagId)->first();
            if($loanTag){
                $existingIds[] = $loanTag->id;
                continue;
            }else{
                $data = [
    	        	'loan_id' => $loanId,
    	        	'tag_id' => $tagId,
    	        ];
        	   $loanTag = Apiato::call('Tag@CreateLoanTagTask', [$data]);
               $existingIds[] = $loanTag->id;
            }
    	}
        // \DB::enableQueryLog(); // Enable query log
        LoanTag::whereNotIn('id', $existingIds)->where('loan_id', $loanId)->delete();
        // dd(\DB::getQueryLog()); // Show results of log
        \DB::commit();
        
        return $loanTag;
    }
}
