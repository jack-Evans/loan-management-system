<?php

namespace App\Containers\Tag\Models;

use App\Ship\Parents\Models\Model;

class LoanTag extends Model
{
    protected $fillable = [
        'loan_id',
        'tag_id',
    ];

    protected $attributes = [

    ];

    protected $hidden = [

    ];

    protected $casts = [

    ];

    protected $dates = [
        'created_at',
        'updated_at',
    ];

    /**
     * A resource key to be used by the the JSON API Serializer responses.
     */
    protected $resourceKey = 'tags';
}
