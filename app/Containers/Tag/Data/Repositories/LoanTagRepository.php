<?php

namespace App\Containers\Tag\Data\Repositories;

use App\Ship\Parents\Repositories\Repository;

/**
 * Class LoanTagRepository
 */
class LoanTagRepository extends Repository
{

    /**
     * @var array
     */
    protected $fieldSearchable = [
        'id' => '=',
        // ...
    ];

}
