<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateJournalTagsTable extends Migration
{

    /**
     * Run the migrations.
     */
    public function up()
    {
        Schema::create('journal_tags', function (Blueprint $table) {

            $table->increments('id');
            $table->unsignedInteger('journal_id');
            $table->unsignedInteger('tag_id')->default(1);
            $table->timestamps();
            //$table->softDeletes();

        });
    }

    /**
     * Reverse the migrations.
     */
    public function down()
    {
        Schema::dropIfExists('journal_tags');
    }
}
