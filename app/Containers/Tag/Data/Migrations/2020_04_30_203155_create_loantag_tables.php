<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateLoantagTables extends Migration
{

    /**
     * Run the migrations.
     */
    public function up()
    {
        Schema::create('loan_tags', function (Blueprint $table) {

            $table->increments('id');
            $table->unsignedInteger('loan_id');
            $table->unsignedInteger('tag_id')->default(1);
            $table->timestamps();
            //$table->softDeletes();

        });
    }

    /**
     * Reverse the migrations.
     */
    public function down()
    {
        Schema::dropIfExists('loan_tags');
    }
}
