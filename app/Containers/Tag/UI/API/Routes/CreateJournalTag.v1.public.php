<?php

/**
 * @apiGroup           Tag
 * @apiName            createTag
 *
 * @api                {POST} /v1/tags Endpoint title here..
 * @apiDescription     Endpoint description here..
 *
 * @apiVersion         1.0.0
 * @apiPermission      none
 *
 * @apiParam           {String}  parameters here..
 *
 * @apiSuccessExample  {json}  Success-Response:
 * HTTP/1.1 200 OK
{
  // Insert the response of the request here...
}
 */

/** @var Route $router */
$router->post('createjournaltag', [
    'as' => 'api_tag_create_journal_tag',
    'uses'  => 'Controller@createJournalTag',
    'middleware' => [
      'auth:api',
    ],
]);
