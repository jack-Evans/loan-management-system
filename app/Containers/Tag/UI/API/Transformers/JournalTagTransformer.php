<?php

namespace App\Containers\Tag\UI\API\Transformers;

use App\Containers\Tag\Models\JournalTag;
use App\Ship\Parents\Transformers\Transformer;

class JournalTagTransformer extends Transformer
{
    /**
     * @var  array
     */
    protected $defaultIncludes = [

    ];

    /**
     * @var  array
     */
    protected $availableIncludes = [

    ];

    /**
     * @param Tag $entity
     *
     * @return array
     */
    public function transform(JournalTag $entity)
    {
        $response = [
            'object' => 'Tag',
            'id' => $entity->getHashedKey(),
            'journal_id' => $entity->journal_id,
            'tag_id' => $entity->tag_id,
            'created_at' => $entity->created_at,
            'updated_at' => $entity->updated_at,

        ];

        $response = $this->ifAdmin([
            'real_id'    => $entity->id,
            // 'deleted_at' => $entity->deleted_at,
        ], $response);

        return $response;
    }
}
