<?php

namespace App\Containers\Tag\UI\WEB\Controllers;

use App\Containers\Tag\UI\WEB\Requests\CreateTagRequest;
use App\Containers\Tag\UI\WEB\Requests\DeleteTagRequest;
use App\Containers\Tag\UI\WEB\Requests\GetAllTagsRequest;
use App\Containers\Tag\UI\WEB\Requests\FindTagByIdRequest;
use App\Containers\Tag\UI\WEB\Requests\UpdateTagRequest;
use App\Containers\Tag\UI\WEB\Requests\StoreTagRequest;
use App\Containers\Tag\UI\WEB\Requests\EditTagRequest;
use App\Ship\Parents\Controllers\WebController;
use Apiato\Core\Foundation\Facades\Apiato;


/**
 * Class Controller
 *
 * @package App\Containers\Tag\UI\WEB\Controllers
 */
class Controller extends WebController
{
    /**
     * The assigned API PATH for this Controller
     *
     * @var string
     */
    
    protected $apiPath;
    
    public function __construct(){
        $this->apiPath = config('token-container.WEB_API_URL');
    }

    /**
     * Show all entities
     *
     * @param GetAllTagsRequest $request
     */
    public function index(GetAllTagsRequest $request)
    {
        $tagUrl = $this->apiPath.'tags';
        try {
            $response = get($tagUrl, ['limit' => 0]);
            $tags =isset($response->data) ? $response->data : array();
        } catch (\Exception $e) {
            return exception($e);
        }

        return view('tag::tags', compact('tags'));
    }

    /**
     * Show one entity
     *
     * @param FindTagByIdRequest $request
     */
    public function show(FindTagByIdRequest $request)
    {
        $tag = Apiato::call('Tag@FindTagByIdAction', [$request]);

        // ..
    }

    /**
     * Create entity (show UI)
     *
     * @param CreateTagRequest $request
     */
    public function create(CreateTagRequest $request)
    {
        return view('tag::create-update-tag');
    }

    /**
     * Add a new entity
     *
     * @param StoreTagRequest $request
     */
    public function store(StoreTagRequest $request)
    {
        $tagUrl = $this->apiPath.'tags';

        $requestData = [
            'name' => $request->name,
        ];
        try {
            $response = post($tagUrl, $requestData);
            $tags = $response->data;
        } catch (\Exception $e) {
            return exception($e);
        }

        return redirect('tags');
    }

    /**
     * Edit entity (show UI)
     *
     * @param EditTagRequest $request
     */
    public function edit(EditTagRequest $request)
    {
        $tag = Apiato::call('Tag@GetTagByIdAction', [$request]);

        // ..
    }

    /**
     * Update a given entity
     *
     * @param UpdateTagRequest $request
     */
    public function update(UpdateTagRequest $request)
    {
        $tag = Apiato::call('Tag@UpdateTagAction', [$request]);

        // ..
    }

    /**
     * Delete a given entity
     *
     * @param DeleteTagRequest $request
     */
    public function delete(DeleteTagRequest $request)
    {
         $result = Apiato::call('Tag@DeleteTagAction', [$request]);

         // ..
    }
}
