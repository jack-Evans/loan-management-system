@extends('layouts.app')

@section('title', 'Tags')

@section('content')
<div class="col-lg-12">
    <div class="has-bg">
        <h3>
            Tags
            <a href="{{url('tags/create')}}" class="pull-right">
                <button class="btn btn-success">Create Tag</button>
            </a>
        </h3>
        <span class="space"></span>
        <table class="table table-striped table-hover table-bordered text-left MDBootstrapDatatable" cellspacing="0" width="100%">
            <thead>
                <tr>
                    <!-- <th class="th-sm">Code</th> -->
                    <th>Name</th>
                    <!-- <th>Actions</th> -->
                </tr>
            </thead>
            <tbody>
                @foreach($tags as $key => $tag)
                <tr>
                    <td class="text-capitalize">{{$tag->name}}</td>
                    <!-- <td>
                        <a href="{{url('tags/'.$tag->id.'/edit')}}">
                            <button class="btn btn-primary">Edit</button>
                        </a>
                    </td> -->
                </tr>
                @endforeach
            </tbody>
        </table>
    </div>
</div>
@endsection
