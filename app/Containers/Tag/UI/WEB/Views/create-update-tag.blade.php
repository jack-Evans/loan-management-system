@extends('layouts.app')

@section('title', 'Tag')

@section('content')
<div class="col-lg-12">
    <div class="has-bg">
      <div class="row">
          <div class="col-md-5 col-md-offset-2">
          <h3>{{isset($tag->id) ? 'Edit ' : 'Create '}}Tag</h3>
              <form action="{{url(isset($tag->id) ? 'tags/'.$tag->id.'/update': 'tags/store' )}}" method="post">
                  @csrf()
                  <div class="form-group">
                    <label for="name">Name:</label>
                    <input type="text" class="form-control" name="name"
                           value="{{isset($tag->name) ? $nominalCode->name : old('name')}}">
                  </div>
                  <button type="submit" class="btn btn-primary">Submit</button>
              </form>
          </div>
      </div>
    </div>
</div>
@endsection
