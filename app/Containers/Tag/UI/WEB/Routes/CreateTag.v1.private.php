<?php

/** @var Route $router */
$router->get('tags/create', [
    'as' => 'web_tag_create',
    'uses'  => 'Controller@create',
    'middleware' => [
      'auth:web',
    ],
]);
