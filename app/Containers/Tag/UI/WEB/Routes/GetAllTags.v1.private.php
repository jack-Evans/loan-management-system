<?php

/** @var Route $router */
$router->get('tags', [
    'as' => 'web_tag_index',
    'uses'  => 'Controller@index',
    'middleware' => [
      'auth:web',
    ],
]);
