<?php

/** @var Route $router */
$router->post('tags/store', [
    'as' => 'web_tag_store',
    'uses'  => 'Controller@store',
    'middleware' => [
      'auth:web',
    ],
]);
