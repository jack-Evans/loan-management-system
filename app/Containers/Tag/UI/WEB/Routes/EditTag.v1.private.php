<?php

/** @var Route $router */
$router->get('tags/{id}/edit', [
    'as' => 'web_tag_edit',
    'uses'  => 'Controller@edit',
    'middleware' => [
      'auth:web',
    ],
]);
