<?php

/** @var Route $router */
$router->patch('tags/{id}', [
    'as' => 'web_tag_update',
    'uses'  => 'Controller@update',
    'middleware' => [
      'auth:web',
    ],
]);
