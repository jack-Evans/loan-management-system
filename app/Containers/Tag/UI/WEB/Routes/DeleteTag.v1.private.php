<?php

/** @var Route $router */
$router->delete('tags/{id}', [
    'as' => 'web_tag_delete',
    'uses'  => 'Controller@delete',
    'middleware' => [
      'auth:web',
    ],
]);
