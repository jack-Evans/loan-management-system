<?php

/** @var Route $router */
$router->get('tags/{id}', [
    'as' => 'web_tag_show',
    'uses'  => 'Controller@show',
    'middleware' => [
      'auth:web',
    ],
]);
