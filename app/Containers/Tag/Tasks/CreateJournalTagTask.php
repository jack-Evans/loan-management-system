<?php

namespace App\Containers\Tag\Tasks;

use App\Containers\Tag\Data\Repositories\JournalTagRepository;
use App\Ship\Exceptions\CreateResourceFailedException;
use App\Ship\Parents\Tasks\Task;
use Exception;

class CreateJournalTagTask extends Task
{

    protected $journalTagRepository;

    public function __construct(JournalTagRepository $journalTagRepository)
    {
        $this->journalTagRepository = $journalTagRepository;
    }

    public function run(array $data)
    {
        try {
            return $this->journalTagRepository->create($data);
        }
        catch (Exception $exception) {
            throw new CreateResourceFailedException();
        }
    }
}
