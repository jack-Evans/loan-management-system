<?php

namespace App\Containers\Tag\Tasks;

use App\Ship\Parents\Tasks\Task;
use App\Containers\Tag\Models\LoanTag;

class GetAllLoansBasedOnTagsTask extends Task
{

    public function run($tagIds)
    {
    	$ids = array();
    	foreach ($tagIds as $key => $tagId) {
    		array_push($ids, \Hashids::decode($tagId)[0]);
    	}
    	$loanTags = LoanTag::whereIn('tag_id', $ids)->groupBy('loan_id')->get();
        return $loanTags;
    }
}
