<?php

namespace App\Containers\Tranch\Data\Repositories;

use App\Ship\Parents\Repositories\Repository;

/**
 * Class TranchRepository
 */
class TranchRepository extends Repository
{

    /**
     * @var array
     */
    protected $fieldSearchable = [
        'id' => '=',
        // ...
    ];

}
