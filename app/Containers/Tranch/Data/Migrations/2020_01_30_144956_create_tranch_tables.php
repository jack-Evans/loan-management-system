<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateTranchTables extends Migration
{

    /**
     * Run the migrations.
     */
    public function up()
    {
        Schema::create('tranches', function (Blueprint $table) {

            $table->increments('id');
            $table->unsignedInteger('loan_id');
            $table->float('amount', 12, 4);
            $table->float('net_loan', 12, 4)->nullable();
            $table->float('gross_loan', 12, 4)->nullable();
            $table->date('tranch_date');
            $table->string('description')->nullable();
            $table->timestamps();
            //$table->softDeletes();

        });
    }

    /**
     * Reverse the migrations.
     */
    public function down()
    {
        Schema::dropIfExists('tranches');
    }
}
