<?php

namespace App\Containers\Tranch\Actions;

use App\Ship\Parents\Actions\Action;
use App\Ship\Parents\Requests\Request;
use Apiato\Core\Foundation\Facades\Apiato;

class GetAllTranchesAction extends Action
{
    public function run(Request $request)
    {
        $tranches = Apiato::call('Tranch@GetAllTranchesTask', [], ['addRequestCriteria']);

        return $tranches;
    }
}
