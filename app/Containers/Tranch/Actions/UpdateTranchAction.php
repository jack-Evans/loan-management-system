<?php

namespace App\Containers\Tranch\Actions;

use App\Ship\Parents\Actions\Action;
use App\Ship\Parents\Requests\Request;
use Apiato\Core\Foundation\Facades\Apiato;

class UpdateTranchAction extends Action
{
    public function run(Request $request)
    {
        $data = $request->sanitizeInput([
            // add your request data here
        ]);

        $tranch = Apiato::call('Tranch@UpdateTranchTask', [$request->id, $data]);

        return $tranch;
    }
}
