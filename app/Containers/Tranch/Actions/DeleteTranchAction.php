<?php

namespace App\Containers\Tranch\Actions;

use App\Ship\Parents\Actions\Action;
use App\Ship\Parents\Requests\Request;
use Apiato\Core\Foundation\Facades\Apiato;

class DeleteTranchAction extends Action
{
    public function run(Request $request)
    {
        return Apiato::call('Tranch@DeleteTranchTask', [$request->id]);
    }
}
