<?php

namespace App\Containers\Tranch\Actions;

use App\Ship\Parents\Actions\Action;
use App\Ship\Parents\Requests\Request;
use Apiato\Core\Foundation\Facades\Apiato;
use Carbon\Carbon;

class CreateTranchAction extends Action
{
    public function run(Request $request)
    {
        $loan = Apiato::call('Loan@FindLoanByIdTask', [$request->loanid]);
        Apiato::call('Statement@VerifyStatementDateTask', [$loan->id, $request->tranchDate]);

        $data = [
        	'loan_id' 		=> $request->loanid,
        	'amount' 		=> $request->amount,
            // 'net_loan'      => $request->grossLoan,
            'gross_loan'    => $request->grossLoan,
        	'tranch_date' 	=> $request->tranchDate,
        	'description' 	=> $request->description,
        ];

        \DB::beginTransaction();
        $tranch = Apiato::call('Tranch@CreateTranchTask', [$data]);
        $tnxDate = Carbon::parse($request->tranchDate);
        $journalPostingRequest = [
            'creditAccountId' => 0,
            'debitAccountId' => $loan->customer_id,
            'type' => 'tranch',
            'loanid' => $loan->id,
            'tnxDate' => $tnxDate->format('Y-m-d'),
            'amount' => $tranch->amount,
            'accountingPeriod' => $tnxDate->format('M Y'),
            'assetType' => 'tranch',
            'description' => $tranch->description,
            'nominalCode' => $request->nominalCode,
            'payableId' => $tranch->id,
            'payableType' => 'App\Containers\Tranch\Models\Tranch',
        ];
        $journal = Apiato::call('Payment@CreateJournalPostingTask', [$journalPostingRequest]);
        // dd($tranch, $journal);
        \DB::commit();

        return $tranch;
    }
}
