<?php

namespace App\Containers\Tranch\Actions;

use App\Ship\Parents\Actions\Action;
use App\Ship\Parents\Requests\Request;
use Apiato\Core\Foundation\Facades\Apiato;

class FindTranchByIdAction extends Action
{
    public function run(Request $request)
    {
        $tranch = Apiato::call('Tranch@FindTranchByIdTask', [$request->id]);

        return $tranch;
    }
}
