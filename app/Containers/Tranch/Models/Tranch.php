<?php

namespace App\Containers\Tranch\Models;

use App\Ship\Parents\Models\Model;

class Tranch extends Model
{
    protected $fillable = [
        'loan_id',
        'amount',
        'net_loan',
        'gross_loan',
        'tranch_date',
        'description',
    ];

    protected $attributes = [

    ];

    protected $hidden = [

    ];

    protected $casts = [

    ];

    protected $dates = [
        'tranch_date',
        'created_at',
        'updated_at',
    ];

    /**
     * A resource key to be used by the the JSON API Serializer responses.
     */
    protected $resourceKey = 'tranches';

    public function journal() {
        return $this->morphOne(\App\Containers\Payment\Models\Journal::class, 'payable')->with('posts');
    }
}
