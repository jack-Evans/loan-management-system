<?php

namespace App\Containers\Tranch\UI\API\Controllers;

use App\Containers\Tranch\UI\API\Requests\CreateTranchRequest;
use App\Containers\Tranch\UI\API\Requests\DeleteTranchRequest;
use App\Containers\Tranch\UI\API\Requests\GetAllTranchesRequest;
use App\Containers\Tranch\UI\API\Requests\FindTranchByIdRequest;
use App\Containers\Tranch\UI\API\Requests\UpdateTranchRequest;
use App\Containers\Tranch\UI\API\Transformers\TranchTransformer;
use App\Ship\Parents\Controllers\ApiController;
use Apiato\Core\Foundation\Facades\Apiato;

/**
 * Class Controller
 *
 * @package App\Containers\Tranch\UI\API\Controllers
 */
class Controller extends ApiController
{
    /**
     * @param CreateTranchRequest $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function createTranch(CreateTranchRequest $request)
    {
        $tranch = Apiato::call('Tranch@CreateTranchAction', [$request]);

        return $this->created($this->transform($tranch, TranchTransformer::class));
    }

    /**
     * @param FindTranchByIdRequest $request
     * @return array
     */
    public function findTranchById(FindTranchByIdRequest $request)
    {
        $tranch = Apiato::call('Tranch@FindTranchByIdAction', [$request]);

        return $this->transform($tranch, TranchTransformer::class);
    }

    /**
     * @param GetAllTranchesRequest $request
     * @return array
     */
    public function getAllTranches(GetAllTranchesRequest $request)
    {
        $tranches = Apiato::call('Tranch@GetAllTranchesAction', [$request]);

        return $this->transform($tranches, TranchTransformer::class);
    }

    /**
     * @param UpdateTranchRequest $request
     * @return array
     */
    public function updateTranch(UpdateTranchRequest $request)
    {
        $tranch = Apiato::call('Tranch@UpdateTranchAction', [$request]);

        return $this->transform($tranch, TranchTransformer::class);
    }

    /**
     * @param DeleteTranchRequest $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function deleteTranch(DeleteTranchRequest $request)
    {
        Apiato::call('Tranch@DeleteTranchAction', [$request]);

        return $this->noContent();
    }
}
