<?php

namespace App\Containers\Tranch\UI\API\Transformers;

use App\Containers\Tranch\Models\Tranch;
use App\Ship\Parents\Transformers\Transformer;

class TranchTransformer extends Transformer
{
    /**
     * @var  array
     */
    protected $defaultIncludes = [

    ];

    /**
     * @var  array
     */
    protected $availableIncludes = [

    ];

    /**
     * @param Tranch $entity
     *
     * @return array
     */
    public function transform(Tranch $entity)
    {
        $response = [
            'object' => 'Tranch',
            'id' => $entity->getHashedKey(),
            'amount' => $entity->amount,
            'description' => $entity->description,
            'tranch_date' => $entity->tranch_date,
            'created_at' => $entity->created_at,
            'updated_at' => $entity->updated_at,

        ];

        $response = $this->ifAdmin([
            'real_id'    => $entity->id,
            // 'deleted_at' => $entity->deleted_at,
        ], $response);

        return $response;
    }
}
