<?php

/**
 * @apiGroup           Tranch
 * @apiName            deleteTranch
 *
 * @api                {DELETE} /v1/tranches/:id Endpoint title here..
 * @apiDescription     Endpoint description here..
 *
 * @apiVersion         1.0.0
 * @apiPermission      none
 *
 * @apiParam           {String}  parameters here..
 *
 * @apiSuccessExample  {json}  Success-Response:
 * HTTP/1.1 200 OK
{
  // Insert the response of the request here...
}
 */

/** @var Route $router */
$router->delete('tranches/{id}', [
    'as' => 'api_tranch_delete_tranch',
    'uses'  => 'Controller@deleteTranch',
    'middleware' => [
      'auth:api',
    ],
]);
