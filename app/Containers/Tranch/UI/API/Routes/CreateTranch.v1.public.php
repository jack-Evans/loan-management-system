<?php

/**
 * @apiGroup           Tranch
 * @apiName            createTranch
 *
 * @api                {POST} /v1/tranches Endpoint title here..
 * @apiDescription     Endpoint description here..
 *
 * @apiVersion         1.0.0
 * @apiPermission      none
 *
 * @apiParam           {String}  parameters here..
 *
 * @apiSuccessExample  {json}  Success-Response:
 * HTTP/1.1 200 OK
{
  // Insert the response of the request here...
}
 */

/** @var Route $router */
$router->post('tranches', [
    'as' => 'api_tranch_create_tranch',
    'uses'  => 'Controller@createTranch',
    'middleware' => [
      'auth:api',
    ],
]);
