<?php

/**
 * @apiGroup           Tranch
 * @apiName            updateTranch
 *
 * @api                {PATCH} /v1/tranches/:id Endpoint title here..
 * @apiDescription     Endpoint description here..
 *
 * @apiVersion         1.0.0
 * @apiPermission      none
 *
 * @apiParam           {String}  parameters here..
 *
 * @apiSuccessExample  {json}  Success-Response:
 * HTTP/1.1 200 OK
{
  // Insert the response of the request here...
}
 */

/** @var Route $router */
$router->patch('tranches/{id}', [
    'as' => 'api_tranch_update_tranch',
    'uses'  => 'Controller@updateTranch',
    'middleware' => [
      'auth:api',
    ],
]);
