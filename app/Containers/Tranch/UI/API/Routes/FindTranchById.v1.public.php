<?php

/**
 * @apiGroup           Tranch
 * @apiName            findTranchById
 *
 * @api                {GET} /v1/tranches/:id Endpoint title here..
 * @apiDescription     Endpoint description here..
 *
 * @apiVersion         1.0.0
 * @apiPermission      none
 *
 * @apiParam           {String}  parameters here..
 *
 * @apiSuccessExample  {json}  Success-Response:
 * HTTP/1.1 200 OK
{
  // Insert the response of the request here...
}
 */

/** @var Route $router */
$router->get('tranches/{id}', [
    'as' => 'api_tranch_find_tranch_by_id',
    'uses'  => 'Controller@findTranchById',
    'middleware' => [
      'auth:api',
    ],
]);
