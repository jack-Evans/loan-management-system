<?php

/**
 * @apiGroup           Tranch
 * @apiName            getAllTranches
 *
 * @api                {GET} /v1/tranches Endpoint title here..
 * @apiDescription     Endpoint description here..
 *
 * @apiVersion         1.0.0
 * @apiPermission      none
 *
 * @apiParam           {String}  parameters here..
 *
 * @apiSuccessExample  {json}  Success-Response:
 * HTTP/1.1 200 OK
{
  // Insert the response of the request here...
}
 */

/** @var Route $router */
$router->get('tranches', [
    'as' => 'api_tranch_get_all_tranches',
    'uses'  => 'Controller@getAllTranches',
    'middleware' => [
      'auth:api',
    ],
]);
