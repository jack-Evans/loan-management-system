<?php

namespace App\Containers\Tranch\Tasks;

use App\Containers\Tranch\Data\Repositories\TranchRepository;
use App\Ship\Parents\Tasks\Task;

class GetAllTranchesTask extends Task
{

    protected $repository;

    public function __construct(TranchRepository $repository)
    {
        $this->repository = $repository;
    }

    public function run()
    {
        return $this->repository->paginate();
    }
}
