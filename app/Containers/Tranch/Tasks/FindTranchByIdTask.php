<?php

namespace App\Containers\Tranch\Tasks;

use App\Containers\Tranch\Data\Repositories\TranchRepository;
use App\Ship\Exceptions\NotFoundException;
use App\Ship\Parents\Tasks\Task;
use Exception;

class FindTranchByIdTask extends Task
{

    protected $repository;

    public function __construct(TranchRepository $repository)
    {
        $this->repository = $repository;
    }

    public function run($id)
    {
        try {
            return $this->repository->find($id);
        }
        catch (Exception $exception) {
            throw new NotFoundException();
        }
    }
}
