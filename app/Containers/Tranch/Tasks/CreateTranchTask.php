<?php

namespace App\Containers\Tranch\Tasks;

use App\Containers\Tranch\Data\Repositories\TranchRepository;
use App\Ship\Exceptions\CreateResourceFailedException;
use App\Ship\Parents\Tasks\Task;
use Exception;

class CreateTranchTask extends Task
{

    protected $repository;

    public function __construct(TranchRepository $repository)
    {
        $this->repository = $repository;
    }

    public function run(array $data)
    {
        try {
            return $this->repository->create($data);
        }
        catch (Exception $exception) {
            throw new CreateResourceFailedException();
        }
    }
}
