<?php

namespace App\Containers\Tranch\Tasks;

use App\Containers\Tranch\Data\Repositories\TranchRepository;
use App\Ship\Exceptions\UpdateResourceFailedException;
use App\Ship\Parents\Tasks\Task;
use Exception;

class UpdateTranchTask extends Task
{

    protected $repository;

    public function __construct(TranchRepository $repository)
    {
        $this->repository = $repository;
    }

    public function run($id, array $data)
    {
        try {
            return $this->repository->update($data, $id);
        }
        catch (Exception $exception) {
            throw new UpdateResourceFailedException();
        }
    }
}
