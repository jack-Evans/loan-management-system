<?php

namespace App\Containers\Tranch\Tasks;

use App\Containers\Tranch\Data\Repositories\TranchRepository;
use App\Ship\Exceptions\DeleteResourceFailedException;
use App\Ship\Parents\Tasks\Task;
use Exception;

class DeleteTranchTask extends Task
{

    protected $repository;

    public function __construct(TranchRepository $repository)
    {
        $this->repository = $repository;
    }

    public function run($id)
    {
        try {
            return $this->repository->delete($id);
        }
        catch (Exception $exception) {
            throw new DeleteResourceFailedException();
        }
    }
}
