<?php

namespace App\Containers\Customer\Models;

use App\Ship\Parents\Models\Model;

class Customer extends Model
{
    protected $fillable = [
        'email',
        'first_name',
        'last_name',
        'username',
        'phone',
    ];

    protected $attributes = [

    ];

    protected $hidden = [

    ];

    protected $casts = [

    ];

    protected $dates = [
        'created_at',
        'updated_at',
    ];

    /**
     * A resource key to be used by the the JSON API Serializer responses.
     */
    protected $resourceKey = 'customers';


    public function account() {
        return $this->hasOne(\App\Containers\Payment\Models\Account::class);
    }
}
