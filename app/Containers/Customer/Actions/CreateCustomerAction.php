<?php

namespace App\Containers\Customer\Actions;

use App\Ship\Parents\Actions\Action;
use App\Ship\Parents\Requests\Request;
use App\Containers\Customer\Data\Transporters\CreateCustomerTransporter;
use Apiato\Core\Foundation\Facades\Apiato;

class CreateCustomerAction extends Action
{
    public function run(Request $request)
    {
        $data = [
            'email' 		=> $request->email,
            'first_name' 	=> $request->firstname,
            'last_name' 	=> $request->lastname,
            'username' 		=> $request->username,
            'phone' 		=> $request->phone,
        ];

        \DB::beginTransaction();

        $customer = Apiato::call('Customer@CreateCustomerTask', [(new CreateCustomerTransporter($data))->toArray()]);
        
        // TODO - Create a transporter
        // Create a Customers Account with 0 debit, credit and balance.
        // Account type is payable
        $account  = Apiato::call('Payment@CreateAccountTask', [[
            'customer_id'   => $customer->id,
            'credit'        => 0,
            'debit'         => 0,
            'balance'       => 0,
            'status'        => 'payable',
        ]]);

        \DB::commit();

        return $customer;
    }
}
