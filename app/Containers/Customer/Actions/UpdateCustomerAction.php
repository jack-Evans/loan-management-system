<?php

namespace App\Containers\Customer\Actions;

use App\Ship\Parents\Actions\Action;
use App\Ship\Parents\Requests\Request;
use Apiato\Core\Foundation\Facades\Apiato;
use App\Containers\Customer\Data\Transporters\UpdateCustomerTransporter;

class UpdateCustomerAction extends Action
{
    public function run(Request $request)
    {
        $data = [
            'email' 		=> $request->email,
            'first_name' 	=> $request->firstname,
            'last_name' 	=> $request->lastname,
            'username' 		=> $request->username,
            'phone' 		=> $request->phone,
        ];

        $customer = Apiato::call('Customer@UpdateCustomerTask', [$request->id, $data]);

        return $customer;
    }
}
