<?php

namespace App\Containers\Customer\Data\Seeders;

use App\Ship\Parents\Seeders\Seeder;
use Apiato\Core\Foundation\Facades\Apiato;

class CustomerSeeder extends Seeder
{
    public function run()
    {
        $faker = \Faker\Factory::create();

    	for($i = 0; $i<100; $i++) {
            $request = new \App\Containers\Customer\UI\API\Requests\CreateCustomerRequest();
            $request->email         = $faker->email;
            $request->firstname     = $faker->name;
            $request->lastname      = $faker->name;
            $request->username      = $faker->userName;
            $request->phone         = $faker->phoneNumber;
            $customer = Apiato::call('Customer@CreateCustomerAction', [$request]);
    		/*\App\Containers\Customer\Models\Customer::create([
    			'username'		=> $faker->userName,
	            'first_name' 	=> $faker->name,
	            'last_name' 	=> $faker->name,
	            'email' 		=> $faker->email,
	            'phone'			=> $faker->phoneNumber,
    		]);*/
    	}
    }
}
