<?php

namespace App\Containers\Customer\Data\Transporters;

use App\Ship\Parents\Transporters\Transporter;

class CreateCustomerTransporter extends Transporter
{

    /**
     * @var array
     */
    protected $schema = [
        'type' => 'object',
        'properties' => [
            'username'  => ['type' => 'string'],
            'first_name' => ['type' => 'string'],
            'last_name'  => ['type' => 'string'],
            'phone'     => ['type' => 'string'],
            'email'     => ['type' => 'string'],
        ],
        'required'   => [
            'username',
            'first_name',
            'last_name',
            'email',
        ],
        'default'    => [
            // provide default values for specific properties here
        ]
    ];
}
