<?php

namespace App\Containers\Customer\UI\API\Transformers;

use App\Containers\Customer\Models\Customer;
use App\Ship\Parents\Transformers\Transformer;

class CustomerTransformer extends Transformer
{
    /**
     * @var  array
     */
    protected $defaultIncludes = [

    ];

    /**
     * @var  array
     */
    protected $availableIncludes = [

    ];

    /**
     * @param Customer $entity
     *
     * @return array
     */
    public function transform(Customer $entity)
    {
        $response = [
            'object'        => 'Customer',
            'id'            => $entity->getHashedKey(),
            'username'      => $entity->username,
            'firstname'     => $entity->first_name,
            'lastname'      => $entity->last_name,
            'email'         => $entity->email,
            'account'       => [
                'id'        => $entity->account->getHashedKey(),
                'credit'    => $entity->account->credit,
                'debit'     => $entity->account->debit,
                'balance'   => $entity->account->balance,
                'created_at' => $entity->account->created_at,
                'updated_at' => $entity->account->updated_at,
            ],
            'phone'         => $entity->phone,
            'created_at'    => $entity->created_at,
            'updated_at'    => $entity->updated_at,

        ];

        $response = $this->ifAdmin([
            'real_id'    => $entity->id,
            // 'deleted_at' => $entity->deleted_at,
        ], $response);

        return $response;
    }
}
