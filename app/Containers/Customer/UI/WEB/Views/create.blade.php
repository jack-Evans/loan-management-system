@extends('layouts.app')

@section('title', 'Customer')

@section('content')
<div class="col-lg-12">
          <div class="has-bg">
              <h3>{{isset($customer->id) ? 'Edit ' : 'Create '}}Customer</h3>
            <div class="col-md-5 col-md-offset-2">
              <form action="{{url(isset($customer->id) ? 'customers/'.$customer->id.'/update': 'customers/store' )}}" method="post">
                  @csrf()
                  <div class="form-group">
                    <label for="firstname">First Name:</label>
                    <input type="text" class="form-control" name="firstname"
                          value="{{isset($customer->firstname) ? $customer->firstname : old('firstname')}}">
                  </div>
                  <div class="form-group">
                    <label for="lastname">Last Name:</label>
                    <input type="text" class="form-control" name="lastname"
                          value="{{isset($customer->lastname) ? $customer->lastname : old('lastname')}}">
                  </div>
                  <div class="form-group">
                    <label for="username">Username:</label>
                    <input type="text" class="form-control" name="username"
                          value="{{isset($customer->username) ? $customer->username : old('username')}}">
                  </div>
                  <div class="form-group">
                    <label for="email">Email address:</label>
                    <input type="email" class="form-control" name="email"
                          value="{{isset($customer->email) ? $customer->email : old('email')}}">
                  </div>
                  <div class="form-group">
                    <label for="phone">Phone Number:</label>
                    <input type="text" class="form-control" name="phone"
                          value="{{isset($customer->phone) ? $customer->phone : old('phone')}}">
                  </div>
                  <button type="submit" class="btn custom-padding btn-primary">Submit</button>
              </form>
          </div>
      </div>
</div>
@endsection
