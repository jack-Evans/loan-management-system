<!DOCTYPE html>
<html>
<head>
    <title>Login</title>    
    <meta charset="utf-8"> 
    <meta name="viewport" content="width=device-width, initial-scale=1">
</head>
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/css/bootstrap.min.css">
<body>
<div class="container">
    <div class="content">
        <div class="row">
            <div class="col-md-5 col-md-offset-2">
                <form action="{{url('doLogin' )}}" method="post">
                    @csrf()
                    
                    <div class="form-group">
                      <label for="token">API login token:</label>
                      <textarea class="form-control" name="token"></textarea>
                    </div>
                    
                    <button type="submit" class="btn btn-primary">Submit</button>
                </form>
            </div>
        </div>
    </div>
</div>
</body>
</html>