@extends('layouts.app')

@section('title', 'Customers')

@section('content')
<div class="col-lg-12">
    <div class="has-bg">
        <h3>
            Customers
            <a href="{{url('customers/create')}}" class="pull-right">
                <button class="btn btn-success">Create customer</button>
            </a>
        </h3>
        <span class="space"></span>
        <table class="table table-striped table-hover table-bordered text-left MDBootstrapDatatable" cellspacing="0" width="100%">
            <thead>
                <tr>
                    <th>Customer Name</th>
                    <th>Customer Email</th>
                    <th>Actions</th>
                </tr>
            </thead>
            <tbody>
                @foreach($customers as $key => $customer)
                <tr>
                    <td>{{$customer->firstname." ".$customer->lastname}}</td>
                    <td>{{$customer->email}}</td>
                    <td>
                        <a href="{{url('customers/'.$customer->id.'/edit')}}">
                        <button class="btn btn-primary">Edit</button>
                        </a>
                        <a href="{{ url('customers/delete/'.$customer->id) }}">
                            <button class="btn btn-danger">Delete</button>
                        </a>
                    </td>
                </tr>
                @endforeach
            </tbody>
        </table>
    </div>
</div>
@endsection
