<?php

/** @var Route $router */
$router->get('customers/delete/{id}', [
    'as' => 'web_customer_delete',
    'uses'  => 'Controller@delete',
    'middleware' => [
      'auth:web',
    ],
]);
