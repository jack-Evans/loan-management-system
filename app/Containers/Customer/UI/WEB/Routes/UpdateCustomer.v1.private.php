<?php

/** @var Route $router */
$router->post('customers/{id}/update', [
    'as' => 'web_customer_update',
    'uses'  => 'Controller@update',
    'middleware' => [
      'auth:web',
    ],
]);
