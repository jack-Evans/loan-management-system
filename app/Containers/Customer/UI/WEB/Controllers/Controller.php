<?php

namespace App\Containers\Customer\UI\WEB\Controllers;

use App\Containers\Customer\UI\WEB\Requests\CreateCustomerRequest;
use App\Containers\Customer\UI\WEB\Requests\DeleteCustomerRequest;
use App\Containers\Customer\UI\WEB\Requests\GetAllCustomersRequest;
use App\Containers\Customer\UI\WEB\Requests\FindCustomerByIdRequest;
use App\Containers\Customer\UI\WEB\Requests\UpdateCustomerRequest;
use App\Containers\Customer\UI\WEB\Requests\StoreCustomerRequest;
use App\Containers\Customer\UI\WEB\Requests\EditCustomerRequest;
use App\Ship\Parents\Controllers\WebController;
use Apiato\Core\Foundation\Facades\Apiato;

/**
 * Class Controller
 *
 * @package App\Containers\Customer\UI\WEB\Controllers
 */
class Controller extends WebController
{
   
    /**
     * Show all entities
     *
     * @param GetAllCustomersRequest $request
     */
    public function index(GetAllCustomersRequest $request)
    {
        $url = config('token-container.WEB_API_URL').'customers';
        
        try{
            $response = get($url, ['limit' => 0]);
            $customers = $response->data;
        } catch (\Exception $e) {
            return exception($e);
        }
        
        return view('customer::customers', compact('customers'));
    }

    /**
     * Show one entity
     *
     * @param FindCustomerByIdRequest $request
     */
    public function show(FindCustomerByIdRequest $request)
    {
        $customer = Apiato::call('Customer@FindCustomerByIdAction', [$request]);

        // ..
    }

    /**
     * Create entity (show UI)
     *
     * @param CreateCustomerRequest $request
     */
    public function create(CreateCustomerRequest $request)
    {
        return view('customer::create');
    }

    /**
     * Add a new entity
     *
     * @param StoreCustomerRequest $request
     */
    public function store(StoreCustomerRequest $request)
    {
        $url = config('token-container.WEB_API_URL').'customers';
        $data = [
            'firstname' => $request->firstname,
            'lastname' => $request->lastname,
            'username' => $request->username,
            'email' => $request->email,
            'phone' => $request->phone,
        ];
        
        try{
            $response = post($url, $data);               
        } catch (\Exception $e) {
            return exception($e);
        }
        
        return redirect('customers');
    }

    /**
     * Edit entity (show UI)
     *
     * @param EditCustomerRequest $request
     */
    public function edit(EditCustomerRequest $request)
    {
        $url = config('token-container.WEB_API_URL').'customers/'.$request->id;
        $response = get($url);    
                
        $customer = isset($response->data) ? $response->data : array();
        
        return view('customer::create', compact('customer'));

    }

    /**
     * Update a given entity
     *
     * @param UpdateCustomerRequest $request
     */
    public function update(UpdateCustomerRequest $request)
    {        
        $url = config('token-container.WEB_API_URL').'customers/'.$request->id;
        $data = [
            'firstname' => $request->firstname,
            'lastname' => $request->lastname,
            'username' => $request->username,
            'email' => $request->email,
            'phone' => $request->phone,
        ];
        
        try{
            $response = patch($url, $data);
        } catch (\Exception $e) {
            return exception($e);
        }
        return redirect('customers/'.$request->id.'/edit');
    }

    /**
     * Delete a given entity
     *
     * @param DeleteCustomerRequest $request
     */
    public function delete(DeleteCustomerRequest $request)
    {
        $url = config('token-container.WEB_API_URL').'customers/'.$request->id;
    
        try{
            $response = delete($url);
        } catch (\Exception $e) {
            return exception($e);
        }
         return redirect('customers');
    }
}
