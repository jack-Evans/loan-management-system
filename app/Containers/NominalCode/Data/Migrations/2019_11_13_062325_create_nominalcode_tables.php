<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateNominalcodeTables extends Migration
{

    /**
     * Run the migrations.
     */
    public function up()
    {
        Schema::create('nominal_codes', function (Blueprint $table) {

            $table->increments('id');
            $table->string('code');
            $table->string('name');
            $table->timestamps();
            $table->softDeletes();

        });
    }

    /**
     * Reverse the migrations.
     */
    public function down()
    {
        Schema::drop('nominal_codes');
    }
}
