<?php

namespace App\Containers\NominalCode\Data\Repositories;

use App\Ship\Parents\Repositories\Repository;

/**
 * Class NominalCodeRepository
 */
class NominalCodeRepository extends Repository
{

    /**
     * @var array
     */
    protected $fieldSearchable = [
        'id' => '=',
        // ...
    ];

}
