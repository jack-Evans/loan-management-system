<?php

/** @var Route $router */
$router->delete('nominalcodes/{id}', [
    'as' => 'web_nominalcode_delete',
    'uses'  => 'Controller@delete',
    'middleware' => [
      'auth:web',
    ],
]);
