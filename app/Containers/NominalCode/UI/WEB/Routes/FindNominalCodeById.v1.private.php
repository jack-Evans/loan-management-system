<?php

/** @var Route $router */
$router->get('nominalcodes/{id}', [
    'as' => 'web_nominalcode_show',
    'uses'  => 'Controller@show',
    'middleware' => [
      'auth:web',
    ],
]);
