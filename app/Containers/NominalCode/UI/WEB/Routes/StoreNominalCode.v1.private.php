<?php

/** @var Route $router */
$router->post('nominalcodes/store', [
    'as' => 'web_nominalcode_store',
    'uses'  => 'Controller@store',
    'middleware' => [
      'auth:web',
    ],
]);
