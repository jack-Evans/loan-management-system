<?php

/** @var Route $router */
$router->get('nominalcodes/{id}/edit', [
    'as' => 'web_nominalcode_edit',
    'uses'  => 'Controller@edit',
    'middleware' => [
      'auth:web',
    ],
]);
