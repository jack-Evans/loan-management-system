<?php

/** @var Route $router */
$router->post('nominalcodes/{id}/update', [
    'as' => 'web_nominalcode_update',
    'uses'  => 'Controller@update',
    'middleware' => [
      'auth:web',
    ],
]);
