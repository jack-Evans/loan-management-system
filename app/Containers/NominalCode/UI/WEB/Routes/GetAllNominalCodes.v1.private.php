<?php

/** @var Route $router */
$router->get('nominalcodes', [
    'as' => 'web_nominalcode_index',
    'uses'  => 'Controller@index',
    'middleware' => [
      'auth:web',
    ],
]);
