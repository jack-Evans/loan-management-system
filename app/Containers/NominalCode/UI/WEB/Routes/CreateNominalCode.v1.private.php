<?php

/** @var Route $router */
$router->get('nominalcodes/create', [
    'as' => 'web_nominalcode_create',
    'uses'  => 'Controller@create',
    'middleware' => [
      'auth:web',
    ],
]);
