<?php

namespace App\Containers\NominalCode\UI\WEB\Controllers;

use App\Containers\NominalCode\UI\WEB\Requests\CreateNominalCodeRequest;
use App\Containers\NominalCode\UI\WEB\Requests\DeleteNominalCodeRequest;
use App\Containers\NominalCode\UI\WEB\Requests\GetAllNominalCodesRequest;
use App\Containers\NominalCode\UI\WEB\Requests\FindNominalCodeByIdRequest;
use App\Containers\NominalCode\UI\WEB\Requests\UpdateNominalCodeRequest;
use App\Containers\NominalCode\UI\WEB\Requests\StoreNominalCodeRequest;
use App\Containers\NominalCode\UI\WEB\Requests\EditNominalCodeRequest;
use App\Ship\Parents\Controllers\WebController;
use Apiato\Core\Foundation\Facades\Apiato;

/**
 * Class Controller
 *
 * @package App\Containers\NominalCode\UI\WEB\Controllers
 */
class Controller extends WebController
{
    /**
     * The assigned API PATH for this Controller
     *
     * @var string
     */
    
    protected $apiPath;
    
    public function __construct(){
        $this->apiPath = config('token-container.WEB_API_URL');
    }

    /**
     * Show all entities
     *
     * @param GetAllNominalCodesRequest $request
     */
    public function index(GetAllNominalCodesRequest $request)
    {
        $nominalCodeUrl = $this->apiPath.'nominalcodes';
        try {
            $response = get($nominalCodeUrl, ['limit' => 0]);
            $nominalCodes =isset($response->data) ? $response->data : array();
        } catch (\Exception $e) {
            return exception($e);
        }

        return view('nominalcode::nominal-code', compact('nominalCodes'));
    }

    /**
     * Show one entity
     *
     * @param FindNominalCodeByIdRequest $request
     */
    public function show(FindNominalCodeByIdRequest $request)
    {
        $nominalcode = Apiato::call('NominalCode@FindNominalCodeByIdAction', [$request]);

        // ..
    }

    /**
     * Create entity (show UI)
     *
     * @param CreateNominalCodeRequest $request
     */
    public function create(CreateNominalCodeRequest $request)
    {
        return view('nominalcode::create-update-nominal-code');
    }

    /**
     * Add a new entity
     *
     * @param StoreNominalCodeRequest $request
     */
    public function store(StoreNominalCodeRequest $request)
    {
        $nominalCodeUrl = $this->apiPath.'nominalcodes';

        $requestData = [
            'code' => $request->code,
            'name' => $request->name,
        ];
        try {
            $response = post($nominalCodeUrl, $requestData);
            $nominalCodes = $response->data;
        } catch (\Exception $e) {
            return exception($e);
        }

        return redirect('nominalcodes');
    }

    /**
     * Edit entity (show UI)
     *
     * @param EditNominalCodeRequest $request
     */
    public function edit(EditNominalCodeRequest $request)
    {
        $nominalCodeUrl = $this->apiPath.'nominalcodes/'.$request->id;
        try {
            $response = get($nominalCodeUrl);
            $nominalCode = $response->data;
        } catch (\Exception $e) {
            return exception($e);
        }

        return view('nominalcode::create-update-nominal-code', compact('nominalCode'));
    }

    /**
     * Update a given entity
     *
     * @param UpdateNominalCodeRequest $request
     */
    public function update(UpdateNominalCodeRequest $request)
    {
        $nominalCodeUrl = $this->apiPath.'nominalcodes/'.$request->id;

        $requestData = [
            'code' => $request->code,
            'name' => $request->name,
        ];
        try {
            $response = patch($nominalCodeUrl, $requestData);
            $nominalCodes = $response->data;
        } catch (\Exception $e) {
            return exception($e);
        }

        return redirect('nominalcodes');
    }

    /**
     * Delete a given entity
     *
     * @param DeleteNominalCodeRequest $request
     */
    public function delete(DeleteNominalCodeRequest $request)
    {
         $result = Apiato::call('NominalCode@DeleteNominalCodeAction', [$request]);

         // ..
    }
}
