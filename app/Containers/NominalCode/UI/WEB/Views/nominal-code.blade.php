@extends('layouts.app')

@section('title', 'Nominal Code')

@section('content')
<div class="col-lg-12">
    <div class="has-bg">
        <h3>
            Nominal Codes
            <a href="{{url('nominalcodes/create')}}" class="pull-right">
                <button class="btn btn-success">Create Nominal Code</button>
            </a>
        </h3>
        <span class="space"></span>
        <table class="table table-striped table-hover table-bordered text-left MDBootstrapDatatable" cellspacing="0" width="100%">
            <thead>
                <tr>
                    <th class="th-sm">Code</th>
                    <th>Name</th>
                    <th>Actions</th>
                </tr>
            </thead>
            <tbody>
                @foreach($nominalCodes as $key => $nominalCode)
                <tr>
                    <td>{{$nominalCode->code}}</td>
                    <td class="text-capitalize">{{$nominalCode->name}}</td>
                    <td>
                        <a href="{{url('nominalcodes/'.$nominalCode->id.'/edit')}}">
                            <button class="btn btn-primary">Edit</button>
                        </a>
                    </td>
                    <!-- <td> -->
                        <!-- <a href="{{ url('nominalcodes/'.$nominalCode->id).'/delete' }}">
                            <button class="btn btn-danger">Delete</button>
                        </a> -->
                    <!-- </td> -->
                </tr>
                @endforeach
            </tbody>
        </table>
    </div>
</div>
<!-- <button id="choose-files">Choose Files</button> -->
<!-- <button onClick="launchOneDrivePicker()">Open from OneDrive</button> -->
@endsection
@section('pages-js')
<!-- <script type="text/javascript" src="https://js.live.net/v7.2/OneDrive.js"></script> -->
<!-- <script src="https://static-cdn.kloudless.com/p/platform/sdk/kloudless.picker.js"></script> -->

<script type="text/javascript">
    // function launchOneDrivePicker(){
    //     var odOptions = {
    //       clientId: "37182860-f46f-4d2f-876d-6b261301609b",
    //       action: "download",
    //       multiSelect: false,
    //       oauth2AllowUrlPathMatching: true,
    //       advanced: {},
    //       success: function(files) { 
    //             console.log(files);
    //       },
    //       cancel: function() { /* cancel handler */ },
    //       error: function(error) { 
    //         console.log(error);
    //        }
    //     }
    // OneDrive.open(odOptions);
    // }

    // var appId = "rJsnTocn_UHisN5_l1bYX3lZki967HqW6sdY6q6F01N2iqvN";
    // var picker = window.Kloudless.filePicker.picker({ 
    //     app_id: appId,
    //     multiselect: false,
    //     // link: true,
    //     computer: true,
    //     services: ['all'],
    //     types: ['all'],
    // });
    //     picker.choose(document.getElementById('choose-files'));
    //     picker.on('success', function(files) {
    //     // files is an array of JS objects that contain file metadata.
    //         console.log('Successfully selected files: ', files);
    //     });

    //  var explorer = window.Kloudless.explorer({
    //   app_id: appId,
    //   copy_to_upload_location: 'sync',
    //   upload_location_account: '371159923', // the Google Drive account
    //   upload_location_folder: 'F6spW84S19pGcRsJU0KuOwjNjoKe2q6JfVVdQ4mLd5VuksiULJSe-z9B8jypNdpqt7GB3U7eF8l9KQiJm9mFI8cUwdHBYofVYDzhZbqFI_uaMhTYE0PcnWI8BvVzXFHW9/Site contents/Shared Documents/KloudlesUploadedFiles', // the "public_files" folder
    // });
</script>

@endsection