@extends('layouts.app')

@section('title', 'Nominal Code')

@section('content')
<div class="col-lg-12">
    <div class="has-bg">
      <div class="row">
          <div class="col-md-5 col-md-offset-2">
          <h3>{{isset($nominalCode->id) ? 'Edit ' : 'Create '}}Nominal Code</h3>
              <form action="{{url(isset($nominalCode->id) ? 'nominalcodes/'.$nominalCode->id.'/update': 'nominalcodes/store' )}}" method="post">
                  @csrf()
                  <div class="form-group">
                    <label for="code">Code:</label>
                    <input type="text" class="form-control" name="code"
                           value="{{isset($nominalCode->code) ? $nominalCode->code : old('code')}}">
                  </div>
                  <div class="form-group">
                    <label for="name">Name:</label>
                    <input type="text" class="form-control" name="name"
                           value="{{isset($nominalCode->name) ? $nominalCode->name : old('name')}}">
                  </div>
                  <button type="submit" class="btn btn-primary">Submit</button>
              </form>
          </div>
      </div>
    </div>
</div>
@endsection
