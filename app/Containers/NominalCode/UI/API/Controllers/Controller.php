<?php

namespace App\Containers\NominalCode\UI\API\Controllers;

use App\Containers\NominalCode\UI\API\Requests\CreateNominalCodeRequest;
use App\Containers\NominalCode\UI\API\Requests\DeleteNominalCodeRequest;
use App\Containers\NominalCode\UI\API\Requests\GetAllNominalCodesRequest;
use App\Containers\NominalCode\UI\API\Requests\FindNominalCodeByIdRequest;
use App\Containers\NominalCode\UI\API\Requests\UpdateNominalCodeRequest;
use App\Containers\NominalCode\UI\API\Transformers\NominalCodeTransformer;
use App\Ship\Parents\Controllers\ApiController;
use Apiato\Core\Foundation\Facades\Apiato;

/**
 * Class Controller
 *
 * @package App\Containers\NominalCode\UI\API\Controllers
 */
class Controller extends ApiController
{
    /**
     * @param CreateNominalCodeRequest $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function createNominalCode(CreateNominalCodeRequest $request)
    {
        $nominalcode = Apiato::call('NominalCode@CreateNominalCodeAction', [$request]);

        return $this->created($this->transform($nominalcode, NominalCodeTransformer::class));
    }

    /**
     * @param FindNominalCodeByIdRequest $request
     * @return array
     */
    public function findNominalCodeById(FindNominalCodeByIdRequest $request)
    {
        $nominalcode = Apiato::call('NominalCode@FindNominalCodeByIdAction', [$request]);

        return $this->transform($nominalcode, NominalCodeTransformer::class);
    }

    /**
     * @param GetAllNominalCodesRequest $request
     * @return array
     */
    public function getAllNominalCodes(GetAllNominalCodesRequest $request)
    {
        $nominalcodes = Apiato::call('NominalCode@GetAllNominalCodesAction', [$request]);

        return $this->transform($nominalcodes, NominalCodeTransformer::class);
    }

    /**
     * @param UpdateNominalCodeRequest $request
     * @return array
     */
    public function updateNominalCode(UpdateNominalCodeRequest $request)
    {
        $nominalcode = Apiato::call('NominalCode@UpdateNominalCodeAction', [$request]);

        return $this->transform($nominalcode, NominalCodeTransformer::class);
    }

    /**
     * @param DeleteNominalCodeRequest $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function deleteNominalCode(DeleteNominalCodeRequest $request)
    {
        Apiato::call('NominalCode@DeleteNominalCodeAction', [$request]);

        return $this->noContent();
    }
}
