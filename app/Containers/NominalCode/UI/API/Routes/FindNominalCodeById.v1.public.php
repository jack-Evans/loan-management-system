<?php

/**
 * @apiGroup           NominalCode
 * @apiName            findNominalCodeById
 *
 * @api                {GET} /vnominalcodes/nominalcodes/:id Endpoint title here..
 * @apiDescription     Endpoint description here..
 *
 * @apiVersion         nominalcodes.0.0
 * @apiPermission      none
 *
 * @apiParam           {String}  parameters here..
 *
 * @apiSuccessExample  {json}  Success-Response:
 * HTTP/1.1 200 OK
{
  // Insert the response of the request here...
}
 */

/** @var Route $router */
$router->get('nominalcodes/{id}', [
    'as' => 'api_nominalcode_find_nominal_code_by_id',
    'uses'  => 'Controller@findNominalCodeById',
    'middleware' => [
      'auth:api',
    ],
]);
