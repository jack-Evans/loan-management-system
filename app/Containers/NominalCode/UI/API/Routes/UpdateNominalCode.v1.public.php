<?php

/**
 * @apiGroup           NominalCode
 * @apiName            updateNominalCode
 *
 * @api                {PATCH} /vnominalcodes/nominalcodes/:id Endpoint title here..
 * @apiDescription     Endpoint description here..
 *
 * @apiVersion         nominalcodes.0.0
 * @apiPermission      none
 *
 * @apiParam           {String}  parameters here..
 *
 * @apiSuccessExample  {json}  Success-Response:
 * HTTP/1.1 200 OK
{
  // Insert the response of the request here...
}
 */

/** @var Route $router */
$router->patch('nominalcodes/{id}', [
    'as' => 'api_nominalcode_update_nominal_code',
    'uses'  => 'Controller@updateNominalCode',
    'middleware' => [
      'auth:api',
    ],
]);
