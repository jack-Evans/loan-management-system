<?php

/**
 * @apiGroup           NominalCode
 * @apiName            getAllNominalCodes
 *
 * @api                {GET} /vnominalcodes/nominalcodes Endpoint title here..
 * @apiDescription     Endpoint description here..
 *
 * @apiVersion         nominalcodes.0.0
 * @apiPermission      none
 *
 * @apiParam           {String}  parameters here..
 *
 * @apiSuccessExample  {json}  Success-Response:
 * HTTP/1.1 200 OK
{
  // Insert the response of the request here...
}
 */

/** @var Route $router */
$router->get('nominalcodes', [
    'as' => 'api_nominalcode_get_all_nominal_codes',
    'uses'  => 'Controller@getAllNominalCodes',
    'middleware' => [
      'auth:api',
    ],
]);
