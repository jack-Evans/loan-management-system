<?php

/**
 * @apiGroup           NominalCode
 * @apiName            createNominalCode
 *
 * @api                {POST} /vnominalcodes/nominalcodes Endpoint title here..
 * @apiDescription     Endpoint description here..
 *
 * @apiVersion         nominalcodes.0.0
 * @apiPermission      none
 *
 * @apiParam           {String}  parameters here..
 *
 * @apiSuccessExample  {json}  Success-Response:
 * HTTP/1.1 200 OK
{
  // Insert the response of the request here...
}
 */

/** @var Route $router */
$router->post('nominalcodes', [
    'as' => 'api_nominalcode_create_nominal_code',
    'uses'  => 'Controller@createNominalCode',
    'middleware' => [
      'auth:api',
    ],
]);
