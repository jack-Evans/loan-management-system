<?php

/**
 * @apiGroup           NominalCode
 * @apiName            deleteNominalCode
 *
 * @api                {DELETE} /vnominalcodes/nominalcodes/:id Endpoint title here..
 * @apiDescription     Endpoint description here..
 *
 * @apiVersion         nominalcodes.0.0
 * @apiPermission      none
 *
 * @apiParam           {String}  parameters here..
 *
 * @apiSuccessExample  {json}  Success-Response:
 * HTTP/1.1 200 OK
{
  // Insert the response of the request here...
}
 */

/** @var Route $router */
$router->delete('nominalcodes/{id}', [
    'as' => 'api_nominalcode_delete_nominal_code',
    'uses'  => 'Controller@deleteNominalCode',
    'middleware' => [
      'auth:api',
    ],
]);
