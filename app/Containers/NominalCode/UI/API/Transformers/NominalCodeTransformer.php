<?php

namespace App\Containers\NominalCode\UI\API\Transformers;

use App\Containers\NominalCode\Models\NominalCode;
use App\Ship\Parents\Transformers\Transformer;

class NominalCodeTransformer extends Transformer
{
    /**
     * @var  array
     */
    protected $defaultIncludes = [

    ];

    /**
     * @var  array
     */
    protected $availableIncludes = [

    ];

    /**
     * @param NominalCode $entity
     *
     * @return array
     */
    public function transform(NominalCode $entity)
    {
        $response = [
            'object' => 'NominalCode',
            // 'id' => $entity->getHashedKey(),
            'id' => $entity->id,
            'code' => $entity->code,
            'name' => $entity->name,
            'created_at' => $entity->created_at,
            'updated_at' => $entity->updated_at,

        ];

        $response = $this->ifAdmin([
            'real_id'    => $entity->id,
            // 'deleted_at' => $entity->deleted_at,
        ], $response);

        return $response;
    }
}
