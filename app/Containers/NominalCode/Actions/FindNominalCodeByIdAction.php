<?php

namespace App\Containers\NominalCode\Actions;

use App\Ship\Parents\Actions\Action;
use App\Ship\Parents\Requests\Request;
use Apiato\Core\Foundation\Facades\Apiato;

class FindNominalCodeByIdAction extends Action
{
    public function run(Request $request)
    {
        $nominalcode = Apiato::call('NominalCode@FindNominalCodeByIdTask', [$request->id]);

        return $nominalcode;
    }
}
