<?php

namespace App\Containers\NominalCode\Actions;

use App\Ship\Parents\Actions\Action;
use App\Ship\Parents\Requests\Request;
use Apiato\Core\Foundation\Facades\Apiato;

class GetAllNominalCodesAction extends Action
{
    public function run(Request $request)
    {
        $nominalcodes = Apiato::call('NominalCode@GetAllNominalCodesTask', [], ['addRequestCriteria']);

        return $nominalcodes;
    }
}
