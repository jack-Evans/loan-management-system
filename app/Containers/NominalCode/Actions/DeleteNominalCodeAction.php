<?php

namespace App\Containers\NominalCode\Actions;

use App\Ship\Parents\Actions\Action;
use App\Ship\Parents\Requests\Request;
use Apiato\Core\Foundation\Facades\Apiato;

class DeleteNominalCodeAction extends Action
{
    public function run(Request $request)
    {
        return Apiato::call('NominalCode@DeleteNominalCodeTask', [$request->id]);
    }
}
