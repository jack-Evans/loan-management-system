<?php

namespace App\Containers\NominalCode\Actions;

use App\Ship\Parents\Actions\Action;
use App\Ship\Parents\Requests\Request;
use Apiato\Core\Foundation\Facades\Apiato;

class UpdateNominalCodeAction extends Action
{
    public function run(Request $request)
    {
        $data = [
        	'code' => $request->code,
        	'name' => $request->name,
        ];

        $nominalcode = Apiato::call('NominalCode@UpdateNominalCodeTask', [$request->id, $data]);

        return $nominalcode;
    }
}
