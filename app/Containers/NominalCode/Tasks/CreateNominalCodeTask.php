<?php

namespace App\Containers\NominalCode\Tasks;

use App\Containers\NominalCode\Data\Repositories\NominalCodeRepository;
use App\Ship\Exceptions\CreateResourceFailedException;
use App\Ship\Parents\Tasks\Task;
use Exception;

class CreateNominalCodeTask extends Task
{

    protected $repository;

    public function __construct(NominalCodeRepository $repository)
    {
        $this->repository = $repository;
    }

    public function run(array $data)
    {
        try {
            return $this->repository->create($data);
        }
        catch (Exception $exception) {
            throw new CreateResourceFailedException();
        }
    }
}
