<?php

namespace App\Containers\NominalCode\Tasks;

use App\Containers\NominalCode\Data\Repositories\NominalCodeRepository;
use App\Ship\Exceptions\NotFoundException;
use App\Ship\Parents\Tasks\Task;
use Exception;

class FindNominalCodeByIdTask extends Task
{

    protected $repository;

    public function __construct(NominalCodeRepository $repository)
    {
        $this->repository = $repository;
    }

    public function run($id)
    {
        try {
            return $this->repository->find($id);
        }
        catch (Exception $exception) {
            throw new NotFoundException();
        }
    }
}
