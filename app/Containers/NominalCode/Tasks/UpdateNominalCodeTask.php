<?php

namespace App\Containers\NominalCode\Tasks;

use App\Containers\NominalCode\Data\Repositories\NominalCodeRepository;
use App\Ship\Exceptions\UpdateResourceFailedException;
use App\Ship\Parents\Tasks\Task;
use Exception;

class UpdateNominalCodeTask extends Task
{

    protected $repository;

    public function __construct(NominalCodeRepository $repository)
    {
        $this->repository = $repository;
    }

    public function run($id, array $data)
    {
        try {
            return $this->repository->update($data, $id);
        }
        catch (Exception $exception) {
            throw new UpdateResourceFailedException();
        }
    }
}
