<?php

namespace App\Containers\NominalCode\Tasks;

use App\Containers\NominalCode\Data\Repositories\NominalCodeRepository;
use App\Ship\Parents\Tasks\Task;

class GetAllNominalCodesTask extends Task
{

    protected $repository;

    public function __construct(NominalCodeRepository $repository)
    {
        $this->repository = $repository;
    }

    public function run()
    {
        return $this->repository->paginate();
    }
}
