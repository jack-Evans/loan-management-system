<?php

namespace App\Containers\NominalCode\Models;

use App\Ship\Parents\Models\Model;

class NominalCode extends Model
{
    protected $fillable = [
        'code',
        'name',
    ];

    protected $attributes = [

    ];

    protected $hidden = [

    ];

    protected $casts = [

    ];

    protected $dates = [
        'created_at',
        'updated_at',
    ];

    /**
     * A resource key to be used by the the JSON API Serializer responses.
     */
    protected $resourceKey = 'nominalcodes';

    public function journal() {
        return $this->hasMany(\App\Containers\Payment\Models\Journal::class, 'nominal_code_id')->with(['posts']);
    }
}
