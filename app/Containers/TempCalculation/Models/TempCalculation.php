<?php

namespace App\Containers\TempCalculation\Models;

use App\Ship\Parents\Models\Model;
use App\Containers\Payment\Models\PaymentPlan;

class TempCalculation extends Model
{
    protected $fillable = [
        'loan_id',
        'amount',
        'type',
        'balance',
        'rate',
        'default_rate',
        'duration',
        'descriptions',
        'tnx_log',
        'tnx_date',
    ];

    protected $attributes = [

    ];

    protected $hidden = [

    ];

    protected $casts = [

    ];

    protected $dates = [
        'created_at',
        'updated_at',
        'tnx_date',
    ];

    protected $appends = [
        'currentBalance',
        'dueDate'
    ];

    /**
     * A resource key to be used by the the JSON API Serializer responses.
     */
    protected $resourceKey = 'tempcalculations';
    
    protected $table = 'tempcalculations';


    public function getcurrentBalanceAttribute(){
        if($this->tnx_log != 'loan') return null;
        $debit = $this->where(['loan_id' => $this->loan_id, 'type' => 'debit', 'tnx_log' => 'loan'])->get()->sum('amount');
        $credit = $this->where(['loan_id' => $this->loan_id, 'type' => 'credit', 'tnx_log' => 'loan'])->get()->sum('amount');
        return $debit - $credit;
    }

    public function getdueDateAttribute() {
        if($this->tnx_log != 'loan') return null;
        $lastPayment = $this->latest('tnx_date')->where(['loan_id' => $this->loan_id, 'tnx_log' => 'payment'])->first();
        
        $today = \Carbon\Carbon::today();
        $dueDate = \Carbon\Carbon::parse("{$today->year}-{$today->month}-{$this->tnx_date->day}");

        $isFirstPaymentPaid = true;

        if(!$lastPayment) {
            $lastPayment = $this->first();
            $isFirstPaymentPaid = false;
        }
        // dd($lastPayment);

        if($lastPayment->tnx_date->month == $today->month)
            $dueDate->addMonth();

        if(
            (!$isFirstPaymentPaid && $today->diffInMonths($this->tnx_date) >= 1) ||
            ($isFirstPaymentPaid && $today->diffInMonths($lastPayment->tnx_date) >= 1)
        ) {
            $dueDate = \Carbon\Carbon::parse("{$today->year}-{$today->month}-{$this->tnx_date->day}");
            return $dueDate->format('M-d-y'). " ({$dueDate->diffInDays($today)} days late)";
            
        }
        return "{$dueDate->format('M-d-y')} ({$dueDate->diffInDays($today)} days remaining)";
    }

    public function paymentPlan(){
        return $this->hasMany(PaymentPlan::class, 'loan_id', 'loan_id');
    }
}
