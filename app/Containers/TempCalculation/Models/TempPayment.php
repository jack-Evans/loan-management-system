<?php

namespace App\Containers\TempCalculation\Models;

use App\Ship\Parents\Models\Model;

class TempPayment extends Model
{
    protected $fillable = [
        'loan_id',
        'amount',
        'current_balance',
        'status',
        'paid_at',
        'pay_before',
    ];

    protected $attributes = [

    ];

    protected $hidden = [

    ];

    protected $casts = [

    ];

    protected $dates = [
        'created_at',
        'updated_at',
    ];

    /**
     * A resource key to be used by the the JSON API Serializer responses.
     */
    protected $resourceKey = 'temppayments';
    
    protected $table = 'temp_payments';
}
