<?php

namespace App\Containers\TempCalculation\UI\API\Requests;

use App\Ship\Parents\Requests\Request;

/**
 * Class CreateTempCalculationRequest.
 */
class CreateTempCalculationRequest extends Request
{

    /**
     * The assigned Transporter for this Request
     *
     * @var string
     */
    // protected $transporter = \App\Ship\Transporters\DataTransporter::class;

    /**
     * Define which Roles and/or Permissions has access to this request.
     *
     * @var  array
     */
    protected $access = [
        'permissions' => '',
        'roles'       => '',
    ];

    /**
     * Id's that needs decoding before applying the validation rules.
     *
     * @var  array
     */
    protected $decode = [
        // 'id',
    ];

    /**
     * Defining the URL parameters (e.g, `/user/{id}`) allows applying
     * validation rules on them and allows accessing them like request data.
     *
     * @var  array
     */
    protected $urlParameters = [
        // 'id',
    ];

    /**
     * @return  array
     */
    public function rules()
    {
        return [
            'loanid' => 'required|integer',
            'netloan' => 'required|regex:/^\d*(\.\d{1,2})?$/',
            'rate' => 'required|regex:/^\d*(\.\d{1,2})?$/',
            'defaultRate' => 'required|regex:/^\d*(\.\d{1,2})?$/',
            'duration' => 'required|integer',
            'issuedate' => 'required|date_format:Y-m-d',
        ];
    }

    /**
     * @return  bool
     */
    public function authorize()
    {
        return $this->check([
            'hasAccess',
        ]);
    }
}
