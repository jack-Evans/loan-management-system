<?php

namespace App\Containers\TempCalculation\UI\API\Transformers;

use App\Containers\TempCalculation\Models\TempCalculation;
use App\Ship\Parents\Transformers\Transformer;

class TempCalculationTransformer extends Transformer
{
    /**
     * @var  array
     */
    protected $defaultIncludes = [

    ];

    /**
     * @var  array
     */
    protected $availableIncludes = [

    ];

    /**
     * @param TempCalculation $entity
     *
     * @return array
     */
    public function transform(TempCalculation $entity)
    {
        $response = [
            'object' => 'TempCalculation',
            'id' => $entity->getHashedKey(),
            'loan_id' => $entity->loan_id,
            'amount' => round($entity->amount, 2),
            'type' => $entity->type,
            'balance' => round($entity->balance, 2),
            'rate' => $entity->rate,
            'default_rate' => $entity->default_rate,
            'duration' => $entity->duration,
            'descriptions' => $entity->descriptions,
            'tnx_log' => $entity->tnx_log,
            'tnx_date' => $entity->tnx_date,
            'status' => $entity->status,
            'currentBalance' => $entity->currentBalance,
            'dueDate' => $entity->dueDate,
            'paymentPlan' => $entity->paymentPlan,
            'created_at' => $entity->created_at,
            'updated_at' => $entity->updated_at,

        ];

        $response = $this->ifAdmin([
            'real_id'    => $entity->id,
            // 'deleted_at' => $entity->deleted_at,
        ], $response);

        return $response;
    }
}
