<?php

/**
 * @apiGroup           TempCalculation
 * @apiName            addCharges
 *
 * @api                {POST} /v1/tempcalculations/addcharges Endpoint title here..
 * @apiDescription     Endpoint description here..
 *
 * @apiVersion         1.0.0
 * @apiPermission      none
 *
 * @apiParam           {String}  parameters here..
 *
 * @apiSuccessExample  {json}  Success-Response:
 * HTTP/1.1 200 OK
{
  // Insert the response of the request here...
}
 */

/** @var Route $router */
$router->get('tempcalculations/getextrainterests/{loanid}', [
    'as' => 'api_tempcalculation_add_charges',
    'uses'  => 'Controller@makeExtraInterests',
    'middleware' => [
      'auth:api',
    ],
]);
