<?php

/**
 * @apiGroup           TempCalculation
 * @apiName            addCharges
 *
 * @api                {POST} /v1/tempcalculations/addcharges Endpoint title here..
 * @apiDescription     Endpoint description here..
 *
 * @apiVersion         1.0.0
 * @apiPermission      none
 *
 * @apiParam           {Date}    date in Y-m-d formate
 * @apiParam           {Float}   amount with 2 decimal precision
 * @apiParam           {Enum}    credit or debit
 * @apiParam           {String}  description of payment
 *
 * @apiSuccessExample  {json}  Success-Response:
 * HTTP/1.1 200 OK
{
  // Insert the response of the request here...
}
 */

/** @var Route $router */
$router->post('tempcalculations/addcharges/{loanid}', [
    'as' => 'api_tempcalculation_add_charges',
    'uses'  => 'Controller@addCharges',
    'middleware' => [
      'auth:api',
    ],
]);
