<?php

/**
 * @apiGroup           TempCalculation
 * @apiName            deleteTempCalculation
 *
 * @api                {DELETE} /v1/tempcalculations/:id Endpoint title here..
 * @apiDescription     Endpoint description here..
 *
 * @apiVersion         1.0.0
 * @apiPermission      none
 *
 * @apiParam           {String}  parameters here..
 *
 * @apiSuccessExample  {json}  Success-Response:
 * HTTP/1.1 200 OK
{
  // Insert the response of the request here...
}
 */

/** @var Route $router */
$router->delete('tempcalculations/deleteloan/{loanid}', [
    'as' => 'api_tempcalculation_delete_temp_loan',
    'uses'  => 'Controller@deleteLoan',
    'middleware' => [
      'auth:api',
    ],
]);
