<?php

/**
 * @apiGroup           TempCalculation
 * @apiName            updateTempCalculation
 *
 * @api                {PATCH} /v1/tempcalculations/:id Endpoint title here..
 * @apiDescription     Endpoint description here..
 *
 * @apiVersion         1.0.0
 * @apiPermission      none
 *
 * @apiParam           {String}  parameters here..
 *
 * @apiSuccessExample  {json}  Success-Response:
 * HTTP/1.1 200 OK
{
  // Insert the response of the request here...
}
 */

/** @var Route $router */
$router->patch('tempcalculations/updatecharge/{id}', [
    'as' => 'api_tempcalculation_update_temp_charge',
    'uses'  => 'Controller@updateCharge',
    'middleware' => [
      'auth:api',
    ],
]);
