<?php

/**
 * @apiGroup           TempCalculation
 * @apiName            createTempCalculation
 *
 * @api                {POST} /v1/tempcalculations Endpoint title here..
 * @apiDescription     Endpoint description here..
 *
 * @apiVersion         1.0.0
 * @apiPermission      none
 *
 * @apiParam           {String}  parameters here..
 *
 * @apiSuccessExample  {json}  Success-Response:
 * HTTP/1.1 200 OK
{
  // Insert the response of the request here...
}
 */

/** @var Route $router */
$router->post('tempcalculations', [
    'as' => 'api_tempcalculation_create_temp_calculation',
    'uses'  => 'Controller@createTempCalculation',
    'middleware' => [
      'auth:api',
    ],
]);
