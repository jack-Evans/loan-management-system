<?php


/** @var Route $router */
$router->get('getpayment/{loanid}', [
    'as' => 'api_tempcalculation_get_all_temp_payments',
    'uses'  => 'Controller@getPayment',
    'middleware' => [
      'auth:api',
    ],
]);
