<?php

/**
 * @apiGroup           TempCalculation
 * @apiName            findTempCalculationById
 *
 * @api                {GET} /v1/tempcalculations/:id Endpoint title here..
 * @apiDescription     Endpoint description here..
 *
 * @apiVersion         1.0.0
 * @apiPermission      none
 *
 * @apiParam           {String}  parameters here..
 *
 * @apiSuccessExample  {json}  Success-Response:
 * HTTP/1.1 200 OK
{
  // Insert the response of the request here...
}
 */

/** @var Route $router */
$router->get('tempcalculations/{loanid}', [
    'as' => 'api_tempcalculation_find_temp_calculation_by_id',
    'uses'  => 'Controller@findTempCalculationById',
    'middleware' => [
      'auth:api',
    ],
]);
