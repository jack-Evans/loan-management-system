<?php

/**
 * @apiGroup           TempCalculation
 * @apiName            payMonthlyInterest
 *
 * @api                {POST} /v1/payinterest Endpoint title here..
 * @apiDescription     Endpoint description here..
 *
 * @apiVersion         1.0.0
 * @apiPermission      none
 *
 * @apiParam           {String}  parameters here..
 *
 * @apiSuccessExample  {json}  Success-Response:
 * HTTP/1.1 200 OK
{
  // Insert the response of the request here...
}
 */

/** @var Route $router */
$router->post('tempcalculations/paytempcalculation/{loanid}', [
    'as' => 'api_tempcalculation_pay_monthly_interest',
    'uses'  => 'Controller@payTempCalculation',
    'middleware' => [
      'auth:api',
    ],
]);
