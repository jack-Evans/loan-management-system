<?php

/**
 * @apiGroup           TempCalculation
 * @apiName            getAllTempCalculations
 *
 * @api                {GET} /v1/tempcalculations Endpoint title here..
 * @apiDescription     Endpoint description here..
 *
 * @apiVersion         1.0.0
 * @apiPermission      none
 *
 * @apiParam           {String}  parameters here..
 *
 * @apiSuccessExample  {json}  Success-Response:
 * HTTP/1.1 200 OK
{
  // Insert the response of the request here...
}
 */

/** @var Route $router */
$router->get('tempcalculations', [
    'as' => 'api_tempcalculation_get_all_temp_calculations',
    'uses'  => 'Controller@getAllTempCalculations',
    'middleware' => [
      'auth:api',
    ],
]);
