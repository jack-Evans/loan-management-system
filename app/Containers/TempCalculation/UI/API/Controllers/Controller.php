<?php

namespace App\Containers\TempCalculation\UI\API\Controllers;

use App\Containers\TempCalculation\UI\API\Requests\CreateTempCalculationRequest;
use App\Containers\TempCalculation\UI\API\Requests\DeleteTempCalculationRequest;
use App\Containers\TempCalculation\UI\API\Requests\GetAllTempCalculationsRequest;
use App\Containers\TempCalculation\UI\API\Requests\FindTempCalculationByIdRequest;
use App\Containers\TempCalculation\UI\API\Requests\UpdateTempCalculationRequest;
use App\Containers\TempCalculation\UI\API\Transformers\TempCalculationTransformer;
use App\Ship\Parents\Controllers\ApiController;
use Apiato\Core\Foundation\Facades\Apiato;
use App\Containers\TempCalculation\UI\API\Requests\CalculateMonthlyInterestRequest;
use App\Containers\TempCalculation\UI\API\Requests\PayTempCalculationRequest;
use App\Containers\TempCalculation\UI\API\Requests\AddChargesRequest;
use App\Containers\TempCalculation\UI\API\Requests\CurrentBalanceRequest;
use App\Containers\Payment\UI\API\Requests\UpdatePaymentRequest;
use App\Containers\TempCalculation\UI\API\Requests\UpdateChargeRequest;
use App\Containers\TempCalculation\UI\API\Requests\GetPaymentRequest;
/**
 * Class Controller
 *
 * @package App\Containers\TempCalculation\UI\API\Controllers
 */
class Controller extends ApiController
{
    /**
     * @param CreateTempCalculationRequest $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function createTempCalculation(CreateTempCalculationRequest $request)
    {
        $tempcalculation = Apiato::call('TempCalculation@CreateTempCalculationAction', [$request]);

        return $this->created($this->transform($tempcalculation, TempCalculationTransformer::class));
    }        

    /**
     * @param FindTempCalculationByIdRequest $request
     * @return array
     */
    public function findTempCalculationById(FindTempCalculationByIdRequest $request)
    {
        $tempcalculations = Apiato::call('TempCalculation@FindTempCalculationByIdAction', [$request]);

        return $this->transform($tempcalculations, TempCalculationTransformer::class);
    }
    
    /**
     * @param FindTempCalculationByIdRequest $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function makeExtraInterests(FindTempCalculationByIdRequest $request)
    {
        $extraInterests = Apiato::call('TempCalculation@MakeExtraInterestsAction', [$request]);
        
        return $extraInterests;
    }

    /**
     * @param GetAllTempCalculationsRequest $request
     * @return array
     */
    public function getAllTempCalculations(GetAllTempCalculationsRequest $request)
    {
        $tempcalculations = Apiato::call('TempCalculation@GetAllTempCalculationsAction', [$request]);

        return $this->transform($tempcalculations, TempCalculationTransformer::class);
    }

    /**
     * @param UpdateTempCalculationRequest $request
     * @return array
     */
    public function updateTempCalculation(UpdateTempCalculationRequest $request)
    {
        $tempcalculation = Apiato::call('TempCalculation@UpdateTempCalculationAction', [$request]);

        return $this->transform($tempcalculation, TempCalculationTransformer::class);
    }

    /**
     * @param DeleteTempCalculationRequest $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function deleteTempCalculation(DeleteTempCalculationRequest $request)
    {
        Apiato::call('TempCalculation@DeleteTempCalculationAction', [$request]);

        return $this->noContent();
    }
    
    /**
     * @param DeleteTempCalculationRequest $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function deleteLoan(DeleteTempCalculationRequest $request)
    {
        Apiato::call('TempCalculation@DeleteTempLoanAction', [$request]);

        return $this->noContent();
    }
    
    /**
     * @param CalculateMonthlyInterestRequest $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function manageMonthlyInterest(CalculateMonthlyInterestRequest $request)
    { 
        $manageinterest = Apiato::call('TempCalculation@AddMonthlyInterestAction', [$request]);
        
        return $manageinterest;
    }   
    
    /**
     * @param AddChargesRequest $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function addCharges(AddChargesRequest $request)
    {   
        $charges = Apiato::call('TempCalculation@AddChargesAction', [$request]);

        return $charges;
    }
    
    /**
     * @param UpdateChargeRequest $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function updateCharge(UpdateChargeRequest $request)
    {
        $updateCharge = Apiato::call('TempCalculation@UpdateChargeAction', [$request]);
        
        return $updateCharge;
    }
    
     /**
     * @param CurrentBalanceRequest $request
     * @return \Illuminate\Http\JsonResponse
     */    
    public function currentBalance(CurrentBalanceRequest $request)
    {
        $balance = Apiato::call('TempCalculation@CurrentBalanceAction', [$request]);
        return $balance;
    }
    
    /**
     * @param PayTempCalculationRequest $request
     * @return \Illuminate\Http\JsonResponse
     */  
    public function payTempCalculation(PayTempCalculationRequest $request)
    {
        $payInterest = Apiato::call('TempCalculation@PayTempCalculationAction', [$request]);
        
        return $payInterest;
    }
    
    /**
     * @param UpdatePaymentRequest $request
     * @return \Illuminate\Http\JsonResponse
     */  
    public function updatePayment(UpdatePaymentRequest $request)
    {
        $updatePayment = Apiato::call('TempCalculation@UpdatePaymentAction', [$request]);
        
        return $updatePayment;
    }
    
    /**
     * @param GetPaymentRequest $request
     * @return \Illuminate\Http\JsonResponse
     */  
    public function getPayment(GetPaymentRequest $request)
    {
        $payment = Apiato::call('TempCalculation@GetTempPaymentAction', [$request]);
        return $payment;
    }
}
