<?php

/** @var Route $router */
$router->post('tempcalculations/addcharges', [
    'as' => 'web_tempcalculation_add_charges',
    'uses'  => 'Controller@addCharges',
    'middleware' => [
      'auth:web',
    ],
]);
