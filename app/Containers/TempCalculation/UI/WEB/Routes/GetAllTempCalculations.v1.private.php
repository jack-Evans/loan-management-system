<?php

/** @var Route $router */
$router->get('tempcalculations', [
    'as' => 'web_tempcalculation_index',
    'uses'  => 'Controller@index',
    'middleware' => [
      'auth:web',
    ],
]);
