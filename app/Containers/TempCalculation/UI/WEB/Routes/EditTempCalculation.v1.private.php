<?php

/** @var Route $router */
$router->get('tempcalculations/{id}/edit', [
    'as' => 'web_tempcalculation_edit',
    'uses'  => 'Controller@edit',
    'middleware' => [
      'auth:web',
    ],
]);
