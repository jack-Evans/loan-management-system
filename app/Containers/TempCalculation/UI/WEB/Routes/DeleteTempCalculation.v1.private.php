<?php

/** @var Route $router */
$router->get('tempcalculations/delete/{id}', [
    'as' => 'web_tempcalculation_delete',
    'uses'  => 'Controller@delete',
    'middleware' => [
      'auth:web',
    ],
]);
