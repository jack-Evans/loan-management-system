<?php

/** @var Route $router */
$router->get('tempcalculations/deleteloan/{loanid}', [
    'as' => 'web_tempcalculation_delete_loan',
    'uses'  => 'Controller@deleteLoan',
    'middleware' => [
      'auth:web',
    ],
]);
