<?php

/** @var Route $router */
$router->post('tempcalculations/paytempcalculation', [
    'as' => 'web_tempcalculation_pay_interest',
    'uses'  => 'Controller@payTempCalculation',
    'middleware' => [
      'auth:web',
    ],
]);
