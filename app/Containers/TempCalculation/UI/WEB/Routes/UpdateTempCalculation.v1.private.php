<?php

/** @var Route $router */
$router->patch('tempcalculations/{id}', [
    'as' => 'web_tempcalculation_update',
    'uses'  => 'Controller@update',
    'middleware' => [
      'auth:web',
    ],
]);
