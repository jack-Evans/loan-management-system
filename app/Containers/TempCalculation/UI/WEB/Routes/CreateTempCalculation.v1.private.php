<?php

/** @var Route $router */
$router->get('tempcalculations/create', [
    'as' => 'web_tempcalculation_create',
    'uses'  => 'Controller@create',
    'middleware' => [
      'auth:web',
    ],
]);
