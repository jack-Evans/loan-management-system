<?php

/** @var Route $router */
$router->post('tempcalculations/editcharge', [
    'as' => 'web_tempcalculation_update',
    'uses'  => 'Controller@editCharge',
    'middleware' => [
      'auth:web',
    ],
]);
