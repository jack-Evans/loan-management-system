<?php

/** @var Route $router */
$router->get('addinterest', [
    'as' => 'web_tempcalculation_add_monthly_interest',
    'uses'  => 'Controller@addMonthlyInterst',
    'middleware' => [
      'auth:web',
    ],
]);
