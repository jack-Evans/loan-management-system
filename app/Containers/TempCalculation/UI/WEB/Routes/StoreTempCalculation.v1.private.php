<?php

/** @var Route $router */
$router->post('tempcalculations/store', [
    'as' => 'web_tempcalculation_store',
    'uses'  => 'Controller@store',
    'middleware' => [
      'auth:web',
    ],
]);
