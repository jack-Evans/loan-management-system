<?php

/** @var Route $router */
$router->post('tempcalculations/editpayment', [
    'as' => 'web_tempcalculation_update',
    'uses'  => 'Controller@editPayment',
    'middleware' => [
      'auth:web',
    ],
]);
