<?php

/** @var Route $router */
$router->get('tempcalculations/{id}', [
    'as' => 'web_tempcalculation_show',
    'uses'  => 'Controller@show',
    'middleware' => [
      'auth:web',
    ],
]);
