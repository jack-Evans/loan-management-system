<?php

namespace App\Containers\TempCalculation\UI\WEB\Controllers;

use App\Containers\TempCalculation\UI\WEB\Requests\CreateTempCalculationRequest;
use App\Containers\TempCalculation\UI\WEB\Requests\DeleteTempCalculationRequest;
use App\Containers\TempCalculation\UI\WEB\Requests\GetAllTempCalculationsRequest;
use App\Containers\TempCalculation\UI\WEB\Requests\FindTempCalculationByIdRequest;
use App\Containers\TempCalculation\UI\WEB\Requests\UpdateTempCalculationRequest;
use App\Containers\TempCalculation\UI\WEB\Requests\StoreTempCalculationRequest;
use App\Containers\TempCalculation\UI\WEB\Requests\EditTempCalculationRequest;
use App\Ship\Parents\Controllers\WebController;
use Apiato\Core\Foundation\Facades\Apiato;
use App\Containers\TempCalculation\UI\WEB\Requests\PayTempCalculationRequest;

/**
 * Class Controller
 *
 * @package App\Containers\TempCalculation\UI\WEB\Controllers
 */
class Controller extends WebController
{

    /**
     * The assigned API PATH for this Controller
     *
     * @var string
     */
    protected $apiPath;


    public function __construct(){
        $this->apiPath = config('token-container.WEB_API_URL');
    }

    /**
     * Show all Temporary Calculation
     *
     * @param GetAllTempCalculationsRequest $request
     */
    public function index(GetAllTempCalculationsRequest $request) {
        
        if(isset($request->loanid)) {
            session(['loanid' => $request->loanid]);
        }
        // else {
        //     session(['loanid' => null]);
        // }
        
        $currentBalance = '';
        $extraInterests = [];

        if(session('loanid') != null) {        
        
            $url = $this->apiPath.'tempcalculations/'.session('loanid').'?date='.$request->currentdate;
            try {
                $response = get($url);
            } catch (\Exception $e) {
                 return exception($e);
            }                

            $calculations = isset($response->data) ? $response->data : array();


            $url = $this->apiPath.'tempcalculations/getextrainterests/'.session('loanid').'?date='.$request->currentdate;
            try {
                $extraInterests = get($url);
            } catch (\Exception $e) {
                 return exception($e);
            }
            if(isset($request->currentdate)) {              
                $url = $this->apiPath.'tempcalculations/currentbalance/'.session('loanid');            
                $currentDateRequest = [
                    'date' => $request->currentdate
                ];
                
                try {
                    $currentBalance = post($url, $currentDateRequest);
                } catch (\Exception $e) {
                    return exception($e);
                }
            }
        }
        
        $url = $this->apiPath.'tempcalculations';
        $response = get($url);
        $loans = isset($response->data) ? $response->data : array();

        return view('tempcalculation::calculationView', compact('loans', 'calculations', 'currentBalance', 'extraInterests'));
    }

    /**
     * Show one Temporary Calculation
     *
     * @param FindTempCalculationByIdRequest $request
     */
    public function show(FindTempCalculationByIdRequest $request) {
        $tempcalculation = Apiato::call('TempCalculation@FindTempCalculationByIdAction', [$request]);

        // ..
    }

    /**
     * Create Temporary Calculation (show UI)
     *
     * @param CreateTempCalculationRequest $request
     */
    public function create(CreateTempCalculationRequest $request) {
        return view('tempcalculation::create');
    }

    /**
     * Add a new Temporary Calculation
     *
     * @param StoreTempCalculationRequest $request
     */
    public function store(StoreTempCalculationRequest $request)
    {
        $url = $this->apiPath.'tempcalculations';
        
        $loanRequest = [
            'loanid'            => $request->loanid,
            'netloan'           => $request->netloan,
            'rate'              => $request->rate,
            'defaultRate'       => $request->defaultRate,
            'duration'          => $request->duration,
            'issuedate'         => $request->issuedate,
    	];

        try {
            $response = post($url, $loanRequest);
        } catch (\Exception $e) {
            return exception($e);
        }

        return redirect('tempcalculations?loanid='.$request->loanid);
    }

    /**
     * Edit entity (show UI)
     *
     * @param EditTempCalculationRequest $request
     */
    public function edit(EditTempCalculationRequest $request) {
        $tempcalculation = Apiato::call('TempCalculation@GetTempCalculationByIdAction', [$request]);

        // ..
    }

    /**
     * Update a given entity
     *
     * @param UpdateTempCalculationRequest $request
     */
    public function update(UpdateTempCalculationRequest $request) {
        $tempcalculation = Apiato::call('TempCalculation@UpdateTempCalculationAction', [$request]);

        // ..
    }

    /**
     * Delete a given Temporary Calculation
     *
     * @param DeleteTempCalculationRequest $request
     */
    public function delete(DeleteTempCalculationRequest $request) {
        $url = $this->apiPath.'tempcalculations/'.$request->id;
        
        try {
            $response = delete($url);
        } catch (\Exception $e) {
            return exception($e);
        }

        return redirect('tempcalculations');

    }
    
    /**
     * Delete a given loan
     *
     * @param DeleteTempCalculationRequest $request
     */
    public function deleteLoan(DeleteTempCalculationRequest $request) {
        $url = $this->apiPath.'tempcalculations/deleteloan/'.$request->loanid;
        
        try {
            $response = delete($url);
        } catch (\Exception $e) {
            return exception($e);
        }

        return redirect('tempcalculations');
    }
    

    // PayTempCalculationRequest for both functions because we aren't doing anything in them.
    /**
     * Add a new Charge
     *
     * @param PayTempCalculationRequest $request
     */       
    public function addCharges(PayTempCalculationRequest $request) {
        $url = $this->apiPath.'tempcalculations/addcharges/'.session('loanid');
        
        $addChargeRequest = [
            'date'          => $request->date,
            'amount'        => $request->amount,
            'type'          => $request->type,
            'description'   => $request->description,
    	];

        try {
            $response = post($url, $addChargeRequest);
        } catch (\Exception $e) {
            return exception($e);
        }

        return redirect('tempcalculations');
    }

    /**
     * Edit Charge
     *
     * @param PayTempCalculationRequest $request
     */       
    public function editCharge(PayTempCalculationRequest $request) {
        $url = $this->apiPath.'tempcalculations/updatecharge/'.$request->loanId;
        
        $editChargeRequest = [
            'amount'    => $request->amount,
            'type'      => $request->type,
    	];

        try {
            $response = patch($url, $editChargeRequest);
        } catch (\Exception $e) {
            return exception($e);
        }
        return redirect('tempcalculations');
    }

     /**
     * Add Monthly Interest
     *
     * @param PayTempCalculationRequest $request
     */ 
    public function addMonthlyInterst(PayTempCalculationRequest $request) {
        $url = $this->apiPath.'manageinterest/'.session('loanid');
        
        try {
            $response = get($url);
        } catch (\Exception $e) {
            return exception($e);
        }

        return redirect('tempcalculations');
    }


    /**
     * Pay Temporary Loan
     *
     * @param PayTempCalculationRequest $request
     */
    public function payTempCalculation(PayTempCalculationRequest $request) {
        $url = $this->apiPath.'tempcalculations/paytempcalculation/'.session('loanid');
        
        $payInterestRequest = [
            'date'         => $request->date,
            'amount'        => $request->amount,
            'description'        => $request->description,
    	];

        try {
            $response = post($url, $payInterestRequest);
        } catch (\Exception $e) {
            return exception($e);
        }

        return redirect('tempcalculations');
    }
    
    /**
     * Edit Payment
     *
     * @param PayTempCalculationRequest $request
     */
    public function editPayment(PayTempCalculationRequest $request) {
        $url = $this->apiPath.'tempcalculations/updatepaymet/'.$request->loanId;
        
        $editPaymentRequest = [
            'amount'        => $request->amount,
    	];

        try {
            $response = __patch($url, $editPaymentRequest);
        } catch (\Exception $e) {
            return exception($e);
        }
        return redirect('tempcalculations');
    }   
        
}
