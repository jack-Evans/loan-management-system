@extends('layouts.app')

@section('title', 'Calculations')

@section('content')

    <form action="{{url('tempcalculations')}}" method="get">
        <div class="row">
            <div class="col-3">
                <b class="text-info">Today made up date:</b>
                <input class="form-control datepicker4" autocomplete="off" name="currentdate" value="{{isset($_GET['currentdate']) ? $_GET['currentdate'] : ''}}">
            </div>
            <div class="col-2">
                <button type="submit" class="btn btn-success mt-4">Go</button>
            </div>
            <div class="col-2">
              <div class="mt-4">
                <b class="text-success">Current Balance:</b>
                <p class="text-success">{{$currentBalance > 0 ? $currentBalance: '' }}</p>
                </div>
            </div>
         </div>
    </form>
    <span class="space padding-10"></span>
    <h6>Create Loan</h6>
    <form action="{{ url('tempcalculations/store' )}}" method="post">
        <div class="row">
            <div class="col-2">
                @csrf()
                <div class="form-group">
                  <label for="loanid">Loan Id:</label>
                  <input type="text" class="form-control" name="loanid"
                         value="{{old('loanid')}}">
                </div>
            </div>

            <div class="col-2">
                <div class="form-group">
                  <label for="netloan">Net Loan:</label>
                  <input type="text" class="form-control" name="netloan"
                         value="{{old('netloan')}}">
                </div>

            </div>
            <div class="col-1">
                <div class="form-group">
                  <label for="rate">Rate:</label>
                  <input type="text" class="form-control" name="rate"
                         value="{{ old('rate')}}">
                </div>
            </div>
            <div class="col-2">
                <div class="form-group ">
                    <label for="defaultRate">Default Rate:</label>
                    <input type="text" class="form-control" name="defaultRate"
                           value="{{ old('defaultRate')}}">
                </div>
            </div>
            <div class="col-2">
                <div class="form-group">
                    <label for="duration">Duration:</label>
                    <input type="text" class="form-control" name="duration"
                           value="{{ old('duration')}}">
                </div>
            </div>
            <div class="col-2">
                <div class="form-group">
                    <label for="issuedate">Issue Date:</label>
                    <input class="form-control datepicker" autocomplete="off" name="issuedate"
                           value="{{ old('issuedate')}}">
                </div>
            </div>
            {{-- <div class="col-2">
                <div class="form-group">
                    <label for="issuedate">Description:</label>
                    <input class="form-control datepicker" autocomplete="off" name="issuedate"
                           value="{{ old('issuedate')}}">
                </div>
            </div> --}}
            <div class="col-1 mt-3">
                <button type="submit" class="btn btn-primary mt-3 float-right">Submit</button>
            </div>
        </div>
    </form>

    @if(isset($loans))
    <div class="panel-group has-box-shadow" id="accordion">
        <div class="panel panel-default">
            <div class="panel-heading" data-target="#Collapseone" data-toggle="collapse" data-parent="#accordion">
                <h5 class="text-info panel-title">
                    All Loans
                    <a href="#">
                        <span class="text-info fa fa-plus"></span>
                    </a>
                </h5>
            </div>
            <div class="panel-collapse collapse in" id="Collapseone">
                <div class="panel-body">
                    <table class="table text-left">
                        <thead>
                            <tr>
                                <th>Loan Id</th>
                                <th>Issue Date</th>
                                <th>Net Loan</th>
                                <th>Status</th>
                                <th>Balance</th>
                                <th>Due Date</th>
                                <th>Description</th>
                                <th></th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach($loans as $key => $loan)
                            @php
                            if($loan->tnx_log != 'loan')
                            {
                                continue;
                            }
                            @endphp
                            <tr>
                                <td>{{$loan->loan_id}}</td>
                                <td>{{date('M d Y', strtotime($loan->tnx_date->date))}}</td>
                                <td>{{$loan->balance}}</td>
                                <td>{{ $loan->status }}</td>
                                <td>{{ $loan->currentBalance }}</td>
                                <td>{{ $loan->dueDate }}</td>
                                <td>{{ $loan->descriptions }}</td>
                                <td>
                                    <a href="{{url('tempcalculations/deleteloan/'.$loan->loan_id)}}" class="btn btn-danger ml-1 float-right" >
                                            Delete
                                    </a>
                                    <a href="{{url('tempcalculations?loanid='.$loan->loan_id)}}" class="btn btn-info ml-1 float-right" >
                                            View
                                    </a>
                                </td>
                            </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
    @endif

@if(session('loanid') != null && sizeof($calculations) > 0)
  <div class="has-box-shadow">
    <h5 class="text-primary">Charges</h5>
    @if(isset($calculations))
        <table class="table text-left">
            <thead>
                <tr>
                    <th>Date</th>
                    <th>Charge amount</th>
                    <th>Balance after charge added</th>
                    <th colspan="2">Description</th>
                </tr>
            </thead>
            <tbody>
                @foreach($calculations as $key => $charge)
                @php
                if($charge->amount == 0)
                {
                    continue;
                }
                @endphp
                @if($charge->tnx_log == 'charge')
                <tr>
                    <td>{{date('M d Y', strtotime($charge->tnx_date->date))}}</td>
                    <td>{{($charge->type == 'debit') ? $charge->amount : '' }}</td>
                    <td>{{$charge->balance}}</td>
                    <td>{{$charge->descriptions}}</td>
                    <td>
                        <a href="{{url('tempcalculations/delete/'.$charge->id)}}" class="btn btn-danger ml-1 float-right" >
                            Ignore
                        </a>
                        <button type="button" class="btn btn-default float-right loanModal" data-toggle="modal"
                                data-target="#editChargeModal" data-loanid="{{$charge->id}}">
                            Edit
                        </button>
                    </td>
                </tr>
                @endif
                @endforeach
            </tbody>
        </table>
    @endif
  </div>
  <div class="has-box-shadow">
    <h6>Add charges</h6>
    <form action="{{ url('tempcalculations/addcharges' )}}" method="post">
        <div class="row">
            <div class="col-3">
                @csrf()
                <div class="form-group">
                  <label for="date">Date:</label>
                  <input type="text" class="form-control datepicker2" autocomplete="off" name="date"
                         value="{{old('date')}}">
                </div>
            </div>
            <div class="col-2">
                <div class="form-group">
                  <label for="amount">Amount:</label>
                  <input type="text" class="form-control" name="amount"
                         value="{{ old('amount')}}">
                </div>
            </div>
            <div class="col-2">
                <div class="form-group ">
                    <label for="type">Type:</label>
                    <select class="form-control" name="type">
                        <option value="fixed">Fixed</option>
                        <option value="percentage">Percentage</option>
                    </select>

                </div>
            </div>
            <div class="col-2">
                <div class="form-group">
                    <label for="description">Description:</label>
                    <input type="text" class="form-control" name="description"
                           value="{{ old('description')}}">
                </div>
            </div>
            <div class="col-3">
                <button type="submit" class="btn btn-primary mt-3 float-right">Submit</button>
            </div>
        </div>
    </form>
  </div>
  <div class="has-box-shadow">
    <h5 class="text-success">Payments</h5>
    @if(isset($calculations))
        <table class="table text-left">
            <thead>
                <tr>
                    <th>Date</th>
                    <th>Amount paid</th>
                    <th>Balance after payment</th>
                    <th colspan="2">Description</th>
                </tr>
            </thead>
            <tbody>
                @foreach($calculations as $key => $payment)
                @php
                if($payment->amount == 0)
                {
                    continue;
                }
                @endphp
                @if($payment->tnx_log == 'payment')
                <tr>
                    <td>{{date('M d Y', strtotime($payment->tnx_date->date))}}</td>
                    <td>{{($payment->type == 'credit') ? $payment->amount : '' }}</td>
                    <td>{{$payment->balance}}</td>
                    <td>{{$payment->descriptions}}</td>
                    <td>
                        <a href="{{url('tempcalculations/delete/'.$payment->id)}}" class="btn btn-danger ml-1 float-right" >
                            Ignore
                        </a>
                        <button type="button" class="btn btn-default float-right loanModal" data-toggle="modal"
                                data-target="#editPaymentModal" data-loanid="{{$payment->id}}">
                            Edit
                        </button>
                    </td>

                </tr>
                @endif
                @endforeach
            </tbody>
        </table>
    @endif
  </div>
  <div class="has-box-shadow">
    <h6>Pay amount</h6>
    <form action="{{ url('tempcalculations/paytempcalculation' )}}" method="post">
        <div class="row">
            <div class="col-3">
                @csrf()
                <div class="form-group">
                  <label for="date">Date:</label>
                  <input type="text" class="form-control datepicker3" autocomplete="off" name="date"
                         value="{{old('date')}}">
                </div>
            </div>
            <div class="col-2">
                <div class="form-group">
                  <label for="amount">Amount:</label>
                  <input type="text" class="form-control" name="amount"
                         value="{{ old('amount')}}">
                </div>
            </div>
            <div class="col-2">
                <div class="form-group">
                    <label for="description">Description:</label>
                    <input type="text" class="form-control" name="description"
                           value="{{ old('description')}}">
                </div>
            </div>
            <div class="col-5">
                <button type="submit" class="btn btn-primary mt-3 float-right">Submit</button>
            </div>
        </div>
    </form>
  </div>
  <div class="has-box-shadow">
    <div class="row">
        <div class="col-6">
            <h5 class="text-warning">Statements</h5>
        </div>
        <div class="col-6">
<!--            <a class="pull-right" href="{{url('addinterest')}}">
                <button class="btn btn-success mt-3 float-right">Add monthly interest</button>
            </a>-->
        </div>
              </div>
    <div class="row mt-1">
        <div class="col-12">
          <div class="auto-scroll">
            <table class="table text-left">
                <thead>
                    <tr>
                        <th>Date</th>
                        <th>Debit</th>
                        <th>Credit</th>
                        <th>Balance</th>
                        <th>Description</th>
                    </tr>
                </thead>

                @if(isset($calculations))
                <tbody>
                    @php 
                        $netloan = 0;
                        $paymentPlan = [];
                    @endphp
                    @foreach($calculations as $key => $calculation)
                    @php
                    if($calculation->amount == 0)
                    {
                        continue;
                    }
                    if($calculation->tnx_log == 'loan' || $calculation->tnx_log == 'charge')
                    {
                        $netloan = $netloan + $calculation->amount;
                        $rate = $calculation->rate;
                        if($calculation->paymentPlan)
                            $paymentPlan = $calculation->paymentPlan;
                    }
                    @endphp
                    <tr>
                        <td>{{date('M d Y', strtotime($calculation->tnx_date->date))}}</td>
                        <td>{{($calculation->type == 'debit') ? $calculation->amount : '' }}</td>
                        <td>{{($calculation->type == 'credit') ? $calculation->amount : '' }}</td>
                        <td>{{$calculation->balance}}</td>
                        <td>{{$calculation->descriptions}}</td>

                    </tr>
                    @endforeach

                    @if(isset($extraInterests))
                        @foreach($extraInterests as $key => $interest)
                        <tr>
                            <td>{{date('M d Y', strtotime($interest->tnx_date->date))}}</td>
                            <td>{{$interest->amount}}</td>
                            <td></td>
                            <td>{{$interest->balance}}</td>
                            <td>{{$interest->descriptions}}</td>

                        </tr>
                        @endforeach
                    @endif

                </tbody>
                @endif
            </table>
          </div>
        </div>
    </div>
</div>
<div class="has-box-shadow">
    <div class="row">
        <div class="col-6">
            <h5 class="text-danger">Payment Plan</h5>
        </div>
    </div>

    <div class="row mt-1">
        <div class="col-12">
          <div class="auto-scroll">
            <table class="table text-left">
                <thead>
                    <tr>
                        <th>Amount</th>
                        <!--<th>Status</th>-->
                        <th>Due date</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach ($paymentPlan as $payable)
                        <tr>
                            <td>{{ $payable->amount }}</td>
                            <td>{{ $payable->pay_before }}</td>
                        </tr>
                    @endforeach
                </tbody>
                {{-- @if(sizeof($calculations) > 0)
                @php
                    $duration = $calculations[0]->duration + 1;
                    $issueDate = $calculations[0]->tnx_date->date;
                    $tnxDate = end($calculations)->tnx_date->date;

                    $ts1 = strtotime($issueDate);
                    $ts2 = strtotime($tnxDate);

                    $year1 = date('Y', $ts1);
                    $year2 = date('Y', $ts2);

                    $month1 = date('m', $ts1);
                    $month2 = date('m', $ts2);
                    $diff = (($year2 - $year1) * 12) + ($month2 - $month1);
                    $diff = ($diff == 0) ? $diff+1 : $diff;
                @endphp
                <tbody>
                    @while($diff < $duration )
                    @php
                        $nowDate = date('M d Y', strtotime("+".$diff."months", strtotime($issueDate)));
                        $diff++;
                    @endphp
                    <tr>
                        <td>{{ ($netloan/100) * $rate }}</td>
                        <!--<td></td>-->
                        <td>{{$nowDate}}</td>
                    </tr>

                    @endwhile
                </tbody>
                @endif --}}
            </table>
          </div>
        </div>
    </div>
@endif
</div>

<!-- Edit Payment Modal -->
<div class="modal" id="editPaymentModal">
  <div class="modal-dialog">
    <div class="modal-content">

      <!-- Modal Header -->
      <div class="modal-header">
        <h4 class="modal-title">Edit Payment</h4>
        <button type="button" class="close" data-dismiss="modal">&times;</button>
      </div>

      <!-- Modal body -->
      <div class="modal-body">
        <form action="{{'tempcalculations/editpayment'}}" method="post">
            @csrf()

            <input type="hidden" class="form-control" name="loanId" value="{{old('loanId')}}">

            <div class="form-group">
              <label for="amount">Amount:</label>
              <input class="form-control" name="amount" value="{{old('amount')}}"/>
            </div>

            <button type="submit" class="btn btn-primary">Submit</button>
        </form>
      </div>

      <!-- Modal footer -->
      <div class="modal-footer">
        <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
      </div>

    </div>
  </div>
</div>

<!-- Edit Charge Modal -->
<div class="modal" id="editChargeModal">
  <div class="modal-dialog">
    <div class="modal-content">

      <!-- Modal Header -->
      <div class="modal-header">
        <h4 class="modal-title">Edit Charge</h4>
        <button type="button" class="close" data-dismiss="modal">&times;</button>
      </div>

      <!-- Modal body -->
      <div class="modal-body">
        <form action="{{'tempcalculations/editcharge'}}" method="post">
            @csrf()

            <input type="hidden" class="form-control" name="loanId" value="{{old('loanId')}}">

            <div class="form-group">
              <label for="amount">Amount:</label>
              <input class="form-control" name="amount" value="{{old('amount')}}"/>
            </div>

            <div class="form-group">
                <label for="type">Type:</label>
                <select class="form-control" name="type">
                    <option value="fixed">Fixed</option>
                    <option value="percentage">Percentage</option>
                </select>
            </div>

            <button type="submit" class="btn btn-primary">Submit</button>
        </form>
      </div>

      <!-- Modal footer -->
      <div class="modal-footer">
        <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
      </div>

    </div>
  </div>
</div>

@endsection

@section('pages-js')
<script>
    $(document).on("click", ".loanModal", function () {
        var loanId = $(this).data('loanid');
        $("input[name=loanId]").val(loanId);
    });

    $(document).ready(function(){
        $('.collapse').on('shown.bs.collapse', function(){
            $(this).parent().find(".fa-plus").removeClass("fa-plus").addClass("fa-minus");
        }).on('hidden.bs.collapse', function(){
            $(this).parent().find(".fa-minus").removeClass("fa-minus").addClass("fa-plus");
        });
    });
</script>
@endsection
