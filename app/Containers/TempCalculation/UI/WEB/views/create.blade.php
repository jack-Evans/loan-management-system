@extends('layouts.app')

@section('title', 'Calculator')

@section('content')
    <div class="row">
        <div class="col-md-5 col-md-offset-2">
            <h3>Create Dummy Loan</h3>             
            <form action="{{ url('tempcalculations/store' )}}" method="post">
                @csrf()

                <div class="form-group">
                  <label for="loanid">Loan Id:</label>
                  <input type="text" class="form-control" name="loanid" 
                         value="{{old('loanid')}}">
                </div>                  

                <div class="form-group">
                  <label for="loanamount">Loan Amount:</label>
                  <input type="text" class="form-control" name="loanamount" 
                         value="{{ old('loanamount')}}">
                </div>

                <div class="form-group">
                  <label for="charges">Charges:</label>
                  <input type="text" class="form-control" name="charges" 
                         value="{{ old('charges')}}">
                </div>

                <div class="form-group ">
                  <label for="issueAt">Issue At:</label>
                  <input type="text" class="form-control datepicker" name="issueAt" 
                         value="{{ old('issueAt')}}">
                </div>
                
                <div class="form-group">
                  <label for="duration">Duration:</label>
                  <input type="text" class="form-control" name="duration" 
                         value="{{ old('duration')}}">
                </div>

                <div class="form-group">
                  <label for="rate">Rate:</label>
                  <input type="text" class="form-control" name="rate" 
                         value="{{ old('rate')}}">
                </div>                                                          

                <button type="submit" class="btn btn-primary">Submit</button>
            </form>
        </div>
    </div>
@endsection