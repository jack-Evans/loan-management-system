<?php

namespace App\Containers\TempCalculation\Exceptions;

use App\Ship\Parents\Exceptions\Exception;
use Symfony\Component\HttpFoundation\Response;

class LoanIdAlreadyTakenException extends Exception
{
    public $httpStatusCode = Response::HTTP_BAD_REQUEST;

    public $message = 'This loan id has already been taken.';

    public $code = 0;
}
