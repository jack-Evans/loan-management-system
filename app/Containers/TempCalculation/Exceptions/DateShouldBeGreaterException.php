<?php

namespace App\Containers\TempCalculation\Exceptions;

use App\Ship\Parents\Exceptions\Exception;
use Symfony\Component\HttpFoundation\Response;

class DateShouldBeGreaterException extends Exception
{
    public $httpStatusCode = Response::HTTP_BAD_REQUEST;

    public $message = 'Date should be greater than loan issue date.';

    public $code = 0;
}
