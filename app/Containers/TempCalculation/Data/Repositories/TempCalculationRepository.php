<?php

namespace App\Containers\TempCalculation\Data\Repositories;

use App\Ship\Parents\Repositories\Repository;

/**
 * Class TempCalculationRepository
 */
class TempCalculationRepository extends Repository
{

    /**
     * @var array
     */
    protected $fieldSearchable = [
        'id' => '=',
        // ...
    ];

}
