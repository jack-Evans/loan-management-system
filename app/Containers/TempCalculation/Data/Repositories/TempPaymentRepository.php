<?php

namespace App\Containers\TempCalculation\Data\Repositories;

use App\Ship\Parents\Repositories\Repository;

/**
 * Class TempPaymentRepository
 */
class TempPaymentRepository extends Repository
{

    /**
     * @var array
     */
    protected $fieldSearchable = [
        'id' => '=',
        // ...
    ];

}
