<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateTempcalculationTables extends Migration
{

    /**
     * Run the migrations.
     */
    public function up()
    {
        Schema::create('tempcalculations', function (Blueprint $table) {

            $table->increments('id');
            $table->unsignedInteger('loan_id');
            $table->float('amount', 12, 4);
            $table->enum('type', ['credit', 'debit'])->default('credit');
            $table->float('balance', 12, 4);
            $table->float('rate', 8,4);
            $table->float('default_rate', 8,4);
            $table->string('duration');
            $table->string('descriptions');
            $table->date('tnx_date');
            $table->string('tnx_log');
            $table->timestamps();
            $table->softDeletes();

        });
    }

    /**
     * Reverse the migrations.
     */
    public function down()
    {
        Schema::drop('tempcalculations');
    }
}
