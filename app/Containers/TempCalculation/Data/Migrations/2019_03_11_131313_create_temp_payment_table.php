<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateTempPaymentTable extends Migration
{

    /**
     * Run the migrations.
     */
    public function up()
    {
        Schema::create('temp_payments', function (Blueprint $table) {

            $table->increments('id');
            $table->unsignedInteger('loan_id');
            $table->float('amount', 12, 4);
            $table->float('current_balance', 12, 4);
            $table->enum('status', ['pending','paid'])->default('pending');
            $table->date('paid_at')->nullable();
            $table->date('pay_before');
            $table->timestamps();
            $table->softDeletes();

        });
    }

    /**
     * Reverse the migrations.
     */
    public function down()
    {
        Schema::drop('temp_payments');
    }
}
