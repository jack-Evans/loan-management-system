<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class AddColumnStatusTempcalculation extends Migration
{

    /**
     * Run the migrations.
     */
    public function up()
    {
        Schema::table('tempcalculations', function ($table) {
            $table->enum('status', ['approved','processing', 'rejected', 'defaulted'])->default('processing');
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down()
    {
        Schema::table('tempcalculations', function ($table) {
            $table->dropColumn('status');
        });
    }
}
