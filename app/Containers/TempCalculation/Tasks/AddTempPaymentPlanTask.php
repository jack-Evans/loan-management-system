<?php

namespace App\Containers\TempCalculation\Tasks;

use App\Containers\Payment\Data\Repositories\PaymentPlanRepository;
use App\Ship\Exceptions\UpdateResourceFailedException;
use App\Ship\Parents\Tasks\Task;
use Exception;
use \Carbon\Carbon;

class AddTempPaymentPlanTask extends Task
{

    protected $repository;

    public function __construct(PaymentPlanRepository $repository)
    {
        $this->repository = $repository;
    }

    public function run($loan)
    {
        $payBefore = Carbon::parse($loan['tnx_date']);
        $loanId   = $loan['loan_id'];
        try {
            $paymentPlans = [];
            $perMonthAmount = calculateMonthlyPMT($loan['rate'], $loan['balance']);
            for($i = 0; $i < $loan['duration']; $i++){
                $payBefore->addMonth();
                $paymentPlans[] = $this->repository->create(['amount' =>$perMonthAmount, 'pay_before' => $payBefore, 'loan_id' => $loanId]);
            }
            return $paymentPlans;
        }
        catch (Exception $exception) {
            throw new UpdateResourceFailedException();
        }
    }
}
