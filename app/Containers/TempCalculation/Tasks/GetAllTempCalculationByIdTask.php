<?php

namespace App\Containers\TempCalculation\Tasks;

use App\Containers\TempCalculation\Data\Repositories\TempCalculationRepository;
use App\Ship\Exceptions\NotFoundException;
use App\Ship\Parents\Tasks\Task;
use Exception;

class GetAllTempCalculationByIdTask extends Task
{

    protected $repository;

    public function __construct(TempCalculationRepository $repository)
    {
        $this->repository = $repository;
    }

    public function run($loanid, $date = null)
    {
        try {
             return $this->repository->with('paymentPlan')->scopeQuery(function($q) use ($loanid, $date){
                if($date != null)
                {
                    $q = $q->where('tnx_date', '<=', $date);
                }
                return $q = $q->where('loan_id', '=', $loanid);
            })->get();
        }
        catch (Exception $exception) {
            throw new NotFoundException();
        }
    }
}
