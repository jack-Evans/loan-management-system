<?php

namespace App\Containers\TempCalculation\Tasks;

use App\Containers\TempCalculation\Data\Repositories\TempCalculationRepository;
use App\Ship\Parents\Tasks\Task;
use Carbon\Carbon;

class CalculateLastMonthInterestTask extends Task
{

    protected $repository;

    public function __construct(TempCalculationRepository $repository)
    {
        $this->repository = $repository;
    }

    public function run(array $data, $uptoDate = null)
    {                
        $lastMonthTnxs = $data['lastMonthTnxs'];
        
        $lastTnx = '';
        
//        $response = [];
        $interest = 0;

        $tnxCount = count($lastMonthTnxs);
        $i = 0;
        
        foreach($lastMonthTnxs as $key => $tnx)
        {
            ++$i; 

            if($key == 0)
            {
                $startDate = new Carbon($tnx->tnx_date);
                if($lastMonthTnxs[0]->tnx_log != 'loan')
                {
                    $startDate->startOfMonth();
                }
            }
            else
            {
                $startDate = new Carbon($lastMonthTnxs[$key-1]->tnx_date);
            }
            
            $currentTnxDate = new Carbon($tnx->tnx_date);

            $diffInDays = $startDate->diffInDays($currentTnxDate);

            $dailyInterest =  calculateDailyRate($tnx->rate, ($tnx->type == 'debit') ? $tnx->balance - $tnx->amount : $tnx->balance + $tnx->amount );            

            $interest = round($interest + round($dailyInterest * $diffInDays, 2), 2);
            
            // if its last transaction
            if($i == $tnxCount)
            {
                $dailyInterest =  calculateDailyRate($tnx->rate, $tnx->balance);

                $tnxDate = new Carbon($tnx->tnx_date);
                
                if($uptoDate != null)
                {
                    $endOfMonth = $uptoDate;
                }
                else
                {
                    $endOfMonth = $tnxDate->endOfMonth();
                }
                $diffInDays = $currentTnxDate->diffInDays($endOfMonth) + 1;
                $interest = round($interest + round($dailyInterest * $diffInDays, 2), 2);
                
            }
        }
        
        return [
//                'response' => $response,
                'interest' => $interest,
                'endOfMonth' => $endOfMonth,
            ];
    }
}