<?php

namespace App\Containers\TempCalculation\Tasks;

use App\Containers\TempCalculation\Data\Repositories\TempCalculationRepository;
use App\Ship\Exceptions\NotFoundException;
use App\Ship\Parents\Tasks\Task;
use Exception;
//use App\Containers\TempCalculation\Models\TempCalculation;

class GetLastTranasactionByDateTask extends Task
{

    protected $repository;

    public function __construct(TempCalculationRepository $repository)
    {
        $this->repository = $repository;
    }
    
    public function run($loanid, $date = null)
    {                
        try {
            return $this->repository->scopeQuery(function($q) use($loanid, $date){
                $q = $q->where("loan_id", "=", $loanid);
                if($date != null)
                {
                    $q->where("tnx_date", "<=", $date);
                }
               return $q;
            })->get()->last();                    
        }
        catch (Exception $exception) {
            throw new NotFoundException();
        }
    }
}
