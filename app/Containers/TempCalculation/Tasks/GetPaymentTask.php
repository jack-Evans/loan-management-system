<?php

namespace App\Containers\TempCalculation\Tasks;

use App\Containers\TempCalculation\Data\Repositories\TempPaymentRepository;
use App\Ship\Parents\Tasks\Task;

class GetPaymentTask extends Task
{

    protected $repository;

    public function __construct(TempPaymentRepository $repository)
    {
        $this->repository = $repository;
    }

    public function run($loanid)
    {
        return $this->repository->scopeQuery(function($q) use ($loanid){
            return $q->where('loan_id', '=', $loanid);
        })->get();
    }
}
