<?php

namespace App\Containers\TempCalculation\Tasks;

use App\Containers\TempCalculation\Data\Repositories\TempCalculationRepository;
use App\Ship\Exceptions\NotFoundException;
use App\Ship\Parents\Tasks\Task;
use Exception;
use App\Containers\TempCalculation\Models\TempCalculation;

class FindLastMonthtnxsTask extends Task
{

    protected $repository;

    public function __construct(TempCalculationRepository $repository)
    {
        $this->repository = $repository;
    }
    // parameter interest to check if interest already paid for the month
    public function run($loanid, $startDate, $endDate, $interest = null, $withoutTnxLog = null)
    {                
        try {
            if($interest != null){
                return TempCalculation::where('tnx_date', '>=', $startDate)
                    ->where('tnx_date', '<=', $endDate)
                    ->where('tnx_log', '=', $interest)
                    ->where('loan_id', '=', $loanid)
                    ->orderby('tnx_date', 'asc')->get();
            }

            else if($withoutTnxLog != null){
                return TempCalculation::where('tnx_date', '>=', $startDate)
                    ->where('tnx_date', '<=', $endDate)
                    ->where('tnx_log', '!=', $withoutTnxLog)
                    ->where('loan_id', '=', $loanid)
                    ->orderby('tnx_date', 'asc')->get();
            }
            else{
                return TempCalculation::where('tnx_date', '>=', $startDate)
                    ->where('tnx_date', '<=', $endDate)
                    ->where('loan_id', '=', $loanid)
                    ->orderby('tnx_date', 'asc')->get();
            }
        }
        catch (Exception $exception) {
            throw new NotFoundException();
        }
    }
}
