<?php

namespace App\Containers\TempCalculation\Tasks;

use App\Containers\TempCalculation\Data\Repositories\TempCalculationRepository;
use App\Ship\Exceptions\NotFoundException;
use App\Ship\Parents\Tasks\Task;
use Exception;
use App\Containers\TempCalculation\Models\TempCalculation;

class GetTnxsFromLastUpdateToEndTask extends Task
{

    protected $repository;

    public function __construct(TempCalculationRepository $repository)
    {
        $this->repository = $repository;
    }

    public function run($id, $loanId, $tnxDate)
    {            
        try {
            return TempCalculation::where('id', '>', $id)
                        ->where('loan_id', '=', $loanId)
                        ->where('tnx_date', '>=', $tnxDate)
                        ->orderby('tnx_date', 'asc')
                        ->get();
        }
        catch (Exception $exception) {
            throw new NotFoundException();
        }
    }
}
