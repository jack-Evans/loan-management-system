<?php

namespace App\Containers\TempCalculation\Tasks;

use App\Containers\TempCalculation\Data\Repositories\TempPaymentRepository;
use App\Ship\Exceptions\UpdateResourceFailedException;
use App\Ship\Parents\Tasks\Task;
use Exception;

class UpdateTempPaymentTask extends Task
{

    protected $repository;

    public function __construct(TempPaymentRepository $repository)
    {
        $this->repository = $repository;
    }

    public function run($id, array $data)
    {
        
        try {
            return $this->repository->update($data, $id);
        }
        catch (Exception $exception) {
            throw new UpdateResourceFailedException();
        }
    }
}
