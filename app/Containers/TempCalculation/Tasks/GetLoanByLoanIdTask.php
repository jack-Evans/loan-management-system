<?php

namespace App\Containers\TempCalculation\Tasks;

use App\Containers\TempCalculation\Data\Repositories\TempCalculationRepository;
use App\Ship\Parents\Tasks\Task;

class GetLoanByLoanIdTask extends Task
{

    protected $repository;

    public function __construct(TempCalculationRepository $repository)
    {
        $this->repository = $repository;
    }

    public function run($loanid)
    {
        return $this->repository->findWhere([
            'loan_id' => $loanid,
            'tnx_log' => 'loan'
                ])->first();
    }
}
