<?php

namespace App\Containers\TempCalculation\Tasks;

use App\Containers\TempCalculation\Data\Repositories\TempCalculationRepository;
use App\Ship\Exceptions\UpdateResourceFailedException;
use App\Ship\Parents\Tasks\Task;
use Exception;
use Apiato\Core\Foundation\Facades\Apiato;
use App\Ship\Exceptions\InternalErrorException;
use Carbon\Carbon;

class UpdateTempCalculationStatementTask extends Task
{

    protected $repository;

    public function __construct(TempCalculationRepository $repository)
    {
        $this->repository = $repository;
    }

    public function run($id, $amount)
    {
        $transaction = Apiato::call('TempCalculation@FindTempCalculationByIdTask', [$id]);
        
        $previousBalance = round(($transaction->type == 'debit') ? $transaction->balance - $transaction->amount : $transaction->balance + $transaction->amount, 2);
        $newBalance = round(($transaction->type == 'debit') ? $previousBalance + $amount : $previousBalance - $amount, 2);
        
        $updateTnxRequest = [
            'amount' => $amount,
            'balance' => $newBalance,
        ];
        
        \DB::beginTransaction();
        
        Apiato::call('TempCalculation@UpdateTempCalculationTask', [$id, $updateTnxRequest]);

        $tnxs = Apiato::call('TempCalculation@GetTnxsFromLastUpdateToEndTask', [$id, $transaction->loan_id, $transaction->tnx_date]);
        
        foreach ($tnxs as $key => $tnx)
        {
           if($tnx->tnx_log == 'payment' || $tnx->tnx_log == 'charge')
           {
                $newBalance = $this->updateBalance($tnx, $newBalance, $tnx->amount);
           }
           
           // get updated new balance
           else if($tnx->tnx_log == 'interest')
           {
                $startDate = new Carbon($tnx->tnx_date);
                $endDate = new Carbon($startDate);
                $startDate->startOfMonth();
                $endDate->endOfMonth();
                
                $lastMonthTnxs = Apiato::call('TempCalculation@FindLastMonthtnxsTask', [$tnx->loan_id, $startDate, $endDate, null, 'interest']);

                $monthlyInterest = Apiato::call('TempCalculation@CalculateLastMonthInterestTask', [['lastMonthTnxs' => $lastMonthTnxs]]);                

                $newBalance = $this->updateBalance($tnx, $newBalance, $monthlyInterest['interest']);
            }
           
           else
            {
               throw new InternalErrorException();
            }
        }                
        \DB::commit();
        return $newBalance;
    }
    
    protected function updateBalance($tnx, $newBalance, $amount)
    {
        $currentBalance = round(($tnx->type == 'debit') ? $newBalance + $amount : $newBalance - $amount, 2);

        $updateRequest = [
            'amount' => $amount,
            'balance' => $currentBalance,
        ];

        Apiato::call('TempCalculation@UpdateTempCalculationTask', [$tnx->id, $updateRequest]);
        
        return $currentBalance;
    }

}
