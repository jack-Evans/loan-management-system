<?php

namespace App\Containers\TempCalculation\Tasks;

use App\Containers\TempCalculation\Data\Repositories\TempCalculationRepository;
use App\Ship\Exceptions\DeleteResourceFailedException;
use App\Ship\Parents\Tasks\Task;
use Exception;
use App\Containers\TempCalculation\Models\TempCalculation;
use App\Containers\TempCalculation\Models\TempPayment;
class TruncateTempCalculationTask extends Task
{

    protected $repository;

    public function __construct(TempCalculationRepository $repository)
    {
        $this->repository = $repository;
    }

    public function run($id = null)
    {
        try {
            TempPayment::truncate();
            return TempCalculation::truncate();
        }
        catch (Exception $exception) {
            throw new DeleteResourceFailedException();
        }
    }
}
