<?php

namespace App\Containers\TempCalculation\Tasks;

use App\Containers\TempCalculation\Data\Repositories\TempPaymentRepository;
use App\Ship\Parents\Tasks\Task;

class GetMonthlyPaymentPlanTask extends Task
{

    protected $repository;

    public function __construct(TempPaymentRepository $repository)
    {
        $this->repository = $repository;
    }

    public function run($loanid, $date)
    {
        return $this->repository->scopeQuery(function($q) use ($loanid, $date){
            return $q->where('loan_id', '=', $loanid)->where('pay_before', '<', $date);
        })->orderby('pay_before', 'desc')->first();
    }
}
