<?php

namespace App\Containers\TempCalculation\Tasks;

use App\Containers\TempCalculation\Data\Repositories\TempCalculationRepository;
use App\Ship\Exceptions\NotFoundException;
use App\Ship\Parents\Tasks\Task;
use Exception;

class FindLastTempCalculationByLoanIdTask extends Task
{

    protected $repository;

    public function __construct(TempCalculationRepository $repository)
    {
        $this->repository = $repository;
    }

    public function run($loanid)
    {        
        try {
            return $this->repository->orderby('id', 'desc')->findWhere(['loan_id' => $loanid])->first();
        }
        catch (Exception $exception) {
            throw new NotFoundException();
        }
    }
}
