<?php

namespace App\Containers\TempCalculation\Tasks;

use App\Containers\TempCalculation\Data\Repositories\TempCalculationRepository;
use App\Containers\TempCalculation\Data\Repositories\TempPaymentRepository;
use App\Ship\Exceptions\DeleteResourceFailedException;
use App\Ship\Parents\Tasks\Task;
use Exception;

class DeleteTempLoanTask extends Task
{

    protected $repository;
    protected $paymentRepository;

    public function __construct(TempCalculationRepository $repository, TempPaymentRepository $paymentRepository)
    {
        $this->repository = $repository;
        $this->paymentRepository = $paymentRepository;
    }

    public function run($loanid = null)
    {
        try {
            \DB::begintransaction();
            
            $deletePayments = $this->paymentRepository->deleteWhere(['loan_id' => $loanid]);
            $deleteLoan = $this->repository->deleteWhere(['loan_id' => $loanid]);
            
            \DB::commit();
            return $deleteLoan;
        }
        catch (Exception $exception) {
            throw new DeleteResourceFailedException();
        }
    }
}
