<?php

namespace App\Containers\TempCalculation\Tasks;

use App\Containers\TempCalculation\Data\Repositories\TempPaymentRepository;
use App\Ship\Exceptions\CreateResourceFailedException;
use App\Ship\Parents\Tasks\Task;
use Exception;

class CreateTempPaymentTask extends Task
{

    protected $repository;

    public function __construct(TempPaymentRepository $repository)
    {
        $this->repository = $repository;
    }

    public function run(array $data)
    {
        try {
            return $this->repository->create($data);
        }
        catch (Exception $exception) {
            throw new CreateResourceFailedException();
        }
    }
}
