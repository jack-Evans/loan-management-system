<?php

namespace App\Containers\TempCalculation\Tasks;

use App\Containers\TempCalculation\Data\Repositories\TempCalculationRepository;
use App\Ship\Parents\Tasks\Task;

class CalculateTempMonthlyInterestTask extends Task
{

    protected $repository;

    public function __construct(TempCalculationRepository $repository)
    {
        $this->repository = $repository;
    }

    public function run(array $data)
    {                
        $lasttempcalculation = $data['lasttempcalculation'];
        $response = [];
        $interest = 0;

        if($lasttempcalculation->tnx_log == 'payment')
        {
            $beforeInterestRate =  calculateDailyRate($lasttempcalculation->rate, $lasttempcalculation->balance + $lasttempcalculation->amount);
            
            $beforeTnxDate = \Carbon\Carbon::Parse($lasttempcalculation->tnx_date);
            $startOfMonth = \Carbon\Carbon::Parse($lasttempcalculation->tnx_date)->startOfMonth();
            
            $beforeDiffInDays = $startOfMonth->diffInDays($beforeTnxDate);
            
            $beforeInterest = round($beforeDiffInDays * $beforeInterestRate, 2);
            
            
            $afterInterestRate =  calculateDailyRate($lasttempcalculation->rate, $lasttempcalculation->balance);
            
            $afterTnxDate = \Carbon\Carbon::Parse($lasttempcalculation->tnx_date);
            $endOfMonth = \Carbon\Carbon::Parse($lasttempcalculation->tnx_date)->endOfMonth();
            
            $afterDiffInDays = $endOfMonth->diffInDays($afterTnxDate) + 1;
            
            $afterInterest = round($afterDiffInDays * $afterInterestRate, 2);
            
            $interest = round($beforeInterest + $afterInterest, 2);
            $response = [
                'total' => $interest,
                'before' => $beforeInterest,
                'after' => $afterInterest,
                'daysBeforeInterest' => $beforeDiffInDays,
                'daysAfterInterest' => $afterDiffInDays,
            ];
        }
        
        else if($lasttempcalculation->tnx_log == 'loan' || $lasttempcalculation->tnx_log == 'charge'){
            $interestRate =  calculateDailyRate($lasttempcalculation->rate, $lasttempcalculation->balance);

            $tnxDate = \Carbon\Carbon::Parse($lasttempcalculation->tnx_date);
            $endOfMonth = \Carbon\Carbon::Parse($lasttempcalculation->tnx_date)->endOfMonth();

            $diffInDays = $endOfMonth->diffInDays($tnxDate) + 1;

            $interest = round($diffInDays * $interestRate, 2);
            
            $response = [
                'days'      => $diffInDays,
                'interest'  => $interest
            ];
        }
        
        return [
                'response' => $response,
                'interest' => $interest,
                'endOfMonth' => $endOfMonth,
            ];
    }
}