<?php

namespace App\Containers\TempCalculation\Actions;

use App\Ship\Parents\Actions\Action;
use App\Ship\Parents\Requests\Request;
use Apiato\Core\Foundation\Facades\Apiato;

class GetTempPaymentAction extends Action
{
    public function run(Request $request)
    {
        $tempPayment = Apiato::call('TempCalculation@GetPaymentTask', [$request->loanid]);

        return $tempPayment;
    }
}
