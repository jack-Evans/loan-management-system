<?php

namespace App\Containers\TempCalculation\Actions;

use App\Ship\Parents\Actions\Action;
use App\Ship\Parents\Requests\Request;
use Apiato\Core\Foundation\Facades\Apiato;

class FindTempCalculationByIdAction extends Action
{
    public function run(Request $request)
    {
        $tempcalculation = Apiato::call('TempCalculation@GetAllTempCalculationByIdTask', [$request->loanid, $request->date]);
        
        return $tempcalculation;
    }
}
