<?php

namespace App\Containers\TempCalculation\Actions;

use App\Ship\Parents\Actions\Action;
use App\Ship\Parents\Requests\Request;
use Apiato\Core\Foundation\Facades\Apiato;
use App\Containers\TempCalculation\Exceptions\LoanIdAlreadyTakenException;

class CreateTempCalculationAction extends Action
{
    public function run(Request $request)
    {
        $loanExistCheck = Apiato::call('TempCalculation@GetLoanByLoanIdTask', [$request->loanid]);
        
        if($loanExistCheck)
        {
            throw new LoanIdAlreadyTakenException();
        }
        $loanRequest = [
            'loan_id'       => $request->loanid,
            'amount'        => round($request->netloan, 2),
            'type'          => 'debit',
            'balance'       => round($request->netloan, 2),
            'rate'          => $request->rate,
            'default_rate'  => $request->defaultRate,
            'descriptions'  => 'Loan',
            'tnx_log'       => 'loan',
            'duration'      => $request->duration,
            'tnx_date'      => $request->issuedate,
        ];
        
        // Delete previous calculations so there's no mixup between loans
//        Apiato::call('TempCalculation@TruncateTempCalculationTask');
        \DB::beginTransaction();
        $tempcalculation = Apiato::call('TempCalculation@CreateTempCalculationTask', [$loanRequest]);
        Apiato::call('TempCalculation@AddTempPaymentPlanTask', [$loanRequest]);
        \DB::commit();
        // $paymentPlan = Apiato::call('Payment@CreatePaymentPlanTask', [$loan]);
        
//        Apiato::call('TempCalculation@CreateTempPaymentAction', [$tempcalculation]);
        
        return $tempcalculation;
    }
}
