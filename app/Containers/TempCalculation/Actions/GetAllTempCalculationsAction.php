<?php

namespace App\Containers\TempCalculation\Actions;

use App\Ship\Parents\Actions\Action;
use App\Ship\Parents\Requests\Request;
use Apiato\Core\Foundation\Facades\Apiato;

class GetAllTempCalculationsAction extends Action
{
    public function run(Request $request)
    {
        $tempcalculations = Apiato::call('TempCalculation@GetAllTempCalculationsTask', [], ['addRequestCriteria']);

        return $tempcalculations;
    }
}
