<?php

namespace App\Containers\TempCalculation\Actions;

use App\Ship\Parents\Actions\Action;
use App\Ship\Parents\Requests\Request;
use Apiato\Core\Foundation\Facades\Apiato;

class DeleteTempCalculationAction extends Action
{
    public function run(Request $request)
    {
        // to revert back payments
        $transaction = Apiato::call('TempCalculation@FindTempCalculationByIdTask', [$request->id]);
        if($transaction)
        {
            $managePaymentPlan = Apiato::call('TempCalculation@ManagePaymentPlanAction', [$transaction->loan_id, $transaction->tnx_date, $transaction->amount, $operator = 'add']);
        }
        
        return $updateTempCalculation = Apiato::call('TempCalculation@UpdateTempCalculationStatementTask', [$request->id, 0]);
//        return Apiato::call('TempCalculation@DeleteTempCalculationTask', [$request->id]);
    }
}
