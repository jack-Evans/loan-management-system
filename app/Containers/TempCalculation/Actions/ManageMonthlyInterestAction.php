<?php

namespace App\Containers\TempCalculation\Actions;

use App\Ship\Parents\Actions\Action;
use Apiato\Core\Foundation\Facades\Apiato;
use App\Ship\Parents\Requests\Request;
use App\Containers\TempCalculation\Exceptions\ResourceNotFoundException;

class ManageMonthlyInterestAction extends Action
{
    public function run(Request $request)
    {                       
        $lasttempcalculation = Apiato::call('TempCalculation@FindLastTempCalculationByLoanIdTask', [$request->loanid]);
        
        if(!$lasttempcalculation)
        {
            throw new ResourceNotFoundException;
        }
        
        $tnx_month =  date('m', strtotime($lasttempcalculation->tnx_date));
        $new_month =  date('m', strtotime($request->date));

        if($tnx_month < $new_month)
        {
            if($lasttempcalculation->tnx_log != 'interest' || $tnx_month+1 < $new_month)
            {
                $addInterest = Apiato::call('TempCalculation@AddMonthlyInterestAction', [$request]);
                Apiato::call('TempCalculation@ManageMonthlyInterestAction', [$request]);
            }
        }

        return $lasttempcalculation;
        
    }
}
