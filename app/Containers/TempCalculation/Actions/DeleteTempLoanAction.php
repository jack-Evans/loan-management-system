<?php

namespace App\Containers\TempCalculation\Actions;

use App\Ship\Parents\Actions\Action;
use App\Ship\Parents\Requests\Request;
use Apiato\Core\Foundation\Facades\Apiato;

class DeleteTempLoanAction extends Action
{
    public function run(Request $request)
    {
        return Apiato::call('TempCalculation@DeleteTempLoanTask', [$request->loanid]);
    }
}
