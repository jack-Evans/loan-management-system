<?php

namespace App\Containers\TempCalculation\Actions;

use App\Ship\Parents\Actions\Action;
use App\Ship\Parents\Requests\Request;
use Apiato\Core\Foundation\Facades\Apiato;

class AddChargesAction extends Action
{
    public function run(Request $request)
    {        
        
        $manageMonthlyInterest = Apiato::call('TempCalculation@ManageMonthlyInterestAction', [$request]);
        
        $lastCalculation = Apiato::call('TempCalculation@FindLastTempCalculationByLoanIdTask', [$request->loanid]);

        $chargeAmount = 0;
        
        if($request->type == 'percentage')
        {
            $chargeAmount = round( resolvePercentage($lastCalculation->balance) *  $request->amount, 2);
//            dd($chargeAmount);
        }
        else
        {
            $chargeAmount = round($request->amount, 2);
        }
        
        $chargeRequest = [
            'loan_id'       => $lastCalculation->loan_id,
            'amount'        => round($chargeAmount, 2),
            'type'          => 'debit',
            'balance'       => round($lastCalculation->balance + $chargeAmount, 2),
            'rate'          => $lastCalculation->rate,
            'default_rate'  => $lastCalculation->default_rate,
            'duration'      => $lastCalculation->duration,
            'descriptions'  => $request->description,
            'tnx_log'       => 'charge',
            'tnx_date'      => $request->date,
        ];
        
        $tempcalculation = Apiato::call('TempCalculation@CreateTempCalculationTask', [$chargeRequest]);

        return $tempcalculation;
    }
}
