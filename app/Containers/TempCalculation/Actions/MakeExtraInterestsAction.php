<?php

namespace App\Containers\TempCalculation\Actions;

use App\Ship\Parents\Actions\Action;
use App\Ship\Parents\Requests\Request;
use Apiato\Core\Foundation\Facades\Apiato;
use Carbon\Carbon;

class MakeExtraInterestsAction extends Action
{
    public function run(Request $request)
    {
        $lastTempCalculation = Apiato::call('TempCalculation@GetLastTranasactionByDateTask', [$request->loanid, $request->date]);

        $extraInterests = [];
        if($lastTempCalculation && $request->date != null)
        {
        // check if date is a future date. if last transaction was interest, then no need to calculate that month's interest.
            if($request->date > $lastTempCalculation->tnx_date && $lastTempCalculation->tnx_log == 'interest')
            {
                $interestCaseArr = [
                    'tnx_date'  => $lastTempCalculation->tnx_date,
                    'balance'   => $lastTempCalculation->balance,
                    'rate'      => $lastTempCalculation->rate,
                    'loan_id'      => $lastTempCalculation->loan_id,
                ];
                return $this->getInCaseOfInterest($request->date, $interestCaseArr);
            }
            
            else if($lastTempCalculation->tnx_log == 'interest')
            {
                return $extraInterests;
            }
            else
            {
                // here we will calculate current month's interest according transactions in that month then upcoming month's interest.
                return $this->getInCaseOfOtherThanInterest($request->date, $lastTempCalculation);
            }
        }
        return $extraInterests;
    }
    
    public function getInCaseOfInterest($requestUptoDate, $interestCaseArr)
    {
        $tnxDate        = new Carbon($interestCaseArr['tnx_date']);
        $currentDate    = new Carbon($requestUptoDate);    

        $diffInDays = $tnxDate->diffInDays($currentDate);

        $balance = $interestCaseArr['balance'];
        $rate = $interestCaseArr['rate'];

        do{
            $tnxDate->startOfMonth();
            $tnxDate->addMonth(1);
            $tnxDate->endOfMonth();

            $tnxDateOtherObj = new Carbon($tnxDate);
            $daysInMonth = $tnxDate->daysInMonth;

            if($diffInDays >= $daysInMonth)
            {                    
                $dailyInterest =  calculateDailyRate($rate, $balance);

                $interest = round($dailyInterest * $daysInMonth, 2);

                $balance = round($balance + $interest, 2);
                $diffInDays = $diffInDays - $daysInMonth;

                $extraInterests[] = [
                    'loan_id'       => $interestCaseArr['loan_id'],
                    'tnx_date'      => $tnxDateOtherObj,
                    'amount'        => $interest,
                    'balance'       => $balance,
                    'descriptions'  => 'Upcoming days interest',
                ];

            }
            else
            {
                $dailyInterest =  calculateDailyRate($rate, $balance);

                $interest = round($dailyInterest * $diffInDays, 2);
                $balance = round($balance + $interest, 2);

                $diffInDays = 0;

                $tnxDateOtherObj = new Carbon($requestUptoDate);
                $extraInterests[] = [
                    'loan_id'       => $interestCaseArr['loan_id'],
                    'tnx_date'      => $tnxDateOtherObj,
                    'amount'        => $interest,
                    'balance'       => $balance,
                    'descriptions'  => 'Upcoming days interest',
                ];
            }
        } while ($diffInDays > 0);

        return $extraInterests;
    }
    
    public function getInCaseOfOtherThanInterest($requestUptoDate, $lastTempCalculation)
    {
        
        $tnxDate        = new Carbon($lastTempCalculation->tnx_date);
        $tnxDate->startOfMonth();

        $currentDate    = new Carbon($requestUptoDate);    

        $diffInDays = $tnxDate->diffInDays($currentDate);

        $daysInMonth = $tnxDate->daysInMonth;


        // first add current month's interest according to transactions in that month.
        $interest = $this->getCurrentMonthInterest($requestUptoDate, $lastTempCalculation);

        $balance = $interest['balance'];
        $extraInterests[] = [
                'loan_id'       => $lastTempCalculation->loan_id,
                'tnx_date'      => $interest['interest']['endOfMonth'],
                'amount'        => $interest['interest']['interest'],
                'balance'       => $balance,
                'descriptions'  => 'Upcoming days interest',
            ];
        
        // then if we have to calculate more than current month
        if($diffInDays >= $daysInMonth)
        {
            $previousEndOfMonth = new Carbon($interest['interest']['endOfMonth']);
            $previousEndOfMonth->subDay(1);
            
            $interestCaseArr = [
                    'tnx_date'      => $previousEndOfMonth,
                    'balance'       => $balance,
                    'rate'          => $lastTempCalculation->rate,
                    'loan_id'       => $lastTempCalculation->loan_id,
                ];
            
            $withoutTnxInterests = $this->getInCaseOfInterest($requestUptoDate, $interestCaseArr);
            foreach ($withoutTnxInterests as $key => $tnx)
            {
                $extraInterests[] = [
                    'loan_id'       => $tnx['loan_id'],
                    'tnx_date'      => $tnx['tnx_date'],
                    'amount'        => $tnx['amount'],
                    'balance'       => $tnx['balance'],
                    'descriptions'  => 'Upcoming days interest',
                ];
            }
        }
    return $extraInterests;
    }
    
    public function getCurrentMonthInterest($requestUptoDate, $lastTempCalculation)
    {
        $tnxStartDate           = new Carbon($lastTempCalculation->tnx_date);
        $tnxEndDate             = new Carbon($lastTempCalculation->tnx_date);
        
        $tnxStartDate->startOfMonth();
        $tnxEndDate->endOfMonth();
        
        if($requestUptoDate > $tnxEndDate)
        {
            $uptoDate = $tnxEndDate;
        }
        else
        {
            $uptoDate = new Carbon($requestUptoDate);
        }
        
        $lastMonthTnxs = Apiato::call('TempCalculation@FindLastMonthtnxsTask', [$lastTempCalculation->loan_id, $tnxStartDate, $uptoDate]);
//    dd($lastMonthTnxs);
        $previousBalance = round(($lastMonthTnxs[0]->type == 'debit') ? $lastMonthTnxs[0]->balance - $lastMonthTnxs[0]->amount : $lastMonthTnxs[0]->balance + $lastMonthTnxs[0]->amount, 2);

        $interest = Apiato::call('TempCalculation@CalculateLastMonthInterestTask', [['lastMonthTnxs' => $lastMonthTnxs], $uptoDate]);
        
        $balance = round($lastTempCalculation->balance + $interest['interest'], 2);
        
        return ['interest' => $interest, 'balance' => $balance];
    }
    
}