<?php

namespace App\Containers\TempCalculation\Actions;

use App\Ship\Parents\Actions\Action;
use Apiato\Core\Foundation\Facades\Apiato;
use Carbon\Carbon;
class CreateTempPaymentAction extends Action
{
    public function run($tempcalculation) 
    {
        $tnx_date = new Carbon($tempcalculation->tnx_date);
        
        $amount = round(resolvePercentage($tempcalculation->balance) * $tempcalculation->rate, 2);
        
        for($i=1; $i <= $tempcalculation->duration; $i++)
        {
            $paymentRequest = [
                'loan_id'           => $tempcalculation->loan_id,
                'amount'            => $amount,
                'current_balance'   => $tempcalculation->balance,
                'status'            => 'pending',
                'pay_before'        => $tnx_date->addMonth(1),
            ];
            
            try {

                \DB::beginTransaction();
                Apiato::call('TempCalculation@CreateTempPaymentTask', [$paymentRequest]);
                \DB::commit();

            }
            catch (Exception $exception) {
                    \DB::rollback();
                    throw new CreateResourceFailedException();
            }
        }
        return $tempcalculation;
    }
}
