<?php

namespace App\Containers\TempCalculation\Actions;

use App\Ship\Parents\Actions\Action;
use App\Ship\Parents\Requests\Request;
use Apiato\Core\Foundation\Facades\Apiato;
use App\Ship\Exceptions\InternalErrorException;

class UpdateChargeAction extends Action
{
    public function run(Request $request)
    {
        $amount = 0;
        if($request->type == 'fixed')
        {
            $amount = $request->amount;            
        }
        else if($request->type == 'percentage')
        {
            $transaction = Apiato::call('TempCalculation@FindTempCalculationByIdTask', [$request->id]);
        
            $previousBalance = round($transaction->balance - $transaction->amount, 2);

            $amount = round(resolvePercentage($previousBalance) * $request->amount, 2);
            
        }

        else
        {
            throw new InternalErrorException();
        }

        $updateTempCalculation = Apiato::call('TempCalculation@UpdateTempCalculationStatementTask', [$request->id, $amount]);

        return $updateTempCalculation;
    }
}
