<?php

namespace App\Containers\TempCalculation\Actions;

use App\Ship\Parents\Actions\Action;
use App\Ship\Parents\Requests\Request;
use Apiato\Core\Foundation\Facades\Apiato;
use App\Containers\TempCalculation\Exceptions\DateShouldBeGreaterException;
use Carbon\Carbon;

class CurrentBalanceAction extends Action
{
    public function run(Request $request)
    {        
        
        $lastCalculation = Apiato::call('TempCalculation@GetLastTranasactionByDateTask', [$request->loanid, $request->date]);
        
        
        if(!$lastCalculation)
        {
            throw new DateShouldBeGreaterException();
        }        

        $currentBalance = 0;
        // check if date is a future date.
        if($request->date > $lastCalculation->tnx_date)
        {
            $extraInterests = Apiato::call('TempCalculation@MakeExtraInterestsAction', [$request]);
            
            $currentBalance = $lastCalculation->balance;
            
            foreach($extraInterests as $key => $interest)
            {
                $currentBalance = round($currentBalance + $interest['amount'], 2); 
            }
        }            
        else
        {

            $tnxDate = new Carbon($lastCalculation->tnx_date);
            $uptoDate = new Carbon($request->date);
            
            $tnxDate->startOfMonth();            
            $lastMonthTnxs = Apiato::call('TempCalculation@FindLastMonthtnxsTask', [$lastCalculation->loan_id, $tnxDate, $uptoDate]);
            $interest = Apiato::call('TempCalculation@CalculateLastMonthInterestTask', [['lastMonthTnxs' => $lastMonthTnxs], $uptoDate]);
            
            $previousBalance = round(($lastCalculation->tnx_log == 'interest') ? $lastCalculation->balance - $lastCalculation->amount : $lastCalculation->balance, 2);

            $currentBalance = round($interest['interest'] + $previousBalance, 2);           
        }
        return $currentBalance;
    }
}
