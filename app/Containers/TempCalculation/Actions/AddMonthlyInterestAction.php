<?php

namespace App\Containers\TempCalculation\Actions;

use App\Ship\Parents\Actions\Action;
use Apiato\Core\Foundation\Facades\Apiato;
use App\Ship\Parents\Requests\Request;
use Carbon\Carbon;

class AddMonthlyInterestAction extends Action
{
    public function run(Request $request)
    {                       
        $lasttempcalculation = Apiato::call('TempCalculation@FindLastTempCalculationByLoanIdTask', [$request->loanid]);
        
        
        if(!$lasttempcalculation)
        {
            throw new ResourceNotFoundException;
        }
        
        $startDate = new Carbon($lasttempcalculation->tnx_date);
        $endDate = new Carbon($startDate);
        $startDate->startOfMonth();
        $endDate->endOfMonth();
            
        if($lasttempcalculation->tnx_log == 'interest')
        {
            $calculateMonthlyInterest['interest'] = 0;
            
            $endDate = new Carbon($startDate->startOfMonth()->addMonths(1));
            $endDate->endOfMonth();

            $diffInDays = $startDate->diffInDays($endDate) + 1;
            $dailyInterest =  calculateDailyRate($lasttempcalculation->rate, $lasttempcalculation->balance);     
            
            $calculateMonthlyInterest['interest'] = round($diffInDays * $dailyInterest, 2);
            $calculateMonthlyInterest['endOfMonth'] = $endDate;
        }
        else
        {            
//            $interestCheck = Apiato::call('TempCalculation@FindLastMonthtnxsTask', [$startDate, $endDate, 'interest']);
//            if(count($interestCheck) > 0)
//            {
//                throw new DailyInterestAlreadyCreatedException;
//            }

            $lastMonthTnxs = Apiato::call('TempCalculation@FindLastMonthtnxsTask', [$lasttempcalculation->loan_id, $startDate, $endDate]);

            $calculateMonthlyInterest = Apiato::call('TempCalculation@CalculateLastMonthInterestTask', [['lastMonthTnxs' => $lastMonthTnxs]]);
        }

        if($calculateMonthlyInterest['interest'] != 0)
        {
            $loanRequest = [
                'loan_id'       => $lasttempcalculation->loan_id,
                'amount'        => $calculateMonthlyInterest['interest'],
                'type'          => 'debit',
                'balance'       => round($lasttempcalculation->balance + $calculateMonthlyInterest['interest'], 2),
                'rate'          => $lasttempcalculation->rate,               
                'default_rate'  => $lasttempcalculation->default_rate,               
                'duration'      => $lasttempcalculation->duration,
                'descriptions'  => 'Monthly interest',
                'tnx_log'       => 'interest',
                'tnx_date'      => $calculateMonthlyInterest['endOfMonth'],
            ];

            $tempcalculation = Apiato::call('TempCalculation@CreateTempCalculationTask', [$loanRequest]);
        }

        return $calculateMonthlyInterest['interest'];
        
    }
}
