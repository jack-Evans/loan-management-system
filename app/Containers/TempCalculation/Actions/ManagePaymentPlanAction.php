<?php

namespace App\Containers\TempCalculation\Actions;

use App\Ship\Parents\Actions\Action;
use Apiato\Core\Foundation\Facades\Apiato;
use Carbon\Carbon;

class ManagePaymentPlanAction extends Action
{
    public function run($loanid, $date, $amount, $operator = 'sub')
    {               
        $paymentDate = new Carbon($date);
        
        $paymentDate->addMonth(1);
   
        $paymentPlan = Apiato::call('TempCalculation@GetMonthlyPaymentPlanTask', [$loanid, $paymentDate]);        
        
        if($paymentPlan)
        {            
            $newPayment = $this->$operator($paymentPlan->amount, $amount);
            
            if($newPayment <= 0)
            {
                $data = [
                    'status' => 'paid',
                    'amount' => $newPayment,
                ];
            }
            else
            {
                $data = [
                    'status' => 'pending',
                    'amount' => $newPayment,
                ];
            }
            
            return Apiato::call('TempCalculation@UpdateTempPaymentTask', [$paymentPlan->id, $data]);
        }
        
        return [];
    }
    
    public function sub($amount1, $amount2)
    {
        return $amount1 - $amount2;
    }
    
    public function add($amount1, $amount2)
    {
        return $amount1 + $amount2;
    }
}
