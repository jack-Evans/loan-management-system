<?php

namespace App\Containers\TempCalculation\Actions;

use App\Ship\Parents\Actions\Action;
use Apiato\Core\Foundation\Facades\Apiato;
use App\Ship\Parents\Requests\Request;

class PayTempCalculationAction extends Action
{
    public function run(Request $request)
    {        
        $manageMonthlyInterest = Apiato::call('TempCalculation@ManageMonthlyInterestAction', [$request]);

        $lasttempcalculation = Apiato::call('TempCalculation@FindLastTempCalculationByLoanIdTask', [$request->loanid]);
        
        $loanRequest = [
            'loan_id'       => $lasttempcalculation->loan_id,
            'amount'        => $request->amount,
            'type'          => 'credit',
            'balance'       => round($lasttempcalculation->balance - $request->amount, 2),
            'rate'          => $lasttempcalculation->rate,
            'default_rate'  => $lasttempcalculation->default_rate,
            'duration'      => $lasttempcalculation->duration,
            'descriptions'  => $request->description,
            'tnx_log'       => 'payment',
            'tnx_date'      => $request->date,
        ];                               
                
//        $managePaymentPlan = Apiato::call('TempCalculation@ManagePaymentPlanAction', [$request->loanid, $request->date, $request->amount, $operator = 'sub']);
        
        $tempcalculation = Apiato::call('TempCalculation@CreateTempCalculationTask', [$loanRequest]);
                
        \DB::commit();
        return $tempcalculation;
        
        
    }
}
