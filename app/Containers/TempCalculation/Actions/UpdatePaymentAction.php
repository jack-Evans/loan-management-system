<?php

namespace App\Containers\TempCalculation\Actions;

use App\Ship\Parents\Actions\Action;
use App\Ship\Parents\Requests\Request;
use Apiato\Core\Foundation\Facades\Apiato;

class UpdatePaymentAction extends Action
{
    public function run(Request $request)
    {
        $updateTempCalculation = Apiato::call('TempCalculation@UpdateTempCalculationStatementTask', [$request->id, $request->amount]);

        return $updateTempCalculation;
    }
}
