<?php

namespace App\Classes;

use App\Containers\Payment\Exceptions\HTTPPreConditionFailedException;
use PhpOffice\PhpSpreadsheet\Reader\Csv;
use PhpOffice\PhpSpreadsheet\Reader\Xlsx;

/**
 * Class Excel
 */
class Excel
{
	public function initializeExcel($request){
		ini_set('memory_limit', '1000M');
		$extensions = array("xls","xlsx","csv");
		$result = array($request->file('file')->getClientOriginalExtension());

		if($_FILES['file']['error'] == 1)
			throw new HTTPPreConditionFailedException("Your file exceeds max file size limit.");
		if($_FILES['file']['error'] != 0)
			throw new HTTPPreConditionFailedException("Error loading your file.");
		
		if(in_array($result[0],$extensions)){
		
		    $arr_file = explode('.', $_FILES['file']['name']);
		    $extension = end($arr_file);

		    if('csv' == $extension) {
		        $reader = new Csv();
		    } else {
		        $reader = new Xlsx();
		    }
			// $reader->setReadDataOnly(true);
		    $spreadsheet = $reader->load($_FILES['file']['tmp_name']);

		    return $spreadsheet;
	    }

    	return false;
    }
}