<?php

namespace App\Classes;
use TCPDF;

class MonthStatementPdf {
    public function generateBorrowerHtml($pdf, $statement, $fileName, $fileOptions = 'FI', $statements = null, $loan = null){
        // create new PDF document
        // $pdf = new \App\Containers\Calculator\MyClasses\PdfSetHeader(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);
        // $pdf = new MonthStatementPdf(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);
        // set document information

        $pdf->SetCreator(PDF_CREATOR);
        $pdf->SetAuthor('Salman Saeed');
        $pdf->SetTitle($statement->statement_month." statement");
        // $pdf->SetSubject('Borrower Statement');
        $pdf->SetKeywords('Borrower, Statement');

        $pdf->setPrintHeader(false);
        $pdf->setPrintFooter(false);
        // set default header data
        $pdf->SetHeaderData(PDF_HEADER_LOGO, PDF_HEADER_LOGO_WIDTH, PDF_HEADER_TITLE, PDF_HEADER_STRING);

        // set header and footer fonts
        $pdf->setHeaderFont(Array(PDF_FONT_NAME_MAIN, '', PDF_FONT_SIZE_MAIN));
        $pdf->setFooterFont(Array(PDF_FONT_NAME_DATA, '', PDF_FONT_SIZE_DATA));

        // set default monospaced font
        $pdf->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED);

        // set margins
        $pdf->SetMargins(PDF_MARGIN_LEFT, PDF_MARGIN_TOP, PDF_MARGIN_RIGHT);
        $pdf->SetHeaderMargin(PDF_MARGIN_HEADER);
        $pdf->SetFooterMargin(PDF_MARGIN_FOOTER);

        // set auto page breaks
        $pdf->SetAutoPageBreak(TRUE, PDF_MARGIN_BOTTOM);

        // set image scale factor
        $pdf->setImageScale(PDF_IMAGE_SCALE_RATIO);

        // set some language-dependent strings (optional)
        if (@file_exists(dirname(__FILE__).'/lang/eng.php')) {
            require_once(dirname(__FILE__).'/lang/eng.php');
            $pdf->setLanguageArray($l);
        }

        // ---------------------------------------------------------

        // set font
        $pdf->SetFont('dejavusans', '', 10);

        // add a page
        $pdf->AddPage('P', 'A4');

        // $pdf->Image(ulr('images/kuflink_logo-white2.png'));
        // $url = url('');
        $pdf->SetXY(8, 10);
        $pdf->Image('images/pdf_logo.png', '', '', 65, 20, '', '', 'T', false, 300, '', false, false, 1, false, false, false);
        // create columns content
        $left_column = '<br><br><br><br><br><br><Strong>'.$loan->loanid.'<br>1 Eldertree Way<br>Mitcham<br>CE4 1AJ</Strong>';

        $right_column = '<p>Kuflink,<br>21 West Street,<br>Gravesend, Kent.<br>DA11 0BF</p>
            <p>hello@kuflink.com
            <br>01474334488<br>kuflink.com</p><br><br><br><br><br>';

        $y = $pdf->getY();
        $pdf->writeHTMLCell(140, '', '', $y, $left_column, 0, 0, 0, true, 'J', true);
        $pdf->writeHTMLCell(40, '', '', '', $right_column, 0, 1, 0, true, 'J', true);
        

        $headingHtml = "<h1>Borrowers Statement</h1> 
            <h5 style='text-align: center;margin: 0;'><strong>".\Carbon\Carbon::parse($statement->end_date)->format('d/m/Y')."</strong></h5>
            <br><br>";
        $pdf->writeHTML($headingHtml, true, false, true, false, 'C');

        $accDetailsHtml = "<h4>Account Details</h4><br>";
        $pdf->SetFillColor(82, 79, 132);
        $pdf->SetTextColor(255, 255, 255);
        $pdf->writeHTMLCell(180, '', '', '', $accDetailsHtml, 1, 1, 1, true, 'R', true);

        $accDetailsHtml = "<p>Opening Balance:</p><br>";
        $pdf->SetFillColor(255, 255, 255);
        $pdf->SetTextColor(0, 0, 0);
        $pdf->writeHTMLCell(90, '', '', '', $accDetailsHtml, 1, 0, 1, true, 'L', true);

        $accDetailsHtml = "<p>".number_format($statement->opening_balance, 2)."</p><br>";
        $pdf->writeHTMLCell(90, '', '', '', $accDetailsHtml, 1, 1, 1, true, 'C', true);

        $emptyHtml = "<br/><br/>";
        $pdf->SetFillColor(235, 243, 246);
        $pdf->writeHTMLCell(180, '', '', '', $emptyHtml, 1, 1, 1, true, 'C', true);

        $accDetailsHtml = "<p>Interest Retained:</p><br>";
        $pdf->SetFillColor(255, 255, 255);
        $pdf->writeHTMLCell(90, '', '', '', $accDetailsHtml, 1, 0, 1, true, 'L', true);

        $accDetailsHtml = "<p>".number_format($statement->interest_charged, 2)."</p><br>";
        $pdf->SetFillColor(255, 255, 255);
        $pdf->writeHTMLCell(90, '', '', '', $accDetailsHtml, 1, 1, 1, true, 'C', true);

        $emptyHtml = "<br/><br/>";
        $pdf->SetFillColor(235, 243, 246);
        $pdf->writeHTMLCell(180, '', '', '', $emptyHtml, 1, 1, 1, true, 'C', true);

        $accDetailsHtml = "<p>Current Interest Rate:</p><br>";
        $pdf->SetFillColor(255, 255, 255);
        $pdf->writeHTMLCell(90, '', '', '', $accDetailsHtml, 1, 0, 1, true, 'L', true);

        $accDetailsHtml = "<p>$statement->current_interest_rate %</p><br>";
        $pdf->writeHTMLCell(90, '', '', '', $accDetailsHtml, 1, 1, 1, true, 'C', true);

        $emptyHtml = "<br/><br/><br/><br/>";
        $pdf->writeHTMLCell(180, '', '', '', $emptyHtml, 0, 1, 0, true, 'C', true);

        $accDetailsHtml = "<h4>Interest Due Date:</h4><br>";
        $pdf->SetFillColor(82, 79, 132);
        $pdf->SetTextColor(255, 255, 255);
        $pdf->writeHTMLCell(90, '', '', '', $accDetailsHtml, 1, 0, 1, true, 'R', true);

        $accDetailsHtml = "<h4>Closing Balance:</h4><br>";
        $pdf->writeHTMLCell(90, '', '', '', $accDetailsHtml, 1, 1, 1, true, 'R', true);


        $accDetailsHtml = "<p>$statement->interest_due_date</p><br>";
        $pdf->SetFillColor(255, 255, 255);
        $pdf->SetTextColor(0, 0, 0);
        $pdf->writeHTMLCell(90, '', '', '', $accDetailsHtml, 1, 0, 1, true, 'R', true);

        $accDetailsHtml = "<p>".number_format($statement->closing_balance, 2)."</p><br>";
        $pdf->writeHTMLCell(90, '', '', '', $accDetailsHtml, 1, 1, 1, true, 'R', true);

        $accDetailsHtml = "<br><br><br>";
        $pdf->writeHTMLCell(90, '', '', '', $accDetailsHtml, 0, 0, 0, true, 'R', true);

        $accDetailsHtml = "<p>Loan End Date: $statement->loan_end_date</p><br>";
        $pdf->writeHTMLCell(90, '', '', '', $accDetailsHtml, 1, 1, 1, true, 'R', true);

        $emptyHtml = "<br/><br/><br/><br/><br/><br/><br/><br/><br/>";
        $pdf->writeHTMLCell(180, '', '', '', $emptyHtml, 0, 1, 0, true, 'C', true);

        $html = "<p>Register Office: 21 West Street, Gravesend, Kent, DA110BF<br>Kuflink Group PLC (Company no. 09084634)<br>Kuflink Bridging Ltd (Company no. 7889226) | FCA Number:723495<br>Kuflink Ltd (Company no. 8460508) | FCA Number:665279 <span style='float: right;'></span> </p>";

        // output the HTML content
        $pdf->writeHTML($html, true, false, true, false, '');


        $pdf->AddPage('L', 'A4');
        if($statements){
            $html = '<table border="0" cellpadding="2">
                <tr>
                    <th colspan="5"><strong>'.$loan->loanid.'</strong></th>
                    <th><strong>'.$loan->interest->rate.' %</strong></th>
                    <th>'.$loan->interest->default_rate.' %</th>
                    <th><strong>'.$loan->interest->duration.' mths</strong></th>
                </tr>
            </table>';
            $pdf->writeHTML($html, true, false, true, false, '');                
            
            $html = '
            <table border="1" cellpadding="3">
                <tr>
                    <th>Date</th>
                    <th>Debit</th>
                    <th>Credit</th>
                    <th>Balance</th>
                    <th colspan="3">Description</th>
                </tr>';

            foreach($statements as $key => $statement) {
                $statement = (object)$statement;
                if(array_key_exists('date', $statement->created_at))
                    $date = \Carbon\Carbon::parse($statement->created_at->date)->format('d M Y');
                else
                    $date = $statement->created_at->format('d M Y');
                $html .= '<tr>
                    <td>'. $date .'</td>
                    <td>'. number_format($statement->debit, 2) .'</td>
                    <td>'. number_format($statement->credit, 2) .'</td>
                    <td>'. number_format($statement->balance, 2) .'</td>
                    <td colspan="3">'. $statement->comments .'</td>
                </tr>';
            }

            $html .= '</table>';
            $pdf->writeHTML($html, true, false, true, false, '');
        }
        
        // reset pointer to the last page
        $pdf->lastPage();

        // ---------------------------------------------------------
        // ob_end_clean();
        // 'D' for download
        // $filename = 'asdf.pdf';
        // $pdf->Output(base_path("public/month-statements")."/".$fileName.".pdf", 'FI');
        $pdf->Output(public_path().'/'.$fileName, $fileOptions);
        // $pdf->Output($statement->statement_month." Statement", 'D');
        // $pdf->Output(" Statement", 'I');

        //============================================================+
        // END OF FILE
        //============================================================+
    }
}
?>