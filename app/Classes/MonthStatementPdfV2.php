<?php

namespace App\Classes;
// use TCPDF;
use Carbon\Carbon;

class MonthStatementPdfV2 {
    public function generateBorrowerHtml($pdf, $statement, $fileName, $fileOptions = 'FI', $statements = null, $loan = null, $monthTnxs = false){
        ini_set('memory_limit', '1000M');
        // create new PDF document
        // $pdf = new \App\Containers\Calculator\MyClasses\PdfSetHeader(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);
        // $pdf = new MonthStatementPdf(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);
        // set document information
        
        $stmtEndDateCarbon = Carbon::parse(str_replace('/', '-', $statement->end_date));
        $stmtStartDateCarbon = Carbon::parse(str_replace('/', '-', $statement->start_date));
        $monthStatements = collect($statements)->filter(function ($item) use ($stmtEndDateCarbon, $stmtStartDateCarbon) {
            $createdAt = Carbon::parse($item->created_at->date);
            return ($createdAt->gte($stmtStartDateCarbon)) && ($createdAt->lte($stmtEndDateCarbon));
        });

        $pdf->SetCreator(PDF_CREATOR);
        $pdf->SetAuthor('Salman Saeed');
        $pdf->SetTitle($statement->statement_month." statement");
        // $pdf->SetSubject('Borrower Statement');
        $pdf->SetKeywords('Borrower, Statement');

        // $pdf->setPrintHeader(false);
        // $pdf->setPrintFooter(false);
        // set default header data
        // $pdf->SetHeaderData(PDF_HEADER_LOGO, PDF_HEADER_LOGO_WIDTH, PDF_HEADER_TITLE, PDF_HEADER_STRING);
        $pdf->SetHeaderData(PDF_HEADER_LOGO, PDF_HEADER_LOGO_WIDTH, PDF_HEADER_TITLE.' 001', PDF_HEADER_STRING, array(0,64,255), array(0,64,128));
        $pdf->setFooterData(array(0,64,0), array(0,64,128));

        // set header and footer fonts
        $pdf->setHeaderFont(Array(PDF_FONT_NAME_MAIN, '', PDF_FONT_SIZE_MAIN));
        $pdf->setFooterFont(Array(PDF_FONT_NAME_DATA, '', PDF_FONT_SIZE_DATA));

        // set default monospaced font
        $pdf->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED);

        // set margins
        // $pdf->SetMargins(FPDF_MARGIN_LEFT, PDF_MARGIN_TOP, PDF_MARGIN_RIGHT);
        $pdf->SetMargins(11.5, 50, 0);
        $pdf->SetHeaderMargin(PDF_MARGIN_HEADER);
        $pdf->SetFooterMargin(PDF_MARGIN_FOOTER);

        // set auto page breaks
        $pdf->SetAutoPageBreak(TRUE, PDF_MARGIN_BOTTOM);

        // set image scale factor
        $pdf->setImageScale(PDF_IMAGE_SCALE_RATIO);

        // set some language-dependent strings (optional)
        if (@file_exists(dirname(__FILE__).'/lang/eng.php')) {
            require_once(dirname(__FILE__).'/lang/eng.php');
            $pdf->setLanguageArray($l);
        }

        // ---------------------------------------------------------

        // set font
        $pdf->SetFont('dejavusans', '', 10);

        // add a page
        $pdf->AddPage('P', 'A4');

        // $pdf->SetXY(10, 10);
        // $loanOptions = collect($loan->options)->filter(function ($item) {
        //     if(strpos(strtolower($item->name), 'statement address') === 0)
        //         return $item;
        // });
        // $loanOptions = $loanOptions->sortBy('name');

        $options = collect($loan->options);

        $name = '';
        $firstName = $options->where('name', 'first name')->first();
        if($firstName)
            $name = $options->where('name', 'first name')->first()->value;
        
        $lastName = $options->where('name', 'last name')->first();
        if($lastName)
            $name = $name.' '.$options->where('name', 'last name')->first()->value;
            // $addressInfo = '<br><br><br><br><br><br><br><Strong>'.$options->where('name', 'first name')->first()->value.' '.$options->where('name', 'last name')->first()->value;
        $addressInfo = '<br><br><br><br><br><br><br><span>'.$name;
        // foreach ($loanOptions as $key => $option) {
            // if(is_null($option->value))
                // continue;
            // $addressInfo .= '<br>'.$option->value .',';
        // }
        $addressLine1 = $options->where('name', 'statement address line 1')->first();
        if($addressLine1 && $addressLine1->value)
            $addressInfo .= '<br>'.$addressLine1->value .',';

        $addressLine2 = $options->where('name', 'statement address line 2')->first();
        if($addressLine2 && $addressLine2->value)
            $addressInfo .= '<br>'.$addressLine2->value;

        $addressCity = $options->where('name', 'statement address city')->first();
        if($addressCity && $addressCity->value)
            $addressInfo .= '<br>'.$addressCity->value;

        $addressCounty = $options->where('name', 'statement address county')->first();
        if($addressCounty && $addressCounty->value)
            $addressInfo .= '<br>'.$addressCounty->value;

        $postCode = $options->where('name', 'statement address post code')->first();
        if($postCode && $postCode->value)
            $addressInfo .= '<br>'.$postCode->value;

        $addressInfo .= '</span>';

        $pdf->SetXY(10, 10);
        $y = $pdf->getY();
        $pdf->writeHTMLCell(60, '', '', $y, $addressInfo, 0, 0, 0, true, 'L', true);

        $pdf->SetXY(10, 60);
        $y = $pdf->getY();
        $pdf->writeHTMLCell(162, '', '', $y, $stmtEndDateCarbon->copy()->addDay()->format('d/m/Y'), 0, 1, 0, true, 'R', true);
        

        // $y = $pdf->getY();
        $pdf->SetXY(10, 50);
        $stmtStartDate = $stmtStartDateCarbon->format('d/m/Y');
        $stmtEndDate = $stmtEndDateCarbon->format('d/m/Y');
        $headingHtml = <<<EOF
            <style>
                .color {
                    color: black;
                    font-size: 17px;
                }
                .small {
                    font-size: 13px;
                    margin-top: 2px;
                }
            </style>

            <br><br><br><p class="color">Borrowers Statement<br><span class="small">$stmtStartDate - $stmtEndDate</span> </p>
            <br><br>
EOF;
        // $pdf->writeHTML($headingHtml, true, false, true, false, '');
        $pdf->writeHTML($headingHtml, true, false, true, false, 'C');
        
        // $pdf->SetXY(0, 0);
        
        $issueAt = Carbon::parse($loan->issueat->date)->format('d/m/Y');
        $loanClosingDate = addDecimalMonthsIntoDate($loan->interest->duration, $loan->issueat->date)->copy()->subDay()->format('d/m/Y');
        $rate = number_format($loan->interest->rate, 2).'%';
        $monthlyPmt = calculateMonthlyPMT($loan->interest->rate, $loan->interest->gross_loan);
        $monthlyPmt = number_format($monthlyPmt, 2);

        $openingBalance = ($monthStatements && isset(collect($monthStatements)->first()->balance)) ? number_format(collect($monthStatements)->first()->balance, 2) : '-';
        $closingBalance = ($monthStatements && isset(collect($monthStatements)->last()->balance)) ? number_format(collect($monthStatements)->last()->balance, 2) : '-';
        // $y = $pdf->getY();
        // Loan Details
        $html = <<<EOF
            <!-- EXAMPLE OF CSS STYLE -->
            <style>
                
                td.first {
                    color: #ffffff;
                    font-family: helvetica;
                    font-size: 12px;
                    background-color: #1A6FA8;
                    padding: 5px;
                }
                td {
                    font-size: 11px;
                    border-bottom: 1px solid #009FD3;
                    /* border-left: 1px solid #009FD3; */
                    /* border-radius: 100%; */
                }
                td.border-right {
                    /* border-right: 1px solid #009FD3; */
                }
                td.border-bottom {
                    /* border-bottom: 1px solid #009FD3; */
                }
            </style>

            <table cellpadding="4" cellspacing="1">
             <tr>
              <td class="first border-right" width="201"><b> Loan Details</b></td>
             </tr>
             <tr>
              <td width="200">Loan Reference</td>
              <td width="140" class="border-right">$loan->loanid</td>
             </tr>
             <tr>
              <td width="200">Loan Start Date</td>
              <td width="140" class="border-right">$issueAt</td>
             </tr>
             <tr>
              <td width="200">Loan End Date</td>
              <td width="140" class="border-right">$loanClosingDate</td>
             </tr>
             <tr>
              <td width="200">Interest Rate</td>
              <td width="140" class="border-right">$rate</td>
             </tr>
             <tr>
              <td width="200">Monthly Payment Due</td>
              <td width="140" class="border-right">&#163; $monthlyPmt</td>
             </tr>
             <tr>
              <td width="200">Next Payment Due Date</td>
              <td width="140">$statement->interest_due_date</td>
             </tr>
             <tr>
              <td width="200">Opening Balance</td>
              <td width="140">$openingBalance</td>
             </tr>
             <tr>
              <td width="200" class="border-bottom">Closing Balance</td>
              <td width="140" class="border-right border-bottom">$closingBalance</td>
             </tr>
            </table>
            <p></p> <p></p>
EOF;
        $pdf->writeHTML($html, true, false, true, false, '');

        $monthYear = ($monthTnxs == 1) ? $stmtEndDateCarbon->format('F') : 'All';

        // Month transactions
        $tnxHtml = '
            <style>
                td.first {
                    color: #ffffff;
                    font-family: helvetica;
                    font-size: 12px;
                    background-color: #1A6FA8;
                    padding: 5px;
                }
                td {
                    font-size: 11px;
                    border-bottom: 1px solid #009FD3;
                    /* border-left: 1px solid #009FD3; */
                }
                td.border-right {
                    /* border-right: 1px solid #009FD3; */
                }
                td.border-bottom {
                    /* border-bottom: 1px solid #009FD3; */
                }
            </style>

            <table cellpadding="4" cellspacing="1">
                <tr>
                  <td class="first border-right" width="202"><b> '. $monthYear .' Transactions</b></td>
                </tr>
                <tr>
                    <td width="80" align="left">Date</td>
                    <td width="80" align="right">Debit</td>
                    <td width="80" align="right">Credit</td>
                    <td width="80" align="right">Balance</td>
                    <td width="330" align="left" class="border-right">Description</td>
                </tr>';
                $stmtMonth = $stmtEndDateCarbon->format('m/y');
                foreach($statements as $key => $statement) {
                    $statement = (object)$statement;
                    if(array_key_exists('date', $statement->created_at)){
                        $tnxDateCarbon = Carbon::parse($statement->created_at->date);
                        if($tnxDateCarbon->format('m/y') != $stmtMonth && $monthTnxs == 1)
                            continue;
                        $date = $tnxDateCarbon->format('d M Y');
                    }
                    else{
                        if($statement->created_at->format('m/y') != $stmtMonth && $monthTnxs == 1)
                            continue;
                        $date = $statement->created_at->format('d M Y');
                    }
                    $debit = ($statement->debit) ? number_format($statement->debit, 2) : '';
                    $credit = ($statement->credit) ? number_format($statement->credit, 2) : '';
                    if($key == count($statements)-1)
                        $tnxHtml .= '<tr>
                            <td align="left" class="border-bottom">'. $date .'</td>
                            <td align="right" class="border-bottom">'. $debit .'</td>
                            <td align="right" class="border-bottom">'. $credit .'</td>
                            <td align="right" class="border-bottom">'. number_format($statement->balance, 2) .'</td>
                            <td align="left" class="border-right border-bottom">'. $statement->comments .'</td>
                        </tr>';
                    else
                        $tnxHtml .= '<tr>
                            <td align="left">'. $date .'</td>
                            <td align="right">'. $debit .'</td>
                            <td align="right">'. $credit .'</td>
                            <td align="right">'. number_format($statement->balance, 2) .'</td>
                            <td align="left" class="border-right">'. $statement->comments .'</td>
                        </tr>';
                }

            $tnxHtml .= '</table>
            <p></p> <p></p>';
        $pdf->writeHTML($tnxHtml, true, false, true, false, '');

        $bottomHtml = '
            <style>
                td.first {
                    color: #ffffff;
                    font-family: helvetica;
                    font-size: 12px;
                    background-color: #1A6FA8;
                    padding: 5px;
                }
                td {
                    font-size: 11px;
                    border-bottom: 1px solid #009FD3;
                    /* border-left: 1px solid #009FD3; */
                }
                td.border-right {
                    /* border-right: 1px solid #009FD3; */
                }
                td.border-bottom {
                    /* border-bottom: 1px solid #009FD3; */
                }
            </style>

            <table cellpadding="4" cellspacing="1">
             <tr>
              <td class="first border-right" width="330" align="right"><b>Interest Calculated Date</b></td>
              <td class="first border-right" width="330" align="right"><b>Closing balance</b></td>
             </tr>
             <tr>
              <td width="330" class="border-bottom" align="right">'. Carbon::parse($statement->created_at->date)->format('d/m/Y') .'</td>
              <td width="330" class="border-right border-bottom" align="right">&#163; '. number_format($statement->balance, 2) .'</td>
             </tr>

            </table>
            <p></p> <p></p>';
        $pdf->writeHTML($bottomHtml, true, false, true, false, '');
              /* <tr>
              <th width="330"></th>
              <td width="330" class="border-right border-bottom" align="right">Loan End Date: '. $loanClosingDate .'</td>
             </tr> */

        // $html = "
        //     <style>
        //         p{
        //             font-size: 10px;
        //         }
        //     </style>
        //     <p>Register Office: 21 West Street, Gravesend, Kent, DA110BF<br>Kuflink Group PLC (Company no. 09084634)<br>Kuflink Bridging Ltd (Company no. 7889226) | FCA Number:723495<br>Kuflink Ltd (Company no. 8460508) | FCA Number:665279 <span style='float: right;'></span> </p>";

        // // output the HTML content
        // $pdf->writeHTML($html, true, false, true, false, '');

        
        // reset pointer to the last page
        $pdf->lastPage();

        // ---------------------------------------------------------
        // ob_end_clean();
        // 'D' for download
        // $filename = 'asdf.pdf';
        // $pdf->Output(base_path("public/month-statements")."/".$fileName.".pdf", 'FI');
        $pdf->Output(public_path().'/'.$fileName, $fileOptions);
        // $pdf->Output($statement->statement_month." Statement", 'D');
        // $pdf->Output(" Statement", 'I');

        //============================================================+
        // END OF FILE
        //============================================================+
    }
}
?>