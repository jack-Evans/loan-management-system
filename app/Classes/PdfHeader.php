<?php 

namespace App\Classes;
use TCPDF;

// Extend the TCPDF class to create custom Header and Footer
class PdfHeader extends TCPDF {

    //Page header
    public function Header() {
        // Set font
        $this->SetFont('dejavusans', '', 10);
        $this->SetXY(10, 0);
        $this->Image('images/main_logo.png', '', '', 68, 50, '', '', 'T', false, 300, '', false, false, 1, false, false, false);
        $this->SetXY(10, 10);
        $right_column = '<p>Kuflink,<br>21 West Street,<br>Gravesend, Kent.<br>DA11 0BF</p>
            <p>hello@kuflink.com
            <br>01474334488<br>kuflink.com</p><br><br><br><br><br>';

        $y = $this->getY();
        $this->writeHTMLCell(140, '', '', $y, '', 0, 0, 0, true, 'J', true);
        $this->writeHTMLCell(40, '', '', '', $right_column, 0, 1, 0, true, 'J', true);
    }

    // Page footer
    public function Footer() {
        // Position at 15 mm from bottom
        $this->SetY(-20);
        // Set font
        $this->SetFont('dejavusans', '', 8);

        $html = "
            <p>Register Office: 21 West Street, Gravesend, Kent, DA110BF<br>Kuflink Group PLC (Company no. 09084634)<br>Kuflink Bridging Ltd (Company no. 7889226) | FCA Number:723495<br>Kuflink Ltd (Company no. 8460508) | FCA Number:665279 <span style='float: right;'></span> </p>";

        // output the HTML content
        $this->writeHTML($html, true, false, true, false, '');
        
        $this->SetFont('helvetica', 'I', 8);
        $this->SetY(-8);
        // Page number
        $this->Cell(0, 10, 'Page '.$this->getAliasNumPage().'/'.$this->getAliasNbPages(), 0, false, 'C', 0, '', 0, false, 'T', 'M');
    }
}

?>