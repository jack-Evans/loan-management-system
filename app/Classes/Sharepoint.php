<?php

namespace App\Classes;

use Office365\PHP\Client\Runtime\Auth\AuthenticationContext;
use Office365\PHP\Client\SharePoint\ClientContext;
use Office365\PHP\Client\SharePoint\FileCreationInformation;

/**
 * Class Sharepoint
 */
class Sharepoint
{
	public $url;
	public $ctx;

	public function __construct(){
		$this->url = env('SHAREPOINT_URL');
        $username = env('SHAREPOINT_USER_NAME');
        $password = env('SHAREPOINT_PASSWORD');
        try {
            $authCtx = new AuthenticationContext($this->url);
            $authCtx->acquireTokenForUser($username, $password);
            $this->ctx = new ClientContext($this->url,$authCtx);
        } catch (\Exception $e) {
            return 'Sharepoint authentication failed: '.$e->getMessage();
        }
	}

	public function uploadToSharepoint($filePath, $fileName ){
		// $server = env('SHAREPOINT_URL'); // no ending slash
		$fileUri = "Shared Documents/$fileName";

		$fileCreationInformation = new FileCreationInformation();
		$fileCreationInformation->Content = file_get_contents($filePath);
		$fileCreationInformation->Url = basename($filePath);
		$uploadFile = $this->ctx->getWeb()
		    ->getFolderByServerRelativeUrl(dirname($fileUri))
		    ->getFiles()
		    ->add($fileCreationInformation);
		return $this->ctx->executeQuery();
	}

	public function unlinkFile($fileName)
	{
		$filePath = public_path()."/$fileName";
		return unlink($filePath);
	}

	public function createFolder($folderName = "2001", $parentFolderUrl = "/Documents/Month Statements")
	{
		$parentFolder = $this->ctx->getWeb()->getFolderByServerRelativeUrl($parentFolderUrl);
		$childFolder = $parentFolder->getFolders()->add($folderName);
		$this->ctx->executeQuery();
		// print "Child folder {$childFolder->getProperty("ServerRelativeUrl")} has been created ";
	}

	public static function createZip($archiveName, $fileName, $create)
	{
		$zipFile = public_path()."/".$archiveName.".zip"; // Name of our archive to download
        $zip = new \ZipArchive();
        if($create){
	        $zip->open($zipFile, \ZipArchive::CREATE | \ZipArchive::OVERWRITE);
        }
	    else
	    	$zip->open($zipFile);
        // Adding file: second parameter is what will the path inside of the archive
        // So it will create another folder called "storage/" inside ZIP, and put the file there.
        $zip->addFile(public_path($fileName), $fileName);
        $zip->close();
	}

	// public function getSharepointFiles($listTitle)
	// {
	// 	$web = $this->ctx->getWeb();
	// 	$list = $web->getLists()->getByTitle($listTitle); //init List resource
	// 	$items = $list->getItems();  //prepare a query to retrieve from the 
	// 	$this->ctx->load($items);  //save a query to retrieve list items from the server 
	// 	$this->ctx->executeQuery(); //submit query to SharePoint Online REST service
	// 	foreach( $items->getData() as $item ) {
	// 	    print "Task: '{$item->Title}'\r\n";
	// 	}
	// }
}