<?php

namespace App\Jobs;

use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Apiato\Core\Foundation\Facades\Apiato;

class ProcessMonthStatements implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    /**
     * The number of times the job may be attempted.
     *
     * @var int
     */
    public $tries = 2;

    protected $request;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct($requestDate)
    {
        $this->request = $requestDate;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $request = new \App\Containers\Calculator\UI\WEB\Requests\StoreCalculatorRequest();
        $request->loanId = $this->request['loanId'];
        $request->tagIds = $this->request['tagIds'];
        $request->date = $this->request['date'];
        Apiato::call('Payment@MonthStatementsAction', [$request]);
        
    }
}
