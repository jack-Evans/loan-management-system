<?php

namespace App\Jobs;

use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Apiato\Core\Foundation\Facades\Apiato;

class ProcessImport implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;
    
    /**
     * The number of times the job may be attempted.
     *
     * @var int
     */
    public $tries = 1;

    /**
     * The number of seconds the job can run before timing out.
     *
     * @var int
     */
    // public $timeout = 60;

    public $sheetData;
    public $sheetName;
    public $isImport;
    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct($sheetData, $sheeName = 'DefaultSheet', $isImport)
    {
        $this->sheetData = $sheetData;
        $this->sheetName = $sheeName;
        $this->isImport = $isImport;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $request = new \App\Containers\Calculator\UI\WEB\Requests\StoreCalculatorRequest();
        \DB::beginTransaction();
        $importErrors = Apiato::call('Statement@ImportFormatedSpreadSheet2Task', [$this->sheetData, $request, $this->sheetName]);
        if($this->isImport && (isset($request->isImport) && $request->isImport != 0))
            \DB::commit();
        else
            \DB::rollback();

        $queueImportInfoData = [
            'sheet_name' => $this->sheetName,
            'messages' => (is_array($importErrors)) ? json_encode($importErrors) : '',
        ];
        $queueImportInfo = Apiato::call('Loan@CreateQueueImportInfoTask', [$queueImportInfoData]);
    }
}
