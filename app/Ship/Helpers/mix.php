<?php
use GuzzleHttp\Exception\GuzzleException;
use GuzzleHttp\Client;
use Carbon\Carbon;
use Apiato\Core\Foundation\Facades\Apiato;

// Add your helper functions here...


if (! function_exists('interest')) {

    /**
     * Calculate Interest
     *
     * @param float $principalAmount Principal amount of loan
     * @param float $rate Default/rate of loan
     * @param int $duration Duration of loan in months
     * @return float Interest Rate 
     */
    function interest(float $principalAmount, float $rate, int $duration) {

    	$ratePerYear 		= $rate/100;
    	$durationInYears 	= round($duration/12, 2);
    	$totalAccruedAmount = $principalAmount*(1+($ratePerYear * $durationInYears));
    	return round($totalAccruedAmount, 2);
    }
}

if (! function_exists('perDay')) {

    /**
     * Calculate Per Day Amount
     *
     * @param float $amount total amount
     * @return float Per Day amount 
     */
    function perDay(float $amount) {
        // TODO what in case of leap year
    	return round($amount/365, 2);
    }
}

if (! function_exists('perMonth')) {

    /**
     * Calculate Per Month
     *
     * @param float $amount total amount
     * @param float $duration Duration in months
     * @return float Per Day amount 
     */
    function perMonth(float $amount, int $duration) {
    	return round($amount/$duration, 2);
    }
}


if (! function_exists('calculateMonthlyPMT')) {

    /**
     * Calculate monthly payment
     *
     * @param float $amount total amount
     * @param float $amount total amount
     * @return float Per Day amount 
     */
    function calculateMonthlyPMT($rate, $amount) {
        return round(resolvePercentage($rate) * $amount, 2);
    }
}

if (! function_exists('resolvePercentage')) {
    /**
     * Calculate monthly payment
     *
     * @param float $value Value need to resolve
     * @return float Value After resolving 
     */
    function resolvePercentage($value) {
        return $value/100;
    }
}


if (! function_exists('calculateDailyRate')) {
    /**
     * Calculate Daily Payment Rate
     *
     * @param float $rate Interest Rate of loan
     * @param float $balance Current balance
     * @param float $loan optional loan object for retained loan interest
     * @return float Daily Rate based on balance 
     */
    function calculateDailyRate(float $rate, float $balance, $loan = false, $startDate = false): float {
        if($loan != false && $loan->loan_type === 'retained'){
            $duration = $loan->interest->duration;
            $monthlyPmt = resolvePercentage($rate) * $balance;
            $retainedInterest = $duration * $monthlyPmt;
            $closingDate = addDecimalMonthsIntoDate($duration, $loan->issue_at)->copy()->subDay();
            $totalDays = $loan->issue_at->diffInDays($closingDate) + 1;
                // var_dump($startDate && $startDate->gt($closingDate));
            if($rate === $loan->interest->default_rate || ($startDate && $startDate->gt($closingDate)))
                $dailyRate = round((round(resolvePercentage($rate) * $balance, 2))/30.4, 2);
            else
                $dailyRate = $retainedInterest/$totalDays;
            // var_dump($dailyRate);
            return $dailyRate;
        }
        else
            return (resolvePercentage($rate) * $balance) / 30.4;
            // return round((round(resolvePercentage($rate) * $balance, 2))/30.4, 2);
    }
}

if (! function_exists('getRetainedInterest')) {
    /**
     * Calculate retained interest
     *
     * @param float $rate Interest Rate of loan
     * @return float Daily Rate based on balance 
     */
    function getRetainedInterest($dailyRate, $loan = false)
     {
        $duration = $loan->interest->duration;
        $closingDate = addDecimalMonthsIntoDate($duration, $loan->issue_at)->copy()->subDay();
        $totalDays = $loan->issue_at->diffInDays($closingDate) + 1;
        return $dailyRate * $totalDays;
    }
}

if (! function_exists('getDueDateHolidays')) {
    /**
     * Get holidays form current due date
     *
     * @param float $currentDueDate 
     * @return int number of days
     */
    function getDueDateHolidays($currentDueDate)
    {
        $days = 0;
        do{
            $publicHoliday = $currentDueDate->isSaturday() ? $currentDueDate->isSaturday() : $currentDueDate->isSunday();                        
            if(!$publicHoliday)
                $publicHoliday = Apiato::call('Holiday@FindHolidayByDateTask', [$currentDueDate]);

            if($publicHoliday){
                $days++;
                $currentDueDate->addDay();
            }
        } while($publicHoliday);

        return $days;
    }
}

if(! function_exists('floatgt')) {
    /**
     * Compare two float values
     *
     * @param float $float1
     * @param float $float2
     * @return true if float1 is greater than float2
     */

    function floatgt($float1, $float2) {
        $epsilon = 0.00001;
        if (abs($float1 - $float2) < $epsilon) {  
            return false;  
        }  
        else  
        {  
            if ($float1 > $float2) {  
                return true;  
            }
            else
                return false;  
        }  
    }
}

if(! function_exists('floateq')) {
    /**
     * Compare two float values
     *
     * @param float $float1
     * @param float $float2
     * @return true if float1 is equal to float2
     */

    function floateq($float1, $float2) {
        $epsilon = 0.00001;
        if (abs($float1 - $float2) < $epsilon)
            return true;  
        else
            return false;
    }
}

if(! function_exists('floatgte')) {
    /**
     * Compare two float values
     *
     * @param float $float1
     * @param float $float2
     * @return true if float1 is greater than or equal to float2
     */

    function floatgte($float1, $float2) {
        $epsilon = 0.00001;
        if (floatgt($float1, $float2) || floateq($float1, $float2, '='))
            return true;  
        else
            return false;
    }
}

if (! function_exists('exception')) {

    /**
     * Formate Exception
     *
     * @param Exception $e Exception
     * @return mixed Formated exception 
     */
    function exception($e)
    {
        $rsp = json_decode((string)$e->getResponse()->getBody(true));

        if(isset($rsp->errors)){
            return redirect()->back()->withErrors($rsp->errors)->withInput();       
        }

        if(isset($rsp->message)){
            return redirect()->back()->withErrors($rsp->message)->withInput();
        }

        return redirect()->back()->withInput();
    }
}


if (! function_exists('get')) {

    /**
     * Sends HTTP GET Request
     *
     * @param string $apiEndPoint Exception
     * @param array $params Parameters of GET Request sends as query parameters
     * @param array $json If true returns JSON encoded response default value is false
     * @return mixed Result of HTTP request 
     */
    function get($apiEndPoint, $params = array(), $json = false)
    {
        $client = new Client();
        try {

            if(!empty($params)) {
                $encoded = http_build_query($params);
                $apiEndPoint = $apiEndPoint .'?'.$encoded;
            }

            $response_raw = $client->get($apiEndPoint, [
                    'headers' => [
                        'Accept'        => 'application/json',
                        'Authorization' => session('auth_token'),
                    ]
                ],
                [
                	'timeout'  => 240
            	]
            );
            $response = $response_raw->getBody();
            if(!$json)
                $response = json_decode((string) $response);
            return $response;
        } catch (GuzzleHttp\Exception\BadResponseException $e) {
            throw $e;
        }
    }
}



if (! function_exists('post')) {
    /**
     * Sends HTTP POST Request
     *
     * @param string $apiEndPoint Exception
     * @param array $params Parameters of POST
     * @param array $json If true returns JSON encoded response default value is false
     * @return mixed Result of HTTP request 
     */
    function post($apiEndPoint, $params=array(), $json = false) {

        $client = new Client();
        try {
            if(!empty($params)) {
                $encoded = http_build_query($params);
                $apiEndPoint = $apiEndPoint .'?'.$encoded;
            }
                
            $response_raw = $client->post($apiEndPoint, [
                'form_params' => $params,
                'headers' => [
                   'Accept'         => 'application/json',
                   'Authorization'  => session('auth_token')
                ],

               ],
               [
                'timeout' => 300
            ]);
            $response = (string)$response_raw->getBody();
            if(!$json) {
                $response = json_decode((string) $response);
            }
            return $response;
        } catch (GuzzleHttp\Exception\BadResponseException $e) {
        	throw $e;
        }
    }
}

if(! function_exists('postWithFile')) {
    /**
     * Sends HTTP postWithFile Request
     *
     * @param string $apiEndPoint Exception
     * @param array $fileName Name of file input
     * @param array $json If true returns JSON encoded response default value is false
     * @return mixed Result of HTTP request 
     */
    function postWithFile($apiEndPoint, $fileName = "file", $params=array(), $json = false) {
        $arr = [
            'name' => $fileName,
            'filename' => $_FILES[$fileName]['name'],
            'Mime-Type' => $_FILES[$fileName]['type'],
            'contents' => file_get_contents($_FILES[$fileName]['tmp_name']),
            'headers' => ['Content-Type' => 'multipart/form-data']
        ];
        try{
            if(!empty($params)) {
                $encoded = http_build_query($params);
                $apiEndPoint = $apiEndPoint .'?'.$encoded;
            }
            $client = new Client();
            $response_raw = $client->post($apiEndPoint, [
                'multipart' => [$arr],
                'headers' => [
                    'Accept' => 'application/json',
                    'Authorization' => session('auth_token'),
                ],
            ]);
            $response = (string)$response_raw->getBody();
            if(!$json) {
                $response = json_decode((string) $response);
            }
            return $response;
            // return redirect()->back()->with('importResponse', json_decode((string)$response_raw->getBody(), true));
        // } catch (\Exception $e) {
        // return exception($e);
        } catch (GuzzleHttp\Exception\BadResponseException $e) {
            throw $e;
        }
    }
}

if (! function_exists('delete')) {
    /**
     * Sends HTTP DELETE Request
     *
     * @param string $apiEndPoint Exception
     * @param array $params Parameters of DELETE
     * @param array $json If true returns JSON encoded response default value is false
     * @return mixed Result of HTTP request 
     */
    function delete($apiEndPoint, $params=array(), $json = false) {
    	$client = new Client();
    	try {
                if(!empty($params)) {
                    $encoded = http_build_query($params);
                    $apiEndPoint = $apiEndPoint .'?'.$encoded;
                }
                
    	    $response_raw = $client->delete($apiEndPoint, [
    	        'headers' => [
    	        	'Accept'          => 'application/json',
    	        	'Authorization'   => session('auth_token')
    	        ],
    	    ]);
    	    $response = $response_raw->getBody();
    	    if(!$json)
    	    	$response = json_decode((string) $response);
    	    return $response;
    	} catch (GuzzleHttp\Exception\BadResponseException $e) {
                throw $e;
    	}
    }
}

if (! function_exists('patch')) {

    /**
     * Sends HTTP PATCH Request
     *
     * @param string $apiEndPoint Exception
     * @param array $params Parameters of PATCH
     * @param array $json If true returns JSON encoded response default value is false
     * @return mixed Result of HTTP request 
     */
    function patch($apiEndPoint, $params=array(), $json = false) {
    	$client = new Client();
    	try {
                if(!empty($params)) {
                    $encoded = http_build_query($params);
                    $apiEndPoint = $apiEndPoint .'?'.$encoded;
                }
            
    	    $response_raw = $client->patch($apiEndPoint, [
    	        'headers' => [
    	        	'Accept'          => 'application/json',
    	        	'Authorization'   => session('auth_token')
    	        ],
    	    ]);
    	    $response = $response_raw->getBody();
    	    if(!$json)
    	    	$response = json_decode((string) $response);
    	    return $response;
    	} catch (GuzzleHttp\Exception\BadResponseException $e) {
            throw $e;
    	}
    }
}

if (! function_exists('sortByDate')) {
    /**
     * Sort Given Array by date
     *
     * @param array $data Exception
     * @param array $params Parameters of POST
     * @param array $json If true returns JSON encoded response default value is false
     * @return mixed Result of HTTP request 
     */
    function sortByDate($recodes, $col = null) {
        return collect($recodes)->sortBy(function($recode) use ($col) {
            if(!$col) {
                $col = 'date';
            }
            return ($recode[$col])->getTimestamp();
        });
    }
}

if (! function_exists('getNumber')) {
    /**
     * Sort Given Array by date
     *
     * @param array $params Parameters of POST
     * @param array $json If true returns JSON encoded response default value is false
     * @return mixed Result of HTTP request
     */
    function getNumber($numberFormatVal) {
        return floatval(preg_replace('/[^\d.]/', '', $numberFormatVal));
    }
}

if (! function_exists('removeExtraSpaces')) {
    /**
     * Remove extra white spaces in a string
     *
     * @param string $string
     * @return string without extra spaces
     */
    function removeExtraSpaces($string) {
        return preg_replace('/\s+/', ' ', $string);
    }
}

if (! function_exists('addDecimalMonthsIntoDate')) {
    /**
     * Add decimal months into date
     *
     * @param float $floatDuration
     * @param start date $startDate
     * @return date after adding float months into start date
     */
    function addDecimalMonthsIntoDate(float $floatDuration, $startDate) {
        $startDate = Carbon::parse($startDate);
        $duration = explode('.', $floatDuration);
        // get days of decimal points, $duration[0] -> is months, $duration[1] is decimal months.
        $decimalDays = $floatDuration - $duration[0];
        $endDate = $startDate->copy()->addMonths($duration[0])->copy()->addDays(round($decimalDays*30.4));
        return $endDate;
    }
}

if (! function_exists('getExcelDate')) {
    /**
     * Get date for excel
     *
     * @param date $date
     * @param date default format $format
     * @return date date if it's a true date else false
     */
    function getExcelDate($date, $format = 'd/m/Y')
    {
        $d = \DateTime::createFromFormat($format, $date);
        // The Y ( 4 digits year ) returns TRUE for any integer with any number of digits so changing the comparison from == to === fixes the issue.
        $format = $d && $d->format($format) === $date;
        if($d){
            $dateArr = explode('/', (string)$date);
            if($format)
                $dateObj = Carbon::parse($dateArr[0].'-'.$dateArr[1].'-'.$dateArr[2]);
            else
                $dateObj = Carbon::parse($dateArr[1].'-'.$dateArr[0].'-'.$dateArr[2]);
        }
        else
            return false;

        return $dateObj;
    }
}

if (! function_exists('trimedLowerStr')) {
    /**
     * Get lower case string with remove whitespaces
     *
     * @param tring $str
     * @return lower cased string with removed whitspaces
     */
    function trimedLowerStr($str)
    {
        $str = removeExtraSpaces($str);
        return trim(strtolower($str));
    }
}

if (! function_exists('formatedDate')) {
    /**
     * Get Formated Date From Carbon
     *
     * @param string $strDate
     * @return carbon formated date
     */
    function formatedDate($strDate)
    {
        return Carbon::parse($strDate)->format('Y M d');
    }
}

if (! function_exists('getTransactionGrace')) {
    /**
     * Get Transaction grace
     *
     * @param string $loan
     * @return carbon formated tnxDate
     */
    function getTransactionGrace($loan, $tnxDate)
    {
        $graces = collect($loan->grace)->filter(function ($item) use ($tnxDate) {
            $graceStartDate = Carbon::parse($item->start_date);
            $graceEndDate = Carbon::parse($item->end_date);
            return ($graceEndDate->gte($tnxDate)) && ($graceStartDate->lte($tnxDate));
        });

        return $graces;
    }
}