<?php

namespace App\Ship\Middlewares\Http;
use App\Ship\Parents\Middlewares\Middleware;
use Illuminate\Http\Request;
use Closure;

class TempDatabase extends Middleware
{

    /**
     * The URIs that should be excluded from TEMPDB verification.
     *
     * @var array
     */
    protected $except = [
        //
    ];
    
    public function handle(Request $request, Closure $next)
    {
        // \DB::setDefaultConnection('tempdb');
        return $next($request);
    }

}
